#!/bin/bash
#
#	Scripted by noesis
#  	Version 1.1
#
#	TODO: 
#	- autodetectar la cantidad de nucleos 				--listo
#	- si no encuentra alguna libreria, detener todo
#	- que cada vez que si inicie, descomprima libav-0.8.12.* --listo
#	- Saltar los warnings al compilar
#	- Algunas weas quedaron medias pencas
#	- Crear algunas funciones para los procesos que se repiten	--listo
#   	- Bajar la wata oiajoiajoaijajaj XD
#	- Como siempre las liberias libav son las mismas, es mejor copiarlas al source	--listo
#	- Convertir las procedimiento en funciones y que validan por parametros sus estados
#	- Crear un archivo comprimido con la cosas necesario para usar StepNXA en otra distro	--listo
#	- Crear un menu principal para escojer lo que quieres hacer, pero lo encuentro inutil
#	- Capturar la momento de que este actualizado el repo "already-update o algo asi" ofrezca un reset --head y wea

# Dependencias (Debian o basadas, Solo con libavcodec53, el libavcodec53 es necesario solo para compilar, para ejecutar puede ser una version posterior)
# sudo aptitude install libasound2-dev libpulse-dev libmad0-dev libtheora-dev libvorbis-dev libpng-dev libswscale-dev libavutil-dev libavformat-dev libavcodec-dev libjpeg-dev libglu1-mesa-dev libgl1-mesa-dev libgtk2.0-dev xorg-dev libxrandr-dev libbz2-dev libglew1.5-dev automake1.10 build-essential curl g++ yasm libncurses5-dev build-essential cmake git subversion automake bc

# Dependencias (Archlinux or based: Solo para ejecutar)
# sudo pacman -Sy libmad libpulse libpng12

# Dependencias (Archlinux or based: Solo para compilar: AUN NO SE PUEDE COMPILAR!!! WIP)
# sudo pacman -Sy base-devel yasm git libmad libpulse libpng12

# Variable Globales
core=`cat /proc/cpuinfo | grep processor | wc -l`
let "core = $core + 1"
HOY=`date`
clear

function cleanBuild
{
	make distclean
}

function backupBin
{ 
if [ -d backups ]
then
	if [[ -e "stepmania" && -e "GtkModule.so" ]];
	then
		echo "Moviendo binarios antiguos a backups/"
		mv stepmania backups/stepmania_"$HOY"
		mv GtkModule.so backups/GtkModule.so_"$HOY"
		echo "Binarios respaldados.... Go ahead"
	else
		echo "Sin binarios para respaldar.... Skipping"
	fi
else
	mkdir backups
	mv stepmania backups/stepmania_"$HOY"
	mv GtkModule.so backups/GtkModule.so_"$HOY"
fi
}

function makeSongsFolder
{
	if [ -d Songs ];
	then
		echo ""
	else
		mkdir Songs
	fi
}

function copyLibav
{
	if [ -d extern ];
	then
		cp extern/libav/libavcodec.a src/
		cp extern/libav/libavutil.a src/
		cp extern/libav/libavformat.a src/
		cp extern/libav/libswscale.a src/
	else	
		echo "No esta la carpeta extern.... Stop"
	fi
}

function buildLibav
{
if [ -d libav-0.8.12 ];
then
	echo "Compilando libav-0.8.12.... Wait"
	cd libav-0.8.12
	#cleanBuild
	./configure --enable-static --disable-vaapi
	make -j$core -s
else
	if [ -e "libav_0.8.12.orig.tar.gz" ];
	then
		echo "Descomprimiendo libav-0.8.12.... Wait"
		tar -xzf libav_0.8.12.orig.tar.gz
		echo "Compilando libav-0.8.12.... Wait"
		cd libav-0.8.12
		cleanBuild
		./configure --enable-static --disable-vaapi
		make -j$core -s
	else
			echo "No esta libav_0.8.12.orig.tar.gz!!!!!.... Stop"
	fi
fi

	echo "Copiando librerias para linkear con StepNXA"
	if [ -e "libavcodec/libavcodec.a" ];
	then
		cp libavcodec/libavcodec.a ../src/
		echo "Copiado libavcodec.a en src/"
		else
		echo "No esta libavcodec.a"
		fi

	if [ -e "libavutil/libavutil.a" ];
	then
		cp libavutil/libavutil.a ../src/
		echo "Copiado libavutil.a en src/"
	else
		echo "No esta libavutil.a"
	fi

	if [ -e "libavformat/libavformat.a" ];
	then
		cp libavformat/libavformat.a ../src/
		echo "Copiado libavformat.a en src/"
	else
		echo "No esta libavformat.a"
	fi

	if [ -e "libswscale/libswscale.a" ];
	then
		cp libswscale/libswscale.a ../src/
		echo "Copiado libswscale.a en src/"
	else
		echo "No esta libswscale.a"
	fi
}

function linkearConFFMPEG
{
g++ -fno-exceptions -finline-limit=300   -Wall -W -Wno-unused -Wno-switch -O3   -rdynamic -o stepmania Screen.o ScreenAttract.o ScreenBookkeeping.o ScreenCenterImage.o ScreenContinue.o ScreenCredits.o ScreenDebugOverlay.o ScreenDemonstration.o ScreenEdit.o ScreenEditMenu.o ScreenEnding.o ScreenEvaluation.o ScreenExit.o ScreenNetEvaluation.o ScreenNetSelectMusic.o ScreenNetSelectBase.o ScreenNetRoom.o ScreenGameplay.o ScreenGameplayLesson.o ScreenGameplayNormal.o ScreenGameplaySyncMachine.o ScreenHowToPlay.o ScreenInstructions.o ScreenJukebox.o ScreenMapControllers.o ScreenMessage.o ScreenMiniMenu.o ScreenNameEntry.o ScreenNameEntryTraditional.o ScreenOptions.o ScreenOptionsEditCourse.o ScreenOptionsEditCourseEntry.o ScreenOptionsEditProfile.o ScreenOptionsManageCourses.o ScreenOptionsManageEditSteps.o ScreenOptionsManageProfiles.o ScreenOptionsMaster.o ScreenOptionsMasterPrefs.o ScreenOptionsToggleSongs.o ScreenPackages.o ScreenNetworkOptions.o ScreenPlayerOptions.o ScreenProfileLoad.o ScreenProfileSave.o ScreenPrompt.o ScreenRanking.o ScreenReloadSongs.o ScreenSandbox.o ScreenSaveSync.o ScreenServiceAction.o ScreenStatsOverlay.o ScreenSelect.o ScreenSelectCharacter.o ScreenSelectDifficulty.o ScreenSelectGroup.o ScreenSelectLanguage.o ScreenSelectMaster.o ScreenSelectMode.o ScreenSelectMusic.o ScreenSelectProfile.o ScreenSelectStyle.o ScreenSyncOverlay.o ScreenSystemLayer.o ScreenSetTime.o ScreenSongOptions.o ScreenSplash.o ScreenStage.o ScreenTestFonts.o ScreenTestInput.o ScreenTestLights.o ScreenTestSound.o ScreenTextEntry.o ScreenTitleMenu.o ScreenUnlockBrowse.o ScreenUnlockCelebrate.o ScreenUnlockStatus.o ScreenWithMenuElements.o ScreenWorldMax.o ScreenSMOnlineLogin.o Attack.o AutoKeysounds.o AdjustSync.o BackgroundUtil.o BannerCache.o CatalogXml.o Character.o CodeDetector.o CodeSet.o Command.o CommonMetrics.o Course.o CourseLoaderCRS.o CourseUtil.o CourseWriterCRS.o DateTime.o Difficulty.o EnumHelper.o Font.o FontCharAliases.o FontCharmaps.o Game.o GameCommand.o GameplayAssist.o GameConstantsAndTypes.o GamePreferences.o GameInput.o Grade.o HighScore.o Inventory.o LocalizedString.o LuaReference.o LuaExpressionTransform.o LyricsLoader.o ModsGroup.o NoteData.o NoteDataUtil.o NoteDataWithScoring.o NoteTypes.o NotesLoader.o NotesLoaderBMS.o NotesLoaderDWI.o NotesLoaderKSF.o NotesLoaderMidi.o NotesLoaderSM.o NotesWriterDWI.o NotesWriterSM.o OptionRowHandler.o OptionsList.o PlayerAI.o PlayerNumber.o PlayerOptions.o PlayerStageStats.o PlayerState.o Preference.o Profile.o RandomSample.o RadarValues.o SampleHistory.o ScreenDimensions.o ScoreKeeper.o ScoreKeeperGuitar.o ScoreKeeperNormal.o ScoreKeeperRave.o Song.o SongCacheIndex.o SongOptions.o SongUtil.o StageStats.o Steps.o SoundEffectControl.o StepsUtil.o Style.o StyleUtil.o TimingData.o Trail.o TrailUtil.o TitleSubstitution.o Tween.o Workout.o Mission.o MissionArrowMods.o MissionBullet.o MissionLoaderMSF.o MissionSector.o MissionSelectSector.o ScreenTransition.o StationWheel.o StationWheelItem.o NotesLoaderSSC.o NotesLoaderSMA.o NotesLoaderNXA.o NotesWriterNXA.o RoomWheel.o IniFile.o MsdFile.o XmlFile.o XmlFileUtil.o ExportStrings.o StepMania.o GameLoop.o global.o SpecialFiles.o LoadingWindow.o LoadingWindow_Gtk.o RageSoundDriver.o RageSoundDriver_Null.o RageSoundDriver_Generic_Software.o RageSoundDriver_OSS.o ALSA9Dynamic.o ALSA9Helpers.o RageSoundDriver_ALSA9_Software.o ArchHooks.o ArchHooksUtil.o ArchHooks_Unix.o InputHandler.o InputHandler_MonkeyKeyboard.o InputHandler_X11.o InputHandler_Linux_Joystick.o InputHandler_Linux_PIUIO.o InputHandler_Linux_Event.o MovieTexture.o MovieTexture_Generic.o MovieTexture_Null.o  MovieTexture_FFMpeg.o LightsDriver.o LightsDriver_Export.o LightsDriver_SystemMessage.o LightsDriver_LinuxWeedTech.o LightsDriver_LinuxParallel.o MemoryCardDriver.o MemoryCardDriverThreaded_Linux.o LowLevelWindow.o LowLevelWindow_X11.o X11Helper.o AssertionHandler.o GetSysInfo.o SignalHandler.o LinuxThreadHelpers.o RunningUnderValgrind.o EmergencyShutdown.o Backtrace.o BacktraceNames.o CrashHandler.o CrashHandlerChild.o CrashHandlerInternal.o Dialog.o DialogDriver.o Threads_Pthreads.o RageDriver.o BGAnimation.o BGAnimationLayer.o Banner.o DifficultyIcon.o MeterDisplay.o StreamDisplay.o Transition.o ActiveAttackList.o BPMDisplay.o ComboGraph.o CourseContentsList.o DifficultyList.o DifficultyMeter.o DifficultyRating.o DualScrollBar.o EditMenu.o FadingBanner.o GradeDisplay.o GraphDisplay.o GrooveRadar.o GroupList.o HelpDisplay.o MemoryCardDisplay.o MenuTimer.o MusicBannerWheel.o MusicList.o MusicSortDisplay.o MusicWheel.o MusicWheelItem.o OptionIcon.o OptionIconRow.o OptionRow.o OptionsCursor.o PaneDisplay.o ScrollBar.o ScrollingList.o SnapDisplay.o TextBanner.o WheelBase.o WheelItemBase.o WheelNotifyIcon.o WorkoutGraph.o RoomInfoDisplay.o NetPlayerInfo.o ArrowEffects.o AttackDisplay.o Background.o BeginnerHelper.o CombinedLifeMeterTug.o DancingCharacters.o Foreground.o GhostArrowRow.o HoldJudgment.o LifeMeter.o LifeMeterBar.o LifeMeterBattery.o LifeMeterTime.o LyricDisplay.o NoteDisplay.o NoteField.o PercentageDisplay.o Player.o ReceptorArrow.o ReceptorArrowRow.o ScoreDisplay.o ScoreDisplayAliveTime.o ScoreDisplayBattle.o ScoreDisplayCalories.o ScoreDisplayLifeTime.o ScoreDisplayNormal.o ScoreDisplayOni.o ScoreDisplayPercentage.o ScoreDisplayRave.o get.o maketables.o pcre.o study.o lapi.o lauxlib.o lbaselib.o lcode.o ldblib.o ldebug.o ldo.o ldump.o lfunc.o lgc.o linit.o liolib.o llex.o lmathlib.o lmem.o loadlib.o lobject.o lopcodes.o loslib.o lparser.o lstate.o lstring.o lstrlib.o ltable.o ltablib.o ltm.o lundump.o lvm.o lzio.o RageFileBasic.o RageFile.o RageFileDriver.o RageFileManager.o RageFileDriverDirect.o RageFileDriverDirectHelpers.o RageFileDriverMemory.o RageFileDriverZip.o RageFileDriverDeflate.o RageFileDriverSlice.o RageFileDriverTimeout.o RageSoundReader_WAV.o RageSoundReader_Vorbisfile.o RageSoundReader_MP3.o RageBitmapTexture.o RageDisplay.o RageDisplay_OGL.o RageDisplay_OGL_Helpers.o RageDisplay_Null.o RageException.o RageInput.o RageInputDevice.o RageLog.o RageMath.o RageModelGeometry.o RageSound.o RageSoundManager.o RageSoundUtil.o RageSoundMixBuffer.o RageSoundPosMap.o RageSoundReader.o RageSoundReader_FileReader.o RageSoundReader_ChannelSplit.o RageSoundReader_Extend.o RageSoundReader_Merge.o RageSoundReader_PitchChange.o RageSoundReader_PostBuffering.o RageSoundReader_Pan.o RageSoundReader_Preload.o RageSoundReader_Resample_Good.o RageSoundReader_SpeedChange.o RageSoundReader_ThreadedBuffer.o RageSoundReader_Chain.o RageSurface.o RageSurfaceUtils.o RageSurfaceUtils_Dither.o RageSurface_Save_JPEG.o RageSurfaceUtils_Palettize.o RageSurfaceUtils_Zoom.o RageSurface_Load.o RageSurface_Load_PNG.o RageSurface_Load_JPEG.o RageSurface_Load_GIF.o RageSurface_Load_BMP.o RageSurface_Load_XPM.o RageTexture.o RageTexturePreloader.o RageTextureRenderTarget.o RageSurface_Save_BMP.o RageSurface_Save_PNG.o RageTextureID.o RageTextureManager.o RageThreads.o RageTimer.o RageTypes.o RageUtil.o RageUtil_CachedObject.o RageUtil_CharConversions.o RageUtil_BackgroundLoader.o RageUtil_FileDB.o RageUtil_WorkerThread.o Actor.o ActorFrame.o ActorFrameTexture.o ActorMultiTexture.o ActorProxy.o ActorScroller.o ActorUtil.o ActorSound.o AutoActor.o BitmapText.o Model.o DynamicActorScroller.o ModelManager.o ModelTypes.o Quad.o RollingNumbers.o Sprite.o AnnouncerManager.o Bookkeeper.o CharacterManager.o CryptManager.o FontManager.o GameSoundManager.o GameManager.o GameState.o InputFilter.o InputMapper.o InputQueue.o LuaBinding.o LuaManager.o LightsManager.o MemoryCardManager.o MessageManager.o NetworkSyncManager.o NoteSkinManager.o PrefsManager.o ProfileManager.o ScreenManager.o SongManager.o StatsManager.o ThemeManager.o UnlockManager.o WorkoutManager.o ezsockets.o ver.o -Wl,-static -lvorbisfile -lvorbis -logg -Wl,-dy -lmad -lX11 -lXtst -lXrandr -lGL -lGLU libtomcrypt.a libtommath.a -ldl  -Wl,-static -lpng -lz -lm -Wl,-dy libavformat.a libavcodec.a libavutil.a libswscale.a -lbz2 -Wl,-static -ljpeg -Wl,-dy -Wl,-static -lz -Wl,-dy -lpulse -lpthread -lrt
}

function buildStepNXA
{
	echo "Compilando StepNXA"
	cleanBuild
	./autogen.sh && clear && ./configure && clear
	make -j$core -s

	cd src/
	sh linkear_con_ffmpeg.sh
	mv stepmania GtkModule.so ../

	if [[ -e "../stepmania" && -e "../GtkModule.so" ]];
	then
		echo "Binario Compilado!"
		cd .. && ls -la stepmania GtkModule.so
	else
		echo "Algo salio mal, revisa la wea!"
	fi
}

function buildStepNXANew
{
	echo "Compilando StepNXA"
	cleanBuild
	./autogen.sh && clear && ./configure && clear
	make -j$core -s
	cd src
	linkearConFFMPEG
}

function checkBuildStepNXA
{
	if [[ -e "stepmania" && -e "GtkModule.so" ]];
	then
		mv stepmania ../
		mv GtkModule.so ../
		cd ..
		clear
		ls -la stepmania GtkModule.so
		echo "Binario Compilado!"
	else
		echo "Algo salio mal, revisa la wea!"
	fi	
}

function updateSourceBeforeBuild
{
	git pull
}

function buildPackageStepNXA
{
# Es todo lo necesario para que cualquier mortal puedo ejecutar StepNXA en alguna distro
# NO COMPARTIR, binario con COINMODE!!!
echo "Creando portable StepNXA"
tar -zcf StepNXA.tar.gz BackgroundEffects BackgroundTransitions BGAnimations channels Characters Courses Data Manual NoteSkins Themes Songs GtkModule.so stepmania
}

backupBin
copyLibav
#buildLibav
#buildStepNXA
updateSourceBeforeBuild
makeSongsFolder
buildStepNXANew
checkBuildStepNXA
buildPackageStepNXA
