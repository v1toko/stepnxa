#include "global.h"
#include "ScreenTransition.h"
#include "MessageManager.h"
#include "GameState.h"

REGISTER_SCREEN_CLASS( ScreenTransition );

ScreenTransition::ScreenTransition()
{
	FOREACH_PlayerNumber( pn )
	{
		m_bCenterIsPressed[pn] = false;
	}

	//ALLOW_LATE_JOIN.Load( m_sName, "AllowLateJoin" );
}

ScreenTransition::~ScreenTransition()
{
}

void ScreenTransition::HandleScreenMessage( const ScreenMessage SM )
{
	if( SM == SM_MenuTimer )
		this->PostScreenMessage( SM_BeginFadingOut, 0 );
	else if( SM == SM_BeginFadingOut )
	{
		if( !IsTransitioning() )
			StartTransitioningScreen( SM_GoToNextScreen );
	}

	ScreenWithMenuElements::HandleScreenMessage( SM );
}

void ScreenTransition::MenuStart( const InputEventPlus &input )
{
	if( input.type != IET_FIRST_PRESS )
		return;

	if( !GAMESTATE->IsPlayerEnabled( input.pn ) )
	{
		//if( ALLOW_LATE_JOIN )
		//GAMESTATE->JoinInput( input.pn );

		//// HACK: Only play start sound for the 2nd player who joins.  The 
		//// start sound for the 1st player will be played by ScreenTitleMenu 
		//// when the player makes a selection on the screen.
		//if( GAMESTATE->GetNumSidesJoined() > 1 )
		//	SCREENMAN->PlayStartSound();

		return;
	}

	if( !m_bCenterIsPressed[input.pn] )
	{
		m_bCenterIsPressed[input.pn] = true;
		MESSAGEMAN->Broadcast( ssprintf( "CenterWasPressedP%d", input.pn+1 ) );
		//return;
	}

	if( GAMESTATE->GetNumHumanPlayers() > 1 ) //2 players
	{
		if( m_bCenterIsPressed[0] && m_bCenterIsPressed[1] )
			this->PostScreenMessage( SM_BeginFadingOut, 0 );
	}
	else
	{
		if( m_bCenterIsPressed[input.pn] )
			this->PostScreenMessage( SM_BeginFadingOut, 0 );
	}
}

#include "LuaBinding.h"

class LunaScreenTransition: public Luna<ScreenTransition>
{
public:
	
	LunaScreenTransition()
	{
	}
};

LUA_REGISTER_DERIVED_CLASS( ScreenTransition, ScreenWithMenuElements )
// lua end

/*
 * (c) 2009 "v1t0ko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */