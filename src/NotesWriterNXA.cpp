#include <cerrno>
#include <cstring>
#include "global.h"
#include "NotesWriterNXA.h"
#include "BackgroundUtil.h"
#include "Foreach.h"
#include "GameManager.h"
#include "LocalizedString.h"
#include "NoteTypes.h"
#include "Profile.h"
#include "ProfileManager.h"
#include "RageFile.h"
#include "RageFileManager.h"
#include "RageLog.h"
#include "RageUtil.h"
#include "song.h"
#include "Steps.h"
//#include "Attack.h"

static RString BackgroundChangeToString( const BackgroundChange &bgc )
{
	// TODO: Technically we need to double-escape the filename (because it might contain '=') and then
	// unescape the value returned by the MsdFile.
	RString s = ssprintf( 
		"%.3f=%s=%.3f=%d=%d=%d=%s=%s=%s=%s=%s", 
		bgc.m_fStartBeat, 
		SmEscape(bgc.m_def.m_sFile1).c_str(), 
		bgc.m_fRate, 
		bgc.m_sTransition == SBT_CrossFade,		// backward compat
		bgc.m_def.m_sEffect == SBE_StretchRewind, 	// backward compat
		bgc.m_def.m_sEffect != SBE_StretchNoLoop, 	// backward compat
		bgc.m_def.m_sEffect.c_str(), 
		bgc.m_def.m_sFile2.c_str(), 
		bgc.m_sTransition.c_str(),
		SmEscape(bgc.m_def.m_sColor1).c_str(),
		SmEscape(bgc.m_def.m_sColor2).c_str()
		);
	return s;
}

static void WriteTimingTags( vector<RString> &lines, const TimingData &StepsTiming, const Steps &steps ) 
{
	//ver que hacer con esos tags

	//if( out.m_UsePiuStops[tt] )
	//	f.PutLine( "#USEPIUSTOPS"+TimingTypeToString(tt)+":YES;" );

	//if( out.m_HoldsNoBody[tt] )
	//	f.PutLine( "#HOLDSNOBODY"+TimingTypeToString(tt)+":YES;" );

	//si no hay bpms o offset, no escribimos basura!
	//el offset no, por que puede ser 0
	//if( out.m_TimingExtra[tt].m_BPMSegments.empty() )
	//	continue;

	lines.push_back( "#NOTEDATA:;" ); //mismo separador que el ssc.
	lines.push_back( ssprintf( "#OFFSET:%.6f;", StepsTiming.m_fBeat0OffsetInSeconds ) );

	//lines.push_back( "#BPMS:" );
	RString bpms = "#BPMS:";
	for( unsigned i=0; i<StepsTiming.m_BPMSegments.size(); i++ )
	{
		const BPMSegment &bs = StepsTiming.m_BPMSegments[i];

		bpms += ( ssprintf( "%.3f=%.6f", NoteRowToBeat(bs.m_iStartRow), bs.GetBPM() ) );
		if( i != StepsTiming.m_BPMSegments.size()-1 )
			bpms += ( ",\r\n" );
	}
	bpms += (";");
	lines.push_back( bpms );

	RString tc = ("#TICKCOUNTS:");
	for( unsigned i=0; i<StepsTiming.m_TickSegments.size(); i++)
	{
		const TickSegment &ts = StepsTiming.m_TickSegments[i];
		tc += ( ssprintf( "%.3f=%d",NoteRowToBeat(ts.m_fStartBeat),ts.m_iTickcount ));
		if( i != StepsTiming.m_TickSegments.size()-1)
			tc += ( ",\r\n" );
	}
	tc += ( ";" );
	lines.push_back( tc );

	RString cb = ( "#COMBOS:");
	for( unsigned i=0; i<StepsTiming.m_ComboSegments.size(); i++)
	{
		const ComboSegment &cs = StepsTiming.m_ComboSegments[i];
		cb += ( ssprintf( "%.3f=%d=%d",cs.m_fStartBeat,cs.m_iComboFactor,cs.m_iMissComboFactor ));
		if( i != StepsTiming.m_ComboSegments.size()-1)
			cb += ( ",\r\n" );
	}
	cb += ( ";" );
	lines.push_back( cb );

	RString cns = ( "#COUNTNOTESSEPARATELY:");
	for( unsigned i=0; i<StepsTiming.m_CSepSegments.size(); i++)
	{
		const CountSeparatelySegment &cs = StepsTiming.m_CSepSegments[i];
		cns += ( ssprintf( "%.3f=%d",NoteRowToBeat(cs.m_iStartRow),cs.m_iCountSep));
		if( i != StepsTiming.m_CSepSegments.size()-1)
			cns += ( ",\r\n" );
	}
	cns += ( ";" );
	lines.push_back( cns );

	RString rshh = ( "#REQUIRESTEPONHOLDHEADS:");
	for( unsigned i=0; i<StepsTiming.m_ReqHoldHeadSegments.size(); i++)
	{
		const RequireStepOnHoldHeadSegment &cs = StepsTiming.m_ReqHoldHeadSegments[i];
		rshh += ( ssprintf( "%.3f=%d",NoteRowToBeat(cs.m_iStartRow),cs.m_iRequireStep));
		if( i != StepsTiming.m_ReqHoldHeadSegments.size()-1)
			rshh += ( ",\r\n" );
	}
	rshh += ( ";" );
	lines.push_back( rshh );

	/*
	 * Testing SPEEDS tag
	RString ap = ( "#ARROWSPEEDS:");
	for( unsigned i=0; i<StepsTiming.m_ArrowSpacingSegments.size(); i++)
	{
		const ArrowSpacingSegment &as = StepsTiming.m_ArrowSpacingSegments[i];
		ap += ( ssprintf( "%.3f=%.3f=%.3f",NoteRowToBeat(as.m_iStartRow),as.m_fArrowSpacing, as.m_fShiftFactor ));
		if( i != StepsTiming.m_ArrowSpacingSegments.size()-1)
			ap += ( "," );
	}
	ap += ( ";" );
	lines.push_back( ap );
	*/
	//si encontramos un tag de piustops, dejamos todos los stops como delays
	for( unsigned i=0; i<StepsTiming.m_StopSegments.size(); i++ )
	{
		if( steps.m_bUsePiuStops )
			const_cast<TimingData&>(StepsTiming).m_StopSegments[i].m_bDelay = true;
	}

	RString st = ( "#STOPS:" );
	for( unsigned i=0; i<StepsTiming.m_StopSegments.size(); i++ )
	{
		const StopSegment &fs = StepsTiming.m_StopSegments[i];

		if( !fs.m_bDelay )
		{
			st += ( ssprintf( "%.3f=%.6f", NoteRowToBeat(fs.m_iStartRow), fs.m_fStopSeconds ) );
			if( i != StepsTiming.m_StopSegments.size()-1 )
				st += ( ",\r\n" );
		}
	}
	st += ( ";" );
	lines.push_back( st );

	RString dl = ( "#DELAYS:" );
	for( unsigned i=0; i<StepsTiming.m_StopSegments.size(); i++ )
	{
		const StopSegment &fs = StepsTiming.m_StopSegments[i];

		if( fs.m_bDelay )
		{
			dl += ( ssprintf( "%.3f=%.6f", NoteRowToBeat(fs.m_iStartRow), fs.m_fStopSeconds ) );
			if( i != StepsTiming.m_StopSegments.size()-1 )
				dl += ( ",\r\n" );
		}
	}
	dl += ( ";" );
	lines.push_back( dl );

	RString ts = ( "#TIMESIGNATURES:" );
	FOREACH_CONST( TimeSignatureSegment, StepsTiming.m_vTimeSignatureSegments, iter )
	{
		ts += ( ssprintf( "%.3f=%d=%d", NoteRowToBeat(iter->m_iStartRow), iter->m_iNumerator, iter->m_iDenominator ) );
		vector<TimeSignatureSegment>::const_iterator iter2 = iter;
		iter2++;
		if( iter2 != StepsTiming.m_vTimeSignatureSegments.end() )
			ts += ( ",\r\n" );
	}
	ts += ( ";" );
	lines.push_back( ts );

	RString fk = ( "#FAKES:");
	for( unsigned i=0; i<StepsTiming.m_FakeSegments.size(); i++)
	{
		const FakeSegment &ts = StepsTiming.m_FakeSegments[i];
		fk += ( ssprintf( "%.3f=%.6f",ts.m_fStartBeat,ts.m_fLength ));
		if( i != StepsTiming.m_FakeSegments.size()-1)
			fk += ( ",\r\n" );
	}
	fk += ( ";" );
	lines.push_back( fk );

	RString wp = ( "#WARPS:");
	for( unsigned i=0; i<StepsTiming.m_WarpSegments.size(); i++)
	{
		const WarpSegment &ts = StepsTiming.m_WarpSegments[i];
		wp += ( ssprintf( "%.3f=%.6f",ts.m_fStartBeat,ts.m_fLength ));
		if( i != StepsTiming.m_WarpSegments.size()-1)
			wp += ( ",\r\n" );
	}
	wp += ( ";" );
	lines.push_back( wp );

	RString sc = ( "#SCROLLS:");
	for( unsigned i=0; i<StepsTiming.m_ScrollSegments.size(); i++)
	{
		const ScrollSegment &ts = StepsTiming.m_ScrollSegments[i];
		sc += ( ssprintf( "%.3f=%.6f",NoteRowToBeat(ts.m_iStartRow),ts.m_fRatio ));
		if( i != StepsTiming.m_ScrollSegments.size()-1)
			sc += ( ",\r\n" );
	}
	sc += ( ";" );
	lines.push_back( sc );

	RString sp = ( "#SPEEDS:");
	for( unsigned i=0; i<StepsTiming.m_SpeedSegments.size(); i++)
	{
		const SpeedSegment &ss = StepsTiming.m_SpeedSegments[i];
		//LOG->Trace( "WRITING SSC SPEEDS: Row: %d, fRatio: %.3f, fWait: %.3f, Unit: %d", ss.m_iStartRow, ss.m_fRatio, ss.m_fWait, ss.m_iUnit );
		sp += ( ssprintf( "%.3f=%.3f=%.3f=%d",NoteRowToBeat(ss.m_iStartRow),ss.m_fRatio, ss.m_fWait, ss.m_iUnit ));
		if( i != StepsTiming.m_SpeedSegments.size()-1)
			sp += ( ",\r\n" );
	}
	sp += ( ";" );
	lines.push_back( sp );

	RString ns = "#NOTESKINCHANGES:";
	{
		unsigned i = 0;		
		FOREACH_CONST( NoteSkinSegment, StepsTiming.m_NoteSkinSegments, iter )
		{
			i++;
			ns += ( ssprintf( "%.3f=%s", NoteRowToBeat(iter->m_iStartRow), SmEscape(iter->m_sNoteSkin).c_str() ) );
			vector<NoteSkinSegment>::const_iterator iter2 = iter;
			iter2++;
			if( iter2 != StepsTiming.m_NoteSkinSegments.end() )
				ns += ( ",\r\n" );
		}
		if( i == 0 )
			ns += (SmEscape(RString("0.000000=default")).c_str());
	}
	ns += ( ";" );
	lines.push_back( ns );

	RString lb = "#LABELS:";
	{
		unsigned i = 0;		
		FOREACH_CONST( LabelSegment, StepsTiming.m_LabelSegments, iter )
		{
			i++;
			lb += ( ssprintf( "%.3f=%s", NoteRowToBeat(iter->m_iStartRow), SmEscape(iter->m_sLabel).c_str() ) );
			vector<LabelSegment>::const_iterator iter2 = iter;
			iter2++;
			if( iter2 != StepsTiming.m_LabelSegments.end() )
				lb += ( ",\r\n" );
		}
		if( i == 0 )
			lb += (SmEscape(RString("0.000000=label1")).c_str());
	}
	lb += ( ";" );
	lines.push_back( lb );

	
	RString at = ( "#ATTACKS:" );
	for( unsigned j = 0; j < steps.m_Attacks.size(); j++ )
	{
		const Attack &a = steps.m_Attacks[j];
		at += ( ssprintf( "TIME=%.2f:LEN=%.2f:MODS=%s",
			a.fStartSecond, a.fSecsRemaining, a.sModifiers.c_str() ) );

		if( j+1 < steps.m_Attacks.size() )
			at += ( ":\r\n" );
		
		at += ( "" );
	}
	at += ( ";" );
	lines.push_back( at );

	//debug
	if( steps.m_bHoldsNoBody )
		lines.push_back( "#HOLDSNOBODY:YES;" );

	//si esta este tag en yes, todos los stops deben ser delays
	//solo informativo
	//if( steps.m_bUsePiuStops )
	//	lines.push_back( "#USEPIUSTOPS:YES;" );
}

static void WriteGlobalTags( RageFile &f, const Song &out )
{
	f.PutLine( ssprintf( "#TITLE:%s;", SmEscape(out.m_sMainTitle).c_str() ) );
	f.PutLine( ssprintf( "#SUBTITLE:%s;", SmEscape(out.m_sSubTitle).c_str() ) );
	f.PutLine( ssprintf( "#ARTIST:%s;", SmEscape(out.m_sArtist).c_str() ) );
	f.PutLine( ssprintf( "#TITLETRANSLIT:%s;", SmEscape(out.m_sMainTitleTranslit).c_str() ) );
	f.PutLine( ssprintf( "#SUBTITLETRANSLIT:%s;", SmEscape(out.m_sSubTitleTranslit).c_str() ) );
	f.PutLine( ssprintf( "#ARTISTTRANSLIT:%s;", SmEscape(out.m_sArtistTranslit).c_str() ) );
	f.PutLine( ssprintf( "#GENRE:%s;", SmEscape(out.m_sGenre).c_str() ) );
	f.PutLine( ssprintf( "#CREDIT:%s;", SmEscape(out.m_sCredit).c_str() ) );
	f.PutLine( ssprintf( "#BANNER:%s;", SmEscape(out.m_sBannerFile).c_str() ) );
	f.PutLine( ssprintf( "#BACKGROUND:%s;", SmEscape(out.m_sBackgroundFile).c_str() ) );
	f.PutLine( ssprintf( "#LYRICSPATH:%s;", SmEscape(out.m_sLyricsFile).c_str() ) );
	f.PutLine( ssprintf( "#CDTITLE:%s;", SmEscape(out.m_sCDTitleFile).c_str() ) );
	f.PutLine( ssprintf( "#MUSIC:%s;", SmEscape(out.m_sMusicFile).c_str() ) );
	
	//suckin stuff
	/*
	{
		vector<RString> vs;
		FOREACH_ENUM( InstrumentTrack, it )
			if( out.HasInstrumentTrack(it) )
				vs.push_back( InstrumentTrackToString(it) + "=" + out.m_sInstrumentTrackFile[it] );
		if( !vs.empty() )
		{
			RString s = join( ",", vs );
			f.PutLine( "#INSTRUMENTTRACK:" + s + ";\n" );
		}
	}
	*/

	f.PutLine( ssprintf( "#OFFSET:%.3f;", out.m_Timing.m_fBeat0OffsetInSeconds ) );
	f.PutLine( ssprintf( "#SAMPLESTART:%.3f;", out.m_fMusicSampleStartSeconds ) );
	f.PutLine( ssprintf( "#SAMPLELENGTH:%.3f;", out.m_fMusicSampleLengthSeconds ) );
	if( out.m_fSpecifiedLastBeat > 0 )
		f.PutLine( ssprintf("#LASTBEATHINT:%.3f;", out.m_fSpecifiedLastBeat) );

	f.Write( "#SELECTABLE:" );
	switch(out.m_SelectionDisplay) {
	default: ASSERT(0);  /* fallthrough */
	case Song::SHOW_ALWAYS:		f.Write( "YES" );		break;
	case Song::SHOW_NEVER:		f.Write( "NO" );		break;
		//MODIFICADO POR MI
	case Song::SHOW_MEMORYCARD:	f.Write( "MEMCARD" );	break;
	}
	f.PutLine( ";" );

	if( out.m_iMileageCost > 0 )
		f.PutLine( ssprintf( "#MILEAGECOST:%d;", out.m_iMileageCost ) );

	if( out.m_iCreditsRequired > 0 )
		f.PutLine( ssprintf( "#CREDITSREQUIRED:%d;", out.m_iCreditsRequired ) );

	if( out.m_bIsSpecialSong )
		f.PutLine( "#SPECIALSONG:YES;" );//modificado por mi, modespecial

	//if( out.m_bUsePiuStops )
	//	f.PutLine( "#USEPIUSTOPS:YES;" );

	if( out.m_bHoldsNoBody )
		f.PutLine( "#HOLDSNOBODY:YES;" );

	switch( out.m_DisplayBPMType )
	{
	case Song::DISPLAY_ACTUAL:
		// write nothing
		break;
	case Song::DISPLAY_SPECIFIED:
		if( out.m_fSpecifiedBPMMin == out.m_fSpecifiedBPMMax )
			f.PutLine( ssprintf( "#DISPLAYBPM:%.3f;", out.m_fSpecifiedBPMMin ) );
		else
			f.PutLine( ssprintf( "#DISPLAYBPM:%.3f:%.3f;", out.m_fSpecifiedBPMMin, out.m_fSpecifiedBPMMax ) );
		break;
	case Song::DISPLAY_RANDOM:
		f.PutLine( ssprintf( "#DISPLAYBPM:*;" ) );
		break;
	}

	//cambiara el formato.
	if( out.m_sLockStepsString != "" )
		f.PutLine( ssprintf( "#LOCKSTEP:%s;", SmEscape(out.m_sLockStepsString).c_str() ) );


	//only bpms for fallback
	f.Write( "#BPMS:" );
	for( unsigned i=0; i<out.m_Timing.m_BPMSegments.size(); i++ )
	{
		const BPMSegment &bs = out.m_Timing.m_BPMSegments[i];

		f.Write( ssprintf( "%.3f=%.3f", NoteRowToBeat(bs.m_iStartRow), bs.GetBPM() ) );
		if( i != out.m_Timing.m_BPMSegments.size()-1 )
			f.Write( ",\r\n" );
	}
	f.PutLine( ";" );

	f.Write( "#TICKCOUNTS:");
	for( unsigned i=0; i<out.m_Timing.m_TickSegments.size(); i++)
	{
		const TickSegment &ts = out.m_Timing.m_TickSegments[i];
		f.Write( ssprintf( "%.3f=%d",NoteRowToBeat(ts.m_fStartBeat),ts.m_iTickcount ));
		if( i != out.m_Timing.m_TickSegments.size()-1)
			f.Write( ",\r\n" );
	}
	f.PutLine( ";" );
	//---------------------------

	//MODIFICADO POR MI
	f.Write( "#COMBOS:");
	for( unsigned i=0; i<out.m_Timing.m_ComboSegments.size(); i++)
	{
		const ComboSegment &cs = out.m_Timing.m_ComboSegments[i];
		f.Write( ssprintf( "%.3f=%d=%d",cs.m_fStartBeat,cs.m_iComboFactor,cs.m_iMissComboFactor ));
		if( i != out.m_Timing.m_ComboSegments.size()-1)
			f.Write( ",\r\n" );
	}
	f.PutLine( ";" );
	//---------------------------

	/*
	f.Write( "#ARROWSPEED:");
	for( unsigned i=0; i<out.m_Timing.m_ArrowSpacingSegments.size(); i++)
	{
		const ArrowSpacingSegment &as = out.m_Timing.m_ArrowSpacingSegments[i];
		f.Write( ssprintf( "%.3f=%.3f=%.3f",NoteRowToBeat(as.m_fStartBeat),as.m_fArrowSpacing, as.m_fShiftFactor ));
		if( i != out.m_Timing.m_ArrowSpacingSegments.size()-1)
			f.Write( "," );
	}
	f.PutLine( ";" );
	*/

	f.Write( "#FAKES:");
	for( unsigned i=0; i<out.m_Timing.m_FakeSegments.size(); i++)
	{
		const FakeSegment &ts = out.m_Timing.m_FakeSegments[i];
		f.Write( ssprintf( "%.3f=%.3f",NoteRowToBeat(ts.m_fStartBeat),ts.m_fLength ));
		if( i != out.m_Timing.m_FakeSegments.size()-1)
			f.Write( ",\r\n" );
	}
	f.PutLine( ";" );

	f.Write( "#STOPS:" );
	for( unsigned i=0; i<out.m_Timing.m_StopSegments.size(); i++ )
	{
		const StopSegment &fs = out.m_Timing.m_StopSegments[i];

		f.PutLine( ssprintf( "%.3f=%.3f", NoteRowToBeat(fs.m_iStartRow), fs.m_fStopSeconds, fs.m_bDelay ) );
		if( i != out.m_Timing.m_StopSegments.size()-1 )
			f.Write( ",\r\n" );
	}
	f.PutLine( ";" );

	f.Write( "#DELAYS:" );
	for( unsigned i=0; i<out.m_Timing.m_StopSegments.size(); i++ )
	{
		const StopSegment &fs = out.m_Timing.m_StopSegments[i];

		if( fs.m_bDelay )
		{
			f.PutLine( ssprintf( "%.3f=%.3f", NoteRowToBeat(fs.m_iStartRow), fs.m_fStopSeconds ) );
			if( i != out.m_Timing.m_StopSegments.size()-1 )
				f.Write( ",\r\n" );
		}
	}
	f.PutLine( ";" );

	f.Write( "#SCROLLS:");
	for( unsigned i=0; i<out.m_Timing.m_ScrollSegments.size(); i++)
	{
		const ScrollSegment &ts = out.m_Timing.m_ScrollSegments[i];
		f.PutLine( ssprintf( "%.3f=%.3f",NoteRowToBeat(ts.m_iStartRow),ts.m_fRatio ));
		if( i != out.m_Timing.m_ScrollSegments.size()-1)
			f.Write( ",\r\n" );
	}
	f.PutLine( ";" );

	f.Write( "#SPEEDS:");
	for( unsigned i=0; i<out.m_Timing.m_SpeedSegments.size(); i++)
	{
		const SpeedSegment &ss = out.m_Timing.m_SpeedSegments[i];
		//LOG->Trace( "WRITING SSC SPEEDS: Row: %d, fRatio: %.3f, fWait: %.3f, Unit: %d", ss.m_iStartRow, ss.m_fRatio, ss.m_fWait, ss.m_iUnit );
		f.PutLine( ssprintf( "%.3f=%.3f=%.3f=%d",NoteRowToBeat(ss.m_iStartRow),ss.m_fRatio, ss.m_fWait, ss.m_iUnit ));
		if( i != out.m_Timing.m_SpeedSegments.size()-1)
			f.Write( ",\r\n" );
	}
	f.PutLine( ";" );

	ASSERT( !out.m_Timing.m_vTimeSignatureSegments.empty() );
	f.Write( "#TIMESIGNATURES:" );
	FOREACH_CONST( TimeSignatureSegment, out.m_Timing.m_vTimeSignatureSegments, iter )
	{
		f.PutLine( ssprintf( "%.3f=%d=%d", NoteRowToBeat(iter->m_iStartRow), iter->m_iNumerator, iter->m_iDenominator ) );
		vector<TimeSignatureSegment>::const_iterator iter2 = iter;
		iter2++;
		if( iter2 != out.m_Timing.m_vTimeSignatureSegments.end() )
			f.Write( ",\r\n" );
	}
	f.PutLine( ";" );

	f.Write( "#NOTESKINCHANGES:" );
	{
		unsigned i = 0;
		for( i=0; i<out.m_Timing.m_NoteSkinSegments.size(); i++ )
		{
			const NoteSkinSegment &fs = out.m_Timing.m_NoteSkinSegments[i];

			f.PutLine( ssprintf( "%.3f=%s", NoteRowToBeat(fs.m_iStartRow), SmEscape(fs.m_sNoteSkin).c_str() ) );
			if( i != out.m_Timing.m_NoteSkinSegments.size()-1 )
				f.Write( ",\r\n" );
		}
		if( i == 0 )
			f.Write(SmEscape(RString("0.000=default")).c_str());
	}
	f.PutLine( ";" );

	f.Write( "#ATTACKS:" );//revisar esto
	for( unsigned j = 0; j < out.m_Attacks.size(); j++ )
	{
		const Attack &a = out.m_Attacks[j];
		f.Write( ssprintf( "TIME=%.2f:LEN=%.2f:MODS=%s",
			a.fStartSecond, a.fSecsRemaining, a.sModifiers.c_str() ) );

		if( j+1 < out.m_Attacks.size() )
			f.Write( ":\r\n" );
		
		f.PutLine( "" );
	}
	f.Write( ";" );
	f.PutLine( "" );

	/*
	//timingtype, modificado por mi!
	FOREACH_ENUM( TimingType, tt )
	{
		if( out.m_UsePiuStops[tt] )
			f.PutLine( "#USEPIUSTOPS"+TimingTypeToString(tt)+":YES;" );

		if( out.m_HoldsNoBody[tt] )
			f.PutLine( "#HOLDSNOBODY"+TimingTypeToString(tt)+":YES;" );

		//si no hay bpms o offset, no escribimos basura!
		//el offset no, por que puede ser 0
		if( out.m_TimingExtra[tt].m_BPMSegments.empty() )
			continue;

		f.PutLine( ssprintf( "#OFFSET"+TimingTypeToString(tt)+":%.3f;", out.m_TimingExtra[tt].m_fBeat0OffsetInSeconds ) );

		f.Write( "#BPMS"+TimingTypeToString(tt)+":" );
		for( unsigned i=0; i<out.m_TimingExtra[tt].m_BPMSegments.size(); i++ )
		{
			const BPMSegment &bs = out.m_TimingExtra[tt].m_BPMSegments[i];

			f.Write( ssprintf( "%.3f=%.3f", NoteRowToBeat(bs.m_iStartRow), bs.GetBPM() ) );
			if( i != out.m_TimingExtra[tt].m_BPMSegments.size()-1 )
				f.Write( "," );
		}
		f.PutLine( ";" );

		f.Write( "#TICKCOUNT"+TimingTypeToString(tt)+":");
		for( unsigned i=0; i<out.m_TimingExtra[tt].m_TickSegments.size(); i++)
		{
			const TickSegment &ts = out.m_TimingExtra[tt].m_TickSegments[i];
			f.Write( ssprintf( "%.3f=%d",NoteRowToBeat(ts.m_fStartBeat),ts.m_iTickcount ));
			if( i != out.m_TimingExtra[tt].m_TickSegments.size()-1)
				f.Write( "," );
		}
		f.PutLine( ";" );

		f.Write( "#MULTIPLIER"+TimingTypeToString(tt)+":");
		for( unsigned i=0; i<out.m_TimingExtra[tt].m_ComboSegments.size(); i++)
		{
			const ComboSegment &cs = out.m_TimingExtra[tt].m_ComboSegments[i];
			f.Write( ssprintf( "%.3f=%d",NoteRowToBeat(cs.m_fStartBeat),cs.m_iComboFactor ));
			if( i != out.m_TimingExtra[tt].m_ComboSegments.size()-1)
				f.Write( "," );
		}
		f.PutLine( ";" );

		f.Write( "#ARROWSPEED"+TimingTypeToString(tt)+":");
		for( unsigned i=0; i<out.m_TimingExtra[tt].m_ArrowSpacingSegments.size(); i++)
		{
			const ArrowSpacingSegment &as = out.m_TimingExtra[tt].m_ArrowSpacingSegments[i];
			f.Write( ssprintf( "%.3f=%.3f=%.3f",NoteRowToBeat(as.m_fStartBeat),as.m_fArrowSpacing, as.m_fShiftFactor ));
			if( i != out.m_TimingExtra[tt].m_ArrowSpacingSegments.size()-1)
				f.Write( "," );
		}
		f.PutLine( ";" );

		f.Write( "#STOPS"+TimingTypeToString(tt)+":" );
		for( unsigned i=0; i<out.m_TimingExtra[tt].m_StopSegments.size(); i++ )
		{
			const StopSegment &fs = out.m_TimingExtra[tt].m_StopSegments[i];

			f.PutLine( ssprintf( "%.3f=%.3f=%d", NoteRowToBeat(fs.m_iStartRow), fs.m_fStopSeconds, fs.m_bDelay ) );
			if( i != out.m_TimingExtra[tt].m_StopSegments.size()-1 )
				f.Write( "," );
		}
		f.PutLine( ";" );

		f.Write( "#TIMESIGNATURES"+TimingTypeToString(tt)+":" );
		FOREACH_CONST( TimeSignatureSegment, out.m_TimingExtra[tt].m_vTimeSignatureSegments, iter )
		{
			f.PutLine( ssprintf( "%.3f=%d=%d", NoteRowToBeat(iter->m_iStartRow), iter->m_iNumerator, iter->m_iDenominator ) );
			vector<TimeSignatureSegment>::const_iterator iter2 = iter;
			iter2++;
			if( iter2 != out.m_TimingExtra[tt].m_vTimeSignatureSegments.end() )
				f.Write( "," );
		}
		f.PutLine( ";" );

		f.Write( "#FAKES"+TimingTypeToString(tt)+":");
		for( unsigned i=0; i<out.m_TimingExtra[tt].m_FakeSegments.size(); i++)
		{
			const FakeSegment &ts = out.m_TimingExtra[tt].m_FakeSegments[i];
			f.Write( ssprintf( "%.3f=%.3f",NoteRowToBeat(ts.m_fStartBeat),ts.m_fLength ));
			if( i != out.m_TimingExtra[tt].m_FakeSegments.size()-1)
				f.Write( "," );
		}
		f.PutLine( ";" );

		{
			unsigned i = 0;
			f.Write( "#NOTESKINCHANGES"+TimingTypeToString(tt)+":" );
			FOREACH_CONST( NoteSkinSegment, out.m_TimingExtra[tt].m_NoteSkinSegments, iter )
			{
				i++;
				f.PutLine( ssprintf( "%.3f=%s", NoteRowToBeat(iter->m_fStartBeat), SmEscape(iter->m_sNoteSkin).c_str() ) );
				vector<NoteSkinSegment>::const_iterator iter2 = iter;
				iter2++;
				if( iter2 != out.m_TimingExtra[tt].m_NoteSkinSegments.end() )
					f.Write( "," );
			}
			if( i == 0 )
				f.PutLine(SmEscape(RString("0.000=default")).c_str());
		}
		f.PutLine( ";" );
	}

	*/

	FOREACH_BackgroundLayer( b )
	{
		if( b==1 )
			f.Write( "#BGCHANGES2:" ); /* test */
		else if( out.GetBackgroundChanges(b).empty() )
			continue;	// skip
		else
			;//f.Write( ssprintf("#BGCHANGES%d:", b+1) );

		FOREACH_CONST( BackgroundChange, out.GetBackgroundChanges(b), bgc )
			f.PutLine( BackgroundChangeToString(*bgc)+"," );

		/* If there's an animation plan at all, add a dummy "-nosongbg-" tag to indicate that
		 * this file doesn't want a song BG entry added at the end.  See SMLoader::TidyUpData.
		 * This tag will be removed on load.  Add it at a very high beat, so it won't cause
		 * problems if loaded in older versions. */
		if( b==0 && !out.GetBackgroundChanges(b).empty() )
			f.PutLine( "99999=-nosongbg-=1.000=0=0=0 // don't automatically add -songbackground-" );
		f.PutLine( ";" );
	}

	if( out.GetForegroundChanges().size() )
	{
		f.Write( "#FGCHANGES:" );
		FOREACH_CONST( BackgroundChange, out.GetForegroundChanges(), bgc )
		{
			f.PutLine( BackgroundChangeToString(*bgc)+"," );
		}
		f.PutLine( ";" );
	}

	/*
	f.Write( "#KEYSOUNDS:" );
	for( unsigned i=0; i<out.m_vsKeysoundFile.size(); i++ )
	{
		f.Write( out.m_vsKeysoundFile[i] );
		if( i != out.m_vsKeysoundFile.size()-1 )
			f.Write( "," );
	}
	f.PutLine( ";" );
	*/
}

static RString JoinLineList( vector<RString> &lines )
{
	for( unsigned i = 0; i < lines.size(); ++i )
		TrimRight( lines[i] );

	/* Skip leading blanks. */
	unsigned j = 0;
	while( j < lines.size() && lines.size() == 0 )
		++j;

	return join( "\r\n", lines.begin()+j, lines.end() );
}

static RString GetNXANotesTag( const Song &song, const Steps &in, bool bSavingCache )
{
	vector<RString> lines;

	lines.push_back( "" );
	// Escape to prevent some clown from making a comment of "\r\n;"
	lines.push_back( ssprintf("//---------------%s - %s----------------",
		GameManager::StepsTypeToString(in.m_StepsType).c_str(), SmEscape(in.GetDescription()).c_str()) );
	WriteTimingTags( lines, in.m_StepsTiming, in );
	lines.push_back( song.m_vsKeysoundFile.empty() ? "#NOTES:" : "#NOTES2:" );
	lines.push_back( ssprintf( "     %s:", GameManager::StepsTypeToString(in.m_StepsType).c_str() ) );
	lines.push_back( ssprintf( "     %s:", SmEscape(in.GetDescription()).c_str() ) );
	lines.push_back( ssprintf( "     %s:", DifficultyToString(in.GetDifficulty()).c_str() ) );
	lines.push_back( ssprintf( "     %d:", in.GetMeter() ) );
	
	vector<RString> asRadarValues;
	FOREACH_PlayerNumber( pn )
	{
		const RadarValues &rv = in.GetRadarValues( pn );
		FOREACH_ENUM( RadarCategory, rc )
			asRadarValues.push_back( ssprintf("%.3f", rv[rc]) );
	}
	lines.push_back( ssprintf( "     %s:", join(",",asRadarValues).c_str() ) );

	RString sNoteData;
	in.GetSMNoteData( sNoteData );

	split( sNoteData, "\n", lines, true );
	lines.push_back( ";" );

	return JoinLineList( lines );
}

bool NotesWriterNXA::Write( RString sPath, const Song &out, const vector<Steps*>& vpStepsToSave, bool bSavingCache )
{
	/* Flush dir cache when writing steps, so the old size isn't cached. */
	FILEMAN->FlushDirCache( Dirname(sPath) );

	int flags = RageFile::WRITE;

	/* If we're not saving cache, we're saving real data, so enable SLOW_FLUSH
	 * to prevent data loss.  If we're saving cache, this will slow things down
	 * too much. */
	if( !bSavingCache )
		flags |= RageFile::SLOW_FLUSH;

	RageFile f;
	if( !f.Open( sPath, flags ) )
	{
		LOG->UserLog( "Song file", sPath, "couldn't be opened for writing: %s", f.GetError().c_str() );
		return false;
	}

	WriteGlobalTags( f, out );
	if( bSavingCache )
	{
		f.PutLine( ssprintf( "// cache tags:" ) );
		f.PutLine( ssprintf( "#FIRSTBEAT:%.3f;", out.m_fFirstBeat ) );
		f.PutLine( ssprintf( "#LASTBEAT:%.3f;", out.m_fLastBeat ) );
		//cambiamos la extension explicitamente
		f.PutLine( ssprintf( "#SONGFILENAME:%s;", SetExtension(out.GetSongFilePath(),"snxa").c_str() ) );
		f.PutLine( ssprintf( "#HASMUSIC:%i;", out.m_bHasMusic ) );
		f.PutLine( ssprintf( "#HASBANNER:%i;", out.m_bHasBanner ) );
		f.PutLine( ssprintf( "#MUSICLENGTH:%.3f;", out.m_fMusicLengthSeconds ) );
		f.PutLine( ssprintf( "// end cache tags" ) );
	}

	//
	// Save specified Steps to this file
	//
	FOREACH_CONST( Steps*, vpStepsToSave, s ) 
	{
		const Steps* pSteps = *s;
		RString sTag = GetNXANotesTag( out, *pSteps, bSavingCache );
		f.PutLine( sTag );
	}
	if( f.Flush() == -1 )
		return false;

	return true;
}

void NotesWriterNXA::GetEditFileContents( const Song *pSong, const Steps *pSteps, RString &sOut )
{
	sOut = "";
	RString sDir = pSong->GetSongDir();
	
	/* "Songs/foo/bar"; strip off "Songs/". */
	vector<RString> asParts;
	split( sDir, "/", asParts );
	if( asParts.size() )
		sDir = join( "/", asParts.begin()+1, asParts.end() );
	sOut += ssprintf( "#SONG:%s;\r\n", sDir.c_str() );
	sOut += GetNXANotesTag( *pSong, *pSteps, false );
}

RString NotesWriterNXA::GetEditFileName( const Song *pSong, const Steps *pSteps )
{
	/* Try to make a unique name.  This isn't guaranteed.  Edit descriptions are
	 * case-sensitive, filenames on disk are usually not, and we decimate certain
	 * characters for FAT filesystems. */
	RString sFile = pSong->GetTranslitFullTitle() + " - " + pSteps->GetDescription();

	// HACK:
	if( pSteps->m_StepsType == STEPS_TYPE_DANCE_DOUBLE )
		sFile += " (doubles)";
	
	sFile += ".edit";

	MakeValidFilename( sFile );
	return sFile;
}

static LocalizedString DESTINATION_ALREADY_EXISTS	("NotesWriterSM", "Error renaming file.  Destination file '%s' already exists.");
static LocalizedString ERROR_WRITING_FILE		("NotesWriterSM", "Error writing file '%s'.");
bool NotesWriterNXA::WriteEditFileToMachine( const Song *pSong, Steps *pSteps, RString &sErrorOut )
{
	RString sDir = PROFILEMAN->GetProfileDir( ProfileSlot_Machine ) + EDIT_STEPS_SUBDIR;

	RString sPath = sDir + GetEditFileName(pSong,pSteps);

	/* Flush dir cache when writing steps, so the old size isn't cached. */
	FILEMAN->FlushDirCache( Dirname(sPath) );

	// Check to make sure that we're not clobering an existing file before opening.
	bool bFileNameChanging = 
		pSteps->GetSavedToDisk()  && 
		pSteps->GetFilename() != sPath;
	if( bFileNameChanging  &&  DoesFileExist(sPath) )
	{
		sErrorOut = ssprintf( DESTINATION_ALREADY_EXISTS.GetValue(), sPath.c_str() );
		return false;
	}

	RageFile f;
	if( !f.Open(sPath, RageFile::WRITE | RageFile::SLOW_FLUSH) )
	{
		sErrorOut = ssprintf( ERROR_WRITING_FILE.GetValue(), sPath.c_str() );
		return false;
	}

	RString sTag;
	GetEditFileContents( pSong, pSteps, sTag );
	if( f.PutLine(sTag) == -1 || f.Flush() == -1 )
	{
		sErrorOut = ssprintf( ERROR_WRITING_FILE.GetValue(), sPath.c_str() );
		return false;
	}

	/* If the file name of the edit has changed since the last save, then delete the old
	 * file after saving the new one.  If we delete it first, then we'll lose data on error. */

	if( bFileNameChanging )
		FILEMAN->Remove( pSteps->GetFilename() );
	pSteps->SetFilename( sPath );

	/* Flush dir cache or else the new file won't be seen. */
	FILEMAN->FlushDirCache( Dirname(sPath) );

	return true;
}

/*
 * (c) 2001-2004 Chris Danford, Glenn Maynard
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
