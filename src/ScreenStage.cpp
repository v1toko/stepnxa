#include "global.h"
#include "ScreenStage.h"
#include "ActorUtil.h"
#include "ScreenManager.h"
#include "RageLog.h"
#include "GameConstantsAndTypes.h"
#include "AnnouncerManager.h"
#include "GameState.h"
#include "GameSoundManager.h"
#include "ThemeManager.h"
#include "ThemeMetric.h"
#include "ProfileManager.h"
#include "Mission.h"
#include "LuaManager.h"
#include "MessageManager.h"

#define MINIMUM_DELAY			THEME->GetMetricF(m_sName,"MinimumDelay")

ThemeMetric<RString> VelocityMods;
ThemeMetric<RString> LifeMods;
ThemeMetric<RString> UnableMods;
ThemeMetric<RString> LotteryMods;

ThemeMetric<int> VelocityCostMod;
ThemeMetric<int> LifeCostMod;
ThemeMetric<int> UnableCostMod;
ThemeMetric<int> LotteryCostMod;

AutoScreenMessage( SM_PrepScreen )

REGISTER_SCREEN_CLASS( ScreenStage );

void ScreenStage::Init()
{
	Screen::Init();

	m_MissionTimer = NULL;

	ALLOW_BACK.Load( m_sName, "AllowBack" );
	MISSION_SECONDS.Load( m_sName, "MissionSecondsToStart" );

	SOUND->StopMusic();

	m_iMileageCostTotal = 0;

	m_bIsMissionMode = GAMESTATE->IsMissionMode() || GAMESTATE->IsWorldTourMode();

	m_pProfile = PROFILEMAN->IsPersistentProfile( GAMESTATE->GetFirstHumanPlayer() ) ? PROFILEMAN->GetProfile( GAMESTATE->GetFirstHumanPlayer() ) : PROFILEMAN->GetMachineProfile();

	m_sprOverlay.Load( THEME->GetPathB(m_sName,"overlay") );
	m_sprOverlay->SetName( "Overlay" );
	m_sprOverlay->SetDrawOrder( DRAW_ORDER_OVERLAY );
	m_sprOverlay->PlayCommand("On");
	this->AddChild( m_sprOverlay );

	m_In.Load( THEME->GetPathB(m_sName,"in") );
	m_In.StartTransitioning();
	m_In.SetDrawOrder( DRAW_ORDER_TRANSITIONS );
	m_In.PlayCommand("On");
	this->AddChild( &m_In );

	m_Out.Load( THEME->GetPathB(m_sName,"out") );
	m_Out.SetDrawOrder( DRAW_ORDER_TRANSITIONS );
	m_Out.PlayCommand("On");
	this->AddChild( &m_Out );

	m_Cancel.Load( THEME->GetPathB(m_sName,"cancel") );
	m_Cancel.SetDrawOrder( DRAW_ORDER_TRANSITIONS );
	m_Cancel.PlayCommand("On");
	this->AddChild( &m_Cancel );

	m_bSelected = false;
	m_bReady = false;
	m_bMissionNotAllow = false;
	m_bIsUsingProfile = m_pProfile->IsMachine() ? false : true;

	//si dice "NO" not allow = true y no permitimos mostrar las arrowmods
	if( m_bIsMissionMode && !GAMESTATE->IsWorldTourMode() )
		m_bMissionNotAllow = GAMESTATE->m_pCurMission->GetIfAllowUseArrowMods();

	//nos aseguramos que el millage no sea < 0
	if( m_pProfile->m_iTotalMileage < 0 )
		m_pProfile->m_iTotalMileage = 0;

	//en las metrics se cargan los commands, pero aqui (Screen) se controla si se ven o no!
	if( m_bIsMissionMode )
	{

		if( !GAMESTATE->IsWorldTourMode() )
		{
			{//los mods de arriba a la izquierda de velocidad
				vector<RString> vsMods;
				VelocityMods.Load( "ArrowMods", "VelocityMods" );
				VelocityCostMod.Load( "ArrowMods", "VelocityModCost" );
				split( VelocityMods.GetValue(), ",", vsMods );
				m_ArrowMods.SetName( "VelocityMods" );
				m_ArrowMods.Iniciar( "Velocity", vsMods, VelocityCostMod.GetValue(), m_pProfile );
				LOAD_ALL_COMMANDS_AND_SET_XY_AND_ON_COMMAND( m_ArrowMods );
				this->AddChild( &m_ArrowMods );
				m_ArrowMods.Actualizar( GAMESTATE->GetFirstHumanPlayer() );
			}

			{//arriba a la derecha, los mods de life
				vector<RString> vsMods;
				LifeMods.Load( "ArrowMods", "LifeMods" );
				LifeCostMod.Load( "ArrowMods", "LifeModCost" );
				split( LifeMods.GetValue(), ",", vsMods );
				m_LifeMods.SetName( "LifeMods" );
				m_LifeMods.Iniciar( "Life", vsMods, LifeCostMod.GetValue(), m_pProfile );
				LOAD_ALL_COMMANDS_AND_SET_XY_AND_ON_COMMAND( m_LifeMods );
				this->AddChild( &m_LifeMods );
				m_LifeMods.Actualizar( GAMESTATE->GetFirstHumanPlayer() );
			}

			{//abajo a la izquierda, mods que quitan todo!
				vector<RString> vsMods;
				UnableMods.Load( "ArrowMods", "UnableMods" );
				UnableCostMod.Load( "ArrowMods", "UnableModCost" );
				split( UnableMods.GetValue(), ",", vsMods );
				m_UnableMods.SetName( "UnableMods" );
				m_UnableMods.Iniciar( "Generic", vsMods, UnableCostMod.GetValue(), m_pProfile );
				LOAD_ALL_COMMANDS_AND_SET_XY_AND_ON_COMMAND( m_UnableMods );
				this->AddChild( &m_UnableMods );
				m_UnableMods.Actualizar( GAMESTATE->GetFirstHumanPlayer() );
			}

			{//abajo a la izquierda, mods que quitan todo!
				vector<RString> vsMods;
				LotteryMods.Load( "ArrowMods", "LotteryMods" );
				//LotteryCostMod.Load( "ArrowMods", "LotteryModCost" );
				split( LotteryMods.GetValue(), ",", vsMods );
				m_LotteryMods.SetName( "LotteryMods" );
				//aqui manda el costo designado en el TAG #MILEAGE, en el msf de la mision
				m_LotteryMods.Iniciar( "Lottery", vsMods, GAMESTATE->m_pCurMission->m_MissionGoals.iGivenMileage, m_pProfile );
				LOAD_ALL_COMMANDS_AND_SET_XY_AND_ON_COMMAND( m_LotteryMods );
				this->AddChild( &m_LotteryMods );
				m_LotteryMods.Actualizar( GAMESTATE->GetFirstHumanPlayer() );
			}
		}
		//nunca empezamos usando lottery
		GAMESTATE->m_bUsedLottery = false;

		//if( !m_bIsUsingProfile || m_bMissionNotAllow )//si no estamos usando una profile, no mostramos las arrowmods!
		{
			m_ArrowMods.SetVisible( false );
			m_LifeMods.SetVisible( false );
			m_UnableMods.SetVisible( false );
			m_LotteryMods.SetVisible( false );
		}

		CHECKPOINT;
		if( MISSION_SECONDS > 0 )
		{
			m_MissionTimer = new MenuTimer;
			m_MissionTimer->SetName( "MissionTimer" );
			m_MissionTimer->Load();
			m_MissionTimer->SetDrawOrder( DRAW_ORDER_AFTER_EVERYTHING );
			m_MissionTimer->Start();
			m_MissionTimer->SetSeconds( MISSION_SECONDS );
			LOAD_ALL_COMMANDS_AND_SET_XY_AND_ON_COMMAND( m_MissionTimer );
			this->AddChild( m_MissionTimer );
		}
		CHECKPOINT;

		soundConfirm.Load( THEME->GetPathS( "ScreenStage", "start" ) );
	}

	SOUND->PlayOnceFromDir( ANNOUNCER->GetPathTo("stage "+StageToString(GAMESTATE->GetCurrentStage())) );

	this->SortByDrawOrder();
}

void ScreenStage::Input( const InputEventPlus &input )
{
	//si no es el player correspondiente no hace nada!
	if( input.pn != GAMESTATE->GetFirstHumanPlayer() )
		return;

	if( !m_bIsMissionMode )
		return;

	if( input.type != IET_FIRST_PRESS )
		return;

	if( m_ArrowMods.IsMoving() || m_LifeMods.IsMoving() || m_UnableMods.IsMoving() || m_LotteryMods.IsMoving() )
		return;

	if( m_bReady )
		return;

	//if( m_bMissionNotAllow )
	//	return;

	//no podemos cambiar cosas si los segundo que quedan son < 2
	if( m_MissionTimer->GetSeconds() < 2 )
		return;

	if( IsArrowModsVisible() )
	{
		if( input.GameI.button == PUMP_BUTTON_UPLEFT )
		{
			//if( m_LotteryMods.IsUsed() /*|| m_UnableMods.IsUsed()*/ )
			//	return;

			m_ArrowMods.ChangeNextIndex();
			//m_ArrowMods.Rotate();
			m_ArrowMods.InformarMileage( GetMileageCost() );
			m_ArrowMods.Actualizar( input.pn );

			{
				Message msg("VelocityChanged");
				msg.SetParam( "Index", m_ArrowMods.GetIndex() );
				MESSAGEMAN->Broadcast( msg );
			}
		}
		else if( input.GameI.button == PUMP_BUTTON_UPRIGHT )
		{
			//if( m_LotteryMods.IsUsed() )
			//	return;

			m_LifeMods.ChangeNextIndex();
			//m_LifeMods.Rotate();
			m_LifeMods.InformarMileage( GetMileageCost() );
			m_LifeMods.Actualizar( input.pn );

			{
				Message msg("LifeChanged");
				msg.SetParam( "Index", m_LifeMods.GetIndex() );
				MESSAGEMAN->Broadcast( msg );
			}
		}
		else if( input.GameI.button == PUMP_BUTTON_DOWNLEFT )
		{
			//if( m_LotteryMods.IsUsed() )
			//	return;

			m_UnableMods.ChangeNextIndex();
			//m_UnableMods.Rotate();
			m_UnableMods.InformarMileage( GetMileageCost() );
			m_UnableMods.Actualizar( input.pn );//HACK: actualizamos 2 veces!

			{
				Message msg("RemoveModsChanged");
				msg.SetParam( "Index", m_UnableMods.GetIndex() );
				MESSAGEMAN->Broadcast( msg );
			}
		}
		else if( input.GameI.button == PUMP_BUTTON_DOWNRIGHT )
		{
			m_LotteryMods.ChangeNextIndex();
			//m_LotteryMods.Rotate();
			m_LotteryMods.InformarMileage( GetMileageCost() );
			m_LotteryMods.Actualizar( input.pn );//HACK: actualizamos 2 veces!

			{
				Message msg("LotteryChanged");
				msg.SetParam( "Index", m_LotteryMods.GetIndex() );
				MESSAGEMAN->Broadcast( msg );
			}
		}
	}

	Screen::Input( input );
}

void ScreenStage::HandleScreenMessage( const ScreenMessage SM )
{
	if( SM == SM_PrepScreen )
	{
		RageTimer length;
		//SCREENMAN->PrepareScreen( GetNextScreenName() );
		float fScreenLoadSeconds = length.GetDeltaTime();

		//solo lo hace en modo mission!
		if( m_bIsMissionMode )
			m_MissionTimer->Pause();

		/* The screen load took fScreenLoadSeconds.  Move on to the next screen after
		 * no less than MINIMUM_DELAY seconds. */
		this->PostScreenMessage( SM_BeginFadingOut, max( 0, MINIMUM_DELAY-fScreenLoadSeconds) );
		return;
	}
	else if( SM == SM_BeginFadingOut )
	{
		if( m_sprOverlay->GetTweenTimeLeft() )
			return;

		/* Clear any other SM_BeginFadingOut messages. */
		this->ClearMessageQueue( SM_BeginFadingOut );

		if( m_LotteryMods.IsUsed() )
			GAMESTATE->m_bUsedLottery = true;

		//aqui restamos el mileage, y screengamplay stagefinish sumamos en base a lottery o no!
		if( m_bIsUsingProfile && m_bIsMissionMode )
		{
			if( GAMESTATE->m_bUsedLottery )
			{
				m_pProfile->m_iTotalMileage -= (GetMileageCost() - GAMESTATE->m_pCurMission->m_MissionGoals.iGivenMileage);
				max( m_pProfile->m_iTotalMileage, 0 );
				//GAMESTATE->DecrementMileageForPlayer( GAMESTATE->GetFirstHumanPlayer(), (GetMileageCost()-GAMESTATE->m_pCurMission->m_MissionGoals.iGivenMileage) );
			}
			else
			{
				m_pProfile->m_iTotalMileage -= GetMileageCost();
				max( m_pProfile->m_iTotalMileage, 0 );
				//GAMESTATE->DecrementMileageForPlayer( GAMESTATE->GetFirstHumanPlayer(), GetMileageCost() );
			}
		}	

		if( m_bIsMissionMode && !GAMESTATE->IsWorldTourMode() )//guardamos la mission!
		{
			m_pProfile->m_sLastMission = GAMESTATE->m_pCurMission->GetMissionName();
			m_pProfile->m_sLastSector = GAMESTATE->m_pCurMission->GetMissionSector();
		}

		m_Out.StartTransitioning();
		m_sprOverlay->PlayCommand( "Off" );
		this->PostScreenMessage( SM_GoToNextScreen, this->GetTweenTimeLeft() );
		return;
	}
	else if( SM == SM_MenuTimer )
	{ 
		this->PostScreenMessage( SM_PrepScreen, 1.0f );
		return;
	}
	
	Screen::HandleScreenMessage( SM );
}

void ScreenStage::Update( float fDeltaTime )
{
	if( !m_bIsMissionMode )
	{
		if( this->IsFirstUpdate() )
		{
			/* Prep the new screen once m_In is complete. */ 	 
			this->PostScreenMessage( SM_PrepScreen, m_sprOverlay->GetTweenTimeLeft() );
		}
	}

	//if( this->IsFirstUpdate() && GAMESTATE->IsMissionMode() )
	//{
	//	ON_COMMAND( m_Confirm );
	//	ON_COMMAND( m_ConfirmMessage );
	//}

	Screen::Update( fDeltaTime );
}

void ScreenStage::MenuStart( const InputEventPlus &input )
{
	if( !GAMESTATE->IsPlayerEnabled( input.pn ) )
		return;

	if( input.type != IET_FIRST_PRESS )
		return;

	if( m_bReady )//salta, por que o sino hace mucho ruido!
		return;

	//actua solo en mode mission
	if( m_bIsMissionMode )
	{
		if( !m_bSelected )
		{
			MESSAGEMAN->Broadcast( "CenterWasPressedOnce" );
			m_bSelected = true;
			soundConfirm.Play();
		}
		else
		{
			//OFF_COMMAND( m_Confirm );
			//OFF_COMMAND( m_ConfirmMessage );
			/* Prep the new screen once m_In is complete. */ 
			MESSAGEMAN->Broadcast( "CenterWasPressedTwice" );
			this->PostScreenMessage( SM_PrepScreen, 1.0f );
			soundConfirm.Play();
			m_bReady = true;
		}
	}
}

void ScreenStage::MenuBack( const InputEventPlus &input )
{
	if( m_In.IsTransitioning() || m_Out.IsTransitioning() || m_Cancel.IsTransitioning() )
		return;

	if( !ALLOW_BACK )
		return;
	
	this->ClearMessageQueue();
	GAMESTATE->CancelStage();
	m_sNextScreen = GetPrevScreen();
	m_Cancel.StartTransitioning( SM_GoToPrevScreen );
	//m_Confirm.SetVisible( false );
	//m_ConfirmMessage.SetVisible( false );

	/* If a Back is buffered while we're prepping the screen (very common), we'll
	 * get it right after the prep finishes.  However, the next update will contain
	 * the time spent prepping the screen.  Zero the next delta, so the update is
	 * seen. */
	SCREENMAN->ZeroNextUpdate();
}

int ScreenStage::GetMileageCost()
{	//todos quitan millage!, excepto el lottery
	int iTotalCost = m_ArrowMods.GetMileageCost() + m_LifeMods.GetMileageCost() + 
		m_UnableMods.GetMileageCost() +	m_LotteryMods.GetMileageCost();

	//LOG->Trace( "CosteTotalDeMileage: %d", iTotalCost );
	return iTotalCost;
}

ScreenStage::~ScreenStage()
{
	if( m_MissionTimer != NULL )
		SAFE_DELETE( m_MissionTimer );
}

// lua start
#include "LuaBinding.h"

class LunaScreenStage: public Luna<ScreenStage>
{
public:
	static int GetMileageCost( T* p, lua_State *L ) 
	{ 
		LUA->YieldLua();
		lua_pushnumber( L, p->GetMileageCost() ); 
		LUA->UnyieldLua();
		return 1; 
	}
	static int IsArrowModsVisible( T* p, lua_State *L ) 
	{ 
		LUA->YieldLua();
		lua_pushboolean( L, p->IsArrowModsVisible() ); 
		LUA->UnyieldLua();
		return 1;
	}

	LunaScreenStage()
	{
  		ADD_METHOD( GetMileageCost );
		//ADD_METHOD( IsArrowModsVisible );
	}
};

LUA_REGISTER_DERIVED_CLASS( ScreenStage, Screen )

/*
 * (c) 2001-2004 Chris Danford
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
