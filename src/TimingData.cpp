#include "global.h"
#include "TimingData.h"
#include "PrefsManager.h"
#include "RageUtil.h"
#include "RageLog.h"
#include "NoteTypes.h"
#include "Foreach.h"
#include <float.h>
#include "GameState.h"


TimingData::TimingData()
{
	m_fBeat0OffsetInSeconds = 0;
}

void TimingData::GetActualBPM( float &fMinBPMOut, float &fMaxBPMOut ) const
{
	fMinBPMOut = FLT_MAX;
	fMaxBPMOut = 0;
	FOREACH_CONST( BPMSegment, m_BPMSegments, seg )
	{
		const float fBPM = seg->GetBPM();
		fMaxBPMOut = max( fBPM, fMaxBPMOut );
		fMinBPMOut = min( fBPM, fMinBPMOut );
	}
}


void TimingData::AddBPMSegment( const BPMSegment &seg )
{
	m_BPMSegments.insert( upper_bound(m_BPMSegments.begin(), m_BPMSegments.end(), seg), seg );
}

void TimingData::AddStopSegment( const StopSegment &seg )
{
	m_StopSegments.insert( upper_bound(m_StopSegments.begin(), m_StopSegments.end(), seg), seg );
}

void TimingData::AddTimeSignatureSegment( const TimeSignatureSegment &seg )
{
	m_vTimeSignatureSegments.insert( upper_bound(m_vTimeSignatureSegments.begin(), m_vTimeSignatureSegments.end(), seg), seg );
}

/* Change an existing BPM segment, merge identical segments together or insert a new one. */
void TimingData::SetBPMAtRow( int iNoteRow, float fBPM )
{
	float fBPS = fBPM / 60.0f;
	unsigned i;
	for( i=0; i<m_BPMSegments.size(); i++ )
		if( m_BPMSegments[i].m_iStartRow >= iNoteRow )
			break;

	if( i == m_BPMSegments.size() || m_BPMSegments[i].m_iStartRow != iNoteRow )
	{
		// There is no BPMSegment at the specified beat.  If the BPM being set differs
		// from the last BPMSegment's BPM, create a new BPMSegment.
		if( i == 0 || fabsf(m_BPMSegments[i-1].m_fBPS - fBPS) > 1e-5f )
			AddBPMSegment( BPMSegment(iNoteRow, fBPM) );
	}
	else	// BPMSegment being modified is m_BPMSegments[i]
	{
		if( i > 0  &&  fabsf(m_BPMSegments[i-1].m_fBPS - fBPS) < 1e-5f )
			m_BPMSegments.erase( m_BPMSegments.begin()+i, m_BPMSegments.begin()+i+1 );
		else
			m_BPMSegments[i].m_fBPS = fBPS;
	}
}
/* Change an existing TS segment, merge identical segments together or insert a new one. */
void TimingData::SetTimeSignatureAtBeat( float fBeat, int iNumerator, int iDenominator  )
{
	int iNoteRow = BeatToNoteRow( fBeat );
	unsigned i;
	for( i=0; i<m_vTimeSignatureSegments.size(); i++ )
		if( m_vTimeSignatureSegments[i].m_iStartRow >= iNoteRow )
			break;

	if( i == m_vTimeSignatureSegments.size() || m_vTimeSignatureSegments[i].m_iStartRow != iNoteRow )
	{
		// There is no BPMSegment at the specified beat.  If the BPM being set differs
		// from the last BPMSegment's BPM, create a new BPMSegment.
		if( i == 0 || fabsf( m_vTimeSignatureSegments[i-1].m_iNumerator - iNumerator > 1e-5f ) )
			AddTimeSignatureSegment( TimeSignatureSegment(iNoteRow, iNumerator, iDenominator) );
	}
	else	// BPMSegment being modified is m_BPMSegments[i]
	{
		if( i > 0  &&  m_vTimeSignatureSegments[i] == m_vTimeSignatureSegments[i-1] )
			m_vTimeSignatureSegments.erase( m_vTimeSignatureSegments.begin()+i, m_vTimeSignatureSegments.begin()+i+1 );
		else
		{
			m_vTimeSignatureSegments[i].m_iNumerator = iNumerator;
			m_vTimeSignatureSegments[i].m_iDenominator = iDenominator;
		}
	}
}

void TimingData::SetStopAtRow( int iRow, float fSeconds, bool bDelay )
{
	unsigned i;
	for( i=0; i<m_StopSegments.size(); i++ )
		if( m_StopSegments[i].m_iStartRow == iRow )
			break;

	if( i == m_StopSegments.size() )	// there is no BPMSegment at the current beat
	{
		// create a new StopSegment
		if( fSeconds > 0 )
			AddStopSegment( StopSegment(iRow, fSeconds, bDelay) );
	}
	else	// StopSegment being modified is m_StopSegments[i]
	{
		if( fSeconds > 0 )
			m_StopSegments[i].m_fStopSeconds = fSeconds;
		else
			m_StopSegments.erase( m_StopSegments.begin()+i, m_StopSegments.begin()+i+1 );
	}
}

float TimingData::GetStopAtRow( int iNoteRow, bool bDelay )
{
	for( unsigned i=0; i<m_StopSegments.size(); i++ )
	{
		if( m_StopSegments[i].m_iStartRow == iNoteRow && m_StopSegments[i].m_bDelay == bDelay )
		{
			return m_StopSegments[i].m_fStopSeconds;
		}
	}

	return 0;
}

/* Multiply the BPM in the range [fStartBeat,fEndBeat) by fFactor. */
void TimingData::MultiplyBPMInBeatRange( int iStartIndex, int iEndIndex, float fFactor )
{
	/* Change all other BPM segments in this range. */
	for( unsigned i=0; i<m_BPMSegments.size(); i++ )
	{
		const int iStartIndexThisSegment = m_BPMSegments[i].m_iStartRow;
		const bool bIsLastBPMSegment = i==m_BPMSegments.size()-1;
		const int iStartIndexNextSegment = bIsLastBPMSegment ? INT_MAX : m_BPMSegments[i+1].m_iStartRow;

		if( iStartIndexThisSegment <= iStartIndex && iStartIndexNextSegment <= iStartIndex )
			continue;

		/* If this BPM segment crosses the beginning of the range, split it into two. */
		if( iStartIndexThisSegment < iStartIndex && iStartIndexNextSegment > iStartIndex )
		{
			BPMSegment b = m_BPMSegments[i];
			b.m_iStartRow = iStartIndexNextSegment;
			m_BPMSegments.insert( m_BPMSegments.begin()+i+1, b );

			/* Don't apply the BPM change to the first half of the segment we just split,
			 * since it lies outside the range. */
			continue;
		}

		/* If this BPM segment crosses the end of the range, split it into two. */
		if( iStartIndexThisSegment < iEndIndex && iStartIndexNextSegment > iEndIndex )
		{
			BPMSegment b = m_BPMSegments[i];
			b.m_iStartRow = iEndIndex;
			m_BPMSegments.insert( m_BPMSegments.begin()+i+1, b );
		}
		else if( iStartIndexNextSegment > iEndIndex )
			continue;

		m_BPMSegments[i].m_fBPS = m_BPMSegments[i].m_fBPS * fFactor;
	}
}

float TimingData::GetBPMAtBeat( float fBeat ) const
{
	int iIndex = BeatToNoteRow( fBeat );
	unsigned i;
	for( i=0; i<m_BPMSegments.size()-1; i++ )
		if( m_BPMSegments[i+1].m_iStartRow > iIndex )
			break;
	return m_BPMSegments[i].GetBPM();
}

int TimingData::GetBPMSegmentIndexAtBeat( float fBeat )
{
	int iIndex = BeatToNoteRow( fBeat );
	int i;
	for( i=0; i<(int)(m_BPMSegments.size())-1; i++ )
		if( m_BPMSegments[i+1].m_iStartRow > iIndex )
			break;
	return i;
}

BPMSegment& TimingData::GetBPMSegmentAtBeat( float fBeat )
{
	static BPMSegment empty;
	if( m_BPMSegments.empty() )
		return empty;
	
	int i = GetBPMSegmentIndexAtBeat( fBeat );
	return m_BPMSegments[i];
}

TimeSignatureSegment& TimingData::GetTimeSignatureSegmentAtBeat( float fBeat )
{
	int iIndex = BeatToNoteRow( fBeat );
	unsigned i;
	for( i=0; i<m_vTimeSignatureSegments.size()-1; i++ )
		if( m_vTimeSignatureSegments[i+1].m_iStartRow > iIndex )
			break;
	return m_vTimeSignatureSegments[i];
}

//MODIFICACION IMPORTANTE
//DE AQUI-------------------

static int CompareTickSegments(const TickSegment &seg1, const TickSegment &seg2)
{
	return seg1.m_fStartBeat < seg2.m_fStartBeat;
}

void SortTickSegmentsArray( vector<TickSegment> &arrayTickSegments )
{
	sort( arrayTickSegments.begin(), arrayTickSegments.end(), CompareTickSegments );
}

void TimingData::AddTickSegment( const TickSegment &seg )
{
	m_TickSegments.insert( upper_bound(m_TickSegments.begin(), m_TickSegments.end(), seg), seg );
}

void TimingData::SetTickcountAtBeat( float fBeat, int iTickcount )
{
	unsigned i;
	for( i=0; i<m_TickSegments.size(); i++ )
		//if( m_TickSegments[i].m_fStartBeat >= (fBeat-0.0001) && m_TickSegments[i].m_fStartBeat <= (fBeat+0.0001) )
		if( m_TickSegments[i].m_fStartBeat >= BeatToNoteRow( fBeat ) )
			break;

	if( i == m_TickSegments.size() || m_TickSegments[i].m_fStartBeat != BeatToNoteRow( fBeat ) )	// there is no TickSegment at the current beat
	{
		// create a new TickSegment
		if( i == 0 || abs(m_TickSegments[i-1].m_iTickcount - iTickcount) > 1e-5f )
			AddTickSegment( TickSegment(BeatToNoteRow( fBeat ), iTickcount) );
	}
	else	// TickSegment being modified is m_TickSegments[i]
	{
		if( i > 0  &&  abs(m_TickSegments[i-1].m_iTickcount - iTickcount) < 1e-5f )
			m_TickSegments.erase( m_TickSegments.begin()+i, m_TickSegments.begin()+i+1);
		else
			m_TickSegments[i].m_iTickcount = iTickcount;
	}
}

int TimingData::GetTickcountAtBeat( float fBeat ) const
{
	int iIndex = BeatToNoteRow( fBeat );
	unsigned i;
	for( i=0; i<m_TickSegments.size()-1; i++ )
		if( m_TickSegments[i+1].m_fStartBeat > iIndex )
			break;
	return m_TickSegments[i].m_iTickcount;
}

float TimingData::GetTickSegmentStart( int iSegment ) const
{
	return m_TickSegments[iSegment].m_fStartBeat;
}

TickSegment& TimingData::GetTickSegmentAtBeat( float fBeat )
{
	static TickSegment empty;
	if( m_TickSegments.empty() )
		return empty;
	int i = GetTickSegmentIndexAtBeat( fBeat );
	return m_TickSegments[i];
}

int TimingData::GetTickSegmentIndexAtBeat( float fBeat )
{
	int iIndex = BeatToNoteRow( fBeat );
	int i;
	for( i=0; i<(int)(m_TickSegments.size())-1; i++ )
		if( m_TickSegments[i+1].m_fStartBeat > iIndex )
			break;
	return i;
}

//HASTA AQUI----------------
//TICKCOUNT STUFF

static int CompareNoteSkinSegments(const NoteSkinSegment &seg1, const NoteSkinSegment &seg2)
{
	return seg1.m_iStartRow < seg2.m_iStartRow;
}

void SortNoteSkinSegmentsArray( vector<NoteSkinSegment> &arrayNoteSkinSegments )
{
	sort( arrayNoteSkinSegments.begin(), arrayNoteSkinSegments.end(), CompareNoteSkinSegments );
}

void TimingData::AddNoteSkinSegment( const NoteSkinSegment &seg )
{
	m_NoteSkinSegments.insert( upper_bound(m_NoteSkinSegments.begin(), m_NoteSkinSegments.end(), seg), seg );
}

void TimingData::SetNoteSkinAtBeat( float fBeat, RString sNoteSkin )
{
	//unsigned i;
	//for( i=0; i<m_NoteSkinSegments.size(); i++ )
	//	if( m_NoteSkinSegments[i].m_fStartBeat >= BeatToNoteRow( fBeat ) )
	//		break;

	//if( i == m_NoteSkinSegments.size() || m_NoteSkinSegments[i].m_fStartBeat != BeatToNoteRow( fBeat ) )// there is no TickSegment at the current beat
	//{
	//	// create a new NoteSkinSegment
	//	//if( i == 0 || m_NoteSkinSegments[i].m_sNoteSkin != sNoteSkin )
	//	AddNoteSkinSegment( NoteSkinSegment(BeatToNoteRow( fBeat ), sNoteSkin) );
	//}
	//else	// NoteSkinSegment being modified is m_NoteSkinSegments[i]
	//{
	//	if( i > 0 && m_NoteSkinSegments[i] == m_NoteSkinSegments[i-1] )
	//		m_NoteSkinSegments.erase( m_NoteSkinSegments.begin()+i, m_NoteSkinSegments.begin()+i+1);
	//	else
	//		m_NoteSkinSegments[i].m_sNoteSkin = sNoteSkin;
	//}

	int iRow = BeatToNoteRow(fBeat);
	unsigned i;
	vector<NoteSkinSegment> &labels = this->m_NoteSkinSegments;
	for( i=0; i<labels.size(); i++ )
		if( labels[i].m_iStartRow >= iRow )
			break;
	
	if( i == labels.size() || labels[i].m_iStartRow != iRow )
	{
		if (i == 0 ||
			labels[i-1].m_sNoteSkin != sNoteSkin )
			AddNoteSkinSegment( NoteSkinSegment(iRow, sNoteSkin) );
	}
	else
	{
		if (i > 0 && ( labels[i-1].m_sNoteSkin == sNoteSkin /* test last or */|| sNoteSkin == "" ) )
			labels.erase( labels.begin()+i, labels.begin()+i+1 );
		else
			labels[i].m_sNoteSkin = sNoteSkin;
	}
}

RString TimingData::GetNoteSkinAtBeat( float fBeat ) const
{
	int iIndex = BeatToNoteRow( fBeat );
	unsigned i;
	for( i=0; i<m_NoteSkinSegments.size()-1; i++ )
		if( m_NoteSkinSegments[i+1].m_iStartRow > iIndex )
			break;
	RString sRet = m_NoteSkinSegments[i].m_sNoteSkin;
	sRet.MakeLower();
	return sRet;
}

float TimingData::GetNoteSkinSegmentStart( int iSegment ) const
{
	return m_NoteSkinSegments[iSegment].m_iStartRow;
}

NoteSkinSegment& TimingData::GetNoteSkinSegmentAtBeat( float fBeat )
{
	static NoteSkinSegment empty;
	if( m_NoteSkinSegments.empty() )
		return empty;
	int i = GetNoteSkinSegmentIndexAtBeat( fBeat );
	return m_NoteSkinSegments[i];
}

int TimingData::GetNoteSkinSegmentIndexAtBeat( float fBeat )
{
	int iIndex = BeatToNoteRow( fBeat );
	int i;
	for( i=0; i<(int)(m_NoteSkinSegments.size())-1; i++ )
		if( m_NoteSkinSegments[i+1].m_iStartRow > iIndex )
			break;
	return i;
}

static int CompareLabelSegments(const LabelSegment &seg1, const LabelSegment &seg2)
{
	return seg1.m_iStartRow < seg2.m_iStartRow;
}

void SortLabelSegmentsArray( vector<LabelSegment> &arrayLabelSegments )
{
	sort( arrayLabelSegments.begin(), arrayLabelSegments.end(), CompareLabelSegments );
}

void TimingData::AddLabelSegment( const LabelSegment &seg )
{
	m_LabelSegments.insert( upper_bound(m_LabelSegments.begin(), m_LabelSegments.end(), seg), seg );
}

void TimingData::SetLabelAtBeat( float fBeat, RString sLabel )
{
	//unsigned i;
	//for( i=0; i<m_NoteSkinSegments.size(); i++ )
	//	if( m_NoteSkinSegments[i].m_fStartBeat >= BeatToNoteRow( fBeat ) )
	//		break;

	//if( i == m_NoteSkinSegments.size() || m_NoteSkinSegments[i].m_fStartBeat != BeatToNoteRow( fBeat ) )// there is no TickSegment at the current beat
	//{
	//	// create a new NoteSkinSegment
	//	//if( i == 0 || m_NoteSkinSegments[i].m_sNoteSkin != sNoteSkin )
	//	AddNoteSkinSegment( NoteSkinSegment(BeatToNoteRow( fBeat ), sNoteSkin) );
	//}
	//else	// NoteSkinSegment being modified is m_NoteSkinSegments[i]
	//{
	//	if( i > 0 && m_NoteSkinSegments[i] == m_NoteSkinSegments[i-1] )
	//		m_NoteSkinSegments.erase( m_NoteSkinSegments.begin()+i, m_NoteSkinSegments.begin()+i+1);
	//	else
	//		m_NoteSkinSegments[i].m_sNoteSkin = sNoteSkin;
	//}

	int iRow = BeatToNoteRow(fBeat);
	unsigned i;
	vector<LabelSegment> &labels = this->m_LabelSegments;
	for( i=0; i<labels.size(); i++ )
		if( labels[i].m_iStartRow >= iRow )
			break;
	
	if( i == labels.size() || labels[i].m_iStartRow != iRow )
	{
		if (i == 0 ||
			labels[i-1].m_sLabel != sLabel )
			AddLabelSegment( LabelSegment(iRow, sLabel) );
	}
	else
	{
		if (i > 0 && ( labels[i-1].m_sLabel == sLabel /* test last or */|| sLabel == "" ) )
			labels.erase( labels.begin()+i, labels.begin()+i+1 );
		else
			labels[i].m_sLabel = sLabel;
	}
}

RString TimingData::GetLabelAtBeat( float fBeat ) const
{
	int iIndex = BeatToNoteRow( fBeat );
	unsigned i;
	for( i=0; i<m_LabelSegments.size()-1; i++ )
		if( m_LabelSegments[i+1].m_iStartRow > iIndex )
			break;
	RString sRet = m_LabelSegments[i].m_sLabel;
	sRet.MakeLower();
	return sRet;
}

int TimingData::GetLabelSegmentStart( int iSegment ) const
{
	return m_LabelSegments[iSegment].m_iStartRow;
}

LabelSegment& TimingData::GetLabelSegmentAtBeat( float fBeat )
{
	static LabelSegment empty;
	if( m_LabelSegments.empty() )
		return empty;
	int i = GetLabelSegmentIndexAtBeat( fBeat );
	return m_LabelSegments[i];
}

int TimingData::GetLabelSegmentIndexAtBeat( float fBeat )
{
	int iIndex = BeatToNoteRow( fBeat );
	int i;
	for( i=0; i<(int)(m_LabelSegments.size())-1; i++ )
		if( m_LabelSegments[i+1].m_iStartRow > iIndex )
			break;
	return i;
}

//combofactor stuff
static int CompareComboSegments(const ComboSegment &seg1, const ComboSegment &seg2)
{
	return seg1.m_fStartBeat < seg2.m_fStartBeat;
}

void SortComboSegmentsArray( vector<ComboSegment> &arrayComboSegments )
{
	sort( arrayComboSegments.begin(), arrayComboSegments.end(), CompareComboSegments );
}

void TimingData::AddComboSegment( const ComboSegment &seg )
{
	m_ComboSegments.insert( upper_bound(m_ComboSegments.begin(), m_ComboSegments.end(), seg), seg );
}

void TimingData::SetComboFactorAtBeat( float fBeat, int iCombo, int iMiss )
{
	unsigned i;
	//int iRow = BeatToNoteRow(fBeat);
	vector<ComboSegment> &combos = this->m_ComboSegments;
	for( i=0; i<combos.size(); i++ )
		if( combos[i].m_fStartBeat >= fBeat )
			break;
	
	if( i == combos.size() || combos[i].m_fStartBeat != fBeat )
	{
		if (i == 0 || combos[i-1].m_iComboFactor != iCombo ||
			combos[i-1].m_iMissComboFactor != iMiss)
			this->AddComboSegment(ComboSegment(fBeat, iCombo, iMiss ) );
	}
	else
	{
		if (i > 0 && combos[i-1].m_iComboFactor == iCombo &&
			combos[i-1].m_iMissComboFactor == iMiss)
			combos.erase( combos.begin()+i, combos.begin()+i+1 );
		else
		{
			combos[i].m_iComboFactor = iCombo;
			combos[i].m_iMissComboFactor = iMiss;
		}
	}
}

int TimingData::GetComboFactorAtBeat( float fBeat ) const
{
	float iIndex = fBeat;
	unsigned i;
	for( i=0; i<m_ComboSegments.size()-1; i++ )
		if( m_ComboSegments[i+1].m_fStartBeat > iIndex )
			break;
	return m_ComboSegments[i].m_iComboFactor;
}

int TimingData::GetMissComboFactorAtBeat( float fBeat ) const
{
	int iIndex = BeatToNoteRow( fBeat );
	unsigned i;
	for( i=0; i<m_ComboSegments.size()-1; i++ )
		if( m_ComboSegments[i+1].m_fStartBeat > iIndex )
			break;
	return m_ComboSegments[i].m_iMissComboFactor;
}

float TimingData::GetComboSegmentStart( int iSegment ) const
{
	return m_ComboSegments[iSegment].m_fStartBeat;
}

ComboSegment& TimingData::GetComboFactorSegmentAtBeat( float fBeat )
{
	static ComboSegment empty;
	if( m_ComboSegments.empty() )
		return empty;
	int i = GetComboSegmentIndexAtBeat( fBeat );
	return m_ComboSegments[i];
}

int TimingData::GetComboSegmentIndexAtBeat( float fBeat )
{
	float iIndex = fBeat;
	int i;
	for( i=0; i<(int)(m_ComboSegments.size())-1; i++ )
		if( m_ComboSegments[i+1].m_fStartBeat > iIndex )
			break;
	return i;
}

//HAsta aqui combo factor

//MODIFICADO POR MI
//EL ARROWSPACING SEGMENTS

static int CompareArrowSpacingSegments(const ArrowSpacingSegment &seg1, const ArrowSpacingSegment &seg2)
{
	return seg1.m_iStartRow < seg2.m_iStartRow;
}

void SortArrowSpacingSegmentsArray( vector<ArrowSpacingSegment> &arrayArrowSpacingSegments )
{
	sort( arrayArrowSpacingSegments.begin(), arrayArrowSpacingSegments.end(), CompareArrowSpacingSegments );
}

void TimingData::AddArrowSpacingSegment( const ArrowSpacingSegment &seg )
{
	m_ArrowSpacingSegments.insert( upper_bound(m_ArrowSpacingSegments.begin(), m_ArrowSpacingSegments.end(), seg), seg );
}

void TimingData::SetArrowSpacingAtBeat( float fBeat, float fArrowSpacing, float fShiftFactor )
{
	unsigned i;
	for( i=0; i<m_ArrowSpacingSegments.size(); i++ )
		//if( m_TickSegments[i].m_fStartBeat >= (fBeat-0.0001) && m_TickSegments[i].m_fStartBeat <= (fBeat+0.0001) )
		if( m_ArrowSpacingSegments[i].m_iStartRow >= BeatToNoteRow( fBeat ) )
			break;

	if( i == m_ArrowSpacingSegments.size() || m_ArrowSpacingSegments[i].m_iStartRow != BeatToNoteRow( fBeat ) )	// there is no TickSegment at the current beat
	{
		// create a new TickSegment
		if( i == 0 || abs(m_ArrowSpacingSegments[i-1].m_fArrowSpacing - fArrowSpacing) > 1e-5f )
			AddArrowSpacingSegment( ArrowSpacingSegment(BeatToNoteRow( fBeat ), fArrowSpacing, fShiftFactor) );
	}
	else	// TickSegment being modified is m_TickSegments[i]
	{
		if( i > 0  &&  abs(m_ArrowSpacingSegments[i-1].m_fArrowSpacing - fArrowSpacing) < 1e-5f )
			m_ArrowSpacingSegments.erase( m_ArrowSpacingSegments.begin()+i, m_ArrowSpacingSegments.begin()+i+1);
		else
		{
			m_ArrowSpacingSegments[i].m_fShiftFactor = fShiftFactor;
			m_ArrowSpacingSegments[i].m_fArrowSpacing = fArrowSpacing;
		}
	}
}

int TimingData::GetArrowSpacingAtBeat( float fBeat, float &fShiftFactorOut ) const
{
	int iIndex = BeatToNoteRow( fBeat );
	unsigned i;
	for( i=0; i<m_ArrowSpacingSegments.size()-1; i++ )
		if( m_ArrowSpacingSegments[i+1].m_iStartRow > iIndex )
			break;
	fShiftFactorOut = m_ArrowSpacingSegments[i].m_fShiftFactor;
	return m_ArrowSpacingSegments[i].m_fArrowSpacing;
}

float TimingData::GetArrowSpacingSegmentStart( int iSegment ) const
{
	return m_ArrowSpacingSegments[iSegment].m_iStartRow;
}

ArrowSpacingSegment& TimingData::GetArrowSpacingSegmentAtBeat( float fBeat )
{
	static ArrowSpacingSegment empty;
	if( m_ArrowSpacingSegments.empty() )
		return empty;
	int i = GetArrowSpacingSegmentIndexAtBeat( fBeat );
	return m_ArrowSpacingSegments[i];
}

int TimingData::GetArrowSpacingSegmentIndexAtBeat( float fBeat )
{
	int iIndex = BeatToNoteRow( fBeat );
	int i;
	for( i=0; i<(int)(m_ArrowSpacingSegments.size())-1; i++ )
		if( m_ArrowSpacingSegments[i+1].m_iStartRow > iIndex )
			break;
	return i;
}

/* FakeSegments stuffshits */

static int CompareFakeSegments(const FakeSegment &seg1, const FakeSegment &seg2)
{
	return seg1.m_fStartBeat < seg2.m_fStartBeat;
}

void SortFakeSegmentsArray( vector<FakeSegment> &arrayFakeSegments )
{
	sort( arrayFakeSegments.begin(), arrayFakeSegments.end(), CompareFakeSegments );
}

void TimingData::AddFakeSegment( const FakeSegment &seg )
{
	m_FakeSegments.insert( upper_bound(m_FakeSegments.begin(), m_FakeSegments.end(), seg), seg );
}

void TimingData::SetFakeAtBeat( float fBeat, float fLength )
{
	unsigned i;
	for( i=0; i<m_FakeSegments.size(); i++ )
		if( m_FakeSegments[i].m_fStartBeat >= fBeat )
			break;

	bool valid = BeatToNoteRow( fBeat ) > 0 && fLength > 0;
	if( i == m_FakeSegments.size() )
	{
		if( valid )
		{
			AddFakeSegment( FakeSegment( fBeat, fLength ) );
		}
	}
	else
	{
		if( valid )
		{
			m_FakeSegments[i].m_fLength = fLength;
		}
		else
			m_FakeSegments.erase( m_FakeSegments.begin()+i, m_FakeSegments.begin()+i+1 );
	}
}

float TimingData::GetFakeAtBeat( float fBeat ) const
{
	//int iIndex = BeatToNoteRow( fBeat );
	/*unsigned i;
	for( i=0; i<m_FakeSegments.size()-1; i++ )
		if( m_FakeSegments[i+1].m_fStartBeat > fBeat )
			break;
	return m_FakeSegments[i].m_fLength;*/
	const vector<FakeSegment> &fakes = this->m_FakeSegments;
	for( unsigned i=0; i<fakes.size(); i++ )
	{
		if( fakes[i].m_fStartBeat == fBeat )
		{
			return fakes[i].m_fLength;
		}
	}
	return 0;
}

float TimingData::GetFakeSegmentStart( int iSegment ) const
{
	return m_FakeSegments[iSegment].m_fStartBeat;
}

FakeSegment& TimingData::GetFakeSegmentAtBeat( float fBeat )
{
	static FakeSegment empty;
	if( m_FakeSegments.empty() )
		return empty;
	int i = GetFakeSegmentIndexAtBeat( fBeat );
	return m_FakeSegments[i];
}

int TimingData::GetFakeSegmentIndexAtBeat( float fBeat )
{
	//int iIndex = BeatToNoteRow( fBeat );
	int i;
	for( i=0; i<(int)(m_FakeSegments.size())-1; i++ )
		if( m_FakeSegments[i+1].m_fStartBeat > fBeat )
			break;
	return i;
}

bool TimingData::IsFakeAtRow(int iRow)
{
	if( m_FakeSegments.empty() )
		return false;
	
	//int i = GetFakeSegmentIndexAtBeat( NoteRowToBeat( iRow ) );
	FakeSegment s = GetFakeSegmentAtBeat( NoteRowToBeat(iRow));
	float beatRow = NoteRowToBeat(iRow);
	if( s.m_fStartBeat <= (beatRow) && (beatRow) < ( s.m_fStartBeat + s.m_fLength ) )
	{
		return true;
	}
	return false;
}

//scrolls stuff
static int CompareScrollSegments(const ScrollSegment &seg1, const ScrollSegment &seg2)
{
	return seg1.m_iStartRow < seg2.m_iStartRow;
}

void SortScrollSegmentsArray( vector<ScrollSegment> &arrayScrollSegments )
{
	sort( arrayScrollSegments.begin(), arrayScrollSegments.end(), CompareScrollSegments );
}

void TimingData::AddScrollSegment( const ScrollSegment &seg )
{
	m_ScrollSegments.insert( upper_bound(m_ScrollSegments.begin(), m_ScrollSegments.end(), seg), seg );
}

void TimingData::SetScrollAtBeat( float fBeat, float fPercent )
{
	int iRow = BeatToNoteRow(fBeat);
	unsigned i;
	vector<ScrollSegment> &scrolls = m_ScrollSegments;
	for( i = 0; i < scrolls.size(); i++ )
	{
		if( scrolls[i].m_iStartRow >= iRow)
			break;
	}
	
	if ( i == scrolls.size() || scrolls[i].m_iStartRow != iRow )
	{
		// the core mod itself matters the most for comparisons.
		if (i == 0 || scrolls[i-1].m_fRatio != fPercent )
			AddScrollSegment( ScrollSegment(iRow, fPercent) );
	}
	else
	{
		// The others aren't compared: only the mod itself matters.
		if (i > 0 && scrolls[i-1].m_fRatio == fPercent )
			scrolls.erase( scrolls.begin()+i, scrolls.begin()+i+1 );
		else
		{
			scrolls[i].m_fRatio = fPercent;
		}
	}
	//unsigned i;
	//for( i=0; i<m_ScrollSegments.size(); i++ )
	//	if( m_ScrollSegments[i].m_iStartRow >= BeatToNoteRow( fBeat ) )
	//		break;

	//if ( i == m_ScrollSegments.size() || BeatToNoteRow(m_ScrollSegments[i].m_iStartRow) != BeatToNoteRow( fBeat ) )
	//{
	//	// the core mod itself matters the most for comparisons.
	//	if (i == 0 || m_ScrollSegments[i-1].m_fRatio != fPercent )
	//		AddScrollSegment( ScrollSegment( BeatToNoteRow( fBeat ), fPercent ) );
	//}
	//else
	//{
	//	// The others aren't compared: only the mod itself matters.
	//	if (i > 0 && m_ScrollSegments[i-1].m_fRatio == fPercent )
	//		m_ScrollSegments.erase( m_ScrollSegments.begin()+i, m_ScrollSegments.begin()+i+1 );
	//	else
	//	{
	//		m_ScrollSegments[i].m_fRatio = fPercent;
	//	}
	//}
}

float TimingData::GetScrollAtBeat( float fBeat ) const
{
	int iIndex = BeatToNoteRow( fBeat );
	unsigned i;
	for( i=0; i<m_ScrollSegments.size()-1; i++ )
		if( m_ScrollSegments[i+1].m_iStartRow > iIndex )
			break;
	return m_ScrollSegments[i].m_fRatio;
}

float TimingData::GetScrollSegmentStart( int iSegment ) const
{
	return m_ScrollSegments[iSegment].m_iStartRow;
}

ScrollSegment& TimingData::GetScrollSegmentAtBeat( float fBeat )
{
	static ScrollSegment empty;
	if( m_ScrollSegments.empty() )
		return empty;
	int i = GetScrollSegmentIndexAtBeat( fBeat );
	return m_ScrollSegments[i];
}

int TimingData::GetScrollSegmentIndexAtBeat( float fBeat )
{
	int iIndex = BeatToNoteRow( fBeat );
	int i;
	for( i=0; i<(int)(m_ScrollSegments.size())-1; i++ )
		if( m_ScrollSegments[i+1].m_iStartRow > iIndex )
			break;
	return i;
}

/* ScrollSegments function utility */
float TimingData::GetDisplayedBeat( float fBeat ) const
{
	if (!this) return 1.0f;
	
	if( m_ScrollSegments.size() == 0 )
		return 1.0f;

	vector<ScrollSegment>::const_iterator it = m_ScrollSegments.begin(), end = m_ScrollSegments.end();
	float fOutBeat = 0;
	for( it = it + 1; it != end; it++ )
	{
		ASSERT(NoteRowToBeat((*(it-1)).m_iStartRow) <= NoteRowToBeat((*it).m_iStartRow));
	}
	it = m_ScrollSegments.begin();
	for( ; it != end; it++ )
	{
		if( it+1 == end || fBeat <= NoteRowToBeat((*(it+1)).m_iStartRow) )
		{
			fOutBeat += ( fBeat - NoteRowToBeat((*it).m_iStartRow) ) * (*it).m_fRatio;
			break;
		}
		else
		{
			fOutBeat += ( NoteRowToBeat((*(it+1)).m_iStartRow) - NoteRowToBeat((*it).m_iStartRow )) * (*it).m_fRatio;
		}
	}
	return fOutBeat;
}

/* Speed style ssc implementation */
static int CompareSpeedSegments(const SpeedSegment &seg1, const SpeedSegment &seg2)
{
	return seg1.m_iStartRow < seg2.m_iStartRow;
}

void SortSpeedSegmentsArray( vector<SpeedSegment> &arraySpeedSegments )
{
	sort( arraySpeedSegments.begin(), arraySpeedSegments.end(), CompareSpeedSegments );
}

void TimingData::AddSpeedSegment( const SpeedSegment &seg )
{
	m_SpeedSegments.insert( upper_bound(m_SpeedSegments.begin(), m_SpeedSegments.end(), seg), seg );
}

void TimingData::SetSpeedAtBeat( float fBeat, float fRatio, float fWait, unsigned short iUnit )
{
	unsigned i;
	for( i = 0; i < m_SpeedSegments.size(); i++ )
	{
		if( m_SpeedSegments[i].m_iStartRow >= BeatToNoteRow(fBeat) )
			break;
	}
	
	if ( i == m_SpeedSegments.size() || m_SpeedSegments[i].m_iStartRow != BeatToNoteRow(fBeat) )
	{
		// the core mod itself matters the most for comparisons.
		if (i == 0 || m_SpeedSegments[i-1].m_fRatio != fRatio )
			AddSpeedSegment( SpeedSegment( BeatToNoteRow(fBeat), fRatio, fWait, iUnit ) );
	}
	else
	{
		// The others aren't compared: only the mod itself matters.
		if (i > 0 && m_SpeedSegments[i-1].m_fRatio == fRatio )
			m_SpeedSegments.erase(m_SpeedSegments.begin()+i, m_SpeedSegments.begin()+i+1 );
		else
		{
			m_SpeedSegments[i].m_fRatio = (fRatio);
			m_SpeedSegments[i].m_fWait = (fWait);
			m_SpeedSegments[i].m_iUnit = (iUnit);
		}
	}
}

float TimingData::GetSpeedAtBeat( float fBeat ) const
{
	int iIndex = BeatToNoteRow( fBeat );
	unsigned i;
	for( i=0; i<m_SpeedSegments.size()-1; i++ )
		if( m_SpeedSegments[i+1].m_iStartRow > iIndex )
			break;
	return m_SpeedSegments[i].m_fRatio;
}

float TimingData::GetSpeedSegmentStart( int iSegment ) const
{
	return m_SpeedSegments[iSegment].m_iStartRow;
}

SpeedSegment& TimingData::GetSpeedSegmentAtBeat( float fBeat )
{
	static SpeedSegment empty;
	if( m_SpeedSegments.empty() )
		return empty;
	int i = GetSpeedSegmentIndexAtBeat( fBeat );
	return m_SpeedSegments[i];
}

int TimingData::GetSpeedSegmentIndexAtBeat( float fBeat )
{
	int iIndex = BeatToNoteRow( fBeat );
	int i;
	for( i=0; i<(int)(m_SpeedSegments.size())-1; i++ )
		if( m_SpeedSegments[i+1].m_iStartRow > iIndex )
			break;
	return i;
}

float TimingData::GetDisplayedSpeedPercent( float fSongBeat, float fMusicSeconds )
{
	/* HACK: Somehow we get called into this function when there is no
	 * TimingData to work with. This seems to happen the most upon
	 * leaving the editor. Still, cover our butts in case this instance
	 * isn't existing. */
	if (!this) return 1.0f;
	
	if( m_SpeedSegments.size() == 0 )
		return 1.0f;

	const int index = this->GetSpeedSegmentIndexAtBeat( fSongBeat );
	
	const SpeedSegment seg = m_SpeedSegments[index];
	float fStartBeat = NoteRowToBeat(seg.m_iStartRow);
	float fStartTime = this->GetElapsedTimeFromBeat( fStartBeat ) - this->GetStopAtBeat( fStartBeat, true );
	float fEndTime;
	float fCurTime = fMusicSeconds;
	
	if( seg.m_iUnit == 1 ) // seconds
	{
		fEndTime = fStartTime + seg.m_fWait;
	}
	else
	{
		fEndTime = GetElapsedTimeFromBeat( fStartBeat + seg.m_fWait ) 
		- GetStopAtBeat( fStartBeat + seg.m_fWait, true );
	}
	
	SpeedSegment first = m_SpeedSegments[0];
	
	if( ( index == 0 && first.m_fWait > 0.0 ) && fCurTime < fStartTime )
	{
		return 1.0f;
	}
	else if( fEndTime >= fCurTime && ( index > 0 || first.m_fWait > 0.0 ) )
	{
		const float fPriorSpeed = (index == 0 ?
								   1 : m_SpeedSegments[index - 1].m_fRatio );
		float fTimeUsed = fCurTime - fStartTime;
		float fDuration = fEndTime - fStartTime;
		float fRatioUsed = fDuration == 0.0 ? 1 : fTimeUsed / fDuration;
		
		float fDistance = fPriorSpeed - seg.m_fRatio;
		float fRatioNeed = fRatioUsed * -fDistance;
		return (fPriorSpeed + fRatioNeed);
	}
	else 
	{
		return seg.m_fRatio;
	}
}

static int CompareCountSepSegments(const CountSeparatelySegment &seg1, const CountSeparatelySegment &seg2)
{
	return seg1.m_iStartRow < seg2.m_iStartRow;
}

void SortCountSepSegmentsArray( vector<CountSeparatelySegment> &arrayCountSepSegments )
{
	sort( arrayCountSepSegments.begin(), arrayCountSepSegments.end(), CompareCountSepSegments );
}

void TimingData::AddCountSepSegment( const CountSeparatelySegment &seg )
{
	m_CSepSegments.insert( upper_bound(m_CSepSegments.begin(), m_CSepSegments.end(), seg), seg );
}

void TimingData::SetCountSepAtBeat( float fBeat, int iCountSep )
{
	unsigned i;
	for( i = 0; i < m_CSepSegments.size(); i++ )
	{
		if( m_CSepSegments[i].m_iStartRow >= BeatToNoteRow(fBeat) )
			break;
	}
	
	if ( i == m_CSepSegments.size() || m_CSepSegments[i].m_iStartRow != BeatToNoteRow(fBeat) )
	{
		// the core mod itself matters the most for comparisons.
		if (i == 0 || m_CSepSegments[i-1].m_iCountSep != iCountSep )
			AddCountSepSegment( CountSeparatelySegment( BeatToNoteRow(fBeat), iCountSep ) );
	}
	else
	{
		// The others aren't compared: only the mod itself matters.
		if (i > 0 && m_CSepSegments[i-1].m_iCountSep == iCountSep )
			m_CSepSegments.erase(m_CSepSegments.begin()+i, m_CSepSegments.begin()+i+1 );
		else
		{
			m_CSepSegments[i].m_iCountSep = (iCountSep);			
		}
	}
}

bool TimingData::GetCountSepAtBeat( float fBeat ) const
{
	if( m_CSepSegments.size() == 0 )
		return false;

	int iIndex = BeatToNoteRow( fBeat );
	unsigned i;
	for( i=0; i<m_CSepSegments.size()-1; i++ )
		if( m_CSepSegments[i+1].m_iStartRow > iIndex )
			break;
	return m_CSepSegments[i].m_iCountSep == 0 ? false : true;
}

float TimingData::GetCountSepSegmentStart( int iSegment ) const
{
	return m_CSepSegments[iSegment].m_iStartRow;
}

CountSeparatelySegment& TimingData::GetCountSepSegmentAtBeat( float fBeat )
{
	static CountSeparatelySegment empty;
	if( m_CSepSegments.empty() )
		return empty;
	int i = GetCountSepSegmentIndexAtBeat( fBeat );
	return m_CSepSegments[i];
}

int TimingData::GetCountSepSegmentIndexAtBeat( float fBeat )
{
	int iIndex = BeatToNoteRow( fBeat );
	int i;
	for( i=0; i<(int)(m_CSepSegments.size())-1; i++ )
		if( m_CSepSegments[i+1].m_iStartRow > iIndex )
			break; 
	return i;
}

static int CompareReqHoldHeadSegments(const RequireStepOnHoldHeadSegment &seg1, const RequireStepOnHoldHeadSegment &seg2)
{
	return seg1.m_iStartRow < seg2.m_iStartRow;
}

void SortReqHoldHeadSegmentsArray( vector<RequireStepOnHoldHeadSegment> &arrayReqHoldHeadSegments )
{
	sort( arrayReqHoldHeadSegments.begin(), arrayReqHoldHeadSegments.end(), CompareReqHoldHeadSegments );
}

void TimingData::AddReqHoldHeadSegment( const RequireStepOnHoldHeadSegment &seg )
{
	m_ReqHoldHeadSegments.insert( upper_bound(m_ReqHoldHeadSegments.begin(), m_ReqHoldHeadSegments.end(), seg), seg );
}

void TimingData::SetReqHoldHeadAtBeat( float fBeat, int iRequiredHoldHead )
{
	unsigned i;
	for( i = 0; i < m_ReqHoldHeadSegments.size(); i++ )
	{
		if( m_ReqHoldHeadSegments[i].m_iStartRow >= BeatToNoteRow(fBeat) )
			break;
	}
	
	if ( i == m_ReqHoldHeadSegments.size() || m_ReqHoldHeadSegments[i].m_iStartRow != BeatToNoteRow(fBeat) )
	{
		// the core mod itself matters the most for comparisons.
		if (i == 0 || m_ReqHoldHeadSegments[i-1].m_iRequireStep != iRequiredHoldHead )
			AddReqHoldHeadSegment( RequireStepOnHoldHeadSegment( BeatToNoteRow(fBeat), iRequiredHoldHead ) );
	}
	else
	{
		// The others aren't compared: only the mod itself matters.
		if (i > 0 && m_ReqHoldHeadSegments[i-1].m_iRequireStep == iRequiredHoldHead )
			m_ReqHoldHeadSegments.erase(m_ReqHoldHeadSegments.begin()+i, m_ReqHoldHeadSegments.begin()+i+1 );
		else
		{
			m_ReqHoldHeadSegments[i].m_iRequireStep = (iRequiredHoldHead);			
		}
	}
}

bool TimingData::GetReqHoldHeadAtBeat( float fBeat ) const
{
	if( m_ReqHoldHeadSegments.size() == 0 )
		return false;

	int iIndex = BeatToNoteRow( fBeat );
	unsigned i;
	for( i=0; i<m_ReqHoldHeadSegments.size()-1; i++ )
		if( m_ReqHoldHeadSegments[i+1].m_iStartRow > iIndex )
			break;
	return m_ReqHoldHeadSegments[i].m_iRequireStep == 0 ? false : true;
}

float TimingData::GetReqHoldHeadSegmentStart( int iSegment ) const
{
	return m_ReqHoldHeadSegments[iSegment].m_iStartRow;
}

RequireStepOnHoldHeadSegment& TimingData::GetReqHoldHeadSegmentAtBeat( float fBeat )
{
	static RequireStepOnHoldHeadSegment empty;
	if( m_ReqHoldHeadSegments.empty() )
		return empty;
	int i = GetReqHoldHeadSegmentIndexAtBeat( fBeat );
	return m_ReqHoldHeadSegments[i];
}

int TimingData::GetReqHoldHeadSegmentIndexAtBeat( float fBeat )
{
	int iIndex = BeatToNoteRow( fBeat );
	int i;
	for( i=0; i<(int)(m_ReqHoldHeadSegments.size())-1; i++ )
		if( m_ReqHoldHeadSegments[i+1].m_iStartRow > iIndex )
			break; 
	return i;
}


/* WarpSegments stuffshits */

static int CompareWarpSegments(const WarpSegment &seg1, const WarpSegment &seg2)
{
	return seg1.m_fStartBeat < seg2.m_fStartBeat;
}

void SortWarpSegmentsArray( vector<WarpSegment> &arrayWarpSegments )
{
	sort( arrayWarpSegments.begin(), arrayWarpSegments.end(), CompareWarpSegments );
}

void TimingData::AddWarpSegment( const WarpSegment &seg )
{
	m_WarpSegments.insert( upper_bound(m_WarpSegments.begin(), m_WarpSegments.end(), seg), seg );
}

void TimingData::SetWarpAtBeat( float fBeat, float fLength, bool bDynamic )
{
	unsigned i;
	for( i=0; i<m_WarpSegments.size(); i++ )
		if( m_WarpSegments[i].m_fStartBeat >= fBeat )
			break;

	bool valid = BeatToNoteRow( fBeat ) > 0 && fLength > 0;
	if( i == m_WarpSegments.size() )
	{
		if( valid )
		{
			WarpSegment ws( fBeat, fLength );
			ws.m_bDynamic = bDynamic;
			AddWarpSegment( ws );
		}
	}
	else
	{
		if( valid )
		{
			m_WarpSegments[i].m_fLength = fLength;
			m_WarpSegments[i].m_bDynamic = bDynamic;
		}
		else
			m_WarpSegments.erase( m_WarpSegments.begin()+i, m_WarpSegments.begin()+i+1 );
	}
}

float TimingData::GetWarpAtBeat( float fBeat ) const
{
	//int iIndex = BeatToNoteRow( fBeat );
	/*unsigned i;
	for( i=0; i<m_WarpSegments.size()-1; i++ )
		if( m_WarpSegments[i+1].m_fStartBeat > fBeat )
			break;
	return m_WarpSegments[i].m_fLength;*/
	//
	const vector<WarpSegment> &warps = this->m_WarpSegments;
	for( unsigned i=0; i<warps.size(); i++ )
	{
		if( warps[i].m_fStartBeat == fBeat )
		{
			return warps[i].m_fLength;
		}
	}
	return 0;
}

float TimingData::GetWarpSegmentStart( int iSegment ) const
{
	return m_WarpSegments[iSegment].m_fStartBeat;
}

WarpSegment& TimingData::GetWarpSegmentAtBeat( float fBeat ) const
{
	static WarpSegment empty;
	if( m_WarpSegments.empty() )
		return empty;
	int i = GetWarpSegmentIndexAtBeat( fBeat );
	return const_cast<WarpSegment&>(m_WarpSegments[i]);
}

int TimingData::GetWarpSegmentIndexAtBeat( float fBeat ) const
{
	//int iIndex = BeatToNoteRow( fBeat );
	int i;
	for( i=0; i<(int)(m_WarpSegments.size())-1; i++ )
		if( m_WarpSegments[i+1].m_fStartBeat > fBeat )
			break;
	return i;
}

bool TimingData::IsWarpAtRow(int iRow)
{
	if( m_WarpSegments.empty() )
		return false;
	
	int i = GetWarpSegmentIndexAtBeat( NoteRowToBeat(iRow) );
	const WarpSegment s = m_WarpSegments[i];
	float beatRow = NoteRowToBeat(iRow);
	if( s.m_fStartBeat <= beatRow && beatRow < (s.m_fStartBeat + s.m_fLength ) )
	{
		// Allow stops inside warps to allow things like stop, warp, stop, warp, stop, and so on.
		if( m_StopSegments.empty() )
		{
			return true;
		}
		if( GetStopAtRow(iRow, false) != 0.0f || GetStopAtRow(iRow, true) != 0.0f )
		{
			return false;
		}
		return true;
	}
	return false;
}

void TimingData::GetBeatAndBPSFromElapsedTime( float fElapsedTime, float &fBeatOut, float &fBPSOut, bool &bFreezeOut, bool &bDelayOut, int &iWarpBeginOut, float &fWarpDestOut ) const
{
	fElapsedTime += PREFSMAN->m_fGlobalOffsetSeconds;

	GetBeatAndBPSFromElapsedTimeNoOffsetSSC( fElapsedTime, fBeatOut, fBPSOut, bFreezeOut, bDelayOut, iWarpBeginOut, fWarpDestOut );
}

//for ssc stuff
enum
{
	FOUND_WARP,
	FOUND_WARP_DESTINATION,
	FOUND_BPM_CHANGE,
	FOUND_STOP,
	FOUND_MARKER,
	NOT_FOUND
};

void TimingData::GetBeatAndBPSFromElapsedTimeNoOffset( float fElapsedTime, float &fBeatOut, float &fBPSOut, bool &bFreezeOut ) const
{
//	LOG->Trace( "GetBeatAndBPSFromElapsedTime( fElapsedTime = %f )", fElapsedTime );

	fElapsedTime += m_fBeat0OffsetInSeconds;


	for( unsigned i=0; i<m_BPMSegments.size(); i++ ) // foreach BPMSegment
	{
		const int iStartRowThisSegment = m_BPMSegments[i].m_iStartRow;
		const float fStartBeatThisSegment = NoteRowToBeat( iStartRowThisSegment );
		const bool bIsFirstBPMSegment = i==0;
		const bool bIsLastBPMSegment = i==m_BPMSegments.size()-1;
		const int iStartRowNextSegment = bIsLastBPMSegment ? MAX_NOTE_ROW : m_BPMSegments[i+1].m_iStartRow; 
		const float fStartBeatNextSegment = NoteRowToBeat( iStartRowNextSegment );
		const float fBPS = m_BPMSegments[i].m_fBPS;

		for( unsigned j=0; j<m_StopSegments.size(); j++ )	// foreach freeze
		{
			if( !bIsFirstBPMSegment && iStartRowThisSegment >= m_StopSegments[j].m_iStartRow )
				continue;
			if( !bIsLastBPMSegment && m_StopSegments[j].m_iStartRow > iStartRowNextSegment )
				continue;

				// this freeze lies within this BPMSegment
			const int iRowsBeatsSinceStartOfSegment = m_StopSegments[j].m_iStartRow - iStartRowThisSegment;
			const float fBeatsSinceStartOfSegment = NoteRowToBeat(iRowsBeatsSinceStartOfSegment);
			const float fFreezeStartSecond = fBeatsSinceStartOfSegment / fBPS;
			
			if( fFreezeStartSecond >= fElapsedTime )
				break;

			// the freeze segment is <= current time
			fElapsedTime -= m_StopSegments[j].m_fStopSeconds;

			if( fFreezeStartSecond >= fElapsedTime )
			{
				/* The time lies within the stop. */
				fBeatOut = NoteRowToBeat(m_StopSegments[j].m_iStartRow);
				fBPSOut = fBPS;
				bFreezeOut = true;
				return;
			}
		}

		const float fBeatsInThisSegment = fStartBeatNextSegment - fStartBeatThisSegment;
		const float fSecondsInThisSegment =  fBeatsInThisSegment / fBPS;
		if( bIsLastBPMSegment || fElapsedTime <= fSecondsInThisSegment )
		{
			// this BPMSegment IS the current segment
			fBeatOut = fStartBeatThisSegment + fElapsedTime*fBPS;
			fBPSOut = fBPS;
			bFreezeOut = false;
			return;
		}

		// this BPMSegment is NOT the current segment
		fElapsedTime -= fSecondsInThisSegment;
	}
}


float TimingData::GetElapsedTimeFromBeat( float fBeat ) const
{
	/* Using SSC modifiers */
	return TimingData::GetElapsedTimeFromBeatNoOffsetSSC( fBeat ) - PREFSMAN->m_fGlobalOffsetSeconds;
}

float TimingData::GetElapsedTimeFromBeatNoOffset( float fBeat ) const
{
	float fElapsedTime = 0;
	fElapsedTime -= m_fBeat0OffsetInSeconds;

	int iRow = BeatToNoteRow(fBeat);
	for( unsigned j=0; j<m_StopSegments.size(); j++ )	// foreach freeze
	{
		/* The exact beat of a stop comes before the stop, not after, so use >=, not >. */
		/* la otra parte (esto hace que los estos sean como la PIU) esta en player.cpp */
		//if( !GAMESTATE->IsUsingPiuStops() ) //test
		if( !m_StopSegments[j].m_bDelay && !PREFSMAN->m_bUsePiuStops )
		{
			if( m_StopSegments[j].m_iStartRow >= iRow )
				break;
		}
		else//aqui actua como piustops
		{
			if( m_StopSegments[j].m_iStartRow > iRow )
				break;
		}

		fElapsedTime += m_StopSegments[j].m_fStopSeconds;

	}

	for( unsigned i=0; i<m_BPMSegments.size(); i++ ) // foreach BPMSegment
	{
		const bool bIsLastBPMSegment = i==m_BPMSegments.size()-1;
		const float fBPS = m_BPMSegments[i].m_fBPS;

		if( bIsLastBPMSegment )
		{
			fElapsedTime += NoteRowToBeat( iRow ) / fBPS;
		}
		else
		{
			const int iStartIndexThisSegment = m_BPMSegments[i].m_iStartRow;
			const int iStartIndexNextSegment = m_BPMSegments[i+1].m_iStartRow; 
			const int iRowsInThisSegment = min( iStartIndexNextSegment - iStartIndexThisSegment, iRow );
			fElapsedTime += NoteRowToBeat( iRowsInThisSegment ) / fBPS;
			iRow -= iRowsInThisSegment;
		}
		
		if( iRow <= 0 )
			return fElapsedTime;
	}

	return fElapsedTime;
}

/* SSC testing stuff */
void TimingData::GetBeatAndBPSFromElapsedTimeNoOffsetSSC( float fElapsedTime, float &fBeatOut, float &fBPSOut, bool &bFreezeOut, bool &bDelayOut, int &iWarpBeginOut, float &fWarpDestinationOut ) const
{
	/*
	const vector<TimingSegment *> * segs = this->allTimingSegments;
	vector<TimingSegment *>::const_iterator itBPMS = segs[SEGMENT_BPM].begin();
	vector<TimingSegment *>::const_iterator itWS   = segs[SEGMENT_WARP].begin();
	vector<TimingSegment *>::const_iterator itSS   = segs[SEGMENT_STOP_DELAY].begin();
	*/

	vector<BPMSegment>::const_iterator itBPMS = m_BPMSegments.begin();
	vector<WarpSegment>::const_iterator itWS = m_WarpSegments.begin();
	vector<StopSegment>::const_iterator itSS = m_StopSegments.begin();
	
	bFreezeOut = false;
	bDelayOut = false;
	
	iWarpBeginOut = -1;
	
	int iLastRow = 0;
	float fLastTime = -m_fBeat0OffsetInSeconds;
	float fBPS = GetBPMAtBeat(0) / 60.0f;
	
	float bIsWarping = false;
	float fWarpDestination = 0;
	
	for( ;; )
	{
		int iEventRow = INT_MAX;
		int iEventType = NOT_FOUND;
		if( bIsWarping && BeatToNoteRow(fWarpDestination) < iEventRow )
		{
			iEventRow = BeatToNoteRow(fWarpDestination);
			iEventType = FOUND_WARP_DESTINATION;
		}
		if (itBPMS != m_BPMSegments.end() && (*itBPMS).m_iStartRow < iEventRow )
		{
			iEventRow = (*itBPMS).m_iStartRow;
			iEventType = FOUND_BPM_CHANGE;
		}
		if (itSS != m_StopSegments.end() && (*itSS).m_iStartRow < iEventRow )
		{
			//WarpSegment wp = GetWarpSegmentAtBeat(NoteRowToBeat((*itSS).m_iStartRow));
			//if( !wp.m_bDynamic /*&& bIsWarping*/ )
			{
				iEventRow = (*itSS).m_iStartRow;
				iEventType = FOUND_STOP;
			}
		}
		if (itWS != m_WarpSegments.end() &&	BeatToNoteRow((*itWS).m_fStartBeat) < iEventRow )
		{
			iEventRow = BeatToNoteRow((*itWS).m_fStartBeat);
			iEventType = FOUND_WARP;
		}
		if( iEventType == NOT_FOUND )
		{
			break;
		}
		float fTimeToNextEvent = bIsWarping ? 0 : NoteRowToBeat( iEventRow - iLastRow ) / fBPS;
		float fNextEventTime   = fLastTime + fTimeToNextEvent;
		if ( fElapsedTime < fNextEventTime )
		{
			break;
		}
		fLastTime = fNextEventTime;
		switch( iEventType )
		{
		case FOUND_WARP_DESTINATION:
			bIsWarping = false;
			break;
		case FOUND_BPM_CHANGE:
			fBPS = (*itBPMS).m_fBPS;
			itBPMS ++;
			break;
		case FOUND_STOP:
			{
				const StopSegment ss = (*itSS);
				fTimeToNextEvent = ss.m_fStopSeconds;
				fNextEventTime   = fLastTime + fTimeToNextEvent;
				const bool bIsDelay = ss.m_bDelay;
				if ( fElapsedTime < fNextEventTime )
				{
					bFreezeOut = !bIsDelay;
					bDelayOut  = bIsDelay;
					fBeatOut   = NoteRowToBeat(ss.m_iStartRow);
					fBPSOut    = fBPS;
					return;
				}
				fLastTime = fNextEventTime;
				itSS ++;
			}
			break;
		case FOUND_WARP:
			{
				bIsWarping = true;
				const WarpSegment ws = (*itWS);
				float fWarpSum = ws.m_fLength + ws.m_fStartBeat;
				if( fWarpSum > fWarpDestination )
				{
					fWarpDestination = fWarpSum;
				}
				iWarpBeginOut = iEventRow;
				fWarpDestinationOut = fWarpDestination;
				itWS ++;
				break;
			}
		}
		iLastRow = iEventRow;
	}
	
	fBeatOut = NoteRowToBeat( iLastRow ) + (fElapsedTime - fLastTime) * fBPS;
	fBPSOut = fBPS;
}

float TimingData::GetElapsedTimeFromBeatNoOffsetSSC( float fBeat ) const
{
	/*
	const vector<TimingSegment *> * segs = this->allTimingSegments;
	vector<TimingSegment *>::const_iterator itBPMS = segs[SEGMENT_BPM].begin();
	vector<TimingSegment *>::const_iterator itWS   = segs[SEGMENT_WARP].begin();
	vector<TimingSegment *>::const_iterator itSS   = segs[SEGMENT_STOP_DELAY].begin();
	*/
	vector<BPMSegment>::const_iterator itBPMS = m_BPMSegments.begin();
	vector<WarpSegment>::const_iterator itWS = m_WarpSegments.begin();
	vector<StopSegment>::const_iterator itSS = m_StopSegments.begin();
	
	int iLastRow = 0;
	float fLastTime = -m_fBeat0OffsetInSeconds;
	float fBPS = GetBPMAtBeat(0) / 60.0f;
	
	float bIsWarping = false;
	float fWarpDestination = 0;
	
	for( ;; )
	{
		int iEventRow = INT_MAX;
		int iEventType = NOT_FOUND;
		if( bIsWarping && BeatToNoteRow(fWarpDestination) < iEventRow )
		{
			iEventRow = BeatToNoteRow(fWarpDestination);
			iEventType = FOUND_WARP_DESTINATION;
		}
		if (itBPMS != m_BPMSegments.end() && (*itBPMS).m_iStartRow < iEventRow )
		{
			iEventRow = (*itBPMS).m_iStartRow;
			iEventType = FOUND_BPM_CHANGE;
		}
		if (itSS != m_StopSegments.end() && (*itSS).m_bDelay && (*itSS).m_iStartRow < iEventRow ) // delays (come before marker)
		{
			//dynamic warps
			WarpSegment wp = GetWarpSegmentAtBeat(NoteRowToBeat((*itSS).m_iStartRow));
			if( !wp.m_bDynamic /*&& bIsWarping*/ )
			{
				iEventRow = (*itSS).m_iStartRow;
				iEventType = FOUND_STOP;
			}
		}
		if( BeatToNoteRow(fBeat) < iEventRow )
		{
			iEventRow = BeatToNoteRow(fBeat);
			iEventType = FOUND_MARKER;
		}
		if (itSS != m_StopSegments.end() &&	!(*itSS).m_bDelay && (*itSS).m_iStartRow < iEventRow ) // stops (come after marker)
		{
			//dynamic warps
			//WarpSegment wp = GetWarpSegmentAtBeat(NoteRowToBeat((*itSS).m_iStartRow));
			//if( !wp.m_bDynamic /*&& bIsWarping*/ )
			{
				iEventRow = (*itSS).m_iStartRow;
				iEventType = FOUND_STOP;
			}
		}
		if (itWS != m_WarpSegments.end() &&	BeatToNoteRow((*itWS).m_fStartBeat) < iEventRow )
		{
			iEventRow = BeatToNoteRow((*itWS).m_fStartBeat);
			iEventType = FOUND_WARP;
		}
		float fTimeToNextEvent = bIsWarping ? 0 : NoteRowToBeat( iEventRow - iLastRow ) / fBPS;
		float fNextEventTime   = fLastTime + fTimeToNextEvent;
		fLastTime = fNextEventTime;
		switch( iEventType )
		{
		case FOUND_WARP_DESTINATION:
			bIsWarping = false;
			break;
		case FOUND_BPM_CHANGE:
			fBPS = (*itBPMS).m_fBPS;
			itBPMS ++;
			break;
		case FOUND_STOP:
			fTimeToNextEvent = (*itSS).m_fStopSeconds;
			fNextEventTime   = fLastTime + fTimeToNextEvent;
			fLastTime = fNextEventTime;
			itSS ++;
			break;
		case FOUND_MARKER:
			return fLastTime;	
		case FOUND_WARP:
			{
				bIsWarping = true;
				WarpSegment ws = (*itWS);
				float fWarpSum = ws.m_fLength + ws.m_fStartBeat;
				if( fWarpSum > fWarpDestination )
				{
					fWarpDestination = fWarpSum;
				}
				itWS ++;
				break;
			}
		}
		iLastRow = iEventRow;
	}
	
	// won't reach here, unless BeatToNoteRow(fBeat == INT_MAX) (impossible)
	
}

void TimingData::Scale( int start, int length, int newLength, 
					vector<SpeedSegment>& speed, 
					vector<ScrollSegment>& scroll,
					vector<NoteSkinSegment>& skin,
					vector<WarpSegment>& warp, 
					vector<FakeSegment>& fake,
					vector<BPMSegment>& bpm,
					vector<StopSegment>& stop,
					vector<ComboSegment>& combo,
					vector<ArrowSpacingSegment>& arrow,
					vector<TickSegment>& tick,
					vector<LabelSegment>& label,
					vector<CountSeparatelySegment>& csep)
{

	if( tick.size() > 0 )
	{
		for( unsigned i = 0; i < tick.size(); i++ )
			tick[i].m_fStartBeat = NoteRowToBeat(ScalePosition(start,length,newLength,BeatToNoteRow(tick[i].m_fStartBeat)));
	}

	if( combo.size() > 0 )
	{
		for( unsigned i = 0; i < combo.size(); i++ )
			combo[i].m_fStartBeat = NoteRowToBeat(ScalePosition(start,length,newLength,BeatToNoteRow(combo[i].m_fStartBeat)));
	}

	if( stop.size() > 0 )
	{
		for( unsigned i = 0; i < stop.size(); i++ )
			stop[i].m_iStartRow = ScalePosition( start, length, newLength, stop[i].m_iStartRow );
	}

	if( speed.size() > 0 )
	{
		for( unsigned i = 0; i < speed.size(); i++ )			
		{
			if( speed[i].m_iUnit == 0 )
			{
				// XXX: this function is duplicated, there should be a better way
				float startBeat    = NoteRowToBeat(speed[i].m_iStartRow);
				float endBeat      = startBeat + speed[i].m_fWait;
				float newStartBeat = ScalePosition(NoteRowToBeat(start),
									NoteRowToBeat(length),
								   NoteRowToBeat(newLength),
								   startBeat);
				float newEndBeat   = ScalePosition(NoteRowToBeat(start),
								   NoteRowToBeat(length),
								   NoteRowToBeat(newLength),
								   endBeat);
				speed[i].m_fWait = ( newEndBeat - newStartBeat );
			}
			//TimingSegment::Scale( start, oldLength, newLength );
			speed[i].m_iStartRow = ScalePosition( start, length, newLength, speed[i].m_iStartRow );
		}		
	}
	
	if( scroll.size() > 0 )
	{
		for( unsigned i = 0; i < scroll.size(); i++ )
			scroll[i].m_iStartRow = ScalePosition( start, length, newLength, scroll[i].m_iStartRow );
	}
	
	if(skin.size() > 0 )
	{
		for( unsigned i = 0; i < skin.size(); i++ )
			skin[i].m_iStartRow = ScalePosition( start, length, newLength, skin[i].m_iStartRow );
	}

	if(label.size() > 0 )
	{
		for( unsigned i = 0; i < label.size(); i++ )
			label[i].m_iStartRow = ScalePosition( start, length, newLength, label[i].m_iStartRow );
	}

	if(warp.size() > 0)
	{
		for( unsigned i = 0; i < warp.size(); i++ )
		{
			float startBeat    = warp[i].m_fStartBeat;
			float endBeat      = startBeat + warp[i].m_fLength;
			float newStartBeat = ScalePosition( NoteRowToBeat(start), NoteRowToBeat(length), NoteRowToBeat(newLength), startBeat );
			float newEndBeat   = ScalePosition( NoteRowToBeat(start), NoteRowToBeat(length), NoteRowToBeat(newLength), endBeat );
			warp[i].m_fLength = ( newEndBeat - newStartBeat );
			//TimingSegment::Scale( start, length, newLength );
			warp[i].m_fStartBeat = NoteRowToBeat(ScalePosition( start, length, newLength, BeatToNoteRow(warp[i].m_fStartBeat) ));
		}
	}

	if( fake.size() > 0 )
	{
		for( unsigned i = 0; i < fake.size(); i++ )
		{
			float startBeat    = fake[i].m_fStartBeat;
			float endBeat      = startBeat + fake[i].m_fLength;
			float newStartBeat = ScalePosition( NoteRowToBeat(start), NoteRowToBeat(length), NoteRowToBeat(newLength), startBeat );
			float newEndBeat   = ScalePosition( NoteRowToBeat(start), NoteRowToBeat(length), NoteRowToBeat(newLength), endBeat );
			fake[i].m_fLength = ( newEndBeat - newStartBeat );
			//TimingSegment::Scale( start, length, newLength );
			fake[i].m_fStartBeat = NoteRowToBeat(ScalePosition( start, length, newLength, BeatToNoteRow(fake[i].m_fStartBeat) ));
		}
	}
}

void TimingData::ScaleRegion( float fScale, int iStartIndex, int iEndIndex, bool bAdjustBPM )
{
	ASSERT( fScale > 0 );
	ASSERT( iStartIndex >= 0 );
	ASSERT( iStartIndex < iEndIndex );
	
	int length = iEndIndex - iStartIndex;
	int newLength = lrintf( fScale * length );
	
	// TODO: Confirm this works as intended.
	/*for (unsigned i = 0; i < NUM_TimingSegmentType; i++)
	{
		vector<TimingSegment *> &segs = this->allTimingSegments[i];
		for (unsigned j = 0; j < segs.size(); j++)
		{
			segs[i][j].Scale(iStartIndex, length, newLength);
		}
	}*/

	Scale( iStartIndex, length, newLength, m_SpeedSegments, m_ScrollSegments,
		m_NoteSkinSegments, m_WarpSegments, m_FakeSegments, m_BPMSegments, m_StopSegments, m_ComboSegments, m_ArrowSpacingSegments, m_TickSegments, m_LabelSegments, m_CSepSegments );
	
	// adjust BPM changes to preserve timing
	if( bAdjustBPM )
	{
		int iNewEndIndex = iStartIndex + newLength;
		float fEndBPMBeforeScaling = GetBPMAtBeat(NoteRowToBeat(iNewEndIndex));
		vector<BPMSegment> &bpms = m_BPMSegments;
		
		// adjust BPM changes "between" iStartIndex and iNewEndIndex
		for ( unsigned i = 0; i < bpms.size(); i++ )
		{
			BPMSegment& bpm = (bpms[i]);
			const int iSegStart = bpm.m_iStartRow;
			if( iSegStart <= iStartIndex )
				continue;
			else if( iSegStart >= iNewEndIndex )
				continue;
			else
				bpm.SetBPM( bpm.GetBPM() * fScale );
		}
		
		// set BPM at iStartIndex and iNewEndIndex.
		SetBPMAtRow( iStartIndex, GetBPMAtBeat(NoteRowToBeat(iStartIndex)) * fScale );
		SetBPMAtRow( iNewEndIndex, fEndBPMBeforeScaling );
	}

	/*ASSERT( fScale > 0 );
	ASSERT( iStartIndex >= 0 );
	ASSERT( iStartIndex < iEndIndex );*/

	//for ( unsigned i = 0; i < m_BPMSegments.size(); i++ )
	//{
	//	const int iSegStart = m_BPMSegments[i].m_iStartRow;
	//	if( iSegStart < iStartIndex )
	//		continue;
	//	else if( iSegStart > iEndIndex )
	//		m_BPMSegments[i].m_iStartRow += lrintf( (iEndIndex - iStartIndex) * (fScale - 1) );
	//	else
	//		m_BPMSegments[i].m_iStartRow = lrintf( (iSegStart - iStartIndex) * fScale ) + iStartIndex;
	//}
	////MODIFICADO POR MI
	//for ( unsigned i = 0; i < m_TickSegments.size(); i++ )
	//{
	//	const int iSegStart = m_TickSegments[i].m_fStartBeat;
	//	if( iSegStart < iStartIndex )
	//		continue;
	//	else if( iSegStart > iEndIndex )
	//		m_TickSegments[i].m_fStartBeat += lrintf( (iEndIndex - iStartIndex) * (fScale - 1) );
	//	else
	//		m_TickSegments[i].m_fStartBeat = lrintf( (iSegStart - iStartIndex) * fScale ) + iStartIndex;
	//}
	////----------------------

	//for( unsigned i = 0; i < m_StopSegments.size(); i++ )
	//{
	//	const int iSegStartRow = m_StopSegments[i].m_iStartRow;
	//	if( iSegStartRow < iStartIndex )
	//		continue;
	//	else if( iSegStartRow > iEndIndex )
	//		m_StopSegments[i].m_iStartRow += lrintf((iEndIndex - iStartIndex) * (fScale - 1));
	//	else
	//		m_StopSegments[i].m_iStartRow = lrintf((iSegStartRow - iStartIndex) * fScale) + iStartIndex;
	//}
}

void TimingData::InsertRows( int iStartRow, int iRowsToAdd )
{
	/*for (unsigned i = 0; i < NUM_TimingSegmentType; i++)
	{
		vector<TimingSegment *> &segs = this->allTimingSegments[i];
		for (unsigned j = 0; j < segs.size(); j++)
		{
			TimingSegment *seg = segs[j];
			if (seg->GetRow() < iStartRow)
				continue;
			seg->SetRow(seg->GetRow() + iRowsToAdd);
		}
	}*/
	for( unsigned i = 0; i < m_BPMSegments.size(); i++ )
	{
		if( m_BPMSegments[i].m_iStartRow < iStartRow )
			continue;
		m_BPMSegments[i].m_iStartRow = m_BPMSegments[i].m_iStartRow + iRowsToAdd;
	}
	for( unsigned i = 0; i < m_StopSegments.size(); i++ )
	{
		if( m_StopSegments[i].m_iStartRow < iStartRow )
			continue;
		m_StopSegments[i].m_iStartRow = m_StopSegments[i].m_iStartRow + iRowsToAdd;
	}
	for( unsigned i = 0; i < m_SpeedSegments.size(); i++ )
	{
		if( m_SpeedSegments[i].m_iStartRow < iStartRow )
			continue;
		m_SpeedSegments[i].m_iStartRow = m_SpeedSegments[i].m_iStartRow + iRowsToAdd;
	}
	for( unsigned i = 0; i < m_ComboSegments.size(); i++ )
	{
		if( m_ComboSegments[i].m_fStartBeat < NoteRowToBeat(iStartRow) )
			continue;
		m_ComboSegments[i].m_fStartBeat = m_ComboSegments[i].m_fStartBeat + NoteRowToBeat(iRowsToAdd);
	}
	for( unsigned i = 0; i < m_FakeSegments.size(); i++ )
	{
		if( m_FakeSegments[i].m_fStartBeat < NoteRowToBeat(iStartRow) )
			continue;
		m_FakeSegments[i].m_fStartBeat = m_FakeSegments[i].m_fStartBeat + NoteRowToBeat(iRowsToAdd);
	}
	for( unsigned i = 0; i < m_WarpSegments.size(); i++ )
	{
		if( m_WarpSegments[i].m_fStartBeat < NoteRowToBeat(iStartRow) )
			continue;
		m_WarpSegments[i].m_fStartBeat = m_WarpSegments[i].m_fStartBeat + NoteRowToBeat(iRowsToAdd);
	}
	for( unsigned i = 0; i < m_TickSegments.size(); i++ )
	{
		if( m_TickSegments[i].m_fStartBeat < NoteRowToBeat(iStartRow) )
			continue;
		m_TickSegments[i].m_fStartBeat = m_TickSegments[i].m_fStartBeat + NoteRowToBeat(iRowsToAdd);
	}
	for( unsigned i = 0; i < m_NoteSkinSegments.size(); i++ )
	{
		if( m_NoteSkinSegments[i].m_iStartRow < iStartRow )
			continue;
		m_NoteSkinSegments[i].m_iStartRow = m_NoteSkinSegments[i].m_iStartRow + iRowsToAdd;
	}
	for( unsigned i = 0; i < m_ScrollSegments.size(); i++ )
	{
		if( m_ScrollSegments[i].m_iStartRow < iStartRow )
			continue;
		m_ScrollSegments[i].m_iStartRow = m_ScrollSegments[i].m_iStartRow + iRowsToAdd;
	}

	if( iStartRow == 0 )
	{
		/* If we're shifting up at the beginning, we just shifted up the first
		 * BPMSegment. That segment must always begin at 0. */
		vector<BPMSegment> &bpms = m_BPMSegments;
		ASSERT_M( bpms.size() > 0, "There must be at least one BPM Segment in the chart!" );
		bpms[0].m_iStartRow = 0;
	}
}

/* Delete BPMChanges and StopSegments in [iStartRow,iRowsToDelete), and shift down. */
void TimingData::DeleteRows( int iStartRow, int iRowsToDelete )
{
	/* Remember the BPM at the end of the region being deleted. */
	float fNewBPM = this->GetBPMAtBeat( NoteRowToBeat(iStartRow+iRowsToDelete) );

	/* We're moving rows up. Delete any BPM changes and stops in the region
	 * being deleted. */
	//for (unsigned i = 0; i < NUM_TimingSegmentType; i++)
	//{
	//	vector<TimingSegment *> &segs = this->allTimingSegments[i];
	//	for (unsigned j = 0; j < segs.size(); j++)
	//	{
	//		TimingSegment *seg = segs[j];
	//		// Before deleted region:
	//		if (seg->GetRow() < iStartRow)
	//			continue;
	//		
	//		// Inside deleted region:
	//		if (seg->GetRow() < iStartRow + iRowsToDelete)
	//		{
	//			segs.erase(segs.begin()+j, segs.begin()+j+1);
	//			--j;
	//			continue;
	//		}
	//		
	//		// After deleted regions:
	//		seg->SetRow(seg->GetRow() - iRowsToDelete);
	//	}
	//}

	for( unsigned i = 0; i < m_BPMSegments.size(); i++ )
	{
		if( m_BPMSegments[i].m_iStartRow < iStartRow )
			continue;

		// Inside deleted region:
		if (m_BPMSegments[i].m_iStartRow < iStartRow + iRowsToDelete)
		{
			m_BPMSegments.erase(m_BPMSegments.begin()+i, m_BPMSegments.begin()+i+1);
			--i;
			continue;
		}	
		m_BPMSegments[i].m_iStartRow = m_BPMSegments[i].m_iStartRow - iRowsToDelete;
	}
	for( unsigned i = 0; i < m_StopSegments.size(); i++ )
	{
		if( m_StopSegments[i].m_iStartRow < iStartRow )
			continue;
		// Inside deleted region:
		if (m_StopSegments[i].m_iStartRow < iStartRow + iRowsToDelete)
		{
			m_StopSegments.erase(m_StopSegments.begin()+i, m_StopSegments.begin()+i+1);
			--i;
			continue;
		}	
		m_StopSegments[i].m_iStartRow = m_StopSegments[i].m_iStartRow - iRowsToDelete;
	}
	for( unsigned i = 0; i < m_SpeedSegments.size(); i++ )
	{
		if( m_SpeedSegments[i].m_iStartRow < iStartRow )
			continue;
		// Inside deleted region:
		if (m_SpeedSegments[i].m_iStartRow < iStartRow + iRowsToDelete)
		{
			m_SpeedSegments.erase(m_SpeedSegments.begin()+i, m_SpeedSegments.begin()+i+1);
			--i;
			continue;
		}	
		m_SpeedSegments[i].m_iStartRow = m_SpeedSegments[i].m_iStartRow - iRowsToDelete;
	}
	for( unsigned i = 0; i < m_ComboSegments.size(); i++ )
	{
		if( m_ComboSegments[i].m_fStartBeat < NoteRowToBeat(iStartRow) )
			continue;
		// Inside deleted region:
		if (m_ComboSegments[i].m_fStartBeat < NoteRowToBeat(iStartRow + iRowsToDelete))
		{
			m_ComboSegments.erase(m_ComboSegments.begin()+i, m_ComboSegments.begin()+i+1);
			--i;
			continue;
		}	
		m_ComboSegments[i].m_fStartBeat = m_ComboSegments[i].m_fStartBeat - NoteRowToBeat(iRowsToDelete);
	}
	for( unsigned i = 0; i < m_FakeSegments.size(); i++ )
	{
		if( m_FakeSegments[i].m_fStartBeat < NoteRowToBeat(iStartRow) )
			continue;
		// Inside deleted region:
		if (m_FakeSegments[i].m_fStartBeat < NoteRowToBeat(iStartRow + iRowsToDelete))
		{
			m_FakeSegments.erase(m_FakeSegments.begin()+i, m_FakeSegments.begin()+i+1);
			--i;
			continue;
		}	
		m_FakeSegments[i].m_fStartBeat = m_FakeSegments[i].m_fStartBeat - NoteRowToBeat(iRowsToDelete);
	}
	for( unsigned i = 0; i < m_WarpSegments.size(); i++ )
	{
		if( m_WarpSegments[i].m_fStartBeat < NoteRowToBeat(iStartRow) )
			continue;
		// Inside deleted region:
		if (m_WarpSegments[i].m_fStartBeat < NoteRowToBeat(iStartRow + iRowsToDelete))
		{
			m_WarpSegments.erase(m_WarpSegments.begin()+i, m_WarpSegments.begin()+i+1);
			--i;
			continue;
		}	
		m_WarpSegments[i].m_fStartBeat = m_WarpSegments[i].m_fStartBeat - NoteRowToBeat(iRowsToDelete);
	}
	for( unsigned i = 0; i < m_TickSegments.size(); i++ )
	{
		if( m_TickSegments[i].m_fStartBeat < NoteRowToBeat(iStartRow) )
			continue;
		// Inside deleted region:
		if (m_TickSegments[i].m_fStartBeat < NoteRowToBeat(iStartRow + iRowsToDelete))
		{
			m_TickSegments.erase(m_TickSegments.begin()+i, m_TickSegments.begin()+i+1);
			--i;
			continue;
		}	
		m_TickSegments[i].m_fStartBeat = m_TickSegments[i].m_fStartBeat - NoteRowToBeat(iRowsToDelete);
	}
	for( unsigned i = 0; i < m_NoteSkinSegments.size(); i++ )
	{
		if( m_NoteSkinSegments[i].m_iStartRow < iStartRow )
			continue;
		// Inside deleted region:
		if (m_NoteSkinSegments[i].m_iStartRow < iStartRow + iRowsToDelete)
		{
			m_NoteSkinSegments.erase(m_NoteSkinSegments.begin()+i, m_NoteSkinSegments.begin()+i+1);
			--i;
			continue;
		}	
		m_NoteSkinSegments[i].m_iStartRow = m_NoteSkinSegments[i].m_iStartRow - iRowsToDelete;
	}
	for( unsigned i = 0; i < m_ScrollSegments.size(); i++ )
	{
		if( m_ScrollSegments[i].m_iStartRow < iStartRow )
			continue;
		// Inside deleted region:
		if (m_ScrollSegments[i].m_iStartRow < iStartRow + iRowsToDelete)
		{
			m_ScrollSegments.erase(m_ScrollSegments.begin()+i, m_ScrollSegments.begin()+i+1);
			--i;
			continue;
		}	
		m_ScrollSegments[i].m_iStartRow = m_ScrollSegments[i].m_iStartRow - iRowsToDelete;
	}
	
	this->SetBPMAtRow( iStartRow, fNewBPM );
	///* Remember the BPM at the end of the region being deleted. */
	//float fNewBPM = this->GetBPMAtBeat( NoteRowToBeat(iStartRow+iRowsToDelete) );
	////MODIFICADO POR MI
	////POR LA SECCION DEL TICKCOUNT
	//int iNewTickcount = this->GetTickcountAtBeat( NoteRowToBeat(iStartRow+iRowsToDelete) );

	///* We're moving rows up.  Delete any BPM changes and stops in the region being
	// * deleted. */
	//for( unsigned i = 0; i < m_BPMSegments.size(); i++ )
	//{
	//	BPMSegment &bpm = m_BPMSegments[i];

	//	/* Before deleted region: */
	//	if( bpm.m_iStartRow < iStartRow )
	//		continue;

	//	/* Inside deleted region: */
	//	if( bpm.m_iStartRow < iStartRow+iRowsToDelete )
	//	{
	//		m_BPMSegments.erase( m_BPMSegments.begin()+i, m_BPMSegments.begin()+i+1 );
	//		--i;
	//		continue;
	//	}

	//	/* After deleted region: */
	//	bpm.m_iStartRow -= iRowsToDelete;
	//}

	//for( unsigned i = 0; i < m_StopSegments.size(); i++ )
	//{
	//	StopSegment &stop = m_StopSegments[i];

	//	/* Before deleted region: */
	//	if( stop.m_iStartRow < iStartRow )
	//		continue;

	//	/* Inside deleted region: */
	//	if( stop.m_iStartRow < iStartRow+iRowsToDelete )
	//	{
	//		m_StopSegments.erase( m_StopSegments.begin()+i, m_StopSegments.begin()+i+1 );
	//		--i;
	//		continue;
	//	}

	//	/* After deleted region: */
	//	stop.m_iStartRow -= iRowsToDelete;
	//}

	////SECCION DEL TICKCOUNT
	////MODIFICADO POR MI
	//for( unsigned i = 0; i < m_TickSegments.size(); i++ )
	//{
	//	TickSegment &tickcount = m_TickSegments[i];

	//	/* Before deleted region: */
	//	if( tickcount.m_fStartBeat < iStartRow )
	//		continue;

	//	/* Inside deleted region: */
	//	if( tickcount.m_fStartBeat < iStartRow+iRowsToDelete )
	//	{
	//		m_TickSegments.erase( m_TickSegments.begin()+i, m_TickSegments.begin()+i+1 );
	//		--i;
	//		continue;
	//	}

	//	/* After deleted region: */
	//	tickcount.m_fStartBeat -= iRowsToDelete;
	//}

	//this->SetBPMAtRow( iStartRow, fNewBPM );
	//this->SetTickcountAtBeat( NoteRowToBeat(iStartRow), iNewTickcount );//MOD POR MI
}

bool TimingData::DoesLabelExist( RString sLabel ) const
{
	const vector<LabelSegment> &labels = m_LabelSegments;
	for (unsigned i = 0; i < labels.size(); i++)
	{
		if (labels[i].m_sLabel == sLabel)
			return true;
	}
	return false;
}

bool TimingData::HasBpmChanges() const
{
	return m_BPMSegments.size()>1;
}

bool TimingData::HasStops() const
{
	return m_StopSegments.size()>0;
}
//MODIFICADO POR MI
bool TimingData::HasTickChanges() const
{
	return m_TickSegments.size() > 1;
}
//-----------------------
//MODIFICADO POR MI
bool TimingData::HasArrowSpacingChanges() const
{
	return m_ArrowSpacingSegments.size() > 1;
}

bool TimingData::HasComboFactorChanges() const
{
	return m_ComboSegments.size() > 1;
}

bool TimingData::HasNoteSkinChanges() const
{
	return m_NoteSkinSegments.size() >= 1;
}

void TimingData::GetAllNoteSkinUsed( vector<RString> &sAddTo ) const
{
	for( unsigned i = 0; i < m_NoteSkinSegments.size(); i++ )
	{
		RString ns = m_NoteSkinSegments[i].m_sNoteSkin;
		Trim(ns);
		sAddTo.push_back( ns );
	}
}
//-----------------------

void TimingData::NoteRowToMeasureAndBeat( int iNoteRow, int &iMeasureIndexOut, int &iBeatIndexOut, int &iRowsRemainder ) const
{
	iMeasureIndexOut = 0;

	FOREACH_CONST( TimeSignatureSegment, m_vTimeSignatureSegments, iter )
	{
		vector<TimeSignatureSegment>::const_iterator next = iter;
		next++;
		int iSegmentEndRow = (next == m_vTimeSignatureSegments.end()) ? INT_MAX : next->m_iStartRow;
	
		int iRowsPerMeasureThisSegment = iter->GetNoteRowsPerMeasure();

		if( iNoteRow >= iter->m_iStartRow )
		{
			// iNoteRow lands in this segment
			int iNumRowsThisSegment = iNoteRow - iter->m_iStartRow;
			int iNumMeasuresThisSegment = (iNumRowsThisSegment) / iRowsPerMeasureThisSegment;	// don't round up
			iMeasureIndexOut += iNumMeasuresThisSegment;
			iBeatIndexOut = iNumRowsThisSegment / iRowsPerMeasureThisSegment;
			iRowsRemainder = iNumRowsThisSegment % iRowsPerMeasureThisSegment;
			return;
		}
		else
		{
			// iNoteRow lands after this segment
			int iNumRowsThisSegment = iSegmentEndRow - iter->m_iStartRow;
			int iNumMeasuresThisSegment = (iNumRowsThisSegment + iRowsPerMeasureThisSegment - 1) / iRowsPerMeasureThisSegment;	// round up
			iMeasureIndexOut += iNumMeasuresThisSegment;
		}
	}

	ASSERT(0);
	return;
}

/*
 * (c) 2001-2004 Chris Danford, Glenn Maynard
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
