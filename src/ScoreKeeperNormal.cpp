#include "global.h"
#include "ScoreKeeperNormal.h"
#include "GameState.h"
#include "PrefsManager.h"
#include "GamePreferences.h"
#include "Steps.h"
#include "ScreenManager.h"
#include "GameState.h"
#include "Course.h"
#include "SongManager.h"
#include "NoteDataUtil.h"
#include "NoteData.h"
#include "RageLog.h"
#include "StageStats.h"
#include "ProfileManager.h"
#include "NetworkSyncManager.h"
#include "PlayerState.h"
#include "Style.h"
#include "song.h"
#include "NoteDataWithScoring.h"

//Intentaré que el scoring new sea scoring type PIU
//tomando como referencia el code del sm 3.9+


void PercentScoreWeightInit( size_t /*ScoreEvent*/ i, RString &sNameOut, int &defaultValueOut )
{
	sNameOut = "PercentScoreWeight" + ScoreEventToString( (ScoreEvent)i );
	switch( i )
	{
	default:		ASSERT(0);
	case SE_W1:		defaultValueOut = 3;	break;
	case SE_W2:		defaultValueOut = 2;	break;
	case SE_W3:		defaultValueOut = 1;	break;
	case SE_W4:		defaultValueOut = 0;	break;
	case SE_W5:		defaultValueOut = 0;	break;
	case SE_Miss:		defaultValueOut = 0;	break;
	case SE_HitMine:	defaultValueOut = 0;	break;
	case SE_CheckpointHit:	defaultValueOut = 3;	break;//modificado por mi //default 0
	case SE_CheckpointMiss:	defaultValueOut = 0;	break;
	case SE_Held:		defaultValueOut = 3;	break;
	case SE_LetGo:		defaultValueOut = 0;	break;
	}
}

void GradeWeightInit( size_t /*ScoreEvent*/ i, RString &sNameOut, int &defaultValueOut )
{
	sNameOut = "GradeWeight" + ScoreEventToString( (ScoreEvent)i );
	switch( i )
	{
	default:		ASSERT(0);
	case SE_W1:		defaultValueOut = 2;	break;
	case SE_W2:		defaultValueOut = 2;	break;
	case SE_W3:		defaultValueOut = 1;	break;
	case SE_W4:		defaultValueOut = 0;	break;
	case SE_W5:		defaultValueOut = -4;	break;
	case SE_Miss:		defaultValueOut = -8;	break;
	case SE_HitMine:	defaultValueOut = 0;	break;
	case SE_CheckpointHit:	defaultValueOut = 2;	break;//modificado por mi//default 0
	case SE_CheckpointMiss:	defaultValueOut = -8;	break;//default 0
	case SE_Held:		defaultValueOut = 6;	break;
	case SE_LetGo:		defaultValueOut = 0;	break;
	}
}

static Preference1D<int> g_iPercentScoreWeight( PercentScoreWeightInit, NUM_ScoreEvent );
static Preference1D<int> g_iGradeWeight( GradeWeightInit, NUM_ScoreEvent );

ScoreKeeperNormal::ScoreKeeperNormal( PlayerState *pPlayerState, PlayerStageStats *pPlayerStageStats ):
	ScoreKeeper(pPlayerState, pPlayerStageStats)
{
}

void ScoreKeeperNormal::Load(
		const vector<Song*>& apSongs,
		const vector<Steps*>& apSteps,
		const vector<AttackArray> &asModifiers )
{
	m_apSteps = apSteps;
	ASSERT( apSongs.size() == apSteps.size() );
	ASSERT( apSongs.size() == asModifiers.size() );

	/* True if a jump is one to combo, false if combo is purely based on tap count. */
	m_ComboIsPerRow.Load( "Gameplay", "ComboIsPerRow" );
	m_MinScoreToContinueCombo.Load( "Gameplay", "MinScoreToContinueCombo" );
	m_MinScoreToMaintainCombo.Load( "Gameplay", "MinScoreToMaintainCombo" );

	//
	// Fill in STATSMAN->m_CurStageStats, calculate multiplier
	//
	int iTotalPossibleDancePoints = 0;
	int iTotalPossibleGradePoints = 0;

	m_iPassedHolds = 0;
	
	for( unsigned i=0; i<apSteps.size(); i++ )
	{
		Song* pSong = apSongs[i];
		ASSERT( pSong );
		Steps* pSteps = apSteps[i];
		ASSERT( pSteps );
		const AttackArray &aa = asModifiers[i];
		NoteData ndTemp;
		
		pSteps->GetNoteData( ndTemp );

		/* We might have been given lots of songs; don't keep them in memory uncompressed. */
		pSteps->Compress();

		const Style* pStyle = GAMESTATE->GetCurrentStyle();
		NoteData nd;
		pStyle->GetTransformedNoteDataForStyle( m_pPlayerState->m_PlayerNumber, ndTemp, nd );

		/* Compute RadarValues before applying any user-selected mods.  Apply
		 * Course mods and count them in the "pre" RadarValues because they're
		 * forced and not chosen by the user.
		 */
		NoteDataUtil::TransformNoteData( nd, aa, pSteps->m_StepsType, pSong );
		RadarValues rvPre;
		NoteDataUtil::CalculateRadarValues( nd, pSong->m_fMusicLengthSeconds, rvPre );

		/* Apply user transforms to find out how the notes will really look. 
		 *
		 * XXX: This is brittle: if we end up combining mods for a song differently
		 * than ScreenGameplay, we'll end up with the wrong data.  We should probably
		 * have eg. GAMESTATE->GetOptionsForCourse(po,so,pn) to get options based on
		 * the last call to StoreSelectedOptions and the modifiers list, but that'd
		 * mean moving the queues in ScreenGameplay to GameState ... */
		NoteDataUtil::TransformNoteData( nd, m_pPlayerState->m_PlayerOptions.GetStage(), pSteps->m_StepsType );
		RadarValues rvPost;
		NoteDataUtil::CalculateRadarValues( nd, pSong->m_fMusicLengthSeconds, rvPost );
		 
		iTotalPossibleDancePoints += this->GetPossibleDancePoints( rvPre, rvPost );
		iTotalPossibleGradePoints += this->GetPossibleGradePoints( rvPre, rvPost );
	}

	m_pPlayerStageStats->m_iPossibleDancePoints = iTotalPossibleDancePoints;
	m_pPlayerStageStats->m_iPossibleGradePoints = iTotalPossibleGradePoints;

	m_iScoreRemainder = 0;
	m_iCurToastyCombo = 0; 
	m_iMaxScoreSoFar = 0;
	m_iPointBonus = 0;
	m_iNumTapsAndHolds = 0;
	m_bIsLastSongInCourse = false;

	Message msg( "ScoreChanged" );
	msg.SetParam( "PlayerNumber", m_pPlayerState->m_PlayerNumber );
	msg.SetParam( "MultiPlayer", m_pPlayerState->m_mp );
	MESSAGEMAN->Broadcast( msg );

	memset( m_ComboBonusFactor, 0, sizeof(m_ComboBonusFactor) );
	switch( PREFSMAN->m_ScoringType )
	{
	case SCORING_NEW:
		m_iRoundTo = 1;
		break;
	case SCORING_OLD:
		m_iRoundTo = 5;
		if (!GAMESTATE->IsCourseMode())
		{
			m_ComboBonusFactor[TNS_W1] = 55;
			m_ComboBonusFactor[TNS_W2] = 55;
			m_ComboBonusFactor[TNS_W3] = 33;
		}
		break;
	DEFAULT_FAIL( int(PREFSMAN->m_ScoringType) );
	}

}

void ScoreKeeperNormal::OnNextSong( int iSongInCourseIndex, const Steps* pSteps, const NoteData* pNoteData )
{
/*
  Note on NONSTOP Mode scoring

  Nonstop mode requires the player to play 4 songs in succession, with the total maximum possible score for
  the four song set being 100,000,000. This comes from the sum of the four stages' maximum possible scores,
  which, regardless of song or difficulty is: 

  10,000,000 for the first song
  20,000,000 for the second song
  30,000,000 for the third song
  40,000,000 for the fourth song

  We extend this to work with nonstop courses of any length.

  We also keep track of this scoring type in endless, with 100mil per iteration
  of all songs, though this score isn't actually seen anywhere right now.
*/
	//
	// Calculate the score multiplier
	//
	m_iMaxPossiblePoints = 0;
	if( GAMESTATE->IsCourseMode() )
	{
		const int numSongsInCourse = m_apSteps.size();
		ASSERT( numSongsInCourse != 0 );

		const int iIndex = iSongInCourseIndex % numSongsInCourse;
		m_bIsLastSongInCourse = (iIndex+1 == numSongsInCourse);

		if( numSongsInCourse < 10 )
		{
			const int courseMult = (numSongsInCourse * (numSongsInCourse + 1)) / 2;
			ASSERT(courseMult >= 0);

			m_iMaxPossiblePoints = (100000000 * (iIndex+1)) / courseMult;
		}
		else
		{
			/* When we have lots of songs, the scale above biases too much: in a
			 * course with 50 songs, the first song is worth 80k, the last 4mil, which
			 * is too much of a difference.
			 *
			 * With this, each song in a 50-song course will be worth 2mil. */
			m_iMaxPossiblePoints = 100000000 / numSongsInCourse;
		}
	}
	else
	{
		const int iMeter = clamp( pSteps->GetMeter(), 1, 10 );

		// long ver and marathon ver songs have higher max possible scores
		int iLengthMultiplier = GameState::GetNumStagesMultiplierForSong( GAMESTATE->m_pCurSong );
		switch( PREFSMAN->m_ScoringType )
		{
		case SCORING_NEW:
			//MODIFICADO POR MI******************************
			//m_iMaxPossiblePoints = iMeter * 10000000 * iLengthMultiplier;
			m_iMaxPossiblePoints = 0; //pump score style no lo necesita
			break;
		case SCORING_OLD:
			m_iMaxPossiblePoints = (iMeter * iLengthMultiplier + 1) * 5000000;
			break;
		DEFAULT_FAIL( int(PREFSMAN->m_ScoringType) );
		}
	}
	ASSERT( m_iMaxPossiblePoints >= 0 );
	m_iMaxScoreSoFar += m_iMaxPossiblePoints;

	//*************MODIFICADO POR MI***********************
	m_iNumTapsAndHolds = pNoteData->GetNumRowsWithTapOrHoldHead() + pNoteData->GetNumHoldNotes();
		//+ pNoteData->GetNumRolls();


	m_iPointBonus = m_iMaxPossiblePoints;
	m_pPlayerStageStats->m_iMaxScore = m_iMaxScoreSoFar;

	/* MercifulBeginner shouldn't clamp weights in course mode, even if a beginner song
	 * is in a course, since that makes PlayerStageStats::GetGrade hard. */
	m_bIsBeginner = pSteps->GetDifficulty() == Difficulty_Beginner && !GAMESTATE->IsCourseMode();

	ASSERT( m_iPointBonus >= 0 );

	m_iTapNotesHit = 0;
}

static int GetScore(int p, int Z, int S, int n)
{
	/* There's a problem with the scoring system described below.  Z/S is truncated
	 * to an int.  However, in some cases we can end up with very small base scores.
	 * Each song in a 50-song nonstop course will be worth 2mil, which is a base of
	 * 200k; Z/S will end up being zero.
	 *
	 * If we rearrange the equation to (p*Z*n) / S, this problem goes away.
	 * (To do that, we need to either use 64-bit ints or rearrange it a little
	 * more and use floats, since p*Z*n won't fit a 32-bit int.)  However, this
	 * changes the scoring rules slightly.
	 */

#if 0
	/* This is the actual method described below. */
	return p * (Z / S) * n;
#elif 1
	/* This doesn't round down Z/S. */
	return int(int64_t(p) * n * Z / S);
#else
	/* This also doesn't round down Z/S. Use this if you don't have 64-bit ints. */
	return int(p * n * (float(Z) / S));
#endif

}

void ScoreKeeperNormal::AddTapScore( TapNoteScore tns )
{
}

void ScoreKeeperNormal::AddHoldScore( HoldNoteScore hns, int iRow, int iNumTapsInRow  )
{
	#if 0
	if( hns == HNS_Held )
		AddScoreInternal( TNS_W1, iRow, iNumTapsInRow );
	else if ( hns == HNS_LetGo )
		AddScoreInternal( TNS_Miss, iRow, iNumTapsInRow ); // required for subtractive score display to work properly
	#endif
}

void ScoreKeeperNormal::AddTapRowScore( TapNoteScore score, int iRow, int iNumTapsInRow )
{
	AddScoreInternal( score, iRow, iNumTapsInRow );
}

extern ThemeMetric<bool> PENALIZE_TAP_SCORE_NONE;
void ScoreKeeperNormal::HandleTapScoreNone()
{
	if( PENALIZE_TAP_SCORE_NONE )
	{
		m_pPlayerStageStats->m_iCurCombo = 0;

		if( m_pPlayerState->m_PlayerNumber != PLAYER_INVALID )
			MESSAGEMAN->Broadcast( enum_add2(Message_CurrentComboChangedP1,m_pPlayerState->m_PlayerNumber) );

		//AddScoreInternal( TNS_Miss );
	}


	// TODO: networking code
}

void ScoreKeeperNormal::AddScoreInternal( TapNoteScore score, int iRow, int iNumTapsInRow )
{
	int &iScore = m_pPlayerStageStats->m_iScore;
	int &iCombo = m_pPlayerStageStats->m_iCurCombo;
	float p = 0;	// score multiplier 

	/*
		Reference: http://www.ajworld.net/piuscore.html
		Fiesta EX / Fiesta

		O score total Ã© a soma do score de cada passo, mais bÃ´nus ao final da mÃºsica por tirar S (ou SS).

		Score de cada passo: (score base + bÃ´nus de combo + bÃ´nus de seta mÃºltipla) x multiplicador de nÃ­vel x multiplicador de Double

		Score base:

		Perfect: 1000 pontos;
		Great: 500 pontos;
		Good: 100 pontos;
		Bad: -200 pontos;
		Miss: -500 pontos;
		Miss em long note: -300 pontos.

		BÃ´nus de combo:
		Para cada passo com julgamento Perfect ou Great Ã© adicionado um bÃ´nus, dependendo do combo alcanÃ§ado naquele passo:
		AtÃ© 50 de combo: +0;
		A partir de 51 de combo: +1000 pontos.
		Note que o Good nÃ£o recebe esse bÃ´nus.

		BÃ´nus de seta mÃºltipla:
		Para cada passo com julgamento Perfect ou Great que tenha 3 ou mais setas (triplo, quÃ¡druplo etc.), Ã© adicionado um bÃ´nus de (quantidade de setas â€“ 2) * 1000

		Ou seja, para o triplo hÃ¡ um bÃ´nus de 1000, quÃ¡druplo tem bÃ´nus de 2000, quÃ­ntuplo tem bÃ´nus de 3000 etc.

		Multiplicador de nÃ­vel
		Caso o nÃ­vel da mÃºsica seja maior do que 10, a pontuaÃ§Ã£o do passo Ã© multiplicada por (nÃ­vel / 10).
		Ou seja, em uma mÃºsica nÃ­vel 23, o score final de cada passo Ã© multiplicado por 2,3 (inclusive para goods, bads e misses).

		Multiplicador de Double
		Caso a mÃºsica seja um Double, a pontuaÃ§Ã£o de cada passo Ã© multiplicada por 1,2, inclusive para goods, bads e misses.

		BÃ´nus de S e SS
		Ao final da mÃºsica, Ã© adicionado um bÃ´nus Ã  pontuaÃ§Ã£o total da mÃºsica caso o jogador tire S ou SS:
		S prateado: +100000 pontos;
		S dourado: +150000 pontos;
		SS (full perfect): +300000 pontos.
		ObservaÃ§Ãµes:
		A pontuaÃ§Ã£o nÃ£o pode cair abaixo de 0 -- ou seja, caso a pessoa dÃª um miss no primeiro passo da mÃºsica, por exemplo, ela fica com 0 de pontuaÃ§Ã£o apÃ³s aquele passo, e nÃ£o com -500.
		Caso a pontuaÃ§Ã£o final da mÃºsica nÃ£o dÃª um mÃºltiplo de 100, arredondar para baixo atÃ© o mÃºltiplo de 100 mais prÃ³ximo.
	*/

	switch( score )
	{
	case TNS_W1:	p = +1000;			break;
	case TNS_W2:	p = +1000;			break;
	case TNS_W3:	p = +500;			break;
	case TNS_W4:	p = +100;			break;
	case TNS_W5:	p = -200;			break;
	case TNS_Miss:	p = -500;			break;
	case TNS_CheckpointHit:	ASSERT(0);	break;
	case TNS_CheckpointMiss:p = -300;	break;
	default:		p = 0;				break;
	}

	m_iTapNotesHit++;

	if( score == TNS_W1 || score == TNS_W2 || score == TNS_W3 )
	{
		if( iCombo > 50 )
			p += 1000;

		if( iNumTapsInRow >= 3 )
		{
			p += ((iNumTapsInRow-2)*1000);
		}
	}
	int iMeter = GAMESTATE->m_pCurSteps[m_pPlayerState->m_PlayerNumber]->GetMeter();
	if( iMeter > 10 )
	{
		p *= ( iMeter / 10 );
	}

	if( GAMESTATE->m_pCurSteps[m_pPlayerState->m_PlayerNumber]->m_StepsType == STEPS_TYPE_PUMP_DOUBLE )
		p *= 1.2f;

	//combo bonus
	/*if( iCombo < 50 )
	{
		if( iNumTapsInRow == 3 )
			p += 500;
		else if( iNumTapsInRow >= 4 )
			p += 1000;
	} 
	else
	{
		if( iNumTapsInRow == 1 || iNumTapsInRow == 2 )
			p += 1000;
		else if( iNumTapsInRow == 3 )
			p += 2000;
		else if( iNumTapsInRow >= 4 )
			p += 3000;
	}*/

	////es correcto esto?
	//#if 0
	//if( m_pPlayerState )
	//{
	//	switch( score )
	//	{
	//	case TNS_W1:
	//	case TNS_W2:
	//	case TNS_W3:
	//	case TNS_W4:
	//		p *= m_pPlayerState->m_iComboFactor;
	//		break;
	//	case TNS_Miss:
	//	case TNS_W5:
	//		p *= m_pPlayerState->m_TimingState.GetMissComboFactorAtBeat( m_pPlayerState->m_fSongBeat );
	//	default:
	//		break;
	//	}		
	//}
	//#endif

	iScore += p;
	
	if (iScore < 0)
		iScore = 0;
	
	ASSERT( iScore >= 0 );
}

void ScoreKeeperNormal::HandleTapScore( const TapNote &tn )
{
	TapNoteScore tns = tn.result.tns;

	if( tn.type == TapNote::mine )
	{
		if( tns == TNS_HitMine )
		{
			if( !m_pPlayerStageStats->m_bFailed )
				m_pPlayerStageStats->m_iActualDancePoints += TapNoteScoreToDancePoints( TNS_HitMine );
			m_pPlayerStageStats->m_iTapNoteScores[TNS_HitMine] += 1;
		}

		NSMAN->ReportScore( 
			m_pPlayerState->m_PlayerNumber, 
			tns,
			m_pPlayerStageStats->m_iScore,
			m_pPlayerStageStats->m_iCurCombo,
			tn.result.fTapNoteOffset //not would be 0
		);
		Message msg( "ScoreChanged" );
		msg.SetParam( "PlayerNumber", m_pPlayerState->m_PlayerNumber );
		msg.SetParam( "MultiPlayer", m_pPlayerState->m_mp );
		MESSAGEMAN->Broadcast( msg );

	}

	if( tn.type == TapNote::heart )
	{
		if( tns == TNS_Heart )
			m_pPlayerStageStats->m_iTapNoteScores[TNS_Heart] += 1;
	}

	if( tn.type == TapNote::hidden )
	{
		if( tns == TNS_Hidden )
			m_pPlayerStageStats->m_iTapNoteScores[TNS_Hidden] += 1;
	}

	if( tn.type == TapNote::potion )
	{
		if( tns == TNS_Potion )
			m_pPlayerStageStats->m_iTapNoteScores[TNS_Potion] += 1;
	}

	if( tn.type == TapNote::item1x || tn.type == TapNote::item2x || tn.type == TapNote::item3x || 
		tn.type == TapNote::item4x || tn.type == TapNote::item8x )
	{
		if( tns == TNS_Item )
			m_pPlayerStageStats->m_iTapNoteScores[TNS_Item] += 1;
	}

	AddTapScore( tns );
}

void ScoreKeeperNormal::HandleHoldCheckpointScore( const NoteData &nd, int iRow, int iNumHoldsHeldThisRow, int iNumHoldsMissedThisRow )
{
	//HandleTapNoteScoreInternal( iNumHoldsMissedThisRow == 0? TNS_CheckpointHit:TNS_CheckpointMiss, TNS_W1 );
	//**************MODIFICADO POR MI***********************
	//CAMBIA EL COMBO QUE NO SE VE POR COMBO QUE SIRVE PARA EL SCORE FINAL
	HandleTapNoteScoreInternal( iNumHoldsMissedThisRow == 0? TNS_W1:TNS_Miss, TNS_W1 );
	HandleComboInternal( iNumHoldsHeldThisRow, 0, iNumHoldsMissedThisRow );

	AddScoreInternal( iNumHoldsMissedThisRow == 0? TNS_W1:TNS_CheckpointMiss, iRow, iNumHoldsMissedThisRow == 0? iNumHoldsHeldThisRow:iNumHoldsMissedThisRow );
	/* comentar para el release */
	NSMAN->ReportScore( 
			m_pPlayerState->m_PlayerNumber, 
			iNumHoldsMissedThisRow == 0? TNS_W1:TNS_Miss,
			m_pPlayerStageStats->m_iScore,
			m_pPlayerStageStats->m_iCurCombo,
			0 /* checkpoint 0 offset */ 
		);
}

void ScoreKeeperNormal::HandleTapNoteScoreInternal( TapNoteScore tns, TapNoteScore maximum )
{
	int combo = m_pPlayerState->m_TimingState.GetComboFactorAtBeat(m_pPlayerState->m_fSongBeat);
	int misscombo = m_pPlayerState->m_TimingState.GetMissComboFactorAtBeat(m_pPlayerState->m_fSongBeat);

	// Update dance points.
	if( !m_pPlayerStageStats->m_bFailed )
		m_pPlayerStageStats->m_iActualDancePoints += TapNoteScoreToDancePoints( tns );

	if (tns >= m_MinScoreToContinueCombo)
	{
		m_pPlayerStageStats->m_iTapNoteScores[tns] += combo;
	}
	else if (tns < m_MinScoreToMaintainCombo)
	{
		m_pPlayerStageStats->m_iTapNoteScores[tns] += misscombo;
	}
	else
	{	
		m_pPlayerStageStats->m_iTapNoteScores[tns] += 1;
	}

	// increment the current total possible dance score
	m_pPlayerStageStats->m_iCurPossibleDancePoints += TapNoteScoreToDancePoints( maximum );
}

void ScoreKeeperNormal::HandleComboInternal( int iNumHitContinueCombo, int iNumHitMaintainCombo, int iNumBreakCombo )
{
	//si presionamos un hold de 2 o más, miss
	if( iNumHitContinueCombo > 0 && iNumBreakCombo > 0 )
		iNumHitContinueCombo = 0;

	//
	// Regular combo
	//
	const bool bSeparately = m_pPlayerState->m_TimingState.GetCountSepAtBeat( m_pPlayerState->m_fSongBeat );
	//if( m_ComboIsPerRow )
	if( !bSeparately )
	{
		iNumHitContinueCombo = min( iNumHitContinueCombo, 1 );
		iNumHitMaintainCombo = min( iNumHitMaintainCombo, 1 );
		iNumBreakCombo = min( iNumBreakCombo, 1 );
	}

	int iOldMissCombo = m_pPlayerStageStats->m_iCurMissCombo;//misscombo
	int iCurMissCombo;

	if( iNumHitContinueCombo > 0 || iNumHitMaintainCombo > 0 )
	{
		m_pPlayerStageStats->m_iCurMissCombo = 0;
	}

	//MODIFICADO POR MI, combo factor, quitar el uso del cursong->...
	const int iComboFactor = m_pPlayerState->m_TimingState.GetComboFactorAtBeat(m_pPlayerState->m_fSongBeat);
	const int iMissCombo = m_pPlayerState->m_TimingState.GetMissComboFactorAtBeat(m_pPlayerState->m_fSongBeat);

	//cada vez que pisamos un tap, volvemos a 0 los holds?
	if( iNumBreakCombo == 0 )
	{
		m_pPlayerStageStats->m_iCurCombo += iNumHitContinueCombo * iComboFactor;

		//si los holds que pasaron fueron > 0 asignamos los holds
		if(m_iPassedHolds > 0)
		{
			m_pPlayerStageStats->m_iTapNoteScores[TNS_W1] += m_iPassedHolds;

			m_pPlayerStageStats->m_iCurCombo += m_iPassedHolds;
			m_iPassedHolds = 0;
		}
	}
	else
	{
		m_pPlayerStageStats->m_iCurCombo = 0;
		m_pPlayerStageStats->m_iCurMissCombo += iNumBreakCombo * iMissCombo;

		if(m_iPassedHolds > 0)
		{
			m_pPlayerStageStats->m_iTapNoteScores[TNS_Miss] += m_iPassedHolds;

			m_pPlayerStageStats->m_iCurMissCombo += m_iPassedHolds;
			m_iPassedHolds = 0;
		}
	}

	iCurMissCombo = m_pPlayerStageStats->m_iCurMissCombo;

	if( iCurMissCombo > iOldMissCombo )//el combo sigue aumentado, revisar si es >=
		if( m_pPlayerStageStats->m_iCurMaxMissCombo < m_pPlayerStageStats->m_iCurMissCombo )//revisa si es el combo más alto que hemos tenido
			m_pPlayerStageStats->m_iCurMaxMissCombo = m_pPlayerStageStats->m_iCurMissCombo;
}

void ScoreKeeperNormal::GetRowCounts( const NoteData &nd, int iRow,
				      int &iNumHitContinueCombo, int &iNumHitMaintainCombo,
				      int &iNumBreakCombo )
{
	PlayerNumber pn = m_pPlayerState->m_PlayerNumber;
	iNumHitContinueCombo = iNumHitMaintainCombo = iNumBreakCombo = 0;
	for( int track = 0; track < nd.GetNumTracks(); ++track )
	{
		const TapNote &tn = nd.GetTapNote( track, iRow );

		if( tn.bFakeHold )
			continue;

		if( !m_pPlayerState->m_TimingState.IsJudgableAtRow(iRow) )
			continue;
	
		if( tn.pn != PLAYER_INVALID && tn.pn != pn )
			continue;
		if( tn.type != TapNote::tap && tn.type != TapNote::hold_head && tn.type != TapNote::sudden )
			continue;
		TapNoteScore tns = tn.result.tns;
		if( tns >= m_MinScoreToContinueCombo )
			++iNumHitContinueCombo;
		else if( tns >= m_MinScoreToMaintainCombo )
			++iNumHitMaintainCombo;
		else			
			++iNumBreakCombo;
	}

}

void ScoreKeeperNormal::HandleRowComboInternal( TapNoteScore tns, int iNumTapsInRow, int iRow )
{
	const bool bSeparately = m_pPlayerState->m_TimingState.GetCountSepAtBeat( m_pPlayerState->m_fSongBeat );
	//if( m_ComboIsPerRow )
	if( !bSeparately )
	{
		iNumTapsInRow = min( iNumTapsInRow, 1);
	}
	TimingData td = m_pPlayerState->m_TimingState;
	if ( tns >= m_MinScoreToContinueCombo )
	{
		m_pPlayerStageStats->m_iCurMissCombo = 0;
		int multiplier = ( iRow == -1 ? 1 : td.GetComboFactorSegmentAtBeat( NoteRowToBeat(iRow) ).m_iComboFactor );
		m_pPlayerStageStats->m_iCurCombo += iNumTapsInRow * multiplier;

		//TODO: Testear esto
		// if(m_iPassedHolds > 0)
		// {
		// 	m_iPassedHolds = 0;
		// }
	}
	else if ( tns < m_MinScoreToMaintainCombo )
	{
		m_pPlayerStageStats->m_iCurCombo = 0;

		if( tns == TNS_Miss )
		{
			int multiplier = ( iRow == -1 ? 1 : td.GetComboFactorSegmentAtBeat( NoteRowToBeat(iRow) ).m_iMissComboFactor);
			m_pPlayerStageStats->m_iCurMissCombo += ( iNumTapsInRow ) * multiplier;

			//TODO: Testear esto
		// if(m_iPassedHolds > 0)
		// {
		// 	m_iPassedHolds = 0;
		// }
		}
		else
		{
			m_pPlayerStageStats->m_iCurMissCombo = 0;
			m_pPlayerStageStats->m_iCurCombo = 0;
		}
	}
	else
	{
		m_pPlayerStageStats->m_iCurMissCombo = 0;
		//m_pPlayerStageStats->m_iCurCombo = 0;

		//TODO: Testear esto
		// if(m_iPassedHolds > 0)
		// {
		// 	m_iPassedHolds = 0;
		// }
	}
}

void ScoreKeeperNormal::HandleTapRowScore( const NoteData &nd, int iRow )
{
	int iNumHitContinueCombo, iNumHitMaintainCombo, iNumBreakCombo;
	GetRowCounts( nd, iRow, iNumHitContinueCombo, iNumHitMaintainCombo, iNumBreakCombo );

	int iNumTapsInRow = iNumHitContinueCombo + iNumHitMaintainCombo + iNumBreakCombo;
	if( iNumTapsInRow <= 0 )
		return;

	TapNoteScore scoreOfLastTap = NoteDataWithScoring::LastTapNoteWithResult( nd, iRow, m_pPlayerState->m_PlayerNumber ).result.tns;
	HandleTapNoteScoreInternal( scoreOfLastTap, TNS_W1 );


	//HandleComboInternal( iNumHitContinueCombo, iNumHitMaintainCombo, iNumBreakCombo );
	//revisar el misscombo y el combo factor
	HandleRowComboInternal(scoreOfLastTap,iNumTapsInRow,iRow);

	if( m_pPlayerState->m_PlayerNumber != PLAYER_INVALID )
		MESSAGEMAN->Broadcast( enum_add2(Message_CurrentComboChangedP1,m_pPlayerState->m_PlayerNumber) );


	//
	//	Aqui controlamos el scoring system tipo nx
	//	debemos enviar tambien iNumTapsInRow
	//
	AddTapRowScore( scoreOfLastTap, iRow, iNumTapsInRow );		// only score once per row

	//
	// handle combo logic
	//
#ifndef DEBUG
	if( GamePreferences::m_AutoPlay != PC_HUMAN && !GAMESTATE->m_bDemonstrationOrJukebox )	// cheaters never prosper
	{
		m_iCurToastyCombo = 0;
		return;
	}
#endif //DEBUG

	//
	// Toasty combo
	//
	switch( scoreOfLastTap )
	{
	case TNS_W1:
	case TNS_W2:
	//*********MODIFICADO POR MI**********************
	//IMPORTANTE: Trata QE LA TOASTY SALGA CON LAS LONG NOTES (no implementado)
	//************************************************
	case TNS_CheckpointHit:

		m_iCurToastyCombo += iNumTapsInRow;

		if( m_iCurToastyCombo >= 250 &&
			m_iCurToastyCombo - iNumTapsInRow < 250 &&
			!GAMESTATE->m_bDemonstrationOrJukebox )
		{
			SCREENMAN->PostMessageToTopScreen( SM_PlayToasty, 0 );

			// TODO: keep a pointer to the Profile.  Don't index with m_PlayerNumber
			PROFILEMAN->IncrementToastiesCount( m_pPlayerState->m_PlayerNumber );
		}
		break;
	default:
		m_iCurToastyCombo = 0;
		break;
	}


	// TODO: Remove indexing with PlayerNumber
	PlayerNumber pn = m_pPlayerState->m_PlayerNumber;
	float offset = NoteDataWithScoring::LastTapNoteWithResult( nd, iRow, pn ).result.fTapNoteOffset;
	NSMAN->ReportScore( pn, scoreOfLastTap,
			m_pPlayerStageStats->m_iScore,
			m_pPlayerStageStats->m_iCurCombo, offset /* not 0 */ );
	Message msg( "ScoreChanged" );
	msg.SetParam( "PlayerNumber", m_pPlayerState->m_PlayerNumber );
	msg.SetParam( "MultiPlayer", m_pPlayerState->m_mp );
	MESSAGEMAN->Broadcast( msg );
}


void ScoreKeeperNormal::HandleHoldScore( const TapNote &tn )
{
	HoldNoteScore holdScore = tn.HoldResult.hns;

	// update dance points totals
	if( !m_pPlayerStageStats->m_bFailed )
		m_pPlayerStageStats->m_iActualDancePoints += HoldNoteScoreToDancePoints( holdScore );
	m_pPlayerStageStats->m_iCurPossibleDancePoints += HoldNoteScoreToDancePoints( HNS_Held );


	m_pPlayerStageStats->m_iHoldNoteScores[holdScore] += m_pPlayerState->m_iComboFactor;

	// increment the current total possible dance score

	m_pPlayerStageStats->m_iCurPossibleDancePoints += HoldNoteScoreToDancePoints( HNS_Held );

#if 0
	AddHoldScore( holdScore );
#endif

	// TODO: Remove indexing with PlayerNumber
	PlayerNumber pn = m_pPlayerState->m_PlayerNumber;
	NSMAN->ReportScore(
		pn, 
		holdScore+TapNoteScore_Invalid, 
		m_pPlayerStageStats->m_iScore,
		m_pPlayerStageStats->m_iCurCombo,
		tn.result.fTapNoteOffset );
	Message msg( "ScoreChanged" );
	msg.SetParam( "PlayerNumber", m_pPlayerState->m_PlayerNumber );
	msg.SetParam( "MultiPlayer", m_pPlayerState->m_mp );
	MESSAGEMAN->Broadcast( msg );
}


int ScoreKeeperNormal::GetPossibleDancePoints( const RadarValues& radars )
{
	/* Note that, if W1 timing is disabled or not active (not course mode),
	 * W2 will be used instead. */

	int NumTaps = int(radars[RadarCategory_TapsAndHolds]);
	int NumHolds = int(radars[RadarCategory_Holds]); 
	//int NumRolls = int(radars[RadarCategory_Rolls]); 
	return 
		NumTaps*TapNoteScoreToDancePoints(TNS_W1, false)+
		NumHolds*HoldNoteScoreToDancePoints(HNS_Held, false);
		
		//*************MODIFICADO POR MI************************
 		//+NumRolls*HoldNoteScoreToDancePoints(HNS_Held, false);
}

int ScoreKeeperNormal::GetPossibleDancePoints( const RadarValues& fOriginalRadars, const RadarValues& fPostRadars )
{
	/*
	 * The logic here is that if you use a modifier that adds notes, you should have to
	 * hit the new notes to get a high grade.  However, if you use one that removes notes,
	 * they should simply be counted as misses. */
	return max( 
		GetPossibleDancePoints(fOriginalRadars),
		GetPossibleDancePoints(fPostRadars) );
}

int ScoreKeeperNormal::GetPossibleGradePoints( const RadarValues& radars )
{
	/* Note that, if W1 timing is disabled or not active (not course mode),
	 * W2 will be used instead. */

	int NumTaps = int(radars[RadarCategory_TapsAndHolds]);
	int NumHolds = int(radars[RadarCategory_Holds]); 
	//int NumRolls = int(radars[RadarCategory_Rolls]); 
	return 
		NumTaps*TapNoteScoreToGradePoints(TNS_W1, false)+
		NumHolds*HoldNoteScoreToGradePoints(HNS_Held, false);
		//MODIFICADO POR MI***************************************
		// +
		//NumRolls*HoldNoteScoreToGradePoints(HNS_Held, false);
}

int ScoreKeeperNormal::GetPossibleGradePoints( const RadarValues& fOriginalRadars, const RadarValues& fPostRadars )
{
	/*
	 * The logic here is that if you use a modifier that adds notes, you should have to
	 * hit the new notes to get a high grade.  However, if you use one that removes notes,
	 * they should simply be counted as misses. */
	return max( 
		GetPossibleGradePoints(fOriginalRadars),
		GetPossibleGradePoints(fPostRadars) );
}

int ScoreKeeperNormal::TapNoteScoreToDancePoints( TapNoteScore tns ) const
{
	return TapNoteScoreToDancePoints( tns, m_bIsBeginner );
}

int ScoreKeeperNormal::HoldNoteScoreToDancePoints( HoldNoteScore hns ) const
{
	return HoldNoteScoreToDancePoints( hns, m_bIsBeginner );
}

int ScoreKeeperNormal::TapNoteScoreToGradePoints( TapNoteScore tns ) const
{
	return TapNoteScoreToGradePoints( tns, m_bIsBeginner );
}
int ScoreKeeperNormal::HoldNoteScoreToGradePoints( HoldNoteScore hns ) const
{
	return HoldNoteScoreToGradePoints( hns, m_bIsBeginner );
}

int ScoreKeeperNormal::TapNoteScoreToDancePoints( TapNoteScore tns, bool bBeginner )
{
	if( !GAMESTATE->ShowW1() && tns == TNS_W1 )
		tns = TNS_W2;

	/* This is used for Oni percentage displays.  Grading values are currently in
	 * StageStats::GetGrade. */
	int iWeight = 0;
	switch( tns )
	{
	DEFAULT_FAIL( tns );
	case TNS_None:		iWeight = 0;						break;
	case TNS_Heart:		iWeight = 0;						break;
	case TNS_MissHeart:	iWeight = 0;						break;
	case TNS_Hidden:	iWeight = 0;						break;
	case TNS_MissHidden:iWeight = 0;						break;
	case TNS_Potion:	iWeight = 0;						break;
	case TNS_MissPotion:iWeight = 0;						break;
	case TNS_Item:		iWeight = 0;						break;
	case TNS_Division:	iWeight = 0;						break;
	case TNS_MissItem:	iWeight = 0;						break;
	case TNS_HitMine:	iWeight = g_iPercentScoreWeight[SE_HitMine];	break;
	case TNS_Miss:		iWeight = g_iPercentScoreWeight[SE_Miss];	break;
	case TNS_W5:		iWeight = g_iPercentScoreWeight[SE_W5];	break;
	case TNS_W4:		iWeight = g_iPercentScoreWeight[SE_W4];	break;
	case TNS_W3:		iWeight = g_iPercentScoreWeight[SE_W3];	break;
	case TNS_W2:		iWeight = g_iPercentScoreWeight[SE_W2];	break;
	case TNS_W1:		iWeight = g_iPercentScoreWeight[SE_W1];	break;
	
	
	//*****************MODIFICADO POR MI*************************
	case TNS_CheckpointHit:	iWeight = g_iPercentScoreWeight[SE_W1];	break;
	case TNS_CheckpointMiss:iWeight = g_iPercentScoreWeight[SE_Miss];	break;
	//***********************************************************
	
	
	}
	if( bBeginner && PREFSMAN->m_bMercifulBeginner )
		iWeight = max( 0, iWeight );
	return iWeight;
}

int ScoreKeeperNormal::HoldNoteScoreToDancePoints( HoldNoteScore hns, bool bBeginner )
{
	int iWeight = 0;
	switch( hns )
	{
	DEFAULT_FAIL( hns );
	case HNS_None:	iWeight = 0;						break;
	
	//*****************CAMBIADO POR MI ******************************
	case HNS_LetGo:	iWeight = g_iPercentScoreWeight[SE_Miss];	break;
	case HNS_Held:	iWeight = g_iPercentScoreWeight[SE_W1];	break;
	//*****************************************************************
	
	
	}
	if( bBeginner && PREFSMAN->m_bMercifulBeginner )
		iWeight = max( 0, iWeight );
	return iWeight;
}

int ScoreKeeperNormal::TapNoteScoreToGradePoints( TapNoteScore tns, bool bBeginner )
{
	if( !GAMESTATE->ShowW1() && tns == TNS_W1 )
		tns = TNS_W2;

	/* This is used for Oni percentage displays.  Grading values are currently in
	 * StageStats::GetGrade. */
	int iWeight = 0;
	switch( tns )
	{
	DEFAULT_FAIL( tns );
	case TNS_Heart:		iWeight = 0;						break;
	case TNS_MissHeart:	iWeight = 0;						break;
	case TNS_Hidden:	iWeight = 0;						break;
	case TNS_MissHidden:iWeight = 0;						break;
	case TNS_Potion:	iWeight = 0;						break;
	case TNS_MissPotion:iWeight = 0;						break;
	case TNS_Item:		iWeight = 0;						break;
	case TNS_MissItem:	iWeight = 0;						break;
	case TNS_None:		iWeight = 0;						break;
	case TNS_AvoidMine:	iWeight = 0;						break;
	case TNS_Division:	iWeight = 0;						break;
	case TNS_HitMine:	iWeight = g_iGradeWeight[SE_HitMine];	break;
	case TNS_Miss:		iWeight = g_iGradeWeight[SE_Miss];	break;
	case TNS_W5:		iWeight = g_iGradeWeight[SE_W5];	break;
	case TNS_W4:		iWeight = g_iGradeWeight[SE_W4];	break;
	case TNS_W3:		iWeight = g_iGradeWeight[SE_W3];	break;
	case TNS_W2:		iWeight = g_iGradeWeight[SE_W2];	break;
	case TNS_W1:		iWeight = g_iGradeWeight[SE_W1];	break;
	
	//************************MODIFICADO POR MI***************************
	case TNS_CheckpointHit:	iWeight = g_iGradeWeight[SE_W1];	break;
	case TNS_CheckpointMiss:iWeight = g_iGradeWeight[SE_Miss];	break;
	//*******************************************************************
	
	
	}
	if( bBeginner && PREFSMAN->m_bMercifulBeginner )
		iWeight = max( 0, iWeight );
	return iWeight;
}

int ScoreKeeperNormal::HoldNoteScoreToGradePoints( HoldNoteScore hns, bool bBeginner )
{
	int iWeight = 0;
	switch( hns )
	{
	DEFAULT_FAIL( hns );
	case HNS_None:	iWeight = 0;					break;
	

	//MODIFICADO POR MI**********************************************
	case HNS_LetGo:	iWeight = g_iGradeWeight[SE_Miss];	break;
	case HNS_Held:	iWeight = g_iGradeWeight[SE_W1];	break;
	//**************************************************************
	
	}
	if( bBeginner && PREFSMAN->m_bMercifulBeginner )
		iWeight = max( 0, iWeight );
	return iWeight;
}

/*
 * (c) 2001-2004 Chris Danford, Glenn Maynard
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
