#include "global.h"
#include "StationWheelItem.h"
#include "ActorUtil.h"
#include "RageLog.h"

StationWheelItemData::StationWheelItemData( WheelItemType wit, GameCommand GameCommand ):
	WheelItemBaseData(wit, "", RageColor(1,1,1,1) )
{
	m_GameCommand = GameCommand;
	m_Type = wit;
	m_sText = m_GameCommand.m_sName;
}

StationWheelItem::StationWheelItem( const StationWheelItem &cpy ):
	WheelItemBase( cpy ),
	m_sprBanner( cpy.m_sprBanner )
{
	data = NULL;
	this->AddChild( &m_sprBanner );
}

StationWheelItem::StationWheelItem( RString sType ) : WheelItemBase( sType )
{
	m_sprBanner.SetName( "Banner" );
	m_sprBanner.SetZTestMode( ZTEST_WRITE_ON_PASS );
	LOAD_ALL_COMMANDS_AND_SET_XY_AND_ON_COMMAND( m_sprBanner );
	m_sprBanner.Load( THEME->GetPathG( "Station", "wheelitem" ) );
	this->AddChild( &m_sprBanner );

	data = NULL;
}

void StationWheelItem::LoadFromWheelItemData( const WheelItemBaseData *pWIBD, int iIndex, bool bHastFocus )
{
	const StationWheelItemData *pWID = dynamic_cast<const StationWheelItemData*>( pWIBD );

	ASSERT( pWID != NULL );
	data = pWID;

	ASSERT( data );

	m_sprBanner.Load( THEME->GetPathG( "Station", data->m_sText ) );
	m_sprBanner.SetVisible( true );
	//this->AddChild( &m_sprBanner );
}

StationWheelItem::~StationWheelItem(void)
{
}

/*
 * (c) 2009 "v1toko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */