/* ScreenStage - Shows the stage and mission mods - Plus! */

#ifndef ScreenStage_H
#define ScreenStage_H

#include "Screen.h"
#include "Transition.h"
#include "ActorUtil.h"
#include "ThemeMetric.h"
#include "Sprite.h"
#include "MenuTimer.h"
#include "InputEventPlus.h"
#include "RageSound.h"
#include "MissionArrowMods.h"
#include "Profile.h"

class ScreenStage : public Screen
{
public:
	virtual void Init();

	~ScreenStage();
	virtual void HandleScreenMessage( const ScreenMessage SM );
	virtual void Update( float fDeltaTime );
	virtual void MenuBack( const InputEventPlus &input );
	//modificado por mi, confirmacion en mode mission
	virtual void MenuStart( const InputEventPlus &input );
	virtual void Input( const InputEventPlus &input );
	virtual int GetMileageCost();
	bool IsArrowModsVisible() { return m_bIsUsingProfile && m_bMissionNotAllow && m_bIsMissionMode; }

	//
	// Lua
	//
	virtual void PushSelf( lua_State *L );

private:
	ThemeMetric<bool>	ALLOW_BACK;
	//segundo hasta que carga la mission!
	ThemeMetric<int>	MISSION_SECONDS;

	Transition		m_In, m_Out, m_Cancel;
	AutoActor		m_sprOverlay;
	//para confirmacion en missionmode
	bool			m_bSelected;
	bool			m_bReady;
	bool			m_bIsUsingProfile;
	bool			m_bIsMissionMode;
	bool			m_bMissionNotAllow;//permite o no, mostrar las arrowmods
	Profile*		m_pProfile;
	int				m_iMileageCostTotal;
	//Sprite			m_Confirm;
	//Sprite			m_ConfirmMessage;
	MenuTimer			*m_MissionTimer;
	MissionArrowMods	m_ArrowMods;
	MissionArrowMods	m_LifeMods;
	MissionArrowMods	m_UnableMods;
	MissionArrowMods	m_LotteryMods;
	RageSound			soundConfirm;
};

#endif

/*
 * (c) 2001-2004 Chris Danford
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
