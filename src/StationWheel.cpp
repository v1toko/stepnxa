#include "global.h"
#include "StationWheel.h"
#include "RageTimer.h"
#include "RageLog.h"

StationWheel::StationWheel(void)
{
}

StationWheelItem *StationWheel::MakeItem()
{
	return new StationWheelItem;
}

void StationWheel::Load( RString sType, vector<GameCommand> vGameCommands ) 
{
	WheelBase::Load( sType );

	m_aGameCommands = vGameCommands;
}

void StationWheel::BeginScreen()
{
	RageTimer timer;
	RString times;

	/* Build all of the wheel item data.  Do this after selecting
	 * the extra stage, so it knows to always display it. */
	BuildStationWheelItemDatas( m_StationWheelItemDatas );
	times += ssprintf( "%.3f ", timer.GetDeltaTime() );

	LOG->Trace( "StationWheel sorting took: %s", times.c_str() );

	WheelBase::BeginScreen();

	m_ScrollBar.SetVisible( false );

	FillWheelBaseData();

	// rebuild the WheelItems that appear on screen
	RebuildWheelItems();
}

void StationWheel::BuildStationWheelItemDatas( vector<StationWheelItemData *> &arrayStationWheelItems )
{
	arrayStationWheelItems.clear();	// clear out the previous wheel items 
	//vector<RString> vsNames;
	//split( MODE_MENU_CHOICE_NAMES, ",", vsNames );
	for( unsigned i=0; i<m_aGameCommands.size(); ++i )
	{
		GameCommand gc = m_aGameCommands[i];

		//LOG->Trace( "Cargando GC: %s", gc.m_sName.c_str() );

		StationWheelItemData wid( TYPE_GENERIC, gc );
		

		/*if( !wid.m_pGameCommand->IsPlayable() )
			continue;*/

		arrayStationWheelItems.push_back( new StationWheelItemData(wid) );
	}

	CHECKPOINT;
}

void StationWheel::ChangeMusic( int iDist )
{
	m_iSelection += iDist;
	wrap( m_iSelection, m_CurWheelItemData.size() );

	//FillWheelBaseData();

	RebuildWheelItems( iDist );

	m_fPositionOffsetFromSelection += iDist;

	//SCREENMAN->PostMessageToTopScreen( SM_SongChanged, 0 );

	/* If we're moving automatically, don't play this; it'll be called in Update. */
	if(!IsMoving())
		m_soundChangeMusic.Play();
}

void StationWheel::FillWheelBaseData()
{
	const WheelItemBaseData *old = NULL;
	if( !m_CurWheelItemData.empty() )
		old = GetCurStationWheelItemData(m_iSelection);

	m_CurWheelItemData.clear();
	vector<StationWheelItemData *> &from = m_StationWheelItemDatas;
	m_CurWheelItemData.reserve( from.size() );
	for( unsigned i = 0; i < from.size(); i++ )
	{
		StationWheelItemData &d = *from[i];

		//LOG->Trace( "Cargando %s", d.m_pGameCommand->m_sName.c_str() );
		//ASSERT( d.m_GameCommand );

		if( !d.m_GameCommand.IsPlayable() )
			continue;
		//	ASSERT_M(0, "No hay styles para elegir en la ruleta" );

		m_CurWheelItemData.push_back(&d);
	}
}

StationWheel::~StationWheel(void)
{
	FOREACH( StationWheelItemData*, m_StationWheelItemDatas, i )
		delete *i;
}

/*
 * (c) 2009 "v1toko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */