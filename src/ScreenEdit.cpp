#include <utility>
#include <float.h>
#include "global.h"
#include "ScreenEdit.h"
#include "ActorUtil.h"
#include "AdjustSync.h"
#include "BackgroundUtil.h"
#include "CommonMetrics.h"
#include "Foreach.h"
#include "GameManager.h"
#include "GamePreferences.h"
#include "GameSoundManager.h"
#include "GameState.h"
#include "InputEventPlus.h"
#include "InputMapper.h"
#include "LocalizedString.h"
#include "NoteDataUtil.h"
#include "NoteSkinManager.h"
#include "NotesWriterSM.h"
#include "PrefsManager.h"
#include "RageSoundManager.h"
#include "RageInput.h"
#include "RageLog.h"
#include "ScreenDimensions.h"
#include "ScreenManager.h"
#include "ScreenMiniMenu.h"
#include "ScreenPrompt.h"
#include "ScreenSaveSync.h"
#include "ScreenTextEntry.h"
#include "SongManager.h"
#include "SongUtil.h"
#include "StepsUtil.h"
#include "Style.h"
#include "ThemeManager.h"
#include "ThemeMetric.h"
#include "Game.h"
#include "RageSoundReader.h"

static Preference<float> g_iDefaultRecordLength( "DefaultRecordLength", 4 );
static Preference<bool> g_bEditorShowBGChangesPlay( "EditorShowBGChangesPlay", false );

//
// Defines specific to ScreenEdit
//
const float RECORD_HOLD_SECONDS = 0.3f;

#define PLAYER_X		(SCREEN_CENTER_X)
#define PLAYER_Y		(SCREEN_CENTER_Y)
#define PLAYER_HEIGHT		(360)
#define PLAYER_Y_STANDARD	(PLAYER_Y-PLAYER_HEIGHT/2)

#define EDIT_X			(SCREEN_CENTER_X)
#define EDIT_Y			(PLAYER_Y)

#define RECORD_X		(SCREEN_CENTER_X)
#define RECORD_Y		(SCREEN_CENTER_Y)

#define PLAY_RECORD_HELP_TEXT	THEME->GetString(m_sName,"PlayRecordHelpText")
#define EDIT_HELP_TEXT		THEME->GetString(m_sName,"EditHelpText")

// FIXME: Remove hard-coded beat values and instead look at the time signature in the song.
static const int BEATS_PER_MEASURE = 4;
static const int ROWS_PER_MEASURE = ROWS_PER_BEAT * BEATS_PER_MEASURE;

AutoScreenMessage( SM_UpdateTextInfo )
AutoScreenMessage( SM_BackFromMainMenu )
AutoScreenMessage( SM_BackFromAreaMenu )
AutoScreenMessage( SM_BackFromStepsInformation )
AutoScreenMessage( SM_BackFromOptions )
AutoScreenMessage( SM_BackFromSongInformation )
AutoScreenMessage( SM_BackFromBGChange )
AutoScreenMessage( SM_BackFromInsertTapAttack )
AutoScreenMessage( SM_BackFromInsertTapAttackPlayerOptions )
AutoScreenMessage( SM_BackFromInsertCourseAttack )
AutoScreenMessage( SM_BackFromInsertCourseAttackPlayerOptions )
AutoScreenMessage( SM_BackFromCourseModeMenu )
AutoScreenMessage( SM_DoRevertToLastSave )
AutoScreenMessage( SM_DoRevertFromDisk )
//AutoScreenMessage( SM_BackFromBPMChange )
AutoScreenMessage( SM_BackFromOffsetChange )
AutoScreenMessage( SM_DoSaveAndExit )
AutoScreenMessage( SM_DoExit )
AutoScreenMessage( SM_SaveSuccessful );
AutoScreenMessage( SM_SaveFailed );
AutoScreenMessage( SM_BackFromTimingDataInformation );
AutoScreenMessage( SM_BackFromBPMChange );
AutoScreenMessage( SM_BackFromStopChange );
AutoScreenMessage( SM_BackFromDelayChange );
//AutoScreenMessage( SM_BackFromTimeSignatureChange );
AutoScreenMessage( SM_BackFromTickcountChange );
AutoScreenMessage( SM_BackFromComboChange );
AutoScreenMessage( SM_BackFromNoteSkinChange );
AutoScreenMessage( SM_BackFromWarpChange );
AutoScreenMessage( SM_BackFromCountSepChange );
AutoScreenMessage( SM_BackFromReqHoldHeadChange );
AutoScreenMessage( SM_BackFromSpeedPercentChange );
AutoScreenMessage( SM_BackFromSpeedWaitChange );
AutoScreenMessage( SM_BackFromSpeedModeChange );
AutoScreenMessage( SM_BackFromScrollChange );
AutoScreenMessage( SM_BackFromFakeChange );
AutoScreenMessage( SM_BackFromLabelChange );
AutoScreenMessage( SM_BackFromInsertStepAttackPlayerOptions );
AutoScreenMessage( SM_BackFromAlterMenu );
AutoScreenMessage( SM_BackFromChangeSpecialTag );
AutoScreenMessage( SM_BackFromWriteAttack );
AutoScreenMessage( SM_WriteGrooveLabel );
AutoScreenMessage( SM_WriteWildLabel );
AutoScreenMessage( SM_WriteBrainShowerTap );
AutoScreenMessage( SM_WriteTapDivisionLabel );

static const char *EditStateNames[] = {
	"Edit",
	"Record",
	"RecordPaused",
	"Playing"
};
XToString( EditState );

#if defined(XBOX)
void ScreenEdit::InitEditMappings()
{
	/* XXX: fill this in */
}
#else
void ScreenEdit::InitEditMappings()
{
	m_EditMappingsDeviceInput.Clear();

	//
	// Common mappings:
	//
	switch( EDIT_MODE.GetValue() )
	{
	case EditMode_Practice:
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_SCROLL_PREV_MEASURE][0] = DeviceInput(DEVICE_KEYBOARD, KEY_UP);
		m_EditMappingsMenuButton.button[EDIT_BUTTON_SCROLL_PREV_MEASURE][0] = MENU_BUTTON_UP;
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_SCROLL_NEXT_MEASURE][0] = DeviceInput(DEVICE_KEYBOARD, KEY_DOWN);
		m_EditMappingsMenuButton.button[EDIT_BUTTON_SCROLL_NEXT_MEASURE][0] = MENU_BUTTON_DOWN;
		break;
	default:
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_SCROLL_UP_LINE][0] = DeviceInput(DEVICE_KEYBOARD, KEY_UP);
		m_EditMappingsMenuButton.button[EDIT_BUTTON_SCROLL_UP_LINE][0] = MENU_BUTTON_UP;
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_SCROLL_DOWN_LINE][0] = DeviceInput(DEVICE_KEYBOARD, KEY_DOWN);
		m_EditMappingsMenuButton.button[EDIT_BUTTON_SCROLL_DOWN_LINE][0] = MENU_BUTTON_DOWN;
		break;
	}

	m_EditMappingsDeviceInput.button[EDIT_BUTTON_SCROLL_UP_PAGE][0] = DeviceInput(DEVICE_KEYBOARD, KEY_PGUP);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_SCROLL_DOWN_PAGE][0] = DeviceInput(DEVICE_KEYBOARD, KEY_PGDN);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_SCROLL_HOME][0] = DeviceInput(DEVICE_KEYBOARD, KEY_HOME);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_SCROLL_END][0] = DeviceInput(DEVICE_KEYBOARD, KEY_END);
	//MODIFICADO POR MI
	//para que el tickcount pesque botones "," y "."
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_SCROLL_NEXT][0] = DeviceInput(DEVICE_KEYBOARD, KEY_PERIOD);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_SCROLL_PREV][0] = DeviceInput(DEVICE_KEYBOARD, KEY_COMMA);

	m_EditMappingsDeviceInput.button[EDIT_BUTTON_SCROLL_SELECT][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LSHIFT);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_SCROLL_SELECT][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RSHIFT);

	m_EditMappingsDeviceInput.button[EDIT_BUTTON_LAY_SELECT][0] = DeviceInput(DEVICE_KEYBOARD, KEY_SPACE);

	m_EditMappingsDeviceInput.button[EDIT_BUTTON_PLAY_FROM_START][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cp);
	m_EditMappingsDeviceInput.hold[EDIT_BUTTON_PLAY_FROM_START][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LCTRL);
	m_EditMappingsDeviceInput.hold[EDIT_BUTTON_PLAY_FROM_START][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RCTRL);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_PLAY_FROM_CURSOR][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cp);
	m_EditMappingsDeviceInput.hold[EDIT_BUTTON_PLAY_FROM_CURSOR][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LSHIFT);
	m_EditMappingsDeviceInput.hold[EDIT_BUTTON_PLAY_FROM_CURSOR][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RSHIFT);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_PLAY_SELECTION][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cp);

	//
	// EditMode-specific mappings
	//
	switch( EDIT_MODE.GetValue() )
	{
	case EditMode_Practice:
		// Left = Zoom out (1x, 2x, 4x)
		// Right = Zoom in
		m_EditMappingsDeviceInput.button   [EDIT_BUTTON_SCROLL_SPEED_DOWN][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LEFT);
		m_EditMappingsDeviceInput.button   [EDIT_BUTTON_SCROLL_SPEED_UP][0]   = DeviceInput(DEVICE_KEYBOARD, KEY_RIGHT);

		// F1 = Show help popup
		m_EditMappingsDeviceInput.button   [EDIT_BUTTON_OPEN_INPUT_HELP][0]   = DeviceInput(DEVICE_KEYBOARD, KEY_F1);

		// Esc = Show Edit Menu
		m_EditMappingsDeviceInput.button   [EDIT_BUTTON_OPEN_EDIT_MENU][0]    = DeviceInput(DEVICE_KEYBOARD, KEY_ESC);
		m_EditMappingsMenuButton.button   [EDIT_BUTTON_OPEN_EDIT_MENU][0]    = MENU_BUTTON_START;
		m_EditMappingsMenuButton.button   [EDIT_BUTTON_OPEN_EDIT_MENU][1]    = MENU_BUTTON_BACK;

		// Escape, Enter = exit play/record
		m_PlayMappingsDeviceInput.button   [EDIT_BUTTON_RETURN_TO_EDIT][0]    = DeviceInput(DEVICE_KEYBOARD, KEY_ENTER);
		m_PlayMappingsDeviceInput.button   [EDIT_BUTTON_RETURN_TO_EDIT][1]    = DeviceInput(DEVICE_KEYBOARD, KEY_ESC);
		m_PlayMappingsMenuButton.button   [EDIT_BUTTON_RETURN_TO_EDIT][1]    = MENU_BUTTON_BACK;
		return;
	case EditMode_CourseMods:
		// Left/Right = Snap to Next/Prev
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_SNAP_NEXT][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LEFT);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_SNAP_PREV][0] = DeviceInput(DEVICE_KEYBOARD, KEY_RIGHT);
		
		// v = course attack menu
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_OPEN_COURSE_ATTACK_MENU][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cv);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_ADD_COURSE_MODS][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cm);
		
		// F1 = Show help popup
		m_EditMappingsDeviceInput.button   [EDIT_BUTTON_OPEN_INPUT_HELP][0]   = DeviceInput(DEVICE_KEYBOARD, KEY_F1);
		
		// Esc = Show Edit Menu
		m_EditMappingsDeviceInput.button   [EDIT_BUTTON_OPEN_EDIT_MENU][0]    = DeviceInput(DEVICE_KEYBOARD, KEY_ESC);
		m_EditMappingsMenuButton.button   [EDIT_BUTTON_OPEN_EDIT_MENU][0]    = MENU_BUTTON_START;
		m_EditMappingsMenuButton.button   [EDIT_BUTTON_OPEN_EDIT_MENU][1]    = MENU_BUTTON_BACK;
		
		// Escape, Enter = exit play/record
		m_PlayMappingsDeviceInput.button   [EDIT_BUTTON_RETURN_TO_EDIT][0]    = DeviceInput(DEVICE_KEYBOARD, KEY_ENTER);
		m_PlayMappingsDeviceInput.button   [EDIT_BUTTON_RETURN_TO_EDIT][1]    = DeviceInput(DEVICE_KEYBOARD, KEY_ESC);
		m_PlayMappingsMenuButton.button   [EDIT_BUTTON_RETURN_TO_EDIT][0]    = MENU_BUTTON_START;
		return;
		
	case EditMode_Full:
		/* Don't allow F5/F6 in home mode.  It breaks the "delay creation until first save" logic. */
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_SHOW_TIMING_MENU][0]  = DeviceInput(DEVICE_KEYBOARD, KEY_F4);

		m_EditMappingsDeviceInput.button[EDIT_BUTTON_OPEN_PREV_STEPS][0] = DeviceInput(DEVICE_KEYBOARD, KEY_F5);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_OPEN_NEXT_STEPS][0] = DeviceInput(DEVICE_KEYBOARD, KEY_F6);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_BPM_DOWN][0] = DeviceInput(DEVICE_KEYBOARD, KEY_F7);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_BPM_UP][0] = DeviceInput(DEVICE_KEYBOARD, KEY_F8);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_STOP_DOWN][0] = DeviceInput(DEVICE_KEYBOARD, KEY_F9);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_STOP_UP][0]  = DeviceInput(DEVICE_KEYBOARD, KEY_F10);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_OFFSET_DOWN][0] = DeviceInput(DEVICE_KEYBOARD, KEY_F11);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_OFFSET_UP][0]  = DeviceInput(DEVICE_KEYBOARD, KEY_F12);

		m_EditMappingsDeviceInput.button[EDIT_BUTTON_SAMPLE_START_UP][0] = DeviceInput(DEVICE_KEYBOARD, KEY_RBRACKET);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_SAMPLE_START_DOWN][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LBRACKET);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_SAMPLE_LENGTH_UP][0] = DeviceInput(DEVICE_KEYBOARD, KEY_RBRACKET);
		m_EditMappingsDeviceInput.hold[EDIT_BUTTON_SAMPLE_LENGTH_UP][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LSHIFT);
		m_EditMappingsDeviceInput.hold[EDIT_BUTTON_SAMPLE_LENGTH_UP][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RSHIFT);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_SAMPLE_LENGTH_DOWN][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LBRACKET);
		m_EditMappingsDeviceInput.hold[EDIT_BUTTON_SAMPLE_LENGTH_DOWN][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LSHIFT);
		m_EditMappingsDeviceInput.hold[EDIT_BUTTON_SAMPLE_LENGTH_DOWN][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RSHIFT);

		m_EditMappingsDeviceInput.button[EDIT_BUTTON_PLAY_SAMPLE_MUSIC][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cm);

		m_EditMappingsDeviceInput.button[EDIT_BUTTON_OPEN_BGCHANGE_LAYER1_MENU][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cb);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_OPEN_BGCHANGE_LAYER2_MENU][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cb);
		m_EditMappingsDeviceInput.hold[EDIT_BUTTON_OPEN_BGCHANGE_LAYER2_MENU][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LSHIFT);
		m_EditMappingsDeviceInput.hold[EDIT_BUTTON_OPEN_BGCHANGE_LAYER2_MENU][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RSHIFT);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_OPEN_COURSE_MENU][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cc);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_OPEN_COURSE_ATTACK_MENU][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cv);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_OPEN_ALTER_MENU][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Ca);

		m_EditMappingsDeviceInput.button[EDIT_BUTTON_INSERT_SHIFT_PAUSES][0] = DeviceInput(DEVICE_KEYBOARD, KEY_INSERT);
		m_EditMappingsDeviceInput.hold[EDIT_BUTTON_INSERT_SHIFT_PAUSES][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LCTRL);
		m_EditMappingsDeviceInput.hold[EDIT_BUTTON_INSERT_SHIFT_PAUSES][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RCTRL);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_DELETE_SHIFT_PAUSES][0] = DeviceInput(DEVICE_KEYBOARD, KEY_DEL);
		m_EditMappingsDeviceInput.hold[EDIT_BUTTON_DELETE_SHIFT_PAUSES][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LCTRL);
		m_EditMappingsDeviceInput.hold[EDIT_BUTTON_DELETE_SHIFT_PAUSES][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RCTRL);
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_CHANGE_NOTESKIN_UP][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cy );
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_CHANGE_NOTESKIN_DOWN][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Ct );
		//Usando los botones de los timesignatures para cambiar los tapnoteskins
		//tenemos cuatro botones disponible i,u,j,k
		//i sera para disminuir el indice del noteskin, u sera para aumentar el indice
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_CHANGE_TIMESIGNATURE_NUMERATOR_UP][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Ci );
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_CHANGE_TIMESIGNATURE_NUMERATOR_DOWN][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cu );
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_CHANGE_TIMESIGNATURE_DENOMINATOR_UP][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cj );
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_CHANGE_TIMESIGNATURE_DENOMINATOR_DOWN][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Ck );
		m_EditMappingsDeviceInput.button[EDIT_BUTTON_STEP_MODE_CHANGE][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cn );

		break;
	}

	m_EditMappingsDeviceInput.button[EDIT_BUTTON_COLUMN_0][0] = DeviceInput(DEVICE_KEYBOARD, KEY_C1);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_COLUMN_1][0] = DeviceInput(DEVICE_KEYBOARD, KEY_C2);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_COLUMN_2][0] = DeviceInput(DEVICE_KEYBOARD, KEY_C3);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_COLUMN_3][0] = DeviceInput(DEVICE_KEYBOARD, KEY_C4);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_COLUMN_4][0] = DeviceInput(DEVICE_KEYBOARD, KEY_C5);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_COLUMN_5][0] = DeviceInput(DEVICE_KEYBOARD, KEY_C6);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_COLUMN_6][0] = DeviceInput(DEVICE_KEYBOARD, KEY_C7);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_COLUMN_7][0] = DeviceInput(DEVICE_KEYBOARD, KEY_C8);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_COLUMN_8][0] = DeviceInput(DEVICE_KEYBOARD, KEY_C9);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_COLUMN_9][0] = DeviceInput(DEVICE_KEYBOARD, KEY_C0);

	m_EditMappingsDeviceInput.button[EDIT_BUTTON_RIGHT_SIDE][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LALT);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_RIGHT_SIDE][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RALT);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_LAY_MINE_OR_ROLL][0]   = DeviceInput(DEVICE_KEYBOARD, KEY_LSHIFT);
	// m_EditMappingsDeviceInput.button[EDIT_BUTTON_LAY_TAP_ATTACK][0] = DeviceInput(DEVICE_KEYBOARD, KEY_RSHIFT);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_LAY_LIFT][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LCTRL);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_LAY_LIFT][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RCTRL);

	m_EditMappingsDeviceInput.button    [EDIT_BUTTON_SCROLL_SPEED_UP][0] = DeviceInput(DEVICE_KEYBOARD, KEY_UP);
	m_EditMappingsDeviceInput.hold[EDIT_BUTTON_SCROLL_SPEED_UP][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LCTRL);
	m_EditMappingsDeviceInput.hold[EDIT_BUTTON_SCROLL_SPEED_UP][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RCTRL);

	m_EditMappingsDeviceInput.button    [EDIT_BUTTON_SCROLL_SPEED_DOWN][0] = DeviceInput(DEVICE_KEYBOARD, KEY_DOWN);
	m_EditMappingsDeviceInput.hold[EDIT_BUTTON_SCROLL_SPEED_DOWN][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LCTRL);
	m_EditMappingsDeviceInput.hold[EDIT_BUTTON_SCROLL_SPEED_DOWN][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RCTRL);

	m_EditMappingsDeviceInput.button[EDIT_BUTTON_SCROLL_SELECT][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LSHIFT);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_SCROLL_SELECT][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RSHIFT);

	m_EditMappingsDeviceInput.button[EDIT_BUTTON_SNAP_NEXT][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LEFT);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_SNAP_PREV][0] = DeviceInput(DEVICE_KEYBOARD, KEY_RIGHT);

	m_EditMappingsDeviceInput.button[EDIT_BUTTON_OPEN_EDIT_MENU][0] = DeviceInput(DEVICE_KEYBOARD, KEY_ESC);
	m_EditMappingsMenuButton.button[EDIT_BUTTON_OPEN_EDIT_MENU][0] = MENU_BUTTON_START;
	m_EditMappingsMenuButton.button[EDIT_BUTTON_OPEN_EDIT_MENU][1] = MENU_BUTTON_BACK;
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_OPEN_AREA_MENU][0] = DeviceInput(DEVICE_KEYBOARD, KEY_ENTER);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_OPEN_INPUT_HELP][0] = DeviceInput(DEVICE_KEYBOARD, KEY_F1);
	
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_BAKE_RANDOM_FROM_SONG_GROUP][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cb);
	m_EditMappingsDeviceInput.hold[EDIT_BUTTON_BAKE_RANDOM_FROM_SONG_GROUP][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LALT);
	m_EditMappingsDeviceInput.hold[EDIT_BUTTON_BAKE_RANDOM_FROM_SONG_GROUP][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RALT);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_BAKE_RANDOM_FROM_SONG_GROUP_AND_GENRE][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cb);
	m_EditMappingsDeviceInput.hold[EDIT_BUTTON_BAKE_RANDOM_FROM_SONG_GROUP_AND_GENRE][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LCTRL);
	m_EditMappingsDeviceInput.hold[EDIT_BUTTON_BAKE_RANDOM_FROM_SONG_GROUP_AND_GENRE][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RCTRL);

	m_EditMappingsDeviceInput.button[EDIT_BUTTON_RECORD_SELECTION][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cr);
	m_EditMappingsDeviceInput.hold[EDIT_BUTTON_RECORD_SELECTION][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LCTRL);
	m_EditMappingsDeviceInput.hold[EDIT_BUTTON_RECORD_SELECTION][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RCTRL);

	m_EditMappingsDeviceInput.button[EDIT_BUTTON_INSERT][0] = DeviceInput(DEVICE_KEYBOARD, KEY_INSERT);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_INSERT][1] = DeviceInput(DEVICE_KEYBOARD, KEY_BACKSLASH);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_DELETE][0] = DeviceInput(DEVICE_KEYBOARD, KEY_DEL);

	m_EditMappingsDeviceInput.button[EDIT_BUTTON_ADJUST_FINE][0] = DeviceInput(DEVICE_KEYBOARD, KEY_RALT);
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_ADJUST_FINE][1] = DeviceInput(DEVICE_KEYBOARD, KEY_LALT);
	
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_UNDO][1] = DeviceInput(DEVICE_KEYBOARD, KEY_Cu);
	
	// Switch players, if it makes sense to do so.
	m_EditMappingsDeviceInput.button[EDIT_BUTTON_SWITCH_PLAYERS][0] = DeviceInput(DEVICE_KEYBOARD, KEY_SLASH);
	
	m_PlayMappingsDeviceInput.button[EDIT_BUTTON_RETURN_TO_EDIT][0] = DeviceInput(DEVICE_KEYBOARD, KEY_ESC);
	m_PlayMappingsMenuButton.button[EDIT_BUTTON_RETURN_TO_EDIT][1] = MENU_BUTTON_BACK;

	m_RecordMappingsDeviceInput.button[EDIT_BUTTON_LAY_MINE_OR_ROLL][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LSHIFT);
	m_RecordMappingsDeviceInput.button[EDIT_BUTTON_LAY_MINE_OR_ROLL][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RSHIFT);
	m_RecordMappingsDeviceInput.button[EDIT_BUTTON_LAY_LIFT][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LCTRL);
	m_RecordMappingsDeviceInput.button[EDIT_BUTTON_LAY_LIFT][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RCTRL);
	m_RecordMappingsDeviceInput.button[EDIT_BUTTON_REMOVE_NOTE][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LALT);
	m_RecordMappingsDeviceInput.button[EDIT_BUTTON_REMOVE_NOTE][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RALT);
	m_RecordMappingsDeviceInput.button[EDIT_BUTTON_RETURN_TO_EDIT][0] = DeviceInput(DEVICE_KEYBOARD, KEY_ESC);
	m_RecordMappingsMenuButton.button[EDIT_BUTTON_RETURN_TO_EDIT][1] = MENU_BUTTON_BACK;

	m_RecordPausedMappingsDeviceInput.button[EDIT_BUTTON_PLAY_SELECTION][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cp);
	m_RecordPausedMappingsDeviceInput.button[EDIT_BUTTON_RECORD_SELECTION][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cr);
	m_RecordPausedMappingsDeviceInput.hold[EDIT_BUTTON_RECORD_SELECTION][0] = DeviceInput(DEVICE_KEYBOARD, KEY_LCTRL);
	m_RecordPausedMappingsDeviceInput.hold[EDIT_BUTTON_RECORD_SELECTION][1] = DeviceInput(DEVICE_KEYBOARD, KEY_RCTRL);
	m_RecordPausedMappingsDeviceInput.button[EDIT_BUTTON_RECORD_FROM_CURSOR][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cr);
	m_RecordPausedMappingsDeviceInput.button[EDIT_BUTTON_RETURN_TO_EDIT][0] = DeviceInput(DEVICE_KEYBOARD, KEY_ESC);
	m_RecordPausedMappingsMenuButton.button[EDIT_BUTTON_RETURN_TO_EDIT][1] = MENU_BUTTON_BACK;
	m_RecordPausedMappingsDeviceInput.button[EDIT_BUTTON_UNDO][0] = DeviceInput(DEVICE_KEYBOARD, KEY_Cu);
}

#endif

/* Given a DeviceInput that was just depressed, return an active edit function. */
EditButton ScreenEdit::DeviceToEdit( const DeviceInput &DeviceI ) const
{
	ASSERT( DeviceI.IsValid() );

	const MapEditToDI *pCurrentMap = GetCurrentDeviceInputMap();

	/* First, search to see if a key that requires a modifier is pressed. */
	FOREACH_EditButton( e )
	{
		for( int slot = 0; slot < NUM_EDIT_TO_DEVICE_SLOTS; ++slot )
		{
			if( pCurrentMap->button[e][slot] == DeviceI && pCurrentMap->hold[e][0].IsValid() )
			{
				/* The button maps to this function, and has one or more shift modifiers attached. */
				for( int holdslot = 0; holdslot < NUM_EDIT_TO_DEVICE_SLOTS; ++holdslot )
				{
					DeviceInput hDI = pCurrentMap->hold[e][holdslot];
					if( hDI.IsValid() && INPUTFILTER->IsBeingPressed(hDI) )
						return e;
				}
			}
		}
	}

	/* No shifted keys matched.  See if any unshifted inputs are bound to this key. */  
	FOREACH_EditButton(e)
	{
		for( int slot = 0; slot < NUM_EDIT_TO_DEVICE_SLOTS; ++slot )
		{
			if( pCurrentMap->button[e][slot] == DeviceI && !pCurrentMap->hold[e][0].IsValid() )
			{
				/* The button maps to this function. */
				return e;
			}
		}
	}

	return EditButton_Invalid;
}

/* Given a DeviceInput that was just depressed, return an active edit function. */
EditButton ScreenEdit::MenuButtonToEditButton( GameButton MenuI ) const
{
	const MapEditButtonToMenuButton *pCurrentMap = GetCurrentMenuButtonMap();

	FOREACH_EditButton(e)
	{
		for( int slot = 0; slot < NUM_EDIT_TO_MENU_SLOTS; ++slot )
		{
			if( pCurrentMap->button[e][slot] == MenuI )
			{
				/* The button maps to this function. */
				return e;
			}
		}
	}

	return EditButton_Invalid;
}

/* If DeviceI was just pressed, return true if button is triggered.  (More than one
 * function may be mapped to a key.) */
bool ScreenEdit::EditPressed( EditButton button, const DeviceInput &DeviceI )
{
	ASSERT( DeviceI.IsValid() );

	const MapEditToDI *pCurrentMap = GetCurrentDeviceInputMap();

	/* First, search to see if a key that requires a modifier is pressed. */
	bool bPrimaryButtonPressed = false;
	for( int slot = 0; slot < NUM_EDIT_TO_DEVICE_SLOTS; ++slot )
	{
		if( pCurrentMap->button[button][slot] == DeviceI )
			bPrimaryButtonPressed = true;
	}

	if( !bPrimaryButtonPressed )
		return false;

	/* The button maps to this function.  Does the function has one or more shift modifiers attached? */
	if( !pCurrentMap->hold[button][0].IsValid() )
		return true;

	for( int holdslot = 0; holdslot < NUM_EDIT_TO_DEVICE_SLOTS; ++holdslot )
	{
		DeviceInput hDI = pCurrentMap->hold[button][holdslot];
		if( INPUTFILTER->IsBeingPressed(hDI) )
			return true;
	}

	/* No shifted keys matched. */  
	return false;
}

bool ScreenEdit::EditToDevice( EditButton button, int iSlotNum, DeviceInput &DeviceI ) const
{
	ASSERT( iSlotNum < NUM_EDIT_TO_DEVICE_SLOTS );
	const MapEditToDI *pCurrentMap = GetCurrentDeviceInputMap();
	if( pCurrentMap->button[button][iSlotNum].IsValid() )
		DeviceI = pCurrentMap->button[button][iSlotNum];
	else if( pCurrentMap->hold[button][iSlotNum].IsValid() )
		DeviceI = pCurrentMap->hold[button][iSlotNum];
	return DeviceI.IsValid();
}

bool ScreenEdit::EditIsBeingPressed( EditButton button ) const
{
	for( int slot = 0; slot < NUM_EDIT_TO_DEVICE_SLOTS; ++slot )
	{
		DeviceInput DeviceI;
		if( EditToDevice( button, slot, DeviceI ) && INPUTFILTER->IsBeingPressed(DeviceI) )
			return true;
	}

	return false;
}

const MapEditToDI *ScreenEdit::GetCurrentDeviceInputMap() const
{
	switch( m_EditState )
	{
	//DEFAULT_FAIL( m_EditState );
	case STATE_EDITING:		return &m_EditMappingsDeviceInput;
	case STATE_PLAYING:		return &m_PlayMappingsDeviceInput;
	case STATE_RECORDING:		return &m_RecordMappingsDeviceInput;
	case STATE_RECORDING_PAUSED:	return &m_RecordPausedMappingsDeviceInput;
	}
}

const MapEditButtonToMenuButton *ScreenEdit::GetCurrentMenuButtonMap() const
{
	switch( m_EditState )
	{
	//DEFAULT_FAIL( m_EditState );
	case STATE_EDITING:		return &m_EditMappingsMenuButton;
	case STATE_PLAYING:		return &m_PlayMappingsMenuButton;
	case STATE_RECORDING:		return &m_RecordMappingsMenuButton;
	case STATE_RECORDING_PAUSED:	return &m_RecordPausedMappingsMenuButton;
	}
}


static MenuDef g_EditHelp(
	"ScreenMiniMenuEditHelp"
	// fill this in dynamically
);

static MenuDef g_MainMenu(
	"ScreenMiniMenuMainMenu",
	MenuRowDef( ScreenEdit::play_selection,			"Play selection",		true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::set_selection_start,		"Set selection start",		true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::set_selection_end,		"Set selection end",		true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::edit_steps_information,		"Edit steps information",	true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::play_whole_song,		"Play whole song",		true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::play_current_beat_to_end,	"Play current beat to end",	true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::save,				"Save",				true, EditMode_Home, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::revert_to_last_save,		"Revert to last save",		true, EditMode_Home, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::revert_from_disk,		"Revert from disk",		true, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::options,			"Editor options",		true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::edit_song_info,			"Edit song info",		true, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::edit_bpm,			"Edit BPM change",		true, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::edit_stop,			"Edit stop",			true, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::play_preview_music,		"Play preview music",		true, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::exit,				"Exit editor",			true, EditMode_Practice, true, true, 0, NULL )
);

static MenuDef g_AreaMenu(
	"ScreenMiniMenuAreaMenu",
	MenuRowDef( ScreenEdit::paste_at_current_beat,	"Paste at current beat",		true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::paste_at_begin_marker,	"Paste at begin marker",		true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::insert_and_shift,	"Insert beat and shift down",		true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::delete_and_shift,	"Delete beat and shift up",		true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::shift_pauses_forward,	"Shift pauses and BPM changes down",	true, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::shift_pauses_backward,	"Shift pauses and BPM changes up",	true, EditMode_Full, true, true, 0, NULL ),
	//MenuRowDef( ScreenEdit::convert_to_pause,	"Convert selection to pause",		true, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::convert_pause_to_beat,	"Convert pause to beats",		true, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::convert_delay_to_beat,	"Convert delay to beats",		true, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef(ScreenEdit::last_second_at_beat,	"Designate last second at current beat", true, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::undo,			"Undo",					true, EditMode_Practice, true, true, 0, NULL )
);

static MenuDef g_AlterMenu(
	"ScreenMiniMenuAlterMenu",
	MenuRowDef(ScreenEdit::cut,				"Cut",					true, 
	      EditMode_Practice, true, true, 0, NULL ),
   MenuRowDef(ScreenEdit::copy,				"Copy",					true, 
	      EditMode_Practice, true, true, 0, NULL ),
   MenuRowDef(ScreenEdit::clear,			"Clear area",				true, 
	      EditMode_Practice, true, true, 0, NULL ),
   MenuRowDef(ScreenEdit::quantize,			"Quantize",				true, 
	      EditMode_Practice, true, true, 0, 
	      "4th","8th","12th","16th","24th","32nd","48th","64th","192nd"),
   MenuRowDef(ScreenEdit::turn,				"Turn",					true, 
	      EditMode_Practice, true, true, 0, "Left","Right","Mirror","Shuffle","SuperShuffle" ),
   MenuRowDef(ScreenEdit::transform,			"Transform",				true, 
	      EditMode_Practice, true, true, 0, "NoHolds","NoMines","Little","Wide",
	      "Big","Quick","Skippy","Mines","Echo","Stomp","Planted","Floored",
	      "Twister","NoJumps","NoHands","NoQuads","NoStretch" ),
   MenuRowDef(ScreenEdit::alter,			"Alter",				true, 
	      EditMode_Practice, true, true, 0, "Autogen To Fill Width","Backwards","Swap Sides",
	      "Copy Left To Right","Copy Right To Left","Clear Left","Clear Right",
	      "Collapse To One","Collapse Left","Shift Left","Shift Right" ),
   MenuRowDef(ScreenEdit::tempo,			"Tempo",				true, 
	      EditMode_Full, true, true, 0, "Compress 2x","Compress 3->2",
	      "Compress 4->3","Expand 3->4","Expand 2->3","Expand 2x" ),
   MenuRowDef(ScreenEdit::play,				"Play selection",			true, 
	      EditMode_Practice, true, true, 0, NULL ),
   MenuRowDef(ScreenEdit::record,			"Record in selection",			true, 
	      EditMode_Practice, true, true, 0, NULL ),
   MenuRowDef(ScreenEdit::preview_designation,		"Designate as Music Preview",		true,
	      EditMode_Full, true, true, 0, NULL ),
   MenuRowDef(ScreenEdit::convert_to_pause,		"Convert selection to pause",		true, 
	      EditMode_Full, true, true, 0, NULL ),
   MenuRowDef(ScreenEdit::convert_to_delay,		"Convert selection to delay",		true, 
	      EditMode_Full, true, true, 0, NULL ),
   MenuRowDef(ScreenEdit::convert_to_warp,		"Convert selection to warp",		true, 
	      EditMode_Full, true, true, 0, NULL ),
   MenuRowDef(ScreenEdit::convert_to_fake,		"Convert selection to fake",		true, 
	      EditMode_Full, true, true, 0, NULL ),
   MenuRowDef(ScreenEdit::convert_to_attack,	"Convert selection to attack",		true,
			  EditMode_Full, true, true, 0, NULL)
);

static MenuDef g_StepsInformation(
	"ScreenMiniMenuStepsInformation",
	MenuRowDef( ScreenEdit::difficulty,	"Difficulty",		true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::meter,		"Meter",		true, EditMode_Practice, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::description,	"Description",		true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::predict_meter,	"Predicted Meter",	false, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::tap_notes,	"Tap Steps",		false, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::jumps,		"Jumps",		false, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::hands,		"Hands",		false, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::quads,		"Quads",		false, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::holds,		"Holds",		false, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::mines,		"Mines",		false, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::stream,		"Stream",		false, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::voltage,	"Voltage",		false, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::air,		"Air",			false, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::freeze,		"Freeze",		false, EditMode_Full, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::chaos,		"Chaos",		false, EditMode_Full, true, true, 0, NULL )
);

static MenuDef g_SongInformation(
	"ScreenMiniMenuSongInformation",
	MenuRowDef( ScreenEdit::main_title,			"Main title",			true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::sub_title,			"Sub title",			true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::artist,				"Artist",			true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::credit,				"Credit",			true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::main_title_transliteration,	"Main title transliteration",	true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::sub_title_transliteration,	"Sub title transliteration",	true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::artist_transliteration,		"Artist transliteration",	true, EditMode_Practice, true, true, 0, NULL ),
	MenuRowDef( ScreenEdit::last_beat_hint,			"Last beat hint",		true, EditMode_Full, true, true, 0, NULL )
);

static MenuDef g_TimingDataInformation(
	"ScreenMiniMenuTimingDataInformation",
	MenuRowDef( ScreenEdit::beat_0_offset,			"Beat 0 Offset",		true, EditMode_Full, true, true, 0, NULL ),
        MenuRowDef( ScreenEdit::bpm,				"Edit BPM change",		true, EditMode_Full, true, true, 0, NULL ),
        MenuRowDef( ScreenEdit::stop,				"Edit stop",			true, EditMode_Full, true, true, 0, NULL ),
        MenuRowDef( ScreenEdit::delay,				"Edit delay",			true, EditMode_Full, true, true, 0, NULL ),
        //MenuRowDef( ScreenEdit::time_signature,		"Edit time signature",	true, EditMode_Full, true, true, 0, NULL ),
        MenuRowDef( ScreenEdit::noteskin,				"Edit noteskin",			true, EditMode_Full, true, true, 0, NULL ),
        MenuRowDef( ScreenEdit::tickcount,			"Edit tickcount",		true, EditMode_Full, true, true, 0, NULL ),
        MenuRowDef( ScreenEdit::combo,				"Edit combo",			true, EditMode_Full, true, true, 0, NULL ),
        MenuRowDef( ScreenEdit::warp,				"Edit warp",			true, EditMode_Full, true, true, 0, NULL ),
        MenuRowDef( ScreenEdit::speed_percent,			"Edit speed (percent)",		true, EditMode_Full, true, true, 0, NULL ),
        MenuRowDef( ScreenEdit::speed_wait,			"Edit speed (wait)",		true, EditMode_Full, true, true, 0, NULL ),
        MenuRowDef( ScreenEdit::speed_mode,			"Edit speed (mode)",		true, EditMode_Full, true, true, 0, "Beats", "Seconds" ),
        MenuRowDef( ScreenEdit::scroll,			"Edit scrolling factor",		true, EditMode_Full, true, true, 0, NULL ),
        MenuRowDef( ScreenEdit::fake,				"Edit fake",			true, EditMode_Full, true, true, 0, NULL ),
		MenuRowDef( ScreenEdit::label,				"Edit label",			true, EditMode_Full, true, true, 0, NULL ),
		MenuRowDef( ScreenEdit::countsep,				"Edit count sep.",			true, EditMode_Full, true, true, 0, NULL ),
		MenuRowDef( ScreenEdit::reqhold,				"Edit required hold head.",			true, EditMode_Full, true, true, 0, NULL )
        //MenuRowDef( ScreenEdit::copy_timing,		"Copy timing data",			true, EditMode_Full, true, true, 0, NULL ),
        //MenuRowDef( ScreenEdit::paste_timing,		"Paste timing data",			true, EditMode_Full, true, true, 0, NULL ),
        //MenuRowDef( ScreenEdit::erase_step_timing,		"Erase step timing",		true, EditMode_Full, true, true, 0, NULL )
);

enum { song_bganimation, song_movie, song_bitmap, global_bganimation, global_movie, global_movie_song_group, global_movie_song_group_and_genre, dynamic_random, baked_random, none };
static bool EnabledIfSet1SongBGAnimation();
static bool EnabledIfSet1SongMovie();
static bool EnabledIfSet1SongBitmap();
static bool EnabledIfSet1GlobalBGAnimation();
static bool EnabledIfSet1GlobalMovie();
static bool EnabledIfSet1GlobalMovieSongGroup();
static bool EnabledIfSet1GlobalMovieSongGroupAndGenre();
static bool EnabledIfSet2SongBGAnimation();
static bool EnabledIfSet2SongMovie();
static bool EnabledIfSet2SongBitmap();
static bool EnabledIfSet2GlobalBGAnimation();
static bool EnabledIfSet2GlobalMovie();
static bool EnabledIfSet2GlobalMovieSongGroup();
static bool EnabledIfSet2GlobalMovieSongGroupAndGenre();
static MenuDef g_BackgroundChange(
	"ScreenMiniMenuBackgroundChange",
	MenuRowDef( ScreenEdit::layer,						"Layer",				false,						EditMode_Full, true, false, 0, "" ),
	MenuRowDef( ScreenEdit::rate,						"Rate",					true,						EditMode_Full, true, false, 10, "0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%","120%","140%","160%","180%","200%" ),
	MenuRowDef( ScreenEdit::transition,					"Force Transition",			true,						EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::effect,						"Force Effect",				true,						EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::color1,						"Force Color 1",			true,						EditMode_Full, true, false, 0, "-","1,1,1,1","0.5,0.5,0.5,1","1,1,1,0.5","0,0,0,1","1,0,0,1","0,1,0,1","0,0,1,1","1,1,0,1","0,1,1,1","1,0,1,1" ),
	MenuRowDef( ScreenEdit::color2,						"Force Color 2",			true,						EditMode_Full, true, false, 0, "-","1,1,1,1","0.5,0.5,0.5,1","1,1,1,0.5","0,0,0,1","1,0,0,1","0,1,0,1","0,0,1,1","1,1,0,1","0,1,1,1","1,0,1,1" ),
	MenuRowDef( ScreenEdit::file1_type,					"File1 Type",				true,						EditMode_Full, true, true, 0, "Song BGAnimation", "Song Movie", "Song Bitmap", "Global BGAnimation", "Global Movie", "Global Movie from Song Group", "Global Movie from Song Group and Genre", "Dynamic Random", "Baked Random", "None" ),
	MenuRowDef( ScreenEdit::file1_song_bganimation,				"File1 Song BGAnimation",		EnabledIfSet1SongBGAnimation,			EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::file1_song_movie,				"File1 Song Movie",			EnabledIfSet1SongMovie,				EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::file1_song_still,				"File1 Song Still",			EnabledIfSet1SongBitmap,			EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::file1_global_bganimation,			"File1 Global BGAnimation",		EnabledIfSet1GlobalBGAnimation,			EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::file1_global_movie,				"File1 Global Movie",			EnabledIfSet1GlobalMovie,			EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::file1_global_movie_song_group,			"File1 Global Movie (Group)",		EnabledIfSet1GlobalMovieSongGroup,		EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::file1_global_movie_song_group_and_genre,	"File1 Global Movie (Group + Genre)",	EnabledIfSet1GlobalMovieSongGroupAndGenre,	EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::file2_type,					"File2 Type",				true,						EditMode_Full, true, true, 0, "Song BGAnimation", "Song Movie", "Song Bitmap", "Global BGAnimation", "Global Movie", "Global Movie from Song Group", "Global Movie from Song Group and Genre", "Dynamic Random", "Baked Random", "None" ),
	MenuRowDef( ScreenEdit::file2_song_bganimation,				"File2 Song BGAnimation",		EnabledIfSet2SongBGAnimation,			EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::file2_song_movie,				"File2 Song Movie",			EnabledIfSet2SongMovie,				EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::file2_song_still,				"File2 Song Still",			EnabledIfSet2SongBitmap,			EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::file2_global_bganimation,			"File2 Global BGAnimation",		EnabledIfSet2GlobalBGAnimation,			EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::file2_global_movie,				"File2 Global Movie",			EnabledIfSet2GlobalMovie,			EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::file2_global_movie_song_group,			"File2 Global Movie (Group)",		EnabledIfSet2GlobalMovieSongGroup,		EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::file2_global_movie_song_group_and_genre,	"File2 Global Movie (Group + Genre)",	EnabledIfSet2GlobalMovieSongGroupAndGenre,	EditMode_Full, true, false, 0, NULL ),
	MenuRowDef( ScreenEdit::delete_change,					"Remove Change",			true,						EditMode_Full, true, true, 0, NULL )
);
static bool EnabledIfSet1SongBGAnimation()		{ return ScreenMiniMenu::s_viLastAnswers[ScreenEdit::file1_type] == song_bganimation			&& !g_BackgroundChange.rows[ScreenEdit::file1_song_bganimation].choices.empty(); }
static bool EnabledIfSet1SongMovie()			{ return ScreenMiniMenu::s_viLastAnswers[ScreenEdit::file1_type] == song_movie				&& !g_BackgroundChange.rows[ScreenEdit::file1_song_movie].choices.empty(); }
static bool EnabledIfSet1SongBitmap()			{ return ScreenMiniMenu::s_viLastAnswers[ScreenEdit::file1_type] == song_bitmap				&& !g_BackgroundChange.rows[ScreenEdit::file1_song_still].choices.empty(); }
static bool EnabledIfSet1GlobalBGAnimation()		{ return ScreenMiniMenu::s_viLastAnswers[ScreenEdit::file1_type] == global_bganimation			&& !g_BackgroundChange.rows[ScreenEdit::file1_global_bganimation].choices.empty(); }
static bool EnabledIfSet1GlobalMovie()			{ return ScreenMiniMenu::s_viLastAnswers[ScreenEdit::file1_type] == global_movie			&& !g_BackgroundChange.rows[ScreenEdit::file1_global_movie].choices.empty(); }
static bool EnabledIfSet1GlobalMovieSongGroup()		{ return ScreenMiniMenu::s_viLastAnswers[ScreenEdit::file1_type] == global_movie_song_group		&& !g_BackgroundChange.rows[ScreenEdit::file1_global_movie_song_group].choices.empty(); }
static bool EnabledIfSet1GlobalMovieSongGroupAndGenre() { return ScreenMiniMenu::s_viLastAnswers[ScreenEdit::file1_type] == global_movie_song_group_and_genre	&& !g_BackgroundChange.rows[ScreenEdit::file1_global_movie_song_group_and_genre].choices.empty(); }
static bool EnabledIfSet2SongBGAnimation()		{ return ScreenMiniMenu::s_viLastAnswers[ScreenEdit::file2_type] == song_bganimation			&& !g_BackgroundChange.rows[ScreenEdit::file2_song_bganimation].choices.empty(); }
static bool EnabledIfSet2SongMovie()			{ return ScreenMiniMenu::s_viLastAnswers[ScreenEdit::file2_type] == song_movie				&& !g_BackgroundChange.rows[ScreenEdit::file2_song_movie].choices.empty(); }
static bool EnabledIfSet2SongBitmap()			{ return ScreenMiniMenu::s_viLastAnswers[ScreenEdit::file2_type] == song_bitmap				&& !g_BackgroundChange.rows[ScreenEdit::file2_song_still].choices.empty(); }
static bool EnabledIfSet2GlobalBGAnimation()		{ return ScreenMiniMenu::s_viLastAnswers[ScreenEdit::file2_type] == global_bganimation			&& !g_BackgroundChange.rows[ScreenEdit::file2_global_bganimation].choices.empty(); }
static bool EnabledIfSet2GlobalMovie()			{ return ScreenMiniMenu::s_viLastAnswers[ScreenEdit::file2_type] == global_movie			&& !g_BackgroundChange.rows[ScreenEdit::file2_global_movie].choices.empty(); }
static bool EnabledIfSet2GlobalMovieSongGroup()		{ return ScreenMiniMenu::s_viLastAnswers[ScreenEdit::file2_type] == global_movie_song_group		&& !g_BackgroundChange.rows[ScreenEdit::file2_global_movie_song_group].choices.empty(); }
static bool EnabledIfSet2GlobalMovieSongGroupAndGenre() { return ScreenMiniMenu::s_viLastAnswers[ScreenEdit::file2_type] == global_movie_song_group_and_genre	&& !g_BackgroundChange.rows[ScreenEdit::file2_global_movie_song_group_and_genre].choices.empty(); }

static RString GetOneBakedRandomFile( Song *pSong, bool bTryGenre = true )
{
	vector<RString> vsPathsOut; 
	vector<RString> vsNamesOut;
	BackgroundUtil::GetGlobalRandomMovies(
		pSong,
		"",
		vsPathsOut, 
		vsNamesOut,
		bTryGenre );
	if( !vsNamesOut.empty() )
		return vsNamesOut[RandomInt(vsNamesOut.size())];
	else
		return RString();
}

static MenuDef g_InsertTapAttack(
	"ScreenMiniMenuInsertTapAttack",
	MenuRowDef( -1, "Duration seconds",	true, EditMode_Practice, true, false, 3, "5","10","15","20","25","30","35","40","45" ),
	MenuRowDef( -1, "Set modifiers",	true, EditMode_Practice, true, true, 0, "Press Start" )
);

static MenuDef g_InsertCourseAttack(
	"ScreenMiniMenuInsertCourseAttack",
	MenuRowDef( ScreenEdit::duration,	"Duration seconds",	true, EditMode_Practice, true, false, 3, "5","10","15","20","25","30","35","40","45" ),
	MenuRowDef( ScreenEdit::set_mods,	"Set modifiers",	true, EditMode_Practice, true, true, 0, "Press Start" ),
	MenuRowDef( ScreenEdit::remove,		"Remove",		true, EditMode_Practice, true, true, 0, "Press Start" )
);

static MenuDef g_CourseMode(
	"ScreenMiniMenuCourseDisplay",
	MenuRowDef( -1, "Play mods from course",	true, EditMode_Practice, true, false, 0, NULL )
);

// HACK: need to remember the track we're inserting on so
// that we can lay the attack note after coming back from
// menus.
static int g_iLastInsertTapAttackTrack = -1;
static float g_fLastInsertAttackDurationSeconds = -1;
static float g_fLastInsertAttackPositionSeconds = -1;
static int g_iLastInsertDivisionTapTrack = -1;
static int g_iLastInsertBrainShowerTapTrack = -1;
static BackgroundLayer g_CurrentBGChangeLayer = BACKGROUND_LAYER_Invalid;

static void SetDefaultEditorNoteSkin( size_t num, RString &sNameOut, RString &defaultValueOut )
{
	sNameOut = ssprintf( "EditorNoteSkinP%d", int(num + 1) );

	// XXX: We need more supported note skins.
	switch( num )
	{
	case 0: defaultValueOut = "default"; return;
	case 1: defaultValueOut = "default"; return;
	}
	defaultValueOut = "default";
}

static Preference1D<RString> EDITOR_NOTE_SKINS( SetDefaultEditorNoteSkin, NUM_PLAYERS );

static ThemeMetric<RString> EDIT_MODIFIERS		("ScreenEdit","EditModifiers");

REGISTER_SCREEN_CLASS( ScreenEdit );

void ScreenEdit::Init()
{
	m_pSoundMusic = NULL;

	SubscribeToMessage( "Judgment" );

	ASSERT( GAMESTATE->m_pCurSong );
	ASSERT( GAMESTATE->m_pCurSteps[PLAYER_1] );

	EDIT_MODE.Load( m_sName, "EditMode" );
	ScreenWithMenuElements::Init();

	InitEditMappings();

	// save the originals for reverting later
	CopyToLastSave();

	m_CurrentAction = MAIN_MENU_CHOICE_INVALID;
	if( GAMESTATE->m_pCurSteps[0]->m_StepsType == STEPS_TYPE_DANCE_ROUTINE )
		m_InputPlayerNumber = PLAYER_1;
	else
		m_InputPlayerNumber = PLAYER_INVALID;

	FOREACH_PlayerNumber( p )
		GAMESTATE->m_bSideIsJoined[p] = false;
	GAMESTATE->m_bSideIsJoined[PLAYER_1] = true;

	m_pSong = GAMESTATE->m_pCurSong;
	m_pSteps = GAMESTATE->m_pCurSteps[PLAYER_1];
	m_bReturnToRecordMenuAfterPlay = false;
	m_fBeatToReturnTo = 0;

	GAMESTATE->m_bGameplayLeadIn.Set( true );
	GAMESTATE->m_EditMode = EDIT_MODE.GetValue();

	m_pPlayerStageStats = new PlayerStageStats();
	m_pPlayerStateEdit = new PlayerState();
	m_pPlayerStateEdit->m_TimingState = m_pSong->m_Timing;

	{
		TimingData &t = m_pPlayerStateEdit->m_TimingState;
		if( t.m_TickSegments.size() < 1 )
		{
			TickSegment new_tick_seg;
			new_tick_seg.m_fStartBeat = 0.0f;
			new_tick_seg.m_iTickcount = 6;
			t.AddTickSegment( new_tick_seg );
		}
		if( t.m_ArrowSpacingSegments.size() < 1 )
		{
			ArrowSpacingSegment new_ArrowSpacing_seg;
			new_ArrowSpacing_seg.m_iStartRow = 0;
			new_ArrowSpacing_seg.m_fArrowSpacing = 64.0f;
			t.AddArrowSpacingSegment( new_ArrowSpacing_seg );
		}
		if( t.m_ComboSegments.size() < 1 )
		{
			ComboSegment new_Combo_seg;
			new_Combo_seg.m_fStartBeat = 0.0f;
			new_Combo_seg.m_iComboFactor = 1;
			new_Combo_seg.m_iMissComboFactor = 1;
			t.AddComboSegment( new_Combo_seg );
		}
		if( t.m_NoteSkinSegments.size() < 1 )
		{
			RString sNS = "default";
			Trim(sNS);
			NoteSkinSegment new_NoteSkin_seg;
			new_NoteSkin_seg.m_iStartRow = 0;
			new_NoteSkin_seg.m_sNoteSkin = sNS;
			t.AddNoteSkinSegment( new_NoteSkin_seg );
		}
		if( t.m_ScrollSegments.size() < 1 )
		{					
			ScrollSegment new_Scroll_seg;
			new_Scroll_seg.m_iStartRow = 0.0f;
			new_Scroll_seg.m_fRatio = 1.0f;
			t.AddScrollSegment( new_Scroll_seg );
		}
		if( t.m_vTimeSignatureSegments.size() < 1 )
		{
			TimeSignatureSegment new_Time_seg;
			new_Time_seg.m_iStartRow = 0;
			new_Time_seg.m_iNumerator = 4;
			new_Time_seg.m_iDenominator = 4;
			t.AddTimeSignatureSegment( new_Time_seg );
		}
		if( t.m_SpeedSegments.size() < 1 )
		{
			SpeedSegment new_Speed_seg;
			new_Speed_seg.m_iStartRow = 0;
			new_Speed_seg.m_fRatio = 1;
			new_Speed_seg.m_fWait = 0;
			new_Speed_seg.m_iUnit = 0;
			t.AddSpeedSegment( new_Speed_seg );
		}
	}

	NoteData nd;
	m_pSteps->GetNoteData( nd );
	
	if( !nd.IsEmpty() )
	{
		//si estan las notes vacias, no hay timing y tenemos "exception"
		//cuando se guarda la canci�n se conserva el songtiming.
		//luego seguir usando los stepstiming.
		//SOLO cuando se guarde y se cumpla esta condicion
		m_pPlayerStateEdit->m_TimingState = m_pSteps->m_StepsTiming;
	}


	m_pPlayerStateEdit->m_fSongBeat = 0;
	m_fTrailingBeat = m_pPlayerStateEdit->m_fSongBeat;

	m_iShiftAnchor = -1;
	m_iStartPlayingAt = -1;
	m_iStopPlayingAt = -1;

	this->AddChild( &m_Background );

	m_SnapDisplay.SetXY( EDIT_X, PLAYER_Y_STANDARD );
	m_SnapDisplay.Load( PLAYER_1 );
	m_SnapDisplay.SetZoom( 0.5f );
	this->AddChild( &m_SnapDisplay );

	// We keep track of this bit of state so that when the user is in Edit/Sync Songs and makes a change to the NoteSkins,
	// that change is "sticky" across multiple ScreenEdits.  That is the way the rest of the options work.
	// TODO: It would be cleaner to do this by making it possible to set an option in metrics.ini.
	if( !GAMESTATE->m_bDidModeChangeNoteSkin ) 
	{
		GAMESTATE->m_bDidModeChangeNoteSkin = true;
		FOREACH_PlayerNumber( pn ) 
		{
			const RString &sNoteSkin = EDITOR_NOTE_SKINS[pn].Get();
			if( NOTESKIN->DoesNoteSkinExist( sNoteSkin ) )
				PO_GROUP_ASSIGN( GAMESTATE->m_pPlayerState[pn]->m_PlayerOptions, 
				                 ModsLevel_Preferred, m_sNoteSkin, sNoteSkin );
		}
	}
	m_pPlayerStateEdit->m_PlayerNumber = PLAYER_1;
	// If we always go with the GAMESTATE NoteSkin, we will have fun effects like Vivid or Flat in the editor notefield.
	// This is not conducive to productive editing.
	if( NOTESKIN->DoesNoteSkinExist( EDITOR_NOTE_SKINS[PLAYER_1].Get() ) )
	{
		PO_GROUP_ASSIGN( m_pPlayerStateEdit->m_PlayerOptions, ModsLevel_Stage, m_sNoteSkin, EDITOR_NOTE_SKINS[PLAYER_1].Get() );
	}
	else
	{
		PO_GROUP_ASSIGN( m_pPlayerStateEdit->m_PlayerOptions, ModsLevel_Stage, m_sNoteSkin, GAMESTATE->m_pPlayerState[PLAYER_1]->m_PlayerOptions.GetStage().m_sNoteSkin );
	}
	m_pPlayerStateEdit->m_PlayerOptions.FromString( ModsLevel_Stage, EDIT_MODIFIERS );

	m_pSteps->GetNoteData( m_NoteDataEdit );
	m_NoteFieldEdit.SetXY( EDIT_X, EDIT_Y );
	m_NoteFieldEdit.SetZoom( 0.5f );
	m_NoteFieldEdit.Init( m_pPlayerStateEdit, PLAYER_HEIGHT*2 );
	m_NoteFieldEdit.Load( &m_NoteDataEdit, -240, 850 );
	this->AddChild( &m_NoteFieldEdit );

	m_NoteDataRecord.SetNumTracks( m_NoteDataEdit.GetNumTracks() );
	m_NoteFieldRecord.SetXY( RECORD_X, RECORD_Y );
	m_NoteFieldRecord.Init( GAMESTATE->m_pPlayerState[PLAYER_1], PLAYER_HEIGHT );
	m_NoteFieldRecord.Load( &m_NoteDataRecord, -(int)SCREEN_HEIGHT/2, (int)SCREEN_HEIGHT/2 );
	this->AddChild( &m_NoteFieldRecord );

	m_EditState = EditState_Invalid;
	TransitionEditState( STATE_EDITING );
	
	m_bRemoveNoteButtonDown = false;

	m_Clipboard.SetNumTracks( m_NoteDataEdit.GetNumTracks() );

	m_bHasUndo = false;
	m_Undo.SetNumTracks( m_NoteDataEdit.GetNumTracks() );

	m_bDirty = false;

	//effects
	GAMESTATE->m_bEffectsInEditor = true;

	//m_pScoreKeeperNormal = new ScoreKeeperNormal(m_pPlayerStateEdit, m_pPlayerStageStats);
	m_pScoreKeeperNormal = ScoreKeeper::MakeScoreKeeper( "ScoreKeeperNormal", m_pPlayerStateEdit, m_pPlayerStageStats );

	m_Player->Init( "Player", m_pPlayerStateEdit, m_pPlayerStageStats, NULL, NULL, NULL, NULL, NULL, m_pScoreKeeperNormal, NULL );
	m_Player->CacheAllUsedNoteSkins();
	GAMESTATE->m_pPlayerState[PLAYER_1]->m_PlayerController = PC_HUMAN;
	m_Player->SetXY( PLAYER_X, PLAYER_Y );
	this->AddChild( m_Player );

	this->AddChild( &m_Foreground );

	m_textInputTips.SetName( "EditHelp" );
	m_textInputTips.LoadFromFont( THEME->GetPathF("ScreenEdit","EditHelp") );
	m_textInputTips.SetText( EDIT_HELP_TEXT );
	LOAD_ALL_COMMANDS_AND_SET_XY_AND_ON_COMMAND( m_textInputTips );
	this->AddChild( &m_textInputTips );

	m_textInfo.SetName( "Info" );
	m_textInfo.LoadFromFont( THEME->GetPathF("ScreenEdit","Info") );
	LOAD_ALL_COMMANDS_AND_SET_XY_AND_ON_COMMAND( m_textInfo );
	this->AddChild( &m_textInfo );

	m_textPlayRecordHelp.SetName( "PlayRecordHelp" );
	m_textPlayRecordHelp.LoadFromFont( THEME->GetPathF("ScreenEdit","PlayRecordHelp") );
	m_textPlayRecordHelp.SetText( PLAY_RECORD_HELP_TEXT );
	LOAD_ALL_COMMANDS_AND_SET_XY_AND_ON_COMMAND( m_textPlayRecordHelp );
	this->AddChild( &m_textPlayRecordHelp );

	m_soundAddNote.Load(		THEME->GetPathS("ScreenEdit","AddNote"), true );
	m_soundRemoveNote.Load(		THEME->GetPathS("ScreenEdit","RemoveNote"), true );
	m_soundChangeLine.Load(		THEME->GetPathS("ScreenEdit","line"), true );
	m_soundChangeSnap.Load(		THEME->GetPathS("ScreenEdit","snap"), true );
	m_soundMarker.Load(		THEME->GetPathS("ScreenEdit","marker"), true );
	m_soundValueIncrease.Load(	THEME->GetPathS("ScreenEdit","value increase"), true );
	m_soundValueDecrease.Load(	THEME->GetPathS("ScreenEdit","value decrease"), true );
	m_soundSwitchSteps.Load(	THEME->GetPathS("ScreenEdit","switch steps") );
	m_soundSave.Load(		THEME->GetPathS("ScreenEdit","save") );
	m_GameplayAssist.Init();



	m_AutoKeysounds.FinishLoading();
	m_pSoundMusic = m_AutoKeysounds.GetSound();
	

	this->HandleScreenMessage( SM_UpdateTextInfo );
	m_bTextInfoNeedsUpdate = true;

	m_iCurrentTapsAdd = 0;//Tap Fake
	m_iCurrentTapMode = 0;//normal
	m_sCurrentNoteSkin = RString("default");//no noteskin todavia
	
	vector<Song*> songs;
	songs.push_back(m_pSong);
	vector<Steps*> steps;
	steps.push_back(m_pSteps);
	vector<AttackArray> attacks;
	attacks.push_back( AttackArray() );
	m_pScoreKeeperNormal->Load(songs, steps, attacks);
	m_pScoreKeeperNormal->OnNextSong( 1, m_pSteps, &m_NoteDataEdit );

	SubscribeToMessage( Message_SongModified );
}

ScreenEdit::~ScreenEdit()
{
	// UGLY: Don't delete the Song's steps.
	m_SongLastSave.DetachSteps();

	LOG->Trace( "ScreenEdit::~ScreenEdit()" );
	m_pSoundMusic->StopPlaying();

	GAMESTATE->m_bEffectsInEditor = false;
}

void ScreenEdit::BeginScreen()
{
	ScreenWithMenuElements::BeginScreen();

	/* We do this ourself. */
	SOUND->HandleSongTimer( false );
}

void ScreenEdit::EndScreen()
{
	ScreenWithMenuElements::EndScreen();

	SOUND->HandleSongTimer( true );
}

// play assist ticks
void ScreenEdit::PlayTicks()
{
	if( m_EditState != STATE_PLAYING )
		return;

	const TimingData &tm = m_pPlayerStateEdit->m_TimingState;
	const PlayerState &ps = *m_pPlayerStateEdit;

	m_GameplayAssist.PlayTicks( m_Player->GetNoteData(), tm, ps );
}

void ScreenEdit::PlayPreviewMusic()
{
	SOUND->StopMusic();
	SOUND->PlayMusic( 
		m_pSong->GetMusicPath(), 
		NULL,
		false,
		m_pSong->m_fMusicSampleStartSeconds,
		m_pSong->m_fMusicSampleLengthSeconds,
		0.0f,
		1.5f );
}

void ScreenEdit::MakeFilteredMenuDef( const MenuDef* pDef, MenuDef &menu )
{
	menu = *pDef;
	menu.rows.clear();

	vector<MenuRowDef> aRows;
	FOREACH_CONST( MenuRowDef, pDef->rows, r )
	{
		// Don't add rows that aren't applicable to this edit mode.
		if( EDIT_MODE >= r->emShowIn )
			menu.rows.push_back( *r );
	}
}

void ScreenEdit::EditMiniMenu( const MenuDef* pDef, ScreenMessage SM_SendOnOK, ScreenMessage SM_SendOnCancel )
{
	/* Reload options. */
	MenuDef menu("");
	MakeFilteredMenuDef( pDef, menu );
	ScreenMiniMenu::MiniMenu( &menu, SM_SendOnOK, SM_SendOnCancel );
}

void ScreenEdit::Update( float fDeltaTime )
{
	m_pPlayerStateEdit->Update( fDeltaTime );

	if( m_pSoundMusic->IsPlaying() )
	{
		RageTimer tm;
		const float fSeconds = m_pSoundMusic->GetPositionSeconds( NULL, &tm );
		//GAMESTATE->UpdateSongPosition( fSeconds, GAMESTATE->m_pCurSong->m_Timing, tm );
		m_pPlayerStateEdit->UpdateSongPosition( fSeconds, m_pPlayerStateEdit->m_TimingState, tm );
		//GAMESTATE->m_pPlayerState[PLAYER_1]->UpdateSongPosition( fSeconds, GAMESTATE->m_pPlayerState[PLAYER_1]->m_TimingState, tm );
	}

	m_Player->SetPlayerController( GamePreferences::m_AutoPlay );

	if( m_EditState == STATE_RECORDING  )	
	{
		for( int t=0; t<GAMESTATE->GetCurrentStyle()->m_iColsPerPlayer; t++ )	// for each track
		{
			GameInput GameI = GAMESTATE->GetCurrentStyle()->StyleInputToGameInput( t, PLAYER_1 );
			float fSecsHeld = INPUTMAPPER->GetSecsHeld( GameI );
			fSecsHeld = min( fSecsHeld, m_RemoveNoteButtonLastChanged.Ago() );
			if( fSecsHeld == 0 )
				continue;

			float fStartPlayingAtBeat = NoteRowToBeat(m_iStartPlayingAt);
			if( m_pPlayerStateEdit->m_fSongBeat <= fStartPlayingAtBeat )
				continue;

			float fStartedHoldingSeconds = m_pSoundMusic->GetPositionSeconds() - fSecsHeld;
			float fStartBeat = max( fStartPlayingAtBeat, m_pPlayerStateEdit->m_TimingState.GetBeatFromElapsedTime(fStartedHoldingSeconds) );
			float fEndBeat = max( fStartBeat, m_pPlayerStateEdit->m_fSongBeat );
			fEndBeat = min( fEndBeat, NoteRowToBeat(m_iStopPlayingAt) );

			// Round start and end to the nearest snap interval
			fStartBeat = Quantize( fStartBeat, NoteTypeToBeat(m_SnapDisplay.GetNoteType()) );
			fEndBeat = Quantize( fEndBeat, NoteTypeToBeat(m_SnapDisplay.GetNoteType()) );

			if( m_bRemoveNoteButtonDown )
			{
				m_NoteDataRecord.ClearRangeForTrack( BeatToNoteRow(fStartBeat), BeatToNoteRow(fEndBeat), t );
			}
			else if( fSecsHeld > RECORD_HOLD_SECONDS )
			{
				// create or extend a hold or roll note
				TapNote tn = EditIsBeingPressed(EDIT_BUTTON_LAY_MINE_OR_ROLL) ? TAP_ORIGINAL_ROLL_HEAD: TAP_ORIGINAL_HOLD_HEAD;

				if( m_iCurrentTapsAdd == 17 ) //fakes holds or rolls
					tn.bFakeHold = true;

				tn.pn = m_InputPlayerNumber;
				m_NoteDataRecord.AddHoldNote( t, BeatToNoteRow(fStartBeat), BeatToNoteRow(fEndBeat), tn );
			}
		}
	}

	//
	// check for end of playback/record
	//
	if( m_EditState == STATE_RECORDING  ||  m_EditState == STATE_PLAYING )
	{
		/*
		 * If any arrow is being held, continue for up to half a second after
		 * the end marker.  This makes it possible to start a hold note near
		 * the end of the range.  We won't allow placing anything outside of the
		 * range.
		 */
		bool bButtonIsBeingPressed = false;
		for( int t=0; t<GAMESTATE->GetCurrentStyle()->m_iColsPerPlayer; t++ )	// for each track
		{
			GameInput GameI = GAMESTATE->GetCurrentStyle()->StyleInputToGameInput( t, PLAYER_1 );
			if( INPUTMAPPER->IsBeingPressed(GameI) )
				bButtonIsBeingPressed = true;
		}

		float fLastBeat = NoteRowToBeat(m_iStopPlayingAt);
		if( bButtonIsBeingPressed && m_EditState == STATE_RECORDING )
		{
			float fSeconds = m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat( fLastBeat );
			fLastBeat = m_pPlayerStateEdit->m_TimingState.GetBeatFromElapsedTime( fSeconds + 0.5f );
		}

		float fStopAtSeconds = m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat( NoteRowToBeat(m_iStopPlayingAt) ) + 1;
		if( GAMESTATE->m_fMusicSeconds > fStopAtSeconds )
		{
			// loop
			TransitionEditState( STATE_PLAYING );			
		}
	}


	//LOG->Trace( "ScreenEdit::Update(%f)", fDeltaTime );
	ScreenWithMenuElements::Update( fDeltaTime );


	// Update trailing beat
	float fDelta = m_pPlayerStateEdit->m_fSongBeat - m_fTrailingBeat;
	if( fabsf(fDelta) < 10 )
		fapproach( m_fTrailingBeat, m_pPlayerStateEdit->m_fSongBeat,
			fDeltaTime*40 / m_NoteFieldEdit.GetPlayerState()->m_PlayerOptions.GetCurrent().m_fScrollSpeed );
	else
		fapproach( m_fTrailingBeat, m_pPlayerStateEdit->m_fSongBeat,
			fabsf(fDelta) * fDeltaTime*5 );

	PlayTicks();
}

static LocalizedString CURRENT_BEAT("ScreenEdit", "Current beat");
static LocalizedString CURRENT_SECOND("ScreenEdit", "Current second");
static LocalizedString SNAP_TO("ScreenEdit", "Snap to");
static LocalizedString NOTES("ScreenEdit", "%s notes");
static LocalizedString SELECTION_BEAT("ScreenEdit", "Selection beat");
static LocalizedString DIFFICULTY("ScreenEdit", "Difficulty");
static LocalizedString DESCRIPTION("ScreenEdit", "Description");
static LocalizedString MAIN_TITLE("ScreenEdit", "Main title");
static LocalizedString SUBTITLE("ScreenEdit", "Subtitle");
static LocalizedString TAP_STEPS("ScreenEdit", "Tap Steps");
static LocalizedString JUMPS("ScreenEdit", "Jumps");
static LocalizedString HANDS("ScreenEdit", "Hands");
static LocalizedString HOLDS("ScreenEdit", "Holds");
static LocalizedString MINES("ScreenEdit", "Mines");
static LocalizedString ROLLS("ScreenEdit", "Rolls");
static LocalizedString BEAT_0_OFFSET("ScreenEdit", "Beat 0 offset");
static LocalizedString PREVIEW_START("ScreenEdit", "Preview Start");
static LocalizedString PREVIEW_LENGTH("ScreenEdit", "Preview Length");
void ScreenEdit::UpdateTextInfo()
{
	if( m_pSteps == NULL )
		return;

	// Don't update the text during playback or record.  It causes skips.
	if( m_EditState != STATE_EDITING )
		return;

	if( !m_bTextInfoNeedsUpdate )
		return;

	m_bTextInfoNeedsUpdate = false;

	RString sNoteType = ssprintf( NOTES.GetValue(), NoteTypeToLocalizedString(m_SnapDisplay.GetNoteType()).c_str() );

	RString sText;
	sText += ssprintf( "%s:\n  %.3f\n",	CURRENT_BEAT.GetValue().c_str(), m_pPlayerStateEdit->m_fSongBeat );
	sText += ssprintf( "%s:\n  %.3f\n",	CURRENT_SECOND.GetValue().c_str(), m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat(m_pPlayerStateEdit->m_fSongBeat) );
	switch( EDIT_MODE.GetValue() )
	{
	//DEFAULT_FAIL( EDIT_MODE.GetValue() );
	case EditMode_Practice:
		break;
	case EditMode_CourseMods:
	case EditMode_Home:
	case EditMode_Full:
		sText += ssprintf( "%s:\n  %s\n", SNAP_TO.GetValue().c_str(), sNoteType.c_str() );
		break;
	}

	if( m_NoteFieldEdit.m_iBeginMarker != -1 )
	{
		sText += ssprintf( "%s:\n  %.3f", SELECTION_BEAT.GetValue().c_str(), NoteRowToBeat(m_NoteFieldEdit.m_iBeginMarker) );

		if( m_NoteFieldEdit.m_iEndMarker != -1 )
			sText += ssprintf( "-%.3f\n", NoteRowToBeat(m_NoteFieldEdit.m_iEndMarker) );
		else
			sText += " ...\n";
	}

	switch( EDIT_MODE.GetValue() )
	{
	//DEFAULT_FAIL( EDIT_MODE.GetValue() );
	case EditMode_Practice:
	case EditMode_CourseMods:
	case EditMode_Home:
		break;
	case EditMode_Full:
		sText += ssprintf( "%s:\n  %s\n",	DIFFICULTY.GetValue().c_str(), DifficultyToString( m_pSteps->GetDifficulty() ).c_str() );
		//sText += ssprintf( "%s:\n  %s\n",	DESCRIPTION.GetValue().c_str(), m_pSteps->GetDescription().c_str() );
		sText += ssprintf( "%s:\n  %s\n",	MAIN_TITLE.GetValue().c_str(), m_pSong->m_sMainTitle.c_str() );
		/*if( m_pSong->m_sSubTitle.size() )
			sText += ssprintf( "%s:\n  %s\n",	SUBTITLE.GetValue().c_str(), m_pSong->m_sSubTitle.c_str() );*/
		break;
	}
	sText += ssprintf( "%s:\n  %d\n", TAP_STEPS.GetValue().c_str(), m_NoteDataEdit.GetNumTapNotes() );
	//sText += ssprintf( "%s:\n  %d\n", JUMPS.GetValue().c_str(), m_NoteDataEdit.GetNumJumps() );
	//sText += ssprintf( "%s:\n  %d\n", HANDS.GetValue().c_str(), m_NoteDataEdit.GetNumHands() );
	sText += ssprintf( "%s:\n  %d\n", HOLDS.GetValue().c_str(), m_NoteDataEdit.GetNumHoldNotes() );
	sText += ssprintf( "%s:\n  %d\n", MINES.GetValue().c_str(), m_NoteDataEdit.GetNumMines() );

	switch(m_iCurrentTapMode)
	{
	case 1:
		{
			sText += ssprintf( "%s:\n  %s\n", "Taps To Add", "Vanish" );
			break;
		}
	case 2:
		{
			sText += ssprintf( "%s:\n  %s\n", "Taps To Add", "Sudden" );
			break;
		}
	case 3:
		{
			sText += ssprintf( "%s:\n  %s\n", "Taps To Add", "Invisible" );
			break;
		}
	case 4:
		{
			sText += ssprintf( "%s:\n  %s\n", "Taps To Add", "Drunken" );
			break;
		}
	default:
		{
			sText += ssprintf( "%s:\n  %s\n", "Taps To Add", "Normal" );
			break;
		}
	}

	/*
	0fake
	1heart
	2hidden
	3potion
	4vanish
	5item1x
	6item2x
	7item3x
	8item4x
	9item8x
	10sudden
	11left
	12right
	13down
	14blink
	15overhead
	16answer
	17fakehold
	*/
	switch( m_iCurrentTapsAdd )
	{
	case 0:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Fake" );
			break;
		}
	case 1:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Heart" );
			break;
		}
	case 2:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Hidden" );
			break;
		}
	case 3:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Potion" );
			break;
		}
	case 4:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Vanish" );
			break;
		}
	case 5:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Item1x" );
			break;
		}
	case 6:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Item2x" );
			break;
		}
	case 7:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Item3x" );
			break;
		}
	case 8:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Item4x" );
			break;
		}
	case 9:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Item8x" );
			break;
		}
	case 10:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Sudden" );
			break;
		}
	case 11:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Left" );
			break;
		}
	case 12:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Right" );
			break;
		}
	case 13:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Down" );
			break;
		}
	case 14:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Blink" );
			break;
		}
	case 15:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Overhead" );
			break;
		}
	case 16:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "Answer" );
			break;
		}
	case 17:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "FakeHolds" );
			break;
		}
	case 18:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "WildTap" );
			break;
		}
	case 19:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "GrooveTap" );
			break;
		}
	case 20:
		{
			sText += ssprintf( "%s:\n  %s\n", "Tap type", "DivisionTap" );
			break;
		}
	}

	sText += ssprintf( "%s:\n %s\n", "Noteskin Tap", m_sCurrentNoteSkin.c_str() );

	switch( EDIT_MODE.GetValue() )
	{
	//DEFAULT_FAIL( EDIT_MODE.GetValue() );
	case EditMode_Practice:
	case EditMode_CourseMods:
	case EditMode_Home:
		break;
	case EditMode_Full:
		sText += ssprintf( "%s:\n  %.3f secs\n", BEAT_0_OFFSET.GetValue().c_str(), m_pPlayerStateEdit->m_TimingState.m_fBeat0OffsetInSeconds );
		sText += ssprintf( "%s:\n  %.3f secs\n", PREVIEW_START.GetValue().c_str(), m_pSong->m_fMusicSampleStartSeconds );
		sText += ssprintf( "%s:\n  %.3f secs\n", PREVIEW_LENGTH.GetValue().c_str(), m_pSong->m_fMusicSampleLengthSeconds );
		break;
	}

	m_textInfo.SetText( sText );
}

void ScreenEdit::DrawPrimitives()
{
	// HACK:  Draw using the trailing beat
	float fSongBeat = m_pPlayerStateEdit->m_fSongBeat;	// save song beat
	float fSongBeatNoOffset = m_pPlayerStateEdit->m_fSongBeatNoOffset;
	float fSongBeatVisible = m_pPlayerStateEdit->m_fSongBeatVisible;

	if( !m_pSoundMusic->IsPlaying() )
	{
		m_pPlayerStateEdit->m_fSongBeat = m_fTrailingBeat;	// put trailing beat in effect
		m_pPlayerStateEdit->m_fSongBeatNoOffset = m_fTrailingBeat;	// put trailing beat in effect
		m_pPlayerStateEdit->m_fSongBeatVisible = m_fTrailingBeat;	// put trailing beat in effect
	}

	ScreenWithMenuElements::DrawPrimitives();

	if( !m_pSoundMusic->IsPlaying() )
	{
		m_pPlayerStateEdit->m_fSongBeat = fSongBeat;	// restore real song beat
		m_pPlayerStateEdit->m_fSongBeatNoOffset = fSongBeatNoOffset;
		m_pPlayerStateEdit->m_fSongBeatVisible = fSongBeatVisible;
	}
}

void ScreenEdit::Input( const InputEventPlus &input )
{
//	LOG->Trace( "ScreenEdit::Input()" );

	if( m_In.IsTransitioning() || m_Out.IsTransitioning() )
		return;

	EditButton EditB = DeviceToEdit( input.DeviceI );
	if( EditB == EditButton_Invalid )
		EditB = MenuButtonToEditButton( input.MenuI );
		

	if( EditB == EDIT_BUTTON_REMOVE_NOTE )
	{
		// Ugly: we need to know when the button was pressed or released, so we
		// can clamp operations to that time.  Should InputFilter keep track of
		// last release, too?
		m_bRemoveNoteButtonDown = (input.type != IET_RELEASE);
		m_RemoveNoteButtonLastChanged.Touch();
	}

	switch( m_EditState )
	{
	//DEFAULT_FAIL( m_EditState );
	case STATE_EDITING:
		InputEdit( input, EditB );
		m_bTextInfoNeedsUpdate = true;
		break;
	case STATE_RECORDING:
		InputRecord( input, EditB );
		break;
	case STATE_RECORDING_PAUSED:
		InputRecordPaused( input, EditB );
		break;
	case STATE_PLAYING:
		InputPlay( input, EditB );
		break;
	}
}

static void ShiftToRightSide( int &iCol, int iNumTracks )
{
	switch( GAMESTATE->GetCurrentStyle()->m_StyleType )
	{
	//DEFAULT_FAIL( GAMESTATE->GetCurrentStyle()->m_StyleType );
	case StyleType_OnePlayerOneSide:
		break;
	case StyleType_TwoPlayersTwoSides:
	case StyleType_OnePlayerTwoSides:
	case StyleType_TwoPlayersSharedSides:
		iCol += iNumTracks/2;
		break;
	}
}

static int FindAttackAtTime( const AttackArray& attacks, float fStartTime )
{
	for( unsigned i = 0; i < attacks.size(); ++i )
	{
		if( fabs(attacks[i].fStartSecond - fStartTime) < 0.001f )
			return i;
	}
	return -1;
}

static LocalizedString SWITCHED_TO		( "ScreenEdit", "Switched to" );
static LocalizedString NO_BACKGROUNDS_AVAILABLE	( "ScreenEdit", "No backgrounds available" );
static LocalizedString ALTER_MENU_NO_SELECTION	( "ScreenEdit", "You must have an area selected to enter the Alter Menu." );
void ScreenEdit::InputEdit( const InputEventPlus &input, EditButton EditB )
{
	if( input.type == IET_RELEASE )
	{
		if( EditPressed( EDIT_BUTTON_SCROLL_SELECT, input.DeviceI ) )
			m_iShiftAnchor = -1;
		return;
	}

	switch( EditB )
	{
	case EDIT_BUTTON_COLUMN_0:
	case EDIT_BUTTON_COLUMN_1:
	case EDIT_BUTTON_COLUMN_2:
	case EDIT_BUTTON_COLUMN_3:
	case EDIT_BUTTON_COLUMN_4:
	case EDIT_BUTTON_COLUMN_5:
	case EDIT_BUTTON_COLUMN_6:
	case EDIT_BUTTON_COLUMN_7:
	case EDIT_BUTTON_COLUMN_8:
	case EDIT_BUTTON_COLUMN_9:
		{
			if( input.type != IET_FIRST_PRESS )
				break;	// We only care about first presses

			int iCol = EditB - EDIT_BUTTON_COLUMN_0;


			// Alt + number = input to right half
			if( EditIsBeingPressed(EDIT_BUTTON_RIGHT_SIDE) )
				ShiftToRightSide( iCol, m_NoteDataEdit.GetNumTracks() );


			const float fSongBeat = m_pPlayerStateEdit->m_fSongBeat;
			const int iSongIndex = BeatToNoteRow( fSongBeat );

			if( iCol >= m_NoteDataEdit.GetNumTracks() )	// this button is not in the range of columns for this Style
				break;

			// check for to see if the user intended to remove a HoldNote
			int iHeadRow;
			if( m_NoteDataEdit.IsHoldNoteAtRow( iCol, iSongIndex, &iHeadRow ) )
			{
				m_soundRemoveNote.Play();
				SetDirty( true );
				SaveUndo();
				m_NoteDataEdit.SetTapNote( iCol, iHeadRow, TAP_EMPTY );
				// Don't CheckNumberOfNotesAndUndo.  We don't want to revert any change that removes notes.
			}
			else if( m_NoteDataEdit.GetTapNote(iCol, iSongIndex).type != TapNote::empty )
			{
				m_soundRemoveNote.Play();
				SetDirty( true );
				SaveUndo();
				m_NoteDataEdit.SetTapNote( iCol, iSongIndex, TAP_EMPTY );
				// Don't CheckNumberOfNotesAndUndo.  We don't want to revert any change that removes notes.
			}
			else if( EditIsBeingPressed(EDIT_BUTTON_LAY_MINE_OR_ROLL) )
			{
				m_soundAddNote.Play();
				SetDirty( true );
				SaveUndo();
				TapNote tn = TAP_ORIGINAL_MINE;
				tn.pn = m_InputPlayerNumber;
				m_NoteDataEdit.SetTapNote( iCol, iSongIndex, tn );
				CheckNumberOfNotesAndUndo();
			}
			else if( EditIsBeingPressed(EDIT_BUTTON_LAY_TAP_ATTACK) )
			{
				g_iLastInsertTapAttackTrack = iCol;
				EditMiniMenu( &g_InsertTapAttack, SM_BackFromInsertTapAttack );
			}
			else if( EditIsBeingPressed(EDIT_BUTTON_LAY_LIFT) )
			{
				m_soundAddNote.Play();
				SetDirty( true );
				SaveUndo();
				TapNote tn;
				/*
				Esto debe ir al final de este if
				if( THEME->HasMetric( "NoteSkinsDef", m_sCurrentNoteSkin ) ) {
					tn.sChar = THEME->GetString("NoteSkinsDef", m_sCurrentNoteSkin);
					tn.sNoteSkin = m_sCurrentNoteSkin;
				}
				*/

				if( m_iCurrentTapsAdd == 0 )
				{
					tn = TAP_ORIGINAL_FAKE;////fake modificado por mi
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::fake, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::fake, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::fake, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::fake, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 1 )
				{
					tn = TAP_ORIGINAL_HEART;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::heart, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::heart, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::heart, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::heart, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 2 )
				{
					tn = TAP_ORIGINAL_HIDDEN;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::hidden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::hidden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::hidden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::hidden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 3 )
				{
					tn = TAP_ORIGINAL_POTION;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::potion, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::potion, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::potion, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::potion, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 4 )
				{
					tn = TAP_ORIGINAL_VANISH;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::vanish, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::vanish, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::vanish, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::vanish, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 5 )
				{
					tn = TAP_ORIGINAL_ITEM1X;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::item1x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::item1x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::item1x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::item1x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 6 )
				{
					tn = TAP_ORIGINAL_ITEM2X;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::item2x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::item2x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::item2x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::item2x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 7 )
				{
					tn = TAP_ORIGINAL_ITEM3X;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::item3x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::item3x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::item3x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::item3x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 8 )
				{
					tn = TAP_ORIGINAL_ITEM4X;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::item4x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::item4x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::item4x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::item4x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 9 )
				{
					tn = TAP_ORIGINAL_ITEM8X;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::item8x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::item8x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::item8x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::item8x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 10 )
				{
					tn = TAP_ORIGINAL_SUDDEN;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::sudden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::sudden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::sudden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::sudden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 11 )
				{
					tn = TAP_ORIGINAL_ITEMLEFT;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::itemleft, TapNote::SubType_Invalid, TapNote::original, "turnleft,*9999 no reverse", 9999.0f, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::itemleft, TapNote::SubType_Invalid, TapNote::original, "turnleft,*9999 no reverse", 9999.0f, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::itemleft, TapNote::SubType_Invalid, TapNote::original, "turnleft,*9999 no reverse", 9999.0f, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::itemleft, TapNote::SubType_Invalid, TapNote::original, "turnleft,*9999 no reverse", 9999.0f, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 12 )
				{
					tn = TAP_ORIGINAL_ITEMRIGHT;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::itemright, TapNote::SubType_Invalid, TapNote::original, "turnright,*9999 no reverse", 9999.0f, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::itemright, TapNote::SubType_Invalid, TapNote::original, "turnright,*9999 no reverse", 9999.0f, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::itemright, TapNote::SubType_Invalid, TapNote::original, "turnright,*9999 no reverse", 9999.0f, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::itemright, TapNote::SubType_Invalid, TapNote::original, "turnright,*9999 no reverse", 9999.0f, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 13 )
				{
					tn = TAP_ORIGINAL_ITEMUNDER;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::itemunder, TapNote::SubType_Invalid, TapNote::original, "*9999 reverse", 9999.0f, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::itemunder, TapNote::SubType_Invalid, TapNote::original, "*9999 reverse", 9999.0f, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::itemunder, TapNote::SubType_Invalid, TapNote::original, "*9999 reverse", 9999.0f, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::itemunder, TapNote::SubType_Invalid, TapNote::original, "*9999 reverse", 9999.0f, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 14 )
				{
					tn = TAP_ORIGINAL_ITEMBLINK;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::itemblink, TapNote::SubType_Invalid, TapNote::original, "blink", 9999.0f, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::itemblink, TapNote::SubType_Invalid, TapNote::original, "blink", 9999.0f, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::itemblink, TapNote::SubType_Invalid, TapNote::original, "blink", 9999.0f, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::itemblink, TapNote::SubType_Invalid, TapNote::original, "blink", 9999.0f, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 15 )
				{
					tn = TAP_ORIGINAL_ITEMOVERHEAD;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::itemoverhead, TapNote::SubType_Invalid, TapNote::original, "turnoverhead,no blink,*9999 no reverse", 9999.0f, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::itemoverhead, TapNote::SubType_Invalid, TapNote::original, "turnoverhead,no blink,*9999 no reverse", 9999.0f, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::itemoverhead, TapNote::SubType_Invalid, TapNote::original, "turnoverhead,no blink,*9999 no reverse", 9999.0f, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::itemoverhead, TapNote::SubType_Invalid, TapNote::original, "turnoverhead,no blink,*9999 no reverse", 9999.0f, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 16 )
				{
					/*tn = TapNote( TapNote::answer, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "   ", "", "" );
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::answer, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "   ", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::answer, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "   ", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::answer, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "   ", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::answer, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "   ", "Drunk", "" );
							break;
						default:
							break;
						}
					}*/
					g_iLastInsertBrainShowerTapTrack = iCol;
					ScreenTextEntry::TextEntry( SM_WriteBrainShowerTap, "Insert the condition 0/1 and graphics name like 1/G01 or 0/9 and so on", "",9,NULL );
				}
				else if( m_iCurrentTapsAdd == 18 )
				{
					//tn = TAP_ORIGINAL_WILD;		
					g_iLastInsertDivisionTapTrack = iCol;
					ScreenTextEntry::TextEntry( SM_WriteWildLabel, "Insert the beat to GOTO and beat to LAND, like 75.5/101.5", "",255,NULL );
				}
				else if( m_iCurrentTapsAdd == 19 )
				{
					//tn = TAP_ORIGINAL_GROOVE;	
					g_iLastInsertDivisionTapTrack = iCol;
					ScreenTextEntry::TextEntry( SM_WriteGrooveLabel, "Insert the beat to GOTO and beat to LAND, like 75.5/101.5", "",255,NULL );
				}
				else if( m_iCurrentTapsAdd = 20 )
				{
					g_iLastInsertDivisionTapTrack = iCol;
					ScreenTextEntry::TextEntry( SM_WriteTapDivisionLabel, "Insert the beat to GOTO and beat to LAND, like 75.5/101.5", "",255,NULL );
				}
				// = TAP_ORIGINAL_FAKE;//modificado por mi, cambia lift por fake
				if( THEME->HasMetric( "NoteSkinsDef", m_sCurrentNoteSkin ) ) 
				{										
					tn.sNoteSkin = m_sCurrentNoteSkin;
					ThemeMetric<RString> skin ( "NoteSkinsDef", tn.sNoteSkin );
					skin.Load( "NoteSkinsDef", tn.sNoteSkin);
					tn.sChar = skin.GetValue();
				}
				tn.pn = m_InputPlayerNumber;
				m_NoteDataEdit.SetTapNote( iCol, iSongIndex, tn );
				CheckNumberOfNotesAndUndo();
			}
			else
			{
				m_soundAddNote.Play();
				SetDirty( true );
				SaveUndo();
				TapNote tn = TAP_ORIGINAL_TAP;
				if( m_iCurrentTapMode != 0 )
				{
					switch( m_iCurrentTapMode )
					{
					case 1:
						tn = TapNote( TapNote::tap, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
						break;
					case 2:
						tn = TapNote( TapNote::tap, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
						break;
					case 3:
						tn = TapNote( TapNote::tap, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
						break;
					case 4:
						tn = TapNote( TapNote::tap, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
						break;
					default:
						break;
					}
				}
				//no encuentra los metrics
				if( THEME->HasMetric( "NoteSkinsDef", m_sCurrentNoteSkin ) ) 
				{										
					tn.sNoteSkin = m_sCurrentNoteSkin;
					ThemeMetric<RString> skin ( "NoteSkinsDef", tn.sNoteSkin );
					skin.Load( "NoteSkinsDef", tn.sNoteSkin);
					tn.sChar = skin.GetValue();
				}
				tn.pn = m_InputPlayerNumber;
				m_NoteDataEdit.SetTapNote(iCol, iSongIndex, tn );
				CheckNumberOfNotesAndUndo();
			}
		}
		break;
	case EDIT_BUTTON_SCROLL_SPEED_UP:
	case EDIT_BUTTON_SCROLL_SPEED_DOWN:
		{
			PlayerState *pPlayerState = const_cast<PlayerState *> (m_NoteFieldEdit.GetPlayerState());
			float fScrollSpeed = pPlayerState->m_PlayerOptions.GetSong().m_fScrollSpeed;

			const float fSpeeds[] = { 1.0f, 1.5f, 2.0f, 3.0f, 4.0f, 6.0f, 8.0f };
			int iSpeed = 0;
			for( unsigned i = 0; i < ARRAYLEN(fSpeeds); ++i )
			{
				if( fSpeeds[i] == fScrollSpeed )
				{
					iSpeed = i;
					break;
				}
			}

			switch( EditB )
			{
			//DEFAULT_FAIL(EditB);
			case EDIT_BUTTON_SCROLL_SPEED_DOWN:
				--iSpeed;
				break;
			case EDIT_BUTTON_SCROLL_SPEED_UP:
				++iSpeed;
				break;
			}
			iSpeed = clamp( iSpeed, 0, (int) ARRAYLEN(fSpeeds)-1 );
			
			if( fSpeeds[iSpeed] != fScrollSpeed )
			{
				m_soundMarker.Play();
				fScrollSpeed = fSpeeds[iSpeed];
			}

			PO_GROUP_ASSIGN( pPlayerState->m_PlayerOptions, ModsLevel_Song, m_fScrollSpeed, fScrollSpeed );
			break;
		}

		break;
	case EDIT_BUTTON_SCROLL_HOME:
		ScrollTo( 0 );
		break;
	case EDIT_BUTTON_SCROLL_END:
		ScrollTo( m_NoteDataEdit.GetLastBeat() );
		break;
	case EDIT_BUTTON_SCROLL_UP_LINE:
	case EDIT_BUTTON_SCROLL_UP_PAGE:
	case EDIT_BUTTON_SCROLL_DOWN_LINE:
	case EDIT_BUTTON_SCROLL_DOWN_PAGE:
		{
			float fBeatsToMove=0.f;
			switch( EditB )
			{
			//DEFAULT_FAIL( EditB );
			case EDIT_BUTTON_SCROLL_UP_LINE:
			case EDIT_BUTTON_SCROLL_DOWN_LINE:
				fBeatsToMove = NoteTypeToBeat( m_SnapDisplay.GetNoteType() );
				if( EditB == EDIT_BUTTON_SCROLL_UP_LINE )	
					fBeatsToMove *= -1;
				break;
			case EDIT_BUTTON_SCROLL_UP_PAGE:
			case EDIT_BUTTON_SCROLL_DOWN_PAGE:
				fBeatsToMove = BEATS_PER_MEASURE;
				if( EditB == EDIT_BUTTON_SCROLL_UP_PAGE )	
					fBeatsToMove *= -1;
				break;
			}

			if( m_pPlayerStateEdit->m_PlayerOptions.GetSong().m_fScrolls[PlayerOptions::SCROLL_REVERSE] > 0.5 )
				fBeatsToMove *= -1;

			float fDestinationBeat = m_pPlayerStateEdit->m_fSongBeat + fBeatsToMove;
			fDestinationBeat = Quantize( fDestinationBeat, NoteTypeToBeat(m_SnapDisplay.GetNoteType()) );

			ScrollTo( fDestinationBeat );
		}
		break;
	case EDIT_BUTTON_SCROLL_NEXT_MEASURE:
		{
			float fDestinationBeat = m_pPlayerStateEdit->m_fSongBeat + BEATS_PER_MEASURE;
			fDestinationBeat = ftruncf( fDestinationBeat, (float)BEATS_PER_MEASURE );
			ScrollTo( fDestinationBeat );
			break;
		}
	case EDIT_BUTTON_SCROLL_PREV_MEASURE:
		{
			float fDestinationBeat = QuantizeUp( m_pPlayerStateEdit->m_fSongBeat, (float)BEATS_PER_MEASURE );
			fDestinationBeat -= (float)BEATS_PER_MEASURE;
			ScrollTo( fDestinationBeat );
			break;
		}
	case EDIT_BUTTON_OPEN_ALTER_MENU:
		{
			bool bAreaSelected = m_NoteFieldEdit.m_iBeginMarker!=-1 && m_NoteFieldEdit.m_iEndMarker!=-1;
			if (!bAreaSelected)
			{
				SCREENMAN->SystemMessage( ALTER_MENU_NO_SELECTION );
				SCREENMAN->PlayInvalidSound();
			}
			else 
			{				
				EditMiniMenu(&g_AlterMenu, SM_BackFromAlterMenu);
			}
			break;

		}
	//case EDIT_BUTTON_SCROLL_NEXT:
	//	{
	//		int iRow = BeatToNoteRow( m_pPlayerStateEdit->m_fSongBeat );
	//		NoteDataUtil::GetNextEditorPosition( m_NoteDataEdit, iRow );
	//		ScrollTo( NoteRowToBeat(iRow) );
	//	}
	//	break;
	//case EDIT_BUTTON_SCROLL_PREV:
	//	{
	//		int iRow = BeatToNoteRow( m_pPlayerStateEdit->m_fSongBeat );
	//		NoteDataUtil::GetPrevEditorPosition( m_NoteDataEdit, iRow );
	//		ScrollTo( NoteRowToBeat(iRow) );
	//	}
	//	break;
	case EDIT_BUTTON_SHOW_TIMING_MENU:
		DisplayTimingMenu();
		break;
	case EDIT_BUTTON_SNAP_NEXT:
		if( m_SnapDisplay.PrevSnapMode() )
			OnSnapModeChange();
		break;
	case EDIT_BUTTON_SNAP_PREV:
		if( m_SnapDisplay.NextSnapMode() )
			OnSnapModeChange();
		break;
	case EDIT_BUTTON_LAY_SELECT:
		{
			const int iCurrentRow = BeatToNoteRow(m_pPlayerStateEdit->m_fSongBeat);
			if( m_NoteFieldEdit.m_iBeginMarker==-1 && m_NoteFieldEdit.m_iEndMarker==-1 )
			{
				// lay begin marker
				m_NoteFieldEdit.m_iBeginMarker = BeatToNoteRow(m_pPlayerStateEdit->m_fSongBeat);
			}
			else if( m_NoteFieldEdit.m_iEndMarker==-1 )	// only begin marker is laid
			{
				if( iCurrentRow == m_NoteFieldEdit.m_iBeginMarker )
				{
					m_NoteFieldEdit.m_iBeginMarker = -1;
				}
				else
				{
					m_NoteFieldEdit.m_iEndMarker = max( m_NoteFieldEdit.m_iBeginMarker, iCurrentRow );
					m_NoteFieldEdit.m_iBeginMarker = min( m_NoteFieldEdit.m_iBeginMarker, iCurrentRow );
				}
			}
			else	// both markers are laid
			{
				m_NoteFieldEdit.m_iBeginMarker = iCurrentRow;
				m_NoteFieldEdit.m_iEndMarker = -1;
			}
			m_soundMarker.Play();
		}
		break;
	case EDIT_BUTTON_OPEN_AREA_MENU:
		{
			// update enabled/disabled in g_AreaMenu
			//bool bAreaSelected = m_NoteFieldEdit.m_iBeginMarker!=-1 && m_NoteFieldEdit.m_iEndMarker!=-1;
			//g_AreaMenu.rows[cut].bEnabled = bAreaSelected;
			//g_AreaMenu.rows[copy].bEnabled = bAreaSelected;
			g_AreaMenu.rows[paste_at_current_beat].bEnabled = !m_Clipboard.IsEmpty();
			g_AreaMenu.rows[paste_at_begin_marker].bEnabled = !m_Clipboard.IsEmpty() != 0 && m_NoteFieldEdit.m_iBeginMarker!=-1;
			//g_AreaMenu.rows[clear].bEnabled = bAreaSelected;
			//g_AreaMenu.rows[quantize].bEnabled = bAreaSelected;
			//g_AreaMenu.rows[turn].bEnabled = bAreaSelected;
			//g_AreaMenu.rows[transform].bEnabled = bAreaSelected;
			//g_AreaMenu.rows[alter].bEnabled = bAreaSelected;
			//g_AreaMenu.rows[tempo].bEnabled = bAreaSelected;
			//g_AreaMenu.rows[play].bEnabled = bAreaSelected;
			//g_AreaMenu.rows[record].bEnabled = bAreaSelected;
			//g_AreaMenu.rows[convert_to_pause].bEnabled = bAreaSelected;
			g_AreaMenu.rows[undo].bEnabled = m_bHasUndo;
			EditMiniMenu( &g_AreaMenu, SM_BackFromAreaMenu );
		}
		break;
	case EDIT_BUTTON_OPEN_EDIT_MENU:
		EditMiniMenu( &g_MainMenu, SM_BackFromMainMenu );
		break;
	case EDIT_BUTTON_OPEN_INPUT_HELP:
		DoHelp();
		break;
	case EDIT_BUTTON_STEP_MODE_CHANGE:
		{
			m_iCurrentTapMode ++;

			wrap( m_iCurrentTapMode, 5 );

			/*switch( m_iCurrentTapMode )
			{
			case 0:
				SCREENMAN->SystemMessageNoAnimate( "Normal Taps" );
				break;
			case 1:
				SCREENMAN->SystemMessageNoAnimate( "Vanish Taps" );
				break;
			case 2:
				SCREENMAN->SystemMessageNoAnimate( "Sudden Taps" );
				break;
			case 3:
				SCREENMAN->SystemMessageNoAnimate( "Hidden Taps" );
				break;
			case 4:
				SCREENMAN->SystemMessageNoAnimate( "Drunked Taps" );
				break;
			}*/
		}
		break;
	case EDIT_BUTTON_CHANGE_TIMESIGNATURE_NUMERATOR_UP:
	case EDIT_BUTTON_CHANGE_TIMESIGNATURE_NUMERATOR_DOWN:
	//case EDIT_BUTTON_CHANGE_TIMESIGNATURE_DENOMINATOR_UP:
	//case EDIT_BUTTON_CHANGE_TIMESIGNATURE_DENOMINATOR_DOWN:
		{
			//TimeSignatureSegment &ts = m_pPlayerStateEdit->m_TimingState.GetTimeSignatureSegmentAtBeat( m_pPlayerStateEdit->m_fSongBeat );

			//ASSERT( ts );

			//int iCurN = ts.m_iNumerator;
			//int iCurD = ts.m_iDenominator;

			//int iDeltaN = 0;
			//int iDeltaD = 0;
			//switch( EditB )
			//{
			//	//case EDIT_BUTTON_CHANGE_TIMESIGNATURE_DENOMINATOR_UP: iDeltaD += 1; break;				
			//	//case EDIT_BUTTON_CHANGE_TIMESIGNATURE_DENOMINATOR_DOWN: iDeltaD -= 1; break;
			//	case EDIT_BUTTON_CHANGE_TIMESIGNATURE_NUMERATOR_UP: iDeltaN += 1; break;
			//	case EDIT_BUTTON_CHANGE_TIMESIGNATURE_NUMERATOR_DOWN: iDeltaN -= 1; break;
			//}

			//int iNewN = iCurN + iDeltaN;
			//int iNewD = iCurD + iDeltaD;

			//if( iNewN > 0 && iNewD > 0 )
			//	m_pPlayerStateEdit->m_TimingState.SetTimeSignatureAtBeat( m_pPlayerStateEdit->m_fSongBeat, iNewN, iNewD );

			RString sCurrent = m_sCurrentNoteSkin;
			//Trim( sCurrent );
			//TrimRight( sCurrent );
			//CHECKPOINT;

			
			vector<RString> vsNoteSkinArray;
			NOTESKIN->GetNoteSkinNames( vsNoteSkinArray );

			//CHECKPOINT_M( "NoteSkin To Find: " + sCurrent );

			static unsigned i = 0;
			//bool bFind = false;
			//RString sAll;
			//for( i = 0; i < vsNoteSkinArray.size(); i++ )//get index noteskin
			//{
			//	sAll += " " + vsNoteSkinArray[i];
			//	if( vsNoteSkinArray[i].CompareNoCase(sCurrent)==0 ) 
			//	{
			//		bFind = true;
			//		break;
			//	}
			//}
			////CHECKPOINT_M( sAll );
			//ASSERT( bFind );//encontramos la noteskin

			switch( EditB )
			{
			DEFAULT_FAIL( EditB );
			case EDIT_BUTTON_CHANGE_TIMESIGNATURE_NUMERATOR_UP:
				
				if( i == vsNoteSkinArray.size()-1 )
					return; //upper bound

				i++;

				sCurrent = vsNoteSkinArray[i];
				
				break;
			case EDIT_BUTTON_CHANGE_TIMESIGNATURE_NUMERATOR_DOWN:

				if( i == 0 )
					return; //lower bound

				i--;

				sCurrent = vsNoteSkinArray[i];

				break;
			}

			ASSERT( NOTESKIN->DoesNoteSkinExist( sCurrent ) );

			m_sCurrentNoteSkin = RString(sCurrent);
			//SCREENMAN->SystemMessageNoAnimate( "NoteSkin para los taps: " + m_sCurrentNoteSkin );
			
			//SetDirty( true );
		}
		break;
	case EDIT_BUTTON_CHANGE_NOTESKIN_UP:
	case EDIT_BUTTON_CHANGE_NOTESKIN_DOWN:
		{
			if( !(m_NoteFieldEdit.m_iBeginMarker != -1) )
			{
				SCREENMAN->SystemMessageNoAnimate( "You should select an attack at start time" );
				break;
			}

			int index = FindAttackAtTime( m_pSteps->m_Attacks,
				m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat( 
					NoteRowToBeat(m_NoteFieldEdit.m_iBeginMarker)));

			//SCREENMAN->SystemMessageNoAnimate( ssprintf( "index: %d", index ) );

			if( index >= 0 )
				m_pSteps->m_Attacks.erase( m_pSteps->m_Attacks.begin() + index );

			//NOTESKIN POR TAG | las seccion de arriba tiene ns separadas por taps

			//CHECKPOINT;

			//RString sCurrent = m_pPlayerStateEdit->m_TimingState.GetNoteSkinAtBeat( m_pPlayerStateEdit->m_fSongBeat );
			//Trim( sCurrent );
			////TrimRight( sCurrent );
			////CHECKPOINT;

			//vector<RString> vsNoteSkinArray;
			//NOTESKIN->GetNoteSkinNames( vsNoteSkinArray );

			//CHECKPOINT_M( "NoteSkin To Find: " + sCurrent );

			//unsigned i;
			//bool bFind = false;
			//RString sAll;
			//for( i = 0; i < vsNoteSkinArray.size(); i++ )//get index noteskin
			//{
			//	sAll += " " + vsNoteSkinArray[i];
			//	if( vsNoteSkinArray[i].CompareNoCase(sCurrent)==0 ) 
			//	{
			//		bFind = true;
			//		break;
			//	}
			//}
			//CHECKPOINT_M( sAll );
			//ASSERT( bFind );//encontramos la noteskin

			////CHECKPOINT;

			//switch( EditB )
			//{
			//DEFAULT_FAIL( EditB );
			//case EDIT_BUTTON_CHANGE_NOTESKIN_UP:
			//	
			//	if( i == vsNoteSkinArray.size()-1 )
			//		return; //upper bound

			//	//CHECKPOINT_M( ssprintf( "ChangeUp: i=%d", i ) );

			//	i++;

			//	sCurrent = vsNoteSkinArray[i];
			//	
			//	break;
			//case EDIT_BUTTON_CHANGE_NOTESKIN_DOWN:

			//	if( i == 0 )
			//		return; //lower bound

			//	//CHECKPOINT_M( ssprintf( "ChangeDown: i=%d", i ) );

			//	i--;

			//	sCurrent = vsNoteSkinArray[i];

			//	break;
			//}

			//ASSERT( NOTESKIN->DoesNoteSkinExist( sCurrent ) );

			//CHECKPOINT_M( "Applying NoteSkin: "+ sCurrent );

			//m_pPlayerStateEdit->m_TimingState.SetNoteSkinAtBeat( m_pPlayerStateEdit->m_fSongBeat, sCurrent );
			//SetDirty( true );
		}
		break;
	case EDIT_BUTTON_OPEN_NEXT_STEPS:
	case EDIT_BUTTON_OPEN_PREV_STEPS:
		{/*
			// don't keep undo when changing Steps
			ClearUndo();

			// save current steps
			Steps* pSteps = GAMESTATE->m_pCurSteps[PLAYER_1];
			ASSERT( pSteps );
			pSteps->SetNoteData( m_NoteDataEdit );

			// Get all Steps of this StepsType
			StepsType st = pSteps->m_StepsType;
			vector<Steps*> vSteps;
			SongUtil::GetSteps( GAMESTATE->m_pCurSong, vSteps, st );

			// Sort them by difficulty.
			StepsUtil::SortStepsByTypeAndDifficulty( vSteps );

			// Find out what index the current Steps are
			vector<Steps*>::iterator it = find( vSteps.begin(), vSteps.end(), pSteps );
			ASSERT( it != vSteps.end() );

			switch( EditB )
			{
			DEFAULT_FAIL( EditB );
			case EDIT_BUTTON_OPEN_PREV_STEPS:	
				if( it==vSteps.begin() )
				{
					SCREENMAN->PlayInvalidSound();
					return;
				}
				it--;
				break;
			case EDIT_BUTTON_OPEN_NEXT_STEPS:
				it++;
				if( it==vSteps.end() )
				{
					SCREENMAN->PlayInvalidSound();
					return;
				}
				break;
			}

			pSteps = *it;
			GAMESTATE->m_pCurSteps[PLAYER_1].Set( pSteps );
			m_pSteps = pSteps;
			pSteps->GetNoteData( m_NoteDataEdit );

			RString s = ssprintf(
				SWITCHED_TO.GetValue() + " %s %s '%s' (%d of %d)",
				GAMEMAN->StepsTypeToString( pSteps->m_StepsType ).c_str(),
				DifficultyToString( pSteps->GetDifficulty() ).c_str(),
				pSteps->GetDescription().c_str(),
				it - vSteps.begin() + 1,
				int(vSteps.size()) );
			SCREENMAN->SystemMessage( s );
			m_soundSwitchSteps.Play();
		}
		break;*/
		//MODIFICADO POR MI, ahora f5/f6 seran para el combo factor
			//int iComboFactor = m_pPlayerStateEdit->m_TimingState.GetComboFactorAtBeat( m_pPlayerStateEdit->m_fSongBeat );
			//int iDeltaCombo = 0;
			//switch( EditB )
			//{
			////DEFAULT_FAIL( EditB );
			//case EDIT_BUTTON_OPEN_NEXT_STEPS:	iDeltaCombo = +1;		break;
			//case EDIT_BUTTON_OPEN_PREV_STEPS:	iDeltaCombo = -1;		break;
			//}
			//if( EditIsBeingPressed( EDIT_BUTTON_ADJUST_FINE ) )
			//{
			//	iDeltaCombo += 1;	// 1 tickcount
			//}

			//int iNewComboFactor = iComboFactor + iDeltaCombo;

			////No permitir tickcounts negativos
			//if( iNewComboFactor > 0 && iNewComboFactor < 100 )
			//	m_pPlayerStateEdit->m_TimingState.SetComboFactorAtBeat( m_pPlayerStateEdit->m_fSongBeat, iNewComboFactor );
			//SetDirty( true );
		}
		break;
	case EDIT_BUTTON_BPM_UP:
	case EDIT_BUTTON_BPM_DOWN:
		{
			//float fBPM = m_pSong->GetBPMAtBeat( m_pPlayerStateEdit->m_fSongBeat );
			//float fBPM = m_pPlayerStateEdit->m_TimingState.GetBPMAtBeat( m_pPlayerStateEdit->m_fSongBeat );
			//float fDelta = 0;
			//switch( EditB )
			//{
			////DEFAULT_FAIL( EditB );
			//case EDIT_BUTTON_BPM_UP:	fDelta = +0.020f;	break;
			//case EDIT_BUTTON_BPM_DOWN:	fDelta = -0.020f;	break;
			//}
			//if( EditIsBeingPressed( EDIT_BUTTON_ADJUST_FINE ) )
			//{
			//	fDelta /= 20;	// .001 bpm
			//}
			//else if( input.type == IET_REPEAT )
			//{
			//	if( INPUTFILTER->GetSecsHeld(input.DeviceI) < 1.0f )
			//		fDelta *= 10;
			//	else
			//		fDelta *= 40;
			//}
			//
			//float fNewBPM = fBPM + fDelta;
			//m_pPlayerStateEdit->m_TimingState.SetBPMAtBeat( m_pPlayerStateEdit->m_fSongBeat, fNewBPM );
			//(fDelta>0 ? m_soundValueIncrease : m_soundValueDecrease).Play();
			//SetDirty( true );
		}
		break;
	//MODIFICADO POR MI----
	case EDIT_BUTTON_SCROLL_NEXT:
		{
			ScreenTextEntry::TextEntry(  SM_BackFromChangeSpecialTag, "Insert the values YES/NO", "",3,NULL );
		}
		break;
	case EDIT_BUTTON_SCROLL_PREV:
		{
			if( !(m_NoteFieldEdit.m_iBeginMarker != -1 && m_NoteFieldEdit.m_iEndMarker != -1) )
			{
				SCREENMAN->SystemMessageNoAnimate( "Select an area and write the mods" );
				break;
			}
			ScreenTextEntry::TextEntry( SM_BackFromWriteAttack, "Insert the attack string \n", "MODS=",256,NULL );
			//int iTickcount = m_pPlayerStateEdit->m_TimingState.GetTickcountAtBeat( m_pPlayerStateEdit->m_fSongBeat );
			//int iDeltaTick = 0;
			//switch( EditB )
			//{
			////DEFAULT_FAIL( EditB );
			//case EDIT_BUTTON_SCROLL_PREV:	iDeltaTick = -1;		break;
			//case EDIT_BUTTON_SCROLL_NEXT:	iDeltaTick = +1;		break;
			//}
			//if( EditIsBeingPressed( EDIT_BUTTON_ADJUST_FINE ) )
			//{
			//	iDeltaTick += 1;	// 1 tickcount
			//}

			//int iNewTickcount = iTickcount + iDeltaTick;

			////No permitir tickcounts negativos
			//if( iNewTickcount > -1 && iNewTickcount < 49 )
			//	m_pPlayerStateEdit->m_TimingState.SetTickcountAtBeat( m_pPlayerStateEdit->m_fSongBeat, iNewTickcount );
			//SetDirty( true );
		}
		break;
	//TICKCOUNT SEGMENT----------------------
		//MODIFICADO POR MI
		//AHORA NO HAY STOP BUTTONS
		//LOS CAMBIE PARA EL ARROWSPACING
	case EDIT_BUTTON_STOP_UP:
	case EDIT_BUTTON_STOP_DOWN:
		{
			//float fFactor = 0.5f;
			//float fArrowSpace = m_pPlayerStateEdit->m_TimingState.GetArrowSpacingAtBeat( m_pPlayerStateEdit->m_fSongBeat, fFactor );
			//float fDeltaSpace = 0;
			//switch( EditB )
			//{
			////DEFAULT_FAIL( EditB );
			//case EDIT_BUTTON_STOP_UP:	fDeltaSpace = +1.0f;	break;
			//case EDIT_BUTTON_STOP_DOWN:	fDeltaSpace = -1.0f;	break;
			//}

			//float fNewSpace = fArrowSpace + fDeltaSpace;

			//if( fNewSpace >= 0.0f || fNewSpace <= 128.0f )
			//	m_pPlayerStateEdit->m_TimingState.SetArrowSpacingAtBeat( m_pPlayerStateEdit->m_fSongBeat, fNewSpace, fFactor );
			//SetDirty( true );
		}
		break;
	case EDIT_BUTTON_OFFSET_UP:
	case EDIT_BUTTON_OFFSET_DOWN:
		{
			float fDelta = 0;
			switch( EditB )
			{
			//DEFAULT_FAIL( EditB );
			case EDIT_BUTTON_OFFSET_DOWN:	fDelta = -0.02f;	break;
			case EDIT_BUTTON_OFFSET_UP:	fDelta = +0.02f;	break;
			}
			if( EditIsBeingPressed( EDIT_BUTTON_ADJUST_FINE ) )
			{
				fDelta /= 20; /* 1ms */
			}
			else if( input.type == IET_REPEAT )
			{
				if( INPUTFILTER->GetSecsHeld(input.DeviceI) < 1.0f )
					fDelta *= 10;
				else
					fDelta *= 40;
			}
			m_pPlayerStateEdit->m_TimingState.m_fBeat0OffsetInSeconds += fDelta;
			(fDelta>0 ? m_soundValueIncrease : m_soundValueDecrease).Play();
			SetDirty( true );
		}
		break;
	case EDIT_BUTTON_SAMPLE_START_UP:
	case EDIT_BUTTON_SAMPLE_START_DOWN:
	case EDIT_BUTTON_SAMPLE_LENGTH_DOWN:
	case EDIT_BUTTON_SAMPLE_LENGTH_UP:
		{
			//MODIFICADO POR MI
			/*float fDelta;
			switch( EditB )
			{
			DEFAULT_FAIL( EditB );
			case EDIT_BUTTON_SAMPLE_START_DOWN:		fDelta = -0.02f;	break;
			case EDIT_BUTTON_SAMPLE_START_UP:		fDelta = +0.02f;	break;
			case EDIT_BUTTON_SAMPLE_LENGTH_DOWN:		fDelta = -0.02f;	break;
			case EDIT_BUTTON_SAMPLE_LENGTH_UP:		fDelta = +0.02f;	break;
			}
			
			if( input.type == IET_REPEAT )
			{
				if( INPUTFILTER->GetSecsHeld(input.DeviceI) < 1.0f )
					fDelta *= 10;
				else
					fDelta *= 40;
			}

			if( EditB == EDIT_BUTTON_SAMPLE_LENGTH_DOWN || EditB == EDIT_BUTTON_SAMPLE_LENGTH_UP )
			{
				m_pSong->m_fMusicSampleLengthSeconds += fDelta;
				m_pSong->m_fMusicSampleLengthSeconds = max(m_pSong->m_fMusicSampleLengthSeconds,0);
			}
			else
			{
				m_pSong->m_fMusicSampleStartSeconds += fDelta;
				m_pSong->m_fMusicSampleStartSeconds = max(m_pSong->m_fMusicSampleStartSeconds,0);
			}
			(fDelta>0 ? m_soundValueIncrease : m_soundValueDecrease).Play();
			SetDirty( true );*/
			int iDelta = 0;
			switch( EditB )
			{
			//DEFAULT_FAIL( EditB );
			case EDIT_BUTTON_SAMPLE_START_DOWN:		iDelta = -1;	break;
			case EDIT_BUTTON_SAMPLE_START_UP:		iDelta = +1;	break;
			/*case EDIT_BUTTON_SAMPLE_LENGTH_DOWN:		fDelta = -0.02f;	break;
			case EDIT_BUTTON_SAMPLE_LENGTH_UP:		fDelta = +0.02f;	break;*/
			}
			
			/*if( input.type == IET_REPEAT )
			{
				if( INPUTFILTER->GetSecsHeld(input.DeviceI) < 1.0f )
					fDelta *= 10;
				else
					fDelta *= 40;
			}*/
			m_iCurrentTapsAdd += iDelta;

			wrap( m_iCurrentTapsAdd, 21 );
			
			/*if( m_iCurrentTapsAdd == 0 )
			{
				SCREENMAN->SystemMessage( "Taps Fakes" );
			}
			else if( m_iCurrentTapsAdd == 1 )
			{
				SCREENMAN->SystemMessage( "Taps Hearts" );
			}
			else if( m_iCurrentTapsAdd == 2 )
			{
				SCREENMAN->SystemMessage( "Taps Hiddens" );
			}
			else if( m_iCurrentTapsAdd == 3 )
			{
				SCREENMAN->SystemMessage( "Taps Potions" );
			}
			else if( m_iCurrentTapsAdd == 4 )
			{
				SCREENMAN->SystemMessage( "Taps Vanish" );
			}
			else if( m_iCurrentTapsAdd == 5 )
			{
				SCREENMAN->SystemMessage( "Taps Item1x" );
			}
			else if( m_iCurrentTapsAdd == 6 )
			{
				SCREENMAN->SystemMessage( "Taps Item2x" );
			}
			else if( m_iCurrentTapsAdd == 7 )
			{
				SCREENMAN->SystemMessage( "Taps Item3x" );
			}
			else if( m_iCurrentTapsAdd == 8 )
			{
				SCREENMAN->SystemMessage( "Taps Item4x" );
			}
			else if( m_iCurrentTapsAdd == 9 )
			{
				SCREENMAN->SystemMessage( "Taps Item8x" );
			}
			else if( m_iCurrentTapsAdd == 10 )
			{
				SCREENMAN->SystemMessage( "Taps Sudden" );
			}
			else if( m_iCurrentTapsAdd == 11 )
			{
				SCREENMAN->SystemMessage( "Taps ItemLeft" );
			}
			else if( m_iCurrentTapsAdd == 12 )
			{
				SCREENMAN->SystemMessage( "Taps ItemRight" );
			}
			else if( m_iCurrentTapsAdd == 13 )
			{
				SCREENMAN->SystemMessage( "Taps ItemUnder" );
			}
			else if( m_iCurrentTapsAdd == 14 )
			{
				SCREENMAN->SystemMessage( "Taps ItemBlink" );
			}
			else if( m_iCurrentTapsAdd == 15 )
			{
				SCREENMAN->SystemMessage( "Taps ItemOverhead" );
			}
			else if( m_iCurrentTapsAdd == 16 )
			{
				SCREENMAN->SystemMessage( "Taps BrainShower" );
			}
			else if( m_iCurrentTapsAdd == 17 )
			{
				SCREENMAN->SystemMessage( "FakeHolds Mode" );
			}*/
		}
		break;
	case EDIT_BUTTON_PLAY_SAMPLE_MUSIC:
		PlayPreviewMusic();
		break;
	case EDIT_BUTTON_OPEN_BGCHANGE_LAYER1_MENU:
	case EDIT_BUTTON_OPEN_BGCHANGE_LAYER2_MENU:
		switch( EditB )
		{
		//DEFAULT_FAIL( EditB );
		case EDIT_BUTTON_OPEN_BGCHANGE_LAYER1_MENU: g_CurrentBGChangeLayer = BACKGROUND_LAYER_1; break;
		case EDIT_BUTTON_OPEN_BGCHANGE_LAYER2_MENU: g_CurrentBGChangeLayer = BACKGROUND_LAYER_2; break;
		}

		{
			//
			// Fill in option names
			//
			vector<RString> vThrowAway;

			MenuDef &menu = g_BackgroundChange;

			menu.rows[layer].choices[0] = ssprintf("%d",g_CurrentBGChangeLayer);
			BackgroundUtil::GetBackgroundTransitions(	"", vThrowAway, menu.rows[transition].choices );
			g_BackgroundChange.rows[transition].choices.insert( g_BackgroundChange.rows[transition].choices.begin(), "None" );	// add "no transition"
			BackgroundUtil::GetBackgroundEffects(		"", vThrowAway, menu.rows[effect].choices );
			menu.rows[effect].choices.insert( menu.rows[effect].choices.begin(), "Default" );	// add "default effect"

			BackgroundUtil::GetSongBGAnimations(   m_pSong, "", vThrowAway, menu.rows[file1_song_bganimation].choices );
			BackgroundUtil::GetSongMovies(         m_pSong, "", vThrowAway, menu.rows[file1_song_movie].choices );
			BackgroundUtil::GetSongBitmaps(        m_pSong, "", vThrowAway, menu.rows[file1_song_still].choices );
			BackgroundUtil::GetGlobalBGAnimations( m_pSong, "", vThrowAway, menu.rows[file1_global_bganimation].choices );	// NULL to get all background files
			BackgroundUtil::GetGlobalRandomMovies( m_pSong, "", vThrowAway, menu.rows[file1_global_movie].choices, false, false );	// all backgrounds
			BackgroundUtil::GetGlobalRandomMovies( m_pSong, "", vThrowAway, menu.rows[file1_global_movie_song_group].choices, false, true );	// song group's backgrounds
			BackgroundUtil::GetGlobalRandomMovies( m_pSong, "", vThrowAway, menu.rows[file1_global_movie_song_group_and_genre].choices, true, true );	// song group and genre's backgrounds

			menu.rows[file2_type].choices					= menu.rows[file1_type].choices;
			menu.rows[file2_song_bganimation].choices			= menu.rows[file1_song_bganimation].choices;
			menu.rows[file2_song_movie].choices				= menu.rows[file1_song_movie].choices;
			menu.rows[file2_song_still].choices				= menu.rows[file1_song_still].choices;
			menu.rows[file2_global_bganimation].choices			= menu.rows[file1_global_bganimation].choices;
			menu.rows[file2_global_movie].choices				= menu.rows[file1_global_movie].choices;
			menu.rows[file2_global_movie_song_group].choices		= menu.rows[file1_global_movie_song_group].choices;
			menu.rows[file2_global_movie_song_group_and_genre].choices	= menu.rows[file1_global_movie_song_group_and_genre].choices;
	

			//
			// Fill in line's enabled/disabled
			//
			bool bAlreadyBGChangeHere = false;
			BackgroundChange bgChange; 
			FOREACH( BackgroundChange, m_pSong->GetBackgroundChanges(g_CurrentBGChangeLayer), bgc )
			{
				if( bgc->m_fStartBeat == m_pPlayerStateEdit->m_fSongBeat )
				{
					bAlreadyBGChangeHere = true;
					bgChange = *bgc;
				}
			}

#define FILL_ENABLED( x )	menu.rows[x].bEnabled	= menu.rows[x].choices.size() > 0;
			FILL_ENABLED( transition );
			FILL_ENABLED( effect );
			FILL_ENABLED( file1_song_bganimation );
			FILL_ENABLED( file1_song_movie );
			FILL_ENABLED( file1_song_still );
			FILL_ENABLED( file1_global_bganimation );
			FILL_ENABLED( file1_global_movie );
			FILL_ENABLED( file1_global_movie_song_group );
			FILL_ENABLED( file1_global_movie_song_group_and_genre );
			FILL_ENABLED( file2_song_bganimation );
			FILL_ENABLED( file2_song_movie );
			FILL_ENABLED( file2_song_still );
			FILL_ENABLED( file2_global_bganimation );
			FILL_ENABLED( file2_global_movie );
			FILL_ENABLED( file2_global_movie_song_group );
			FILL_ENABLED( file2_global_movie_song_group_and_genre );
#undef FILL_ENABLED
			menu.rows[delete_change].bEnabled = bAlreadyBGChangeHere;


			// set default choices
			menu.rows[rate].					SetDefaultChoiceIfPresent( ssprintf("%2.0f%%",bgChange.m_fRate*100) );
			menu.rows[transition].					SetDefaultChoiceIfPresent( bgChange.m_sTransition );
			menu.rows[effect].					SetDefaultChoiceIfPresent( bgChange.m_def.m_sEffect );
			menu.rows[file1_type].iDefaultChoice			= none;
			if( bgChange.m_def.m_sFile1 == RANDOM_BACKGROUND_FILE )								menu.rows[file1_type].iDefaultChoice	= dynamic_random;
			if( menu.rows[file1_song_bganimation].			SetDefaultChoiceIfPresent( bgChange.m_def.m_sFile1 ) )	menu.rows[file1_type].iDefaultChoice	= song_bganimation;
			if( menu.rows[file1_song_movie].			SetDefaultChoiceIfPresent( bgChange.m_def.m_sFile1 ) )	menu.rows[file1_type].iDefaultChoice	= song_movie;
			if( menu.rows[file1_song_still].			SetDefaultChoiceIfPresent( bgChange.m_def.m_sFile1 ) )	menu.rows[file1_type].iDefaultChoice	= song_bitmap;
			if( menu.rows[file1_global_bganimation].		SetDefaultChoiceIfPresent( bgChange.m_def.m_sFile1 ) )	menu.rows[file1_type].iDefaultChoice	= global_bganimation;
			if( menu.rows[file1_global_movie].			SetDefaultChoiceIfPresent( bgChange.m_def.m_sFile1 ) )	menu.rows[file1_type].iDefaultChoice	= global_movie;
			if( menu.rows[file1_global_movie_song_group].		SetDefaultChoiceIfPresent( bgChange.m_def.m_sFile1 ) )	menu.rows[file1_type].iDefaultChoice	= global_movie_song_group;
			if( menu.rows[file1_global_movie_song_group_and_genre].	SetDefaultChoiceIfPresent( bgChange.m_def.m_sFile1 ) )	menu.rows[file1_type].iDefaultChoice	= global_movie_song_group_and_genre;
			menu.rows[file2_type].iDefaultChoice			= none;
			if( bgChange.m_def.m_sFile2 == RANDOM_BACKGROUND_FILE )								menu.rows[file2_type].iDefaultChoice	= dynamic_random;
			if( menu.rows[file2_song_bganimation].			SetDefaultChoiceIfPresent( bgChange.m_def.m_sFile2 ) )	menu.rows[file2_type].iDefaultChoice	= song_bganimation;
			if( menu.rows[file2_song_movie].			SetDefaultChoiceIfPresent( bgChange.m_def.m_sFile2 ) )	menu.rows[file2_type].iDefaultChoice	= song_movie;
			if( menu.rows[file2_song_still].			SetDefaultChoiceIfPresent( bgChange.m_def.m_sFile2 ) )	menu.rows[file2_type].iDefaultChoice	= song_bitmap;
			if( menu.rows[file2_global_bganimation].		SetDefaultChoiceIfPresent( bgChange.m_def.m_sFile2 ) )	menu.rows[file2_type].iDefaultChoice	= global_bganimation;
			if( menu.rows[file2_global_movie].			SetDefaultChoiceIfPresent( bgChange.m_def.m_sFile2 ) )	menu.rows[file2_type].iDefaultChoice	= global_movie;
			if( menu.rows[file2_global_movie_song_group].		SetDefaultChoiceIfPresent( bgChange.m_def.m_sFile2 ) )	menu.rows[file2_type].iDefaultChoice	= global_movie_song_group;
			if( menu.rows[file2_global_movie_song_group_and_genre].	SetDefaultChoiceIfPresent( bgChange.m_def.m_sFile2 ) )	menu.rows[file2_type].iDefaultChoice	= global_movie_song_group_and_genre;
			menu.rows[color1].					SetDefaultChoiceIfPresent( bgChange.m_def.m_sColor1 );
			menu.rows[color2].					SetDefaultChoiceIfPresent( bgChange.m_def.m_sColor2 );

			EditMiniMenu( &g_BackgroundChange, SM_BackFromBGChange );
		}
		break;

	case EDIT_BUTTON_OPEN_COURSE_MENU:
		{
			g_CourseMode.rows[0].choices.clear();
			g_CourseMode.rows[0].choices.push_back( CommonMetrics::LocalizeOptionItem("Off",false) );
			g_CourseMode.rows[0].iDefaultChoice = 0;

			vector<Course*> courses;
			SONGMAN->GetAllCourses( courses, false );
			for( unsigned i = 0; i < courses.size(); ++i )
			{
				const Course *crs = courses[i];

				bool bUsesThisSong = false;
				for( unsigned e = 0; e < crs->m_vEntries.size(); ++e )
				{
					if( crs->m_vEntries[e].songID.ToSong() != m_pSong )
						continue;
					bUsesThisSong = true;
				}

				if( bUsesThisSong )
				{
					g_CourseMode.rows[0].choices.push_back( crs->GetDisplayFullTitle() );
					if( crs == GAMESTATE->m_pCurCourse )
						g_CourseMode.rows[0].iDefaultChoice = g_CourseMode.rows[0].choices.size()-1;
				}
			}

			EditMiniMenu( &g_CourseMode, SM_BackFromCourseModeMenu );
		}
		break;
		//step attack menu
	case EDIT_BUTTON_OPEN_COURSE_ATTACK_MENU:
		{
			float start = -1;
			float end = -1;
			PlayerOptions po;
			
			if (m_NoteFieldEdit.m_iBeginMarker == -1) // not highlighted
			{
				po.FromString("");
			}
			else
			{
				TimingData &timing = m_pPlayerStateEdit->m_TimingState;
				start = timing.GetElapsedTimeFromBeat(NoteRowToBeat(m_NoteFieldEdit.m_iBeginMarker));
				AttackArray &attacks = m_pSteps->m_Attacks;
				int index = FindAttackAtTime(attacks, start);
				
				if (index >= 0)
				{
					po.FromString("");
				}
				if (m_NoteFieldEdit.m_iEndMarker == -1)
				{
					end = m_pSong->m_fMusicLengthSeconds;
				}
				else
				{
					end = timing.GetElapsedTimeFromBeat(NoteRowToBeat(m_NoteFieldEdit.m_iEndMarker));
				}
				
			}
			ModsGroup<PlayerOptions> &toEdit = GAMESTATE->m_pPlayerState[PLAYER_1]->m_PlayerOptions;
			//this->originalPlayerOptions.Assign(ModsLevel_Preferred, toEdit.GetPreferred());
			g_fLastInsertAttackPositionSeconds = start;
			g_fLastInsertAttackDurationSeconds = end - start;
			toEdit.Assign( ModsLevel_Stage, po );
			SCREENMAN->AddNewScreenToTop( "ScreenPlayerOptions", SM_BackFromInsertStepAttackPlayerOptions );
			break;
		}
	case EDIT_BUTTON_ADD_COURSE_MODS:
		{
			float fStart, fEnd;
			PlayerOptions po;
			const Course *pCourse = GAMESTATE->m_pCurCourse;
			if( pCourse == NULL )
				break;
			const CourseEntry &ce = pCourse->m_vEntries[GAMESTATE->m_iEditCourseEntryIndex];
			
			if( m_NoteFieldEdit.m_iBeginMarker == -1 )
			{				
				fStart = -1;
				fEnd = -1;
				po.FromString( ce.sModifiers );
			}
			else
			{
				fStart = m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat( NoteRowToBeat(m_NoteFieldEdit.m_iBeginMarker) );
				int iAttack = FindAttackAtTime( ce.attacks, fStart );
				
				if( iAttack >= 0 )
					po.FromString( ce.attacks[iAttack].sModifiers );
				
				if( m_NoteFieldEdit.m_iEndMarker == -1 )
					fEnd = m_pSong->m_fMusicLengthSeconds;
				else
					fEnd = m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat( NoteRowToBeat(m_NoteFieldEdit.m_iEndMarker) );
			}
			g_fLastInsertAttackPositionSeconds = fStart;
			g_fLastInsertAttackDurationSeconds = fEnd - fStart;
			GAMESTATE->m_pPlayerState[PLAYER_1]->m_PlayerOptions.Assign( ModsLevel_Stage, po );
			SCREENMAN->AddNewScreenToTop( "ScreenPlayerOptions", SM_BackFromInsertCourseAttackPlayerOptions );
			
		}
		break;
	case EDIT_BUTTON_BAKE_RANDOM_FROM_SONG_GROUP:
	case EDIT_BUTTON_BAKE_RANDOM_FROM_SONG_GROUP_AND_GENRE:
		{
			bool bTryGenre = EditB == EDIT_BUTTON_BAKE_RANDOM_FROM_SONG_GROUP_AND_GENRE;
			RString sName = GetOneBakedRandomFile(m_pSong, bTryGenre);
			if( sName.empty() )
			{
				SCREENMAN->PlayInvalidSound();
				SCREENMAN->SystemMessage( NO_BACKGROUNDS_AVAILABLE );
			}
			else
			{
				bool bAlreadyBGChangeHere = false;
				BackgroundLayer iLayer = BACKGROUND_LAYER_1;
				BackgroundChange bgChange;
				bgChange.m_fStartBeat = m_pPlayerStateEdit->m_fSongBeat;
				FOREACH( BackgroundChange, m_pSong->GetBackgroundChanges(iLayer), bgc )
				{
					if( bgc->m_fStartBeat == m_pPlayerStateEdit->m_fSongBeat )
					{
						bAlreadyBGChangeHere = true;
						bgChange = *bgc;
						m_pSong->GetBackgroundChanges(iLayer).erase( bgc );
						break;
					}
				}
				bgChange.m_def.m_sFile1 = sName;
				m_pSong->AddBackgroundChange( iLayer, bgChange );
				m_soundMarker.Play();
			}
		}
		break;

	case EDIT_BUTTON_PLAY_FROM_START:
		HandleMainMenuChoice( play_whole_song );
		break;

	case EDIT_BUTTON_PLAY_FROM_CURSOR:
		HandleMainMenuChoice( play_current_beat_to_end );
		break;

	case EDIT_BUTTON_PLAY_SELECTION:
		HandleMainMenuChoice( play_selection );
		break;
	case EDIT_BUTTON_RECORD_SELECTION:
		if( m_NoteFieldEdit.m_iBeginMarker!=-1 && m_NoteFieldEdit.m_iEndMarker!=-1 )
		{
			HandleAlterMenuChoice( record );
		}
		else
		{
			if( g_iDefaultRecordLength.Get() == -1 )
			{
				m_iStartPlayingAt = BeatToNoteRow(m_pPlayerStateEdit->m_fSongBeat);
				m_iStopPlayingAt = max( m_iStartPlayingAt, m_NoteDataEdit.GetLastRow() + 1 );
			}
			else
			{
				m_iStartPlayingAt = BeatToNoteRow( ftruncf(m_pPlayerStateEdit->m_fSongBeat, g_iDefaultRecordLength.Get()) );
				m_iStopPlayingAt = m_iStartPlayingAt + BeatToNoteRow( g_iDefaultRecordLength.Get() );
			}

			if( GAMESTATE->m_pCurSteps[0]->IsAnEdit() )
				m_iStopPlayingAt = min( m_iStopPlayingAt, BeatToNoteRow(GetMaximumBeatForNewNote()) );

			if( m_iStartPlayingAt >= m_iStopPlayingAt )
			{
				SCREENMAN->PlayInvalidSound();
				return;
			}

			TransitionEditState( STATE_RECORDING );
		}
		break;
	case EDIT_BUTTON_RECORD_FROM_CURSOR:
		m_iStartPlayingAt = BeatToNoteRow(m_pPlayerStateEdit->m_fSongBeat);
		m_iStopPlayingAt = max( m_iStartPlayingAt, m_NoteDataEdit.GetLastRow() );
		TransitionEditState( STATE_RECORDING );
		break;

	case EDIT_BUTTON_INSERT:
		HandleAreaMenuChoice( insert_and_shift );
		SCREENMAN->PlayInvalidSound();
		break;

	case EDIT_BUTTON_INSERT_SHIFT_PAUSES:
		HandleAreaMenuChoice( shift_pauses_forward );
		SCREENMAN->PlayInvalidSound();
		break;

	case EDIT_BUTTON_DELETE:
		HandleAreaMenuChoice( delete_and_shift );
		SCREENMAN->PlayInvalidSound();
		break;

	case EDIT_BUTTON_DELETE_SHIFT_PAUSES:
		HandleAreaMenuChoice( shift_pauses_backward );
		SCREENMAN->PlayInvalidSound();
		break;

	case EDIT_BUTTON_UNDO:
		Undo();
		break;
		
	case EDIT_BUTTON_SWITCH_PLAYERS:
		if( m_InputPlayerNumber != PLAYER_INVALID )
		{
			enum_add( m_InputPlayerNumber, 1 );
			if( m_InputPlayerNumber == NUM_PLAYERS )
				m_InputPlayerNumber = PLAYER_1;
		}
		break;
	}
}

void ScreenEdit::InputRecord( const InputEventPlus &input, EditButton EditB )
{
	if( input.type == IET_FIRST_PRESS  &&  EditB == EDIT_BUTTON_RETURN_TO_EDIT )
	{
		TransitionEditState( STATE_EDITING );
		return;
	}	

	if( input.pn != PLAYER_1 )
		return;		// ignore

	const int iCol = GAMESTATE->GetCurrentStyle()->GameInputToColumn( input.GameI );

	switch( input.type )
	{
	//DEFAULT_FAIL( input.type );
	case IET_FIRST_PRESS:
		{
			if( EditIsBeingPressed(EDIT_BUTTON_REMOVE_NOTE) )
			{
				// Remove notes in Update.
				break;
			}

			// Add a tap
			float fBeat = m_pPlayerStateEdit->m_fSongBeat;
			fBeat = Quantize( fBeat, NoteTypeToBeat(m_SnapDisplay.GetNoteType()) );
			
			const int iRow = BeatToNoteRow( fBeat );
			if( iRow < 0 )
				break;

			// Don't add outside of the range.
			if( iRow < m_iStartPlayingAt || iRow >= m_iStopPlayingAt )
				return;

			// Remove hold if any so that we don't have taps inside of a hold.
			int iHeadRow;
			if( m_NoteDataRecord.IsHoldNoteAtRow( iCol, iRow, &iHeadRow ) )
				m_NoteDataRecord.SetTapNote( iCol, iHeadRow, TAP_EMPTY );

			TapNote tn = TAP_ORIGINAL_TAP;
			if( EditIsBeingPressed(EDIT_BUTTON_LAY_MINE_OR_ROLL) )
				tn = TAP_ORIGINAL_MINE;
			else if( EditIsBeingPressed(EDIT_BUTTON_LAY_LIFT) )
			{
				m_soundAddNote.Play();
				SetDirty( true );
				SaveUndo();
				TapNote tn;
				if( m_iCurrentTapsAdd == 0 )
				{
					tn = TAP_ORIGINAL_FAKE;////fake modificado por mi
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::fake, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::fake, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::fake, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::fake, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 1 )
				{
					tn = TAP_ORIGINAL_HEART;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::heart, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::heart, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::heart, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::heart, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 2 )
				{
					tn = TAP_ORIGINAL_HIDDEN;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::hidden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::hidden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::hidden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::hidden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 3 )
				{
					tn = TAP_ORIGINAL_POTION;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::potion, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::potion, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::potion, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::potion, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 4 )
				{
					tn = TAP_ORIGINAL_VANISH;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::vanish, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::vanish, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::vanish, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::vanish, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 5 )
				{
					tn = TAP_ORIGINAL_ITEM1X;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::item1x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::item1x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::item1x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::item1x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 6 )
				{
					tn = TAP_ORIGINAL_ITEM2X;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::item2x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::item2x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::item2x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::item2x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 7 )
				{
					tn = TAP_ORIGINAL_ITEM3X;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::item3x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::item3x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::item3x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::item3x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 8 )
				{
					tn = TAP_ORIGINAL_ITEM4X;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::item4x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::item4x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::item4x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::item4x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 9 )
				{
					tn = TAP_ORIGINAL_ITEM8X;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::item8x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::item8x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::item8x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::item8x, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 10 )
				{
					tn = TAP_ORIGINAL_SUDDEN;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::sudden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::sudden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::sudden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::sudden, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 11 )
				{
					tn = TAP_ORIGINAL_ITEMLEFT;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::itemleft, TapNote::SubType_Invalid, TapNote::original, "turnleft,*9999 no reverse", 9999.0f, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::itemleft, TapNote::SubType_Invalid, TapNote::original, "turnleft,*9999 no reverse", 9999.0f, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::itemleft, TapNote::SubType_Invalid, TapNote::original, "turnleft,*9999 no reverse", 9999.0f, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::itemleft, TapNote::SubType_Invalid, TapNote::original, "turnleft,*9999 no reverse", 9999.0f, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 12 )
				{
					tn = TAP_ORIGINAL_ITEMRIGHT;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::itemright, TapNote::SubType_Invalid, TapNote::original, "turnright,*9999 no reverse", 9999.0f, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::itemright, TapNote::SubType_Invalid, TapNote::original, "turnright,*9999 no reverse", 9999.0f, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::itemright, TapNote::SubType_Invalid, TapNote::original, "turnright,*9999 no reverse", 9999.0f, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::itemright, TapNote::SubType_Invalid, TapNote::original, "turnright,*9999 no reverse", 9999.0f, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 13 )
				{
					tn = TAP_ORIGINAL_ITEMUNDER;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::itemunder, TapNote::SubType_Invalid, TapNote::original, "*9999 reverse", 9999.0f, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::itemunder, TapNote::SubType_Invalid, TapNote::original, "*9999 reverse", 9999.0f, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::itemunder, TapNote::SubType_Invalid, TapNote::original, "*9999 reverse", 9999.0f, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::itemunder, TapNote::SubType_Invalid, TapNote::original, "*9999 reverse", 9999.0f, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 14 )
				{
					tn = TAP_ORIGINAL_ITEMBLINK;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::itemblink, TapNote::SubType_Invalid, TapNote::original, "blink", 9999.0f, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::itemblink, TapNote::SubType_Invalid, TapNote::original, "blink", 9999.0f, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::itemblink, TapNote::SubType_Invalid, TapNote::original, "blink", 9999.0f, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::itemblink, TapNote::SubType_Invalid, TapNote::original, "blink", 9999.0f, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 15 )
				{
					tn = TAP_ORIGINAL_ITEMOVERHEAD;
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::itemoverhead, TapNote::SubType_Invalid, TapNote::original, "turnoverhead,no blink,*9999 no reverse", 9999.0f, -1, false, "", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::itemoverhead, TapNote::SubType_Invalid, TapNote::original, "turnoverhead,no blink,*9999 no reverse", 9999.0f, -1, false, "", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::itemoverhead, TapNote::SubType_Invalid, TapNote::original, "turnoverhead,no blink,*9999 no reverse", 9999.0f, -1, false, "", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::itemoverhead, TapNote::SubType_Invalid, TapNote::original, "turnoverhead,no blink,*9999 no reverse", 9999.0f, -1, false, "", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 16 )
				{
					tn = TapNote( TapNote::answer, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "   ", "", "" );
					if( m_iCurrentTapMode != 0 )
					{
						switch( m_iCurrentTapMode )
						{
						case 1:
							tn = TapNote( TapNote::answer, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "   ", "Vanish", "" );
							break;
						case 2:
							tn = TapNote( TapNote::answer, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "   ", "Sudden", "" );
							break;
						case 3:
							tn = TapNote( TapNote::answer, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "   ", "NonStep", "" );
							break;
						case 4:
							tn = TapNote( TapNote::answer, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "   ", "Drunk", "" );
							break;
						default:
							break;
						}
					}
				}
				else if( m_iCurrentTapsAdd == 18 )
				{
					//tn = TAP_ORIGINAL_WILD;		
					g_iLastInsertDivisionTapTrack = iCol;
					ScreenTextEntry::TextEntry( SM_WriteWildLabel, "Insert the beat to GOTO and beat to LAND, like 75.5/101.5", "",255,NULL );
				}
				else if( m_iCurrentTapsAdd == 19 )
				{
					//tn = TAP_ORIGINAL_GROOVE;	
					g_iLastInsertDivisionTapTrack = iCol;
					ScreenTextEntry::TextEntry( SM_WriteGrooveLabel, "Insert the beat to GOTO and beat to LAND, like 75.5/101.5", "",255,NULL );
				}
			}

			if( m_iCurrentTapMode != 0 )
			{
				switch( m_iCurrentTapMode )
				{
				case 1:
					tn = TapNote( TapNote::tap, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
					break;
				case 2:
					tn = TapNote( TapNote::tap, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
					break;
				case 3:
					tn = TapNote( TapNote::tap, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
					break;
				case 4:
					tn = TapNote( TapNote::tap, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
					break;
				default:
					break;
				}
			}
			if( THEME->HasMetric( "NoteSkinsDef", m_sCurrentNoteSkin ) ) 
			{										
				tn.sNoteSkin = m_sCurrentNoteSkin;
				ThemeMetric<RString> skin ( "NoteSkinsDef", tn.sNoteSkin );
				skin.Load( "NoteSkinsDef", tn.sNoteSkin);
				tn.sChar = skin.GetValue();
			}
			tn.pn = m_InputPlayerNumber;
			m_NoteDataRecord.SetTapNote( iCol, iRow, tn );
			m_NoteFieldRecord.Step( iCol, TNS_W1 );
		}
		break;
	case IET_REPEAT:
	case IET_RELEASE:
		// don't add or extend holds here; we do it in Update()
		break;
	}
}

void ScreenEdit::InputRecordPaused( const InputEventPlus &input, EditButton EditB )
{
	if( input.type != IET_FIRST_PRESS )
		return;		// don't care

	switch( EditB )
	{
	case EDIT_BUTTON_UNDO:
		/* We've already actually committed changes to m_NoteDataEdit, so all we have
		 * to do to undo is Undo() as usual, and copy the note data back in. */
		Undo();
		m_NoteDataRecord.CopyAll( m_NoteDataEdit );
		break;

	case EDIT_BUTTON_PLAY_SELECTION:
		TransitionEditState( STATE_PLAYING );
		break;

	case EDIT_BUTTON_RECORD_SELECTION:
		TransitionEditState( STATE_RECORDING );
		break;

	case EDIT_BUTTON_RECORD_FROM_CURSOR:
		if( GAMESTATE->m_pCurSteps[0]->IsAnEdit() )
		{
			float fMaximumBeat = GetMaximumBeatForNewNote();
			if( NoteRowToBeat(m_iStopPlayingAt) >= fMaximumBeat )
			{
				// We're already at the end.
				SCREENMAN->PlayInvalidSound();
				return;
			}
		}

		/* Pick up where we left off. */
		{
			int iSize = m_iStopPlayingAt - m_iStartPlayingAt;
			m_iStartPlayingAt = m_iStopPlayingAt;
			m_iStopPlayingAt += iSize;

			if( GAMESTATE->m_pCurSteps[0]->IsAnEdit() )
				m_iStopPlayingAt = min( m_iStopPlayingAt, BeatToNoteRow(GetMaximumBeatForNewNote()) );
		}

		TransitionEditState( STATE_RECORDING );
		break;

	case EDIT_BUTTON_RETURN_TO_EDIT:
		TransitionEditState( STATE_EDITING );
		break;
	}
}

void ScreenEdit::InputPlay( const InputEventPlus &input, EditButton EditB )
{
	switch( input.type )
	{
	case IET_RELEASE:
	case IET_FIRST_PRESS:
		break;
	default:
		return;
	}

	GameButtonType gbt = GAMESTATE->m_pCurGame->GetPerButtonInfo(input.GameI.button)->m_gbt;

	if( GamePreferences::m_AutoPlay == PC_HUMAN )
	{
		const int iCol = GAMESTATE->GetCurrentStyle()->GameInputToColumn( input.GameI );
		bool bRelease = input.type == IET_RELEASE;
		switch( input.pn )
		{
		case PLAYER_1:
			{
				switch( gbt )
				{
				case GameButtonType_INVALID:
					break;
				case GameButtonType_Step:
					if( iCol != -1 )
						m_Player->Step( iCol, -1, input.DeviceI.ts, false, bRelease );
					break;
				case GameButtonType_Fret:
					if( iCol != -1 )
						m_Player->Fret( iCol, -1, input.DeviceI.ts, false, bRelease );
					break;
				case GameButtonType_Strum:
					m_Player->Strum( iCol, -1, input.DeviceI.ts, false, bRelease );
					break;
				}
			}
			break;
		case PLAYER_2:
			if( GAMESTATE->GetCurrentStyle()->m_StyleType == StyleType_TwoPlayersSharedSides )
				m_Player->Step( iCol, -1, input.DeviceI.ts, false, input.type == IET_RELEASE );
		}
	}

	if( gbt == GameButtonType_INVALID  &&  input.type == IET_FIRST_PRESS )
	{
		switch( EditB )
		{
		case EDIT_BUTTON_RETURN_TO_EDIT:
			/* When exiting play mode manually, leave the cursor where it is. */
			m_fBeatToReturnTo = m_pPlayerStateEdit->m_fSongBeat;
			TransitionEditState( STATE_EDITING );
			break;
		case EDIT_BUTTON_OFFSET_UP:
		case EDIT_BUTTON_OFFSET_DOWN:
		{
			float fOffsetDelta = 0;
			switch( EditB )
			{
				//DEFAULT_FAIL( EditB );
				case EDIT_BUTTON_OFFSET_DOWN:	fOffsetDelta = -0.020f;		break;
				case EDIT_BUTTON_OFFSET_UP:	fOffsetDelta = +0.020f;		break;
			}
			
			if( EditIsBeingPressed( EDIT_BUTTON_ADJUST_FINE ) )
			{
				fOffsetDelta /= 20; /* 1ms */
			}
			else if( input.type == IET_REPEAT )
			{
				if( INPUTFILTER->GetSecsHeld(input.DeviceI) < 1.0f )
					fOffsetDelta *= 10;
				else
					fOffsetDelta *= 40;
			}
			
			m_pPlayerStateEdit->m_TimingState.m_fBeat0OffsetInSeconds += fOffsetDelta;
		}
			break;
		}
	}
}

void ScreenEdit::TransitionEditState( EditState em )
{
	EditState old = m_EditState;
	
	// If we're going from recording to paused, come back when we're done.
	if( old == STATE_RECORDING_PAUSED && em == STATE_PLAYING )
		m_bReturnToRecordMenuAfterPlay = true;

	const bool bStateChanging = em != old;

#if 0
	//
	// If switching out of record, open the menu.
	//
	{
		bool bGoToRecordMenu = (old == STATE_RECORDING);
		if( m_bReturnToRecordMenuAfterPlay && old == STATE_PLAYING )
		{
			bGoToRecordMenu = true;
			m_bReturnToRecordMenuAfterPlay = false;
		}
		
		if( bGoToRecordMenu )
			em = STATE_RECORDING_PAUSED;
	}
#endif

	/* If we're playing music, sample music or assist ticks when changing modes, stop. */
	SOUND->StopMusic();
	if( m_pSoundMusic )
		m_pSoundMusic->StopPlaying();
	m_GameplayAssist.StopPlaying();
	GAMESTATE->m_bGameplayLeadIn.Set( true );

	static TimingData oldTiming;
	static RString options;

	if( bStateChanging )
	{
		switch( old )
		{
		case STATE_EDITING:
			// If exiting EDIT mode, save the cursor position.
			m_fBeatToReturnTo = m_pPlayerStateEdit->m_fSongBeat;
			GAMESTATE->m_bEffectsInEditor = false;
			oldTiming = m_pPlayerStateEdit->m_TimingState;
			options = m_pPlayerStateEdit->m_PlayerOptions.GetCurrent().GetString(true);
			break;

		case STATE_PLAYING:
			//AdjustSync::HandleSongEnd();
			//if( AdjustSync::IsSyncDataChanged() )
			//	ScreenSaveSync::PromptSaveSync();
			GAMESTATE->m_bEffectsInEditor = true;
			m_pPlayerStateEdit->m_TimingState = oldTiming;
			m_pPlayerStageStats->m_ComboList.clear();
			m_pPlayerStageStats->m_iCurCombo = 0;
			m_pPlayerStageStats->m_iCurMissCombo = 0;
			break;

		case STATE_RECORDING:
			SetDirty( true );
			SaveUndo();

			// delete old TapNotes in the range
			m_NoteDataEdit.ClearRange( m_iStartPlayingAt, m_iStopPlayingAt );
			m_NoteDataEdit.CopyRange( m_NoteDataRecord, m_iStartPlayingAt, m_iStopPlayingAt, m_iStartPlayingAt );
			m_NoteDataRecord.ClearAll();

			CheckNumberOfNotesAndUndo();
			break;
		}
	}

	//
	// Set up player options for this mode.  (EDITING uses m_pPlayerStateEdit, which we
	// don't need to change.)
	//
	if( em != STATE_EDITING )
	{
		// Stop displaying course attacks, if any.
		m_pPlayerStateEdit->RemoveActiveAttacks();
		// Load the player's default PlayerOptions.
		m_pPlayerStateEdit->RebuildPlayerOptionsFromActiveAttacks();

		switch( em )
		{
		case STATE_RECORDING:
		case STATE_RECORDING_PAUSED:
			PO_GROUP_ASSIGN_N( m_pPlayerStateEdit->m_PlayerOptions, ModsLevel_Stage, m_fScrolls, PlayerOptions::SCROLL_CENTERED, 1.0f );
			break;
		}

		/* Snap to current options. */
		m_pPlayerStateEdit->m_PlayerOptions.SetCurrentToLevel( ModsLevel_Stage );
	}

	//hace que los attacks se detengan y no sigan despues de volver de jugar
	//y no queden activados.
	if( em == STATE_EDITING && old == STATE_PLAYING )
	{
		// Stop displaying course attacks, if any.
		m_pPlayerStateEdit->RemoveActiveAttacks();
		// Load the player's default PlayerOptions.
		m_pPlayerStateEdit->RebuildPlayerOptionsFromActiveAttacks();

		/* Snap to current options. */
		//m_pPlayerStateEdit->m_PlayerOptions.SetCurrentToLevel( ModsLevel_Stage );
		
		//default player options para cuando cambiamos los mods, por el menu opciones
		PlayerOptions po;
		po.FromString( options );
		//GAMESTATE->GetDefaultPlayerOptions( po );
		m_pPlayerStateEdit->m_PlayerOptions.Assign( ModsLevel_Stage, po );
	}

	switch( em )
	{
	//DEFAULT_FAIL( em );
	case STATE_EDITING:
		/* Important: people will stop playing, change the BG and start again; make sure we reload */
		m_Background.Unload();
		m_Foreground.Unload();

		/* Restore the cursor position. */
		m_pPlayerStateEdit->m_fSongBeat = m_fBeatToReturnTo;

		/* Make sure we're snapped. */
		m_pPlayerStateEdit->m_fSongBeat = Quantize( m_pPlayerStateEdit->m_fSongBeat, NoteTypeToBeat(m_SnapDisplay.GetNoteType()) );

		/* Playing and recording have lead-ins, which may start before beat 0;
		 * make sure we don't stay there if we escaped out early. */
		m_pPlayerStateEdit->m_fSongBeat = max( m_pPlayerStateEdit->m_fSongBeat, 0 );

		break;

	case STATE_PLAYING:
	case STATE_RECORDING:
	{
		//if( bStateChanging )
		//	AdjustSync::ResetOriginalSyncData();

		/* Give a 1 second lead-in.  If we're loading Player, this must be done first. */
		float fSeconds = m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat( NoteRowToBeat(m_iStartPlayingAt) ) - 1;
		GAMESTATE->UpdateSongPosition( fSeconds, m_pPlayerStateEdit->m_TimingState );
		m_pPlayerStateEdit->UpdateSongPosition( fSeconds, m_pPlayerStateEdit->m_TimingState );

		GAMESTATE->m_bGameplayLeadIn.Set( false );

		m_pPlayerStageStats->m_ComboList.clear();
		m_pPlayerStageStats->m_iCurCombo = 0;
		m_pPlayerStageStats->m_iCurMissCombo = 0;

		/* Reset the note skin, in case preferences have changed. */
		// XXX
		// GAMESTATE->ResetNoteSkins();
		//GAMESTATE->res

		break;
	}
	case STATE_RECORDING_PAUSED:
		break;
	}

	switch( em )
	{
	case STATE_PLAYING:
		/* If we're in course display mode, set that up. */
		SetupCourseAttacks();
		
		m_Player.Load( m_NoteDataEdit );
		GAMESTATE->m_pPlayerState[PLAYER_1]->m_PlayerController = GamePreferences::m_AutoPlay;

		if( g_bEditorShowBGChangesPlay )
		{
			/* FirstBeat affects backgrounds, so commit changes to memory (not to disk)
			 * and recalc it. */
			Steps* pSteps = GAMESTATE->m_pCurSteps[PLAYER_1];
			ASSERT( pSteps );
			pSteps->SetNoteData( m_NoteDataEdit );
			m_pSong->ReCalculateRadarValuesAndLastBeat();

			m_Background.Unload();
			m_Background.LoadFromSong( m_pSong );

			m_Foreground.Unload();
			m_Foreground.LoadFromSong( m_pSong );
		}

		m_pPlayerStateEdit->m_PlayerOptions.FromString(
			ModsLevel_Stage, 
			GAMESTATE->m_pPlayerState[PLAYER_1]->m_PlayerOptions.GetCurrent().GetString() );

		break;
	case STATE_RECORDING:
	case STATE_RECORDING_PAUSED:
		// initialize m_NoteFieldRecord
		m_NoteDataRecord.CopyAll( m_NoteDataEdit );

		// highlight the section being recorded
		m_NoteFieldRecord.m_iBeginMarker = m_iStartPlayingAt;
		m_NoteFieldRecord.m_iEndMarker = m_iStopPlayingAt;

		break;
	}

	//
	// Show/hide depending on em
	//
	m_sprOverlay->PlayCommand( EditStateToString(em) );
	m_sprUnderlay->PlayCommand( EditStateToString(em) );

	m_Background.SetVisible( g_bEditorShowBGChangesPlay  &&  em != STATE_EDITING );
	m_autoHeader->SetVisible( em == STATE_EDITING );
	m_textInputTips.SetVisible( em == STATE_EDITING );
	m_textInfo.SetVisible( em == STATE_EDITING );
	// Play the OnCommands again so that these will be re-hidden if the OnCommand hides them.
	if( em == STATE_EDITING )
	{
		m_textInputTips.PlayCommand( "On" );
		m_textInfo.PlayCommand( "On" );
	}
	m_textPlayRecordHelp.SetVisible( em != STATE_EDITING );
	m_SnapDisplay.SetVisible( em == STATE_EDITING );
	m_NoteFieldEdit.SetVisible( em == STATE_EDITING );
	m_NoteFieldRecord.SetVisible( em == STATE_RECORDING  ||  em == STATE_RECORDING_PAUSED );
	m_Player->SetVisible( em == STATE_PLAYING );
	m_Foreground.SetVisible( g_bEditorShowBGChangesPlay  &&  em != STATE_EDITING );

	switch( em )
	{
	case STATE_PLAYING:
	case STATE_RECORDING:
		const float fStartSeconds = m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat(m_pPlayerStateEdit->m_fSongBeat);
		LOG->Trace( "Starting playback at %f", fStartSeconds );

		RageSoundParams p;
		p.m_fSpeed = GAMESTATE->m_SongOptions.GetCurrent().m_fMusicRate;
		p.m_StartSecond = fStartSeconds;
		p.StopMode = RageSoundParams::M_CONTINUE;
		m_pSoundMusic->SetProperty( "AccurateSync", true );
		m_pSoundMusic->Play( &p );
		break;
	}

	m_EditState = em;
}

void ScreenEdit::ScrollTo( float fDestinationBeat )
{
	CLAMP( fDestinationBeat, 0, GetMaximumBeatForMoving() );

	// Don't play the sound and do the hold note logic below if our position didn't change.
	const float fOriginalBeat = m_pPlayerStateEdit->m_fSongBeat;
	if( fOriginalBeat == fDestinationBeat )
		return;

	m_pPlayerStateEdit->m_fSongBeat = fDestinationBeat;

	// check to see if they're holding a button
	for( int n=0; n<NUM_EDIT_BUTTON_COLUMNS; n++ )
	{
		int iCol = n;

		// Ctrl + number = input to right half
		if( EditIsBeingPressed(EDIT_BUTTON_RIGHT_SIDE) )
			ShiftToRightSide( iCol, m_NoteDataEdit.GetNumTracks() );

		if( iCol >= m_NoteDataEdit.GetNumTracks() )
			continue;	// skip

		EditButton b = EditButton(EDIT_BUTTON_COLUMN_0+n);
		if( !EditIsBeingPressed(b) )
			continue;

		// create a new hold note
		int iStartRow = BeatToNoteRow( min(fOriginalBeat, fDestinationBeat) );
		int iEndRow = BeatToNoteRow( max(fOriginalBeat, fDestinationBeat) );

		// Don't SaveUndo.  We want to undo the whole hold, not just the last segment
		// that the user made.  Dragging the hold bigger can only absorb and remove
		// other taps, so dragging won't cause us to exceed the note limit.
		TapNote tn = EditIsBeingPressed(EDIT_BUTTON_LAY_MINE_OR_ROLL) ? TAP_ORIGINAL_ROLL_HEAD : TAP_ORIGINAL_HOLD_HEAD;
		
		if( m_iCurrentTapsAdd == 17 ) //fakes holds or rolls
			tn.bFakeHold = true;

		if( THEME->HasMetric( "NoteSkinsDef", m_sCurrentNoteSkin ) ) 
		{										
			tn.sNoteSkin = m_sCurrentNoteSkin;
			ThemeMetric<RString> skin ( "NoteSkinsDef", tn.sNoteSkin );
			skin.Load( "NoteSkinsDef", tn.sNoteSkin);
			tn.sChar = skin.GetValue();
		}
		
		tn.pn = m_InputPlayerNumber;
		m_NoteDataEdit.AddHoldNote( iCol, iStartRow, iEndRow, tn );
	}

	if( EditIsBeingPressed(EDIT_BUTTON_SCROLL_SELECT) )
	{
		/* Shift is being held. 
		 *
		 * If this is the first time we've moved since shift was depressed,
		 * the old position (before this move) becomes the start pos: */
		int iDestinationRow = BeatToNoteRow( fDestinationBeat );
		if( m_iShiftAnchor == -1 )
			m_iShiftAnchor = BeatToNoteRow(fOriginalBeat);
		
		if( iDestinationRow == m_iShiftAnchor )
		{
			/* We're back at the anchor, so we have nothing selected. */
			m_NoteFieldEdit.m_iBeginMarker = m_NoteFieldEdit.m_iEndMarker = -1;
		}
		else
		{
			m_NoteFieldEdit.m_iBeginMarker = m_iShiftAnchor;
			m_NoteFieldEdit.m_iEndMarker = iDestinationRow;
			if( m_NoteFieldEdit.m_iBeginMarker > m_NoteFieldEdit.m_iEndMarker )
				swap( m_NoteFieldEdit.m_iBeginMarker, m_NoteFieldEdit.m_iEndMarker );
		}
	}

	m_soundChangeLine.Play();
}

void ScreenEdit::HandleMessage( const Message &msg )
{
	if( msg == "Judgment" )
	{
		PlayerNumber pn;
		msg.GetParam( "Player", pn );

		if( GAMESTATE->m_pPlayerState[PLAYER_1]->m_PlayerOptions.GetCurrent().m_bMuteOnError )
		{
			RageSoundReader *pSoundReader = m_AutoKeysounds.GetPlayerSound( pn );
			if( pSoundReader == NULL )
				pSoundReader = m_AutoKeysounds.GetSharedSound();

			HoldNoteScore hns;
			msg.GetParam( "HoldNoteScore", hns );
			TapNoteScore tns;
			msg.GetParam( "TapNoteScore", tns );

			bool bOn = false;
			if( hns != HoldNoteScore_Invalid )
				bOn = hns != HNS_LetGo;
			else
				bOn = tns != TNS_Miss;

			if( pSoundReader )
				pSoundReader->SetProperty( "Volume", bOn? 1.0f:0.0f );
		}
	}
	if( msg == Message_SongModified )
	{
		SetDirty( true );
	}
	Screen::HandleMessage( msg );
}

static LocalizedString SAVE_SUCCESSFUL				( "ScreenEdit", "Save successful." );

void ScreenEdit::HandleScreenMessage( const ScreenMessage SM )
{
	if( SM != SM_UpdateTextInfo )
		m_bTextInfoNeedsUpdate = true;

	if( SM == SM_UpdateTextInfo )
	{
		UpdateTextInfo();
		this->PostScreenMessage( SM_UpdateTextInfo, 0.1f );
	}
	else if( SM == SM_GoToNextScreen )
	{
		GAMESTATE->m_EditMode = EditMode_Invalid;
	}
	else if( SM == SM_BackFromMainMenu )
	{
		HandleMainMenuChoice( (MainMenuChoice)ScreenMiniMenu::s_iLastRowCode, ScreenMiniMenu::s_viLastAnswers );
	}
	else if( SM == SM_BackFromAreaMenu )
	{
		HandleAreaMenuChoice( (AreaMenuChoice)ScreenMiniMenu::s_iLastRowCode, ScreenMiniMenu::s_viLastAnswers );
	}
	else if( SM == SM_BackFromStepsInformation )
	{
		HandleStepsInformationChoice( (StepsInformationChoice)ScreenMiniMenu::s_iLastRowCode, ScreenMiniMenu::s_viLastAnswers );
	}
	else if( SM == SM_BackFromSongInformation )
	{
		HandleSongInformationChoice( (SongInformationChoice)ScreenMiniMenu::s_iLastRowCode, ScreenMiniMenu::s_viLastAnswers );
	}
	else if( SM == SM_BackFromBPMChange )
	{
		float fBPM = StringToFloat( ScreenTextEntry::s_sLastAnswer );
		//if( fBPM > 0 )
		m_pPlayerStateEdit->m_TimingState.SetBPMAtBeat( m_pPlayerStateEdit->m_fSongBeat, fBPM );
			//m_pPlayerStateEdit->m_TimingState.SetBPMAtBeat( m_pPlayerStateEdit->m_fSongBeat, fBPM );
		SetDirty( true );
	}
	else if( SM == SM_BackFromStopChange )
	{
		float fStop = StringToFloat( ScreenTextEntry::s_sLastAnswer );
		if( fStop >= 0 )
			m_pPlayerStateEdit->m_TimingState.SetStopAtBeat( m_pPlayerStateEdit->m_fSongBeat, fStop );
		SetDirty( true );
	}
	else if( SM == SM_BackFromOffsetChange )
	{		
		TimingData &timing = m_pPlayerStateEdit->m_TimingState;
		float old = timing.m_fBeat0OffsetInSeconds;
		timing.m_fBeat0OffsetInSeconds = StringToFloat( ScreenTextEntry::s_sLastAnswer );
		float delta = timing.m_fBeat0OffsetInSeconds - old;
		/*if (GAMESTATE->m_bIsUsingStepTiming)
		{
			GAMESTATE->m_pCurSteps[PLAYER_1]->m_Attacks.UpdateStartTimes(delta);
		}
		else
		{
			GAMESTATE->m_pCurSong->m_Attacks.UpdateStartTimes(delta);
			GAMESTATE->m_pCurSong->m_fMusicSampleStartSeconds += delta;
		}*/
		GAMESTATE->m_pCurSteps[PLAYER_1]->m_Attacks.UpdateStartTimes(delta);
		SetDirty( true );
	}
	else if( SM == SM_BackFromBGChange )
	{
		HandleBGChangeChoice( (BGChangeChoice)ScreenMiniMenu::s_iLastRowCode, ScreenMiniMenu::s_viLastAnswers );
		SetDirty( true );
	}
	else if( SM == SM_BackFromCourseModeMenu )
	{
		const int num = ScreenMiniMenu::s_viLastAnswers[0];
		GAMESTATE->m_pCurCourse.Set( NULL );
		if( num != 0 )
		{
			const RString name = g_CourseMode.rows[0].choices[num];
			Course *pCourse = SONGMAN->FindCourse( name );

			int iCourseEntryIndex = -1;
			FOREACH_CONST( CourseEntry, pCourse->m_vEntries, i )
			{
				if( i->songID.ToSong() == GAMESTATE->m_pCurSong.Get() )
					iCourseEntryIndex = i - pCourse->m_vEntries.begin();
			}

			ASSERT( iCourseEntryIndex != -1 );

			GAMESTATE->m_pCurCourse.Set( pCourse );
			GAMESTATE->m_iEditCourseEntryIndex.Set( iCourseEntryIndex );
			ASSERT( GAMESTATE->m_pCurCourse );
		}
	}
	else if( SM == SM_BackFromOptions )
	{
		/*SCREENMAN->SystemMessageNoAnimate( 
			ssprintf( "GAMESTATE: %s, PlayerState: %s", 
				GAMESTATE->m_pPlayerState[PLAYER_1]->m_PlayerOptions.GetCurrent().GetString().c_str(), 
				m_pPlayerStateEdit->m_PlayerOptions.GetCurrent().GetString().c_str() ) );*/
		
		// The options may have changed the note skin.
		m_NoteFieldRecord.CacheAllUsedNoteSkins();
		m_Player->CacheAllUsedNoteSkins();

		// stop any music that screen may have been playing
		SOUND->StopMusic();
	}
	else if( SM == SM_BackFromInsertTapAttack )
	{
		int iDurationChoice = ScreenMiniMenu::s_viLastAnswers[0];
		g_fLastInsertAttackDurationSeconds = StringToFloat( g_InsertTapAttack.rows[0].choices[iDurationChoice] );

		SCREENMAN->AddNewScreenToTop( "ScreenPlayerOptions", SM_BackFromInsertTapAttackPlayerOptions );
	}
	else if( SM == SM_BackFromInsertTapAttackPlayerOptions )
	{
		PlayerOptions poChosen = GAMESTATE->m_pPlayerState[PLAYER_1]->m_PlayerOptions.GetPreferred();
		RString sMods = poChosen.GetString();
		const int row = BeatToNoteRow( m_pPlayerStateEdit->m_fSongBeat );
		
		TapNote tn(
			TapNote::attack, 
			TapNote::SubType_Invalid,
			TapNote::original, 
			sMods,
			g_fLastInsertAttackDurationSeconds, 
			-1, false, "", "", "" );
		tn.pn = m_InputPlayerNumber;
		SetDirty( true );
		SaveUndo();
		m_NoteDataEdit.SetTapNote( g_iLastInsertTapAttackTrack, row, tn );
		CheckNumberOfNotesAndUndo();
	}
	else if( SM == SM_BackFromInsertCourseAttack )
	{
		int iDurationChoice = ScreenMiniMenu::s_viLastAnswers[0];
		Course *pCourse = GAMESTATE->m_pCurCourse;
		CourseEntry &ce = pCourse->m_vEntries[GAMESTATE->m_iEditCourseEntryIndex];
		int iAttack;
		
		g_fLastInsertAttackPositionSeconds = m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat( m_pPlayerStateEdit->m_fSongBeat );
		g_fLastInsertAttackDurationSeconds = StringToFloat( g_InsertCourseAttack.rows[0].choices[iDurationChoice] );
		iAttack = FindAttackAtTime( ce.attacks, g_fLastInsertAttackPositionSeconds );
		
		if( ScreenMiniMenu::s_iLastRowCode == ScreenEdit::remove )
		{
			ASSERT( iAttack >= 0 );
			ce.attacks.erase( ce.attacks.begin() + iAttack );
		}
		else
		{
			PlayerOptions po;
			
			if( iAttack >= 0 )
				po.FromString( ce.attacks[iAttack].sModifiers );

			GAMESTATE->m_pPlayerState[PLAYER_1]->m_PlayerOptions.Assign( ModsLevel_Preferred, po );
			SCREENMAN->AddNewScreenToTop( "ScreenPlayerOptions", SM_BackFromInsertCourseAttackPlayerOptions );
		}
	}
	else if( SM == SM_BackFromInsertCourseAttackPlayerOptions )
	{
		PlayerOptions poChosen = GAMESTATE->m_pPlayerState[PLAYER_1]->m_PlayerOptions.GetPreferred();
		RString sMods = poChosen.GetString();

		Course *pCourse = GAMESTATE->m_pCurCourse;
		CourseEntry &ce = pCourse->m_vEntries[GAMESTATE->m_iEditCourseEntryIndex];
		if( g_fLastInsertAttackPositionSeconds < 0 )
		{
			ce.sModifiers = sMods;
		}
		else
		{
			Attack a(
				 ATTACK_LEVEL_1,
				 g_fLastInsertAttackPositionSeconds,
				 g_fLastInsertAttackDurationSeconds,
				 sMods,
				 false,
				 false );
			int iAttack = FindAttackAtTime( ce.attacks, g_fLastInsertAttackPositionSeconds );
			
			if( iAttack >= 0 )
				ce.attacks[iAttack] = a;
			else
				ce.attacks.push_back( a );
		}
	}
	else if( SM == SM_DoRevertToLastSave )
	{
		if( ScreenPrompt::s_LastAnswer == ANSWER_YES )
		{
			SaveUndo();
			CopyFromLastSave();
			m_pSteps->GetNoteData( m_NoteDataEdit );
			SetDirty( false );
		}
	}
	else if( SM == SM_DoRevertFromDisk )
	{
		if( ScreenPrompt::s_LastAnswer == ANSWER_YES )
		{
			SaveUndo();
			RevertFromDisk();
			m_pSteps->GetNoteData( m_NoteDataEdit );
			SetDirty( false );
		}
	}
	else if( SM == SM_DoSaveAndExit ) // just asked "save before exiting? yes, no, cancel"
	{
		switch( ScreenPrompt::s_LastAnswer )
		{
		//DEFAULT_FAIL( ScreenPrompt::s_LastAnswer );
		case ANSWER_YES:
			// This will send SM_SaveSuccessful or SM_SaveFailed.
			HandleMainMenuChoice( ScreenEdit::save_on_exit );
			return;
		case ANSWER_NO:
			/* Don't save; just exit. */
			SCREENMAN->SendMessageToTopScreen( SM_DoExit );
			return;
		case ANSWER_CANCEL:
			break; // do nothing
		}
	}
	else if( SM == SM_SaveSuccessful )
	{
		LOG->Trace( "Save successful." );
		CopyToLastSave();
		SetDirty( false );
		SONGMAN->Invalidate( GAMESTATE->m_pCurSong );

		if( m_CurrentAction == save_on_exit )
			ScreenPrompt::Prompt( SM_DoExit, SAVE_SUCCESSFUL );
		else
			SCREENMAN->SystemMessage( SAVE_SUCCESSFUL );
	}
	else if( SM == SM_SaveFailed ) // save failed; stay in the editor
	{
		/* We committed the steps to SongManager.  Revert to the last save, and
		 * recommit the reversion to SongManager. */
		LOG->Trace( "Save failed.  Changes uncommitted from memory." );
		CopyFromLastSave();
		m_pSteps->SetNoteData( m_NoteDataEdit );
	}
	else if( SM == SM_DoExit )
	{
		// IMPORTANT: CopyFromLastSave before deleting the Steps below
		CopyFromLastSave();

		/* The user has been given a choice to save.  Delete all unsaved
		 * steps before exiting the editor. */
		Song *pSong = GAMESTATE->m_pCurSong;
		const vector<Steps*> &apSteps = pSong->GetAllSteps();

		//guardamos los steps
		//pero cada steps con su "steptiming"
		GAMESTATE->m_pCurSteps[PLAYER_1]->m_StepsTiming = m_pPlayerStateEdit->m_TimingState;
		GAMESTATE->m_pCurSteps[PLAYER_1]->m_Attacks = m_pSteps->m_Attacks;

		vector<Steps*> apToDelete;
		FOREACH_CONST( Steps *, apSteps, s )
		{
			if( (*s)->IsAutogen() || (*s)->GetSavedToDisk() )
				continue;
			apToDelete.push_back( *s );
		}
		FOREACH_CONST( Steps *, apToDelete, s )
		{
			Steps *pSteps = *s;
			pSong->DeleteSteps( pSteps );
			if( m_pSteps == pSteps )
				m_pSteps = NULL;
			if( GAMESTATE->m_pCurSteps[PLAYER_1].Get() == pSteps )
				GAMESTATE->m_pCurSteps[PLAYER_1].Set( NULL );
		}

		m_Out.StartTransitioning( SM_GoToNextScreen );
	}
	else if( SM == SM_GainFocus )
	{
		/* When another screen comes up, RageSounds takes over the sound timer.  When we come
		 * back, put the timer back to where it was. */
		m_pPlayerStateEdit->m_fSongBeat = m_fTrailingBeat;
	}
	else if( SM == SM_LoseFocus )
	{
		/* Snap the trailing beat, in case we lose focus while tweening. */
		m_fTrailingBeat = m_pPlayerStateEdit->m_fSongBeat;
	}


	//sm5 style handle edit
	else if( SM == SM_BackFromTimingDataInformation )
	{
		HandleTimingDataInformationChoice( (TimingDataInformationChoice)ScreenMiniMenu::s_iLastRowCode, ScreenMiniMenu::s_viLastAnswers );
	}
	//else if( SM == SM_BackFromDifficultyMeterChange )
	//{
	//	int i = StringToInt( ScreenTextEntry::s_sLastAnswer );
	//	GAMESTATE->m_pCurSteps[PLAYER_1]->SetMeter(i);
	//	SetDirty( true );
	//}
	else if( SM == SM_BackFromBPMChange && !ScreenTextEntry::s_bCancelledLast )
	{
		float fBPM = StringToFloat( ScreenTextEntry::s_sLastAnswer );
		if( fBPM > 0 )
			m_pPlayerStateEdit->m_TimingState.SetBPMAtBeat( m_pPlayerStateEdit->m_fSongBeat, fBPM );
		SetDirty( true );
	}
	else if( SM == SM_BackFromStopChange && !ScreenTextEntry::s_bCancelledLast )
	{
		float fStop = StringToFloat( ScreenTextEntry::s_sLastAnswer );
		if( fStop >= 0 )
			m_pPlayerStateEdit->m_TimingState.SetStopAtBeat( m_pPlayerStateEdit->m_fSongBeat, fStop );
		SetDirty( true );
	}
	else if( SM == SM_BackFromDelayChange && !ScreenTextEntry::s_bCancelledLast )
	{
		float fDelay = StringToFloat( ScreenTextEntry::s_sLastAnswer );
		if( fDelay >= 0 )
			m_pPlayerStateEdit->m_TimingState.SetStopAtBeat( m_pPlayerStateEdit->m_fSongBeat, fDelay, true );
		SetDirty( true );
	}
	//else if( SM == SM_BackFromTimeSignatureChange && !ScreenTextEntry::s_bCancelledLast )
	//{
	//	int iNum, iDen;
	//	if( sscanf( ScreenTextEntry::s_sLastAnswer.c_str(), " %d / %d ", &iNum, &iDen ) == 2 )
	//	{
	//		GetAppropriateTiming().SetTimeSignatureAtBeat( GetBeat(), iNum, iDen );
	//	}
	//	SetDirty( true );
	//}
	else if ( SM == SM_BackFromTickcountChange && !ScreenTextEntry::s_bCancelledLast )
	{
		int iTick = StringToInt( ScreenTextEntry::s_sLastAnswer );
		if ( iTick >= 0 && iTick <= ROWS_PER_BEAT )
		{
			m_pPlayerStateEdit->m_TimingState.SetTickcountAtBeat( m_pPlayerStateEdit->m_fSongBeat, iTick );
		}
		SetDirty( true );
	}
	else if ( SM == SM_BackFromComboChange && !ScreenTextEntry::s_bCancelledLast )
	{
		int iCombo, iMiss;
		if (sscanf(ScreenTextEntry::s_sLastAnswer.c_str(), "%d/%d", &iCombo, &iMiss ) == 2)
		{
			m_pPlayerStateEdit->m_TimingState.SetComboFactorAtBeat( m_pPlayerStateEdit->m_fSongBeat, iCombo, iMiss );
		}
		SetDirty( true );
	}
	else if ( SM == SM_BackFromNoteSkinChange && !ScreenTextEntry::s_bCancelledLast )
	{
		RString sSkin = ScreenTextEntry::s_sLastAnswer;
		if( NOTESKIN->DoesNoteSkinExist( sSkin ) )
		{
			m_pPlayerStateEdit->m_TimingState.SetNoteSkinAtBeat( m_pPlayerStateEdit->m_fSongBeat, sSkin );
			SetDirty( true );
		}
	}
	else if ( SM == SM_BackFromLabelChange && !ScreenTextEntry::s_bCancelledLast )
	{
		RString sLabel = ScreenTextEntry::s_sLastAnswer;
		if ( !m_pPlayerStateEdit->m_TimingState.DoesLabelExist(sLabel) )
		{
			sLabel.Replace("=", "_");
			sLabel.Replace(",", "_");
			m_pPlayerStateEdit->m_TimingState.SetLabelAtBeat( m_pPlayerStateEdit->m_fSongBeat, sLabel );
			SetDirty( true );
		}
	}
	else if ( SM == SM_BackFromCountSepChange && !ScreenTextEntry::s_bCancelledLast )
	{
		int iCount = StringToInt(ScreenTextEntry::s_sLastAnswer);
		if ( iCount != 0 && iCount != 1 )
		{
			//m_pPlayerStateEdit->m_TimingState.SetCountSepAtBeat( m_pPlayerStateEdit->m_fSongBeat, 0 );
			//SetDirty( true );
			SCREENMAN->SystemMessageNoAnimate( "change not applied: value must be 0 or 1" );
		}
		else 
		{
			m_pPlayerStateEdit->m_TimingState.SetCountSepAtBeat( m_pPlayerStateEdit->m_fSongBeat, iCount );
			SetDirty( true );
		}
	}
	else if ( SM == SM_BackFromReqHoldHeadChange && !ScreenTextEntry::s_bCancelledLast )
	{
		int iCount = StringToInt(ScreenTextEntry::s_sLastAnswer);
		if ( iCount != 0 && iCount != 1 )
		{
			//m_pPlayerStateEdit->m_TimingState.SetCountSepAtBeat( m_pPlayerStateEdit->m_fSongBeat, 0 );
			//SetDirty( true );
			SCREENMAN->SystemMessageNoAnimate( "change not applied: value must be 0 or 1" );
		}
		else 
		{
			m_pPlayerStateEdit->m_TimingState.SetReqHoldHeadAtBeat( m_pPlayerStateEdit->m_fSongBeat, iCount );
			SetDirty( true );
		}
	}
	else if ( SM == SM_BackFromWarpChange && !ScreenTextEntry::s_bCancelledLast )
	{
		float fWarp = StringToFloat( ScreenTextEntry::s_sLastAnswer );
		if( fWarp >= 0 ) // allow 0 to kill a warp.
		{
			m_pPlayerStateEdit->m_TimingState.SetWarpAtBeat(m_pPlayerStateEdit->m_fSongBeat, fWarp );
			SetDirty( true );
		}
	}
	else if( SM == SM_BackFromSpeedPercentChange && !ScreenTextEntry::s_bCancelledLast )
	{
		float fNum = StringToFloat( ScreenTextEntry::s_sLastAnswer );
		m_pPlayerStateEdit->m_TimingState.SetSpeedAtBeat( m_pPlayerStateEdit->m_fSongBeat, fNum, 0, 0);
		SetDirty( true );
	}
	else if ( SM == SM_BackFromSpeedWaitChange && !ScreenTextEntry::s_bCancelledLast )
	{
		float fDen = StringToFloat( ScreenTextEntry::s_sLastAnswer );
		if( fDen >= 0)
		{
			m_pPlayerStateEdit->m_TimingState.GetSpeedSegmentAtBeat( m_pPlayerStateEdit->m_fSongBeat ).m_fWait = fDen;			
		}
		SetDirty( true );
	}
	else if ( SM == SM_BackFromSpeedModeChange && !ScreenTextEntry::s_bCancelledLast )
	{
		if( ScreenTextEntry::s_sLastAnswer.substr(0, 1) == "b" || ScreenTextEntry::s_sLastAnswer.substr(0, 1) == "B" )
		{			
			m_pPlayerStateEdit->m_TimingState.GetSpeedSegmentAtBeat( m_pPlayerStateEdit->m_fSongBeat ).m_iUnit = 0;
		}
		else if( ScreenTextEntry::s_sLastAnswer.substr(0, 1) == "s" || ScreenTextEntry::s_sLastAnswer.substr(0, 1) == "S" )
		{
			m_pPlayerStateEdit->m_TimingState.GetSpeedSegmentAtBeat( m_pPlayerStateEdit->m_fSongBeat ).m_iUnit = 1;
		}
		else
		{
			int tmp = StringToInt(ScreenTextEntry::s_sLastAnswer );
			if( tmp == 0 )
			{
				m_pPlayerStateEdit->m_TimingState.GetSpeedSegmentAtBeat( m_pPlayerStateEdit->m_fSongBeat ).m_iUnit = 0;
			}
			else
			{
				m_pPlayerStateEdit->m_TimingState.GetSpeedSegmentAtBeat( m_pPlayerStateEdit->m_fSongBeat ).m_iUnit = 1;
			}
		}
		SetDirty( true );
	}
	else if( SM == SM_BackFromScrollChange && !ScreenTextEntry::s_bCancelledLast )
	{
		float fNum = StringToFloat( ScreenTextEntry::s_sLastAnswer );
		m_pPlayerStateEdit->m_TimingState.SetScrollAtBeat( m_pPlayerStateEdit->m_fSongBeat, fNum );
		SetDirty( true );
	}
	else if ( SM == SM_BackFromFakeChange && !ScreenTextEntry::s_bCancelledLast )
	{
		float fFake = StringToFloat( ScreenTextEntry::s_sLastAnswer );
		if( fFake >= 0 ) // allow 0 to kill a warp.
		{
			m_pPlayerStateEdit->m_TimingState.SetFakeAtBeat( m_pPlayerStateEdit->m_fSongBeat, fFake );
			SetDirty( true );
		}
	}
	else if (SM == SM_BackFromInsertStepAttackPlayerOptions)
	{
		ModsGroup<PlayerOptions> &toRestore = GAMESTATE->m_pPlayerState[PLAYER_1]->m_PlayerOptions;
		PlayerOptions poChosen = toRestore.GetPreferred();
		RString mods = poChosen.GetString();
		
		if (g_fLastInsertAttackPositionSeconds >= 0)
		{
			Attack a(ATTACK_LEVEL_1,
				 g_fLastInsertAttackPositionSeconds,
				 g_fLastInsertAttackDurationSeconds,
				 mods,
				 false,
				 false);
			AttackArray &attacks =m_pSteps->m_Attacks;
			int index = FindAttackAtTime(attacks, g_fLastInsertAttackPositionSeconds);
			if (index >= 0)
				attacks[index] = a;
			else
				attacks.push_back(a);
		}
		PlayerOptions po;
		GAMESTATE->GetDefaultPlayerOptions(po);
		toRestore.Assign(ModsLevel_Preferred, po );
	}
	else if( SM == SM_BackFromAlterMenu )
	{
		HandleAlterMenuChoice( (AlterMenuChoice)ScreenMiniMenu::s_iLastRowCode, ScreenMiniMenu::s_viLastAnswers );
	}
	else if( SM == SM_BackFromChangeSpecialTag && !ScreenTextEntry::s_bCancelledLast )
	{	
		RString sNew_ = ScreenTextEntry::s_sLastAnswer;
		Song* pSong = GAMESTATE->m_pCurSong;

		if(!stricmp(sNew_,"YES"))
			pSong->m_bIsSpecialSong = true;
		else if(!stricmp(sNew_,"NO"))
			pSong->m_bIsSpecialSong = false;

	}
	else if( SM == SM_BackFromWriteAttack  && !ScreenTextEntry::s_bCancelledLast )
	{
		//LOG->Trace("DNS Start attacks: %d", m_pSteps->m_Attacks.size() );
		//SCREENMAN->SystemMessageNoAnimate( ScreenTextEntry::s_sLastAnswer );
		float end = -9999;
		//int iNumAttacks = 0;
		//vector<RString> sParams;
		//AttackArray attacks;
		/*split( ScreenTextEntry::s_sLastAnswer, ":", sParams );		
		for( unsigned j = 1; j < sParams.size(); ++j )*/
		RString sParams = ScreenTextEntry::s_sLastAnswer;
		{
			Attack attack;
			vector<RString> sBits;
			split( sParams, "=", sBits, false );
			//if( sBits.size() < 2 )
			//	continue;

			//LOG->Trace( "Params: %s", sParams.c_str() );
			attack.fStartSecond = m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat( NoteRowToBeat( m_NoteFieldEdit.m_iBeginMarker ));
			attack.fSecsRemaining = m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat( NoteRowToBeat( m_NoteFieldEdit.m_iEndMarker ) ) - attack.fStartSecond;

			Trim( sBits[0] );
			if( !sBits[0].CompareNoCase("TIME") )
				attack.fStartSecond = max( StringToFloat(sBits[1]), 0.0f );
			else if( !sBits[0].CompareNoCase("LEN") )
				attack.fSecsRemaining = StringToFloat( sBits[1] );
			else if( !sBits[0].CompareNoCase("END") )
				end = StringToFloat( sBits[1] );
			else if( !sBits[0].CompareNoCase("MODS") )
			{
				//LOG->Trace( "DNS Attack: %s", sBits[1].c_str() );
				//attack.sModifiers = sBits[1];
				bool bEmpty = false;			
				{
					PlayerOptions po;
					po.FromString(sBits[1]);
					vector<RString> v;
					po.GetMods( v );
					if( v.size() <= 0 )
						bEmpty = true;
					else
						attack.sModifiers = po.GetString();
				}
				
				if( end != -9999 )
				{
					attack.fSecsRemaining = end - attack.fStartSecond;
					end = -9999;
				}

				if( attack.fSecsRemaining <= 0.0f)
				{
					//LOG->UserLog( "Song file", sPath, "has an attack with a nonpositive length: %s", sBits[1].c_str() );
					attack.fSecsRemaining = 0.0f;
				}

				//LOG->Trace( "DNS attack st: %2f, sr: %f", attack.fStartSecond, attack.fSecsRemaining );

				if (attack.fStartSecond >= 0 && !bEmpty )
				{
					Attack a(ATTACK_LEVEL_1,
						attack.fStartSecond,
						attack.fSecsRemaining,
						 sBits[1],
						 false,
						 false);
					AttackArray &attacks = m_pSteps->m_Attacks;
					//int index = FindAttackAtTime(attacks, attack.fStartSecond);
					//if (index >= 0)
					//	attacks[index] = a;
					//else
						attacks.push_back(a);

					//LOG->Trace( "DNS Number of attacks: %d", m_pSteps->m_Attacks.size() );
				}
			}
		}
		//LOG->Trace("DNS Final attacks: %d", m_pSteps->m_Attacks.size() );

	}
	else if( SM == SM_WriteGrooveLabel && !ScreenTextEntry::s_bCancelledLast )
	{		
		vector<RString> values;
		split( ScreenTextEntry::s_sLastAnswer, "/", values );
		if( values.size() != 2 )
		{
			SCREENMAN->SystemMessageNoAnimate( "Bad values for division tap" );
			return;
		}
		float fWarpAt = StringToFloat(values[0]);
		float fWarpTo = StringToFloat(values[1]);
		if( (fWarpTo > 0 && fWarpAt > 0) && (fWarpAt < fWarpTo)  )
		{
			const int row = BeatToNoteRow( m_pPlayerStateEdit->m_fSongBeat );
		
			TapNote tn = TAP_ORIGINAL_GROOVE;
			if( m_iCurrentTapMode != 0 )
			{
				switch( m_iCurrentTapMode )
				{
				case 1:
					tn = TapNote( TapNote::groove, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
					break;
				case 2:
					tn = TapNote( TapNote::groove, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
					break;
				case 3:
					tn = TapNote( TapNote::groove, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
					break;
				case 4:
					tn = TapNote( TapNote::groove, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
					break;
				default:
					break;
				}
			}
			tn.pn = m_InputPlayerNumber;
			tn.fWarpTo = fWarpTo;
			tn.fWarpAt = fWarpAt;
			SetDirty( true );
			SaveUndo();
			m_NoteDataEdit.SetTapNote( g_iLastInsertDivisionTapTrack, row, tn );
			CheckNumberOfNotesAndUndo();
		}
		else
		{
			SCREENMAN->SystemMessageNoAnimate( "Warp beats not valid, skipping" );
		}
	}
	else if( SM == SM_WriteWildLabel && !ScreenTextEntry::s_bCancelledLast )
	{
		vector<RString> values;
		split( ScreenTextEntry::s_sLastAnswer, "/", values );
		if( values.size() != 2 )
		{
			SCREENMAN->SystemMessageNoAnimate( "Bad values for division tap" );
			return;
		}
		float fWarpAt = StringToFloat(values[0]);
		float fWarpTo = StringToFloat(values[1]);
		if( (fWarpTo > 0 && fWarpAt > 0) && (fWarpAt < fWarpTo)  )
		{
			const int row = BeatToNoteRow( m_pPlayerStateEdit->m_fSongBeat );
		
			TapNote tn = TAP_ORIGINAL_WILD;
			if( m_iCurrentTapMode != 0 )
			{
				switch( m_iCurrentTapMode )
				{
				case 1:
					tn = TapNote( TapNote::wild, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
					break;
				case 2:
					tn = TapNote( TapNote::wild, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
					break;
				case 3:
					tn = TapNote( TapNote::wild, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
					break;
				case 4:
					tn = TapNote( TapNote::wild, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
					break;
				default:
					break;
				}
			}
			tn.pn = m_InputPlayerNumber;
			tn.fWarpTo = fWarpTo;
			tn.fWarpAt = fWarpAt;
			SetDirty( true );
			SaveUndo();
			m_NoteDataEdit.SetTapNote( g_iLastInsertDivisionTapTrack, row, tn );
			CheckNumberOfNotesAndUndo();
		}
		else
		{
			SCREENMAN->SystemMessageNoAnimate( "Warp beats not valid, skipping" );
		}
	}
	else if( SM == SM_WriteBrainShowerTap && !ScreenTextEntry::s_bCancelledLast )
	{
		RString sGraphics;
		int iAnswer = 0;
		vector<RString> values;
		split( ScreenTextEntry::s_sLastAnswer, "/", values );
		do
		{
			if( values.size() < 2 )
			{
				SCREENMAN->SystemMessageNoAnimate( "BrainShower tap values aren't valid" );
				break;
			}
			TapNote tn = TAP_ORIGINAL_ANSWER;
			if( m_iCurrentTapMode != 0 )
			{
				switch( m_iCurrentTapMode )
				{
				case 1:
					tn = TapNote( TapNote::answer, TapNote::SubType_Invalid, TapNote::original, "   ", 0, -1, false, "", "Vanish", "" );
					break;
				case 2:
					tn = TapNote( TapNote::answer, TapNote::SubType_Invalid, TapNote::original, "   ", 0, -1, false, "", "Sudden", "" );
					break;
				case 3:
					tn = TapNote( TapNote::answer, TapNote::SubType_Invalid, TapNote::original, "   ", 0, -1, false, "", "NonStep", "" );
					break;
				case 4:
					tn = TapNote( TapNote::answer, TapNote::SubType_Invalid, TapNote::original, "   ", 0, -1, false, "", "Drunk", "" );
					break;
				default:
					break;
				}
			}
			iAnswer = StringToInt(values[0]);
			if( iAnswer != 1 && iAnswer != 0 )
			{
				SCREENMAN->SystemMessageNoAnimate( "BrainShower tap answer is no valid: 0-1 values accepted" );
				break;
			}
			//rendimiento?
			if( iAnswer )
				tn.bAnswer = true;
			else
				tn.bAnswer = false;

			sGraphics = values[1];
			if( sGraphics.size() > 3 )
			{
				SCREENMAN->SystemMessageNoAnimate( "BrainShower tap graphic is not valid, max 3 characters" );
				break;
			}

			tn.sText = sGraphics;
			const int row = BeatToNoteRow( m_pPlayerStateEdit->m_fSongBeat );
			tn.pn = m_InputPlayerNumber;			
			SetDirty( true );
			SaveUndo();
			m_NoteDataEdit.SetTapNote( g_iLastInsertBrainShowerTapTrack, row, tn );
			CheckNumberOfNotesAndUndo();
		}
		while(0);
	}
	else if( SM == SM_WriteTapDivisionLabel && !ScreenTextEntry::s_bCancelledLast )
	{
		vector<RString> values;
		split( ScreenTextEntry::s_sLastAnswer, "/", values );
		if( values.size() != 2 )
		{
			SCREENMAN->SystemMessageNoAnimate( "Bad values for division tap" );
			return;
		}
		float fWarpAt = StringToFloat(values[0]);
		float fWarpTo = StringToFloat(values[1]);
		if( (fWarpTo > 0 && fWarpAt > 0) && (fWarpAt < fWarpTo)  )
		{
			const int row = BeatToNoteRow( m_pPlayerStateEdit->m_fSongBeat );
		
			TapNote tn = TAP_ORIGINAL_TAP;
			tn.type = TapNote::division;
			if( m_iCurrentTapMode != 0 )
			{
				switch( m_iCurrentTapMode )
				{
				case 1:
					tn = TapNote( TapNote::division, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Vanish", "" );
					break;
				case 2:
					tn = TapNote( TapNote::division, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Sudden", "" );
					break;
				case 3:
					tn = TapNote( TapNote::division, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "NonStep", "" );
					break;
				case 4:
					tn = TapNote( TapNote::division, TapNote::SubType_Invalid, TapNote::original, "", 0, -1, false, "", "Drunk", "" );
					break;
				default:
					break;
				}
			}
			tn.pn = m_InputPlayerNumber;
			tn.fWarpTo = fWarpTo;
			tn.fWarpAt = fWarpAt;
			SetDirty( true );
			SaveUndo();
			m_NoteDataEdit.SetTapNote( g_iLastInsertDivisionTapTrack, row, tn );
			CheckNumberOfNotesAndUndo();
		}
		else
		{
			SCREENMAN->SystemMessageNoAnimate( "Warp beats not valid, skipping" );
		}
	}
	

	ScreenWithMenuElements::HandleScreenMessage( SM );
}

void ScreenEdit::OnSnapModeChange()
{
	m_soundChangeSnap.Play();
			
	NoteType nt = m_SnapDisplay.GetNoteType();
	int iStepIndex = BeatToNoteRow( m_pPlayerStateEdit->m_fSongBeat );
	int iElementsPerNoteType = BeatToNoteRow( NoteTypeToBeat(nt) );
	int iStepIndexHangover = iStepIndex % iElementsPerNoteType;
	m_pPlayerStateEdit->m_fSongBeat -= NoteRowToBeat( iStepIndexHangover );
}



// Helper function for below

// Begin helper functions for InputEdit


static void ChangeDescription( const RString &sNew )
{
	Steps* pSteps = GAMESTATE->m_pCurSteps[PLAYER_1];

	/* Don't erase edit descriptions. */
	if( sNew.empty() && pSteps->GetDifficulty() == Difficulty_Edit )
		return;

	pSteps->SetDescription(sNew);
}

static void ChangeMainTitle( const RString &sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;
	pSong->m_sMainTitle = sNew;
}

static void ChangeSubTitle( const RString &sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;
	pSong->m_sSubTitle = sNew;
}

static void ChangeArtist( const RString &sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;
	pSong->m_sArtist = sNew;
}

static void ChangeCredit( const RString &sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;
	pSong->m_sCredit = sNew;
}

static void ChangeMainTitleTranslit( const RString &sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;
	pSong->m_sMainTitleTranslit = sNew;
}

static void ChangeSubTitleTranslit( const RString &sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;
	pSong->m_sSubTitleTranslit = sNew;
}

static void ChangeArtistTranslit( const RString &sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;
	pSong->m_sArtistTranslit = sNew;
}

static void ChangeLastBeatHint( const RString &sNew )
{
	GAMESTATE->m_pCurSong->m_fSpecifiedLastBeat = StringToFloat( sNew );
}

// End helper functions

static LocalizedString REVERT_LAST_SAVE			( "ScreenEdit", "Do you want to revert to your last save?" );
static LocalizedString DESTROY_ALL_UNSAVED_CHANGES	( "ScreenEdit", "This will destroy all unsaved changes." );
static LocalizedString REVERT_FROM_DISK			( "ScreenEdit", "Do you want to revert from disk?" );
static LocalizedString SAVE_CHANGES_BEFORE_EXITING	( "ScreenEdit", "Do you want to save changes before exiting?" );
static LocalizedString ENTER_BPM_VALUE			( "ScreenEdit", "Enter a new BPM value." );
static LocalizedString ENTER_STOP_VALUE			( "ScreenEdit", "Enter a new Stop value." );
void ScreenEdit::HandleMainMenuChoice( MainMenuChoice c, const vector<int> &iAnswers )
{
	switch( c )
	{
		//DEFAULT_FAIL( c );
		case play_selection:
			if( m_NoteFieldEdit.m_iBeginMarker!=-1 && m_NoteFieldEdit.m_iEndMarker!=-1 )
				HandleAlterMenuChoice( play );
			else if( m_NoteFieldEdit.m_iBeginMarker!=-1 )
				HandleMainMenuChoice( play_selection_start_to_end );
			else
				HandleMainMenuChoice( play_current_beat_to_end );
			break;
		case play_whole_song:
			{
				m_iStartPlayingAt = 0;
				m_iStopPlayingAt = m_NoteDataEdit.GetLastRow();
				TransitionEditState( STATE_PLAYING );
			}
			break;
		case play_selection_start_to_end:
			{
				m_iStartPlayingAt = m_NoteFieldEdit.m_iBeginMarker;
				m_iStopPlayingAt = max( m_iStartPlayingAt, m_NoteDataEdit.GetLastRow() );
				TransitionEditState( STATE_PLAYING );
			}
			break;
		case play_current_beat_to_end:
			{
				m_iStartPlayingAt = BeatToNoteRow(m_pPlayerStateEdit->m_fSongBeat);
				m_iStopPlayingAt = max( m_iStartPlayingAt, m_NoteDataEdit.GetLastRow() );
				TransitionEditState( STATE_PLAYING );
			}
			break;
		case set_selection_start:
			{
				const int iCurrentRow = BeatToNoteRow(m_pPlayerStateEdit->m_fSongBeat);
				if( m_NoteFieldEdit.m_iEndMarker!=-1 && iCurrentRow >= m_NoteFieldEdit.m_iEndMarker )
				{
					SCREENMAN->PlayInvalidSound();
				}
				else
				{
					m_NoteFieldEdit.m_iBeginMarker = iCurrentRow;
					m_soundMarker.Play();
				}
			}
			break;
		case set_selection_end:
			{
				const int iCurrentRow = BeatToNoteRow(m_pPlayerStateEdit->m_fSongBeat);
				if( m_NoteFieldEdit.m_iBeginMarker!=-1 && iCurrentRow <= m_NoteFieldEdit.m_iBeginMarker )
				{
					SCREENMAN->PlayInvalidSound();
				}
				else
				{
					m_NoteFieldEdit.m_iEndMarker = iCurrentRow;
					m_soundMarker.Play();
				}
			}
			break;
		case edit_steps_information:
			{
				/* XXX: If the difficulty is changed from EDIT, and pSteps->WasLoadedFromProfile()
				 * is true, we should warn that the steps will no longer be saved to the profile. */
				Steps* pSteps = GAMESTATE->m_pCurSteps[PLAYER_1];
				float fMusicSeconds = m_pSoundMusic->GetLengthSeconds();

				g_StepsInformation.rows[difficulty].choices.clear();
				FOREACH_ENUM( Difficulty, dc )
					g_StepsInformation.rows[difficulty].choices.push_back( DifficultyToLocalizedString(pSteps->GetDifficulty()) );
				g_StepsInformation.rows[difficulty].iDefaultChoice = pSteps->GetDifficulty();
				g_StepsInformation.rows[difficulty].bEnabled = (EDIT_MODE.GetValue() >= EditMode_Full);
				g_StepsInformation.rows[meter].choices.clear();
				for(unsigned i = 1; i <= 30; i++ )
					g_StepsInformation.rows[meter].choices.push_back( ssprintf("|%d",i) );
				g_StepsInformation.rows[meter].choices.push_back( "|99" );
				g_StepsInformation.rows[meter].iDefaultChoice = clamp( pSteps->GetMeter()-1, 0, MAX_METER+1 );
				g_StepsInformation.rows[meter].bEnabled = (EDIT_MODE.GetValue() >= EditMode_Home);
				g_StepsInformation.rows[predict_meter].SetOneUnthemedChoice( ssprintf("%.2f",pSteps->PredictMeter()) );
				g_StepsInformation.rows[description].bEnabled = (EDIT_MODE.GetValue() >= EditMode_Full);
				g_StepsInformation.rows[description].SetOneUnthemedChoice( pSteps->GetDescription() );
				g_StepsInformation.rows[tap_notes].SetOneUnthemedChoice( ssprintf("%d", m_NoteDataEdit.GetNumTapNotes()) );
				g_StepsInformation.rows[jumps].SetOneUnthemedChoice( ssprintf("%d", m_NoteDataEdit.GetNumJumps()) );
				g_StepsInformation.rows[hands].SetOneUnthemedChoice( ssprintf("%d", m_NoteDataEdit.GetNumHands()) );
				g_StepsInformation.rows[quads].SetOneUnthemedChoice( ssprintf("%d", m_NoteDataEdit.GetNumQuads()) );
				g_StepsInformation.rows[holds].SetOneUnthemedChoice( ssprintf("%d", m_NoteDataEdit.GetNumHoldNotes()) );
				g_StepsInformation.rows[mines].SetOneUnthemedChoice( ssprintf("%d", m_NoteDataEdit.GetNumMines()) );
				g_StepsInformation.rows[stream].SetOneUnthemedChoice( ssprintf("%.2f", NoteDataUtil::GetStreamRadarValue(m_NoteDataEdit,fMusicSeconds)) );
				g_StepsInformation.rows[voltage].SetOneUnthemedChoice( ssprintf("%.2f", NoteDataUtil::GetVoltageRadarValue(m_NoteDataEdit,fMusicSeconds)) );
				g_StepsInformation.rows[air].SetOneUnthemedChoice( ssprintf("%.2f", NoteDataUtil::GetAirRadarValue(m_NoteDataEdit,fMusicSeconds)) );
				g_StepsInformation.rows[freeze].SetOneUnthemedChoice( ssprintf("%.2f", NoteDataUtil::GetFreezeRadarValue(m_NoteDataEdit,fMusicSeconds)) );
				g_StepsInformation.rows[chaos].SetOneUnthemedChoice( ssprintf("%.2f", NoteDataUtil::GetChaosRadarValue(m_NoteDataEdit,fMusicSeconds)) );
				EditMiniMenu( &g_StepsInformation, SM_BackFromStepsInformation, SM_None );
			}
			break;
		case save:
		case save_on_exit:
			{
				m_CurrentAction = c;

				// copy edit into current Steps
				m_pSteps->SetNoteData( m_NoteDataEdit );

				GAMESTATE->m_pCurSteps[PLAYER_1]->m_StepsTiming = m_pPlayerStateEdit->m_TimingState;
				GAMESTATE->m_pCurSong->m_Timing = m_pPlayerStateEdit->m_TimingState;

				/*
				RString sModeName = GAMEMAN->StepsTypeToString(GAMESTATE->m_pCurStyle->m_StepsType);
				sModeName.MakeUpper();

				bool bNormal = sModeName=="PUMP-SINGLE" && GAMESTATE->m_pCurSteps[PLAYER_1]->GetDifficulty() == Difficulty_Easy;
				bool bHard = sModeName=="PUMP-SINGLE" && GAMESTATE->m_pCurSteps[PLAYER_1]->GetDifficulty() == Difficulty_Medium;
				bool bCrazy = sModeName=="PUMP-SINGLE" && GAMESTATE->m_pCurSteps[PLAYER_1]->GetDifficulty() == Difficulty_Hard;
				bool bFree = sModeName=="PUMP-DOUBLE" && GAMESTATE->m_pCurSteps[PLAYER_1]->GetDifficulty() == Difficulty_Medium;
				bool bNight = sModeName=="PUMP-DOUBLE" && GAMESTATE->m_pCurSteps[PLAYER_1]->GetDifficulty() == Difficulty_Hard;

				if( bNormal )
					GAMESTATE->m_pCurSong->m_TimingExtra[NORMAL] = m_pPlayerStateEdit->m_TimingState;
				else if( bHard )
					GAMESTATE->m_pCurSong->m_TimingExtra[HARD] = m_pPlayerStateEdit->m_TimingState;
				else if( bCrazy )
					GAMESTATE->m_pCurSong->m_TimingExtra[CRAZY] = m_pPlayerStateEdit->m_TimingState;
				else if( bFree )
					GAMESTATE->m_pCurSong->m_TimingExtra[FREESTYLE] = m_pPlayerStateEdit->m_TimingState;
				else if( bNight )
					GAMESTATE->m_pCurSong->m_TimingExtra[NIGHTMARE] = m_pPlayerStateEdit->m_TimingState;
				else
					GAMESTATE->m_pCurSong->m_Timing = m_pPlayerStateEdit->m_TimingState;

				*/

				switch( EDIT_MODE.GetValue() )
				{
				//DEFAULT_FAIL( EDIT_MODE.GetValue() );
				case EditMode_Home:
					{
						ASSERT( m_pSteps->IsAnEdit() );

						RString sError;
						
						m_pSteps->CalculateRadarValues( m_pSong->m_fMusicLengthSeconds );
						if( !NotesWriterSM::WriteEditFileToMachine(m_pSong, m_pSteps, sError) )
						{
							ScreenPrompt::Prompt( SM_None, sError );
							break;
						}

						m_pSteps->SetSavedToDisk( true );

						// HACK: clear undo, so "exit" below knows we don't need to save.
						// This only works because important non-steps data can't be changed in
						// home mode (BPMs, stops).
						ClearUndo();

						SCREENMAN->ZeroNextUpdate();

						HandleScreenMessage( SM_SaveSuccessful );

						/* FIXME
						RString s;
						switch( c )
						{
						case save:			s = "ScreenMemcardSaveEditsAfterSave";	break;
						case save_on_exit:	s = "ScreenMemcardSaveEditsAfterExit";	break;
						default:		ASSERT(0);
						}
						SCREENMAN->AddNewScreenToTop( s );
						*/
					}
					break;
				case EditMode_Full: 
					{
						// This will recalculate radar values.
						m_pSong->Save();
						SCREENMAN->ZeroNextUpdate();

						HandleScreenMessage( SM_SaveSuccessful );
					}
					break;
				case EditMode_CourseMods:
				case EditMode_Practice:
					break;
				}

				m_soundSave.Play();
			}
			break;
		case revert_to_last_save:
			ScreenPrompt::Prompt( SM_DoRevertToLastSave, REVERT_LAST_SAVE.GetValue() + "\n\n" + DESTROY_ALL_UNSAVED_CHANGES.GetValue(), PROMPT_YES_NO, ANSWER_NO );
			break;
		case revert_from_disk:
			ScreenPrompt::Prompt( SM_DoRevertFromDisk, REVERT_FROM_DISK.GetValue() + "\n\n" + DESTROY_ALL_UNSAVED_CHANGES.GetValue(), PROMPT_YES_NO, ANSWER_NO );
			break;
		case options:
			SCREENMAN->AddNewScreenToTop( "ScreenEditOptions", SM_BackFromOptions );			
			break;
		case edit_song_info:
			{
				const Song* pSong = GAMESTATE->m_pCurSong;
				g_SongInformation.rows[main_title].SetOneUnthemedChoice( pSong->m_sMainTitle );
				g_SongInformation.rows[sub_title].SetOneUnthemedChoice( pSong->m_sSubTitle );
				g_SongInformation.rows[artist].SetOneUnthemedChoice( pSong->m_sArtist );
				g_SongInformation.rows[credit].SetOneUnthemedChoice( pSong->m_sCredit );
				g_SongInformation.rows[main_title_transliteration].SetOneUnthemedChoice( pSong->m_sMainTitleTranslit );
				g_SongInformation.rows[sub_title_transliteration].SetOneUnthemedChoice( pSong->m_sSubTitleTranslit );
				g_SongInformation.rows[artist_transliteration].SetOneUnthemedChoice( pSong->m_sArtistTranslit );
				g_SongInformation.rows[last_beat_hint].SetOneUnthemedChoice( ssprintf("%.3f", pSong->m_fSpecifiedLastBeat) );
				//g_SongInformation.rows[special_song].SetOneUnthemedChoice( pSong->m_bIsSpecialSong ? "Yes" : "No" );

				EditMiniMenu( &g_SongInformation, SM_BackFromSongInformation );
			}
			break;
		case edit_bpm:
			ScreenTextEntry::TextEntry( 
				SM_BackFromBPMChange, 
				ENTER_BPM_VALUE, 
				ssprintf( "%.4f", m_pPlayerStateEdit->m_TimingState.GetBPMAtBeat(m_pPlayerStateEdit->m_fSongBeat) ),
				10
				);
			break;
		case edit_stop:
			{
				ScreenTextEntry::TextEntry( 
					SM_BackFromStopChange, 
					ENTER_STOP_VALUE, 
					ssprintf( "%.4f", m_pPlayerStateEdit->m_TimingState.GetStopAtRow( BeatToNoteRow(m_pPlayerStateEdit->m_fSongBeat), false ) ),
					10
					);
			}
			break;
		case play_preview_music:
			PlayPreviewMusic();
			break;
		case exit:
			switch( EDIT_MODE.GetValue() )
			{
			//DEFAULT_FAIL( EDIT_MODE.GetValue() );
			case EditMode_Full:
			case EditMode_Home:
				if( IsDirty() )
					ScreenPrompt::Prompt( SM_DoSaveAndExit, SAVE_CHANGES_BEFORE_EXITING, PROMPT_YES_NO_CANCEL, ANSWER_CANCEL );
				else
					SCREENMAN->SendMessageToTopScreen( SM_DoExit );
				break;
			case EditMode_Practice:
			case EditMode_CourseMods:
				SCREENMAN->SendMessageToTopScreen( SM_DoExit );
				break;
			}
			break;
	};
}

void ScreenEdit::HandleAlterMenuChoice(AlterMenuChoice c, const vector<int> &iAnswers, bool bAllowUndo)
{
	bool bSaveUndo = true;
	switch (c)
	{
		case play:
		case record:
		case cut:
		case copy:
		{
			bSaveUndo = false;
		}
		default:
			break;
	}
	
	if( bSaveUndo )
		SetDirty( true );
	
	/* We call HandleAreaMenuChoice recursively. Only the outermost
	 * HandleAreaMenuChoice should allow Undo so that the inner calls don't
	 * also save Undo and mess up the outermost */
	if( !bAllowUndo )
		bSaveUndo = false;
	
	if( bSaveUndo )
		SaveUndo();
	
	switch(c)
	{
		case cut:
		{
			HandleAlterMenuChoice( copy );
			HandleAlterMenuChoice( clear );
		}
			break;
		case copy:
		{
			ASSERT( m_NoteFieldEdit.m_iBeginMarker!=-1 && m_NoteFieldEdit.m_iEndMarker!=-1 );
			m_Clipboard.ClearAll();
			m_Clipboard.CopyRange( m_NoteDataEdit, m_NoteFieldEdit.m_iBeginMarker, m_NoteFieldEdit.m_iEndMarker );
		}
			break;
		case clear:
		{
			ASSERT( m_NoteFieldEdit.m_iBeginMarker!=-1 && m_NoteFieldEdit.m_iEndMarker!=-1 );
			m_NoteDataEdit.ClearRange( m_NoteFieldEdit.m_iBeginMarker, m_NoteFieldEdit.m_iEndMarker );
		}
			break;
		case quantize:
		{
			NoteType nt = (NoteType)iAnswers[c];
			NoteDataUtil::SnapToNearestNoteType(m_NoteDataEdit, nt, nt,
							    m_NoteFieldEdit.m_iBeginMarker,
							    m_NoteFieldEdit.m_iEndMarker );
			break;
		}
		case turn:
		{
			const NoteData OldClipboard( m_Clipboard );
			HandleAlterMenuChoice( cut );
			
			StepsType st = GAMESTATE->GetCurrentStyle()->m_StepsType;
			TurnType tt = (TurnType)iAnswers[c];
			switch( tt )
			{
				DEFAULT_FAIL( tt );
				case left:		NoteDataUtil::Turn( m_Clipboard, st, NoteDataUtil::left );		break;
				case right:		NoteDataUtil::Turn( m_Clipboard, st, NoteDataUtil::right );		break;
				case mirror:		NoteDataUtil::Turn( m_Clipboard, st, NoteDataUtil::mirror );		break;
				case shuffle:		NoteDataUtil::Turn( m_Clipboard, st, NoteDataUtil::shuffle );		break;
				case super_shuffle:	NoteDataUtil::Turn( m_Clipboard, st, NoteDataUtil::super_shuffle );	break;
			}
			
			HandleAreaMenuChoice( paste_at_begin_marker );
			m_Clipboard = OldClipboard;
		}
			break;
		case transform:
		{
			int iBeginRow = m_NoteFieldEdit.m_iBeginMarker;
			int iEndRow = m_NoteFieldEdit.m_iEndMarker;
			TransformType tt = (TransformType)iAnswers[c];
			StepsType st = GAMESTATE->GetCurrentStyle()->m_StepsType;
			
			switch( tt )
			{
					DEFAULT_FAIL( tt );
				case noholds:		NoteDataUtil::RemoveHoldNotes( m_NoteDataEdit, iBeginRow, iEndRow );	break;
				case nomines:		NoteDataUtil::RemoveMines( m_NoteDataEdit, iBeginRow, iEndRow );	break;
				case little:		NoteDataUtil::Little( m_NoteDataEdit, iBeginRow, iEndRow );		break;
				case wide:		NoteDataUtil::Wide( m_NoteDataEdit, iBeginRow, iEndRow );		break;
				case big:		NoteDataUtil::Big( m_NoteDataEdit, iBeginRow, iEndRow );		break;
				case quick:		NoteDataUtil::Quick( m_NoteDataEdit, iBeginRow, iEndRow );		break;
				case skippy:		NoteDataUtil::Skippy( m_NoteDataEdit, iBeginRow, iEndRow );		break;
				case add_mines:		NoteDataUtil::AddMines( m_NoteDataEdit, iBeginRow, iEndRow );		break;
				case echo:		NoteDataUtil::Echo( m_NoteDataEdit, iBeginRow, iEndRow );		break;
				case stomp:		NoteDataUtil::Stomp( m_NoteDataEdit, st, iBeginRow, iEndRow );		break;
				case planted:		NoteDataUtil::Planted( m_NoteDataEdit, iBeginRow, iEndRow );		break;
				case floored:		NoteDataUtil::Floored( m_NoteDataEdit, iBeginRow, iEndRow );		break;
				case twister:		NoteDataUtil::Twister( m_NoteDataEdit, iBeginRow, iEndRow );		break;
				case nojumps:		NoteDataUtil::RemoveJumps( m_NoteDataEdit, iBeginRow, iEndRow );	break;
				case nohands:		NoteDataUtil::RemoveHands( m_NoteDataEdit, iBeginRow, iEndRow );	break;
				case noquads:		NoteDataUtil::RemoveQuads( m_NoteDataEdit, iBeginRow, iEndRow );	break;
				case nostretch:		NoteDataUtil::RemoveStretch( m_NoteDataEdit, st, iBeginRow, iBeginRow );break;
			}
			
			// bake in the additions
			NoteDataUtil::ConvertAdditionsToRegular( m_NoteDataEdit );
			break;
		}
		case alter:
		{
			const NoteData OldClipboard( m_Clipboard );
			HandleAlterMenuChoice( cut );
			
			AlterType at = (AlterType)iAnswers[c];
			switch( at )
			{
				DEFAULT_FAIL( at );
				case autogen_to_fill_width:
				{
					NoteData temp( m_Clipboard );
					int iMaxNonEmptyTrack = NoteDataUtil::GetMaxNonEmptyTrack( temp );
					if( iMaxNonEmptyTrack == -1 )
						break;
					temp.SetNumTracks( iMaxNonEmptyTrack+1 );
					NoteDataUtil::LoadTransformedSlidingWindow( temp, m_Clipboard, m_Clipboard.GetNumTracks() );
					NoteDataUtil::RemoveStretch( m_Clipboard, GAMESTATE->m_pCurSteps[0]->m_StepsType );
				}
					break;
				case backwards:			NoteDataUtil::Backwards( m_Clipboard );			break;
				case swap_sides:		NoteDataUtil::SwapSides( m_Clipboard );			break;
				case copy_left_to_right:	NoteDataUtil::CopyLeftToRight( m_Clipboard );		break;
				case copy_right_to_left:	NoteDataUtil::CopyRightToLeft( m_Clipboard );		break;
				case clear_left:		NoteDataUtil::ClearLeft( m_Clipboard );			break;
				case clear_right:		NoteDataUtil::ClearRight( m_Clipboard );		break;
				case collapse_to_one:		NoteDataUtil::CollapseToOne( m_Clipboard );		break;
				case collapse_left:		NoteDataUtil::CollapseLeft( m_Clipboard );		break;
				case shift_left:		NoteDataUtil::ShiftLeft( m_Clipboard );			break;
				case shift_right:		NoteDataUtil::ShiftRight( m_Clipboard );		break;
			}
			
			HandleAreaMenuChoice( paste_at_begin_marker );
			m_Clipboard = OldClipboard;
			break;
		}
		case tempo:
		{
			// This affects all steps.
			AlterType at = (AlterType)iAnswers[c];
			float fScale = -1;
			
			switch( at )
			{
					DEFAULT_FAIL( at );
				case compress_2x:	fScale = 0.5f;		break;
				case compress_3_2:	fScale = 2.0f/3;	break;
				case compress_4_3:	fScale = 0.75f;		break;
				case expand_4_3:	fScale = 4.0f/3;	break;
				case expand_3_2:	fScale = 1.5f;		break;
				case expand_2x:		fScale = 2;		break;
			}
			
			int iStartIndex  = m_NoteFieldEdit.m_iBeginMarker;
			int iEndIndex    = m_NoteFieldEdit.m_iEndMarker;
			int iNewEndIndex = iEndIndex + lrintf( (iEndIndex - iStartIndex) * (fScale - 1) );
			
			// scale currently editing notes
			NoteDataUtil::ScaleRegion( m_NoteDataEdit, fScale, iStartIndex, iEndIndex );
			
			// scale timing data
			m_pPlayerStateEdit->m_TimingState.ScaleRegion(fScale, 
							   m_NoteFieldEdit.m_iBeginMarker, 
							   m_NoteFieldEdit.m_iEndMarker, true );
			
			m_NoteFieldEdit.m_iEndMarker = iNewEndIndex;
			break;
			
		}
			
		case play:
			ASSERT( m_NoteFieldEdit.m_iBeginMarker!=-1 && m_NoteFieldEdit.m_iEndMarker!=-1 );
			m_iStartPlayingAt = m_NoteFieldEdit.m_iBeginMarker;
			m_iStopPlayingAt = m_NoteFieldEdit.m_iEndMarker;
			TransitionEditState( STATE_PLAYING );
			break;
		case record:
			ASSERT( m_NoteFieldEdit.m_iBeginMarker!=-1 && m_NoteFieldEdit.m_iEndMarker!=-1 );
			m_iStartPlayingAt = m_NoteFieldEdit.m_iBeginMarker;
			m_iStopPlayingAt = m_NoteFieldEdit.m_iEndMarker;
			TransitionEditState( STATE_RECORDING );
			break;
		case preview_designation:
		{
			ASSERT( m_NoteFieldEdit.m_iBeginMarker!=-1 && m_NoteFieldEdit.m_iEndMarker!=-1 );
			float fMarkerStart = m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat( NoteRowToBeat(m_NoteFieldEdit.m_iBeginMarker) );
			float fMarkerEnd = m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat( NoteRowToBeat(m_NoteFieldEdit.m_iEndMarker) );
			GAMESTATE->m_pCurSong->m_fMusicSampleStartSeconds = fMarkerStart;
			GAMESTATE->m_pCurSong->m_fMusicSampleLengthSeconds = fMarkerEnd - fMarkerStart;
			break;
		}
		case convert_to_pause:
		{
			ASSERT_M( m_NoteFieldEdit.m_iBeginMarker!=-1 && m_NoteFieldEdit.m_iEndMarker!=-1, "Attempted to convert beats outside the notefield to pauses!" );
			float fMarkerStart = m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat( NoteRowToBeat(m_NoteFieldEdit.m_iBeginMarker) );
			float fMarkerEnd = m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat( NoteRowToBeat(m_NoteFieldEdit.m_iEndMarker) );
			
			// The length of the stop segment we're going to create.  This includes time spent in any
			// stops in the selection, which will be deleted and subsumed into the new stop.
			float fStopLength = fMarkerEnd - fMarkerStart;
			
			// be sure not to clobber the row at the start - a row at the end
			// can be dropped safely, though
			NoteDataUtil::DeleteRows( m_NoteDataEdit, 
						 m_NoteFieldEdit.m_iBeginMarker + 1,
						 m_NoteFieldEdit.m_iEndMarker-m_NoteFieldEdit.m_iBeginMarker
						 );
			m_pPlayerStateEdit->m_TimingState.DeleteRows( m_NoteFieldEdit.m_iBeginMarker + 1,
							  m_NoteFieldEdit.m_iEndMarker-m_NoteFieldEdit.m_iBeginMarker );
			m_pPlayerStateEdit->m_TimingState.SetStopAtRow( m_NoteFieldEdit.m_iBeginMarker, fStopLength );
			m_NoteFieldEdit.m_iBeginMarker = -1;
			m_NoteFieldEdit.m_iEndMarker = -1;
			break;
		}
		case convert_to_delay:
		{
			ASSERT_M( m_NoteFieldEdit.m_iBeginMarker!=-1 && m_NoteFieldEdit.m_iEndMarker!=-1, "Attempted to convert beats outside the notefield to pauses!" );
			float fMarkerStart = m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat( NoteRowToBeat(m_NoteFieldEdit.m_iBeginMarker) );
			float fMarkerEnd = m_pPlayerStateEdit->m_TimingState.GetElapsedTimeFromBeat( NoteRowToBeat(m_NoteFieldEdit.m_iEndMarker) );
			
			// The length of the delay segment we're going to create.  This includes time spent in any
			// stops in the selection, which will be deleted and subsumed into the new stop.
			float fStopLength = fMarkerEnd - fMarkerStart;
			
			NoteDataUtil::DeleteRows( m_NoteDataEdit, 
						 m_NoteFieldEdit.m_iBeginMarker,
						 m_NoteFieldEdit.m_iEndMarker-m_NoteFieldEdit.m_iBeginMarker
						 );
			m_pPlayerStateEdit->m_TimingState.DeleteRows( m_NoteFieldEdit.m_iBeginMarker,
							  m_NoteFieldEdit.m_iEndMarker-m_NoteFieldEdit.m_iBeginMarker );
			m_pPlayerStateEdit->m_TimingState.SetStopAtRow( m_NoteFieldEdit.m_iBeginMarker, fStopLength, true );
			m_NoteFieldEdit.m_iBeginMarker = -1;
			m_NoteFieldEdit.m_iEndMarker = -1;
			break;
		}
		case convert_to_warp:
		{
			float startBeat = NoteRowToBeat(m_NoteFieldEdit.m_iBeginMarker);
			float lengthBeat = NoteRowToBeat(m_NoteFieldEdit.m_iEndMarker) - startBeat;
			m_pPlayerStateEdit->m_TimingState.SetWarpAtBeat(startBeat,lengthBeat);
			SetDirty(true);
			break;
		}
		case convert_to_attack:
		{
			float startBeat = NoteRowToBeat(m_NoteFieldEdit.m_iBeginMarker);
			float endBeat = NoteRowToBeat(m_NoteFieldEdit.m_iEndMarker);
			TimingData &timing = m_pPlayerStateEdit->m_TimingState;
			float &start = g_fLastInsertAttackPositionSeconds;
			float &length = g_fLastInsertAttackDurationSeconds;
			start = timing.GetElapsedTimeFromBeat(startBeat);
			length = timing.GetElapsedTimeFromBeat(endBeat) - start;
			
			AttackArray &attacks = m_pSteps->m_Attacks;
			int iAttack = FindAttackAtTime(attacks, start);
			
			ModsGroup<PlayerOptions> &toEdit = GAMESTATE->m_pPlayerState[PLAYER_1]->m_PlayerOptions;
			//this->originalPlayerOptions.Assign(ModsLevel_Preferred, toEdit.GetPreferred());
			PlayerOptions po;
			if (iAttack >= 0)
				po.FromString(attacks[iAttack].sModifiers);
				
			toEdit.Assign( ModsLevel_Preferred, po );
			SCREENMAN->AddNewScreenToTop( "ScreenPlayerOptions", SM_BackFromInsertStepAttackPlayerOptions );
			SetDirty(true);
			break;
		}
		case convert_to_fake:
		{
			float startBeat = NoteRowToBeat(m_NoteFieldEdit.m_iBeginMarker);
			float lengthBeat = NoteRowToBeat(m_NoteFieldEdit.m_iEndMarker) - startBeat;
			m_pPlayerStateEdit->m_TimingState.SetFakeAtBeat(startBeat,lengthBeat);
			SetDirty(true);
			break;
		}
		default: break;
	}
	
}

void ScreenEdit::HandleAreaMenuChoice( AreaMenuChoice c, const vector<int> &iAnswers, bool bAllowUndo )
{
	bool bSaveUndo = true;
	switch( c )
	{
		//case clear_clipboard:
		case undo:
			bSaveUndo = false;
			break;
		default:
			break;
	}

	if( bSaveUndo )
		SetDirty( true );

	/* We call HandleAreaMenuChoice recursively. Only the outermost
	 * HandleAreaMenuChoice should allow Undo so that the inner calls don't
	 * also save Undo and mess up the outermost */
	if( !bAllowUndo )
		bSaveUndo = false;

	if( bSaveUndo )
		SaveUndo();

	switch( c )
	{
		DEFAULT_FAIL( c );
		
		case paste_at_current_beat:
		case paste_at_begin_marker:
			{
				int iDestFirstRow = -1;
				switch( c )
				{
					DEFAULT_FAIL( c );
					case paste_at_current_beat:
						iDestFirstRow = BeatToNoteRow( m_pPlayerStateEdit->m_fSongBeat );
						break;
					case paste_at_begin_marker:
						ASSERT( m_NoteFieldEdit.m_iBeginMarker!=-1 );
						iDestFirstRow = m_NoteFieldEdit.m_iBeginMarker;
						break;
				}

				int iRowsToCopy = m_Clipboard.GetLastRow()+1;
				m_NoteDataEdit.CopyRange( m_Clipboard, 0, iRowsToCopy, iDestFirstRow );
			}
			break;
		case insert_and_shift:
			NoteDataUtil::InsertRows( m_NoteDataEdit, BeatToNoteRow( m_pPlayerStateEdit->m_fSongBeat ), BeatToNoteRow(1) );
			break;
		case delete_and_shift:
			NoteDataUtil::DeleteRows( m_NoteDataEdit, BeatToNoteRow( m_pPlayerStateEdit->m_fSongBeat ), BeatToNoteRow(1) );
			break;
		case shift_pauses_forward:
			m_pPlayerStateEdit->m_TimingState.InsertRows( BeatToNoteRow( m_pPlayerStateEdit->m_fSongBeat ), BeatToNoteRow(1) );
			break;
		case shift_pauses_backward:
			m_pPlayerStateEdit->m_TimingState.DeleteRows( BeatToNoteRow(m_pPlayerStateEdit->m_fSongBeat) + 1, BeatToNoteRow(1) );
			break;

		case convert_pause_to_beat:
		{
			float fStopSeconds = m_pPlayerStateEdit->m_TimingState.GetStopAtBeat(m_pPlayerStateEdit->m_fSongBeat, false);
			m_pPlayerStateEdit->m_TimingState.SetStopAtBeat( m_pPlayerStateEdit->m_fSongBeat , 0 );

			float fStopBeats = fStopSeconds * m_pPlayerStateEdit->m_TimingState.GetBPMAtBeat( m_pPlayerStateEdit->m_fSongBeat ) / 60;

			// don't move the step from where it is, just move everything later
			NoteDataUtil::InsertRows( m_NoteDataEdit, BeatToNoteRow(m_pPlayerStateEdit->m_fSongBeat) + 1, BeatToNoteRow(fStopBeats) );
			m_pPlayerStateEdit->m_TimingState.InsertRows( BeatToNoteRow(m_pPlayerStateEdit->m_fSongBeat) + 1, BeatToNoteRow(fStopBeats) );
		}
		break;
		case convert_delay_to_beat:
		{
			TimingData &timing = m_pPlayerStateEdit->m_TimingState;
			float pause = timing.GetStopAtBeat(m_pPlayerStateEdit->m_fSongBeat, true);
			timing.SetStopAtRow(BeatToNoteRow(m_pPlayerStateEdit->m_fSongBeat), 0, true);
			
			float pauseBeats = pause * timing.GetBPMAtBeat(m_pPlayerStateEdit->m_fSongBeat) / 60;
			
			NoteDataUtil::InsertRows(m_NoteDataEdit,BeatToNoteRow(m_pPlayerStateEdit->m_fSongBeat), BeatToNoteRow(pauseBeats));
			timing.InsertRows(BeatToNoteRow(m_pPlayerStateEdit->m_fSongBeat), BeatToNoteRow(pauseBeats));
			break;
		}
		case last_second_at_beat:
		{
			/*TimingData &timing = m_pPlayerStateEdit->m_TimingState;
			Song &s = *GAMESTATE->m_pCurSong;
			s.SetSpecifiedLastSecond(timing.GetElapsedTimeFromBeat(m_pPlayerStateEdit->m_fSongBeat));
			break;*/
			break;
		}
		case undo:
			Undo();
			break;
		/*case clear_clipboard:
		{
			m_Clipboard.ClearAll();
			break;
		}*/
	};

	if( bSaveUndo )
		CheckNumberOfNotesAndUndo();
}

static LocalizedString ENTER_NEW_DESCRIPTION( "ScreenEdit", "Enter a new description." );
void ScreenEdit::HandleStepsInformationChoice( StepsInformationChoice c, const vector<int> &iAnswers )
{
	Steps* pSteps = GAMESTATE->m_pCurSteps[PLAYER_1];
	Difficulty dc = (Difficulty)iAnswers[difficulty];
	pSteps->SetDifficulty( dc );
	int iMeter = iAnswers[meter]+1;
	pSteps->SetMeter( iMeter );
	
	switch( c )
	{
	case description:
		ScreenTextEntry::TextEntry( 
			SM_None,
			ENTER_NEW_DESCRIPTION, 
			m_pSteps->GetDescription(), 
			(dc == Difficulty_Edit) ? MAX_EDIT_STEPS_DESCRIPTION_LENGTH : 255,
			SongUtil::ValidateCurrentStepsDescription,
			ChangeDescription,
			NULL 
			);
		break;
	}
}

void ScreenEdit::DisplayTimingMenu()
{
	float fBeat = m_pPlayerStateEdit->m_fSongBeat;
	//TimingData &pTime = m_pPlayerStateEdit->m_TimingState;
	bool bHasSpeedOnThisRow = m_pPlayerStateEdit->m_TimingState.GetSpeedSegmentAtBeat( fBeat ).m_iStartRow == BeatToNoteRow(fBeat);
	
	g_TimingDataInformation.rows[beat_0_offset].SetOneUnthemedChoice( ssprintf("%.6f", m_pPlayerStateEdit->m_TimingState.m_fBeat0OffsetInSeconds) );
	g_TimingDataInformation.rows[bpm].SetOneUnthemedChoice( ssprintf("%.6f", m_pPlayerStateEdit->m_TimingState.GetBPMAtBeat( fBeat ) ) );
	g_TimingDataInformation.rows[stop].SetOneUnthemedChoice( ssprintf("%.6f", m_pPlayerStateEdit->m_TimingState.GetStopAtBeat( fBeat, false ) ) ) ;
	g_TimingDataInformation.rows[delay].SetOneUnthemedChoice( ssprintf("%.6f", m_pPlayerStateEdit->m_TimingState.GetStopAtBeat( fBeat, true ) ) );
	//g_TimingDataInformation.rows[time_signature].SetOneUnthemedChoice(ssprintf("%d / %d",
																			   //pTime.GetTimeSignatureNumeratorAtBeat( fBeat ),
																			  // pTime.GetTimeSignatureDenominatorAtBeat( fBeat ) ) );
	g_TimingDataInformation.rows[noteskin].SetOneUnthemedChoice( m_pPlayerStateEdit->m_TimingState.GetNoteSkinAtBeat(fBeat) );
	g_TimingDataInformation.rows[tickcount].SetOneUnthemedChoice( ssprintf("%d", m_pPlayerStateEdit->m_TimingState.GetTickcountAtBeat( fBeat ) ) );
	//provisorio
	g_TimingDataInformation.rows[combo].SetOneUnthemedChoice( ssprintf("%d/%d",
																		m_pPlayerStateEdit->m_TimingState.GetComboFactorAtBeat( fBeat ),m_pPlayerStateEdit->m_TimingState.GetMissComboFactorAtBeat( fBeat ) ) );

	g_TimingDataInformation.rows[warp].SetOneUnthemedChoice( ssprintf("%.6f", m_pPlayerStateEdit->m_TimingState.GetWarpAtBeat( fBeat ) ) );
	g_TimingDataInformation.rows[speed_percent].SetOneUnthemedChoice( bHasSpeedOnThisRow ? ssprintf("%.6f", m_pPlayerStateEdit->m_TimingState.GetSpeedSegmentAtBeat( fBeat ).m_fRatio ) : "---" );
	g_TimingDataInformation.rows[speed_wait].SetOneUnthemedChoice( bHasSpeedOnThisRow ? ssprintf("%.6f", m_pPlayerStateEdit->m_TimingState.GetSpeedSegmentAtBeat( fBeat ).m_fWait ) : "---" );
	
	RString starting = ( m_pPlayerStateEdit->m_TimingState.GetSpeedSegmentAtBeat( fBeat ).m_iUnit == 1 ? "Seconds" : "Beats" );
	g_TimingDataInformation.rows[speed_mode].SetOneUnthemedChoice( starting.c_str() );
	
	g_TimingDataInformation.rows[scroll].SetOneUnthemedChoice( ssprintf("%.6f", m_pPlayerStateEdit->m_TimingState.GetScrollAtBeat( fBeat ) ) );
	g_TimingDataInformation.rows[fake].SetOneUnthemedChoice( ssprintf("%.6f", m_pPlayerStateEdit->m_TimingState.GetFakeAtBeat( fBeat ) ) );
	g_TimingDataInformation.rows[label].SetOneUnthemedChoice( ssprintf("%s", m_pPlayerStateEdit->m_TimingState.GetLabelAtBeat( fBeat ).c_str() ) );
	g_TimingDataInformation.rows[countsep].SetOneUnthemedChoice( ssprintf("%d", m_pPlayerStateEdit->m_TimingState.GetCountSepAtBeat( fBeat ) ) );
	g_TimingDataInformation.rows[reqhold].SetOneUnthemedChoice( ssprintf("%d", m_pPlayerStateEdit->m_TimingState.GetReqHoldHeadAtBeat( fBeat ) ) );
	
	g_TimingDataInformation.rows[speed_wait].bEnabled = bHasSpeedOnThisRow;
	g_TimingDataInformation.rows[speed_mode].bEnabled = bHasSpeedOnThisRow;

	//g_TimingDataInformation.rows[tickcount].bEnabled = GAMESTATE->m_bIsUsingStepTiming;
	//g_TimingDataInformation.rows[combo].bEnabled = GAMESTATE->m_bIsUsingStepTiming;
	//g_TimingDataInformation.rows[speed_percent].bEnabled = GAMESTATE->m_bIsUsingStepTiming;
	//g_TimingDataInformation.rows[fake].bEnabled = GAMESTATE->m_bIsUsingStepTiming;
	//g_TimingDataInformation.rows[scroll].bEnabled = GAMESTATE->m_bIsUsingStepTiming;
		
	EditMiniMenu( &g_TimingDataInformation, SM_BackFromTimingDataInformation );
}

static LocalizedString ENTER_MAIN_TITLE			("ScreenEdit","Enter a new main title.");
static LocalizedString ENTER_SUB_TITLE			("ScreenEdit","Enter a new sub title.");
static LocalizedString ENTER_ARTIST			("ScreenEdit","Enter a new artist.");
static LocalizedString ENTER_CREDIT			("ScreenEdit","Enter a new credit.");
static LocalizedString ENTER_MAIN_TITLE_TRANSLIT	("ScreenEdit","Enter a new main title transliteration.");
static LocalizedString ENTER_SUB_TITLE_TRANSLIT		("ScreenEdit","Enter a new sub title transliteration.");
static LocalizedString ENTER_ARTIST_TRANSLIT		("ScreenEdit","Enter a new artist transliteration.");
static LocalizedString ENTER_LAST_BEAT_HINT		("ScreenEdit","Enter a new last beat hint.");
static LocalizedString ENTER_SPECIAL_SONG		("ScreenEdit","Enter Yes if a song is Special Song, No otherwise");
void ScreenEdit::HandleSongInformationChoice( SongInformationChoice c, const vector<int> &iAnswers )
{
	Song* pSong = GAMESTATE->m_pCurSong;

	switch( c )
	{
	//DEFAULT_FAIL(c);
	case main_title:
		ScreenTextEntry::TextEntry( SM_None, ENTER_MAIN_TITLE, pSong->m_sMainTitle, 100, NULL, ChangeMainTitle, NULL );
		break;
	case sub_title:
		ScreenTextEntry::TextEntry( SM_None, ENTER_SUB_TITLE, pSong->m_sSubTitle, 100, NULL, ChangeSubTitle, NULL );
		break;
	case artist:
		ScreenTextEntry::TextEntry( SM_None, ENTER_ARTIST, pSong->m_sArtist, 100, NULL, ChangeArtist, NULL );
		break;
	case credit:
		ScreenTextEntry::TextEntry( SM_None, ENTER_CREDIT, pSong->m_sCredit, 100, NULL, ChangeCredit, NULL );
		break;
	case main_title_transliteration:
		ScreenTextEntry::TextEntry( SM_None, ENTER_MAIN_TITLE_TRANSLIT, pSong->m_sMainTitleTranslit, 100, NULL, ChangeMainTitleTranslit, NULL );
		break;
	case sub_title_transliteration:
		ScreenTextEntry::TextEntry( SM_None, ENTER_SUB_TITLE_TRANSLIT, pSong->m_sSubTitleTranslit, 100, NULL, ChangeSubTitleTranslit, NULL );
		break;
	case artist_transliteration:
		ScreenTextEntry::TextEntry( SM_None, ENTER_ARTIST_TRANSLIT, pSong->m_sArtistTranslit, 100, NULL, ChangeArtistTranslit, NULL );
		break;
	case last_beat_hint:
		ScreenTextEntry::TextEntry( SM_None, ENTER_LAST_BEAT_HINT, ssprintf("%.3f", pSong->m_fSpecifiedLastBeat), 20, ScreenTextEntry::FloatValidate, ChangeLastBeatHint, NULL );
		/*break;
	case special_song:
		ScreenTextEntry::TextEntry( SM_None, ENTER_SPECIAL_SONG, pSong->m_bIsSpecialSong ? "Yes" : "No", 4, NULL, ChangeSpecialSong, NULL );*/
	};
}

void ScreenEdit::HandleBGChangeChoice( BGChangeChoice c, const vector<int> &iAnswers )
{
	BackgroundChange newChange;

	FOREACH( BackgroundChange, m_pSong->GetBackgroundChanges(g_CurrentBGChangeLayer), iter )
	{
		if( iter->m_fStartBeat == m_pPlayerStateEdit->m_fSongBeat )
		{
			newChange = *iter;
			// delete the old change.  We'll add a new one below.
			m_pSong->GetBackgroundChanges(g_CurrentBGChangeLayer).erase( iter );
			break;
		}
	}

	newChange.m_fStartBeat    = m_pPlayerStateEdit->m_fSongBeat;
	newChange.m_fRate         = StringToFloat( g_BackgroundChange.rows[rate].choices[iAnswers[rate]] )/100.f;
	newChange.m_sTransition   = iAnswers[transition] ? g_BackgroundChange.rows[transition].choices[iAnswers[transition]] : RString();
	newChange.m_def.m_sEffect = iAnswers[effect]     ? g_BackgroundChange.rows[effect].choices[iAnswers[effect]]         : RString();
	newChange.m_def.m_sColor1 = iAnswers[color1]     ? g_BackgroundChange.rows[color1].choices[iAnswers[color1]]         : RString();
	newChange.m_def.m_sColor2 = iAnswers[color2]     ? g_BackgroundChange.rows[color2].choices[iAnswers[color2]]         : RString();
	switch( iAnswers[file1_type] )
	{
	//DEFAULT_FAIL( iAnswers[file1_type] );
	case none:			newChange.m_def.m_sFile1 = "";					break;
	case dynamic_random:		newChange.m_def.m_sFile1 = RANDOM_BACKGROUND_FILE;		break;
	case baked_random:		newChange.m_def.m_sFile1 = GetOneBakedRandomFile( m_pSong );	break;
	case song_bganimation:
	case song_movie:
	case song_bitmap:
	case global_bganimation:
	case global_movie:
	case global_movie_song_group:
	case global_movie_song_group_and_genre:
		{
			BGChangeChoice row1 = (BGChangeChoice)(file1_song_bganimation + iAnswers[file1_type]);
			newChange.m_def.m_sFile1 = g_BackgroundChange.rows[row1].choices.empty() ? "" : g_BackgroundChange.rows[row1].choices[iAnswers[row1]];
		}
		break;
	}
	switch( iAnswers[file2_type] )
	{
	//DEFAULT_FAIL(iAnswers[file2_type]);
	case none:				newChange.m_def.m_sFile2 = "";				break;
	case dynamic_random:		newChange.m_def.m_sFile2 = RANDOM_BACKGROUND_FILE;		break;
	case baked_random:		newChange.m_def.m_sFile2 = GetOneBakedRandomFile( m_pSong );	break;
	case song_bganimation:
	case song_movie:
	case song_bitmap:
	case global_bganimation:
	case global_movie:
	case global_movie_song_group:
	case global_movie_song_group_and_genre:
		{
			BGChangeChoice row2 = (BGChangeChoice)(file2_song_bganimation + iAnswers[file2_type]);
			newChange.m_def.m_sFile2 = g_BackgroundChange.rows[row2].choices.empty() ? "" : g_BackgroundChange.rows[row2].choices[iAnswers[row2]];
		}
		break;
	}


	if( c == delete_change || newChange.m_def.m_sFile1.empty() )
	{
		// don't add
	}
	else
	{
		m_pSong->AddBackgroundChange( g_CurrentBGChangeLayer, newChange );
	}
	g_CurrentBGChangeLayer = BACKGROUND_LAYER_Invalid;
}

void ScreenEdit::SetupCourseAttacks()
{
	/* This is the first beat that can be changed without it being visible.  Until
	 * we draw for the first time, any beat can be changed. */
	GAMESTATE->m_pPlayerState[PLAYER_1]->m_fLastDrawnBeat = -100;

	// Put course options into effect.
	GAMESTATE->m_pPlayerState[PLAYER_1]->m_ModsToApply.clear();
	GAMESTATE->m_pPlayerState[PLAYER_1]->RemoveActiveAttacks();


	if( GAMESTATE->m_pCurCourse )
	{
		AttackArray Attacks;
		
		if( EDIT_MODE == EditMode_CourseMods )
		{
			Attacks = GAMESTATE->m_pCurCourse->m_vEntries[GAMESTATE->m_iEditCourseEntryIndex].attacks;
		}
		else
		{
			GAMESTATE->m_pCurCourse->RevertFromDisk();	// Remove this and have a separate reload key?

			for( unsigned e = 0; e < GAMESTATE->m_pCurCourse->m_vEntries.size(); ++e )
			{
				if( GAMESTATE->m_pCurCourse->m_vEntries[e].songID.ToSong() != m_pSong )
					continue;

				Attacks = GAMESTATE->m_pCurCourse->m_vEntries[e].attacks;
				break;
			}
		}

		FOREACH( Attack, Attacks, attack )
			GAMESTATE->m_pPlayerState[PLAYER_1]->LaunchAttack( *attack );
	}
	else
	{
		//const PlayerOptions &p = GAMESTATE->m_pPlayerState[PLAYER_1]->m_PlayerOptions.GetCurrent();
		if (GAMESTATE->m_pCurSong )
		{
			AttackArray &attacks = m_pSteps->m_Attacks;
			
			if (attacks.size() > 0)
			{
				FOREACH(Attack, attacks, attack)
				{
					float fBeat = m_pPlayerStateEdit->m_TimingState.GetBeatFromElapsedTime(attack->fStartSecond);
					if (fBeat >= m_pPlayerStateEdit->m_fSongBeat)
						m_pPlayerStateEdit->LaunchAttack( *attack );
				}
			}
		}
	}
	GAMESTATE->m_pPlayerState[PLAYER_1]->RebuildPlayerOptionsFromActiveAttacks();
}

void ScreenEdit::CopyToLastSave()
{
	ASSERT( GAMESTATE->m_pCurSong );
	ASSERT( GAMESTATE->m_pCurSteps[PLAYER_1] );
	m_SongLastSave = *GAMESTATE->m_pCurSong;
	m_vStepsLastSave.clear();
	const vector<Steps*> &vSteps = GAMESTATE->m_pCurSong->GetStepsByStepsType( GAMESTATE->m_pCurSteps[PLAYER_1]->m_StepsType );
	FOREACH_CONST( Steps*, vSteps, it )
		m_vStepsLastSave.push_back( **it );
}

void ScreenEdit::CopyFromLastSave()
{
	// We are assuming two things here:
	// 1) No steps can be created by ScreenEdit
	// 2) No steps can be deleted by ScreenEdit (except possibly when we exit)
	*GAMESTATE->m_pCurSong = m_SongLastSave;
	const vector<Steps*> &vSteps = GAMESTATE->m_pCurSong->GetStepsByStepsType( GAMESTATE->m_pCurSteps[PLAYER_1]->m_StepsType );
	ASSERT_M( vSteps.size() == m_vStepsLastSave.size(), ssprintf("Step sizes don't match: %d, %d", int(vSteps.size()), int(m_vStepsLastSave.size())) );
	for( unsigned i = 0; i < vSteps.size(); ++i )
		*vSteps[i] = m_vStepsLastSave[i];
}

void ScreenEdit::RevertFromDisk()
{
	ASSERT( GAMESTATE->m_pCurSteps[PLAYER_1] );
	StepsID id;
	id.FromSteps( GAMESTATE->m_pCurSteps[PLAYER_1] );
	ASSERT( id.IsValid() );

	GAMESTATE->m_pCurSong->ReloadFromSongDir( GAMESTATE->m_pCurSong->GetSongDir() );

	Steps *pNewSteps = id.ToSteps( GAMESTATE->m_pCurSong, true );
	if( !pNewSteps )
	{
		// If the Steps we were currently editing vanished when we did the revert,
		// put a blank Steps in its place.  Note that this does not have to be the
		// work of someone maliciously changing the simfile; it could happen to 
		// someone editing a new stepchart and reverting from disk, for example.
		pNewSteps = new Steps();
		pNewSteps->CreateBlank( id.GetStepsType() );
		pNewSteps->SetDifficulty( id.GetDifficulty() );
		GAMESTATE->m_pCurSong->AddSteps( pNewSteps );
	}
	GAMESTATE->m_pCurSteps[PLAYER_1].Set( pNewSteps );
	m_pSteps = pNewSteps;

	CopyToLastSave();
	SetDirty( false );
	SONGMAN->Invalidate( GAMESTATE->m_pCurSong );
}

void ScreenEdit::SaveUndo()
{
	m_bHasUndo = true;
	m_Undo.CopyAll( m_NoteDataEdit );
}

static LocalizedString UNDO			("ScreenEdit", "Undo");
static LocalizedString CANT_UNDO		("ScreenEdit", "Can't undo - no undo data.");
void ScreenEdit::Undo()
{
	if( m_bHasUndo )
	{
		swap( m_Undo, m_NoteDataEdit );
		SCREENMAN->SystemMessage( UNDO );
	}
	else
	{
		SCREENMAN->SystemMessage( CANT_UNDO );
		SCREENMAN->PlayInvalidSound();
	}
}

void ScreenEdit::ClearUndo()
{
	m_bHasUndo = false;
	m_Undo.ClearAll();
}

static LocalizedString CREATES_MORE_THAN_NOTES	( "ScreenEdit", "This change creates more than %d notes in a measure." );
static LocalizedString MORE_THAN_NOTES		( "ScreenEdit", "More than %d notes per measure is not allowed.  This change has been reverted." );
static LocalizedString CREATES_NOTES_PAST_END	( "ScreenEdit", "This change creates notes past the end of the music and is not allowed." );
static LocalizedString CHANGE_REVERTED		( "ScreenEdit", "The change has been reverted." );
void ScreenEdit::CheckNumberOfNotesAndUndo()
{
	if( EDIT_MODE.GetValue() != EditMode_Home )
		return;

	for( int row=0; row<=m_NoteDataEdit.GetLastRow(); row+=ROWS_PER_MEASURE )
	{
		int iNumNotesThisMeasure = 0;
		FOREACH_NONEMPTY_ROW_ALL_TRACKS_RANGE( m_NoteDataEdit, r, row, row+ROWS_PER_MEASURE )
			iNumNotesThisMeasure += m_NoteDataEdit.GetNumTapNonEmptyTracks( r );
		if( iNumNotesThisMeasure > MAX_NOTES_PER_MEASURE )
		{
			Undo();
			m_bHasUndo = false;
			RString sError = ssprintf( CREATES_MORE_THAN_NOTES.GetValue() + "\n\n" + MORE_THAN_NOTES.GetValue(), MAX_NOTES_PER_MEASURE, MAX_NOTES_PER_MEASURE );
			ScreenPrompt::Prompt( SM_None, sError );
			return;
		}
	}

	if( GAMESTATE->m_pCurSteps[0]->IsAnEdit() )
	{
		// Check that the action didn't push notes any farther past the last measure.
		// This blocks Insert Beat from pushing past the end, but allows Delete Beat
		// to pull back the notes that are already past the end.
		float fNewLastBeat = m_NoteDataEdit.GetLastBeat();
		bool bLastBeatIncreased = fNewLastBeat > m_Undo.GetLastBeat();
		bool bPassedTheEnd = fNewLastBeat > GetMaximumBeatForNewNote();
		if( bLastBeatIncreased && bPassedTheEnd )
		{
			Undo();
			m_bHasUndo = false;
			RString sError = CREATES_NOTES_PAST_END.GetValue() + "\n\n" + CHANGE_REVERTED.GetValue();
			ScreenPrompt::Prompt( SM_None, sError );
			return;
		}
	}
}

float ScreenEdit::GetMaximumBeatForNewNote() const
{
	switch( EDIT_MODE.GetValue() )
	{
	//DEFAULT_FAIL( EDIT_MODE.GetValue() );
	case EditMode_Practice:
	case EditMode_CourseMods:
	case EditMode_Home:
		{
			float fEndBeat = GAMESTATE->m_pCurSong->m_fLastBeat;

			// Round up to the next measure end.  Some songs end on weird beats 
			// mid-measure, and it's odd to have movement capped to these weird
			// beats.
			fEndBeat += BEATS_PER_MEASURE;
			fEndBeat = ftruncf( fEndBeat, (float)BEATS_PER_MEASURE );

			return fEndBeat;
		}
	case EditMode_Full:
		return NoteRowToBeat(MAX_NOTE_ROW);
	}
}

float ScreenEdit::GetMaximumBeatForMoving() const
{
	float fEndBeat = GetMaximumBeatForNewNote();

	// Jump to GetLastBeat even if it's past m_pCurSong->m_fLastBeat
	// so that users can delete garbage steps past then end that they have 
	// have inserted in a text editor.  Once they delete all steps on 
	// GetLastBeat() and move off of that beat, they won't be able to return.
	fEndBeat = max( fEndBeat, m_NoteDataEdit.GetLastBeat() );

	return fEndBeat;
}

struct EditHelpLine
{
	const char *szEnglishDescription;
	vector<EditButton> veb;

	EditHelpLine( 
		const char *_szEnglishDescription,
		EditButton eb0, 
		EditButton eb1 = EditButton_Invalid, 
		EditButton eb2 = EditButton_Invalid, 
		EditButton eb3 = EditButton_Invalid, 
		EditButton eb4 = EditButton_Invalid, 
		EditButton eb5 = EditButton_Invalid, 
		EditButton eb6 = EditButton_Invalid, 
		EditButton eb7 = EditButton_Invalid, 
		EditButton eb8 = EditButton_Invalid, 
		EditButton eb9 = EditButton_Invalid )
	{
		szEnglishDescription = _szEnglishDescription;
#define PUSH_IF_VALID( x ) if( x != EditButton_Invalid ) veb.push_back( x );
		PUSH_IF_VALID( eb0 );
		PUSH_IF_VALID( eb1 );
		PUSH_IF_VALID( eb2 ); 
		PUSH_IF_VALID( eb3 ); 
		PUSH_IF_VALID( eb4 ); 
		PUSH_IF_VALID( eb5 ); 
		PUSH_IF_VALID( eb6 ); 
		PUSH_IF_VALID( eb7 ); 
		PUSH_IF_VALID( eb8 ); 
		PUSH_IF_VALID( eb9 ); 
#undef PUSH_IF_VALID
	}
};
static const EditHelpLine g_EditHelpLines[] =
{
	EditHelpLine( "Move cursor",					EDIT_BUTTON_SCROLL_UP_LINE,		EDIT_BUTTON_SCROLL_DOWN_LINE ),
	EditHelpLine( "Jump measure",					EDIT_BUTTON_SCROLL_UP_PAGE,		EDIT_BUTTON_SCROLL_DOWN_PAGE ),
	EditHelpLine( "Jump measure",					EDIT_BUTTON_SCROLL_PREV_MEASURE,	EDIT_BUTTON_SCROLL_NEXT_MEASURE ),
	EditHelpLine( "Select region",					EDIT_BUTTON_SCROLL_SELECT ),
	EditHelpLine( "Jump to first/last beat",			EDIT_BUTTON_SCROLL_HOME,		EDIT_BUTTON_SCROLL_END ),
	EditHelpLine( "Change zoom",					EDIT_BUTTON_SCROLL_SPEED_UP,		EDIT_BUTTON_SCROLL_SPEED_DOWN ),
	EditHelpLine( "Play",						EDIT_BUTTON_PLAY_SELECTION ),
	EditHelpLine( "Play current beat to end",			EDIT_BUTTON_PLAY_FROM_CURSOR ),
	EditHelpLine( "Play whole song",				EDIT_BUTTON_PLAY_FROM_START ),
	EditHelpLine( "Record",						EDIT_BUTTON_RECORD_SELECTION ),
	EditHelpLine( "Set selection",					EDIT_BUTTON_LAY_SELECT ),
	EditHelpLine( "Drag area marker",				EDIT_BUTTON_SCROLL_SELECT ),
	EditHelpLine( "Next/prev steps of same StepsType",		EDIT_BUTTON_OPEN_NEXT_STEPS,		EDIT_BUTTON_OPEN_PREV_STEPS ),
	EditHelpLine( "Decrease/increase BPM at cur beat",		EDIT_BUTTON_BPM_DOWN,			EDIT_BUTTON_BPM_UP ),
	EditHelpLine( "Decrease/increase stop at cur beat",		EDIT_BUTTON_STOP_DOWN,			EDIT_BUTTON_STOP_UP ),
	EditHelpLine( "Decrease/increase music offset",			EDIT_BUTTON_OFFSET_DOWN,		EDIT_BUTTON_OFFSET_UP ),
	EditHelpLine( "Decrease/increase sample music start",		EDIT_BUTTON_SAMPLE_START_DOWN,		EDIT_BUTTON_SAMPLE_START_UP ),
	EditHelpLine( "Decrease/increase sample music length",		EDIT_BUTTON_SAMPLE_LENGTH_DOWN,		EDIT_BUTTON_SAMPLE_LENGTH_UP ),
	EditHelpLine( "Play sample music",				EDIT_BUTTON_PLAY_SAMPLE_MUSIC ),
	EditHelpLine( "Add/Edit Background Change",			EDIT_BUTTON_OPEN_BGCHANGE_LAYER1_MENU ),
	EditHelpLine( "Insert beat and shift down",			EDIT_BUTTON_INSERT ),
	EditHelpLine( "Shift BPM changes and stops down one beat",	EDIT_BUTTON_INSERT_SHIFT_PAUSES ),
	EditHelpLine( "Delete beat and shift up",			EDIT_BUTTON_DELETE ),
	EditHelpLine( "Shift BPM changes and stops up one beat",	EDIT_BUTTON_DELETE_SHIFT_PAUSES ),
	EditHelpLine( "Lay mine",					EDIT_BUTTON_LAY_MINE_OR_ROLL ),
	EditHelpLine( "Lay lift",					EDIT_BUTTON_LAY_LIFT ),
	EditHelpLine( "Add to/remove from right half",			EDIT_BUTTON_RIGHT_SIDE ),
};

static bool IsMapped( EditButton eb, const MapEditToDI &editmap )
{
	for( int s=0; s<NUM_EDIT_TO_DEVICE_SLOTS; s++ )
	{
		DeviceInput diPress = editmap.button[eb][s];
		if( diPress.IsValid() )
			return true;
	}
	return false;
}

static void ProcessKeyName( RString &s )
{
	s.Replace( "Key_", "" );
}

static void ProcessKeyNames( vector<RString> &vs )
{
	FOREACH( RString, vs, s )
		ProcessKeyName( *s );

	sort( vs.begin(), vs.end() );
	vector<RString>::iterator toDelete = unique( vs.begin(), vs.end() );
	vs.erase(toDelete, vs.end());
}

static RString GetDeviceButtonsLocalized( const vector<EditButton> &veb, const MapEditToDI &editmap )
{
	vector<RString> vsPress;
	vector<RString> vsHold;
	FOREACH_CONST( EditButton, veb, eb )
	{
		if( !IsMapped( *eb, editmap ) )
			continue;

		for( int s=0; s<NUM_EDIT_TO_DEVICE_SLOTS; s++ )
		{
			DeviceInput diPress = editmap.button[*eb][s];
			DeviceInput diHold = editmap.hold[*eb][s];
			if( diPress.IsValid() )
				vsPress.push_back( Capitalize(INPUTMAN->GetLocalizedInputString(diPress)) );
			if( diHold.IsValid() )
				vsHold.push_back( Capitalize(INPUTMAN->GetLocalizedInputString(diHold)) );
		}
	}

	ProcessKeyNames( vsPress );
	ProcessKeyNames( vsHold );

	RString s = join("/",vsPress);
	if( !vsHold.empty() )
		s = join("/",vsHold) + " + " + s;
	return s;
}

void ScreenEdit::DoHelp()
{
	g_EditHelp.rows.clear();

	for( unsigned i=0; i<ARRAYLEN(g_EditHelpLines); ++i )
	{
		const EditHelpLine &hl = g_EditHelpLines[i];
		
		if( !IsMapped(hl.veb[0],m_EditMappingsDeviceInput) )
			continue;

		RString sButtons = GetDeviceButtonsLocalized( hl.veb, m_EditMappingsDeviceInput );
		RString sDescription = THEME->GetString( "EditHelpDescription", hl.szEnglishDescription );
		g_EditHelp.rows.push_back( MenuRowDef( -1, sDescription, false, EditMode_Practice, false, false, 0, sButtons ) );
	}
	
	EditMiniMenu( &g_EditHelp );
}

//static void ChangeBeat0Offset( const RString &sNew )
//{
//	TimingData &timing = m_pPlayerStateEdit->m_TimingState;
//	float old = timing.m_fBeat0OffsetInSeconds;
//	timing.m_fBeat0OffsetInSeconds = StringToFloat( sNew );
//	float delta = timing.m_fBeat0OffsetInSeconds - old;
//	/*if (GAMESTATE->m_bIsUsingStepTiming)
//	{
//		GAMESTATE->m_pCurSteps[PLAYER_1]->m_Attacks.UpdateStartTimes(delta);
//	}
//	else
//	{
//		GAMESTATE->m_pCurSong->m_Attacks.UpdateStartTimes(delta);
//		GAMESTATE->m_pCurSong->m_fMusicSampleStartSeconds += delta;
//	}*/
//	GAMESTATE->m_pCurSteps[PLAYER_1]->m_Attacks.UpdateStartTimes(delta);
//}

static LocalizedString ENTER_BEAT_0_OFFSET			( "ScreenEdit", "Enter the offset for the song.");
//static LocalizedString ENTER_BPM_VALUE				( "ScreenEdit", "Enter a new BPM value." );
//static LocalizedString ENTER_STOP_VALUE				( "ScreenEdit", "Enter a new Stop value." );
static LocalizedString ENTER_DELAY_VALUE			( "ScreenEdit", "Enter a new Delay value." );
static LocalizedString ENTER_TIME_SIGNATURE_VALUE	( "ScreenEdit", "Enter a new Time Signature." );
static LocalizedString ENTER_TICKCOUNT_VALUE			( "ScreenEdit", "Enter a new Tickcount value." );
static LocalizedString ENTER_COMBO_VALUE			( "ScreenEdit", "Enter a new Combo value." );
static LocalizedString ENTER_NOTESKIN_VALUE			( "ScreenEdit", "Enter a new Noteskin value." );
static LocalizedString ENTER_WARP_VALUE				( "ScreenEdit", "Enter a new Warp value." );
static LocalizedString ENTER_SPEED_PERCENT_VALUE		( "ScreenEdit", "Enter a new Speed percent value." );
static LocalizedString ENTER_SPEED_WAIT_VALUE			( "ScreenEdit", "Enter a new Speed wait value." );
static LocalizedString ENTER_SPEED_MODE_VALUE			( "ScreenEdit", "Enter a new Speed mode value." );
static LocalizedString ENTER_SCROLL_VALUE		( "ScreenEdit", "Enter a new Scroll value." );
static LocalizedString ENTER_FAKE_VALUE				( "ScreenEdit", "Enter a new Fake value." );
static LocalizedString ENTER_LABEL_VALUE			( "ScreenEdit", "Enter a new Label value." );
static LocalizedString ENTER_COUNTSEP_VALUE			( "ScreenEdit", "Enter a new Count separately value." );
static LocalizedString ENTER_REQUIREHOLDHEAD_VALUE			( "ScreenEdit", "Enter a new required hold head value." );
static LocalizedString CONFIRM_TIMING_ERASE			( "ScreenEdit", "Are you sure you want to erase this chart's timing data?" );
void ScreenEdit::HandleTimingDataInformationChoice( TimingDataInformationChoice c, const vector<int> &iAnswers )
{
	switch( c )
	{
	DEFAULT_FAIL( c );
	case beat_0_offset:
		ScreenTextEntry::TextEntry( SM_BackFromOffsetChange, ENTER_BEAT_0_OFFSET,
			ssprintf("%.6f", m_pPlayerStateEdit->m_TimingState.m_fBeat0OffsetInSeconds), 20 );
		break;
	case bpm:
		ScreenTextEntry::TextEntry( 
			SM_BackFromBPMChange, 
			ENTER_BPM_VALUE, 
			ssprintf( "%.4f", m_pPlayerStateEdit->m_TimingState.GetBPMAtBeat( m_pPlayerStateEdit->m_fSongBeat ) ),
			10
			);
		break;
	case stop:
		ScreenTextEntry::TextEntry( 
			SM_BackFromStopChange, 
			ENTER_STOP_VALUE, 
			ssprintf( "%.4f", m_pPlayerStateEdit->m_TimingState.GetStopAtBeat( m_pPlayerStateEdit->m_fSongBeat, false ) ),
			10
			);
		break;
	case delay:
		ScreenTextEntry::TextEntry( 
			SM_BackFromDelayChange, 
			ENTER_DELAY_VALUE, 
			ssprintf( "%.4f", m_pPlayerStateEdit->m_TimingState.GetStopAtBeat( m_pPlayerStateEdit->m_fSongBeat, true ) ),
			10
		);
		break;
	/*case time_signature:
	{
		TimeSignatureSegment * ts = GetAppropriateTiming().GetTimeSignatureSegmentAtBeat( GetBeat() );
		ScreenTextEntry::TextEntry(
			SM_BackFromTimeSignatureChange,
			ENTER_TIME_SIGNATURE_VALUE,
			ssprintf( "%d/%d", ts->GetNum(), ts->GetDen() ),
			8
			);
		break;
	}*/
	case tickcount:
		ScreenTextEntry::TextEntry(
			SM_BackFromTickcountChange,
			ENTER_TICKCOUNT_VALUE,
			ssprintf( "%d", m_pPlayerStateEdit->m_TimingState.GetTickcountAtBeat( m_pPlayerStateEdit->m_fSongBeat ) ),
			2
			);
		break;
	case combo:
	{
		ComboSegment &cs = m_pPlayerStateEdit->m_TimingState.GetComboFactorSegmentAtBeat(m_pPlayerStateEdit->m_fSongBeat);
		ScreenTextEntry::TextEntry(SM_BackFromComboChange,
								   ENTER_COMBO_VALUE,
								   ssprintf( "%d/%d",
								   cs.m_iComboFactor,cs.m_iMissComboFactor	),
								   7);
		break;
	}
	case noteskin:
		ScreenTextEntry::TextEntry(
		   SM_BackFromNoteSkinChange,
		   ENTER_NOTESKIN_VALUE,
		   ssprintf( "%s", m_pPlayerStateEdit->m_TimingState.GetNoteSkinAtBeat(m_pPlayerStateEdit->m_fSongBeat).c_str() ),
		   64
		   );
		break;
	case warp:
		ScreenTextEntry::TextEntry( 
		   SM_BackFromWarpChange, 
		   ENTER_WARP_VALUE, 
		   ssprintf( "%.6f", m_pPlayerStateEdit->m_TimingState.GetWarpAtBeat( m_pPlayerStateEdit->m_fSongBeat ) ),
		   10
		   );
		break;
	case reqhold:
		ScreenTextEntry::TextEntry( 
			SM_BackFromReqHoldHeadChange, 
			ENTER_REQUIREHOLDHEAD_VALUE, 
			ssprintf( "%d", m_pPlayerStateEdit->m_TimingState.GetReqHoldHeadAtBeat( m_pPlayerStateEdit->m_fSongBeat ) ? 1 : 0 ),
		   1
		   );
		break;
	case speed_percent:
		ScreenTextEntry::TextEntry(
		   SM_BackFromSpeedPercentChange,
		   ENTER_SPEED_PERCENT_VALUE,
		   ssprintf( "%.6f", m_pPlayerStateEdit->m_TimingState.GetSpeedSegmentAtBeat( m_pPlayerStateEdit->m_fSongBeat ).m_fRatio ),
		   10
		   );
		break;
	case scroll:
		ScreenTextEntry::TextEntry(
		   SM_BackFromScrollChange,
		   ENTER_SCROLL_VALUE,
		   ssprintf( "%.6f", m_pPlayerStateEdit->m_TimingState.GetScrollSegmentAtBeat( m_pPlayerStateEdit->m_fSongBeat ).m_fRatio ),
		   10
		   );
		break;
	case speed_wait:
		ScreenTextEntry::TextEntry(
		   SM_BackFromSpeedWaitChange,
		   ENTER_SPEED_WAIT_VALUE,
		   ssprintf( "%.6f", m_pPlayerStateEdit->m_TimingState.GetSpeedSegmentAtBeat( m_pPlayerStateEdit->m_fSongBeat ).m_fWait ),
		   10
		   );
		break;
	case speed_mode: 
		{
			ScreenTextEntry::TextEntry(
						SM_BackFromSpeedModeChange,
						   ENTER_SPEED_MODE_VALUE,
						   "",
						   3
			);
			
			break;
		}
	case fake:
		{
			ScreenTextEntry::TextEntry(
				SM_BackFromFakeChange,
				ENTER_FAKE_VALUE,
			        ssprintf("%.6f", m_pPlayerStateEdit->m_TimingState.GetFakeAtBeat( m_pPlayerStateEdit->m_fSongBeat ) ),
				10
			);
			break;
		}
	case label:
		ScreenTextEntry::TextEntry(
			SM_BackFromLabelChange,
		   ENTER_LABEL_VALUE,
		   ssprintf( "%s", m_pPlayerStateEdit->m_TimingState.GetLabelAtBeat(m_pPlayerStateEdit->m_fSongBeat).c_str() ),
		   64
		   );
		break;
	case countsep:
		ScreenTextEntry::TextEntry(
			SM_BackFromCountSepChange,
			ENTER_COUNTSEP_VALUE,
			ssprintf( "%d", m_pPlayerStateEdit->m_TimingState.GetCountSepAtBeat(m_pPlayerStateEdit->m_fSongBeat) ? 1 : 0 ),
		   1
		   );
		break;
	/*case copy_timing:
	{
		clipboardTiming = GetAppropriateTiming();
		break;
	}
	case paste_timing:
	{
		if (GAMESTATE->m_bIsUsingStepTiming)
		{
			GAMESTATE->m_pCurSteps[PLAYER_1]->m_Timing = clipboardTiming;
		}
		else
		{
			GAMESTATE->m_pCurSong->m_SongTiming = clipboardTiming;
		}
		SetDirty(true);
		break;
	}
	case erase_step_timing:
		ScreenPrompt::Prompt( SM_DoEraseStepTiming, CONFIRM_TIMING_ERASE , PROMPT_YES_NO, ANSWER_NO );
	break;*/
		
	}
}

/*
 * (c) 2001-2004 Chris Danford
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
