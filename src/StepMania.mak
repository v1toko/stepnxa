#
# ICD AS (C) 2007 - MAKEFILE CREATED USING CDP MAKEFILE WIZARD
#
# Make sure that the environment variable CDPBase exists. This can
# be carried out by typing echo $CDPBase at the command promt, where
# the Makefile should be run by make. Note that the Makefile Wizard
# is created for the purpose of CDP Projects. It is, however, fully
# possible to use it for other project types as well.
#
# - To add a profiler in your executable, compile and link with, -pg.
# - To open/use shared libs at runtime, compile -rdynamic and link -ldl.
# - Make sure that a valid CDPLicense.key resides in $CDPBase/License.
# - Run CDP executables as root.


ifndef CFG
CFG=Release
endif

ifndef COMPILER
COMPILER=g++
endif

ifeq "$(CFG)" "Release"
CFLAGS=  -D_MBCS -finline-functions -Wall -mcpu=pentiumpro -D_M_IX86=500 -D_THREAD_SAFE -D_LINUX  -DNDEBUG  -DRELEASE -D_CRT_SECURE_NO_DEPRECATE
LIBS= -lCDP_Release -lrt -lpthread -lm 
ILFLAGS+= -I. -Ivorbis -Ilibjpeg -Ilua-5.1/include -Iffmpeg/include -IBaseClasses -Ilibtommath  -L -L.
TARGET=../../StepNXA/pruebascrollinggame/Program/StepMania
endif

ifeq "$(CFG)" "Debug"
CFLAGS= -g -D_MBCS -O0 -Wall -fno-inline  -mcpu=pentiumpro -D_M_IX86=500 -D_THREAD_SAFE -D_LINUX  -D_DEBUG  -DDEBUG -D_CRT_SECURE_NO_DEPRECATE
LIBS= -lCDP_Debug -lrt -lpthread -lm 
ILFLAGS+= -I. -Ivorbis -Ilibjpeg -Ilua-5.1/include -Iffmpeg/include -IBaseClasses -Ilibtommath  -L -L.
TARGET=../Program/StepMania-debug
endif

SOURCE_FILES= 	\


HEADER_FILES= 	\


OBJS=$(patsubst %.cpp, %.o,$(SOURCE_FILES))

.PHONY:all
all: $(TARGET)

$(TARGET): $(OBJS)
	$(COMPILER) $(CFLAGS) $(ILFLAGS) $(GLOBALS) $(OBJS) -o $(TARGET) $(LIBS)

%.o: %.cpp
	$(COMPILER) $(CFLAGS) $(ILFLAGS) $(GLOBALS) -o $@ -c $<

.PHONY:clean
clean:
	 -rm -f $(OBJS) $(TARGET)

