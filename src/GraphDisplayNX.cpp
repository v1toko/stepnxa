#include "global.h"
#include "GraphDisplayNX.h"
#include "ThemeManager.h"
#include "RageLog.h"
#include "ActorUtil.h"
#include "LuaManager.h"

REGISTER_ACTOR_CLASS( GraphNX )

//ubicaciones, se deducen de solo un X y un Y junto con un espaciado
#define GRAPHP1_X	THEME->GetMetricF("GraphNX","GraphP1X")
#define GRAPHP1_Y	THEME->GetMetricF("GraphNX","GraphP1Y")
#define SPACING_X	THEME->GetMetricF("GraphNX","SpacingX")
#define GRAPHP2_X	THEME->GetMetricF("GraphNX","GraphP2X")
#define GRAPHP2_Y	THEME->GetMetricF("GraphNX","GraphP2Y")

GraphNX::GraphNX()
{
}

GraphNX::GraphNX( const GraphNX &cpy ):
	ActorFrame(cpy),
	m_spr(cpy.m_spr),
	m_spr1(cpy.m_spr1),
	m_spr2(cpy.m_spr2),
	m_sprb(cpy.m_sprb)
{
	this->RemoveAllChildren();
	this->AddChild( &m_spr );
	this->AddChild( &m_spr1 );
	this->AddChild( &m_spr2 );
	this->AddChild( &m_sprb );
}

void GraphNX::LoadFromNode( const XNode *pNode )
{
	ActorFrame::LoadFromNode( pNode );
}

void GraphNX::Load( PlayerNumber pn )
{//HARDCODED!:
	m_spr.Load( THEME->GetPathG("GraphNX",ssprintf("barp%d",pn+1) ) );
	m_spr1.Load( THEME->GetPathG("GraphNX",ssprintf("barp%d",pn+1) ) ); 
	m_spr2.Load( THEME->GetPathG("GraphNX",ssprintf("barp%d",pn+1) ) );
	m_sprb.Load( THEME->GetPathG("GraphNX",ssprintf("barp%d",pn+1) ) );
	this->AddChild( &m_spr );
	this->AddChild( &m_spr1 );
	this->AddChild( &m_spr2 );
	this->AddChild( &m_sprb );

	m_Oxigen[pn].SetName( ssprintf( "OxigenP%d", pn+1 ) );
	m_Oxigen[pn].LoadFromFont( THEME->GetPathF( "" ,"Micrograma2") );
	ActorUtil::LoadAllCommandsAndSetXY( m_Oxigen[pn], "GraphNX" );
	this->AddChild( &m_Oxigen[pn] );

	m_Kcal[pn].SetName( ssprintf( "KCalP%d", pn+1 ) );
	m_Kcal[pn].LoadFromFont( THEME->GetPathF( "" ,"Micrograma2") );
	ActorUtil::LoadAllCommandsAndSetXY( m_Kcal[pn], "GraphNX" );
	this->AddChild( &m_Kcal[pn] );
}

void GraphNX::SetFromGameState( PlayerNumber pn )
{
	unsigned int iStagePlayed=0;//0->4

	{
		StageStats finalStats;
		STATSMAN->GetFinalEvalStageStats( finalStats );

		m_Oxigen[pn].SetText( ssprintf( "%.3f", finalStats.m_player[pn].m_fOxigen ) );
		m_Kcal[pn].SetText( ssprintf( "%.3f", finalStats.m_player[pn].m_fKcal ) );
	}

	//init zoom 0, para que no se vean
	m_spr.SetZoomY( 0 );
	m_spr1.SetZoomY( 0 );
	m_spr2.SetZoomY( 0 );
	m_sprb.SetZoomY( 0 );

	//LOG->Trace( "KcalPlayer1: %.3f", STATSMAN->m_vGraphStageStats[0].m_player[1].m_fKcal );
	//cuenta las stages
	for( unsigned i=0; i < STATSMAN->m_vPlayedStageStats.size(); i++ )
		iStagePlayed ++;

	//no pasar las 4 stages
	CLAMP( iStagePlayed, 0, 4 );

	float fZoomY[4];
	//inicializar
	for( unsigned i=0; i < 4 ; i++ )
	{
		fZoomY[i] = 0.0f;	
	}
	//asigna a StageKcal las calorias per-Stage
	for( unsigned i=0; i < iStagePlayed; i++ )
	{
		m_pStageStats[i] = &STATSMAN->m_vPlayedStageStats[i];
		//LOG->Trace( "KcalPlayer1: %.3f", m_pStageStats[i]->m_player[1].m_fKcal );
		//escala cada valor altiro, no despues!
		//LOG->Trace( "GraphDisplayNX:Set(), StageKcal[%d]: %.3f", i, StageKcal[i] );
		fZoomY[i] = SCALE( m_pStageStats[i]->m_player[pn].m_fKcal, 0, 30, 0, 1 );
		//para una visual, sin pasarse de los limites
		CLAMP( fZoomY[i], 0, 1 );
		//LOG->Trace( "GraphDisplayNX:Set(), fZoomY[%d]: %.3f", i, fZoomY[i] );

		if( pn == 0 )//player1
		{
			if( i==0 )
			{
				m_spr.SetXY( GRAPHP1_X, GRAPHP1_Y );
				m_spr.SetVertAlign( VertAlign_Bottom );
				m_spr.BeginTweening( 2.5 );
				m_spr.SetZoomY( fZoomY[i] );
				m_spr.Draw();
			}
			else if( i==1 )
			{
				m_spr1.SetXY( GRAPHP1_X+SPACING_X, GRAPHP1_Y );
				m_spr1.SetVertAlign( VertAlign_Bottom );
				m_spr1.BeginTweening( 2.5 );
				m_spr1.SetZoomY( fZoomY[i] );
				m_spr1.Draw();
			}
			else if( i==2 )
			{
				m_spr2.SetXY( GRAPHP1_X+SPACING_X*i, GRAPHP1_Y );
				m_spr2.SetVertAlign( VertAlign_Bottom );
				m_spr2.BeginTweening( 2.5 );
				m_spr2.SetZoomY( fZoomY[i] );
				m_spr2.Draw();
			}
			else if( i==3 )
			{
				m_sprb.SetXY( GRAPHP1_X+SPACING_X*i, GRAPHP1_Y );
				m_sprb.SetVertAlign( VertAlign_Bottom );
				m_sprb.BeginTweening( 2.5 );
				m_sprb.SetZoomY( fZoomY[i] );
				m_sprb.Draw();
			}
		}
		else if( pn == 1 )//player2
		{
			if( i==0 )
			{
				m_spr.SetXY( GRAPHP2_X, GRAPHP2_Y );
				m_spr.SetVertAlign( VertAlign_Bottom );
				m_spr.BeginTweening( 2.5 );
				m_spr.SetZoomY( fZoomY[i] );
				m_spr.Draw();
			}
			else if( i==1 )
			{
				m_spr1.SetXY( GRAPHP2_X+SPACING_X, GRAPHP2_Y );
				m_spr1.SetVertAlign( VertAlign_Bottom );
				m_spr1.BeginTweening( 2.5 );
				m_spr1.SetZoomY( fZoomY[i] );
				m_spr1.Draw();
			}
			else if( i==2 )
			{
				m_spr2.SetXY( GRAPHP2_X+SPACING_X*i, GRAPHP2_Y );
				m_spr2.SetVertAlign( VertAlign_Bottom );
				m_spr2.BeginTweening( 2.5 );
				m_spr2.SetZoomY( fZoomY[i] );
				m_spr2.Draw();
			}
			else if( i==3 )
			{
				m_sprb.SetXY( GRAPHP2_X+SPACING_X*i, GRAPHP2_Y );
				m_sprb.SetVertAlign( VertAlign_Bottom );
				m_sprb.BeginTweening( 2.5 );
				m_sprb.SetZoomY( fZoomY[i] );
				m_sprb.Draw();
			}
		}
	}
}

// lua start
#include "LuaBinding.h"
//IMPORTANTE EL LUA... el llama al codigo!, ver def.optioniconrow
class LunaGraphNX: public Luna<GraphNX>
{
public:
	static int set( T* p, lua_State *L )		{ p->SetFromGameState( Enum::Check<PlayerNumber>(L, 1) ); return 0; }

	LunaGraphNX()
	{
		ADD_METHOD( set );
	}
};

LUA_REGISTER_DERIVED_CLASS( GraphNX, ActorFrame )

/*
 * (c) 2002-2004 Chris Danford, "v1t0ko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
