#include "global.h"
#include "PlayerStageStats.h"
#include "RageLog.h"
#include "ThemeManager.h"
#include "Foreach.h"
#include "LuaManager.h"
#include <float.h>
#include "GameState.h"
#include "Course.h"
#include "Steps.h"
#include "ScoreKeeperNormal.h"
#include "PrefsManager.h"
#include "CommonMetrics.h"

#define GRADE_PERCENT_TIER(i)	THEME->GetMetricF("PlayerStageStats",ssprintf("GradePercent%s",GradeToString((Grade)i).c_str()))
#define GRADE_TIER02_IS_ALL_W2S	THEME->GetMetricB("PlayerStageStats","GradeTier02IsAllW2s")
#define GRADE_TIER01_IS_ALL_W2S THEME->GetMetricB("PlayerStageStats","GradeTier01IsAllW2s")
#define GRADE_TIER02_IS_FULL_COMBO THEME->GetMetricB("PlayerStageStats","GradeTier02IsFullCombo")
//MODIFICADO POR MI para el % de bads para obtener igual S
#define GRADE_TIER02_PERCENT_W5S THEME->GetMetricF("PlayerStageStats","GradeTier02PercentW5s")

static ThemeMetric<TapNoteScore> g_MinScoreToMaintainCombo( "Gameplay", "MinScoreToMaintainCombo" );

const float LESSON_PASS_THRESHOLD = 0.8f;

Grade GetGradeFromPercent( float fPercent, bool bMerciful );

void PlayerStageStats::Init()
{
	m_bJoined = false;
	m_vpPossibleSteps.clear();
	m_iStepsPlayed = 0;
	m_fAliveSeconds = 0;
	m_bFailed = false;
	//modemission
	m_bMissionFailed = false;
	m_iPossibleDancePoints = 0;
	m_iCurPossibleDancePoints = 0;
	m_iActualDancePoints = 0;
	m_iPossibleGradePoints = 0;
	m_iCurCombo = 0;
	m_iMaxCombo = 0;
	m_iCurMissCombo = 0;
	m_iCurScoreMultiplier = 1;
	m_iScore = 0;
	m_iBonus = 0;
	m_iMaxScore = 0;
	m_iCurMaxScore = 0;
	m_iSongsPassed = 0;
	m_iSongsPlayed = 0;
	m_fLifeRemainingSeconds = 0;
	m_iNumControllerSteps = 0;
	m_fCaloriesBurned = 0;
	//modificado por mi, kcal, o2
	m_fKcal = 0;
	m_fOxigen = 0;
	m_fMileage = 0;
	m_iCurNumHearts = 0;//modificado por mi
	m_iCurNumHiddens = 0;//hidden
	m_iCurNumPotions = 0;//potion
	m_iCurMaxMissCombo = 0;
	m_iCurNumVelocityItems = 0;

	m_iItemsLeft = 0;
	m_iItemsRight = 0;
	m_iItemsOver = 0;
	m_iItemsBlink = 0;
	m_iItemsUnder = 0;

	m_iIncorrectAnswer = 0;
	m_iCorrectAnswer = 0;

	m_fLifePercent = 0.f;

	ZERO( m_iTapNoteScores );
	ZERO( m_iHoldNoteScores );
	m_radarPossible.Zero();
	m_radarActual.Zero();

	m_fFirstSecond = FLT_MAX;
	m_fLastSecond = 0;

	m_pdaToShow = PerDifficultyAward_Invalid;
	m_pcaToShow = PeakComboAward_Invalid;
	m_iPersonalHighScoreIndex = -1;
	m_iMachineHighScoreIndex = -1;
	m_bDisqualified = false;
	m_rc = RankingCategory_Invalid;
	m_HighScore = HighScore();
}

void PlayerStageStats::AddStats( const PlayerStageStats& other )
{
	m_bJoined = other.m_bJoined;
	FOREACH_CONST( Steps*, other.m_vpPossibleSteps, s )
		m_vpPossibleSteps.push_back( *s );
	m_iStepsPlayed += other.m_iStepsPlayed;
	m_fAliveSeconds += other.m_fAliveSeconds;
	m_bFailed |= other.m_bFailed;
	m_bMissionFailed |= other.m_bMissionFailed;//modemission
	m_iPossibleDancePoints += other.m_iPossibleDancePoints;
	m_iActualDancePoints += other.m_iActualDancePoints;
	m_iCurPossibleDancePoints += other.m_iCurPossibleDancePoints;
	m_iPossibleGradePoints += other.m_iPossibleGradePoints;
	
	for( int t=0; t<NUM_TapNoteScore; t++ )
		m_iTapNoteScores[t] += other.m_iTapNoteScores[t];
	for( int h=0; h<NUM_HoldNoteScore; h++ )
		m_iHoldNoteScores[h] += other.m_iHoldNoteScores[h];
	m_iCurCombo += other.m_iCurCombo;
	m_iMaxCombo += other.m_iMaxCombo;
	m_iCurNumHearts += other.m_iCurNumHearts;//modificado por mi
	m_iCurNumHiddens += other.m_iCurNumHiddens;//hidden
	m_iCurMaxMissCombo += other.m_iCurMaxMissCombo;//misscombo
	m_iCurNumPotions += other.m_iCurNumPotions;//potion
	m_iCurNumVelocityItems += other.m_iCurNumVelocityItems;
	m_iItemsLeft += other.m_iItemsLeft;
	m_iItemsRight += other.m_iItemsRight;
	m_iItemsOver += other.m_iItemsOver;
	m_iItemsBlink += other.m_iItemsBlink;
	m_iItemsUnder += other.m_iItemsUnder;
	m_fLifePercent = other.m_fLifePercent; //no acumular
	m_iIncorrectAnswer += other.m_iIncorrectAnswer;
	m_iCorrectAnswer += other.m_iCorrectAnswer;
	m_iCurMissCombo = other.m_iCurMissCombo;

	m_iScore += other.m_iScore;
	m_iMaxScore += other.m_iMaxScore;
	m_iCurMaxScore += other.m_iCurMaxScore;
	m_radarPossible += other.m_radarPossible;
	m_radarActual += other.m_radarActual;
	m_iSongsPassed += other.m_iSongsPassed;
	m_iSongsPlayed += other.m_iSongsPlayed;
	m_iNumControllerSteps += other.m_iNumControllerSteps;
	m_fCaloriesBurned += other.m_fCaloriesBurned;
	//modificado por mi, kcal, o2
	m_fKcal += other.m_fKcal;
	m_fOxigen += other.m_fOxigen;
	m_fMileage += other.m_fMileage;
	m_fLifeRemainingSeconds = other.m_fLifeRemainingSeconds;	// don't accumulate
	m_bDisqualified |= other.m_bDisqualified;

	for( unsigned i=0; i<5; i++ )
		m_Cols[i] = other.m_Cols[i];

	const float fOtherFirstSecond = other.m_fFirstSecond + m_fLastSecond;
	const float fOtherLastSecond = other.m_fLastSecond + m_fLastSecond;
	m_fLastSecond = fOtherLastSecond;

	map<float,float>::const_iterator it;
	for( it = other.m_fLifeRecord.begin(); it != other.m_fLifeRecord.end(); ++it )
	{
		const float pos = it->first;
		const float life = it->second;
		m_fLifeRecord[fOtherFirstSecond+pos] = life;
	}

	for( unsigned i=0; i<other.m_ComboList.size(); ++i )
	{
		const Combo_t &combo = other.m_ComboList[i];

		Combo_t newcombo(combo);
		newcombo.m_fStartSecond += fOtherFirstSecond;
		m_ComboList.push_back( newcombo );
	}

	/* Merge identical combos.  This normally only happens in course mode, when a combo
	 * continues between songs. */
	for( unsigned i=1; i<m_ComboList.size(); ++i )
	{
		Combo_t &prevcombo = m_ComboList[i-1];
		Combo_t &combo = m_ComboList[i];
		const float PrevComboEnd = prevcombo.m_fStartSecond + prevcombo.m_fSizeSeconds;
		const float ThisComboStart = combo.m_fStartSecond;
		if( fabsf(PrevComboEnd - ThisComboStart) > 0.001 )
			continue;

		/* These are really the same combo. */
		prevcombo.m_fSizeSeconds += combo.m_fSizeSeconds;
		prevcombo.m_cnt += combo.m_cnt;
		m_ComboList.erase( m_ComboList.begin()+i );
		--i;
	}
}

Grade GetGradeFromPercent( float fPercent, bool bMerciful )
{
	if( bMerciful )
		fPercent = SCALE( fPercent, 0.0f, 1.0f, 0.5f, 1.0f );

	Grade grade = Grade_Failed;

	FOREACH_ENUM( Grade,g)
	{
		if( fPercent >= GRADE_PERCENT_TIER(g) )
		{
			grade = g;
			break;
		}
	}

	return grade;
}

Grade PlayerStageStats::GetGrade() const
{
	//MODIFICADO POR MI*************
	//SI LA LIFEMETERBAR TOCA EL FINAL
	//NO DA AUTOMATICAMENTE "F" de NOTA
	//if( m_bFailed )
	//	return Grade_Failed;

	/* XXX: This entire calculation should be in ScoreKeeper, but final evaluation
	 * is tricky since at that point the ScoreKeepers no longer exist. */
	float fActual = 0;
	int iTotalTaps = 0;
	float fPossible = 0;

	bool bIsBeginner = false;
	if( m_iStepsPlayed > 0 && !GAMESTATE->IsCourseMode() )
		bIsBeginner = m_vpPossibleSteps[0]->GetDifficulty() == Difficulty_Beginner;

	FOREACH_ENUM( TapNoteScore, tns )
	{
		//if( tns == TNS_W1 && GAMESTATE->IsBrainMode() ) continue;

		int iTapScoreValue = ScoreKeeperNormal::TapNoteScoreToGradePoints( tns, bIsBeginner );
		fActual += m_iTapNoteScores[tns] * iTapScoreValue;
		iTotalTaps += m_iTapNoteScores[tns];
		LOG->Trace( "GetGrade actual: %i * %i", m_iTapNoteScores[tns], iTapScoreValue );
	}

	//FOREACH_ENUM( HoldNoteScore, hns )
	//{
	//	int iHoldScoreValue = ScoreKeeperNormal::HoldNoteScoreToGradePoints( hns, bIsBeginner );
	//	//holds no cuentan = TNS_W1
	//	//fActual += m_iHoldNoteScores[hns] * iHoldScoreValue;
	//	LOG->Trace( "GetGrade actual: %i * %i", m_iHoldNoteScores[hns], iHoldScoreValue );
	//}

	//if( fActual > m_iPossibleGradePoints )//no m�s que los posibles
	//	fActual = m_iPossibleGradePoints;

	fPossible = iTotalTaps * ScoreKeeperNormal::TapNoteScoreToGradePoints( TNS_W1, bIsBeginner );

	LOG->Trace( "GetGrade: fActual: %f, fPossible: %f", fActual, fPossible );

	float fPercent = (fPossible == 0) ? 0 : fActual / fPossible;
	
	if( GAMESTATE->IsBrainMode() )
	{
		float fBrainActual = m_iCorrectAnswer;
		float fBrainTotal = m_iCorrectAnswer+m_iIncorrectAnswer;
		float fPercentBrain = (fBrainTotal == 0) ? 0 : fBrainActual/fBrainTotal;

		fPercent += fPercentBrain;
		fPercent /= 2; //promedio

		LOG->Trace( "GetGrade: fBrainActual: %f, Inc %d, fPercentBrain: %f, fPercent: %f", fBrainActual,m_iIncorrectAnswer , fPercentBrain, fPercent );

		if( FullComboOfScore(g_MinScoreToMaintainCombo) && fPercentBrain == 1.0f )
			return Grade_Tier01;
	}


	bool bMerciful = 
		m_iStepsPlayed > 0  &&
		GAMESTATE->m_PlayMode == PLAY_MODE_REGULAR &&
		PREFSMAN->m_bMercifulBeginner;
	for( int i = 0; i < m_iStepsPlayed; ++i )
	{
		if( m_vpPossibleSteps[i]->GetDifficulty() != Difficulty_Beginner )
			bMerciful = false;
	}

	Grade grade = GetGradeFromPercent( fPercent, bMerciful );

		//gradenodata = gradefailed
	//MODIFICADO POR MI
	//if( grade == Grade_Invalid)
	//	return Grade_Failed;

	//PlayerState pPlayerState;

	//if( grade == Grade_NoData )
	//	return Grade_Failed;
	//dice que si la nota es mayor que grade_tier07 ("F") retorna failed
	if( grade >= Grade_Tier07 )
	{
		//dice que si el failtype es distinto de inmediate y la nota es mayor que grade_tier04, entonces falla
		//esto es un Hack para que no de extra stage
		//if( PREFSMAN->get != PlayerOptions::FAIL_IMMEDIATE && grade >= Grade_Tier04 )
		//m_bFailed = true;
		return Grade_Failed;
	}


	LOG->Trace( "GetGrade: Grade: %s, %i", GradeToString(grade).c_str(), GRADE_TIER02_IS_ALL_W2S );
	if( GRADE_TIER02_IS_ALL_W2S && !GAMESTATE->IsBrainMode() )
	{
		if( FullComboOfScore(TNS_W1) )
			return Grade_Tier01;

		if( FullComboOfScore(TNS_W2) )
			return Grade_Tier02;

		grade = max( grade, Grade_Tier03 );
	}

	if( GRADE_TIER01_IS_ALL_W2S  && !GAMESTATE->IsBrainMode() )
	{
		if( FullComboOfScore(TNS_W2) )
			return Grade_Tier01;
		grade = max( grade, Grade_Tier02 );
	}

	if( GRADE_TIER02_IS_FULL_COMBO  && !GAMESTATE->IsBrainMode() )
	{
		//MODIFICADO POR MI
		//HACE ESCOGER EL % de W5 para que IGUALMENTE DE NOTA "S"
		float fPercentW5s = GetPercentageOfTaps( TNS_W5 );
		float fPercentMisses = GetPercentageOfTaps( TNS_Miss );
		//if( fPercentW5s >= 0.05f )
			//vPdas.push_back( AWARD_PERCENT_80_W3 );
		if( FullComboOfScore(g_MinScoreToMaintainCombo) )
			return Grade_Tier02;

		//ESTO NO DEBERIA ESTAR
		if (fPercentW5s <= GRADE_TIER02_PERCENT_W5S && !fPercentMisses > 0.00000000001)
			return Grade_Tier02;
		grade = max( grade, Grade_Tier03 );
	}

	return grade;
}

float PlayerStageStats::MakePercentScore( int iActual, int iPossible )
{
	if( iPossible == 0 )
		return 0; // div/0

	if( iActual == iPossible )
		return 1;	// correct for rounding error

	/* This can happen in battle, with transform attacks. */
	//ASSERT_M( iActual <= iPossible, ssprintf("%i/%i", iActual, iPossible) );

	float fPercent =  iActual / (float)iPossible;

	// don't allow negative
	fPercent = max( 0, fPercent );

	int iPercentTotalDigits = 3 + CommonMetrics::PERCENT_SCORE_DECIMAL_PLACES;	// "100" + "." + "00"

	// TRICKY: printf will round, but we want to truncate.  Otherwise, we may display a percent
	// score that's too high and doesn't match up with the calculated grade.
	float fTruncInterval = powf( 0.1f, (float)iPercentTotalDigits-1 );
	
	// TRICKY: ftruncf is rounding 1.0000000 to 0.99990004.  Give a little boost to 
	// fPercentDancePoints to correct for this.
	fPercent += 0.000001f;

	fPercent = ftruncf( fPercent, fTruncInterval );
	return fPercent;
}

RString PlayerStageStats::FormatPercentScore( float fPercentDancePoints )
{
	int iPercentTotalDigits = 3 + CommonMetrics::PERCENT_SCORE_DECIMAL_PLACES;	// "100" + "." + "00"
	
	RString s = ssprintf( "%*.*f%%", iPercentTotalDigits, (int)CommonMetrics::PERCENT_SCORE_DECIMAL_PLACES, fPercentDancePoints*100 );
	return s;
}

float PlayerStageStats::GetPercentDancePoints() const
{
	return MakePercentScore( m_iActualDancePoints, m_iPossibleDancePoints );
}

float PlayerStageStats::GetCurMaxPercentDancePoints() const
{
	if ( m_iPossibleDancePoints == 0 )
		return 0; // div/0

	if ( m_iCurPossibleDancePoints == m_iPossibleDancePoints )
		return 1; // correct for rounding error

	float fCurMaxPercentDancePoints = m_iCurPossibleDancePoints / (float)m_iPossibleDancePoints;

	return fCurMaxPercentDancePoints;
}

int PlayerStageStats::GetLessonScoreActual() const
{
	int iScore = 0;

	FOREACH_ENUM( TapNoteScore, tns )
	{
		switch( tns )
		{
		case TNS_AvoidMine:
		case TNS_W5:
		case TNS_W4:
		case TNS_W3:
		case TNS_W2:
		case TNS_W1:
		case TNS_CheckpointHit:
			iScore += m_iTapNoteScores[tns];
			break;
		}
	}

	FOREACH_ENUM( HoldNoteScore, hns )
	{
		switch( hns )
		{
		case HNS_Held:
			iScore += m_iHoldNoteScores[hns];
			break;
		}
	}

	return iScore;
}

int PlayerStageStats::GetLessonScoreNeeded() const
{
	float fScore = 0;

	FOREACH_CONST( Steps*, m_vpPossibleSteps, steps )
		fScore += (*steps)->GetRadarValues( PLAYER_1 ).m_Values.v.fNumTapsAndHolds;

	return lrintf( fScore * LESSON_PASS_THRESHOLD );
}

void PlayerStageStats::ResetScoreForLesson()
{
	m_iCurPossibleDancePoints = 0;
	m_iActualDancePoints = 0;
	FOREACH_ENUM( TapNoteScore, tns )
		m_iTapNoteScores[tns] = 0;
	FOREACH_ENUM( HoldNoteScore, hns )
		m_iHoldNoteScores[hns] = 0;
	m_iCurCombo = 0;
	m_iMaxCombo = 0;
	m_iCurMissCombo = 0;
	m_iScore = 0;
	m_iCurMaxScore = 0;
	m_iMaxScore = 0;
	m_iCurNumHearts = 0;//modificado por mi
	m_iCurNumHiddens = 0;//hidden
	m_iCurMaxMissCombo = 0;//misscombo
	m_iCurNumPotions = 0;//potion
	m_iCurNumVelocityItems = 0;
	m_fLifePercent = 0.f;
}

void PlayerStageStats::SetLifeRecordAt( float fLife, float fStepsSecond )
{
	// Don't save life stats in endless courses, or could run OOM in a few hours.
	if( GAMESTATE->m_pCurCourse && GAMESTATE->m_pCurCourse->IsEndless() )
		return;
	
	if( fStepsSecond < 0 )
		return;

	m_fFirstSecond = min( fStepsSecond, m_fFirstSecond );
	m_fLastSecond = max( fStepsSecond, m_fLastSecond );
	//LOG->Trace( "fLastSecond = %f", m_fLastSecond );

	// fSecond will always be greater than any value already in the map.
	m_fLifeRecord[fStepsSecond] = fLife;

	MESSAGEMAN->Broadcast( Message_LifeMeterChangedP1 );

	//
	// Memory optimization:
	// If we have three consecutive records A, B, and C all with the same fLife,
	// we can eliminate record B without losing data.  Only check the last three 
	// records in the map since we're only inserting at the end, and we know all 
	// earlier redundant records have already been removed.
	//
	map<float,float>::iterator C = m_fLifeRecord.end();
	--C;
	if( C == m_fLifeRecord.begin() ) // no earlier records left
		return;

	map<float,float>::iterator B = C;
	--B;
	if( B == m_fLifeRecord.begin() ) // no earlier records left
		return;

	map<float,float>::iterator A = B;
	--A;

	if( A->second == B->second && B->second == C->second )
		m_fLifeRecord.erase(B);
}

float PlayerStageStats::GetLifeRecordAt( float fStepsSecond ) const
{
	if( m_fLifeRecord.empty() )
		return 0;
	
	/* Find the first element whose key is greater than k. */
	map<float,float>::const_iterator it = m_fLifeRecord.upper_bound( fStepsSecond );

	/* Find the last element whose key is less than or equal to k. */
	if( it != m_fLifeRecord.begin() )
		--it;

	return it->second;

}

float PlayerStageStats::GetLifeRecordLerpAt( float fStepsSecond ) const
{
	if( m_fLifeRecord.empty() )
		return 0;
	
	/* Find the first element whose key is greater than k. */
	map<float,float>::const_iterator later = m_fLifeRecord.upper_bound( fStepsSecond );

	/* Find the last element whose key is less than or equal to k. */
	map<float,float>::const_iterator earlier = later;
	if( earlier != m_fLifeRecord.begin() )
		--earlier;

	if( later == m_fLifeRecord.end() )
		return earlier->second;
	
	if( earlier->first == later->first )	// two samples from the same time.  Don't divide by zero in SCALE
		return earlier->second;

	/* earlier <= pos <= later */
	return SCALE( fStepsSecond, earlier->first, later->first, earlier->second, later->second );
}


void PlayerStageStats::GetLifeRecord( float *fLifeOut, int iNumSamples, float fStepsEndSecond ) const
{
	for( int i = 0; i < iNumSamples; ++i )
	{
		float from = SCALE( i, 0, (float)iNumSamples, 0.0f, fStepsEndSecond );
		fLifeOut[i] = GetLifeRecordLerpAt( from );
	}
}

float PlayerStageStats::GetCurrentLife() const
{
	if( m_fLifeRecord.empty() )
		return 0;
	map<float,float>::const_iterator iter = m_fLifeRecord.end();
	--iter; 
	return iter->second;
}

/* If bRollover is true, we're being called before gameplay begins, so we can record
 * the amount of the first combo that comes from the previous song. */
void PlayerStageStats::UpdateComboList( float fSecond, bool bRollover )
{
	// Don't save combo stats in endless courses, or could run OOM in a few hours.
	if( GAMESTATE->m_pCurCourse && GAMESTATE->m_pCurCourse->IsEndless() )
		return;
	
	if( fSecond < 0 )
		return;

	if( !bRollover )
	{
		m_fFirstSecond = min( fSecond, m_fFirstSecond );
		m_fLastSecond = max( fSecond, m_fLastSecond );
		//LOG->Trace( "fLastSecond = %f", fLastSecond );
	}

	int cnt = m_iCurCombo;
	if( !cnt )
		return; /* no combo */

	if( m_ComboList.size() == 0 || m_ComboList.back().m_cnt >= cnt )
	{
		/* If the previous combo (if any) starts on -9999, then we rolled over some
		 * combo, but missed the first step.  Remove it. */
		if( m_ComboList.size() && m_ComboList.back().m_fStartSecond == -9999 )
			m_ComboList.erase( m_ComboList.begin()+m_ComboList.size()-1, m_ComboList.end() );

		/* This is a new combo. */
		Combo_t NewCombo;
		/* "start" is the position that the combo started within this song.  If we're
		 * recording rollover, the combo hasn't started yet (within this song), so put
		 * a placeholder in and set it on the next call.  (Otherwise, start will be less
		 * than fFirstPos.) */
		if( bRollover )
			NewCombo.m_fStartSecond = -9999;
		else
			NewCombo.m_fStartSecond = fSecond;
		m_ComboList.push_back( NewCombo );
	}

	Combo_t &combo = m_ComboList.back();
	if( !bRollover && combo.m_fStartSecond == -9999 )
		combo.m_fStartSecond = 0;

	combo.m_fSizeSeconds = fSecond - combo.m_fStartSecond;
	combo.m_cnt = cnt;

	if( bRollover )
		combo.m_rollover = cnt;
}

/* This returns the largest combo contained within the song, as if
 * m_bComboContinuesBetweenSongs is turned off. */
PlayerStageStats::Combo_t PlayerStageStats::GetMaxCombo() const
{
	if( m_ComboList.size() == 0 )
		return Combo_t();

	int m = 0;
	for( unsigned i = 1; i < m_ComboList.size(); ++i )
	{
		if( m_ComboList[i].m_cnt > m_ComboList[m].m_cnt )
			m = i;
	}

	return m_ComboList[m];
}

int	PlayerStageStats::GetComboAtStartOfStage() const
{
	if( m_ComboList.empty() )
		return 0;
	else
		return m_ComboList[0].m_rollover;
}

bool PlayerStageStats::FullComboOfScore( TapNoteScore tnsAllGreaterOrEqual ) const
{
	ASSERT( tnsAllGreaterOrEqual >= TNS_W5 );
	ASSERT( tnsAllGreaterOrEqual <= TNS_W1 );

	// If missed any holds, then it's not a full combo
	if( m_iHoldNoteScores[HNS_LetGo] > 0 )
		return false;

	// If has any of the judgments below, then not a full combo
	for( int i=TNS_Miss; i<tnsAllGreaterOrEqual; i++ )
	{
		if( m_iTapNoteScores[i] > 0 )
			return false;
	}

	// If has at least one of the judgments equal to or above, then is a full combo.
	for( int i=tnsAllGreaterOrEqual; i<NUM_TapNoteScore; i++ )
	{
		if( m_iTapNoteScores[i] > 0 )
			return true;
	}

	return false;
}

bool PlayerStageStats::SingleDigitsOfScore( TapNoteScore tnsAllGreaterOrEqual ) const
{
	return FullComboOfScore( tnsAllGreaterOrEqual ) &&
		m_iTapNoteScores[tnsAllGreaterOrEqual] < 10;
}

bool PlayerStageStats::OneOfScore( TapNoteScore tnsAllGreaterOrEqual ) const
{
	return FullComboOfScore( tnsAllGreaterOrEqual ) &&
		m_iTapNoteScores[tnsAllGreaterOrEqual] == 1;
}

int PlayerStageStats::GetTotalTaps() const
{
	int iTotalTaps = 0;
	for( int i=TNS_Miss; i<NUM_TapNoteScore; i++ )
	{
		iTotalTaps += m_iTapNoteScores[i];
	}
	return iTotalTaps;
}

//devuelve solo el numero de las que pisamos
int PlayerStageStats::GetTotalSteps() const
{
	float iTotalTaps = 0;
	for( int i=TNS_W5; i<TNS_CheckpointHit; i++ )
	{
		iTotalTaps += m_iTapNoteScores[i];
	}
	//iTotalTaps += m_iHoldNoteScores[HNS_Held];

	return iTotalTaps;
}

float PlayerStageStats::GetPercentageOfTaps( TapNoteScore tns ) const
{
	int iTotalTaps = 0;
	for( int i=TNS_Miss; i<NUM_TapNoteScore; i++ )
	{
		iTotalTaps += m_iTapNoteScores[i];
	}
	return m_iTapNoteScores[tns] / (float)iTotalTaps;
}

//MODIFICADO POR MI, life%
float PlayerStageStats::GetLifePercentOfSong() const
{
	//minas ni holdscores cuentan
	/*float iGoodScores = m_iTapNoteScores[TNS_W1]+m_iTapNoteScores[TNS_W2]+m_iTapNoteScores[TNS_W3]+
						m_iTapNoteScores[TNS_W4];
	float iBadScores = m_iTapNoteScores[TNS_W5]+m_iTapNoteScores[TNS_Miss];

	if( iGoodScores == 0 )
		return 0.f;

	float fResult = (100*iBadScores)/iGoodScores;*/
	float fLife = GetLifeRecordAt( GAMESTATE->m_fMusicSeconds )*100.f; /* 0 - 100, no 0 - 1*/

	//para nosotros el valor max del life es 1,4... clamp a 1
	CLAMP( fLife, 0, 100 );

	return /*100.0f-fResult*/fLife;
}

void PlayerStageStats::CalcAwards( PlayerNumber p, bool bGaveUp, bool bUsedAutoplay )
{
	LOG->Trace( "hand out awards" );
	
	m_pcaToShow = PeakComboAward_Invalid;

	if( bGaveUp || bUsedAutoplay )
		return;
	
	deque<PerDifficultyAward> &vPdas = GAMESTATE->m_vLastPerDifficultyAwards[p];

	LOG->Trace( "per difficulty awards" );

	// per-difficulty awards
	// don't give per-difficutly awards if using easy mods
	if( !IsDisqualified() )
	{
		if( FullComboOfScore( TNS_W3 ) )
			vPdas.push_back( AWARD_FULL_COMBO_W3 );
		if( SingleDigitsOfScore( TNS_W3 ) )
			vPdas.push_back( AWARD_SINGLE_DIGIT_W3 );
		if( FullComboOfScore( TNS_W2 ) )
			vPdas.push_back( AWARD_FULL_COMBO_W2 );
		if( SingleDigitsOfScore( TNS_W2 ) )
			vPdas.push_back( AWARD_SINGLE_DIGIT_W2 );
		if( FullComboOfScore( TNS_W1 ) )
			vPdas.push_back( AWARD_FULL_COMBO_W1 );
		
		if( OneOfScore( TNS_W3 ) )
			vPdas.push_back( AWARD_ONE_W3 );
		if( OneOfScore( TNS_W2 ) )
			vPdas.push_back( AWARD_ONE_W2 );

		float fPercentW3s = GetPercentageOfTaps( TNS_W3 );
		if( fPercentW3s >= 0.8f )
			vPdas.push_back( AWARD_PERCENT_80_W3 );
		if( fPercentW3s >= 0.9f )
			vPdas.push_back( AWARD_PERCENT_90_W3 );
		if( fPercentW3s >= 1.f )
			vPdas.push_back( AWARD_PERCENT_100_W3 );
	}

	// Max one PDA per stage
	if( !vPdas.empty() )
		vPdas.erase( vPdas.begin(), vPdas.end()-1 );
	
	if( !vPdas.empty() )
		m_pdaToShow = vPdas.back();
	else
		m_pdaToShow = PerDifficultyAward_Invalid;

	LOG->Trace( "done with per difficulty awards" );

	// DO give peak combo awards if using easy mods
	int iComboAtStartOfStage = GetComboAtStartOfStage();
	int iPeakCombo = GetMaxCombo().m_cnt;

	FOREACH_ENUM( PeakComboAward,pca )
	{
		int iLevel = 1000 * (pca+1);
		bool bCrossedLevel = iComboAtStartOfStage < iLevel && iPeakCombo >= iLevel;
		LOG->Trace( "pca = %d, iLevel = %d, bCrossedLevel = %d", pca, iLevel, bCrossedLevel );
		if( bCrossedLevel )
			GAMESTATE->m_vLastPeakComboAwards[p].push_back( pca );
	}

	if( !GAMESTATE->m_vLastPeakComboAwards[p].empty() )
		m_pcaToShow = GAMESTATE->m_vLastPeakComboAwards[p].back();
	else
		m_pcaToShow = PeakComboAward_Invalid;

	LOG->Trace( "done with per combo awards" );

}

bool PlayerStageStats::IsDisqualified() const
{
	if( !PREFSMAN->m_bDisqualification )
		return false;
	return m_bDisqualified;
}

LuaFunction( GetGradeFromPercent,	GetGradeFromPercent( FArg(1), false ) )
LuaFunction( FormatPercentScore,	PlayerStageStats::FormatPercentScore( FArg(1) ) )


// lua start
#include "LuaBinding.h"

class LunaPlayerStageStats: public Luna<PlayerStageStats>
{
public:
	DEFINE_METHOD( GetCaloriesBurned,		m_fCaloriesBurned )
	DEFINE_METHOD( GetLifeRemainingSeconds,		m_fLifeRemainingSeconds )
	DEFINE_METHOD( GetSurvivalSeconds,		GetSurvivalSeconds() )
	DEFINE_METHOD( GetCurrentCombo,			m_iCurCombo )
	DEFINE_METHOD( GetCurrentScoreMultiplier,	m_iCurScoreMultiplier )
	DEFINE_METHOD( GetScore,			m_iScore )
	DEFINE_METHOD( GetTapNoteScores,		m_iTapNoteScores[Enum::Check<TapNoteScore>(L, 1)] )
	DEFINE_METHOD( GetHoldNoteScores,		m_iHoldNoteScores[Enum::Check<HoldNoteScore>(L, 1)] )
	DEFINE_METHOD( FullCombo,			FullCombo() )
	DEFINE_METHOD( MaxCombo,			GetMaxCombo().m_cnt )
	DEFINE_METHOD( GetCurrentLife,			GetCurrentLife() )
	DEFINE_METHOD( GetGrade,			GetGrade() )
	DEFINE_METHOD( GetActualDancePoints,		m_iActualDancePoints )
	DEFINE_METHOD( GetPossibleDancePoints,		m_iPossibleDancePoints )
	DEFINE_METHOD( GetPercentDancePoints,		GetPercentDancePoints() )
	DEFINE_METHOD( GetLessonScoreActual,		GetLessonScoreActual() )
	DEFINE_METHOD( GetLessonScoreNeeded,		GetLessonScoreNeeded() )
	DEFINE_METHOD( GetPersonalHighScoreIndex,	m_iPersonalHighScoreIndex )
	DEFINE_METHOD( GetMachineHighScoreIndex,	m_iMachineHighScoreIndex )
	DEFINE_METHOD( GetPerDifficultyAward,		m_pdaToShow )
	DEFINE_METHOD( GetPeakComboAward,		m_pcaToShow )
	DEFINE_METHOD( IsDisqualified,			IsDisqualified() )
	//modificado por mi
	DEFINE_METHOD( GetCurrentNumHeartStepped, m_iTapNoteScores[TNS_Heart] )
	DEFINE_METHOD( GetCurrentNumHiddenStepped, m_iTapNoteScores[TNS_Hidden] )
	DEFINE_METHOD( GetCurrentNumPotionsStepped, m_iTapNoteScores[TNS_Potion] )
	DEFINE_METHOD( GetCurrentMissCombo, m_iCurMaxMissCombo )
	DEFINE_METHOD( GetCurrentVelocityItems, m_iCurNumVelocityItems )
	DEFINE_METHOD( GetCurrentOverHeadItems, m_iItemsOver )
	DEFINE_METHOD( GetCurrentLeftItems, m_iItemsLeft )
	DEFINE_METHOD( GetCurrentRightItems, m_iItemsRight )
	DEFINE_METHOD( GetCurrentDropItems, m_iItemsUnder )
	DEFINE_METHOD( GetCurrentBlinkItems, m_iItemsBlink )
	DEFINE_METHOD( GetCurrentLifePercent, GetLifePercentOfSong() )
	DEFINE_METHOD( GetCurrentCorrectAnswers, m_iCorrectAnswer )
	DEFINE_METHOD( GetCurrentIncorrectAnswers, m_iIncorrectAnswer )
	//modificado por mi, kcal, o2
	DEFINE_METHOD( GetOxigenTaken, m_fOxigen )
	DEFINE_METHOD( GetKcal, m_fKcal )
	DEFINE_METHOD( GetMileage, m_fMileage );
	DEFINE_METHOD( GetTotalSteps, GetTotalSteps() );

	static int GetPlayedSteps( T* p, lua_State *L )
	{
		lua_newtable(L);
		for( int i = 0; i < (int) min(p->m_iStepsPlayed, (int) p->m_vpPossibleSteps.size()); ++i )
		{
			p->m_vpPossibleSteps[i]->PushSelf(L);
			lua_rawseti( L, -2, i+1 );
		}
		return 1;
	}
	static int GetPossibleSteps( T* p, lua_State *L )
	{
		lua_newtable(L);
		for( int i = 0; i < (int) p->m_vpPossibleSteps.size(); ++i )
		{
			p->m_vpPossibleSteps[i]->PushSelf(L);
			lua_rawseti( L, -2, i+1 );
		}
		return 1;
	}
	static int GetKcalIncludingHolds( T* p, lua_State *L )
	{
		float ftaps = p->GetTotalSteps();

		float ftotal = ftaps*0.038f;

		lua_pushnumber( L, ftotal );

		return 1;
	}
	static int GetOxigenIncludingHolds( T* p, lua_State *L )
	{
		float ftaps = p->GetTotalSteps();

		float ftotal = ftaps*0.126f;

		lua_pushnumber( L, ftotal );

		return 1;
	}
	static int SetMissionFailed( T* p, lua_State *L )
	{
		p->SetMissionFailed( BArg( 1 ) );
		return 1;
	}
	static int GetNumberOfTnsByCol( T* p, lua_State *L )
	{
		TapNoteScore tns = Enum::Check<TapNoteScore>( L, 1 );
		RString sCol = SArg( 2 );
		if( sCol.CompareNoCase( "downleft" ) == 0 )
			lua_pushnumber( L, p->m_Cols[0].GetNumberOfTns( tns ) );
		else if( sCol.CompareNoCase( "upleft" ) == 0 )
			lua_pushnumber( L, p->m_Cols[1].GetNumberOfTns( tns ) );
		else if( sCol.CompareNoCase( "center" ) == 0 )
			lua_pushnumber( L, p->m_Cols[2].GetNumberOfTns( tns ) );
		else if( sCol.CompareNoCase( "upright" ) == 0 )
			lua_pushnumber( L, p->m_Cols[3].GetNumberOfTns( tns ) );
		else if( sCol.CompareNoCase( "downright" ) == 0 )
			lua_pushnumber( L, p->m_Cols[4].GetNumberOfTns( tns ) );
		else
			ASSERT_M( 0, ssprintf("No hay columna que corresponda a la dada %s", sCol.c_str() ) );

		return 1;
	}
	static int SetCurrentCombo( T *p, lua_State *L )
	{
		p->m_iCurCombo = IArg(1);
		return 1;
	}

	LunaPlayerStageStats()
	{
		ADD_METHOD( GetCurrentIncorrectAnswers );
		ADD_METHOD( GetCurrentCorrectAnswers );
		ADD_METHOD( GetCurrentLeftItems );
		ADD_METHOD( GetCurrentRightItems );
		ADD_METHOD( GetCurrentOverHeadItems );
		ADD_METHOD( GetCurrentBlinkItems );
		ADD_METHOD( GetCurrentDropItems );
		ADD_METHOD( GetCaloriesBurned );
		ADD_METHOD( GetLifeRemainingSeconds );
		ADD_METHOD( GetSurvivalSeconds );
		ADD_METHOD( GetCurrentCombo );
		ADD_METHOD( GetCurrentScoreMultiplier );
		ADD_METHOD( GetScore );
		ADD_METHOD( GetTapNoteScores );
		ADD_METHOD( GetHoldNoteScores );
		ADD_METHOD( FullCombo );
		ADD_METHOD( MaxCombo );
		ADD_METHOD( GetCurrentLife );
		ADD_METHOD( GetGrade );
		ADD_METHOD( GetActualDancePoints );
		ADD_METHOD( GetPossibleDancePoints );
		ADD_METHOD( GetPercentDancePoints );
		ADD_METHOD( GetLessonScoreActual );
		ADD_METHOD( GetLessonScoreNeeded );
		ADD_METHOD( GetPersonalHighScoreIndex );
		ADD_METHOD( GetMachineHighScoreIndex );
		ADD_METHOD( GetPerDifficultyAward );
		ADD_METHOD( GetPeakComboAward );
		ADD_METHOD( IsDisqualified );
		ADD_METHOD( GetPlayedSteps );
		ADD_METHOD( GetPossibleSteps );
		ADD_METHOD( GetCurrentNumHeartStepped );
		ADD_METHOD( GetCurrentNumHiddenStepped );
		ADD_METHOD( GetCurrentNumPotionsStepped );
		ADD_METHOD( GetKcal );
		ADD_METHOD( GetKcalIncludingHolds );
		ADD_METHOD( GetOxigenIncludingHolds );
		ADD_METHOD( GetOxigenTaken );
		ADD_METHOD( GetMileage );
		ADD_METHOD( GetCurrentMissCombo );
		ADD_METHOD( GetCurrentVelocityItems );
		ADD_METHOD( GetCurrentLifePercent );
		ADD_METHOD( GetTotalSteps );
		ADD_METHOD( SetMissionFailed );
		ADD_METHOD( GetNumberOfTnsByCol );
		ADD_METHOD( SetCurrentCombo );
	}
};

LUA_REGISTER_CLASS( PlayerStageStats )
// lua end


/*
 * (c) 2001-2004 Chris Danford, Glenn Maynard
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
