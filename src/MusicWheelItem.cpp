#include "global.h"
#include "MusicWheelItem.h"
#include "RageUtil.h"
#include "SongManager.h"
#include "GameManager.h"
#include "RageLog.h"
#include "GameConstantsAndTypes.h"
#include "GameState.h"
#include "ThemeManager.h"
#include "Steps.h"
#include "song.h"
#include "Course.h"
#include "ProfileManager.h"
#include "Profile.h"
#include "Style.h"
#include "ActorUtil.h"
#include "ThemeMetric.h"
#include "HighScore.h"

//MODIFICADO POR MI
//METRICS PARA EL MUSIC WHEEL BANNER
#define HIGH_QUAL_TIME		THEME->GetMetricF("FadingBanner","FadeSeconds")
#define USE_BANNER_WHEEL	THEME->GetMetricB("ScreenSelectMusic","UseBannerWheel")
#define BANNER_WIDTH		THEME->GetMetricF("MusicWheelItem","BannerWidth")
#define BANNER_HEIGHT		THEME->GetMetricF("MusicWheelItem","BannerHeight")
#define SORT_BANNER_WIDTH		THEME->GetMetricF("MusicWheelItem","SortBannerWidth")
#define SORT_BANNER_HEIGHT		THEME->GetMetricF("MusicWheelItem","SortBannerHeight")


WheelItemData::WheelItemData( WheelItemType wit, Song* pSong, RString sSectionName, Course* pCourse, RageColor color ):
	WheelItemBaseData(wit, sSectionName, color)
{
	m_pSong = pSong;
	m_pCourse = pCourse;
	m_Flags = WheelNotifyIcon::Flags();
}

MusicWheelItem::MusicWheelItem( RString sType ):
	WheelItemBase( sType )
{
	GRADES_SHOW_MACHINE.Load( sType, "GradesShowMachine" );

	data = NULL;

	//modificado por mi, lockedsong
	m_LockedSong.Load( THEME->GetPathG( sType, "lockedsong" ) );
	m_LockedSong.SetName( "LockedSong" );
	ActorUtil::LoadAllCommands( m_LockedSong, "MusicWheelItem" );
	m_LockedSong.PlayCommand( "On" );
	this->AddChild( &m_LockedSong );

	m_sprSectionBar.Load( THEME->GetPathG(sType,"section") );
	this->AddChild( m_sprSectionBar );

	m_sprExpandedBar.Load( THEME->GetPathG(sType,"expanded") );
	this->AddChild( m_sprExpandedBar );

	m_sprModeBar.Load( THEME->GetPathG(sType,"mode") );
	this->AddChild( m_sprModeBar );

	m_sprSortBar.Load( THEME->GetPathG(sType,"sort") );
	this->AddChild( m_sprSortBar );

	m_WheelNotifyIcon.SetName( "Icon" );
	ActorUtil::LoadAllCommands( m_WheelNotifyIcon, "MusicWheelItem" );
	ActorUtil::SetXY( m_WheelNotifyIcon, "MusicWheelItem" );
	m_WheelNotifyIcon.PlayCommand( "On" );
	this->AddChild( &m_WheelNotifyIcon );

	m_SortBanner.SetName( "SortBanner" );
	m_SortBanner.SetZTestMode( ZTEST_WRITE_ON_PASS );
	ActorUtil::LoadAllCommands( m_SortBanner, "MusicWheelItem" );
	m_SortBanner.PlayCommand( "On" );

	//MODIFICADO POR MI PARA EL BANNER WHEEL
	if( USE_BANNER_WHEEL )
	{
		if( GAMESTATE->IsNormalMode() || GAMESTATE->IsSpecialMode() || GAMESTATE->IsWorldTourMode() )
		{
			m_Banner.SetName( "Banner" );
			m_Banner.SetZTestMode( ZTEST_WRITE_ON_PASS );
			ActorUtil::LoadAllCommands( m_Banner, "MusicWheelItem" );
			m_Banner.PlayCommand( "On" );
			m_Banner.ScaleToClipped( BANNER_WIDTH, BANNER_HEIGHT );
			this->AddChild( &m_Banner );
		}
		else if( GAMESTATE->IsBrainMode() )
		{
			m_Banner.SetName( "BannerBrain" );
			m_Banner.SetZTestMode( ZTEST_WRITE_ON_PASS );
			ActorUtil::LoadAllCommands( m_Banner, "MusicWheelItem" );
			m_Banner.PlayCommand( "On" );
			m_Banner.ScaleToClipped( BANNER_WIDTH, BANNER_HEIGHT );
			this->AddChild( &m_Banner );
		}
		else if( GAMESTATE->IsEasyMode() )
		{
			m_Banner.SetName( "BannerEasy" );
			m_Banner.SetZTestMode( ZTEST_WRITE_ON_PASS );
			ActorUtil::LoadAllCommands( m_Banner, "MusicWheelItem" );
			m_Banner.PlayCommand( "On" );
			m_Banner.ScaleToClipped( BANNER_WIDTH, BANNER_HEIGHT );
			this->AddChild( &m_Banner );
		}
	}
	else
	{
		m_TextBanner.SetName( "SongName" );
		ActorUtil::LoadAllCommands( m_TextBanner, "MusicWheelItem" );
		m_TextBanner.Load( "TextBanner" );
		ActorUtil::SetXY( m_WheelNotifyIcon, "MusicWheelItem" );
		m_TextBanner.PlayCommand( "On" );
		this->AddChild( &m_TextBanner );
	}

		//MODIFICADO POR MI PARA EL BANNER WHEEL BACK
	/*
	if( GAMESTATE->IsNormalMode() || GAMESTATE->IsSpecialMode() || GAMESTATE->IsWorldTourMode() )
	{
		m_sprSongBar.Load( THEME->GetPathG(sType,"song") );
		m_sprSongBar.SetName( "SongBar" );
		ActorUtil::LoadAllCommands( m_sprSongBar, "MusicWheelItem" );
		m_sprSongBar.PlayCommand( "On" );
		this->AddChild( &m_sprSongBar );
	}
	else if( GAMESTATE->IsEasyMode() )
	{
		m_sprSongBar.Load( THEME->GetPathG(sType,"songeasy") );
		m_sprSongBar.SetName( "SongBarEasy" );
		ActorUtil::LoadAllCommands( m_sprSongBar, "MusicWheelItem" );
		m_sprSongBar.PlayCommand( "On" );
		this->AddChild( &m_sprSongBar );
	}
	else
	{
		m_sprSongBar.Load( THEME->GetPathG(sType,"songbrain") );
		m_sprSongBar.SetName( "SongBarBrain" );
		ActorUtil::LoadAllCommands( m_sprSongBar, "MusicWheelItem" );
		m_sprSongBar.PlayCommand( "On" );
		this->AddChild( &m_sprSongBar );
	}
	*/

	m_sprSongBar.Load( THEME->GetPathG(sType,"song") );
	//m_sprSongBar.SetName( "SongBar" );
	//ActorUtil::LoadAllCommands( m_sprSongBar, "MusicWheelItem" );
	//m_sprSongBar.PlayCommand( "On" );
	this->AddChild( m_sprSongBar );

	//m_sprSongAA.Load( THEME->GetPathG(sType,"song") );
	//this->AddChild( m_sprSongAA );

	m_textSection.SetName( "Section" );
	ActorUtil::LoadAllCommands( m_textSection, "MusicWheelItem" );
	m_textSection.LoadFromFont( THEME->GetPathF(sType,"section") );
	ActorUtil::SetXY( m_textSection, "MusicWheelItem" );
	m_textSection.SetShadowLength( 0 );
	m_textSection.PlayCommand( "On" );
	this->AddChild( &m_textSection );

	m_textRoulette.SetName( "Roulette" );
	ActorUtil::LoadAllCommands( m_textRoulette, "MusicWheelItem" );
	m_textRoulette.LoadFromFont( THEME->GetPathF(sType,"roulette") );
	ActorUtil::SetXY( m_textRoulette, "MusicWheelItem" );
	m_textRoulette.SetShadowLength( 0 );
	m_textRoulette.PlayCommand( "On" );
	this->AddChild( &m_textRoulette );

	m_textCourse.SetName( "CourseName" );
	m_textCourse.LoadFromFont( THEME->GetPathF(sType,"course") );
	LOAD_ALL_COMMANDS_AND_SET_XY( &m_textCourse );
	this->AddChild( &m_textCourse );

	m_textSort.SetName( "Sort" );
	m_textSort.LoadFromFont( THEME->GetPathF(sType,"sort") );
	LOAD_ALL_COMMANDS_AND_SET_XY( &m_textSort );
	this->AddChild( &m_textSort );

	FOREACH_PlayerNumber( p )
	{
		m_pGradeDisplay[p].Load( THEME->GetPathG(sType,"grades") );
		m_pGradeDisplay[p]->SetName( ssprintf("GradeP%d",int(p+1)) );
		this->AddChild( m_pGradeDisplay[p] );
		LOAD_ALL_COMMANDS_AND_SET_XY( m_pGradeDisplay[p] );
	}

	this->SubscribeToMessage( Message_CurrentStepsP1Changed );
	this->SubscribeToMessage( Message_CurrentStepsP2Changed );
	this->SubscribeToMessage( Message_CurrentTrailP1Changed );
	this->SubscribeToMessage( Message_CurrentTrailP2Changed );
	this->SubscribeToMessage( Message_PreferredDifficultyP1Changed );
	this->SubscribeToMessage( Message_PreferredDifficultyP2Changed );
}

MusicWheelItem::MusicWheelItem( const MusicWheelItem &cpy ):
	WheelItemBase( cpy ),
	GRADES_SHOW_MACHINE( cpy.GRADES_SHOW_MACHINE ),
	m_sprSectionBar( cpy.m_sprSectionBar ),
	m_sprExpandedBar( cpy.m_sprExpandedBar ),
	m_sprModeBar( cpy.m_sprModeBar ),
	m_sprSortBar( cpy.m_sprSortBar ),
	m_WheelNotifyIcon( cpy.m_WheelNotifyIcon ),
	m_TextBanner( cpy.m_TextBanner ),
	m_textSection( cpy.m_textSection ),
	m_textRoulette( cpy.m_textRoulette ),
	m_textCourse( cpy.m_textCourse ),
	m_textSort( cpy.m_textSort ),
	m_Banner( cpy.m_Banner ), //MODIFICADO POR MI PARA EL BANNER WHEEL
	m_sprSongBar( cpy.m_sprSongBar ),
	m_SortBanner( cpy.m_SortBanner ),
	m_LockedSong( cpy.m_LockedSong )//lockedsong
{
	data = NULL;

	this->AddChild( &m_Banner );//MODIFICADO POR MI PARA EL BANNER WHEEL
	this->AddChild( m_sprSongBar );
	this->AddChild( m_sprSectionBar );
	this->AddChild( m_sprExpandedBar );
	this->AddChild( m_sprModeBar );
	this->AddChild( m_sprSortBar );
	this->AddChild( &m_WheelNotifyIcon );
	this->AddChild( &m_TextBanner );
	this->AddChild( &m_textSection );
	this->AddChild( &m_textRoulette );
	this->AddChild( &m_textCourse );
	this->AddChild( &m_textSort );
	this->AddChild( &m_LockedSong );//lockedsong
	this->AddChild( &m_SortBanner );

	FOREACH_PlayerNumber( p )
	{
		m_pGradeDisplay[p] = cpy.m_pGradeDisplay[p];
		this->AddChild( m_pGradeDisplay[p] );
	}
}

MusicWheelItem::~MusicWheelItem()
{
}

void MusicWheelItem::LoadFromWheelItemData( const WheelItemBaseData *pWIBD, int iIndex, bool bHasFocus )
{
	const WheelItemData *pWID = dynamic_cast<const WheelItemData*>( pWIBD );
	
	ASSERT( pWID != NULL );
	data = pWID;


	// hide all
	m_WheelNotifyIcon.SetVisible( false );
	m_TextBanner.SetVisible( false );
	m_sprSongBar->SetVisible( false );
	m_sprSectionBar->SetVisible( false );
	m_sprExpandedBar->SetVisible( false );
	m_sprModeBar->SetVisible( false );
	m_sprSortBar->SetVisible( false );
	m_textSection.SetVisible( false );
	m_textRoulette.SetVisible( false );
	m_Banner.SetVisible(false);//MODIFICADO POR MI PARA EL BANNER WHEEL
	m_LockedSong.SetVisible(false);//lockedsong
	FOREACH_PlayerNumber( p )
		m_pGradeDisplay[p]->SetVisible( false );
	m_textCourse.SetVisible( false );
	m_textSort.SetVisible( false );
	m_SortBanner.SetVisible( false );

	// init and unhide type specific stuff
	switch( pWID->m_Type )
	{
	DEFAULT_FAIL( pWID->m_Type );
	case TYPE_SECTION:
	case TYPE_COURSE:
	case TYPE_SORT:
	{
		RString sDisplayName, sTranslitName;
		BitmapText *bt = NULL;
		switch( pWID->m_Type )
		{
		case TYPE_SECTION:
			sDisplayName = SONGMAN->ShortenGroupName(data->m_sText);
			bt = &m_textSection;
			break;
		case TYPE_COURSE:
			sDisplayName = data->m_pCourse->GetDisplayFullTitle();
			sTranslitName = data->m_pCourse->GetTranslitFullTitle();
			bt = &m_textCourse;
			m_WheelNotifyIcon.SetFlags( data->m_Flags );
			m_WheelNotifyIcon.SetVisible( false );//MODIFICADO POR Mi
			break;
		case TYPE_SORT:
			sDisplayName = data->m_sLabel;
			bt = &m_textSort;
			{
				RageTextureID ID;
				ID.filename = THEME->GetPathG("Sort", sDisplayName.MakeLower());
				m_SortBanner.Load(ID);
			}
			m_SortBanner.SetVisible(true);
			m_SortBanner.ScaleToClipped( SORT_BANNER_WIDTH, SORT_BANNER_HEIGHT );
			break;
		DEFAULT_FAIL( pWID->m_Type );
		}

		bt->SetText( sDisplayName, sTranslitName );
		bt->SetDiffuse( data->m_color );
		bt->SetRainbowScroll( false );
		bt->SetVisible( false );//MODIFICADO POR MI
		break;
	}
	case TYPE_SONG:
		//MODIFICADO POR MI
		//PARA EL BANNER WHEEL
		if( USE_BANNER_WHEEL )
		{
			//RString sBannerPath;
			//sBannerPath = data->m_pSong->GetBannerPath();
			m_Banner.LoadFromSong( data->m_pSong );//modificado por mi
			m_Banner.SetVisible( true );
			//asigna el color para el banner frame!
			//if( !GAMESTATE->IsEasyMode() )			
			{
				if( data->m_pSong->m_cGroupColor == RageColor( 1.0f, 1.0f, 1.0f, 1.0f ) )
					m_sprSongBar->SetDiffuse( SONGMAN->GetSongColor( data->m_pSong ) );
				else
					m_sprSongBar->SetDiffuse( data->m_pSong->m_cGroupColor );
			}			

			if( data->m_pSong->m_bIsSpecialSongLocked || data->m_pSong->m_bMissionWorldTourLocked )
				m_LockedSong.SetVisible( true );
			else
				m_LockedSong.SetVisible( false );
		}
		else
		{
			m_TextBanner.LoadFromSong( data->m_pSong );
			m_TextBanner.SetDiffuse( data->m_color );
			m_TextBanner.SetVisible( true );
		}

		m_WheelNotifyIcon.SetFlags( data->m_Flags );
		m_WheelNotifyIcon.SetVisible( true );
		RefreshGrades();
		break;
	case TYPE_ROULETTE:
		m_textRoulette.SetText( THEME->GetString("MusicWheel","Roulette") );
		m_textRoulette.SetVisible( true );
		break;

	case TYPE_RANDOM:
		m_textRoulette.SetText( THEME->GetString("MusicWheel","Random") );
		m_textRoulette.SetVisible( true );
		break;

	case TYPE_PORTAL:
		m_textRoulette.SetText( THEME->GetString("MusicWheel","Portal") );
		m_textRoulette.SetVisible( true );
		break;
	}

	Actor *pBars[] = { m_sprBar, m_sprExpandedBar, m_sprSectionBar, m_sprModeBar, m_sprSortBar, m_sprSongBar, NULL };
	for( unsigned i = 0; pBars[i] != NULL; ++i )
		pBars[i]->SetVisible( false );

	switch( data->m_Type )
	{
	case TYPE_SECTION: 
	case TYPE_ROULETTE:
	case TYPE_RANDOM:
	case TYPE_PORTAL:
		if( m_bExpanded )
			m_sprExpandedBar->SetVisible( true );
		else
			m_sprSectionBar->SetVisible( true );
		break;
	case TYPE_SORT:
		if( pWID->m_pAction->m_pm != PlayMode_Invalid )
			m_sprModeBar->SetVisible( true );
		else
			m_sprSortBar->SetVisible( true );
		break;
	case TYPE_SONG:		
	case TYPE_COURSE:
		if( !GAMESTATE->IsCourseMode() )//MODIFICADO POR MI
			m_sprSongBar->SetVisible( true );
		if( GAMESTATE->IsNormalMode() )//si no esta en modo mission, ocultar
			m_LockedSong.SetVisible( false );
		break;
	DEFAULT_FAIL( data->m_Type );
	}

	for( unsigned i = 0; pBars[i] != NULL; ++i )
	{
		if( !pBars[i]->GetVisible() )
			continue;
		SetGrayBar( pBars[i] );
		break;
	}

	// Call "Set" so that elements can react to the change in song.
	{
		Message msg( "Set" );
		msg.SetParam( "Song", data->m_pSong );
		msg.SetParam( "Course", data->m_pCourse );
		msg.SetParam( "Index", iIndex );
		msg.SetParam( "HasFocus", bHasFocus );
		msg.SetParam( "SongGroup", pWID->m_sText );
		this->HandleMessage( msg );
	}
}

void MusicWheelItem::RefreshGrades()
{
	if( data == NULL )
		return; // LoadFromWheelItemData() hasn't been called yet.
	FOREACH_HumanPlayer( p )
	{
		m_pGradeDisplay[p]->SetVisible( false );

		if( data->m_pSong == NULL && data->m_pCourse == NULL )
			continue;

		Difficulty dc;
		if( GAMESTATE->m_pCurSteps[p] )
			dc = GAMESTATE->m_pCurSteps[p]->GetDifficulty();
		else if( GAMESTATE->m_pCurTrail[p] )
			dc = GAMESTATE->m_pCurTrail[p]->m_CourseDifficulty;
		else
			dc = GAMESTATE->m_PreferredDifficulty[p];

		ProfileSlot ps;
		if( PROFILEMAN->IsPersistentProfile(p) )
			ps = (ProfileSlot)p;
		else if( GRADES_SHOW_MACHINE )
			ps = ProfileSlot_Machine;
		else
			continue;

		m_pGradeDisplay[p]->SetVisible( true );


		Profile *pProfile = PROFILEMAN->GetProfile(ps);

		HighScoreList *pHSL = NULL;
		if( PROFILEMAN->IsPersistentProfile(ps) && dc != Difficulty_Invalid )
		{
			if( data->m_pSong )
			{
				const Steps* pSteps = SongUtil::GetStepsByDifficulty( data->m_pSong, GAMESTATE->GetCurrentStyle()->m_StepsType, dc );
				if( pSteps != NULL )
					pHSL = &pProfile->GetStepsHighScoreList(data->m_pSong, pSteps);
			}
			else if( data->m_pCourse )
			{
				const Trail *pTrail = data->m_pCourse->GetTrail( GAMESTATE->GetCurrentStyle()->m_StepsType, dc );
				if( pTrail != NULL )
					pHSL = &pProfile->GetCourseHighScoreList( data->m_pCourse, pTrail );
			}
		}

		Message msg( "SetGrade" );
		msg.SetParam( "PlayerNumber", p );
		if( pHSL )
		{
			msg.SetParam( "Grade", pHSL->HighGrade );
			msg.SetParam( "NumTimesPlayed", pHSL->GetNumTimesPlayed() );
		}
		m_pGradeDisplay[p]->HandleMessage( msg );
	}
}

void MusicWheelItem::HandleMessage( const Message &msg )
{
	if( msg == Message_CurrentStepsP1Changed ||
	    msg == Message_CurrentStepsP2Changed ||
	    msg == Message_CurrentTrailP1Changed ||
	    msg == Message_CurrentTrailP2Changed ||
	    msg == Message_PreferredDifficultyP1Changed ||
	    msg == Message_PreferredDifficultyP2Changed )
	{
		RefreshGrades();
	}

	WheelItemBase::HandleMessage( msg );
}

/*
 * (c) 2001-2004 Chris Danford, Chris Gomez, Glenn Maynard
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
