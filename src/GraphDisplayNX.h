/* GraphDisplayNX - Muestra en graficos, las calorias per-Player */

#ifndef GRAPH_DIPLAY_NX_H
#define GRAPH_DIPLAY_NX_H

#include "ActorFrame.h"
#include "Sprite.h"
#include "PlayerNumber.h"
#include "PlayerStageStats.h"
#include "StatsManager.h"
#include "StageStats.h"
#include "BitmapText.h"

struct lua_State;

class GraphNX : public ActorFrame
{
public:
	GraphNX();
	GraphNX( const GraphNX &cpy );
	void Load( PlayerNumber pn );
	void SetFromGameState( PlayerNumber pn );
	virtual GraphNX *Copy() const;
	virtual void LoadFromNode( const XNode* pNode );

	//
	// Commands
	//
	virtual void PushSelf( lua_State *L );

protected:
	Sprite		m_spr;//stage1
	Sprite		m_spr1;//2
	Sprite		m_spr2;//3
	Sprite		m_sprb;//bonus
	StageStats  *m_pStageStats[4];
	BitmapText	m_Oxigen[NUM_PLAYERS];
	BitmapText	m_Kcal[NUM_PLAYERS];
};

#endif

/*
 * (c) 2002-2004 Chris Danford, "v1t0ko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
