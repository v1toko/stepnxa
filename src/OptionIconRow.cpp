#include "global.h"
#include "OptionIconRow.h"
#include "ThemeManager.h"
#include "PlayerOptions.h"
#include "GameState.h"
#include "RageLog.h"
#include "PlayerState.h"
#include "ActorUtil.h"
#include "XmlFile.h"
#include "LuaManager.h"
#include "ThemeMetric.h"

REGISTER_ACTOR_CLASS( OptionIconRow )

#define SPACING_X					THEME->GetMetricF("OptionIconRow","SpacingX")
#define SPACING_Y					THEME->GetMetricF("OptionIconRow","SpacingY")
#define SPACING_MISSION_X			THEME->GetMetricF("OptionIconRow","SpacingMissionX")
#define SPACING_MISSION_Y			THEME->GetMetricF("OptionIconRow","SpacingMissionY")
//#define OPTION_NAMES				"TurnUnder,u;Sudden,a;Boost,A;Hidden,V;Brake,D;Expand,W;Sink,S;XMode,X;NXMode,N;Stealth,n;Blink,B;Dark,F;ReverseGrade,R;Mirror,M;Reverse,P;Rise,r;SuperShuffle,C;RandomSpeed,9"
#define OPTION_NAMES				"RandomSkin,K;Tornado,Q;TurnUnder,u;Sudden,a;Boost,A;Hidden,V;Brake,D;Expand,W;Sink,S;XMode,X;NXMode,N;Stealth,n;Blink,B;Dark,F;ReverseGrade,R;Mirror,M;Reverse,P;Rise,r;SuperShuffle,C;RandomSpeed,9;borrego,b;canond,c;dog,d;easy,e;gostop,g;hee,h;ice,i;interference,l;old,o;poker,p;fire,q;slime,s;tone,t;wpf2010,w;yasangma,y"
//#define OPTION(sOptionName)		THEME->GetMetric ("OptionNamesChars",ssprintf("Option%s",sOptionName.c_str()))

OptionIconRow::OptionIconRow()
{
	for( unsigned i=0; i<NUM_OPTION_COLS; i++ )
	{
		if( GAMESTATE->IsMissionMode() || GAMESTATE->IsWorldTourMode() )//modificado por mi, modemission
			m_OptionIcon[i].SetXY( i*SPACING_MISSION_X, i*SPACING_MISSION_Y );
		else
			m_OptionIcon[i].SetXY( i*SPACING_X, i*SPACING_Y );

		this->AddChild( &m_OptionIcon[i] );
	}
	m_sOldMods = "";
}

void OptionIconRow::LoadFromNode( const XNode* pNode )
{
	Load();

	ActorFrame::LoadFromNode( pNode );
}

struct OptionColumnEntry
{
	char szString[40];
	int iSlotIndex;
};

static const OptionColumnEntry g_OptionColumnEntries[] =
{
	{"Boost",		1},
	{"Brake",		1},
	//{"Wave",		0},
	{"Expand",		1},
	//{"Boomerang",	0},
	//{"Drunk",		0},

	//{"Dizzy",		1},
	//{"Mini",		1},
	//{"Flip",		1},
	//{"Tornado",		1},

	{"Hidden",		2},
	{"Sudden",		2},
	{"Stealth",		2},
	{"Blink",		2},

	{"RandomVanish",3},
	{"Sink",		3},
	{"Rise",		3},
	{"Mirror",		3},
	//{"Left",		3},
	//{"Right",		3},
	//{"Shuffle",		3},
	{"SuperShuffle",4},
	{"NXMode",		4},

	//{"Little",		4},
	//{"NoHolds",		4},
	{"Dark",		4},
	{"Blind",		4},
	{"ReverseGrade",4},

	{"Reverse",		4},
	{"RandomSkin",	5},
	//{"Split",		5},
	//{"Alternate",	5},
	//{"Cross",		5},
	//{"Centered",	5},

	//{"Incoming",	6},
	//{"Space",		6},
	//{"Hallway",		6},
	//{"Distant",		6},
	{"Rate11",		6},
	{"Rate13",		6},
	{"Rate15",		6},
};

int g_iLastOptionEntered; //place of

int OptionIconRow::OptionToPreferredColumn( RString sOptionText )
{
	/* Speedups always go in column 0. digit ... x */
	if( sOptionText.size() > 1 &&
		isdigit(sOptionText[0])    &&
		tolower(sOptionText[sOptionText.size()-1]) == 'x' )
	{
		return 0;
	}

	for( unsigned i=0; i<ARRAYLEN(g_OptionColumnEntries); i++ )
		if( g_OptionColumnEntries[i].szString == sOptionText )
			return g_OptionColumnEntries[i].iSlotIndex;

	/* This warns about C1234 and noteskins. */
//	LOG->Warn("Unknown option: '%s'", sOptionText.c_str() );
	return 5;
}

void OptionIconRow::Load()
{
	for( unsigned i=0; i<NUM_OPTION_COLS; i++ )
		m_OptionIcon[i].Load( "OptionIconRow" );		

	m_OptionNames.clear();
	vector<RString> opNames;

	RString sOptions = OPTION_NAMES;
	split(sOptions, ";", opNames );
	ASSERT( opNames.size() > 0 );

	for( unsigned int i = 0; i < opNames.size(); i++ ) 
	{
		vector<RString> vOpChars;
		split( opNames[i], ",", vOpChars );

		ASSERT( vOpChars.size() == 2 );
		m_OptionNames[vOpChars.at(0)] = vOpChars.at(1);
	}
}

void OptionIconRow::SetFromGameState( PlayerNumber pn )
{
	// init

	RString sOptions = GAMESTATE->m_pPlayerState[pn]->m_PlayerOptions.GetStage().GetString();
	vector<RString> asOptions;
	split( sOptions, ", ", asOptions, true );


	RString asTabs[NUM_OPTION_COLS-1];	// fill these with what will be displayed on the tabs
	
	// for each option, look for the best column to place it in
	for( unsigned i=0; i<asOptions.size(); i++ )
	{
		RString sOption = asOptions[i];

		int iPerferredCol = OptionToPreferredColumn( sOption );

		if( iPerferredCol == -1 )
			continue;	// skip

		// search for a vacant spot
		for( unsigned i=iPerferredCol; i<NUM_OPTION_COLS-1; i++ )
		{
			if( asTabs[i] != "" )
				continue;
			else
			{
				asTabs[i] = sOption;
				break;
			}
		}
	}

	for( unsigned i=0; i<NUM_OPTION_COLS; i++ )
	{
		if( i == 0 )
		{
			m_OptionIcon[i].Set( pn, "", true );
		}
		else
		{
			//busca el option
			RString sOption = asTabs[i-1];

			for ( map<RString,RString>::iterator it=m_OptionNames.begin() ; it != m_OptionNames.end(); it++ )
			{				
				//hacemos el reemplazo de los mods por las letras que le corresponden a cada uno.
				//if( strcmp( (*it).first, sOption ) == 0 ) 
				if( (*it).first == sOption )
				{
					sOption = (*it).second;
				}
			}
			m_OptionIcon[i].Set( pn, sOption, false );	
			//LOG->Trace( "MOD: %s, Letra: %s", asTabs[i-1].c_str(), sOption.c_str() );
		}
	}

	//LOG->Trace( "MOD: old: %s, cur: %s", m_sOldMods.c_str(), sOptions.c_str() );

	//mandamos los options
	{
		vector<RString> voldMods;
		split( m_sOldMods, ",", voldMods );

		RString sModF = "NONE";

		if( asOptions.size() == voldMods.size() )
		{
			bool bFinded = false;

			for( int i=0; i < asOptions.size(); i++ )
			{
				for( int j=0; j<voldMods.size(); j++ )
				{
					RString mod = voldMods[j];
					Trim(mod);
					//LOG->Trace( "MOD i: %s, j: %s", asOptions[i].c_str(), mod.c_str() );
					if( asOptions[i].EqualsNoCase(mod) ) //no encontramos el mod
					{
						bFinded = false;
						break;
					} 
					else 
					{
						bFinded = true;
						sModF = asOptions[i];
						//LOG->Trace( "MOD encontrado: %s", sModF.c_str() );
						//break;
					}
				}
				if( bFinded )
					break;
			}
		}
		else if( asOptions.size() > voldMods.size() )
		{
			bool bFinded = false;

			for( int i=0; i < asOptions.size(); i++ )
			{
				for( int j=0; j<voldMods.size(); j++ )
				{
					RString mod = voldMods[j];
					Trim(mod);
					//LOG->Trace( "2MOD i: %s, j: %s", asOptions[i].c_str(), mod.c_str() );
					
					if( asOptions[i].EqualsNoCase(mod) ) //no encontramos el mod
					{
						//LOG->Trace( "2MOD identicos i: %s, j: %s", asOptions[i].c_str(), voldMods[j].c_str() );
						bFinded = false;
						break;
					} 
					else 
					{
						bFinded = true;
						sModF = asOptions[i];
						//LOG->Trace( "2MOD encontrado: %s", sModF.c_str() );
						//break;
					}
				}
				if( bFinded )
					break;
			}
		}

		int iPerferredCol = OptionToPreferredColumn( sModF );
			

		Message msg("ModsIconsChanged" + PlayerNumberToString(pn));
		//msg.SetParam("OldMods", m_sOldMods );
		//msg.SetParam("CurMods", sOptions ); //shortcut for GAMESTATE->GetPlayerState->blablabalba
		msg.SetParam("FindedMod", sModF );
		msg.SetParam("Player", PlayerNumberToString(pn));
		msg.SetParam("ColNum", iPerferredCol);
		MESSAGEMAN->Broadcast(msg);		
	}

	m_sOldMods = sOptions;//guardamos los options para la proxima iteración
}

// lua start
#include "LuaBinding.h"

class LunaOptionIconRow: public Luna<OptionIconRow>
{
public:
	static int set( T* p, lua_State *L )		{ p->SetFromGameState( Enum::Check<PlayerNumber>(L, 1) ); return 0; }
	static int GetOptionColumn( T* p, lua_State *L)	
	{ 
		RString mod = SArg(1);
		lua_pushnumber( L, p->OptionToPreferredColumn( mod ) );
		return 1;
	}

	LunaOptionIconRow()
	{
		ADD_METHOD( set );
		ADD_METHOD( GetOptionColumn );
	}
};

LUA_REGISTER_DERIVED_CLASS( OptionIconRow, ActorFrame )

// lua end

/*
 * (c) 2002-2004 Chris Danford
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
