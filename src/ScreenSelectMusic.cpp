#include "global.h"
#include "ScreenSelectMusic.h"
#include "ScreenManager.h"
#include "PrefsManager.h"
#include "SongManager.h"
#include "GameManager.h"
#include "GameSoundManager.h"
#include "GameConstantsAndTypes.h"
#include "RageLog.h"
#include "InputMapper.h"
#include "GameState.h"
#include "CodeDetector.h"
#include "ThemeManager.h"
#include "Steps.h"
#include "ActorUtil.h"
#include "RageTextureManager.h"
#include "Course.h"
#include "ProfileManager.h"
#include "Profile.h"
#include "MenuTimer.h"
#include "StatsManager.h"
#include "StepsUtil.h"
#include "Foreach.h"
#include "Style.h"
#include "PlayerState.h"
#include "CommonMetrics.h"
#include "BannerCache.h"
#include "song.h"
#include "InputEventPlus.h"
#include "OptionsList.h"
#include "Quad.h"
#include "MessageManager.h"
#include "NoteData.h"
#include "NoteDataUtil.h"

static const char *SelectionStateNames[] = {
	"SelectingSong",
	"SelectingSteps",
	"SelectinGroups",
	"Finalized"
};
XToString( SelectionState );


const int NUM_SCORE_DIGITS	=	9;

#define SHOW_OPTIONS_MESSAGE_SECONDS		THEME->GetMetricF( m_sName, "ShowOptionsMessageSeconds" )
//MODIFICADO POR MI
//METRIC AGREGADA

#define LAUNCHSCREENWHENLATEJOIN			THEME->GetMetric(m_sName,"LaunchScreenWhenLateJoin")
#define USENXSTYLE							THEME->GetMetricB( m_sName,"UseNXStyle")
#define LAUNCHSCREENWHENBUYSONG				THEME->GetMetric( m_sName, "LaunchScreenWhenBuyASong" )

AutoScreenMessage( SM_AllowOptionsMenuRepeat )
AutoScreenMessage( SM_SongChanged )
AutoScreenMessage( SM_SortOrderChanging )
AutoScreenMessage( SM_SortOrderChanged )
AutoScreenMessage( SM_PlayerJoinLate )

static RString g_sCDTitlePath;
static bool g_bWantFallbackCdTitle;
static bool g_bCDTitleWaiting = false;
static RString g_sBannerPath;
static bool g_bBannerWaiting = false;
static bool g_bSampleMusicWaiting = false;
static RageTimer g_StartedLoadingAt(RageZeroTimer);

REGISTER_SCREEN_CLASS( ScreenSelectMusic );
ScreenSelectMusic::ScreenSelectMusic()
{
	if( PREFSMAN->m_bScreenTestMode )
	{
		GAMESTATE->m_PlayMode.Set( PLAY_MODE_REGULAR );
		GAMESTATE->SetCurrentStyle( GAMEMAN->GameAndStringToStyle(GAMEMAN->GetDefaultGame(),"versus") );
		GAMESTATE->JoinPlayer( PLAYER_1 );
		GAMESTATE->m_MasterPlayerNumber = PLAYER_1;
	}
}


void ScreenSelectMusic::Init()
{

	//MODIFICADO POR MI
	m_bSelected = false;

	//modificado por mi, modemission, reestablece las opciones a default
	if( GAMESTATE->IsWorldTourMode() )
		FOREACH_PlayerNumber ( p )
		{
			if( GAMESTATE->IsPlayerEnabled( p ) )
				GAMESTATE->m_pPlayerState[p]->ResetToDefaultPlayerOptions( ModsLevel_Preferred );
		}

	SAMPLE_MUSIC_DELAY.Load( m_sName, "SampleMusicDelay" );
	SAMPLE_MUSIC_LOOPS.Load( m_sName, "SampleMusicLoops" );
	SAMPLE_MUSIC_FALLBACK_FADE_IN_SECONDS.Load( m_sName, "SampleMusicFallbackFadeInSeconds" );
	DO_ROULETTE_ON_MENU_TIMER.Load( m_sName, "DoRouletteOnMenuTimer" );
	ALIGN_MUSIC_BEATS.Load( m_sName, "AlignMusicBeat" );
	CODES.Load( m_sName, "Codes" );
	MUSIC_WHEEL_TYPE.Load( m_sName, "MusicWheelType" );
	SELECT_MENU_AVAILABLE.Load( m_sName, "SelectMenuAvailable" );
	MODE_MENU_AVAILABLE.Load( m_sName, "ModeMenuAvailable" );
	USE_OPTIONS_LIST.Load( m_sName, "UseOptionsList" );
	TWO_PART_SELECTION.Load( m_sName, "TwoPartSelection" );
	LOCK_INPUT_SECONDS_WHEN_LATE_JOIN.Load( m_sName, "LockInputSecondsWhenLateJoin" );
	BROADCAST_GROUP_CHANGE.Load( m_sName, "AllowPlaySoundWhenGroupChange" );
	ALLOW_ENTER_CODES.Load( m_sName, "AllowEnterCodes" );

	m_GameButtonPreviousSong = INPUTMAPPER->GetInputScheme()->ButtonNameToIndex( THEME->GetMetric(m_sName,"PreviousSongButton") );
	m_GameButtonNextSong = INPUTMAPPER->GetInputScheme()->ButtonNameToIndex( THEME->GetMetric(m_sName,"NextSongButton") );

	FOREACH_ENUM( PlayerNumber, p )
	{
		m_bSelectIsDown[p] = false; // used by UpdateSelectButton
		m_bAcceptSelectRelease[p] = false;
	}

	ScreenWithMenuElements::Init();

	/* Cache: */
	m_sSectionMusicPath =		THEME->GetPathS(m_sName,"section music");
	m_sSortMusicPath =		THEME->GetPathS(m_sName,"sort music");
	m_sRouletteMusicPath =		THEME->GetPathS(m_sName,"roulette music");
	m_sRandomMusicPath =		THEME->GetPathS(m_sName,"random music");
	m_sCourseMusicPath =		THEME->GetPathS(m_sName,"course music");
	m_sLoopMusicPath =		THEME->GetPathS(m_sName,"loop music");
	m_sFallbackCDTitlePath =	THEME->GetPathG(m_sName,"fallback cdtitle");


	m_TexturePreload.Load( m_sFallbackCDTitlePath );

	if( PREFSMAN->m_BannerCache != BNCACHE_OFF )
	{
		m_TexturePreload.Load( Banner::SongBannerTexture(THEME->GetPathG("Banner","all music")) );
		m_TexturePreload.Load( Banner::SongBannerTexture(THEME->GetPathG("Common","fallback banner")) );
		m_TexturePreload.Load( Banner::SongBannerTexture(THEME->GetPathG("Banner","roulette")) );
		m_TexturePreload.Load( Banner::SongBannerTexture(THEME->GetPathG("Banner","random")) );
		m_TexturePreload.Load( Banner::SongBannerTexture(THEME->GetPathG("Banner","mode")) );
	}

	this->SubscribeToMessage( "ExitOptionsList" );
	this->SubscribeToMessage( "MoveGroup" );

	/* Load low-res banners, if needed. */
	BANNERCACHE->Demand();

	m_MusicWheel.SetName( "MusicWheel" );
	m_MusicWheel.Load( MUSIC_WHEEL_TYPE );
	LOAD_ALL_COMMANDS_AND_SET_XY( m_MusicWheel );
	this->AddChild( &m_MusicWheel );

	if( USE_OPTIONS_LIST )
	{
		FOREACH_PlayerNumber(p)
		{
			m_OptionsList[p].SetName( "OptionsList" + PlayerNumberToString(p) );
			m_OptionsList[p].Load( "OptionsList", p );
			m_OptionsList[p].SetDrawOrder( 100 );
			ActorUtil::LoadAllCommands( m_OptionsList[p], m_sName );
			this->AddChild( &m_OptionsList[p] );
		}
		m_OptionsList[PLAYER_1].Link( &m_OptionsList[PLAYER_2] );
		m_OptionsList[PLAYER_2].Link( &m_OptionsList[PLAYER_1] );
	}

	// this is loaded SetSong and TweenToSong
	m_Banner.SetName( "Banner" );
	LOAD_ALL_COMMANDS_AND_SET_XY( m_Banner );
	this->AddChild( &m_Banner );

	m_sprCDTitleFront.SetName( "CDTitle" );
	m_sprCDTitleFront.Load( THEME->GetPathG(m_sName,"fallback cdtitle") );
	LOAD_ALL_COMMANDS_AND_SET_XY( m_sprCDTitleFront );
	COMMAND( m_sprCDTitleFront, "Front" );
	this->AddChild( &m_sprCDTitleFront );

	m_sprCDTitleBack.SetName( "CDTitle" );
	m_sprCDTitleBack.Load( THEME->GetPathG(m_sName,"fallback cdtitle") );
	LOAD_ALL_COMMANDS_AND_SET_XY( m_sprCDTitleBack );
	COMMAND( m_sprCDTitleBack, "Back" );
	this->AddChild( &m_sprCDTitleBack );

	//ARROW STYLE NX!
	if( PREFSMAN->m_bUseNXStyle )
	{
		m_soundChangeGroup.Load( THEME->GetPathS( m_sName, "changegroup" ) );
		m_soundMissionLocked.Load( THEME->GetPathS( m_sName, "missionlocked" ) );

		m_soundUnlock.Load( THEME->GetPathS( m_sName, "unlock" ) );
	}

	FOREACH_ENUM( PlayerNumber, p )
	{
		m_sprHighScoreFrame[p].SetName( ssprintf("ScoreFrameP%d",p+1) );
		m_sprHighScoreFrame[p].Load( THEME->GetPathG(m_sName,ssprintf("score frame p%d",p+1)) );
		LOAD_ALL_COMMANDS_AND_SET_XY( m_sprHighScoreFrame[p] );
		this->AddChild( &m_sprHighScoreFrame[p] );

		//m_textKcalVO2[p].SetName( ssprintf("KCalVO2NumbersP%d",p+1) );
		//m_textKcalVO2[p].LoadFromFont( THEME->GetPathF(m_sName,"score") );
		//m_textKcalVO2[p].SetShadowLength( 0 );
		//LOAD_ALL_COMMANDS_AND_SET_XY( m_textKcalVO2[p] );
		//this->AddChild( &m_textKcalVO2[p] );
	}	

	//m_textHighScoreName.SetName( ssprintf("ScoreName") );
	//m_textHighScoreName.LoadFromFont( THEME->GetPathF(m_sName,"score") );
	//m_textHighScoreName.SetShadowLength( 0 );
	////m_textHighScore[p].RunCommands( CommonMetrics::PLAYER_COLOR.GetValue(p) );
	//LOAD_ALL_COMMANDS_AND_SET_XY( m_textHighScoreName );
	//this->AddChild( &m_textHighScoreName );

	//m_textHighScore.SetName( ssprintf("Score") );
	//m_textHighScore.LoadFromFont( THEME->GetPathF(m_sName,"score") );
	//m_textHighScore.SetShadowLength( 0 );
	////m_textHighScore[p].RunCommands( CommonMetrics::PLAYER_COLOR.GetValue(p) );
	//LOAD_ALL_COMMANDS_AND_SET_XY( m_textHighScore );
	//this->AddChild( &m_textHighScore );

	RageSoundLoadParams SoundParams;
	SoundParams.m_bSupportPan = true;

	m_soundStart.Load( THEME->GetPathS(m_sName,"start") );
	m_soundDifficultyEasier.Load( THEME->GetPathS(m_sName,"difficulty easier"), false, &SoundParams );
	m_soundDifficultyHarder.Load( THEME->GetPathS(m_sName,"difficulty harder"), false, &SoundParams );
	m_soundOptionsChange.Load( THEME->GetPathS(m_sName,"options") );
	m_soundLocked.Load( THEME->GetPathS(m_sName,"locked") );

	this->SortByDrawOrder();
}

void ScreenSelectMusic::BeginScreen()
{
	if( CommonMetrics::AUTO_SET_STYLE )
	{
		vector<StepsType> vst;
		GAMEMAN->GetStepsTypesForGame( GAMESTATE->m_pCurGame, vst );
		const Style *pStyle = GAMEMAN->GetFirstCompatibleStyle( GAMESTATE->m_pCurGame, GAMESTATE->GetNumSidesJoined(), vst[0] );
		GAMESTATE->SetCurrentStyle( pStyle );
	}

	if( GAMESTATE->GetCurrentStyle() == NULL )
		RageException::Throw( "The Style has not been set.  A theme must set the Style before loading ScreenSelectMusic." );

	if( GAMESTATE->m_PlayMode == PlayMode_Invalid )
		RageException::Throw( "The PlayMode has not been set.  A theme must set the PlayMode before loading ScreenSelectMusic." );

	FOREACH_ENUM( PlayerNumber, pn )
	{
		if( GAMESTATE->IsHumanPlayer(pn) )
			continue;
		m_sprHighScoreFrame[pn].SetVisible( false );
	}
	
	OPTIONS_MENU_AVAILABLE.Load( m_sName, "OptionsMenuAvailable" );
	PlayCommand( "Mods" );
	m_MusicWheel.BeginScreen();
	
	m_SelectionState = SelectionState_SelectingSong;
	ZERO( m_bStepsChosen );
	m_bGoToOptions = false;
	m_bAllowOptionsMenu = m_bAllowOptionsMenuRepeat = false;
	ZERO( m_iSelection );

	if( USE_OPTIONS_LIST )
		FOREACH_PlayerNumber( pn )
			m_OptionsList[pn].Reset();

	AfterMusicChange();

	SOUND->PlayOnceFromAnnouncer( "select music intro" );

	if( GAMESTATE->m_pCurSong != NULL )
		m_sLastGroup = GAMESTATE->m_pCurSong->m_sGroupName;

	ScreenWithMenuElements::BeginScreen();
}

ScreenSelectMusic::~ScreenSelectMusic()
{
	LOG->Trace( "ScreenSelectMusic::~ScreenSelectMusic()" );
	BANNERCACHE->Undemand();
}

/* If bForce is true, the next request will be started even if it might cause a skip. */
void ScreenSelectMusic::CheckBackgroundRequests( bool bForce )
{
	/* Loading the rest can cause small skips, so don't do it until the wheel settles. 
	 * Do load if we're transitioning out, though, so we don't miss starting the music
	 * for the options screen if a song is selected quickly.  Also, don't do this
	 * if the wheel is locked, since we're just bouncing around after selecting TYPE_RANDOM,
	 * and it'll take a while before the wheel will settle. */
	if( !m_MusicWheel.IsSettled() && !m_MusicWheel.WheelIsLocked() && !bForce )
		return;

	//TODO: rellenar esto:

	if( g_bCDTitleWaiting )
	{
		/* The CDTitle is normally very small, so we don't bother waiting to display it. */
		RString sPath;
		if( !m_BackgroundLoader.IsCacheFileFinished(g_sCDTitlePath, sPath) )
			return;

		g_bCDTitleWaiting = false;

		RString sCDTitlePath = sPath;

		if( sCDTitlePath.empty() || !IsAFile(sCDTitlePath) ) //si no hay cdtitle obligamos al banner a serlo
			sCDTitlePath = g_sBannerPath;
			//sCDTitlePath = g_bWantFallbackCdTitle? m_sFallbackCDTitlePath:RString("");

		if( !sCDTitlePath.empty() )
		{
			TEXTUREMAN->DisableOddDimensionWarning();
			m_sprCDTitleFront.Load( sCDTitlePath );
			//m_sprCDTitleBack.Load( sCDTitlePath );
			TEXTUREMAN->EnableOddDimensionWarning();
			
			//si estan bloqueadas, no mostrar el banner frame
			if( !GAMESTATE->m_pCurSong->m_bIsSpecialSongLocked && 
				!GAMESTATE->IsEasyMode() && 
				!GAMESTATE->m_pCurSong->m_bMissionWorldTourLocked )
			{
				m_sprCDTitleFront.SetVisible( true );
				//m_sprCDTitleBack.SetVisible( true );

				///*FOREACH_EnabledPlayer( pn )
				//	m_textKcalVO2[pn].SetVisible( true );*/
				////asigna el color que cargamos del grupo!
				//if( !GAMESTATE->IsEasyMode() )
				//{
				//	if( GAMESTATE->m_pCurSong->m_cGroupColor == RageColor( 1.0f, 1.0f, 1.0f, 1.0f ) )
				//	{
				//		//m_BannerFrame.SetDiffuse( SONGMAN->GetSongColor( m_MusicWheel.GetSelectedSong() ) );
				//		//m_BannerGlow.SetDiffuse( SONGMAN->GetSongColor( m_MusicWheel.GetSelectedSong() ) );
				//	}
				//	else
				//	{
				//		//m_BannerFrame.SetDiffuse( GAMESTATE->m_pCurSong->m_cGroupColor );
				//		//m_BannerGlow.SetDiffuse( GAMESTATE->m_pCurSong->m_cGroupColor );
				//	}
				//}
			}
			else if( GAMESTATE->IsEasyMode() )
			{
				//m_BannerFrame.SetVisible( false );//banner frame, modificado por mi
				//m_BannerGlow.SetVisible( false );
				//m_ScoreFrame.SetVisible( false );
				/*m_textHighScore.SetVisible( false );
				m_textHighScoreName.SetVisible( false );*/
				m_sprCDTitleFront.SetVisible( false );
				m_sprCDTitleBack.SetVisible( false );

				/*FOREACH_EnabledPlayer( pn )
					m_textKcalVO2[pn].SetVisible( false );*/
			}
			else
			{
				//m_BannerFrame.SetVisible( false );//banner frame, modificado por mi
				//m_BannerGlow.SetVisible( false );
				//m_ScoreFrame.SetVisible( false );
				/*m_textHighScore.SetVisible( false );
				m_textHighScoreName.SetVisible( false );*/
				m_sprCDTitleFront.SetVisible( false );
				m_sprCDTitleBack.SetVisible( false );

				/*FOREACH_EnabledPlayer( pn )
					m_textKcalVO2[pn].SetVisible( false );*/
			}
		}

		m_BackgroundLoader.FinishedWithCachedFile( g_sCDTitlePath );
	}

	///* Loading the rest can cause small skips, so don't do it until the wheel settles. 
	// * Do load if we're transitioning out, though, so we don't miss starting the music
	// * for the options screen if a song is selected quickly.  Also, don't do this
	// * if the wheel is locked, since we're just bouncing around after selecting TYPE_RANDOM,
	// * and it'll take a while before the wheel will settle. */
	//if( !m_MusicWheel.IsSettled() && !m_MusicWheel.WheelIsLocked() && !bForce )
	//	return;

	//if( g_bBannerWaiting )
	//{
	//	if( m_Banner.GetTweenTimeLeft() > 0 )
	//		return;

	//	RString sPath;
	//	bool bFreeCache = false;
	//	if( TEXTUREMAN->IsTextureRegistered( Sprite::SongBannerTexture(g_sBannerPath) ) )
	//	{
	//		/* If the file is already loaded into a texture, it's finished,
	//		 * and we only do this to honor the HighQualTime value. */
	//		sPath = g_sBannerPath;
	//	}
	//	else
	//	{
	//		if( !m_BackgroundLoader.IsCacheFileFinished( g_sBannerPath, sPath ) )
	//			return;
	//		bFreeCache = true;
	//	}

	//	g_bBannerWaiting = false;
	//	m_Banner.Load( sPath, true );

	//	if( bFreeCache )
	//		m_BackgroundLoader.FinishedWithCachedFile( g_sBannerPath );
	//}

	/* modificado por mi, si no es modemission, toca la cancion :) */
	if( GAMESTATE->m_pCurSong->m_bIsSpecialSongLocked  || GAMESTATE->m_pCurSong->m_bMissionWorldTourLocked )
		return;

	/* Nothing else is going.  Start the music, if we havn't yet. */
	if( g_bSampleMusicWaiting )
	{
		/* Don't start the music sample when moving fast. */
		if( g_StartedLoadingAt.Ago() < SAMPLE_MUSIC_DELAY && !bForce )
			return;

		g_bSampleMusicWaiting = false; 

		GameSoundManager::PlayMusicParams PlayParams; 
		PlayParams.sFile = m_sSampleMusicToPlay;
		PlayParams.pTiming = m_pSampleMusicTimingData;
		PlayParams.bForceLoop = SAMPLE_MUSIC_LOOPS;
		PlayParams.fStartSecond = m_fSampleStartSeconds;
		PlayParams.fLengthSeconds = m_fSampleLengthSeconds;
		PlayParams.fFadeOutLengthSeconds = 1.5f;
		PlayParams.bAlignBeat = ALIGN_MUSIC_BEATS;
		PlayParams.bApplyMusicRate = true;

		GameSoundManager::PlayMusicParams FallbackMusic;
		FallbackMusic.sFile = m_sLoopMusicPath;
		FallbackMusic.fFadeInLengthSeconds = SAMPLE_MUSIC_FALLBACK_FADE_IN_SECONDS;
		FallbackMusic.bAlignBeat = ALIGN_MUSIC_BEATS;

		SOUND->PlayMusic( PlayParams, FallbackMusic );
	}
}

void ScreenSelectMusic::Update( float fDeltaTime )
{
	ScreenWithMenuElements::Update( fDeltaTime );

	CheckBackgroundRequests( false );
}

void ScreenSelectMusic::Input( const InputEventPlus &input )
{
//	LOG->Trace( "ScreenSelectMusic::Input()" );+

	// debugging?
	// I just like being able to see untransliterated titles occasionally.
	if( input.DeviceI.device == DEVICE_KEYBOARD && input.DeviceI.button == KEY_F9 )
	{
		if( input.type != IET_FIRST_PRESS ) 
			return;
		PREFSMAN->m_bShowNativeLanguage.Set( !PREFSMAN->m_bShowNativeLanguage );
		m_MusicWheel.RebuildWheelItems();
		return;
	}

	if( !input.GameI.IsValid() )
		return;		// don't care

	// Handle late joining
	//modificado por mi, missionmode
	//si es mission se juega de 1, esto se handlea en gamestate! no aqui
	if( m_SelectionState != SelectionState_Finalized  &&  input.MenuI == MENU_BUTTON_START  &&  input.type == IET_FIRST_PRESS  &&  GAMESTATE->JoinInput(input.pn) )
	{
		// The current steps may no longer be playable.  If one player has double steps 
		// selected, they are no longer playable now that P2 has joined.  
		
		// TODO: Invalidate the CurSteps only if they are no longer playable.  That way, 
		// after music change will clamp to the nearest in the DifficultyList.
		GAMESTATE->m_pCurSteps[GAMESTATE->m_MasterPlayerNumber].SetWithoutBroadcast( NULL );
		FOREACH_ENUM( PlayerNumber, p )
			GAMESTATE->m_pCurSteps[p].SetWithoutBroadcast( NULL );

		/* If a course is selected, it may no longer be playable.  Let MusicWheel know
		 * about the late join. */
		m_MusicWheel.PlayerJoined();

		AfterMusicChange();

		int iSel = 0;
		PlayerNumber pn = input.pn;
		m_iSelection[pn] = iSel;
		if( GAMESTATE->IsCourseMode() )
		{
			Trail* pTrail = m_vpTrails.empty()? NULL: m_vpTrails[m_iSelection[pn]];
			GAMESTATE->m_pCurTrail[pn].Set( pTrail );
		}
		else
		{
			Steps* pSteps = m_vpSteps.empty()? NULL: m_vpSteps[m_iSelection[pn]];
			GAMESTATE->m_pCurSteps[pn].Set( pSteps );
		}

		//hacer metricable
		//this->PlayCommand( "lockinput,1.5" );
		this->PostScreenMessage( SM_PlayerJoinLate, LOCK_INPUT_SECONDS_WHEN_LATE_JOIN );
		SOUND->StopMusic();
		MESSAGEMAN->Broadcast( "PlayerJoined" );
		return;	// don't handle this press again below
	}

	if( !GAMESTATE->IsHumanPlayer(input.pn) )
		return;

	if( m_SelectionState == SelectionState_SelectingGroups && !m_OptionsList[input.pn].IsOpened() )
	{
		if( input.type != IET_FIRST_PRESS ) 
			return;

		switch( input.MenuI )
		{
			case GAME_BUTTON_MENULEFT:
			{
				Message msg("ChangeGroup");
				msg.SetParam( "Dir", -1 );
				MESSAGEMAN->Broadcast( msg );	
				MESSAGEMAN->Broadcast("PrevGroup");
			}
			break;
			case GAME_BUTTON_MENURIGHT:
			{
				Message msg("ChangeGroup");
				msg.SetParam( "Dir", +1 );
				MESSAGEMAN->Broadcast( msg );
				MESSAGEMAN->Broadcast("NextGroup");
			}
			break;
			default: break;
		}
	}

	// Check for "Press START again for options" button press
	if( m_SelectionState == SelectionState_Finalized  &&
	    input.MenuI == MENU_BUTTON_START  &&
	    input.type != IET_RELEASE  &&
	    OPTIONS_MENU_AVAILABLE.GetValue() )
	{
		if( m_bGoToOptions )
			return; /* got it already */
		if( !m_bAllowOptionsMenu )
			return; /* not allowed */

		if( !m_bAllowOptionsMenuRepeat && input.type == IET_REPEAT )
		{
			return; /* not allowed yet */
		}
		
		m_bGoToOptions = true;
		m_soundStart.Play();
		this->PlayCommand( "ShowEnteringOptions" );

		// Re-queue SM_BeginFadingOut, since ShowEnteringOptions may have
		// short-circuited animations.
		this->ClearMessageQueue( SM_BeginFadingOut );
		this->PostScreenMessage( SM_BeginFadingOut, this->GetTweenTimeLeft() );

		return;
	}

	if( IsTransitioning() )
		return;		// ignore

	// Handle unselect steps
	// xxx: select button could conflict with OptionsList here -aj
	if( m_SelectionState == SelectionState_SelectingSteps && m_bStepsChosen[input.pn]
		&& input.MenuI == GAME_BUTTON_SELECT && input.type == IET_FIRST_PRESS )
	{
		Message msg("StepsUnchosen");
		msg.SetParam( "Player", input.pn );
		MESSAGEMAN->Broadcast( msg );
		m_bStepsChosen[input.pn] = false;
		return;
	}

	if( m_SelectionState == SelectionState_Finalized  ||
		m_bStepsChosen[input.pn] )
		return;		// ignore


	// handle OptionsList input
	bool bWasOpened = false;
	if( USE_OPTIONS_LIST )
	{
		PlayerNumber pn = input.pn;
		// if( pn != PLAYER_INVALID )
		// {
		// 	if( m_OptionsList[pn].IsOpened() )
		// 	{
		// 		return m_OptionsList[pn].Input( input );
		// 	}
		// 	else
		// 	{
		// 		if( input.type == IET_RELEASE  &&  input.MenuI == GAME_BUTTON_SELECT && m_bAcceptSelectRelease[pn] )
		// 			m_OptionsList[pn].Open();
		// 	}
		// }
		if( m_OptionsList[pn].IsOpened() )
		{
			bWasOpened = true;
			if( input.MenuI == GAME_BUTTON_MENULEFT )
			{
				if( input.type == IET_RELEASE )
					return;	

				// if( INPUTMAPPER->IsBeingPressed(GAME_BUTTON_RIGHT, pn) )
				// {
				// 	if( input.type == IET_FIRST_PRESS )
				// 		SwitchMenu( -1 );
				// 	return true;
				// }

				// --m_iMenuStackSelection;
				// wrap( m_iMenuStackSelection, pHandler->m_Def.m_vsChoices.size()+1 ); // +1 for exit row
				// PositionCursor();

				Message lMsg("OptionsListPrev");
				lMsg.SetParam( "Player", input.pn );
				MESSAGEMAN->Broadcast( lMsg );
				return;
			}
			else if( input.MenuI == GAME_BUTTON_MENURIGHT )
			{
				if( input.type == IET_RELEASE )
					return;	
				// if( INPUTMAPPER->IsBeingPressed(GAME_BUTTON_LEFT, pn) )
				// {
				// 	if( input.type == IET_FIRST_PRESS )
				// 		SwitchMenu( +1 );
				// 	return true;
				// }

				// ++m_iMenuStackSelection;
				// wrap( m_iMenuStackSelection, pHandler->m_Def.m_vsChoices.size()+1 ); // +1 for exit row
				// PositionCursor();

				Message lMsg("OptionsListNext");
				lMsg.SetParam( "Player", input.pn );
				MESSAGEMAN->Broadcast( lMsg );
				return;
			}
			else if( input.MenuI == GAME_BUTTON_START )
			{
				if( input.type == IET_FIRST_PRESS )
				{
					//m_bStartIsDown = true;
					//m_bAcceptStartRelease = true;

					Message lMsg("OptionsListCenter");
					lMsg.SetParam( "Player", input.pn );
					MESSAGEMAN->Broadcast( lMsg );
					
					return;
				}
				// if( input.type == IET_RELEASE )
				// {
				// 	if( m_bAcceptStartRelease )
				// 		Start();
				// 	m_bStartIsDown = false;
				// }

				return;
			}
			else if( input.MenuI == GAME_BUTTON_MENUUP || input.MenuI == GAME_BUTTON_MENUDOWN )
			{
				if( input.type == IET_RELEASE )
					return;	

				Message lMsg("OptionsListBack");
				lMsg.SetParam( "Player", input.pn );
				MESSAGEMAN->Broadcast( lMsg );

				//m_OptionsList[input.pn].Close();

				return;
			}
		}
	}

	if( input.MenuI == GAME_BUTTON_SELECT && input.type != IET_REPEAT )
		m_bAcceptSelectRelease[input.pn] = (input.type == IET_FIRST_PRESS);

	if( SELECT_MENU_AVAILABLE && input.MenuI == GAME_BUTTON_SELECT && input.type != IET_REPEAT )
		UpdateSelectButton( input.pn, input.type == IET_FIRST_PRESS );


	if( SELECT_MENU_AVAILABLE  &&  m_bSelectIsDown[input.pn] )
	{
		if( input.type == IET_FIRST_PRESS )
		{
			switch( input.MenuI )
			{
			case MENU_BUTTON_LEFT:
				ChangeDifficulty( input.pn, -1 );
				m_bAcceptSelectRelease[input.pn] = false;
				break;
			case MENU_BUTTON_RIGHT:
				ChangeDifficulty( input.pn, +1 );
				m_bAcceptSelectRelease[input.pn] = false;
				break;
			case MENU_BUTTON_START:
				m_bAcceptSelectRelease[input.pn] = false;
				if( MODE_MENU_AVAILABLE )
					m_MusicWheel.NextSort();
				else
					m_soundLocked.Play();
				break;
			}
		}
//		return;
	}

	if( m_SelectionState == SelectionState_SelectingSong  &&
		(input.MenuI == m_GameButtonNextSong || input.MenuI == m_GameButtonPreviousSong || input.MenuI == GAME_BUTTON_SELECT) )
	{
		//MODIFICADO POR MI
		if(m_bSelected && PREFSMAN->m_bUseNXStyle)
		{			
			m_bSelected = false;
		}

		{
			/* If we're rouletting, hands off. */
			if( m_MusicWheel.IsRouletting() )
				return;
			
			bool bLeftIsDown = false;
			bool bRightIsDown = false;
			FOREACH_HumanPlayer( p )
			{
				if( m_OptionsList[p].IsOpened() )
					continue;
				if( SELECT_MENU_AVAILABLE && INPUTMAPPER->IsBeingPressed(GAME_BUTTON_SELECT, p) )
					continue;

				bLeftIsDown |= INPUTMAPPER->IsBeingPressed( m_GameButtonPreviousSong, p );
				bRightIsDown |= INPUTMAPPER->IsBeingPressed( m_GameButtonNextSong, p );

				if( INPUTMAPPER->IsBeingPressed( m_GameButtonNextSong, p ) &&
					INPUTMAPPER->GetSecsHeld( m_GameButtonNextSong, p ) > 2.0f )
				{
					m_MusicWheel.Accelerate();
				} 
				else if( INPUTMAPPER->IsBeingPressed( m_GameButtonPreviousSong, p ) &&
					INPUTMAPPER->GetSecsHeld( m_GameButtonPreviousSong, p ) > 2.0f )
				{
					m_MusicWheel.Accelerate();
				}				

			}
			
			bool bBothDown = bLeftIsDown && bRightIsDown;
			bool bNeitherDown = !bLeftIsDown && !bRightIsDown;			


			if( bNeitherDown )
			{
				/* Both buttons released. */
				m_MusicWheel.Move( 0 );				
				m_MusicWheel.Deccelerate();
			}
			else if( bBothDown )
			{
				m_MusicWheel.Move( 0 );

				if( input.type == IET_FIRST_PRESS )
				{
					if( input.MenuI == m_GameButtonPreviousSong )
						m_MusicWheel.ChangeMusicUnlessLocked( -1 );
					else if( input.MenuI == m_GameButtonNextSong )
						m_MusicWheel.ChangeMusicUnlessLocked( +1 );
				}
			}
			else if( bLeftIsDown )
			{
				if( input.type != IET_RELEASE )
				{
					m_MusicWheel.Move( -1 );
					MESSAGEMAN->Broadcast( "DownLeftPressed" );
				}	
			}
			else if( bRightIsDown )
			{
				if( input.type != IET_RELEASE )
				{
					m_MusicWheel.Move( +1 );
					MESSAGEMAN->Broadcast( "DownRightPressed" );
				}		
			}
			else
			{
				ASSERT(0);
			}			
			
			// Reset the repeat timer when the button is released.
			// This fixes jumping when you release Left and Right after entering the sort 
			// code at the same if L & R aren't released at the exact same time.
			if( input.type == IET_RELEASE )
			{
				INPUTMAPPER->ResetKeyRepeat( m_GameButtonPreviousSong, input.pn );
				INPUTMAPPER->ResetKeyRepeat( m_GameButtonNextSong, input.pn );
			}
		}
	}

	if( m_SelectionState == SelectionState_SelectingSteps  &&
		input.type == IET_FIRST_PRESS  &&
		(input.MenuI == m_GameButtonNextSong || input.MenuI == m_GameButtonPreviousSong) )
	{
		if( input.MenuI == m_GameButtonPreviousSong )
		{
			ChangeDifficulty( input.pn, -1 );
				
		}
		else if( input.MenuI == m_GameButtonNextSong )
		{
			ChangeDifficulty( input.pn, +1 );
		}		
	}

	if( input.type == IET_FIRST_PRESS && DetectCodes(input) )
		return;

	ScreenWithMenuElements::Input( input );
}

bool ScreenSelectMusic::DetectCodes( const InputEventPlus &input )
{
	if( PREFSMAN->m_bUseNXStyle )
	{//cambia la dificultad solo  presionando una vez y sin code_detector
		if( input.GameI.button == PUMP_BUTTON_UPLEFT )
		{
			if( !GAMESTATE->IsWorldTourMode() )
				ChangeDifficulty( input.pn, -1 );

			if( GAMESTATE->IsBrainMode() )//solo funciona para el brain shower, el cambio automatico de grupos
			{
				vector<RString> vsBrainNames;
				vector<Song*> vpSongs;
				split( CommonMetrics::BRAINSHOWER_GROUP_NAMES, ",", vsBrainNames );

				RString sGroup = GAMESTATE->m_pCurSong->m_sGroupName;
				int iIndex = vsBrainNames.size()-1;

				for( unsigned i = 0; i < vsBrainNames.size(); i ++ )
				{
					if( !SONGMAN->DoesSongGroupExist( vsBrainNames[i] ) )
						vsBrainNames.erase( vsBrainNames.begin()+i );
				}

				for( unsigned i=vsBrainNames.size()-1; i>=0; i-- )
				{
					if( vsBrainNames[i] == sGroup )
					{
						iIndex = i;
						break;
					}
				}

				if( iIndex == 0 )
					iIndex = max( (int)vsBrainNames.size()-1, iIndex );
				else
					iIndex--;

				vpSongs = SONGMAN->GetSongs( vsBrainNames[iIndex] );
				GAMESTATE->m_pCurSong.Set( vpSongs[vpSongs.size()-1] );
				m_MusicWheel.SelectSong( GAMESTATE->m_pCurSong );
				AfterMusicChange();
				m_MusicWheel.RebuildWheelItems();
			}

			MESSAGEMAN->Broadcast( "UpLeftPressed" );
			MESSAGEMAN->Broadcast( "StyleChanged" );
			MESSAGEMAN->Broadcast( ssprintf( "DifficultyP%dChanged", input.pn+1 ) );
		}
		else if( input.GameI.button == PUMP_BUTTON_UPRIGHT )
		{
			if( !GAMESTATE->IsWorldTourMode() )
				ChangeDifficulty( input.pn, +1 );

			if( GAMESTATE->IsBrainMode() )//solo funciona para el brain shower, el cambio automatico de grupos
			{
				vector<RString> vsBrainNames;
				vector<Song*> vpSongs;
				split( CommonMetrics::BRAINSHOWER_GROUP_NAMES, ",", vsBrainNames );

				RString sGroup = GAMESTATE->m_pCurSong->m_sGroupName;
				int iIndex = 0;

				for( unsigned i = 0; i < vsBrainNames.size(); i ++ )
				{
					if( !SONGMAN->DoesSongGroupExist( vsBrainNames[i] ) )
						vsBrainNames.erase( vsBrainNames.begin()+i );
				}

				for( unsigned i=0; i<vsBrainNames.size(); i++ )
				{
					if( vsBrainNames[i] == sGroup )
					{
						iIndex = i;
						break;
					}
				}

				if( iIndex == (int)vsBrainNames.size()-1 )
					iIndex = min( 0, iIndex );
				else
					iIndex++;

				vpSongs = SONGMAN->GetSongs( vsBrainNames[iIndex] );
				GAMESTATE->m_pCurSong.Set( vpSongs[0] );
				m_MusicWheel.SelectSong( GAMESTATE->m_pCurSong );
				AfterMusicChange();
				m_MusicWheel.RebuildWheelItems();
			}

			
			MESSAGEMAN->Broadcast( "UpRightPressed" );
			MESSAGEMAN->Broadcast( "StyleChanged" );
			MESSAGEMAN->Broadcast( ssprintf( "DifficultyP%dChanged", input.pn+1 ) );
		}
	}

	if( !ALLOW_ENTER_CODES )//no permite codes.
		return false;

	if( CodeDetector::EnteredEasierDifficulty(input.GameI.controller) )
	{
		ChangeDifficulty( input.pn, -1 );
	}
	else if( CodeDetector::EnteredHarderDifficulty(input.GameI.controller) )
	{
		ChangeDifficulty( input.pn, +1 );
	}
	else if( CodeDetector::EnteredModeMenu(input.GameI.controller) )
	{
		if( MODE_MENU_AVAILABLE )
			m_MusicWheel.ChangeSort( SORT_MODE_MENU );
		else
			m_soundLocked.Play();
	}
	else if( CodeDetector::EnteredNextBannerGroup(input.GameI.controller) )
	{
		static SelectionState sst = m_SelectionState;
		if( m_SelectionState == SelectionState_SelectingSong && sst == SelectionState_SelectingSong )
		{		
			//if( input.type == IET_RELEASE )
			//	return;

			//switch( input.MenuI )
			//{
			//	case GAME_BUTTON_MENUUP:
			//	case GAME_BUTTON_MENUDOWN:
			//	{
					//SCREENMAN->SystemMessage("GoBackSelectingGroup y de aqui no pasamos por que m_SelectionState == SelectionState_SelectingSong");
					// m_MusicWheel.Move(-1);
					m_SelectionState = SelectionState_SelectingGroups;
					MESSAGEMAN->Broadcast( "GoBackSelectingGroup" );
					MESSAGEMAN->Broadcast( "ChangeGroup" );
					SOUND->StopMusic();
			//	}			
			//	break;
			//	default: break;
			//}
		}	
		sst = m_SelectionState;
	}
	else if( CodeDetector::EnteredNextSort(input.GameI.controller) )
	{
		//m_MusicWheel.NextSort();
		this->OpenOptionsList(input.pn);
		m_MusicWheel.Move(0);
	}
	//else if( !GAMESTATE->IsAnExtraStage() && CodeDetector::DetectAndAdjustMusicOptions(input.GameI.controller) )
	//MODIFICADO POR MI
	//PERMITE ENTRAR CODIGOS EN EXTRA STAGE, pero no en mission mode!
	else if( !GAMESTATE->IsWorldTourMode() && CodeDetector::DetectAndAdjustMusicOptions(input.GameI.controller) )
	{
		m_soundOptionsChange.Play();
		MESSAGEMAN->Broadcast( ssprintf("PlayerOptionsChangedP%i", input.pn+1) );
		MESSAGEMAN->Broadcast( "SongOptionsChanged" );
	}
	else
	{
		return false;
	}
	return true;
}	

void ScreenSelectMusic::UpdateSelectButton( PlayerNumber pn, bool bSelectIsDown )
{
	if( !SELECT_MENU_AVAILABLE  ||  !CanChangeSong() )
		bSelectIsDown = false;

	if( m_bSelectIsDown[pn] != bSelectIsDown )
	{
		m_bSelectIsDown[pn] = bSelectIsDown;
		Message msg( bSelectIsDown ? "SelectMenuOpened" : "SelectMenuClosed" );
		msg.SetParam( "Player", pn );
		MESSAGEMAN->Broadcast( msg );
	}
}

void ScreenSelectMusic::ChangeDifficulty( PlayerNumber pn, int dir )
{
	LOG->Trace( "ScreenSelectMusic::ChangeDifficulty( %d, %d )", pn, dir );

	ASSERT( GAMESTATE->IsHumanPlayer(pn) );

	if( GAMESTATE->m_pCurSong )
	{
		m_iSelection[pn] += dir;

		//MODIFICADO POR MI---
		//sirve solo para cambiar la difficultad de NM a EZ
		if( m_iSelection[pn] > (int)(m_vpSteps.size()-1) )
		{
			wrap( m_iSelection[pn], 1 );
		}	
		if( m_iSelection[pn] < 0 )
		{
			if( (int)m_vpSteps.size() != 0 )
				wrap( m_iSelection[pn], m_vpSteps.size() );
		}

		if( CLAMP(m_iSelection[pn],0,m_vpSteps.size()-1) )
			return;

		//EN esta linea hay un BUG!
		// the user explicity switched difficulties.  Update the preferred Difficulty and StepsType
		Steps *pSteps = m_vpSteps[ m_iSelection[pn] ];

		//if( GAMESTATE->IsMissionMode() )//modificado por mi, salta si es autogen
		//	if( pSteps->IsAutogen() )
		//		return;

		GAMESTATE->ChangePreferredDifficultyAndStepsType( pn, pSteps->GetDifficulty(), pSteps->m_StepsType );

	}
	else if( GAMESTATE->m_pCurCourse )
	{
		m_iSelection[pn] += dir;
		if( CLAMP(m_iSelection[pn],0,m_vpTrails.size()-1) )
			return;

		//  Update the preferred Difficulty and StepsType
		Trail *pTrail = m_vpTrails[ m_iSelection[pn] ];
		GAMESTATE->ChangePreferredCourseDifficultyAndStepsType( pn, pTrail->m_CourseDifficulty, pTrail->m_StepsType );
	}
	else
	{
		// If we're showing multiple StepsTypes in the list, don't allow changing the difficulty/StepsType 
		// when a non-Song, non-Course is selected.  Chaning the preferred Difficulty and StepsType
		// by direction is complicated when multiple StepsTypes are being shown, so we don't support it.
		if( CommonMetrics::AUTO_SET_STYLE )
			return;
		if( !GAMESTATE->ChangePreferredDifficulty( pn, dir ) )
			return;
	}

	vector<PlayerNumber> vpns;
	FOREACH_HumanPlayer( p )
	{
		if( pn == p || GAMESTATE->DifficultiesLocked() )
		{
			m_iSelection[p] = m_iSelection[pn];
			vpns.push_back( p );
		}
	}
	AfterStepsOrTrailChange( vpns );

	float fBalance = GameSoundManager::GetPlayerBalance( pn );

	
	if( dir < 0 )
	{
		m_soundDifficultyEasier.SetProperty( "Pan", fBalance );
		m_soundDifficultyEasier.PlayCopy();
	}
	else if( dir > 0 )
	{
		m_soundDifficultyHarder.SetProperty( "Pan", fBalance );
		m_soundDifficultyHarder.PlayCopy();
	}

	//MODIFICADO POR MI, useconfirm
	if(m_bSelected && PREFSMAN->m_bUseNXStyle)
	{
		/*m_sprConfirm.SetVisible (false);
		m_sprConfirmMessage.SetVisible (false);*/
		m_bSelected = false;
	}

	Message msg( "ChangeSteps" );
	msg.SetParam( "Player", pn );
	msg.SetParam( "Direction", dir );
	MESSAGEMAN->Broadcast( msg );
}

void ScreenSelectMusic::HandleMessage( const Message &msg )
{
	if( msg == "ExitOptionsList" )
	{
		PlayerNumber pn;
		bool b = msg.GetParam( "Player", pn );
		ASSERT(b);

		m_OptionsList[pn].Close();
	}
	else if( msg == "MoveGroup" )
	{
		RString s;
		bool b = msg.GetParam( "Group", s );
		ASSERT(b);

		//Message msg( "UpdateGroupName" );
		//msg.SetParam( "Group", s );
		//MESSAGEMAN->Broadcast( msg );
		if( GAMESTATE->m_pCurSong->m_sGroupName != s )
		{
			m_sLastGroup = s;

			vector<Song*> vpSongs = SONGMAN->GetSongs( m_sLastGroup );
			GAMESTATE->m_pCurSong.Set( vpSongs[0] );
			m_MusicWheel.SelectSong( GAMESTATE->m_pCurSong );
			AfterMusicChange();
			m_MusicWheel.RebuildWheelItems();
		}
	}

	ScreenWithMenuElements::HandleMessage( msg );
}

void ScreenSelectMusic::HandleScreenMessage( const ScreenMessage SM )
{
	if( SM == SM_AllowOptionsMenuRepeat )
	{
		m_bAllowOptionsMenuRepeat = true;
	}
	else if( SM == SM_MenuTimer )
	{
		if( m_MusicWheel.IsRouletting() )
		{
			MenuStart( InputEventPlus() );
			m_MenuTimer->SetSeconds( 15 );
			m_MenuTimer->Start();
		}
		else if( DO_ROULETTE_ON_MENU_TIMER  &&  m_MusicWheel.GetSelectedSong() == NULL  &&  m_MusicWheel.GetSelectedCourse() == NULL )
		{
			m_MusicWheel.StartRoulette();
			m_MenuTimer->SetSeconds( 15 );
			m_MenuTimer->Start();
		}
		else
		{
			// Finish sort changing so that the wheel can respond immediately to our
			// request to choose random.
			m_MusicWheel.FinishChangingSorts();
			if( m_MusicWheel.GetSelectedSong() == NULL && m_MusicWheel.GetSelectedCourse() == NULL )
				m_MusicWheel.StartRandom();

			MenuStart( InputEventPlus() );
			//MODIFICADO POR MI
			//HACE QUE CUANDO SE TERMINE EL TIEMPO
			//SE AUTO SELECCIONE LA CANCION
			if ( PREFSMAN->m_bUseNXStyle && !IsTransitioning() )
				MenuStart( InputEventPlus() );
		}
		return;
	}
	else if( SM == SM_GoToPrevScreen )
	{
		/* We may have stray SM_SongChanged messages from the music wheel.  We can't
		 * handle them anymore, since the title menu (and attract screens) reset
		 * the game state, so just discard them. */
		ClearMessageQueue();
	}
	else if( SM == SM_BeginFadingOut )
	{
		m_bAllowOptionsMenu = false;
		if( OPTIONS_MENU_AVAILABLE && !m_bGoToOptions )
			this->PlayCommand( "HidePressStartForOptions" );

		this->PostScreenMessage( SM_GoToNextScreen, this->GetTweenTimeLeft() );
	}
	else if( SM == SM_GoToNextScreen )
	{
		if( !m_bGoToOptions )
			SOUND->StopMusic();

		if( GAMESTATE->IsWorldTourMode() )
		{
			FOREACH_EnabledPlayer( p )
			{
				if( GAMESTATE->IsPlayerEnabled( p ) )
				{
					PlayerOptions po = GAMESTATE->m_pPlayerState[p]->m_PlayerOptions.GetStage();
					po.FromString( GAMESTATE->m_pCurSong->m_MissionInfo.m_sMods );
					GAMESTATE->m_pPlayerState[p]->m_PlayerOptions.Assign( ModsLevel_Preferred, po );
				}
			}
		}
	}
	else if( SM == SM_SongChanged )
	{
		AfterMusicChange();
		//m_textMileageTotal.SetVisible( false );
	}
	else if( SM == SM_SortOrderChanging ) /* happens immediately */
	{
		this->PlayCommand( "SortChange" );
	}
	else if( SM == SM_GainFocus )
	{
		CodeDetector::RefreshCacheItems( CODES );
	}
	else if( SM == SM_LoseFocus )
	{
		CodeDetector::RefreshCacheItems(); /* reset for other screens */
	}
	else if( SM == SM_PlayerJoinLate )
	{
		SCREENMAN->SetNewScreen( LAUNCHSCREENWHENLATEJOIN );
	}

	ScreenWithMenuElements::HandleScreenMessage( SM );
}

void ScreenSelectMusic::MenuStart( const InputEventPlus &input )
{
	//MODIFICADO POR MI
	//CONFIRMACION DE SELECCION, si la wheel no esta quieta
	if( !m_MusicWheel.IsSettled() )
		return;

	if( input.type != IET_FIRST_PRESS )
		return;

	/* If select is being pressed, this is probably an attempt to change the sort, not
	 * to pick a song or difficulty.  If it gets here, the actual select press was probably
	 * hit during a tween and ignored.  Ignore it. */
	if( input.pn != PLAYER_INVALID && INPUTMAPPER->IsBeingPressed(GAME_BUTTON_SELECT, input.pn) )
		return;

	// Honor locked input for start presses.
	if( m_fLockInputSecs > 0 )
		return;

	switch( m_SelectionState )
	{
	DEFAULT_FAIL( m_SelectionState );
	case SelectionState_SelectingGroups:
		//SCREENMAN->SystemMessage("MENUSTART: GROUPS");

		MESSAGEMAN->Broadcast( "StartSelectingSong" );
		m_SelectionState = SelectionState_SelectingSong;

		//MESSAGEMAN->Broadcast( "GroupSelected" );

		m_MusicWheel.SetOpenGroup(m_sLastGroup);
		m_MusicWheel.Move(+1);
		m_MusicWheel.Move(-1);
		m_MusicWheel.Move(0);

		return;
		break;
	case SelectionState_SelectingSong:

		/* If false, we don't have a selection just yet. */
		if( !m_MusicWheel.Select() )
			return;

		if( m_MusicWheel.GetSelectedSong() != NULL && m_MusicWheel.GetSelectedSong()->m_bMissionWorldTourLocked )
		{
			m_soundLocked.Play();
			return;
		}

		//mostramos el mensaje de que no tenemos suficiente millage
		if( m_MusicWheel.GetSelectedSong() != NULL && 
			m_MusicWheel.GetSelectedSong()->m_bIsSpecialSong && 
			m_MusicWheel.GetSelectedSong()->m_bIsSpecialSongLocked )
		{
			Profile* pProfile = PROFILEMAN->IsPersistentProfile( input.pn ) ? PROFILEMAN->GetProfile( input.pn ) : PROFILEMAN->GetMachineProfile();
			bool bAlreadyBuy = false;

			FOREACHS_CONST( RString, pProfile->m_vsSongsUnlockeds, su )
			{
				RString unlock = su->c_str();

				if( !strcmp( m_MusicWheel.GetSelectedSong()->GetDisplayMainTitle(), unlock ) )//si ya compramos la cancion
					bAlreadyBuy = true;//as� no compramos 2 veces o m�s
			}

			//TODO: aqui solo mandamos msg para .luas...
			if( m_MusicWheel.GetSelectedSong()->m_iMileageCost > pProfile->m_iTotalMileage )
			{
				//m_sprNotEnoughMileage.PlayCommand( "Pulse" );
				MESSAGEMAN->Broadcast( "NotEnoughMileage" );
				m_soundMissionLocked.Play();
				return;
			}
			else if( m_MusicWheel.GetSelectedSong()->m_iMileageCost <= pProfile->m_iTotalMileage && !m_bSelected )//mostramos el msg de compra de song :)
			{
				//m_textMileageRequired.SetText( ssprintf( "%d - %d", pProfile->m_iTotalMileage, m_MusicWheel.GetSelectedSong()->m_iMileageCost ) );
				//m_textMileageRequired.SetVisible( true );
				//m_textMileageTotal.SetText( ssprintf( "%.6d", pProfile->m_iTotalMileage-m_MusicWheel.GetSelectedSong()->m_iMileageCost ) );
				//m_textMileageTotal.SetVisible( true );
				//m_UnlockDialog.SetVisible( true );

				{
					Message msg( "BuyingSong" );
					msg.SetParam( "iMileage", pProfile->m_iTotalMileage-m_MusicWheel.GetSelectedSong()->m_iMileageCost );
					MESSAGEMAN->Broadcast( msg );
				}
			}
			else if( m_bSelected && !bAlreadyBuy )//aqui realmente compramos la cancion
			{
				//m_textMileageTotal.SetVisible( false );
				//m_textMileageRequired.PlayCommand( "Off" );
				//m_textMileageTotal.PlayCommand( "Off" );
				//m_UnlockDialog.PlayCommand( "Off" );
				m_soundUnlock.Play();
				pProfile->m_iTotalMileage -= m_MusicWheel.GetSelectedSong()->m_iMileageCost;
				pProfile->m_vsSongsUnlockeds.insert( m_MusicWheel.GetSelectedSong()->GetDisplayMainTitle() );
				m_MusicWheel.GetSelectedSong()->m_bIsSpecialSongLocked = false;
				MESSAGEMAN->Broadcast( "SongBought" );
				m_MusicWheel.ChangeMusic( 0 );
				m_bSelected = false;
				return;
			}
		}

		if( !m_bSelected && PREFSMAN->m_bUseNXStyle )
		{
			/*m_sprConfirm.SetVisible (true);
			m_sprConfirmMessage.SetVisible (true);*/
			MESSAGEMAN->Broadcast( "CenterWasPressedOnce" );
			m_soundStart.Play();
			m_bSelected = true;
			return;
		}
		MESSAGEMAN->Broadcast( "CenterWasPressedTwice" );

		// a song was selected
		if( m_MusicWheel.GetSelectedSong() != NULL && !m_MusicWheel.GetSelectedSong()->m_bIsSpecialSongLocked )//modificado por mi, modemission
		{
			const bool bIsNew = PROFILEMAN->IsSongNew( m_MusicWheel.GetSelectedSong() );
			bool bIsHard = false;
			FOREACH_HumanPlayer( p )
			{
				if( GAMESTATE->m_pCurSteps[p]  &&  GAMESTATE->m_pCurSteps[p]->GetMeter() >= 10 )
					bIsHard = true;
			}

			/* See if this song is a repeat.  If we're in event mode, only check the last five songs. */
			bool bIsRepeat = false;
			int i = 0;
			if( GAMESTATE->IsEventMode() )
				i = max( 0, int(STATSMAN->m_vPlayedStageStats.size())-5 );
			for( ; i < (int)STATSMAN->m_vPlayedStageStats.size(); ++i )
				if( STATSMAN->m_vPlayedStageStats[i].m_vpPlayedSongs.back() == m_MusicWheel.GetSelectedSong() )
					bIsRepeat = true;

			/* Don't complain about repeats if the user didn't get to pick. */
			if( GAMESTATE->IsAnExtraStage() )
				bIsRepeat = false;

			if( bIsRepeat )
				SOUND->PlayOnceFromAnnouncer( "select music comment repeat" );
			else if( bIsNew )
				SOUND->PlayOnceFromAnnouncer( "select music comment new" );
			else if( bIsHard )
				SOUND->PlayOnceFromAnnouncer( "select music comment hard" );
			else
				SOUND->PlayOnceFromAnnouncer( "select music comment general" );

			/* If we're in event mode, we may have just played a course (putting us
			* in course mode).  Make sure we're in a single song mode. */
			if( GAMESTATE->IsCourseMode() )
				GAMESTATE->m_PlayMode.Set( PLAY_MODE_REGULAR );
		}
		else if( m_MusicWheel.GetSelectedCourse() != NULL )
		{
			SOUND->PlayOnceFromAnnouncer( "select course comment general" );

			Course *pCourse = m_MusicWheel.GetSelectedCourse();
			ASSERT( pCourse );
			GAMESTATE->m_PlayMode.Set( pCourse->GetPlayMode() );

			// apply #LIVES
			if( pCourse->m_iLives != -1 )
			{
				SO_GROUP_ASSIGN( GAMESTATE->m_SongOptions, ModsLevel_Stage, m_LifeType, SongOptions::LIFE_BATTERY );
				SO_GROUP_ASSIGN( GAMESTATE->m_SongOptions, ModsLevel_Stage, m_iBatteryLives, pCourse->m_iLives );
			}
			if( pCourse->GetCourseType() == COURSE_TYPE_SURVIVAL)
				SO_GROUP_ASSIGN( GAMESTATE->m_SongOptions, ModsLevel_Stage, m_LifeType, SongOptions::LIFE_TIME );
		}
		else
		{
			/* We havn't made a selection yet. */
			return;
		}

		break;

	case SelectionState_SelectingSteps:
		{
			PlayerNumber pn = input.pn;
			bool bInitiatedByMenuTimer = pn == PLAYER_INVALID;
			bool bAllOtherHumanPlayersDone = true;
			FOREACH_HumanPlayer( p )
			{
				if( p == pn )
					continue;
				bAllOtherHumanPlayersDone &= m_bStepsChosen[p];
			}

			bool bAllPlayersDoneSelectingSteps = bInitiatedByMenuTimer || bAllOtherHumanPlayersDone;
			if( !bAllPlayersDoneSelectingSteps )
			{
				m_bStepsChosen[pn] = true;
				m_soundStart.Play();

				Message msg("StepsChosen");
				msg.SetParam( "Player", pn );
				MESSAGEMAN->Broadcast( msg );
				return;
			}
		}
		break;
	}

	FOREACH_ENUM( PlayerNumber, p )
	{
		if( !TWO_PART_SELECTION || m_SelectionState == SelectionState_SelectingSteps )
		{
			if( m_OptionsList[p].IsOpened() )
				CloseOptionsList(p);
		}
		UpdateSelectButton( p, false );
	}

	m_SelectionState = GetNextSelectionState();
	Message msg( "Start" + SelectionStateToString(m_SelectionState) );
	MESSAGEMAN->Broadcast( msg );

	m_soundStart.Play();


	if( m_SelectionState == SelectionState_Finalized )
	{


		m_MenuTimer->Stop();


		FOREACH_HumanPlayer( p )
		{
			if( !m_bStepsChosen[p] )
			{
				m_bStepsChosen[p] = true;
				/* Don't play start sound.  We play it again below on finalized */
				//m_soundStart.Play();

				Message msg("StepsChosen");
				msg.SetParam( "Player", p );
				MESSAGEMAN->Broadcast( msg );
			}
		}

		if( CommonMetrics::AUTO_SET_STYLE )
		{
			/* Now that Steps have been chosen, set a Style that can play them. */
			const Style *pStyle = NULL;
			if( GAMESTATE->IsCourseMode() )
				pStyle = GAMESTATE->m_pCurCourse->GetCourseStyle( GAMESTATE->m_pCurGame, GAMESTATE->GetNumSidesJoined() );
			if( pStyle == NULL )
			{
				StepsType stCurrent;
				PlayerNumber pn = GAMESTATE->m_MasterPlayerNumber;
				if( GAMESTATE->IsCourseMode() )
				{
					ASSERT( GAMESTATE->m_pCurTrail[pn] );
					stCurrent = GAMESTATE->m_pCurTrail[pn]->m_StepsType;
				}
				else
				{
					//ASSERT( GAMESTATE->m_pCurSteps[pn] );
					stCurrent = GAMESTATE->m_pCurSteps[pn]->m_StepsType;
				}
				vector<StepsType> vst;
				pStyle = GAMEMAN->GetFirstCompatibleStyle( GAMESTATE->m_pCurGame, GAMESTATE->GetNumSidesJoined(), stCurrent );
			}
			GAMESTATE->SetCurrentStyle( pStyle );
		}


		/* If we're currently waiting on song assets, abort all except the music and
		* start the music, so if we make a choice quickly before background requests
		* come through, the music will still start. */
		g_bCDTitleWaiting = g_bBannerWaiting = false;
		m_BackgroundLoader.Abort();
		CheckBackgroundRequests( true );

		if( OPTIONS_MENU_AVAILABLE )
		{
			// show "hold START for options"
			this->PlayCommand( "ShowPressStartForOptions" );

			m_bAllowOptionsMenu = true;

			/* Don't accept a held START for a little while, so it's not
			* hit accidentally.  Accept an initial START right away, though,
			* so we don't ignore deliberate fast presses (which would be
			* annoying). */
			this->PostScreenMessage( SM_AllowOptionsMenuRepeat, 0.5f );

			StartTransitioningScreen( SM_None );
			float fTime = max( SHOW_OPTIONS_MESSAGE_SECONDS, this->GetTweenTimeLeft() );
			this->PostScreenMessage( SM_BeginFadingOut, fTime );
		}
		else
		{
			StartTransitioningScreen( SM_BeginFadingOut );
		}
	}
	else // !finalized.  Set the timer for selecting difficulty and mods.
	{
		;
		// float fSeconds = m_MenuTimer->GetSeconds();
		// if( fSeconds < 10 )
		// {
		// 	m_MenuTimer->SetSeconds( 10 );
		// 	m_MenuTimer->Start();
		// }
	}
}


void ScreenSelectMusic::MenuBack( const InputEventPlus &input )
{
	m_BackgroundLoader.Abort();
	Cancel( SM_GoToPrevScreen );
}

void ScreenSelectMusic::AfterStepsOrTrailChange( const vector<PlayerNumber> &vpns )
{
	int iBestScore = 0;
	Grade grade = Grade_Invalid;
	RString sScoreName = RString("");
	FOREACH_CONST( PlayerNumber, vpns, p )
	{
		PlayerNumber pn = *p;
		ASSERT( GAMESTATE->IsHumanPlayer(pn) );
		
		if( GAMESTATE->m_pCurSong )
		{
			CLAMP( m_iSelection[pn], 0, m_vpSteps.size()-1 );
			Song* pSong = GAMESTATE->m_pCurSong;
			Steps* pSteps = m_vpSteps.empty()? NULL: m_vpSteps[m_iSelection[pn]];

			GAMESTATE->m_pCurSteps[pn].Set( pSteps );
			GAMESTATE->m_pCurTrail[pn].Set( NULL );

			if( pSteps )
			{//carga las calorias y el vo2 por cada canci�n
				NoteData temp;
				pSteps->GetNoteData( temp );
				float kcal = NoteDataUtil::GetKCal( temp );
				float vo2 = NoteDataUtil::GetVO2( temp );

				/*m_textKcalVO2[pn].SetText( ssprintf("%.0f %.0f", kcal, vo2 ) );*/

				{
					Message msg( ssprintf( "SetKcalPlayer%d", pn+1 ) );
					msg.SetParam( "iKcal", kcal );
					MESSAGEMAN->Broadcast( msg );
				}
				{
					Message msg( ssprintf( "SetVO2Player%d", pn+1 ) );
					msg.SetParam( "iVO2", vo2 );
					MESSAGEMAN->Broadcast( msg );
				}
			}

			int iScore = 0;
			const Profile *pProfile = PROFILEMAN->IsPersistentProfile(pn) ? PROFILEMAN->GetProfile(pn) : PROFILEMAN->GetMachineProfile();

			if( pSteps )
			{
				iScore = pProfile->GetStepsHighScoreList(pSong,pSteps).GetTopScore().GetScore();
				grade = pProfile->GetStepsHighScoreList(pSong,pSteps).HighGrade;
			}

			if( iScore > iBestScore )//modificado por mi, clampeo
			{
				iBestScore = iScore;
				sScoreName = pProfile->GetDisplayNameOrHighScoreName();
			}
		}
		else if( GAMESTATE->m_pCurCourse )
		{
			CLAMP( m_iSelection[pn], 0, m_vpTrails.size()-1 );

			Course* pCourse = GAMESTATE->m_pCurCourse;
			Trail* pTrail = m_vpTrails.empty()? NULL: m_vpTrails[m_iSelection[pn]];
			
			GAMESTATE->m_pCurSteps[pn].Set( NULL );			
			GAMESTATE->m_pCurTrail[pn].Set( pTrail );

			int iScore = 0;
			if( pTrail )
			{
				const Profile *pProfile = PROFILEMAN->IsPersistentProfile(pn) ? PROFILEMAN->GetProfile(pn) : PROFILEMAN->GetMachineProfile();
				iScore = pProfile->GetCourseHighScoreList(pCourse,pTrail).GetTopScore().GetScore();
			}

			//m_textHighScore[pn].SetText( ssprintf("%*i", NUM_SCORE_DIGITS, iScore) );
		}
	}

	/*if( iBestScore != 0 )
	{*/
		//m_textHighScore.SetText( ssprintf("%i", iBestScore) );
		//m_textHighScoreName.SetText( sScoreName );

		{
			Message msg("SendHighScoreName");
			msg.SetParam( "sScoreName", sScoreName );
			MESSAGEMAN->Broadcast( msg );
		}

		{
			Message msg("SendHighScore");
			msg.SetParam( "iScore", iBestScore );
			msg.SetParam( "Grade", grade );
			MESSAGEMAN->Broadcast( msg );
		}
	//}
	/*else
	{
		m_textHighScore.SetText( "" );
		m_textHighScoreName.SetText( "" );
	}*/
}

void ScreenSelectMusic::SwitchToPreferredDifficulty()
{
	if( !GAMESTATE->m_pCurCourse )
	{
		FOREACH_HumanPlayer( pn )
		{
			/* Find the closest match to the user's preferred difficulty and StepsType. */
			int iCurDifference = -1;
			int &iSelection = m_iSelection[pn];
			FOREACH_CONST( Steps*, m_vpSteps, s )
			{
				int i = s - m_vpSteps.begin();

				/* If the current steps are listed, use them. */
				if( GAMESTATE->m_pCurSteps[pn] == *s )
				{
					iSelection = i;
					break;
				}

				if( GAMESTATE->m_PreferredDifficulty[pn] != Difficulty_Invalid  )
				{
					int iDifficultyDifference = abs( (*s)->GetDifficulty() - GAMESTATE->m_PreferredDifficulty[pn] );
					int iStepsTypeDifference = 0;
					if( GAMESTATE->m_PreferredStepsType != StepsType_Invalid )
						iStepsTypeDifference = abs( (*s)->m_StepsType - GAMESTATE->m_PreferredStepsType );
					int iTotalDifference = iStepsTypeDifference * NUM_Difficulty + iDifficultyDifference;

					if( iCurDifference == -1 || iTotalDifference < iCurDifference )
					{
						iSelection = i;
						iCurDifference = iTotalDifference;
					}
				}
			}

			CLAMP( iSelection, 0, m_vpSteps.size()-1 );
		}
	}
	else
	{
		FOREACH_HumanPlayer( pn )
		{
			/* Find the closest match to the user's preferred difficulty. */
			int iCurDifference = -1;
			int &iSelection = m_iSelection[pn];
			FOREACH_CONST( Trail*, m_vpTrails, t )
			{
				int i = t - m_vpTrails.begin();

				/* If the current trail is listed, use it. */
				if( GAMESTATE->m_pCurTrail[pn] == m_vpTrails[i] )
				{
					iSelection = i;
					break;
				}

				if( GAMESTATE->m_PreferredCourseDifficulty[pn] != Difficulty_Invalid  &&  GAMESTATE->m_PreferredStepsType != StepsType_Invalid  )
				{
					int iDifficultyDifference = abs( (*t)->m_CourseDifficulty - GAMESTATE->m_PreferredCourseDifficulty[pn] );
					int iStepsTypeDifference = abs( (*t)->m_StepsType - GAMESTATE->m_PreferredStepsType );
					int iTotalDifference = iStepsTypeDifference * NUM_CourseDifficulty + iDifficultyDifference;

					if( iCurDifference == -1 || iTotalDifference < iCurDifference )
					{
						iSelection = i;
						iCurDifference = iTotalDifference;
					}
				}
			}

			CLAMP( iSelection, 0, m_vpTrails.size()-1 );
		}
	}

	if( GAMESTATE->DifficultiesLocked() )
	{
		FOREACH_HumanPlayer( p )
			m_iSelection[p] = m_iSelection[GAMESTATE->m_MasterPlayerNumber];
	}
}

void ScreenSelectMusic::AfterMusicChange()
{
	if( !m_MusicWheel.IsRouletting() )
		m_MenuTimer->Stall();

	Song* pSong = m_MusicWheel.GetSelectedSong();

	//TODO: reparar esto!
	////modificado por mi, exp
	if( pSong )
	{
		RString sOldGroup = "NULL";//primer valor!
		if( GAMESTATE->m_pCurSong )
			sOldGroup = GAMESTATE->m_pCurSong->m_sGroupName;

		GAMESTATE->m_pCurSong.Set( pSong );

		RString sGroupNew = GAMESTATE->m_pCurSong->m_sGroupName;		
		if( sGroupNew == "" )
			sGroupNew = "sorting";
		if( strcmp( sOldGroup, sGroupNew ) )//modificado por mi, si son distintos!
		{
			//HACK: la primera vez que entra a la screen, no lo hace!
			if( sOldGroup.CompareNoCase("NULL") != 0 )
			{
				//si estamos en easy no tenemos grupos
				if( BROADCAST_GROUP_CHANGE )
				{
					m_soundChangeGroup.Play();

					Message msg( "GroupChanged" );
					MESSAGEMAN->Broadcast( msg );
					m_MusicWheel.Deccelerate();

					FOREACH_HumanPlayer( p )
					{
						INPUTMAPPER->ResetKeyRepeat( m_GameButtonPreviousSong, p );
						INPUTMAPPER->ResetKeyRepeat( m_GameButtonNextSong, p );							
					}

					m_MusicWheel.Move( 0 );
				}
			}
		}
	}

	if( pSong )
		GAMESTATE->m_pPreferredSong = pSong;

	Course* pCourse = m_MusicWheel.GetSelectedCourse();
	GAMESTATE->m_pCurCourse.Set( pCourse );
	if( pCourse )
		GAMESTATE->m_pPreferredCourse = pCourse;

	m_vpSteps.clear();
	m_vpTrails.clear();

	m_Banner.SetMovingFast( !!m_MusicWheel.IsMoving() );

	vector<RString> m_Artists, m_AltArtists;

	m_sSampleMusicToPlay = "";
	m_pSampleMusicTimingData = NULL;
	g_sCDTitlePath = "";
	g_sBannerPath = "";
	g_bWantFallbackCdTitle = false;
	bool bWantBanner = true;

	static SortOrder s_lastSortOrder = SortOrder_Invalid;
	if( GAMESTATE->m_SortOrder != s_lastSortOrder )
	{
		// Reload to let Lua metrics have a chance to change the help text.
		s_lastSortOrder = GAMESTATE->m_SortOrder;
	}

	switch( m_MusicWheel.GetSelectedType() )
	{
	case TYPE_SECTION:
	case TYPE_SORT:
	case TYPE_ROULETTE:
	case TYPE_RANDOM:
		FOREACH_PlayerNumber( p )
			m_iSelection[p] = -1;

		g_sCDTitlePath = ""; // none

		m_fSampleStartSeconds = 0;
		m_fSampleLengthSeconds = -1;

		switch( m_MusicWheel.GetSelectedType() )
		{
		case TYPE_SECTION:
			g_sBannerPath = SONGMAN->GetSongGroupBannerPath( m_MusicWheel.GetSelectedSection() );
			m_sSampleMusicToPlay = m_sSectionMusicPath;
			break;
		case TYPE_SORT:
			bWantBanner = false; /* we load it ourself */
			m_Banner.LoadMode();
			m_sSampleMusicToPlay = m_sSortMusicPath;
			break;
		case TYPE_ROULETTE:
			bWantBanner = false; /* we load it ourself */
			m_Banner.LoadRoulette();
			m_sSampleMusicToPlay = m_sRouletteMusicPath;
			break;
		case TYPE_RANDOM:
			bWantBanner = false; /* we load it ourself */
			m_Banner.LoadRandom();
			m_sSampleMusicToPlay = m_sRandomMusicPath;
			break;
		default:
			ASSERT(0);
		}
		break;
	case TYPE_SONG:
	case TYPE_PORTAL:
		m_sSampleMusicToPlay = pSong->GetMusicPath();
		//FIXME:
		m_pSampleMusicTimingData = &pSong->m_Timing;
		m_fSampleStartSeconds = pSong->m_fMusicSampleStartSeconds;
		m_fSampleLengthSeconds = pSong->m_fMusicSampleLengthSeconds;

		SongUtil::GetPlayableSteps( pSong, m_vpSteps );

		if ( PREFSMAN->m_bShowBanners )
			g_sBannerPath = pSong->GetBannerPath();

		g_sCDTitlePath = pSong->GetCDTitlePath();
		g_bWantFallbackCdTitle = true;
		
		SwitchToPreferredDifficulty();

		////MODIFICADO POR MI
		//if (GAMESTATE->GetCurrentStyle()->m_StyleType == StyleType_OnePlayerOneSide)
		//{
		//	m_ModeNightmare.SetVisible( false );
		//	m_ModeFreestyle.SetVisible( false );

		//	FOREACH_EnabledPlayer( pn )
		//		ChangeDifficulty( pn, 0 );
		//}

		break;
	case TYPE_COURSE:
	{
		const Course *pCourse = m_MusicWheel.GetSelectedCourse();
		const Style *pStyle = NULL;
		if( CommonMetrics::AUTO_SET_STYLE )
			pStyle = pCourse->GetCourseStyle( GAMESTATE->m_pCurGame, GAMESTATE->GetNumSidesJoined() );
		if( pStyle == NULL )
			pStyle = GAMESTATE->GetCurrentStyle();
		pCourse->GetTrails( m_vpTrails, pStyle->m_StepsType );

		m_sSampleMusicToPlay = m_sCourseMusicPath;
		m_fSampleStartSeconds = 0;
		m_fSampleLengthSeconds = -1;

		g_sBannerPath = pCourse->GetBannerPath();
		if( g_sBannerPath.empty() )
			m_Banner.LoadFallback();

		SwitchToPreferredDifficulty();
		break;
	}
	default:
		ASSERT(0);
	}

	m_sprCDTitleFront.UnloadTexture();
	m_sprCDTitleBack.UnloadTexture();

	/* Cancel any previous, incomplete requests for song assets, since we need new ones. */
	m_BackgroundLoader.Abort();

	g_bCDTitleWaiting = false;
	if( !g_sCDTitlePath.empty() || g_bWantFallbackCdTitle )
	{
		LOG->Trace( "cache \"%s\"", g_sCDTitlePath.c_str());
		m_BackgroundLoader.CacheFile( g_sCDTitlePath ); // empty OK
		g_bCDTitleWaiting = true;
	}

	g_bBannerWaiting = false;
	if( bWantBanner )
	{
		LOG->Trace("LoadFromCachedBanner(%s)",g_sBannerPath .c_str());
		if( m_Banner.LoadFromCachedBanner( g_sBannerPath ) )
		{
			/* If the high-res banner is already loaded, just
			 * delay before loading it, so the low-res one has
			 * time to fade in. */
			if( !TEXTUREMAN->IsTextureRegistered( Sprite::SongBannerTexture(g_sBannerPath) ) )
				m_BackgroundLoader.CacheFile( g_sBannerPath );

			g_bBannerWaiting = true;
		}
	}

	// Don't stop music if it's already playing the right file.
	g_bSampleMusicWaiting = false;
	if( !m_MusicWheel.IsRouletting() && SOUND->GetMusicPath() != m_sSampleMusicToPlay )
	{
		SOUND->StopMusic();
		if( !m_sSampleMusicToPlay.empty() )
			g_bSampleMusicWaiting = true;
	}

	g_StartedLoadingAt.Touch();

	vector<PlayerNumber> vpns;
	FOREACH_HumanPlayer( p )
		vpns.push_back( p );

	AfterStepsOrTrailChange( vpns );


	//if( GAMESTATE->IsMissionMode() )//modificado por mi, modemission
	//	FOREACH_EnabledPlayer( pn )
	//	{
	//		Steps *pSteps = m_vpSteps[ m_iSelection[pn] ];
	//		if( pSteps->IsAutogen() )
	//			ChangeDifficulty( pn, +1 );
	//	}
}

void ScreenSelectMusic::OpenOptionsList( PlayerNumber pn )
{
	m_OptionsList[pn].Open();
	Message msg("OptionsListOpened");
	msg.SetParam( "Player", pn );
	MESSAGEMAN->Broadcast( msg );
}

void ScreenSelectMusic::CloseOptionsList( PlayerNumber pn )
{
	m_OptionsList[pn].Close();
	Message msg("OptionsListClosed");
	msg.SetParam( "Player", pn );
	MESSAGEMAN->Broadcast( msg );
}

// lua start
#include "LuaBinding.h"

class LunaScreenSelectMusic: public Luna<ScreenSelectMusic>
{
public:
	static int GetGoToOptions( T* p, lua_State *L ) { lua_pushboolean( L, p->GetGoToOptions() ); return 1; }
	static int OpenOptionsList( T* p, lua_State *L ) { PlayerNumber pn = Enum::Check<PlayerNumber>(L, 1);  p->OpenOptionsList(pn); return 0; }
	static int CloseOptionsList( T* p, lua_State *L ) { PlayerNumber pn = Enum::Check<PlayerNumber>(L, 1);  p->CloseOptionsList(pn); return 0; }
	static int GetMusicWheel( T* p, lua_State *L ) {
		p->GetMusicWheel()->PushSelf(L);
		return 1;
	}

	LunaScreenSelectMusic()
	{
  		ADD_METHOD( GetGoToOptions );
		ADD_METHOD( OpenOptionsList );
		ADD_METHOD( CloseOptionsList );
		ADD_METHOD( GetMusicWheel );
	}
};

LUA_REGISTER_DERIVED_CLASS( ScreenSelectMusic, ScreenWithMenuElements )
// lua end

/*
 * (c) 2001-2004 Chris Danford
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
