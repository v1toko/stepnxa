#include "global.h"
#include "BackgroundUtil.h"
#include "GameManager.h"
#include "MsdFile.h"
#include "NoteTypes.h"
#include "RageFileManager.h"
#include "RageLog.h"
#include "RageUtil.h"
#include "song.h"
#include "SongManager.h"
#include "Steps.h"
#include "NotesLoaderNXA.h"

const int MAX_EDIT_STEPS_SIZE_BYTES		= 30*1024;	// 30KB

static void LoadFromSMTokens( RString sStepsType, RString sDescription, RString sDifficulty, RString sMeter,
	RString sRadarValues, RString sNoteData, Steps &out )
{
	out.SetSavedToDisk( true );	// we're loading from disk, so this is by definintion already saved

	Trim( sStepsType );
	Trim( sDescription );
	Trim( sDifficulty );
	Trim( sNoteData );

//	LOG->Trace( "Steps::LoadFromSMTokens()" );

	out.m_StepsType = GameManager::StringToStepsType( sStepsType );
	out.SetDescription( sDescription );
	out.SetDifficulty( StringToDifficulty(sDifficulty) );

	// HACK:  We used to store SMANIAC as Difficulty_Hard with special description.
	// Now, it has its own Difficulty_Challenge
	if( sDescription.CompareNoCase("smaniac") == 0 ) 
		out.SetDifficulty( Difficulty_Challenge );
	// HACK:  We used to store CHALLENGE as Difficulty_Hard with special description.
	// Now, it has its own Difficulty_Challenge
	if( sDescription.CompareNoCase("challenge") == 0 ) 
		out.SetDifficulty( Difficulty_Challenge );

	out.SetMeter( atoi(sMeter) );
	vector<RString> saValues;
	split( sRadarValues, ",", saValues, true );
	if( saValues.size() == NUM_RadarCategory * NUM_PLAYERS )
	{
		RadarValues v[NUM_PLAYERS];
		FOREACH_PlayerNumber( pn )
			FOREACH_ENUM( RadarCategory, rc )
				v[pn][rc] = StringToFloat( saValues[pn*NUM_RadarCategory + rc] );
		out.SetCachedRadarValues( v );
	}
    
	out.SetSMNoteData( sNoteData );

	out.TidyUpData();
}

void NXALoader::GetApplicableFiles( const RString &sPath, vector<RString> &out )
{
	GetDirListing( sPath + RString("*.snxa"), out );
}

bool NXALoader::LoadTimingFromFile( const RString &fn, TimingData &out )
{
	MsdFile msd;
	if( !msd.ReadFile( fn, true ) )  // unescape
	{
		LOG->UserLog( "Song file", fn, "couldn't be loaded: %s", msd.GetError().c_str() );
		return false;
	}

	out.m_sFile = fn;
	LoadTimingFromNXAFile( msd, out );
	return true;
}

void NXALoader::LoadTimingFromNXAFile( const MsdFile &msd, TimingData &out )
{
	out.m_fBeat0OffsetInSeconds = 0;
	out.m_BPMSegments.clear();
	out.m_StopSegments.clear();
	out.m_TickSegments.clear();
	out.m_ArrowSpacingSegments.clear();
	out.m_ComboSegments.clear();
	out.m_NoteSkinSegments.clear();

	for( unsigned i=0; i<msd.GetNumValues(); i++ )
	{
		const MsdFile::value_t &sParams = msd.GetValue(i);
		RString sValueName = sParams[0];
		sValueName.MakeUpper();

		if( sValueName=="OFFSET" )
		{
			out.m_fBeat0OffsetInSeconds = StringToFloat( sParams[1] );
		}
		else if( sValueName=="STOPS" || sValueName=="FREEZES" )
		{
			vector<RString> arrayFreezeExpressions;
			split( sParams[1], ",", arrayFreezeExpressions );

			for( unsigned f=0; f<arrayFreezeExpressions.size(); f++ )
			{
				vector<RString> arrayFreezeValues;
				split( arrayFreezeExpressions[f], "=", arrayFreezeValues );
				if( arrayFreezeValues.size() != 2 )
				{
					// XXX: Hard to tell which file caused this.
					LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
						      sValueName.c_str(), arrayFreezeExpressions[f].c_str() );
					continue;
				}

				const float fFreezeBeat = StringToFloat( arrayFreezeValues[0] );
				const float fFreezeSeconds = StringToFloat( arrayFreezeValues[1] );
				
				StopSegment new_seg( BeatToNoteRow(fFreezeBeat), fFreezeSeconds );
				out.AddStopSegment( new_seg );
			}
		}

		else if( sValueName=="BPMS" )
		{
			vector<RString> arrayBPMChangeExpressions;
			split( sParams[1], ",", arrayBPMChangeExpressions );

			for( unsigned b=0; b<arrayBPMChangeExpressions.size(); b++ )
			{
				vector<RString> arrayBPMChangeValues;
				split( arrayBPMChangeExpressions[b], "=", arrayBPMChangeValues );
				// XXX: Hard to tell which file caused this.
				if( arrayBPMChangeValues.size() != 2 )
				{
					LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
						      sValueName.c_str(), arrayBPMChangeExpressions[b].c_str() );
					continue;
				}

				const float fBeat = StringToFloat( arrayBPMChangeValues[0] );
				const float fNewBPM = StringToFloat( arrayBPMChangeValues[1] );
								
					out.AddBPMSegment( BPMSegment(BeatToNoteRow(fBeat), fNewBPM) );				
			}
		}
		//tickcount stuff
		else if( sValueName=="TICKCOUNT" )
		{
			vector<RString> arrayTickChangeExpressions;
			split( sParams[1], ",", arrayTickChangeExpressions );

			for( unsigned b=0; b<arrayTickChangeExpressions.size(); b++ )
			{
				vector<RString> arrayTickChangeValues;
				split( arrayTickChangeExpressions[b], "=", arrayTickChangeValues );
				// XXX: Hard to tell which file caused this.
				if( arrayTickChangeValues.size() != 2 )
				{
					LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
						      sValueName.c_str(), arrayTickChangeExpressions[b].c_str() );
					continue;
				}

				const float fBeat = StringToFloat( arrayTickChangeValues[0] );
				const float fNewTickcount = StringToFloat( arrayTickChangeValues[1] );
				
				if( fNewTickcount > -1.0f )
					out.AddTickSegment( TickSegment(BeatToNoteRow(fBeat), fNewTickcount) );
				else
					LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid tickcount change at beat %f, tickcount %f.", fBeat, fNewTickcount );
			}
		}
		//combofactor stuff
		else if( sValueName=="MULTIPLIER" )
		{
			vector<RString> arrayComboExpressions;
			split( sParams[1], ",", arrayComboExpressions );
			
			for( unsigned f=0; f<arrayComboExpressions.size(); f++ )
			{
				vector<RString> arrayComboValues;
				split( arrayComboExpressions[f], "=", arrayComboValues );
				unsigned size = arrayComboValues.size();
				if( size < 2 )
				{
					LOG->UserLog("Song file",
							out.m_sFile,
							 "has an invalid #COMBOS value \"%s\" (must have at least one '='), ignored.",
							 arrayComboExpressions[f].c_str() );
					continue;
				}
				const float fComboBeat = StringToFloat( arrayComboValues[0] );
				const int iCombos = StringToInt( arrayComboValues[1] );
				const int iMisses = (size == 2 ? iCombos : StringToInt(arrayComboValues[2]));
				out.AddComboSegment( ComboSegment( fComboBeat, iCombos, iMisses ) );
			}
		}
		else if( sValueName=="COUNTNOTESSEPARATELY" )
		{
			vector<RString> arrayCountExpressions;
			split( sParams[1], ",", arrayCountExpressions );
			
			for( unsigned f=0; f<arrayCountExpressions.size(); f++ )
			{
				vector<RString> arrayCountValues;
				split( arrayCountExpressions[f], "=", arrayCountValues );
				unsigned size = arrayCountValues.size();
				if( size < 2 )
				{
					LOG->UserLog("Song file",
							out.m_sFile,
							 "has an invalid #COUNTSEPARATELY value \"%s\" (must have at least one '='), ignored.",
							 arrayCountExpressions[f].c_str() );
					continue;
				}
				const float fCountBeat = StringToFloat( arrayCountValues[0] );
				const int iCount = StringToInt( arrayCountValues[1] );
				out.AddCountSepSegment( CountSeparatelySegment( BeatToNoteRow(fCountBeat), iCount ) );
			}
		}
		//arrowspeed stuff
		else if( sValueName=="ARROWSPEED" )
		{
			vector<RString> arrayArrowSpacingChangeExpressions;
			split( sParams[1], ",", arrayArrowSpacingChangeExpressions );

			for( unsigned b=0; b<arrayArrowSpacingChangeExpressions.size(); b++ )
			{
				vector<RString> arrayArrowSpacingChangeValues;
				split( arrayArrowSpacingChangeExpressions[b], "=", arrayArrowSpacingChangeValues );
				// XXX: Hard to tell which file caused this.
				if( arrayArrowSpacingChangeValues.size() != 2 )
				{
					LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
						      sValueName.c_str(), arrayArrowSpacingChangeExpressions[b].c_str() );
					continue;
				}

				const float fBeat = StringToFloat( arrayArrowSpacingChangeValues[0] );
				const float fNewArrowSpacing = StringToFloat( arrayArrowSpacingChangeValues[1] );
				
				if( fNewArrowSpacing > 0.0f )
					out.AddArrowSpacingSegment( ArrowSpacingSegment(BeatToNoteRow(fBeat), fNewArrowSpacing) );
				else
					LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid speed change at beat %f, speed %f.", fBeat, fNewArrowSpacing );
			}
		}
		//noteskins changes stuff
		else if( sValueName=="NOTESKINCHANGES" )
		{
			vector<RString> arrayNoteSkinChangeExpressions;
			split( sParams[1], ",", arrayNoteSkinChangeExpressions );

			for( unsigned b=0; b<arrayNoteSkinChangeExpressions.size(); b++ )
			{
				vector<RString> arrayNoteSkinChangeValues;
				split( arrayNoteSkinChangeExpressions[b], "=", arrayNoteSkinChangeValues );
				// XXX: Hard to tell which file caused this.
				if( arrayNoteSkinChangeValues.size() != 2 )
				{
					//LOG->UserLog( "Song file", "%s", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
					//	      sValueName.c_str(), arrayArrowSpacingChangeExpressions[b].c_str() );
					continue;
				}

				const float fBeat = StringToFloat( arrayNoteSkinChangeValues[0] );
				RString sNewNoteSkin = arrayNoteSkinChangeValues[1];
				
				if( !sNewNoteSkin.empty() )
				{
					Trim(sNewNoteSkin);
					out.AddNoteSkinSegment( NoteSkinSegment(BeatToNoteRow(fBeat), sNewNoteSkin ) );
				}				
			}
		}
		//timesignatures stuff
		else if( sValueName=="TIMESIGNATURES" )
		{
			vector<RString> vs1;
			split( sParams[1], ",", vs1 );

			FOREACH_CONST( RString, vs1, s1 )
			{
				vector<RString> vs2;
				split( *s1, "=", vs2 );

				if( vs2.size() < 3 )
				{
					LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with %i values.", (int)vs2.size() );
					continue;
				}

				const float fBeat = StringToFloat( vs2[0] );

				TimeSignatureSegment seg;
				seg.m_iStartRow = BeatToNoteRow(fBeat);
				seg.m_iNumerator = atoi( vs2[1] ); 
				seg.m_iDenominator = atoi( vs2[2] ); 
				
				if( fBeat < 0 )
				{
					LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with beat %f.", fBeat );
					continue;
				}
				
				if( seg.m_iNumerator < 1 )
				{
					LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with beat %f, iNumerator %i.", fBeat, seg.m_iNumerator );
					continue;
				}

				if( seg.m_iDenominator < 1 )
				{
					LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with beat %f, iDenominator %i.", fBeat, seg.m_iDenominator );
					continue;
				}

				out.AddTimeSignatureSegment( seg );
			}
		}

	}
	//valores por defecto si es que no existen
	if( out.m_TickSegments.size() < 1 )
	{
		TickSegment new_tick_seg;
		new_tick_seg.m_fStartBeat = 0.0f;
		new_tick_seg.m_iTickcount = 6;
		out.AddTickSegment( new_tick_seg );
	}
	if( out.m_ArrowSpacingSegments.size() < 1 )
	{
		ArrowSpacingSegment new_ArrowSpacing_seg;
		new_ArrowSpacing_seg.m_iStartRow = 0.0f;
		new_ArrowSpacing_seg.m_fArrowSpacing = 64.0f;
		out.AddArrowSpacingSegment( new_ArrowSpacing_seg );
	}
	if( out.m_ComboSegments.size() < 1 )
	{
		ComboSegment new_Combo_seg;
		new_Combo_seg.m_fStartBeat = 0.0f;
		new_Combo_seg.m_iComboFactor = 1;
		new_Combo_seg.m_iMissComboFactor = 1;
		out.AddComboSegment( new_Combo_seg );
	}
	if( out.m_NoteSkinSegments.size() < 1 )
	{
		RString sNS = "default";
		Trim(sNS);
		NoteSkinSegment new_NoteSkin_seg;
		new_NoteSkin_seg.m_iStartRow = 0;
		new_NoteSkin_seg.m_sNoteSkin = sNS;
		out.AddNoteSkinSegment( new_NoteSkin_seg );
	}
}




bool LoadFromBGChangesStringNXA( BackgroundChange &change, const RString &sBGChangeExpression )
{
	vector<RString> aBGChangeValues;
	split( sBGChangeExpression, "=", aBGChangeValues, false );

	aBGChangeValues.resize( min((int)aBGChangeValues.size(),11) );

	switch( aBGChangeValues.size() )
	{
	case 11:
		change.m_def.m_sColor2 = aBGChangeValues[10];
		// .sm files made before we started escaping will still have '^' instead of ','
		change.m_def.m_sColor2.Replace( '^', ',' );
		// fall through
	case 10:
		change.m_def.m_sColor1 = aBGChangeValues[9];
		// .sm files made before we started escaping will still have '^' instead of ','
		change.m_def.m_sColor1.Replace( '^', ',' );
		// fall through
	case 9:
		change.m_sTransition = aBGChangeValues[8];
		// fall through
	case 8:
		change.m_def.m_sFile2 = aBGChangeValues[7];
		// fall through
	case 7:
		change.m_def.m_sEffect = aBGChangeValues[6];
		// fall through
	case 6:
		// param 7 overrides this.
		// Backward compatibility:
		if( change.m_def.m_sEffect.empty() )
		{
			bool bLoop = atoi( aBGChangeValues[5] ) != 0;
			if( !bLoop )
				change.m_def.m_sEffect = SBE_StretchNoLoop;
		}
		// fall through
	case 5:
		// param 7 overrides this.
		// Backward compatibility:
		if( change.m_def.m_sEffect.empty() )
		{
			bool bRewindMovie = atoi( aBGChangeValues[4] ) != 0;
			if( bRewindMovie )
				change.m_def.m_sEffect = SBE_StretchRewind;
		}
		// fall through
	case 4:
		// param 9 overrides this.
		// Backward compatibility:
		if( change.m_sTransition.empty() )
			change.m_sTransition = (atoi( aBGChangeValues[3] ) != 0) ? "CrossFade" : "";
		// fall through
	case 3:
		change.m_fRate = StringToFloat( aBGChangeValues[2] );
		// fall through
	case 2:
		change.m_def.m_sFile1 = aBGChangeValues[1];
		// fall through
	case 1:
		change.m_fStartBeat = StringToFloat( aBGChangeValues[0] );
		// fall through
	}
	
	return aBGChangeValues.size() >= 2;
}

bool NXALoader::LoadFromNXAFile( const RString &sPath, Song &out, bool bFromCache )
{
	LOG->Trace( "Song::LoadFromNXAFile(%s)", sPath.c_str() );

	MsdFile msd;
	if( !msd.ReadFile( sPath, true ) )  // unescape
	{
		LOG->UserLog( "Song file", sPath, "couldn't be opened: %s", msd.GetError().c_str() );
		return false;
	}

	out.m_Timing.m_sFile = sPath;
	//LoadTimingFromSMFile( msd, out.m_Timing );
	out.m_bHasSongAttacks = false;	// no hay attacks (hasta ahora)

	//permite cargar los timings
	int state = NXAGETTING_SONG_INFO;
	Steps* pNewNotes = NULL;
	TimingData stepsTiming;
	//set on steptype especification
	//if is already invalid, the song hasn't split timing
	//TimingType tt = TimingType_Invalid;

	AttackArray attacks;

	for( unsigned i=0; i<msd.GetNumValues(); i++ )
	{
		const MsdFile::value_t &sParams = msd.GetValue(i);
		int iNumParams = msd.GetNumParams(i);
		RString sValueName = sParams[0];
		sValueName.MakeUpper();

		// handle the data
		/* Don't use GetMainAndSubTitlesFromFullTitle; that's only for heuristically
		 * splitting other formats that *don't* natively support #SUBTITLE. */
		switch (state)
		{
			case NXAGETTING_SONG_INFO:
			{
				if( sValueName=="TITLE" )
					out.m_sMainTitle = sParams[1];

				else if( sValueName=="SUBTITLE" )
					out.m_sSubTitle = sParams[1];

				else if( sValueName=="ARTIST" )
					out.m_sArtist = sParams[1];

				else if( sValueName=="TITLETRANSLIT" )
					out.m_sMainTitleTranslit = sParams[1];

				else if( sValueName=="SUBTITLETRANSLIT" )
					out.m_sSubTitleTranslit = sParams[1];

				else if( sValueName=="ARTISTTRANSLIT" )
					out.m_sArtistTranslit = sParams[1];

				else if( sValueName=="GENRE" )
					out.m_sGenre = sParams[1];

				else if( sValueName=="CREDIT" )
					out.m_sCredit = sParams[1];

				else if( sValueName=="BANNER" )
					out.m_sBannerFile = sParams[1];

				else if( sValueName=="BACKGROUND" )
					out.m_sBackgroundFile = sParams[1];

				/* Save "#LYRICS" for later, so we can add an internal lyrics tag. */
				else if( sValueName=="LYRICSPATH" )
					out.m_sLyricsFile = sParams[1];

				else if( sValueName=="CDTITLE" )
					out.m_sCDTitleFile = sParams[1];

				else if( sValueName=="MUSIC" )
					out.m_sMusicFile = sParams[1];

				else if( sValueName=="MILEAGECOST" )//cuanto mileage cuestan las special
				{
					out.m_iMileageCost = atoi( sParams[1] );
				}				
				else if( sValueName=="USEPIUSTOPS" )
				{
					if(!stricmp(sParams[1],"YES"))
				
						out.m_bUsePiuStops = true;
					else if(!stricmp(sParams[1],"NO"))
						out.m_bUsePiuStops = false;
					else
						LOG->UserLog( "Song file", sPath, "has an unknown #USEPIUSTOPS value, \"%s\"; ignored.", sParams[1].c_str() );
				}
				else if( sValueName=="HOLDSNOBODY" ) 
				{
					if(!stricmp(sParams[1],"YES"))
						out.m_bHoldsNoBody = true;
					else if(!stricmp(sParams[1],"NO"))
						out.m_bHoldsNoBody = false;
					else
						;
				}
				else if( sValueName=="CREDITSREQUIRED" )
				{
					out.m_iCreditsRequired = atoi( sParams[1] );
				}
				else if( sValueName=="SPECIALSONG" )
				{
					if(!stricmp(sParams[1],"YES"))
						out.m_bIsSpecialSong = true;
					else if(!stricmp(sParams[1],"NO"))
						out.m_bIsSpecialSong = false;
					else
						LOG->UserLog( "Song file", sPath, "has an unknown #SPECIALSONG value, \"%s\"; ignored.", sParams[1].c_str() );
				}
				else if( sValueName=="LOCKSTEPS")
				{
					out.m_sLockStepsString = sParams[1];
				}

				/*else if( sValueName=="INSTRUMENTTRACK" )
				{
					vector<RString> vs1;
					split( sParams[1], ",", vs1 );
					FOREACH_CONST( RString, vs1, s )
					{
						vector<RString> vs2;
						split( *s, "=", vs2 );
						if( vs2.size() >= 2 )
						{
							InstrumentTrack it = StringToInstrumentTrack( vs2[0] );
							if( it != InstrumentTrack_Invalid )
								out.m_sInstrumentTrackFile[it] = vs2[1];
						}
					}
				}
				*/

				else if( sValueName=="MUSICLENGTH" )
				{
					if( !bFromCache )
						continue;
					out.m_fMusicLengthSeconds = StringToFloat( sParams[1] );
				}
				else if( sValueName=="LASTBEATHINT" )
					out.m_fSpecifiedLastBeat = StringToFloat( sParams[1] );

				else if( sValueName=="MUSICBYTES" )
					; /* ignore */

				/* We calculate these.  Some SMs in circulation have bogus values for
				 * these, so make sure we always calculate it ourself. */
				else if( sValueName=="FIRSTBEAT" )
				{
					if( bFromCache )
						out.m_fFirstBeat = StringToFloat( sParams[1] );
				}
				else if( sValueName=="LASTBEAT" )
				{
					if( bFromCache )
						out.m_fLastBeat = StringToFloat( sParams[1] );
				}
				else if( sValueName=="SONGFILENAME" )
				{
					if( bFromCache )
						out.m_sSongFileName = sParams[1];
				}
				else if( sValueName=="HASMUSIC" )
				{
					if( bFromCache )
						out.m_bHasMusic = atoi( sParams[1] ) != 0;
				}
				else if( sValueName=="HASBANNER" )
				{
					if( bFromCache )
						out.m_bHasBanner = atoi( sParams[1] ) != 0;
				}

				else if( sValueName=="SAMPLESTART" )
					out.m_fMusicSampleStartSeconds = HHMMSSToSeconds( sParams[1] );

				else if( sValueName=="SAMPLELENGTH" )
					out.m_fMusicSampleLengthSeconds = HHMMSSToSeconds( sParams[1] );

				else if( sValueName=="DISPLAYBPM" )
				{
					// #DISPLAYBPM:[xxx][xxx:xxx]|[*]; 
					if( sParams[1] == "*" )
						out.m_DisplayBPMType = Song::DISPLAY_RANDOM;
					else 
					{
						out.m_DisplayBPMType = Song::DISPLAY_SPECIFIED;
						out.m_fSpecifiedBPMMin = StringToFloat( sParams[1] );
						if( sParams[2].empty() )
							out.m_fSpecifiedBPMMax = out.m_fSpecifiedBPMMin;
						else
							out.m_fSpecifiedBPMMax = StringToFloat( sParams[2] );
					}
				}

				else if( sValueName=="SELECTABLE" )
				{
					if(!stricmp(sParams[1],"YES"))
						out.m_SelectionDisplay = out.SHOW_ALWAYS;
					else if(!stricmp(sParams[1],"NO"))
						out.m_SelectionDisplay = out.SHOW_NEVER;
					//MODIFICADO POR MI
					else if( !stricmp( sParams[1], "MEMCARD" ) )
						out.m_SelectionDisplay = out.SHOW_MEMORYCARD;
					else
						LOG->UserLog( "Song file", sPath, "has an unknown #SELECTABLE value, \"%s\"; ignored.", sParams[1].c_str() );
				}

				else if( sValueName.Left(strlen("BGCHANGES"))=="BGCHANGES" || sValueName=="ANIMATIONS" )
				{
					BackgroundLayer iLayer = BACKGROUND_LAYER_1;
					//if( sscanf(sValueName, "BGCHANGES%d", &*ConvertValue<int>(&iLayer)) == 1 )
					//	enum_add(iLayer, -2);	// #BGCHANGES2 = BACKGROUND_LAYER_2
					if( sscanf(sValueName, "BGCHANGES%d", &*ConvertValue<int>(&iLayer)) == 1 )
						enum_add(iLayer, -1);	// #BGCHANGES2 = BACKGROUND_LAYER_3

					//LOG->Warn( "Layer for: %s, %d", sPath.c_str(), iLayer );
					bool bValid = iLayer>=0 && iLayer<NUM_BackgroundLayer;
					if( !bValid )
					{
						LOG->UserLog( "Song file", sPath, "has a #BGCHANGES tag \"%s\" that is out of range.", sValueName.c_str() );
					}
					else
					{
						vector<RString> aBGChangeExpressions;
						split( sParams[1], ",", aBGChangeExpressions );

						for( unsigned b=0; b<aBGChangeExpressions.size(); b++ )
						{
							BackgroundChange change;
							if( LoadFromBGChangesStringNXA( change, aBGChangeExpressions[b] ) )
								out.AddBackgroundChange( iLayer, change );
						}
					}
				}

				else if( sValueName=="FGCHANGES" )
				{
					vector<RString> aFGChangeExpressions;
					split( sParams[1], ",", aFGChangeExpressions );

					for( unsigned b=0; b<aFGChangeExpressions.size(); b++ )
					{
						BackgroundChange change;
						if( LoadFromBGChangesStringNXA( change, aFGChangeExpressions[b] ) )
							out.AddForegroundChange( change );
					}
				}
				//ATACKS
				else if( sValueName=="ATTACKS" )
				{
					Attack attack;
					float end = -9999;
					int iNumAttacks = 0;
					for( unsigned j = 1; j < sParams.params.size(); ++j )
					{
						vector<RString> sBits;
						split( sParams[j], "=", sBits, false );
						if( sBits.size() < 2 )
							continue;

						Trim( sBits[0] );
						if( !sBits[0].CompareNoCase("TIME") )
							attack.fStartSecond = max( StringToFloat(sBits[1]), 0.0f );
						else if( !sBits[0].CompareNoCase("LEN") )
							attack.fSecsRemaining = StringToFloat( sBits[1] );
						else if( !sBits[0].CompareNoCase("END") )
							end = StringToFloat( sBits[1] );
						else if( !sBits[0].CompareNoCase("MODS") )
						{
							attack.sModifiers = sBits[1];
							
							if( end != -9999 )
							{
								attack.fSecsRemaining = end - attack.fStartSecond;
								end = -9999;
							}

							if( attack.fSecsRemaining <= 0.0f)
							{
								LOG->UserLog( "Song file", sPath, "has an attack with a nonpositive length: %s", sBits[1].c_str() );
								attack.fSecsRemaining = 0.0f;
							}
							
							// warn on invalid so we catch typos on load
							//CourseUtil::WarnOnInvalidMods( attack.sModifiers );

							attacks.push_back( attack );
							//out.m_Attacks.push_back( attack );
							iNumAttacks++;

							if( iNumAttacks > 0 )
								out.m_bHasSongAttacks = true;
						}
						else
						{
							LOG->UserLog( "Song file", sPath, "has an unexpected value named '%s'", sBits[0].c_str() );
						}

						out.m_Attacks = attacks;
					}
				}
				else if( sValueName=="KEYSOUNDS" )
				{
					split( sParams[1], ",", out.m_vsKeysoundFile );
				}
				else if( sValueName=="OFFSET" )
				{
					out.m_Timing.m_fBeat0OffsetInSeconds = StringToFloat( sParams[1] );
				}
				/* Below are the song based timings that should only be used
				 * if the steps do not have their own timing. */
				else if( sValueName=="STOPS" )
				{
					vector<RString> arrayFreezeExpressions;
					split( sParams[1], ",", arrayFreezeExpressions );

					for( unsigned f=0; f<arrayFreezeExpressions.size(); f++ )
					{
						vector<RString> arrayFreezeValues;
						split( arrayFreezeExpressions[f], "=", arrayFreezeValues );
						if( arrayFreezeValues.size() != 2 )
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #STOPS value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}

						const float fFreezeBeat = StringToFloat( arrayFreezeValues[0] );
						const float fFreezeSeconds = StringToFloat( arrayFreezeValues[1] );
						
						StopSegment new_seg( BeatToNoteRow(fFreezeBeat), fFreezeSeconds );

		//				LOG->Trace( "Adding a freeze segment: beat: %f, seconds = %f", new_seg.m_fStartBeat, new_seg.m_fStopSeconds );

						out.AddStopSegment( new_seg );
					}
				}
				else if( sValueName=="DELAYS" )
				{
					vector<RString> arrayFreezeExpressions;
					split( sParams[1], ",", arrayFreezeExpressions );

					for( unsigned f=0; f<arrayFreezeExpressions.size(); f++ )
					{
						vector<RString> arrayFreezeValues;
						split( arrayFreezeExpressions[f], "=", arrayFreezeValues );
						if( arrayFreezeValues.size() != 2 )
						{							
							LOG->UserLog( "Song file", sPath, "has an unknown #DELAYS value, \"%s\"; ignored.", sParams[1].c_str() );							
							continue;
						}

						const float fFreezeBeat = StringToFloat( arrayFreezeValues[0] );
						const float fFreezeSeconds = StringToFloat( arrayFreezeValues[1] );
						
						StopSegment new_seg( BeatToNoteRow(fFreezeBeat), fFreezeSeconds, true );

						//LOG->Trace( "Adding a freeze segment: beat: %f, seconds = %f", new_seg.m_fStartBeat, new_seg.m_fStopSeconds );

						out.AddStopSegment( new_seg );
					}
					//delays = piustops
					//out.m_bUsePiuStops = true;
				}

				else if( sValueName=="BPMS" )
				{
					vector<RString> arrayBPMChangeExpressions;
					split( sParams[1], ",", arrayBPMChangeExpressions );

					for( unsigned b=0; b<arrayBPMChangeExpressions.size(); b++ )
					{
						vector<RString> arrayBPMChangeValues;
						split( arrayBPMChangeExpressions[b], "=", arrayBPMChangeValues );
						// XXX: Hard to tell which file caused this.
						if( arrayBPMChangeValues.size() != 2 )
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #BPMS value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayBPMChangeValues[0] );
						const float fNewBPM = StringToFloat( arrayBPMChangeValues[1] );
												
						out.AddBPMSegment( BPMSegment(BeatToNoteRow(fBeat), fNewBPM) );						
					}
				}				
				
				else if( sValueName=="WARPS" ) // Older versions allowed em here.
				{
					//ProcessWarps( out.m_SongTiming, sParams[1], out.m_fVersion );
					LOG->Trace( "Found WARPS tag" );
				}
				
				else if( sValueName=="FAKES" )
				{
					vector<RString> arrayFakeExpressions;
					split( sParams[1], ",", arrayFakeExpressions );
					
					for( unsigned b=0; b<arrayFakeExpressions.size(); b++ )
					{
						vector<RString> arrayFakeValues;
						split( arrayFakeExpressions[b], "=", arrayFakeValues );
						if( arrayFakeValues.size() != 2 )
						{
							LOG->UserLog("Song file",
								out.GetSongFilePath(),
									 "has an invalid #FAKES value \"%s\" (must have exactly one '='), ignored.",
									 arrayFakeExpressions[b].c_str() );
							continue;
						}
						
						const float fBeat = StringToFloat( arrayFakeValues[0] );
						const float fNewBeat = StringToFloat( arrayFakeValues[1] );
						
						if(fNewBeat > 0)							
							out.m_Timing.AddFakeSegment( FakeSegment(BeatToNoteRow(fBeat), fNewBeat) );						
						else
						{
							LOG->UserLog("Song file",
								out.GetSongFilePath(),
									 "has an invalid Fake at beat %f, BPM %f.",
									 fBeat, fNewBeat );
						}
					}
				}

				else if( sValueName=="TIMESIGNATURES" )
				{
					vector<RString> vs1;
					split( sParams[1], ",", vs1 );

					FOREACH_CONST( RString, vs1, s1 )
					{
						vector<RString> vs2;
						split( *s1, "=", vs2 );

						if( vs2.size() < 3 )
						{
							LOG->UserLog( "Song file", sPath, "has an invalid time signature change with %i values.", (int)vs2.size() );
							continue;
						}

						const float fBeat = StringToFloat( vs2[0] );

						TimeSignatureSegment seg;
						seg.m_iStartRow = BeatToNoteRow(fBeat);
						seg.m_iNumerator = atoi( vs2[1] ); 
						seg.m_iDenominator = atoi( vs2[2] ); 
						
						if( fBeat < 0 )
						{
							LOG->UserLog( "Song file", sPath, "has an invalid time signature change with beat %f.", fBeat );
							continue;
						}
						
						if( seg.m_iNumerator < 1 )
						{
							LOG->UserLog( "Song file", sPath, "has an invalid time signature change with beat %f, iNumerator %i.", fBeat, seg.m_iNumerator );
							continue;
						}

						if( seg.m_iDenominator < 1 )
						{
							LOG->UserLog( "Song file", sPath, "has an invalid time signature change with beat %f, iDenominator %i.", fBeat, seg.m_iDenominator );
							continue;
						}

						out.m_Timing.AddTimeSignatureSegment( seg );
					}
				}

				else if( sValueName=="TICKCOUNTS" )
				{
					vector<RString> arrayTickChangeExpressions;
					split( sParams[1], ",", arrayTickChangeExpressions );

					for( unsigned b=0; b<arrayTickChangeExpressions.size(); b++ )
					{
						vector<RString> arrayTickChangeValues;
						split( arrayTickChangeExpressions[b], "=", arrayTickChangeValues );
						// XXX: Hard to tell which file caused this.
						if( arrayTickChangeValues.size() != 2 )
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #TICKCOUNTS value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayTickChangeValues[0] );
						const float fNewTickcount = StringToFloat( arrayTickChangeValues[1] );
						
						if( fNewTickcount > -1.0f )
							out.AddTickSegment( TickSegment(BeatToNoteRow(fBeat), fNewTickcount) );
						else
							LOG->UserLog( "Song file", sPath, "has an invalid tickcount change at beat %f, tickcount %f.", fBeat, fNewTickcount );
					}
				}
				//tag speeds, tiene: beat=[0,1]velocidad=approach=?? (siempre 0)
				else if( sValueName=="ARROWSPEEDS" )
				{
					vector<RString> arrayArrowSpacingChangeExpressions;
					split( sParams[1], ",", arrayArrowSpacingChangeExpressions );

					for( unsigned b=0; b<arrayArrowSpacingChangeExpressions.size(); b++ )
					{
						vector<RString> arrayArrowSpacingChangeValues;
						split( arrayArrowSpacingChangeExpressions[b], "=", arrayArrowSpacingChangeValues );

						float fNewFactor = 0.5f;
						// XXX: Hard to tell which file caused this.
						if( arrayArrowSpacingChangeValues.size() == 2 )
						{
							;
						}
						else if( arrayArrowSpacingChangeValues.size() > 2 )
						{
							fNewFactor = StringToFloat( arrayArrowSpacingChangeValues[2] );
						}
						else
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #SCROLLS value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayArrowSpacingChangeValues[0] );
						const float fNewArrowSpacing = StringToFloat( arrayArrowSpacingChangeValues[1] );

						float fNew = fNewArrowSpacing;
						//LOG->Trace( "Scaling ArrowSpacing: %.2f to %.2f", fNewArrowSpacing, fNew );
						
						if( fNew >= 0.0f )
							out.AddArrowSpacingSegment( ArrowSpacingSegment(BeatToNoteRow(fBeat), fNew, fNewFactor) );
						else
							LOG->UserLog( "Song file", sPath, "has an invalid speed change at beat %f, speed %f.", fBeat, fNewArrowSpacing );
					}
				}

				else if( sValueName=="SPEEDS" )
				{
					vector<RString> arraySpeedChangeExpressions;
					split( sParams[1], ",", arraySpeedChangeExpressions );

					for( unsigned b=0; b<arraySpeedChangeExpressions.size(); b++ )
					{
						vector<RString> arraySpeedChangeValues;
						split( arraySpeedChangeExpressions[b], "=", arraySpeedChangeValues );						
					
						float fWait = 0;
						int iUnit = 0;
						if( arraySpeedChangeValues.size() < 2 )
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #SPEEDS value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;							
						}

						if( arraySpeedChangeValues.size() == 4 )
						{
							fWait = StringToFloat( arraySpeedChangeValues[2] );
							iUnit = StringToFloat( arraySpeedChangeValues[3] );
						}

						const float fBeat = StringToFloat( arraySpeedChangeValues[0] );
						const float fNewSpeed = StringToFloat( arraySpeedChangeValues[1] );							
						
						//if( fNewSpeed >= 0.0f )							
							out.m_Timing.AddSpeedSegment( SpeedSegment(BeatToNoteRow(fBeat), fNewSpeed, fWait, iUnit ) );				
						//else
						//	LOG->UserLog( "Song file", sPath, "has an invalid Scroll change at beat %f, speed %f.", fBeat, fNewSpeed );
					}
				}
				
				else if( sValueName=="SCROLLS" )
				{
					//ProcessScrolls( stepsTiming, sParams[1] );
					//LOG->Trace( "Found SCROLLS Combos tag" );
					vector<RString> arrayNewScrollChangeExpressions;
					split( sParams[1], ",", arrayNewScrollChangeExpressions );

					for( unsigned b=0; b<arrayNewScrollChangeExpressions.size(); b++ )
					{
						vector<RString> arrayNewScrollChangeValues;
						split( arrayNewScrollChangeExpressions[b], "=", arrayNewScrollChangeValues );
						if( arrayNewScrollChangeValues.size() != 2 )					
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #SCROLLS value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayNewScrollChangeValues[0] );
						const float fNewScroll = StringToFloat( arrayNewScrollChangeValues[1] );				
						
						if( fNewScroll >= 0.0f )							
							out.m_Timing.AddScrollSegment( ScrollSegment(BeatToNoteRow(fBeat), fNewScroll) );									
						else
							LOG->UserLog( "Song file", sPath, "has an invalid speed change at beat %f, speed %f.", fBeat, fNewScroll );
					}
				}

				else if( sValueName=="COMBOS" )
				{
					vector<RString> arrayComboExpressions;
					split( sParams[1], ",", arrayComboExpressions );
					
					for( unsigned f=0; f<arrayComboExpressions.size(); f++ )
					{
						vector<RString> arrayComboValues;
						split( arrayComboExpressions[f], "=", arrayComboValues );
						unsigned size = arrayComboValues.size();
						if( size < 2 )
						{
							LOG->UserLog("Song file",
									sPath,
									 "has an invalid #COMBOS value \"%s\" (must have at least one '='), ignored.",
									 arrayComboExpressions[f].c_str() );
							continue;
						}
						const float fComboBeat = StringToFloat( arrayComboValues[0] );
						const int iCombos = StringToInt( arrayComboValues[1] );
						const int iMisses = (size == 2 ? iCombos : StringToInt(arrayComboValues[2]));
						out.AddComboSegment( ComboSegment( fComboBeat, iCombos, iMisses ) );
					}
				}
				else if( sValueName=="NOTESKINCHANGES")
				{
					vector<RString> arrayNoteSkinChangeExpressions;
					split( sParams[1], ",", arrayNoteSkinChangeExpressions );

					for( unsigned b=0; b<arrayNoteSkinChangeExpressions.size(); b++ )
					{
						vector<RString> arrayNoteSkinChangeValues;
						split( arrayNoteSkinChangeExpressions[b], "=", arrayNoteSkinChangeValues );
						// XXX: Hard to tell which file caused this.
						if( arrayNoteSkinChangeValues.size() != 2 )
						{
							//LOG->UserLog( "Song file", "%s", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
							//	      sValueName.c_str(), arrayArrowSpacingChangeExpressions[b].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayNoteSkinChangeValues[0] );
						RString sNewNoteSkin = arrayNoteSkinChangeValues[1];
						
						if( !sNewNoteSkin.empty() ) 
						{
							Trim(sNewNoteSkin);
							out.m_Timing.AddNoteSkinSegment( NoteSkinSegment(BeatToNoteRow(fBeat), sNewNoteSkin ) );
						}
						//else
							//LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid speed change at beat %f, speed %f.", fBeat, fNewArrowSpacing );
					}
				}
				else if( sValueName=="FIRSTBEAT" )
				{
					if( bFromCache )
						out.m_fFirstBeat = StringToFloat( sParams[1] );
				}
				else if( sValueName=="LASTBEAT" )
				{
					if( bFromCache )
						out.m_fLastBeat = StringToFloat( sParams[1] );
				}
				else if( sValueName=="SONGFILENAME" )
				{
					if( bFromCache )
						out.m_sSongFileName = sParams[1];
				}
				else if( sValueName=="HASMUSIC" )
				{
					if( bFromCache )
						out.m_bHasMusic = atoi( sParams[1] ) != 0;
				}
				else if( sValueName=="HASBANNER" )
				{
					if( bFromCache )
						out.m_bHasBanner = atoi( sParams[1] ) != 0;
				}
				else if( sValueName=="MUSICLENGTH" )
				{
					if( !bFromCache )
						continue;
					out.m_fMusicLengthSeconds = StringToFloat( sParams[1] );
				}

				// This tag will get us to the next section.
				else if( sValueName=="NOTEDATA" )
				{
					state = NXAGETTING_STEP_INFO;
					pNewNotes = new Steps();
					pNewNotes->SetFilename( sPath );

					pNewNotes->m_StepsTiming = TimingData();
					stepsTiming = TimingData();
					stepsTiming.m_sFile = sPath;
				}
				break;				
			}//case
			case NXAGETTING_STEP_INFO:
			{
				if( sValueName=="NOTES" || sValueName=="NOTES2" )
				{
					state = NXAGETTING_SONG_INFO;

					if( iNumParams < 7 )
					{
						LOG->UserLog( "Song file", sPath, "has %d fields in a #NOTES tag, but should have at least 7.", iNumParams );
						continue;
					}

					//Steps* pNewNotes = new Steps;
					LoadFromSMTokens( 
						sParams[1], 
						sParams[2], 
						sParams[3], 
						sParams[4], 
						sParams[5], 
						sParams[6],
						*pNewNotes );

					//valores por defecto si es que no existen
					if( stepsTiming.m_TickSegments.size() < 1 )
					{
						TickSegment new_tick_seg;
						new_tick_seg.m_fStartBeat = 0.0f;
						new_tick_seg.m_iTickcount = 6;
						stepsTiming.AddTickSegment( new_tick_seg );
					}
					if( stepsTiming.m_ArrowSpacingSegments.size() < 1 )
					{
						ArrowSpacingSegment new_ArrowSpacing_seg;
						new_ArrowSpacing_seg.m_iStartRow = 0;
						new_ArrowSpacing_seg.m_fArrowSpacing = 64.0f;
						stepsTiming.AddArrowSpacingSegment( new_ArrowSpacing_seg );
					}
					if( stepsTiming.m_ComboSegments.size() < 1 )
					{
						ComboSegment new_Combo_seg;
						new_Combo_seg.m_fStartBeat = 0.0f;
						new_Combo_seg.m_iComboFactor = 1;
						new_Combo_seg.m_iMissComboFactor = 1;
						stepsTiming.AddComboSegment( new_Combo_seg );
					}
					if( stepsTiming.m_NoteSkinSegments.size() < 1 )
					{
						RString sNS = "default";
						Trim(sNS);
						NoteSkinSegment new_NoteSkin_seg;
						new_NoteSkin_seg.m_iStartRow = 0;
						new_NoteSkin_seg.m_sNoteSkin = sNS;
						stepsTiming.AddNoteSkinSegment( new_NoteSkin_seg );
					}
					if( stepsTiming.m_ScrollSegments.size() < 1 )
					{					
						ScrollSegment new_Scroll_seg;
						new_Scroll_seg.m_iStartRow = 0.0f;
						new_Scroll_seg.m_fRatio = 1.0f;
						stepsTiming.AddScrollSegment( new_Scroll_seg );
					}
					if( stepsTiming.m_vTimeSignatureSegments.size() < 1 )
					{
						TimeSignatureSegment new_Time_seg;
						new_Time_seg.m_iStartRow = 0;
						new_Time_seg.m_iNumerator = 4;
						new_Time_seg.m_iDenominator = 4;
						stepsTiming.AddTimeSignatureSegment( new_Time_seg );
					}
					if( stepsTiming.m_SpeedSegments.size() < 1 )
					{
						SpeedSegment new_Speed_seg;
						new_Speed_seg.m_iStartRow = 0;
						new_Speed_seg.m_fRatio = 1;
						new_Speed_seg.m_fWait = 0;
						new_Speed_seg.m_iUnit = 0;
						stepsTiming.AddSpeedSegment( new_Speed_seg );
					}
					if( stepsTiming.m_LabelSegments.size() < 1 )
					{
						LabelSegment new_Label_seg;
						new_Label_seg.m_iStartRow = 0;
						new_Label_seg.m_sLabel = "label1";
						stepsTiming.AddLabelSegment( new_Label_seg );
					}

					if( pNewNotes->m_bUsePiuStops )					
						for( unsigned i = 0; i < stepsTiming.m_StopSegments.size(); i++ )						
							stepsTiming.m_StopSegments[i].m_bDelay = true;				
					
					pNewNotes->m_StepsTiming = stepsTiming;
					out.AddSteps( pNewNotes );
				}
				
				else if( sValueName=="BPMS" )
				{
					vector<RString> arrayBPMChangeExpressions;
					split( sParams[1], ",", arrayBPMChangeExpressions );

					for( unsigned b=0; b<arrayBPMChangeExpressions.size(); b++ )
					{
						vector<RString> arrayBPMChangeValues;
						split( arrayBPMChangeExpressions[b], "=", arrayBPMChangeValues );
						// XXX: Hard to tell which file caused this.
						if( arrayBPMChangeValues.size() != 2 )
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #BPMS value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayBPMChangeValues[0] );
						const float fNewBPM = StringToFloat( arrayBPMChangeValues[1] );
												
						
						stepsTiming.AddBPMSegment( BPMSegment(BeatToNoteRow(fBeat), fNewBPM) );											
					}
				}

				else if( sValueName=="COUNTNOTESSEPARATELY" )
				{
					vector<RString> arrayCountExpressions;
					split( sParams[1], ",", arrayCountExpressions );
					
					for( unsigned f=0; f<arrayCountExpressions.size(); f++ )
					{
						vector<RString> arrayCountValues;
						split( arrayCountExpressions[f], "=", arrayCountValues );
						unsigned size = arrayCountValues.size();
						if( size != 2 )
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #COUNTNOTESSEPARATELY value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}
						const float fCountBeat = StringToFloat( arrayCountValues[0] );
						const int iCount = StringToInt( arrayCountValues[1] );
						stepsTiming.AddCountSepSegment( CountSeparatelySegment( BeatToNoteRow(fCountBeat), iCount ) );
					}
				}

				else if( sValueName=="REQUIRESTEPONHOLDHEADS" )
				{
					vector<RString> arrayRequireExpressions;
					split( sParams[1], ",", arrayRequireExpressions );
					
					for( unsigned f=0; f<arrayRequireExpressions.size(); f++ )
					{
						vector<RString> arrayRequireValues;
						split( arrayRequireExpressions[f], "=", arrayRequireValues );
						unsigned size = arrayRequireValues.size();
						if( size != 2 )
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #REQUIRESTEPONHOLDHEADS value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}
						const float fRequireBeat = StringToFloat( arrayRequireValues[0] );
						const int iRequire = StringToInt( arrayRequireValues[1] );
						stepsTiming.AddReqHoldHeadSegment( RequireStepOnHoldHeadSegment( BeatToNoteRow(fRequireBeat), iRequire ) );
					}
				}
				
				else if( sValueName=="STOPS" )
				{
					vector<RString> arrayFreezeExpressions;
					split( sParams[1], ",", arrayFreezeExpressions );

					for( unsigned f=0; f<arrayFreezeExpressions.size(); f++ )
					{
						vector<RString> arrayFreezeValues;
						split( arrayFreezeExpressions[f], "=", arrayFreezeValues );
						if( arrayFreezeValues.size() != 2 )
						{
							// XXX: Hard to tell which file caused this.
							LOG->UserLog( "Song file", sPath, "has an unknown #STOPS value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}

						const float fFreezeBeat = StringToFloat( arrayFreezeValues[0] );
						const float fFreezeSeconds = StringToFloat( arrayFreezeValues[1] );
						
						StopSegment new_seg( BeatToNoteRow(fFreezeBeat), fFreezeSeconds );

		//				LOG->Trace( "Adding a freeze segment: beat: %f, seconds = %f", new_seg.m_fStartBeat, new_seg.m_fStopSeconds );

						stepsTiming.AddStopSegment( new_seg );					
					}
				}
				
				else if( sValueName=="DELAYS" )
				{
					vector<RString> arrayFreezeExpressions;
					split( sParams[1], ",", arrayFreezeExpressions );

					for( unsigned f=0; f<arrayFreezeExpressions.size(); f++ )
					{
						vector<RString> arrayFreezeValues;
						split( arrayFreezeExpressions[f], "=", arrayFreezeValues );
						if( arrayFreezeValues.size() != 2 )
						{
							// XXX: Hard to tell which file caused this.
							LOG->UserLog( "Song file", sPath, "has an unknown #DELAYS value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}

						const float fFreezeBeat = StringToFloat( arrayFreezeValues[0] );
						const float fFreezeSeconds = StringToFloat( arrayFreezeValues[1] );
						
						StopSegment new_seg( BeatToNoteRow(fFreezeBeat), fFreezeSeconds, true );

		//				LOG->Trace( "Adding a freeze segment: beat: %f, seconds = %f", new_seg.m_fStartBeat, new_seg.m_fStopSeconds );

						stepsTiming.AddStopSegment( new_seg );					
					}
				}
				
				else if( sValueName=="TIMESIGNATURES" )
				{
					vector<RString> vs1;
					split( sParams[1], ",", vs1 );

					FOREACH_CONST( RString, vs1, s1 )
					{
						vector<RString> vs2;
						split( *s1, "=", vs2 );

						if( vs2.size() < 3 )
						{
							LOG->UserLog( "Song file", sPath, "has an invalid time signature change with %i values.", (int)vs2.size() );
							continue;
						}

						const float fBeat = StringToFloat( vs2[0] );

						TimeSignatureSegment seg;
						seg.m_iStartRow = BeatToNoteRow(fBeat);
						seg.m_iNumerator = atoi( vs2[1] ); 
						seg.m_iDenominator = atoi( vs2[2] ); 
						
						if( fBeat < 0 )
						{
							LOG->UserLog( "Song file", sPath, "has an invalid time signature change with beat %f.", fBeat );
							continue;
						}
						
						if( seg.m_iNumerator < 1 )
						{
							LOG->UserLog( "Song file", sPath, "has an invalid time signature change with beat %f, iNumerator %i.", fBeat, seg.m_iNumerator );
							continue;
						}

						if( seg.m_iDenominator < 1 )
						{
							LOG->UserLog( "Song file", sPath, "has an invalid time signature change with beat %f, iDenominator %i.", fBeat, seg.m_iDenominator );
							continue;
						}
		
						stepsTiming.AddTimeSignatureSegment( seg );			
					}
				}
				
				else if( sValueName=="TICKCOUNTS" )
				{
					vector<RString> arrayTickChangeExpressions;
					split( sParams[1], ",", arrayTickChangeExpressions );

					for( unsigned b=0; b<arrayTickChangeExpressions.size(); b++ )
					{
						vector<RString> arrayTickChangeValues;
						split( arrayTickChangeExpressions[b], "=", arrayTickChangeValues );
						// XXX: Hard to tell which file caused this.
						if( arrayTickChangeValues.size() != 2 )
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #TICKCOUNTS value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayTickChangeValues[0] );
						const float fNewTickcount = StringToFloat( arrayTickChangeValues[1] );
						
						if( fNewTickcount > -1.0f )
						{							
							stepsTiming.AddTickSegment( TickSegment(BeatToNoteRow(fBeat), fNewTickcount) );				
						}
						else
							LOG->UserLog( "Song file", sPath, "has an invalid tickcount change at beat %f, tickcount %f.", fBeat, fNewTickcount );
					}
				}
				
				else if( sValueName=="COMBOS" )
				{
					vector<RString> arrayComboExpressions;
					split( sParams[1], ",", arrayComboExpressions );
					
					for( unsigned f=0; f<arrayComboExpressions.size(); f++ )
					{
						vector<RString> arrayComboValues;
						split( arrayComboExpressions[f], "=", arrayComboValues );
						unsigned size = arrayComboValues.size();
						if( size < 2 )
						{
							LOG->UserLog("Song file",
									sPath,
									 "has an invalid #COMBOS value \"%s\" (must have at least one '='), ignored.",
									 arrayComboExpressions[f].c_str() );
							continue;
						}
						const float fComboBeat = StringToFloat( arrayComboValues[0] );
						const int iCombos = StringToInt( arrayComboValues[1] );
						const int iMisses = (size == 2 ? iCombos : StringToInt(arrayComboValues[2]));
						stepsTiming.AddComboSegment( ComboSegment( fComboBeat, iCombos, iMisses ) );
					}
				}
				
				else if( sValueName=="SPEEDS" )
				{
					vector<RString> arraySpeedChangeExpressions;
					split( sParams[1], ",", arraySpeedChangeExpressions );

					for( unsigned b=0; b<arraySpeedChangeExpressions.size(); b++ )
					{
						vector<RString> arraySpeedChangeValues;
						split( arraySpeedChangeExpressions[b], "=", arraySpeedChangeValues );						
					
						float fWait = 0;
						int iUnit = 0;
						if( arraySpeedChangeValues.size() < 2 )
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #SPEEDS value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;							
						}

						if( arraySpeedChangeValues.size() == 4 )
						{
							fWait = StringToFloat( arraySpeedChangeValues[2] );
							iUnit = StringToFloat( arraySpeedChangeValues[3] );
						}

						const float fBeat = StringToFloat( arraySpeedChangeValues[0] );
						const float fNewSpeed = StringToFloat( arraySpeedChangeValues[1] );	

						//LOG->Trace("SPEEDSS: %f, %f, %f, %d", fBeat, fNewSpeed, fWait, iUnit);
						
						//if( fNewSpeed >= 0.0f )							
							stepsTiming.AddSpeedSegment( SpeedSegment(BeatToNoteRow(fBeat), fNewSpeed, fWait, iUnit ) );				
						//else
						//	LOG->UserLog( "Song file", sPath, "has an invalid Speed change at beat %f, speed %f.", fBeat, fNewSpeed );
					}
				}
				
				else if( sValueName=="SCROLLS" )
				{
					//ProcessScrolls( stepsTiming, sParams[1] );
					//LOG->Trace( "Found SCROLLS Combos tag" );
					vector<RString> arrayNewScrollChangeExpressions;
					split( sParams[1], ",", arrayNewScrollChangeExpressions );

					for( unsigned b=0; b<arrayNewScrollChangeExpressions.size(); b++ )
					{
						vector<RString> arrayNewScrollChangeValues;
						split( arrayNewScrollChangeExpressions[b], "=", arrayNewScrollChangeValues );
						if( arrayNewScrollChangeValues.size() != 2 )					
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #SCROLLS value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayNewScrollChangeValues[0] );
						const float fNewScroll = StringToFloat( arrayNewScrollChangeValues[1] );				
						
						if( fNewScroll >= 0.0f )							
							stepsTiming.AddScrollSegment( ScrollSegment(BeatToNoteRow(fBeat), fNewScroll) );									
						else
							LOG->UserLog( "Song file", sPath, "has an invalid speed change at beat %f, speed %f.", fBeat, fNewScroll );
					}
				}
				
				else if( sValueName=="ARROWSPEEDS" )
				{
					//ProcessScrolls( stepsTiming, sParams[1] );
					//LOG->Trace( "Found SCROLLS Combos tag" );
					vector<RString> arrayArrowSpacingChangeExpressions;
					split( sParams[1], ",", arrayArrowSpacingChangeExpressions );

					for( unsigned b=0; b<arrayArrowSpacingChangeExpressions.size(); b++ )
					{
						vector<RString> arrayArrowSpacingChangeValues;
						split( arrayArrowSpacingChangeExpressions[b], "=", arrayArrowSpacingChangeValues );
						float fNewFactor = 0.5f;
						// XXX: Hard to tell which file caused this.
						if( arrayArrowSpacingChangeValues.size() == 2 )
						{
							;
						}
						else if( arrayArrowSpacingChangeValues.size() > 2 )
						{
							fNewFactor = StringToFloat( arrayArrowSpacingChangeValues[2] );
						}
						else
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #SCROLLS value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayArrowSpacingChangeValues[0] );
						const float fNewArrowSpacing = StringToFloat( arrayArrowSpacingChangeValues[1] );

						float fNew = fNewArrowSpacing;						
						
						if( fNew >= 0.0f )							
							stepsTiming.AddArrowSpacingSegment( ArrowSpacingSegment(BeatToNoteRow(fBeat), fNew, fNewFactor) );									
						else
							LOG->UserLog( "Song file", sPath, "has an invalid speed change at beat %f, speed %f.", fBeat, fNewArrowSpacing );
					}
				}
				
				else if( sValueName=="FAKES" )
				{
					vector<RString> arrayFakeExpressions;
					split( sParams[1], ",", arrayFakeExpressions );
					
					for( unsigned b=0; b<arrayFakeExpressions.size(); b++ )
					{
						vector<RString> arrayFakeValues;
						split( arrayFakeExpressions[b], "=", arrayFakeValues );
						if( arrayFakeValues.size() != 2 )
						{
							LOG->UserLog("Song file",
								out.GetSongFilePath(),
									 "has an invalid #FAKES value \"%s\" (must have exactly one '='), ignored.",
									 arrayFakeExpressions[b].c_str() );
							continue;
						}
						
						const float fBeat = StringToFloat( arrayFakeValues[0] );
						const float fNewBeat = StringToFloat( arrayFakeValues[1] );
						
						if(fNewBeat > 0)	
						{
							stepsTiming.AddFakeSegment( FakeSegment(fBeat, fNewBeat) );																		
						}
						else
						{
							LOG->UserLog("Song file",
								out.GetSongFilePath(),
									 "has an invalid Fake at beat %f, lenght %f.",
									 fBeat, fNewBeat );
						}
					}
				}
				else if( sValueName=="NOTESKINCHANGES")
				{
					vector<RString> arrayNoteSkinChangeExpressions;
					split( sParams[1], ",", arrayNoteSkinChangeExpressions );

					for( unsigned b=0; b<arrayNoteSkinChangeExpressions.size(); b++ )
					{
						vector<RString> arrayNoteSkinChangeValues;
						split( arrayNoteSkinChangeExpressions[b], "=", arrayNoteSkinChangeValues );
						// XXX: Hard to tell which file caused this.
						if( arrayNoteSkinChangeValues.size() != 2 )
						{
							//LOG->UserLog( "Song file", "%s", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
							//	      sValueName.c_str(), arrayArrowSpacingChangeExpressions[b].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayNoteSkinChangeValues[0] );
						RString sNewNoteSkin = arrayNoteSkinChangeValues[1];
						
						if( !sNewNoteSkin.empty() ) 
						{
							Trim(sNewNoteSkin);
							stepsTiming.AddNoteSkinSegment( NoteSkinSegment(BeatToNoteRow(fBeat), sNewNoteSkin ) );
						}
						//else
							//LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid speed change at beat %f, speed %f.", fBeat, fNewArrowSpacing );
					}
				}

				else if( sValueName=="LABELS")
				{
					vector<RString> arrayLabelChangeExpressions;
					split( sParams[1], ",", arrayLabelChangeExpressions );

					for( unsigned b=0; b<arrayLabelChangeExpressions.size(); b++ )
					{
						vector<RString> arrayLabelChangeValues;
						split( arrayLabelChangeExpressions[b], "=", arrayLabelChangeValues );
						// XXX: Hard to tell which file caused this.
						if( arrayLabelChangeValues.size() != 2 )
						{
							//LOG->UserLog( "Song file", "%s", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
							//	      sValueName.c_str(), arrayArrowSpacingChangeExpressions[b].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayLabelChangeValues[0] );
						RString sNewLabel = arrayLabelChangeValues[1];
						
						if( !sNewLabel.empty() ) 
						{
							stepsTiming.AddLabelSegment( LabelSegment(BeatToNoteRow(fBeat), sNewLabel ) );
						}
						//else
							//LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid speed change at beat %f, speed %f.", fBeat, fNewArrowSpacing );
					}
				}
				
				else if( sValueName=="WARPS" )
				{
					vector<RString> arrayWarpExpressions;
					split( sParams[1], ",", arrayWarpExpressions );
					
					for( unsigned b=0; b<arrayWarpExpressions.size(); b++ )
					{
						vector<RString> arrayWarpValues;
						split( arrayWarpExpressions[b], "=", arrayWarpValues );
						if( arrayWarpValues.size() != 2 )
						{
							LOG->UserLog("Song file",
								out.GetSongFilePath(),
									 "has an invalid #WARPS value \"%s\" (must have exactly one '='), ignored.",
									 arrayWarpExpressions[b].c_str() );
							continue;
						}
						
						const float fBeat = StringToFloat( arrayWarpValues[0] );
						const float fNewBeat = StringToFloat( arrayWarpValues[1] );
						
						if(fNewBeat > 0)	
						{
							stepsTiming.AddWarpSegment( WarpSegment(fBeat, fNewBeat) );																		
						}
						else
						{
							LOG->UserLog("Song file",
								out.GetSongFilePath(),
									 "has an invalid Warp at beat %f, lenght %f.",
									 fBeat, fNewBeat );
						}
					}
				}
				
				else if( sValueName=="ATTACKS" )
				{
					//ProcessAttackString(pNewNotes->m_sAttackString, sParams);
					//ProcessAttacks(pNewNotes->m_Attacks, sParams);
					Attack attack;
					float end = -9999;
					int iNumAttacks = 0;
					for( unsigned j = 1; j < sParams.params.size(); ++j )
					{
						vector<RString> sBits;
						split( sParams[j], "=", sBits, false );
						if( sBits.size() < 2 )
							continue;

						Trim( sBits[0] );
						if( !sBits[0].CompareNoCase("TIME") )
							attack.fStartSecond = max( StringToFloat(sBits[1]), 0.0f );
						else if( !sBits[0].CompareNoCase("LEN") )
							attack.fSecsRemaining = StringToFloat( sBits[1] );
						else if( !sBits[0].CompareNoCase("END") )
							end = StringToFloat( sBits[1] );
						else if( !sBits[0].CompareNoCase("MODS") )
						{
							attack.sModifiers = sBits[1];
							
							if( end != -9999 )
							{
								attack.fSecsRemaining = end - attack.fStartSecond;
								end = -9999;
							}

							if( attack.fSecsRemaining <= 0.0f)
							{
								LOG->UserLog( "Song file", sPath, "has an attack with a nonpositive length: %s", sBits[1].c_str() );
								attack.fSecsRemaining = 0.0f;
							}
							
							// warn on invalid so we catch typos on load
							//CourseUtil::WarnOnInvalidMods( attack.sModifiers );

							attacks.push_back( attack );
							//out.m_Attacks.push_back( attack );
							iNumAttacks++;

							if( iNumAttacks > 0 )
								pNewNotes->m_bHasAttacks = true;
						}
						else
						{
							LOG->UserLog( "Song file", sPath, "has an unexpected value named '%s'", sBits[0].c_str() );
						}

						pNewNotes->m_Attacks = attacks;
					}
				}
				
				else if( sValueName=="OFFSET" )
				{
					//stepsTiming.m_fBeat0OffsetInSeconds = StringToFloat( sParams[1] );					
					stepsTiming.m_fBeat0OffsetInSeconds = StringToFloat( sParams[1] );
				}
				
				//para los steps que no tengan timingtype
				else if( sValueName=="USEPIUSTOPS" )
				{
					if(!stricmp(sParams[1],"YES"))				
						pNewNotes->m_bUsePiuStops = true;
					else if(!stricmp(sParams[1],"NO"))
						pNewNotes->m_bUsePiuStops = false;
					else
						LOG->UserLog( "Song file", sPath, "has an unknown #USEPIUSTOPS value, \"%s\"; ignored.", sParams[1].c_str() );
				}
				else if( sValueName=="HOLDSNOBODY" ) 
				{
					if(!stricmp(sParams[1],"YES"))
						pNewNotes->m_bHoldsNoBody = true;
					else if(!stricmp(sParams[1],"NO"))
						pNewNotes->m_bHoldsNoBody = false;
					else
						LOG->UserLog( "Song file", sPath, "has an unknown #HOLDSNOBODY value, \"%s\"; ignored.", sParams[1].c_str() );
				}

				break;
			}
		}
	}

	//valores por defecto si es que no existen
	if( out.m_Timing.m_TickSegments.size() < 1 )
	{
		TickSegment new_tick_seg;
		new_tick_seg.m_fStartBeat = 0.0f;
		new_tick_seg.m_iTickcount = 6;
		out.m_Timing.AddTickSegment( new_tick_seg );
	}
	if( out.m_Timing.m_ArrowSpacingSegments.size() < 1 )
	{
		ArrowSpacingSegment new_ArrowSpacing_seg;
		new_ArrowSpacing_seg.m_iStartRow = 0;
		new_ArrowSpacing_seg.m_fArrowSpacing = 64.0f;
		out.m_Timing.AddArrowSpacingSegment( new_ArrowSpacing_seg );
	}
	if( out.m_Timing.m_ComboSegments.size() < 1 )
	{
		ComboSegment new_Combo_seg;
		new_Combo_seg.m_fStartBeat = 0.0f;
		new_Combo_seg.m_iComboFactor = 1;
		new_Combo_seg.m_iMissComboFactor = 1;
		out.m_Timing.AddComboSegment( new_Combo_seg );
	}
	if( out.m_Timing.m_NoteSkinSegments.size() < 1 )
	{
		RString sNS = "default";
		Trim(sNS);
		NoteSkinSegment new_NoteSkin_seg;
		new_NoteSkin_seg.m_iStartRow = 0;
		new_NoteSkin_seg.m_sNoteSkin = sNS;
		out.m_Timing.AddNoteSkinSegment( new_NoteSkin_seg );
	}
	if( out.m_Timing.m_ScrollSegments.size() < 1 )
	{					
		ScrollSegment new_Scroll_seg;
		new_Scroll_seg.m_iStartRow = 0.0f;
		new_Scroll_seg.m_fRatio = 1.0f;
		out.m_Timing.AddScrollSegment( new_Scroll_seg );
	}
	if( out.m_Timing.m_vTimeSignatureSegments.size() < 1 )
	{
		TimeSignatureSegment new_Time_seg;
		new_Time_seg.m_iStartRow = 0;
		new_Time_seg.m_iNumerator = 4;
		new_Time_seg.m_iDenominator = 4;
		out.m_Timing.AddTimeSignatureSegment( new_Time_seg );
	}
	if( out.m_Timing.m_SpeedSegments.size() < 1 )
	{
		SpeedSegment new_Speed_seg;
		new_Speed_seg.m_iStartRow = 0;
		new_Speed_seg.m_fRatio = 1;
		new_Speed_seg.m_fWait = 0;
		new_Speed_seg.m_iUnit = 0;
		out.m_Timing.AddSpeedSegment( new_Speed_seg );
	}
	if( out.m_Timing.m_LabelSegments.size() < 1 )
	{
		LabelSegment new_Label_seg;
		new_Label_seg.m_iStartRow = 0;
		new_Label_seg.m_sLabel = "label1";
		out.m_Timing.AddLabelSegment( new_Label_seg );
	}

	TidyUpData(out, bFromCache);	
	return true;
}


bool NXALoader::LoadFromDir( const RString &sPath, Song &out )
{
	vector<RString> aFileNames;
	GetApplicableFiles( sPath, aFileNames );

	if( aFileNames.size() > 1 )
	{
		LOG->UserLog( "Song", sPath, "has more than one SM file. There should be only one!" );
		return false;
	}

	/* We should have exactly one; if we had none, we shouldn't have been
	 * called to begin with. */
	ASSERT( aFileNames.size() == 1 );

	return LoadFromNXAFile( sPath + aFileNames[0], out );
}

bool NXALoader::LoadEditFromFile( RString sEditFilePath, ProfileSlot slot, bool bAddStepsToSong )
{
	LOG->Trace( "NXALoader::LoadEditFromFile(%s)", sEditFilePath.c_str() );

	int iBytes = FILEMAN->GetFileSizeInBytes( sEditFilePath );
	if( iBytes > MAX_EDIT_STEPS_SIZE_BYTES )
	{
		LOG->UserLog( "Edit file", sEditFilePath, "is unreasonably large. It won't be loaded." );
		return false;
	}

	MsdFile msd;
	if( !msd.ReadFile( sEditFilePath, true ) )  // unescape
	{
		LOG->UserLog( "Edit file", sEditFilePath, "couldn't be opened: %s", msd.GetError().c_str() );
		return false;
	}

	return LoadEditFromMsd( msd, sEditFilePath, slot, bAddStepsToSong );
}

bool NXALoader::LoadEditFromBuffer( const RString &sBuffer, const RString &sEditFilePath, ProfileSlot slot )
{
	MsdFile msd;
	msd.ReadFromString( sBuffer, true );  // unescape
	return LoadEditFromMsd( msd, sEditFilePath, slot, true );
}

bool NXALoader::LoadEditFromMsd( const MsdFile &msd, const RString &sEditFilePath, ProfileSlot slot, bool bAddStepsToSong )
{
	Song* pSong = NULL;

	for( unsigned i=0; i<msd.GetNumValues(); i++ )
	{
		int iNumParams = msd.GetNumParams(i);
		const MsdFile::value_t &sParams = msd.GetValue(i);
		RString sValueName = sParams[0];
		sValueName.MakeUpper();

		// handle the data
		if( sValueName=="SONG" )
		{
			if( pSong )
			{
				LOG->UserLog( "Edit file", sEditFilePath, "has more than one #SONG tag." );
				return false;
			}

			RString sSongFullTitle = sParams[1];
			sSongFullTitle.Replace( '\\', '/' );

			pSong = SONGMAN->FindSong( sSongFullTitle );
			if( pSong == NULL )
			{
				LOG->UserLog( "Edit file", sEditFilePath, "requires a song \"%s\" that isn't present.", sSongFullTitle.c_str() );
				return false;
			}

			if( pSong->GetNumStepsLoadedFromProfile(slot) >= MAX_EDITS_PER_SONG_PER_PROFILE )
			{
				LOG->UserLog( "Song file", sSongFullTitle, "already has the maximum number of edits allowed for ProfileSlotP%d.", slot+1 );
				return false;
			}
		}

		else if( sValueName=="NOTES" )
		{
			if( pSong == NULL )
			{
				LOG->UserLog( "Edit file", sEditFilePath, "doesn't have a #SONG tag preceeding the first #NOTES tag." );
				return false;
			}

			if( iNumParams < 7 )
			{
				LOG->UserLog( "Edit file", sEditFilePath, "has %d fields in a #NOTES tag, but should have at least 7.", iNumParams );
				continue;
			}

			if( !bAddStepsToSong )
				return true;

			Steps* pNewNotes = new Steps;
			LoadFromSMTokens( 
				sParams[1], sParams[2], sParams[3], sParams[4], sParams[5], sParams[6],
				*pNewNotes);

			pNewNotes->SetLoadedFromProfile( slot );
			pNewNotes->SetDifficulty( Difficulty_Edit );
			pNewNotes->SetFilename( sEditFilePath );

			if( pSong->IsEditAlreadyLoaded(pNewNotes) )
			{
				LOG->UserLog( "Edit file", sEditFilePath, "is a duplicate of another edit that was already loaded." );
				SAFE_DELETE( pNewNotes );
				return false;
			}

			pSong->AddSteps( pNewNotes );
			return true;	// Only allow one Steps per edit file!
		}
		else
		{
			LOG->UserLog( "Edit file", sEditFilePath, "has an unexpected value \"%s\".", sValueName.c_str() );
		}
	}

	return true;
	
}

void NXALoader::TidyUpData( Song &song, bool bFromCache )
{
	/*
	 * Hack: if the song has any changes at all (so it won't use a random BGA)
	 * and doesn't end with "-nosongbg-", add a song background BGC.  Remove
	 * "-nosongbg-" if it exists.
	 *
	 * This way, songs that were created earlier, when we added the song BG
	 * at the end by default, will still behave as expected; all new songs will
	 * have to add an explicit song BG tag if they want it.  This is really a
	 * formatting hack only; nothing outside of SMLoader ever sees "-nosongbg-".
	 */
	vector<BackgroundChange> &bg = song.GetBackgroundChanges(BACKGROUND_LAYER_1);
	if( !bg.empty() )
	{
		/* BGChanges have been sorted.  On the odd chance that a BGChange exists
		 * with a very high beat, search the whole list. */
		bool bHasNoSongBgTag = false;

		for( unsigned i = 0; !bHasNoSongBgTag && i < bg.size(); ++i )
		{
			if( !bg[i].m_def.m_sFile1.CompareNoCase(NO_SONG_BG_FILE) )
			{
				bg.erase( bg.begin()+i );
				bHasNoSongBgTag = true;
			}
		}

		/* If there's no -nosongbg- tag, add the song BG. */
		if( !bHasNoSongBgTag ) do
		{
			/* If we're loading cache, -nosongbg- should always be in there.  We must
			 * not call IsAFile(song.GetBackgroundPath()) when loading cache. */
			if( bFromCache )
				break;

			/* If BGChanges already exist after the last beat, don't add the background
			 * in the middle. */
			if( !bg.empty() && bg.back().m_fStartBeat-0.0001f >= song.m_fLastBeat )
				break;

			/* If the last BGA is already the song BGA, don't add a duplicate. */
			if( !bg.empty() && !bg.back().m_def.m_sFile1.CompareNoCase(song.m_sBackgroundFile) )
				break;

			if( !IsAFile( song.GetBackgroundPath() ) )
				break;

			bg.push_back( BackgroundChange(song.m_fLastBeat,song.m_sBackgroundFile) );
		} while(0);
	}
}

/*
 * (c) 2008-2011 "v1toko"
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
