#include "global.h"
#include "BackgroundUtil.h"
#include "GameManager.h"
#include "MsdFile.h"
#include "NoteTypes.h"
#include "RageFileManager.h"
#include "RageLog.h"
#include "RageUtil.h"
#include "song.h"
#include "SongManager.h"
#include "Steps.h"
#include "NotesLoaderSMA.h"

const int MAX_EDIT_STEPS_SIZE_BYTES		= 30*1024;	// 30KB

static void LoadFromSMTokens( RString sStepsType, RString sDescription, RString sDifficulty, RString sMeter,
	RString sRadarValues, RString sNoteData, Steps &out )
{
	out.SetSavedToDisk( true );	// we're loading from disk, so this is by definintion already saved

	Trim( sStepsType );
	Trim( sDescription );
	Trim( sDifficulty );
	Trim( sNoteData );

//	LOG->Trace( "Steps::LoadFromSMTokens()" );

	out.m_StepsType = GameManager::StringToStepsType( sStepsType );
	out.SetDescription( sDescription );
	out.SetDifficulty( StringToDifficulty(sDifficulty) );

	// HACK:  We used to store SMANIAC as Difficulty_Hard with special description.
	// Now, it has its own Difficulty_Challenge
	if( sDescription.CompareNoCase("smaniac") == 0 ) 
		out.SetDifficulty( Difficulty_Challenge );
	// HACK:  We used to store CHALLENGE as Difficulty_Hard with special description.
	// Now, it has its own Difficulty_Challenge
	if( sDescription.CompareNoCase("challenge") == 0 ) 
		out.SetDifficulty( Difficulty_Challenge );

	out.SetMeter( atoi(sMeter) );
	vector<RString> saValues;
	split( sRadarValues, ",", saValues, true );
	if( saValues.size() == NUM_RadarCategory * NUM_PLAYERS )
	{
		RadarValues v[NUM_PLAYERS];
		FOREACH_PlayerNumber( pn )
			FOREACH_ENUM( RadarCategory, rc )
				v[pn][rc] = StringToFloat( saValues[pn*NUM_RadarCategory + rc] );
		out.SetCachedRadarValues( v );
	}
    
	out.SetSMNoteData( sNoteData );

	out.TidyUpData();
}

void SMALoader::GetApplicableFiles( const RString &sPath, vector<RString> &out )
{
	GetDirListing( sPath + RString("*.sma"), out );
}


bool LoadFromBGChangesStringSMA( BackgroundChange &change, const RString &sBGChangeExpression )
{
	vector<RString> aBGChangeValues;
	split( sBGChangeExpression, "=", aBGChangeValues, false );

	aBGChangeValues.resize( min((int)aBGChangeValues.size(),11) );

	switch( aBGChangeValues.size() )
	{
	case 11:
		change.m_def.m_sColor2 = aBGChangeValues[10];
		// .sm files made before we started escaping will still have '^' instead of ','
		change.m_def.m_sColor2.Replace( '^', ',' );
		// fall through
	case 10:
		change.m_def.m_sColor1 = aBGChangeValues[9];
		// .sm files made before we started escaping will still have '^' instead of ','
		change.m_def.m_sColor1.Replace( '^', ',' );
		// fall through
	case 9:
		change.m_sTransition = aBGChangeValues[8];
		// fall through
	case 8:
		change.m_def.m_sFile2 = aBGChangeValues[7];
		// fall through
	case 7:
		change.m_def.m_sEffect = aBGChangeValues[6];
		// fall through
	case 6:
		// param 7 overrides this.
		// Backward compatibility:
		if( change.m_def.m_sEffect.empty() )
		{
			bool bLoop = atoi( aBGChangeValues[5] ) != 0;
			if( !bLoop )
				change.m_def.m_sEffect = SBE_StretchNoLoop;
		}
		// fall through
	case 5:
		// param 7 overrides this.
		// Backward compatibility:
		if( change.m_def.m_sEffect.empty() )
		{
			bool bRewindMovie = atoi( aBGChangeValues[4] ) != 0;
			if( bRewindMovie )
				change.m_def.m_sEffect = SBE_StretchRewind;
		}
		// fall through
	case 4:
		// param 9 overrides this.
		// Backward compatibility:
		if( change.m_sTransition.empty() )
			change.m_sTransition = (atoi( aBGChangeValues[3] ) != 0) ? "CrossFade" : "";
		// fall through
	case 3:
		change.m_fRate = StringToFloat( aBGChangeValues[2] );
		// fall through
	case 2:
		change.m_def.m_sFile1 = aBGChangeValues[1];
		// fall through
	case 1:
		change.m_fStartBeat = StringToFloat( aBGChangeValues[0] );
		// fall through
	}
	
	return aBGChangeValues.size() >= 2;
}

float SMALoader::RowToBeat( RString line, const int rowsPerBeat )
{
	RString backup = line;
	Trim(line, "r");
	Trim(line, "R");
	if( backup != line )
	{
		return StringToFloat( line ) / rowsPerBeat;
	}
	else
	{
		return StringToFloat( line );
	}
}

void SMALoader::ProcessSpeeds( TimingData &out, const RString line, const int rowsPerBeat )
{
	vector<RString> vs1;
	split( line, ",", vs1 );
	
	FOREACH_CONST( RString, vs1, s1 )
	{
		vector<RString> vs2;
		split( *s1, "=", vs2 );
		
		if( vs2[0].CompareNoCase("0") && vs2.size() == 2 ) // First one always seems to have 2.
		{
			vs2.push_back("0");
		}
		
		if( vs2.size() == 3 ) // use beats by default.
		{
			vs2.push_back("0");
		}
		
		if( vs2.size() < 4 )
		{
			LOG->UserLog("Song file",
				out.m_sFile,
				     "has an speed change with %i values.",
				     static_cast<int>(vs2.size()) );
			continue;
		}
		
		const float fBeat = RowToBeat( vs2[0], rowsPerBeat );
		
		SpeedSegment seg = SpeedSegment(BeatToNoteRow(fBeat),
											  StringToFloat( vs2[1] ),
											  StringToFloat( vs2[2] ), (atoi(vs2[3])) );
		//seg.m_iUnit = ;
		
		if( fBeat < 0 )
		{
			LOG->UserLog("Song file",
				out.m_sFile,
				     "has an speed change with beat %f.", 
				     fBeat );
			continue;
		}
		
		if( seg.m_fWait < 0 )
		{
			LOG->UserLog("Song file",
				out.m_sFile,
				     "has an speed change with beat %f, length %f.",
					 fBeat, seg.m_fWait );
			continue;
		}
		
		out.AddSpeedSegment( seg );
	}
}

bool SMALoader::ProcessBPMs( TimingData &out, const RString line, const int rowsPerBeat )
{

	const float FAST_BPM_WARP = 9999999.f;

	vector<RString> arrayBPMChangeExpressions;
	split( line, ",", arrayBPMChangeExpressions );
	
	// prepare storage variables for negative BPMs -> Warps.
	float negBeat = -1;
	float negBPM = 1;
	float highspeedBeat = -1;
	bool bNotEmpty = false;
	
	for( unsigned b=0; b<arrayBPMChangeExpressions.size(); b++ )
	{
		vector<RString> arrayBPMChangeValues;
		split( arrayBPMChangeExpressions[b], "=", arrayBPMChangeValues );
		if( arrayBPMChangeValues.size() != 2 )
		{
			LOG->UserLog("Song file",
						out.m_sFile,
				     "has an invalid #BPMs value \"%s\" (must have exactly one '='), ignored.",
				     arrayBPMChangeExpressions[b].c_str() );
			continue;
		}
		
		bNotEmpty = true;
		
		const float fBeat = RowToBeat( arrayBPMChangeValues[0], rowsPerBeat );
		const float fNewBPM = StringToFloat( arrayBPMChangeValues[1] );
		
		if( fNewBPM < 0.0f )
		{
			negBeat = fBeat;
			negBPM = fNewBPM;
		}
		else if( fNewBPM > 0.0f )
		{
			// add in a warp.
			if( negBPM < 0 )
			{
				float endBeat = fBeat + (fNewBPM / -negBPM) * (fBeat - negBeat);
				out.AddWarpSegment(
							   WarpSegment(negBeat, endBeat - negBeat));
				
				negBeat = -1;
				negBPM = 1;
			}
			// too fast. make it a warp.
			if( fNewBPM > FAST_BPM_WARP )
			{
				highspeedBeat = fBeat;
			}
			else
			{
				// add in a warp.
				if( highspeedBeat > 0 )
				{
					out.AddWarpSegment(
								   WarpSegment(highspeedBeat, fBeat - highspeedBeat) );
					highspeedBeat = -1;
				}
				{
					out.AddBPMSegment(
								   BPMSegment(BeatToNoteRow(fBeat), fNewBPM));
				}
			}
		}
	}
	
	return bNotEmpty;
}

void SMALoader::ProcessStops( TimingData &out, const RString line, const int rowsPerBeat )
{
	vector<RString> arrayFreezeExpressions;
	split( line, ",", arrayFreezeExpressions );
	
	// Prepare variables for negative stop conversion.
	float negBeat = -1;
	float negPause = 0;
	
	for( unsigned f=0; f<arrayFreezeExpressions.size(); f++ )
	{
		vector<RString> arrayFreezeValues;
		split( arrayFreezeExpressions[f], "=", arrayFreezeValues );
		if( arrayFreezeValues.size() != 2 )
		{
			LOG->UserLog("Song file",
						out.m_sFile,
				     "has an invalid #STOPS value \"%s\" (must have exactly one '='), ignored.",
				     arrayFreezeExpressions[f].c_str() );
			continue;
		}
		
		const float fFreezeBeat = RowToBeat( arrayFreezeValues[0], rowsPerBeat );
		const float fFreezeSeconds = StringToFloat( arrayFreezeValues[1] );
		
		// Process the prior stop.
		if( negPause > 0 )
		{
			BPMSegment oldBPM = out.GetBPMSegmentAtBeat(negBeat);
			float fSecondsPerBeat = 60 / oldBPM.GetBPM();
			float fSkipBeats = negPause / fSecondsPerBeat;
			
			if( negBeat + fSkipBeats > fFreezeBeat )
				fSkipBeats = fFreezeBeat - negBeat;
			
			out.AddWarpSegment( WarpSegment(negBeat, fSkipBeats));
			
			negBeat = -1;
			negPause = 0;
		}
		
		if( fFreezeSeconds < 0.0f )
		{
			negBeat = fFreezeBeat;
			negPause = -fFreezeSeconds;
		}
		else if( fFreezeSeconds > 0.0f )
		{
			out.AddStopSegment( StopSegment(BeatToNoteRow(fFreezeBeat), fFreezeSeconds));
		}
		
	}
	
	// Process the prior stop if there was one.
	if( negPause > 0 )
	{
		BPMSegment oldBPM = out.GetBPMSegmentAtBeat(negBeat);
		float fSecondsPerBeat = 60 / oldBPM.GetBPM();
		float fSkipBeats = negPause / fSecondsPerBeat;
		
		out.AddWarpSegment(WarpSegment(negBeat, fSkipBeats));
	}
}

bool SMALoader::LoadFromSMAFile( const RString &sPath, Song &out, bool bFromCache )
{
	LOG->Trace( "Song::LoadFromSMAFile(%s)", sPath.c_str() );

	MsdFile msd;
	if( !msd.ReadFile( sPath, true ) )  // unescape
	{
		LOG->UserLog( "Song file", sPath, "couldn't be opened: %s", msd.GetError().c_str() );
		return false;
	}

	out.m_Timing.m_sFile = sPath;
	
	// new style separador #ROWSPERBEAT, si false #OFFSET-(#)-(#): ellos se entienden
	bool bNewStyle = true;		
	for( unsigned i=0; i<msd.GetNumValues(); i++ )
	{
		const MsdFile::value_t &sParams = msd.GetValue(i);		
		RString sValueName = sParams[0];
		sValueName.MakeUpper();
		
		{
			vector<RString> asParts;
			split( sValueName, "-", asParts );
			if( asParts.size() > 1 ) //si es mayor que 1 significa que hay #TAG-#-#.
			{
				//OldTiming Aldo's style (?)		
				//LOG->Trace("Found OLDSTYLE Tag: %s", asParts[0].c_str() );
				bNewStyle = false;
				break; //exit for
			}
		}
	}
	
	//si true, pass
	if( !bNewStyle )
	{
		return LoadFromSMAFileOldStyle( sPath, out, bFromCache );		
	}

	//permite cargar los timings
	int state = SMA_GETTING_SONG_INFO;
	Steps* pNewNotes = NULL;
	//set on steptype especification
	//if is already invalid, the song hasn't split timing
	TimingType tt = TimingType_Invalid;
	AttackArray attacks;
	out.m_bHasSongAttacks = false;	// no hay attacks (hasta ahora)

	//guarda los datos que vamos cargando por modo
	TimingData *pAuxTiming = NULL;

	for( unsigned i=0; i<msd.GetNumValues(); i++ )
	{
		const MsdFile::value_t &sParams = msd.GetValue(i);
		int iNumParams = msd.GetNumParams(i);
		RString sValueName = sParams[0];
		sValueName.MakeUpper();

		// handle the data
		/* Don't use GetMainAndSubTitlesFromFullTitle; that's only for heuristically
		 * splitting other formats that *don't* natively support #SUBTITLE. */
		switch (state)
		{
			case SMA_GETTING_SONG_INFO:
			{
				if( sValueName=="TITLE" )
					out.m_sMainTitle = sParams[1];

				else if( sValueName=="SUBTITLE" )
					out.m_sSubTitle = sParams[1];

				else if( sValueName=="ARTIST" )
					out.m_sArtist = sParams[1];

				else if( sValueName=="TITLETRANSLIT" )
					out.m_sMainTitleTranslit = sParams[1];

				else if( sValueName=="SUBTITLETRANSLIT" )
					out.m_sSubTitleTranslit = sParams[1];

				else if( sValueName=="ARTISTTRANSLIT" )
					out.m_sArtistTranslit = sParams[1];

				else if( sValueName=="GENRE" )
					out.m_sGenre = sParams[1];

				else if( sValueName=="CREDIT" )
					out.m_sCredit = sParams[1];

				else if( sValueName=="BANNER" )
					out.m_sBannerFile = sParams[1];

				else if( sValueName=="BACKGROUND" )
					out.m_sBackgroundFile = sParams[1];

				/* Save "#LYRICS" for later, so we can add an internal lyrics tag. */
				else if( sValueName=="LYRICSPATH" )
					out.m_sLyricsFile = sParams[1];

				else if( sValueName=="CDTITLE" )
					out.m_sCDTitleFile = sParams[1];

				else if( sValueName=="MUSIC" )
					out.m_sMusicFile = sParams[1];

				else if( sValueName=="INSTRUMENTTRACK" )
				{
					vector<RString> vs1;
					split( sParams[1], ",", vs1 );
					FOREACH_CONST( RString, vs1, s )
					{
						vector<RString> vs2;
						split( *s, "=", vs2 );
						if( vs2.size() >= 2 )
						{
							InstrumentTrack it = StringToInstrumentTrack( vs2[0] );
							if( it != InstrumentTrack_Invalid )
								out.m_sInstrumentTrackFile[it] = vs2[1];
						}
					}
				}
				else if( sValueName=="MUSICLENGTH" )
				{
					if( !bFromCache )
						continue;
					out.m_fMusicLengthSeconds = StringToFloat( sParams[1] );
				}
				else if( sValueName=="LASTBEATHINT" )
					out.m_fSpecifiedLastBeat = StringToFloat( sParams[1] );

				else if( sValueName=="MUSICBYTES" )
					; /* ignore */

				/* We calculate these.  Some SMs in circulation have bogus values for
				 * these, so make sure we always calculate it ourself. */
				else if( sValueName=="FIRSTBEAT" )
				{
					if( bFromCache )
						out.m_fFirstBeat = StringToFloat( sParams[1] );
				}
				else if( sValueName=="LASTBEAT" )
				{
					if( bFromCache )
						out.m_fLastBeat = StringToFloat( sParams[1] );
				}
				else if( sValueName=="SONGFILENAME" )
				{
					if( bFromCache )
						out.m_sSongFileName = sParams[1];
				}
				else if( sValueName=="HASMUSIC" )
				{
					if( bFromCache )
						out.m_bHasMusic = atoi( sParams[1] ) != 0;
				}
				else if( sValueName=="HASBANNER" )
				{
					if( bFromCache )
						out.m_bHasBanner = atoi( sParams[1] ) != 0;
				}

				else if( sValueName=="SAMPLESTART" )
					out.m_fMusicSampleStartSeconds = HHMMSSToSeconds( sParams[1] );

				else if( sValueName=="SAMPLELENGTH" )
					out.m_fMusicSampleLengthSeconds = HHMMSSToSeconds( sParams[1] );

				else if( sValueName=="DISPLAYBPM" )
				{
					// #DISPLAYBPM:[xxx][xxx:xxx]|[*]; 
					if( sParams[1] == "*" )
						out.m_DisplayBPMType = Song::DISPLAY_RANDOM;
					else 
					{
						out.m_DisplayBPMType = Song::DISPLAY_SPECIFIED;
						out.m_fSpecifiedBPMMin = StringToFloat( sParams[1] );
						if( sParams[2].empty() )
							out.m_fSpecifiedBPMMax = out.m_fSpecifiedBPMMin;
						else
							out.m_fSpecifiedBPMMax = StringToFloat( sParams[2] );
					}
				}

				else if( sValueName=="SELECTABLE" )
				{
					if(!stricmp(sParams[1],"YES"))
						out.m_SelectionDisplay = out.SHOW_ALWAYS;
					else if(!stricmp(sParams[1],"NO"))
						out.m_SelectionDisplay = out.SHOW_NEVER;
					//MODIFICADO POR MI
					else if( !stricmp( sParams[1], "MEMCARD" ) )
						out.m_SelectionDisplay = out.SHOW_MEMORYCARD;
					else
						LOG->UserLog( "Song file", sPath, "has an unknown #SELECTABLE value, \"%s\"; ignored.", sParams[1].c_str() );
				}

				else if( sValueName.Left(strlen("BGCHANGES"))=="BGCHANGES" || sValueName=="ANIMATIONS" )
				{
					BackgroundLayer iLayer = BACKGROUND_LAYER_1;
					//if( sscanf(sValueName, "BGCHANGES%d", &*ConvertValue<int>(&iLayer)) == 1 )
					//	enum_add(iLayer, -2);	// #BGCHANGES2 = BACKGROUND_LAYER_2
					if( sscanf(sValueName, "BGCHANGES%d", &*ConvertValue<int>(&iLayer)) == 1 )
						enum_add(iLayer, -1);	// #BGCHANGES2 = BACKGROUND_LAYER_3

					//LOG->Warn( "Layer for: %s, %d", sPath.c_str(), iLayer );
					bool bValid = iLayer>=0 && iLayer<NUM_BackgroundLayer;
					if( !bValid )
					{
						LOG->UserLog( "Song file", sPath, "has a #BGCHANGES tag \"%s\" that is out of range.", sValueName.c_str() );
					}
					else
					{
						vector<RString> aBGChangeExpressions;
						split( sParams[1], ",", aBGChangeExpressions );

						for( unsigned b=0; b<aBGChangeExpressions.size(); b++ )
						{
							BackgroundChange change;
							if( LoadFromBGChangesStringSMA( change, aBGChangeExpressions[b] ) )
								out.AddBackgroundChange( iLayer, change );
						}
					}
				}

				else if( sValueName=="FGCHANGES" )
				{
					vector<RString> aFGChangeExpressions;
					split( sParams[1], ",", aFGChangeExpressions );

					for( unsigned b=0; b<aFGChangeExpressions.size(); b++ )
					{
						BackgroundChange change;
						if( LoadFromBGChangesStringSMA( change, aFGChangeExpressions[b] ) )
							out.AddForegroundChange( change );
					}
				}
				//ATACKS
				else if( sValueName=="ATTACKS" )
				{
					Attack attack;
					float end = -9999;
					int iNumAttacks = 0;
					for( unsigned j = 1; j < sParams.params.size(); ++j )
					{
						vector<RString> sBits;
						split( sParams[j], "=", sBits, false );
						if( sBits.size() < 2 )
							continue;

						Trim( sBits[0] );
						if( !sBits[0].CompareNoCase("TIME") )
							attack.fStartSecond = max( StringToFloat(sBits[1]), 0.0f );
						else if( !sBits[0].CompareNoCase("LEN") )
							attack.fSecsRemaining = StringToFloat( sBits[1] );
						else if( !sBits[0].CompareNoCase("END") )
							end = StringToFloat( sBits[1] );
						else if( !sBits[0].CompareNoCase("MODS") )
						{
							attack.sModifiers = sBits[1];
							
							if( end != -9999 )
							{
								attack.fSecsRemaining = end - attack.fStartSecond;
								end = -9999;
							}

							if( attack.fSecsRemaining <= 0.0f)
							{
								LOG->UserLog( "Song file", sPath, "has an attack with a nonpositive length: %s", sBits[1].c_str() );
								attack.fSecsRemaining = 0.0f;
							}
							
							// warn on invalid so we catch typos on load
							//CourseUtil::WarnOnInvalidMods( attack.sModifiers );

							attacks.push_back( attack );
							//out.m_Attacks.push_back( attack );
							iNumAttacks++;

							if( iNumAttacks > 0 )
								out.m_bHasSongAttacks = true;
						}
						else
						{
							LOG->UserLog( "Song file", sPath, "has an unexpected value named '%s'", sBits[0].c_str() );
						}

						out.m_Attacks = attacks;
					}
				}
				else if( sValueName=="KEYSOUNDS" )
				{
					split( sParams[1], ",", out.m_vsKeysoundFile );
				}
				else if( sValueName=="OFFSET" )
				{
					out.m_Timing.m_fBeat0OffsetInSeconds = StringToFloat( sParams[1] );
				}
				/* Below are the song based timings that should only be used
				 * if the steps do not have their own timing. */
				else if( sValueName=="STOPS" )
				{
					/*vector<RString> arrayFreezeExpressions;
					split( sParams[1], ",", arrayFreezeExpressions );

					for( unsigned f=0; f<arrayFreezeExpressions.size(); f++ )
					{
						vector<RString> arrayFreezeValues;
						split( arrayFreezeExpressions[f], "=", arrayFreezeValues );
						if( arrayFreezeValues.size() != 2 )
						{
							// XXX: Hard to tell which file caused this.
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayFreezeExpressions[f].c_str() );
							continue;
						}

						const float fFreezeBeat = RowToBeat( arrayFreezeValues[0], 192 );
						const float fFreezeSeconds = StringToFloat( arrayFreezeValues[1] );
						
						StopSegment new_seg( fFreezeBeat, fFreezeSeconds );

		//				LOG->Trace( "Adding a freeze segment: beat: %f, seconds = %f", new_seg.m_fStartBeat, new_seg.m_fStopSeconds );

						out.AddStopSegment( new_seg );
					}*/
					SMALoader::ProcessStops( out.m_Timing, sParams[1], 192 );
				}
				else if( sValueName=="DELAYS" )
				{
					vector<RString> arrayFreezeExpressions;
					split( sParams[1], ",", arrayFreezeExpressions );

					for( unsigned f=0; f<arrayFreezeExpressions.size(); f++ )
					{
						vector<RString> arrayFreezeValues;
						split( arrayFreezeExpressions[f], "=", arrayFreezeValues );
						if( arrayFreezeValues.size() != 2 )
						{
							// XXX: Hard to tell which file caused this.
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayFreezeExpressions[f].c_str() );
							continue;
						}

						const float fFreezeBeat = RowToBeat( arrayFreezeValues[0], /*ROWS_PER_BEAT*/ 192 );
						const float fFreezeSeconds = StringToFloat( arrayFreezeValues[1] );
						
						StopSegment new_seg( fFreezeBeat, fFreezeSeconds, true );

						//LOG->Trace( "Adding a freeze segment: beat: %f, seconds = %f", new_seg.m_fStartBeat, new_seg.m_fStopSeconds );

						out.AddStopSegment( new_seg );
					}
					//delays = piustops
					//out.m_bUsePiuStops = true;
				}

				else if( sValueName=="BPMS" )
				{
					/*
					vector<RString> arrayBPMChangeExpressions;
					split( sParams[1], ",", arrayBPMChangeExpressions );

					for( unsigned b=0; b<arrayBPMChangeExpressions.size(); b++ )
					{
						vector<RString> arrayBPMChangeValues;
						split( arrayBPMChangeExpressions[b], "=", arrayBPMChangeValues );
						// XXX: Hard to tell which file caused this.
						if( arrayBPMChangeValues.size() != 2 )
						{
							LOG->UserLog( "Song file", sPath, "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayBPMChangeExpressions[b].c_str() );
							continue;
						}

						const float fBeat = RowToBeat( arrayBPMChangeValues[0], 192);
						const float fNewBPM = StringToFloat( arrayBPMChangeValues[1] );
												
						out.AddBPMSegment( BPMSegment(fBeat, fNewBPM) );						
					}
					*/
					SMALoader::ProcessBPMs( out.m_Timing, sParams[1], 192 );
					
				}
				
				else if( sValueName=="WARPS" ) // Older versions allowed em here.
				{
					//ProcessWarps( out.m_SongTiming, sParams[1], out.m_fVersion );
					LOG->Trace( "Found WARPS tag" );
				}
				
				else if( sValueName=="FAKES" )
				{
					vector<RString> arrayFakeExpressions;
					split( sParams[1], ",", arrayFakeExpressions );
					
					for( unsigned b=0; b<arrayFakeExpressions.size(); b++ )
					{
						vector<RString> arrayFakeValues;
						split( arrayFakeExpressions[b], "=", arrayFakeValues );
						if( arrayFakeValues.size() != 2 )
						{
							LOG->UserLog("Song file",
								out.GetSongFilePath(),
									 "has an invalid #FAKES value \"%s\" (must have exactly one '='), ignored.",
									 arrayFakeExpressions[b].c_str() );
							continue;
						}
						
						const float fBeat = SMALoader::RowToBeat( arrayFakeValues[0], 192 /* rowsPerBeat */ );
						const float fNewBeat = StringToFloat( arrayFakeValues[1] );
						
						if(fNewBeat > 0)							
							out.m_Timing.AddFakeSegment( FakeSegment(fBeat, fNewBeat) );						
						else
						{
							LOG->UserLog("Song file",
								out.GetSongFilePath(),
									 "has an invalid Fake at beat %f, BPM %f.",
									 fBeat, fNewBeat );
						}
					}
				}

				else if( sValueName=="TIMESIGNATURES" )
				{
					vector<RString> vs1;
					split( sParams[1], ",", vs1 );

					FOREACH_CONST( RString, vs1, s1 )
					{
						vector<RString> vs2;
						split( *s1, "=", vs2 );

						if( vs2.size() < 3 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with %i values.", (int)vs2.size() );
							continue;
						}

						const float fBeat = StringToFloat( vs2[0] );

						TimeSignatureSegment seg;
						seg.m_iStartRow = BeatToNoteRow(fBeat);
						seg.m_iNumerator = atoi( vs2[1] ); 
						seg.m_iDenominator = atoi( vs2[2] ); 
						
						if( fBeat < 0 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with beat %f.", fBeat );
							continue;
						}
						
						if( seg.m_iNumerator < 1 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with beat %f, iNumerator %i.", fBeat, seg.m_iNumerator );
							continue;
						}

						if( seg.m_iDenominator < 1 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with beat %f, iDenominator %i.", fBeat, seg.m_iDenominator );
							continue;
						}

						out.m_Timing.AddTimeSignatureSegment( seg );
					}
				}

				else if( sValueName=="TICKCOUNT" )
				{
					vector<RString> arrayTickChangeExpressions;
					split( sParams[1], ",", arrayTickChangeExpressions );

					for( unsigned b=0; b<arrayTickChangeExpressions.size(); b++ )
					{
						vector<RString> arrayTickChangeValues;
						split( arrayTickChangeExpressions[b], "=", arrayTickChangeValues );
						// XXX: Hard to tell which file caused this.
						if( arrayTickChangeValues.size() != 2 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayTickChangeExpressions[b].c_str() );
							continue;
						}

						const float fBeat = RowToBeat( arrayTickChangeValues[0], /*ROWS_PER_BEAT*/ 192 );
						const float fNewTickcount = StringToFloat( arrayTickChangeValues[1] );
						
						if( fNewTickcount > -1.0f )
							out.AddTickSegment( TickSegment(BeatToNoteRow(fBeat), fNewTickcount) );
						else
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid tickcount change at beat %f, tickcount %f.", fBeat, fNewTickcount );
					}
				}
				//tag speeds, tiene: beat=[0,1]velocidad=approach=?? (siempre 0)
				else if( sValueName=="SPEED" )
				{
					/*
					vector<RString> arrayArrowSpacingChangeExpressions;
					split( sParams[1], ",", arrayArrowSpacingChangeExpressions );

					for( unsigned b=0; b<arrayArrowSpacingChangeExpressions.size(); b++ )
					{
						vector<RString> arrayArrowSpacingChangeValues;
						split( arrayArrowSpacingChangeExpressions[b], "=", arrayArrowSpacingChangeValues );
						float fNewFactor = 0.5f;
						// XXX: Hard to tell which file caused this.
						if( arrayArrowSpacingChangeValues.size() == 2 )
						{
							;
						}
						else if( arrayArrowSpacingChangeValues.size() > 2 )
						{
							fNewFactor = StringToFloat( arrayArrowSpacingChangeValues[2] );
						}
						else 
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #SPEED value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}

						const float fBeat = RowToBeat( arrayArrowSpacingChangeValues[0], 192 );
						const float fNewArrowSpacing = StringToFloat( arrayArrowSpacingChangeValues[1] );

						float fNew = SCALE( fNewArrowSpacing, 0, 2, 0, 128 );
						LOG->Trace( "Scaling ArrowSpacing: %.2f to %.2f", fNewArrowSpacing, fNew );
						
						if( fNew >= 0.0f )
							out.AddArrowSpacingSegment( ArrowSpacingSegment(BeatToNoteRow(fBeat), fNew, fNewFactor) );
						else
							LOG->UserLog( "Song file", sPath, "has an invalid speed change at beat %f, speed %f.", fBeat, fNewArrowSpacing );
					}
					*/
					SMALoader::ProcessSpeeds( out.m_Timing, sParams[1], 192 );
				}

				else if( sValueName=="MULTIPLIER" )
				{
					vector<RString> arrayComboExpressions;
					split( sParams[1], ",", arrayComboExpressions );
					
					for( unsigned f=0; f<arrayComboExpressions.size(); f++ )
					{
						vector<RString> arrayComboValues;
						split( arrayComboExpressions[f], "=", arrayComboValues );
						unsigned size = arrayComboValues.size();
						if( size < 2 )
						{
							LOG->UserLog("Song file",
									sPath,
									 "has an invalid #COMBOS value \"%s\" (must have at least one '='), ignored.",
									 arrayComboExpressions[f].c_str() );
							continue;
						}
						const float fComboBeat = StringToFloat( arrayComboValues[0] );
						const int iCombos = StringToInt( arrayComboValues[1] );
						const int iMisses = (size == 2 ? iCombos : StringToInt(arrayComboValues[2]));
						out.AddComboSegment( ComboSegment( fComboBeat, iCombos, iMisses ) );
					}
				}

				// This tag will get us to the next section.
				else if( sValueName=="NOTEDATA" )
				{
					;
				}
				else if( sValueName=="ROWSPERBEAT" )
				{
					state = SMA_GETTING_STEP_INFO;
					pNewNotes = new Steps();
					//stepsTiming = TimingData( out.m_SongTiming.m_fBeat0OffsetInSeconds );
					//bHasOwnTiming = false;
					pAuxTiming = new TimingData();
					tt = TimingType_Invalid;
				}
				break;				
			}//case
			case SMA_GETTING_STEP_INFO:
			{

				if( sValueName=="NOTES" || sValueName=="NOTES2" )
				{
					state = SMA_GETTING_SONG_INFO;

					if( iNumParams < 7 )
					{
						LOG->UserLog( "Song file", sPath, "has %d fields in a #NOTES tag, but should have at least 7.", iNumParams );
						continue;
					}

					if( pAuxTiming->m_TickSegments.size() < 1 )
					{
						TickSegment new_tick_seg;
						new_tick_seg.m_fStartBeat = 0.0f;
						new_tick_seg.m_iTickcount = 6;
						pAuxTiming->AddTickSegment( new_tick_seg );
					}
					if( pAuxTiming->m_ArrowSpacingSegments.size() < 1 )
					{
						ArrowSpacingSegment new_ArrowSpacing_seg;
						new_ArrowSpacing_seg.m_iStartRow = 0;
						new_ArrowSpacing_seg.m_fArrowSpacing = 64.0f;
						pAuxTiming->AddArrowSpacingSegment( new_ArrowSpacing_seg );
					}
					if( pAuxTiming->m_ComboSegments.size() < 1 )
					{
						ComboSegment new_Combo_seg;
						new_Combo_seg.m_fStartBeat = 0.0f;
						new_Combo_seg.m_iComboFactor = 1;
						new_Combo_seg.m_iMissComboFactor = 1;
						pAuxTiming->AddComboSegment( new_Combo_seg );
					}
					if( pAuxTiming->m_NoteSkinSegments.size() < 1 )
					{
						RString sNS = "default";
						Trim(sNS);
						NoteSkinSegment new_NoteSkin_seg;
						new_NoteSkin_seg.m_iStartRow = 0;
						new_NoteSkin_seg.m_sNoteSkin = sNS;
						pAuxTiming->AddNoteSkinSegment( new_NoteSkin_seg );
					}
					if( pAuxTiming->m_ScrollSegments.size() < 1 )
					{					
						ScrollSegment new_Scroll_seg;
						new_Scroll_seg.m_iStartRow = 0.0f;
						new_Scroll_seg.m_fRatio = 1.0f;
						pAuxTiming->AddScrollSegment( new_Scroll_seg );
					}
					if( pAuxTiming->m_vTimeSignatureSegments.size() < 1 )
					{
						TimeSignatureSegment new_Time_seg;
						new_Time_seg.m_iStartRow = 0;
						new_Time_seg.m_iNumerator = 4;
						new_Time_seg.m_iDenominator = 4;
						pAuxTiming->AddTimeSignatureSegment( new_Time_seg );
					}
					if( pAuxTiming->m_SpeedSegments.size() < 1 )
					{
						SpeedSegment new_Speed_seg;
						new_Speed_seg.m_iStartRow = 0;
						new_Speed_seg.m_fRatio = 1;
						new_Speed_seg.m_fWait = 0;
						new_Speed_seg.m_iUnit = 0;
						pAuxTiming->AddSpeedSegment( new_Speed_seg );
					}
					if( pAuxTiming->m_LabelSegments.size() < 1 )
					{
						LabelSegment new_Label_seg;
						new_Label_seg.m_iStartRow = 0;
						new_Label_seg.m_sLabel = "label1";
						pAuxTiming->AddLabelSegment( new_Label_seg );
					}


					pNewNotes->m_StepsTiming = *pAuxTiming;

					//Steps* pNewNotes = new Steps;
					LoadFromSMTokens( 
						sParams[1], 
						sParams[2], 
						sParams[3], 
						sParams[4], 
						sParams[5], 
						sParams[6],
						*pNewNotes );

					out.AddSteps( pNewNotes );
				}
				
				else if( sValueName=="BPMS" )
				{
					/*vector<RString> arrayBPMChangeExpressions;
					split( sParams[1], ",", arrayBPMChangeExpressions );

					for( unsigned b=0; b<arrayBPMChangeExpressions.size(); b++ )
					{
						vector<RString> arrayBPMChangeValues;
						split( arrayBPMChangeExpressions[b], "=", arrayBPMChangeValues );
						// XXX: Hard to tell which file caused this.
						if( arrayBPMChangeValues.size() != 2 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayBPMChangeExpressions[b].c_str() );
							continue;
						}

						const float fBeat = RowToBeat( arrayBPMChangeValues[0], 192 );
						const float fNewBPM = StringToFloat( arrayBPMChangeValues[1] );
												
						//if( tt != TimingType_Invalid )
							pAuxTiming->AddBPMSegment( BPMSegment(BeatToNoteRow(fBeat), fNewBPM) );						
						//else
						//	out.m_Timing.AddBPMSegment( BPMSegment(BeatToNoteRow(fBeat), fNewBPM) );
					}*/

					SMALoader::ProcessBPMs( *pAuxTiming, sParams[1], 192);
				}
				
				else if( sValueName=="STOPS" )
				{
					/*vector<RString> arrayFreezeExpressions;
					split( sParams[1], ",", arrayFreezeExpressions );

					for( unsigned f=0; f<arrayFreezeExpressions.size(); f++ )
					{
						vector<RString> arrayFreezeValues;
						split( arrayFreezeExpressions[f], "=", arrayFreezeValues );
						if( arrayFreezeValues.size() != 2 )
						{
							// XXX: Hard to tell which file caused this.
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayFreezeExpressions[f].c_str() );
							continue;
						}

						const float fFreezeBeat = RowToBeat( arrayFreezeValues[0],192 );
						const float fFreezeSeconds = StringToFloat( arrayFreezeValues[1] );
						
						StopSegment new_seg( BeatToNoteRow(fFreezeBeat), fFreezeSeconds );

		//				LOG->Trace( "Adding a freeze segment: beat: %f, seconds = %f", new_seg.m_fStartBeat, new_seg.m_fStopSeconds );

						
						//if( tt != TimingType_Invalid )
							pAuxTiming->AddStopSegment( new_seg );					
						//else
						//	out.m_Timing.AddStopSegment( StopSegment(new_seg) );
					}
					*/
					SMALoader::ProcessStops( *pAuxTiming, sParams[1], 192 );
				}
				
				else if( sValueName=="DELAYS" )
				{
					vector<RString> arrayFreezeExpressions;
					split( sParams[1], ",", arrayFreezeExpressions );

					for( unsigned f=0; f<arrayFreezeExpressions.size(); f++ )
					{
						vector<RString> arrayFreezeValues;
						split( arrayFreezeExpressions[f], "=", arrayFreezeValues );
						if( arrayFreezeValues.size() != 2 )
						{
							// XXX: Hard to tell which file caused this.
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayFreezeExpressions[f].c_str() );
							continue;
						}

						const float fFreezeBeat = RowToBeat( arrayFreezeValues[0], /*ROWS_PER_BEAT*/ 192 );
						const float fFreezeSeconds = StringToFloat( arrayFreezeValues[1] );
						
						StopSegment new_seg( BeatToNoteRow(fFreezeBeat), fFreezeSeconds, true );

		//				LOG->Trace( "Adding a freeze segment: beat: %f, seconds = %f", new_seg.m_fStartBeat, new_seg.m_fStopSeconds );

						
						//if( tt != TimingType_Invalid )
							pAuxTiming->AddStopSegment( new_seg );					
						//else
						//	out.m_Timing.AddStopSegment( StopSegment(new_seg) );
					}
					
					/*if( tt != TimingType_Invalid )
						out.m_UsePiuStops[tt] = true;					
					else
						out.m_bUsePiuStops = true;*/
				}
				
				else if( sValueName=="TIMESIGNATURES" )
				{
					vector<RString> vs1;
					split( sParams[1], ",", vs1 );

					FOREACH_CONST( RString, vs1, s1 )
					{
						vector<RString> vs2;
						split( *s1, "=", vs2 );

						if( vs2.size() < 3 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with %i values.", (int)vs2.size() );
							continue;
						}

						const float fBeat = StringToFloat( vs2[0] );

						TimeSignatureSegment seg;
						seg.m_iStartRow = BeatToNoteRow(fBeat);
						seg.m_iNumerator = atoi( vs2[1] ); 
						seg.m_iDenominator = atoi( vs2[2] ); 
						
						if( fBeat < 0 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with beat %f.", fBeat );
							continue;
						}
						
						if( seg.m_iNumerator < 1 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with beat %f, iNumerator %i.", fBeat, seg.m_iNumerator );
							continue;
						}

						if( seg.m_iDenominator < 1 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with beat %f, iDenominator %i.", fBeat, seg.m_iDenominator );
							continue;
						}

						
						//if( tt != TimingType_Invalid )
							pAuxTiming->AddTimeSignatureSegment( seg );			
						//else
						//	out.m_Timing.AddTimeSignatureSegment( TimeSignatureSegment(seg) );
					}
				}
				
				else if( sValueName=="TICKCOUNT" )
				{
					vector<RString> arrayTickChangeExpressions;
					split( sParams[1], ",", arrayTickChangeExpressions );

					for( unsigned b=0; b<arrayTickChangeExpressions.size(); b++ )
					{
						vector<RString> arrayTickChangeValues;
						split( arrayTickChangeExpressions[b], "=", arrayTickChangeValues );
						// XXX: Hard to tell which file caused this.
						if( arrayTickChangeValues.size() != 2 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayTickChangeExpressions[b].c_str() );
							continue;
						}

						const float fBeat = RowToBeat( arrayTickChangeValues[0],/*ROWS_PER_BEAT*/ 192 );
						const float fNewTickcount = StringToFloat( arrayTickChangeValues[1] );
						
						if( fNewTickcount > -1.0f )
						{							
							//if( tt != TimingType_Invalid )
								pAuxTiming->AddTickSegment( TickSegment(BeatToNoteRow(fBeat), fNewTickcount) );				
							//else
							//	out.m_Timing.AddTickSegment( TickSegment(BeatToNoteRow(fBeat), fNewTickcount) );
						}
						else
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid tickcount change at beat %f, tickcount %f.", fBeat, fNewTickcount );
					}
				}
				
				else if( sValueName=="MULTIPLIER" )
				{
					vector<RString> arrayComboExpressions;
					split( sParams[1], ",", arrayComboExpressions );
					
					for( unsigned f=0; f<arrayComboExpressions.size(); f++ )
					{
						vector<RString> arrayComboValues;
						split( arrayComboExpressions[f], "=", arrayComboValues );
						unsigned size = arrayComboValues.size();
						if( size < 2 )
						{
							LOG->UserLog("Song file",
									sPath,
									 "has an invalid #COMBOS value \"%s\" (must have at least one '='), ignored.",
									 arrayComboExpressions[f].c_str() );
							continue;
						}
						const float fComboBeat = StringToFloat( arrayComboValues[0] );
						const int iCombos = StringToInt( arrayComboValues[1] );
						const int iMisses = (size == 2 ? iCombos : StringToInt(arrayComboValues[2]));
						pAuxTiming->AddComboSegment( ComboSegment( fComboBeat, iCombos, iMisses ) );
					}
				}
				
				else if( sValueName=="WARPS" )
				{
					//ProcessWarps(stepsTiming, sParams[1], out.m_fVersion);
					LOG->Trace( "Found Split Warps tag" );
				}
				
				else if( sValueName=="SCROLLS" )
				{
					LOG->Trace( "Found SPEEDS Combos tag" );
				}
				
				else if( sValueName=="SPEED" )
				{
					//ProcessScrolls( stepsTiming, sParams[1] );
					//LOG->Trace( "Found SCROLLS Combos tag" );
					/*
					vector<RString> arrayArrowSpacingChangeExpressions;
					split( sParams[1], ",", arrayArrowSpacingChangeExpressions );

					for( unsigned b=0; b<arrayArrowSpacingChangeExpressions.size(); b++ )
					{
						vector<RString> arrayArrowSpacingChangeValues;
						split( arrayArrowSpacingChangeExpressions[b], "=", arrayArrowSpacingChangeValues );
						float fNewFactor = 0;
						unsigned iUnit = 0;
						// XXX: Hard to tell which file caused this.
						if( arrayArrowSpacingChangeValues.size() == 2 )
						{
							;
						}
						else if( arrayArrowSpacingChangeValues.size() > 2 )
						{
							RString sSecond = arrayArrowSpacingChangeValues[2];
							RString backup = sSecond;
							Trim( sSecond, "s" );
							Trim( sSecond, "S" );

							if( backup != sSecond ) //end with s
							{
								iUnit = 1;
							}

							fNewFactor = StringToFloat( arrayArrowSpacingChangeValues[2] );
						}
						else 
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #SPEED value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}

						const float fBeat = RowToBeat( arrayArrowSpacingChangeValues[0], 192 );
						const float fNew = StringToFloat( arrayArrowSpacingChangeValues[1] );

						//float fNew = SCALE( fNewArrowSpacing, 0, 2, 0, 128 );
											
						if( fNew >= 0.0f )							
							//if( tt != TimingType_Invalid )
								pAuxTiming->AddSpeedSegment( SpeedSegment(BeatToNoteRow(fBeat), fNew, fNewFactor, iUnit) );				
							//else
							//	out.m_Timing.AddArrowSpacingSegment( ArrowSpacingSegment(BeatToNoteRow(fBeat), fNew) );
						else
							LOG->UserLog( "Song file", sPath, "has an invalid speed change at beat %f, speed %f.", fBeat, fNew );
					}
					*/
					SMALoader::ProcessSpeeds( (*pAuxTiming), sParams[1], 192 );
				}
				
				else if( sValueName=="FAKES" )
				{
					vector<RString> arrayFakeExpressions;
					split( sParams[1], ",", arrayFakeExpressions );
					
					for( unsigned b=0; b<arrayFakeExpressions.size(); b++ )
					{
						vector<RString> arrayFakeValues;
						split( arrayFakeExpressions[b], "=", arrayFakeValues );
						if( arrayFakeValues.size() != 2 )
						{
							LOG->UserLog("Song file",
								out.GetSongFilePath(),
									 "has an invalid #FAKES value \"%s\" (must have exactly one '='), ignored.",
									 arrayFakeExpressions[b].c_str() );
							continue;
						}
						
						const float fBeat = SMALoader::RowToBeat( arrayFakeValues[0], 192 /* rowsPerBeat */ );
						const float fNewBeat = StringToFloat( arrayFakeValues[1] );
						
						if(fNewBeat > 0)	
						{
							//if( tt != TimingType_Invalid )
							//	out.m_TimingExtra[tt].AddFakeSegment( FakeSegment(BeatToNoteRow(fBeat), fNewBeat) );						
							//else
							pAuxTiming->AddFakeSegment( FakeSegment(fBeat, fNewBeat) );						
						}
						else
						{
							LOG->UserLog("Song file",
								out.GetSongFilePath(),
									 "has an invalid Fake at beat %f, BPM %f.",
									 fBeat, fNewBeat );
						}
					}
				}
				
				else if( sValueName=="LABELS" )
				{
					//ProcessLabels(stepsTiming, sParams[1]);
					LOG->Trace( "Found Split LABELS tag" );
				}
				
				else if( sValueName=="ATTACKS" )
				{
					//ProcessAttackString(pNewNotes->m_sAttackString, sParams);
					//ProcessAttacks(pNewNotes->m_Attacks, sParams);
					Attack attack;
					float end = -9999;
					int iNumAttacks = 0;
					for( unsigned j = 1; j < sParams.params.size(); ++j )
					{
						vector<RString> sBits;
						split( sParams[j], "=", sBits, false );
						if( sBits.size() < 2 )
							continue;

						Trim( sBits[0] );
						if( !sBits[0].CompareNoCase("TIME") )
							attack.fStartSecond = max( StringToFloat(sBits[1]), 0.0f );
						else if( !sBits[0].CompareNoCase("LEN") )
							attack.fSecsRemaining = StringToFloat( sBits[1] );
						else if( !sBits[0].CompareNoCase("END") )
							end = StringToFloat( sBits[1] );
						else if( !sBits[0].CompareNoCase("MODS") )
						{
							attack.sModifiers = sBits[1];
							
							if( end != -9999 )
							{
								attack.fSecsRemaining = end - attack.fStartSecond;
								end = -9999;
							}

							if( attack.fSecsRemaining <= 0.0f)
							{
								LOG->UserLog( "Song file", sPath, "has an attack with a nonpositive length: %s", sBits[1].c_str() );
								attack.fSecsRemaining = 0.0f;
							}
							
							// warn on invalid so we catch typos on load
							//CourseUtil::WarnOnInvalidMods( attack.sModifiers );

							attacks.push_back( attack );
							//out.m_Attacks.push_back( attack );
							iNumAttacks++;

							if( iNumAttacks > 0 )
								pNewNotes->m_bHasAttacks = true;
						}
						else
						{
							LOG->UserLog( "Song file", sPath, "has an unexpected value named '%s'", sBits[0].c_str() );
						}

						pNewNotes->m_Attacks = attacks;

						
						//if( tt != TimingType_Invalid )
						//	out.m_AttacksExtra[tt] = attacks;
						//else
						//	out.m_Attacks = attacks;
					}
				}
				
				else if( sValueName=="OFFSET" )
				{
					//stepsTiming.m_fBeat0OffsetInSeconds = StringToFloat( sParams[1] );					
					//if( tt != TimingType_Invalid )
						pAuxTiming->m_fBeat0OffsetInSeconds = atof( sParams[1] );
					//else
					//	out.m_Timing.m_fBeat0OffsetInSeconds = atof( sParams[1] );
				}
				break;
			}
		}
	}

	//si no es oldstyle llega a este punto
	TidyUpData(out, bFromCache);	
	return true;
}

bool SMALoader::LoadFromSMAFileOldStyle( const RString &sPath, Song &out, bool bFromCache )
{
	LOG->Trace( "Song::LoadFromSMAFileOldStyle(%s)", sPath.c_str() );

	MsdFile msd;
	if( !msd.ReadFile( sPath, true ) )  // unescape
	{
		LOG->UserLog( "Song file", sPath, "couldn't be opened: %s", msd.GetError().c_str() );
		return false;
	}

	out.m_Timing.m_sFile = sPath;
	out.m_bHasSongAttacks = false;	// no hay attacks (hasta ahora)

	//permite cargar los timings
	int state = SMA_GETTING_SONG_INFO;
	Steps* pNewNotes = NULL;
	//set on steptype especification
	//if is already invalid, the song hasn't split timing
	TimingType tt = TimingType_Invalid;
	AttackArray attacks;
	TimingData* pAuxTiming = NULL;

	for( unsigned i=0; i<msd.GetNumValues(); i++ )
	{
		const MsdFile::value_t &sParams = msd.GetValue(i);
		int iNumParams = msd.GetNumParams(i);
		RString sValueName = sParams[0];
		sValueName.MakeUpper();
		bool bSplit = false;
		
		//LOG->Trace( "Leyendo tag: %s, con valores: %s", sValueName.c_str(), sParams[1].c_str() );
		
		{
			vector<RString> asParts;
			split( sValueName, "-", asParts );
			if( asParts.size() > 0 ) 
			{
				bSplit = true; //estamos leyendo split timings
				sValueName = asParts[0]; //quitamos las partes con "-"				
			}
		}
		
		//si estamos en este punto es por que hay split timings con #OFFSET-(#)-(#):blablablaaa

		// handle the data
		/* Don't use GetMainAndSubTitlesFromFullTitle; that's only for heuristically
		 * splitting other formats that *don't* natively support #SUBTITLE. */
		switch (state)
		{
			case SMA_GETTING_SONG_INFO:
			{
				if( sValueName=="TITLE" )
					out.m_sMainTitle = sParams[1];

				else if( sValueName=="SUBTITLE" )
					out.m_sSubTitle = sParams[1];

				else if( sValueName=="ARTIST" )
					out.m_sArtist = sParams[1];

				else if( sValueName=="TITLETRANSLIT" )
					out.m_sMainTitleTranslit = sParams[1];

				else if( sValueName=="SUBTITLETRANSLIT" )
					out.m_sSubTitleTranslit = sParams[1];

				else if( sValueName=="ARTISTTRANSLIT" )
					out.m_sArtistTranslit = sParams[1];

				else if( sValueName=="GENRE" )
					out.m_sGenre = sParams[1];

				else if( sValueName=="CREDIT" )
					out.m_sCredit = sParams[1];

				else if( sValueName=="BANNER" )
					out.m_sBannerFile = sParams[1];

				else if( sValueName=="BACKGROUND" )
					out.m_sBackgroundFile = sParams[1];

				/* Save "#LYRICS" for later, so we can add an internal lyrics tag. */
				else if( sValueName=="LYRICSPATH" )
					out.m_sLyricsFile = sParams[1];

				else if( sValueName=="CDTITLE" )
					out.m_sCDTitleFile = sParams[1];

				else if( sValueName=="MUSIC" )
					out.m_sMusicFile = sParams[1];

				else if( sValueName=="INSTRUMENTTRACK" )
				{
					vector<RString> vs1;
					split( sParams[1], ",", vs1 );
					FOREACH_CONST( RString, vs1, s )
					{
						vector<RString> vs2;
						split( *s, "=", vs2 );
						if( vs2.size() >= 2 )
						{
							InstrumentTrack it = StringToInstrumentTrack( vs2[0] );
							if( it != InstrumentTrack_Invalid )
								out.m_sInstrumentTrackFile[it] = vs2[1];
						}
					}
				}
				else if( sValueName=="MUSICLENGTH" )
				{
					if( !bFromCache )
						continue;
					out.m_fMusicLengthSeconds = StringToFloat( sParams[1] );
				}
				else if( sValueName=="LASTBEATHINT" )
					out.m_fSpecifiedLastBeat = StringToFloat( sParams[1] );

				else if( sValueName=="MUSICBYTES" )
					; /* ignore */

				/* We calculate these.  Some SMs in circulation have bogus values for
				 * these, so make sure we always calculate it ourself. */
				else if( sValueName=="FIRSTBEAT" )
				{
					if( bFromCache )
						out.m_fFirstBeat = StringToFloat( sParams[1] );
				}
				else if( sValueName=="LASTBEAT" )
				{
					if( bFromCache )
						out.m_fLastBeat = StringToFloat( sParams[1] );
				}
				else if( sValueName=="SONGFILENAME" )
				{
					if( bFromCache )
						out.m_sSongFileName = sParams[1];
				}
				else if( sValueName=="HASMUSIC" )
				{
					if( bFromCache )
						out.m_bHasMusic = atoi( sParams[1] ) != 0;
				}
				else if( sValueName=="HASBANNER" )
				{
					if( bFromCache )
						out.m_bHasBanner = atoi( sParams[1] ) != 0;
				}

				else if( sValueName=="SAMPLESTART" )
					out.m_fMusicSampleStartSeconds = HHMMSSToSeconds( sParams[1] );

				else if( sValueName=="SAMPLELENGTH" )
					out.m_fMusicSampleLengthSeconds = HHMMSSToSeconds( sParams[1] );

				else if( sValueName=="DISPLAYBPM" )
				{
					// #DISPLAYBPM:[xxx][xxx:xxx]|[*]; 
					if( sParams[1] == "*" )
						out.m_DisplayBPMType = Song::DISPLAY_RANDOM;
					else 
					{
						out.m_DisplayBPMType = Song::DISPLAY_SPECIFIED;
						out.m_fSpecifiedBPMMin = StringToFloat( sParams[1] );
						if( sParams[2].empty() )
							out.m_fSpecifiedBPMMax = out.m_fSpecifiedBPMMin;
						else
							out.m_fSpecifiedBPMMax = StringToFloat( sParams[2] );
					}
				}

				else if( sValueName=="SELECTABLE" )
				{
					if(!stricmp(sParams[1],"YES"))
						out.m_SelectionDisplay = out.SHOW_ALWAYS;
					else if(!stricmp(sParams[1],"NO"))
						out.m_SelectionDisplay = out.SHOW_NEVER;
					//MODIFICADO POR MI
					else if( !stricmp( sParams[1], "MEMCARD" ) )
						out.m_SelectionDisplay = out.SHOW_MEMORYCARD;
					else
						LOG->UserLog( "Song file", sPath, "has an unknown #SELECTABLE value, \"%s\"; ignored.", sParams[1].c_str() );
				}

				else if( sValueName.Left(strlen("BGCHANGES"))=="BGCHANGES" || sValueName=="ANIMATIONS" )
				{
					BackgroundLayer iLayer = BACKGROUND_LAYER_1;
					//if( sscanf(sValueName, "BGCHANGES%d", &*ConvertValue<int>(&iLayer)) == 1 )
					//	enum_add(iLayer, -2);	// #BGCHANGES2 = BACKGROUND_LAYER_2
					if( sscanf(sValueName, "BGCHANGES%d", &*ConvertValue<int>(&iLayer)) == 1 )
						enum_add(iLayer, -1);	// #BGCHANGES2 = BACKGROUND_LAYER_3

					//LOG->Warn( "Layer for: %s, %d", sPath.c_str(), iLayer );
					bool bValid = iLayer>=0 && iLayer<NUM_BackgroundLayer;
					if( !bValid )
					{
						LOG->UserLog( "Song file", sPath, "has a #BGCHANGES tag \"%s\" that is out of range.", sValueName.c_str() );
					}
					else
					{
						vector<RString> aBGChangeExpressions;
						split( sParams[1], ",", aBGChangeExpressions );

						for( unsigned b=0; b<aBGChangeExpressions.size(); b++ )
						{
							BackgroundChange change;
							if( LoadFromBGChangesStringSMA( change, aBGChangeExpressions[b] ) )
								out.AddBackgroundChange( iLayer, change );
						}
					}
				}

				else if( sValueName=="FGCHANGES" )
				{
					vector<RString> aFGChangeExpressions;
					split( sParams[1], ",", aFGChangeExpressions );

					for( unsigned b=0; b<aFGChangeExpressions.size(); b++ )
					{
						BackgroundChange change;
						if( LoadFromBGChangesStringSMA( change, aFGChangeExpressions[b] ) )
							out.AddForegroundChange( change );
					}
				}
				//ATACKS
				else if( sValueName=="ATTACKS" )
				{
					Attack attack;
					float end = -9999;
					int iNumAttacks = 0;
					for( unsigned j = 1; j < sParams.params.size(); ++j )
					{
						vector<RString> sBits;
						split( sParams[j], "=", sBits, false );
						if( sBits.size() < 2 )
							continue;

						Trim( sBits[0] );
						if( !sBits[0].CompareNoCase("TIME") )
							attack.fStartSecond = max( StringToFloat(sBits[1]), 0.0f );
						else if( !sBits[0].CompareNoCase("LEN") )
							attack.fSecsRemaining = StringToFloat( sBits[1] );
						else if( !sBits[0].CompareNoCase("END") )
							end = StringToFloat( sBits[1] );
						else if( !sBits[0].CompareNoCase("MODS") )
						{
							attack.sModifiers = sBits[1];
							
							if( end != -9999 )
							{
								attack.fSecsRemaining = end - attack.fStartSecond;
								end = -9999;
							}

							if( attack.fSecsRemaining <= 0.0f)
							{
								LOG->UserLog( "Song file", sPath, "has an attack with a nonpositive length: %s", sBits[1].c_str() );
								attack.fSecsRemaining = 0.0f;
							}
							
							// warn on invalid so we catch typos on load
							//CourseUtil::WarnOnInvalidMods( attack.sModifiers );

							attacks.push_back( attack );
							//out.m_Attacks.push_back( attack );
							iNumAttacks++;

							if( iNumAttacks > 0 )
								out.m_bHasSongAttacks = true;
						}
						else
						{
							LOG->UserLog( "Song file", sPath, "has an unexpected value named '%s'", sBits[0].c_str() );
						}

						out.m_Attacks = attacks;
					}
				}
				else if( sValueName=="KEYSOUNDS" )
				{
					split( sParams[1], ",", out.m_vsKeysoundFile );
				}
				else if( sValueName=="OFFSET" && !bSplit )
				{
					out.m_Timing.m_fBeat0OffsetInSeconds = StringToFloat( sParams[1] );
				}
				/* Below are the song based timings that should only be used
				 * if the steps do not have their own timing. */
				else if( sValueName=="STOPS" && !bSplit )
				{
					vector<RString> arrayFreezeExpressions;
					split( sParams[1], ",", arrayFreezeExpressions );

					for( unsigned f=0; f<arrayFreezeExpressions.size(); f++ )
					{
						vector<RString> arrayFreezeValues;
						split( arrayFreezeExpressions[f], "=", arrayFreezeValues );
						if( arrayFreezeValues.size() != 2 )
						{
							// XXX: Hard to tell which file caused this.
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayFreezeExpressions[f].c_str() );
							continue;
						}

						const float fFreezeBeat = StringToFloat( arrayFreezeValues[0] );
						const float fFreezeSeconds = StringToFloat( arrayFreezeValues[1] );
						
						StopSegment new_seg( BeatToNoteRow(fFreezeBeat), fFreezeSeconds );

		//				LOG->Trace( "Adding a freeze segment: beat: %f, seconds = %f", new_seg.m_fStartBeat, new_seg.m_fStopSeconds );

						out.AddStopSegment( new_seg );
					}
				}
				else if( sValueName=="DELAYS" && !bSplit )
				{
					vector<RString> arrayFreezeExpressions;
					split( sParams[1], ",", arrayFreezeExpressions );

					for( unsigned f=0; f<arrayFreezeExpressions.size(); f++ )
					{
						vector<RString> arrayFreezeValues;
						split( arrayFreezeExpressions[f], "=", arrayFreezeValues );
						if( arrayFreezeValues.size() != 2 )
						{
							// XXX: Hard to tell which file caused this.
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayFreezeExpressions[f].c_str() );
							continue;
						}

						const float fFreezeBeat = StringToFloat( arrayFreezeValues[0] );
						const float fFreezeSeconds = StringToFloat( arrayFreezeValues[1] );
						
						StopSegment new_seg( BeatToNoteRow(fFreezeBeat), fFreezeSeconds, true );

						//LOG->Trace( "Adding a freeze segment: beat: %f, seconds = %f", new_seg.m_fStartBeat, new_seg.m_fStopSeconds );

						out.AddStopSegment( new_seg );
					}
					//delays = piustops
					//out.m_bUsePiuStops = true;
				}

				else if( sValueName=="BPMS" && !bSplit )
				{
					vector<RString> arrayBPMChangeExpressions;
					split( sParams[1], ",", arrayBPMChangeExpressions );

					for( unsigned b=0; b<arrayBPMChangeExpressions.size(); b++ )
					{
						vector<RString> arrayBPMChangeValues;
						split( arrayBPMChangeExpressions[b], "=", arrayBPMChangeValues );
						// XXX: Hard to tell which file caused this.
						if( arrayBPMChangeValues.size() != 2 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayBPMChangeExpressions[b].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayBPMChangeValues[0] );
						const float fNewBPM = StringToFloat( arrayBPMChangeValues[1] );
												
						out.AddBPMSegment( BPMSegment(BeatToNoteRow(fBeat), fNewBPM) );						
					}
				}
				
				else if( sValueName=="WARPS" && !bSplit ) // Older versions allowed em here.
				{
					//ProcessWarps( out.m_SongTiming, sParams[1], out.m_fVersion );
					LOG->Trace( "Found WARPS tag" );
				}
				
				else if( sValueName=="LABELS" && !bSplit )
				{
					//ProcessLabels( out.m_SongTiming, sParams[1] );
					LOG->Trace( "Found Labels tag" );
				}

				else if( sValueName=="TIMESIGNATURES" && !bSplit )
				{
					vector<RString> vs1;
					split( sParams[1], ",", vs1 );

					FOREACH_CONST( RString, vs1, s1 )
					{
						vector<RString> vs2;
						split( *s1, "=", vs2 );

						if( vs2.size() < 3 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with %i values.", (int)vs2.size() );
							continue;
						}

						const float fBeat = StringToFloat( vs2[0] );

						TimeSignatureSegment seg;
						seg.m_iStartRow = BeatToNoteRow(fBeat);
						seg.m_iNumerator = atoi( vs2[1] ); 
						seg.m_iDenominator = atoi( vs2[2] ); 
						
						if( fBeat < 0 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with beat %f.", fBeat );
							continue;
						}
						
						if( seg.m_iNumerator < 1 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with beat %f, iNumerator %i.", fBeat, seg.m_iNumerator );
							continue;
						}

						if( seg.m_iDenominator < 1 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with beat %f, iDenominator %i.", fBeat, seg.m_iDenominator );
							continue;
						}

						out.m_Timing.AddTimeSignatureSegment( seg );
					}
				}

				else if( sValueName=="TICKCOUNT" && !bSplit )
				{
					vector<RString> arrayTickChangeExpressions;
					split( sParams[1], ",", arrayTickChangeExpressions );

					for( unsigned b=0; b<arrayTickChangeExpressions.size(); b++ )
					{
						vector<RString> arrayTickChangeValues;
						split( arrayTickChangeExpressions[b], "=", arrayTickChangeValues );
						// XXX: Hard to tell which file caused this.
						if( arrayTickChangeValues.size() != 2 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayTickChangeExpressions[b].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayTickChangeValues[0] );
						const float fNewTickcount = StringToFloat( arrayTickChangeValues[1] );
						
						if( fNewTickcount > -1.0f )
							out.AddTickSegment( TickSegment(BeatToNoteRow(fBeat), fNewTickcount) );
						else
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid tickcount change at beat %f, tickcount %f.", fBeat, fNewTickcount );
					}
				}
				//tag speeds, tiene: beat=[0,1]velocidad=approach=?? (siempre 0)
				else if( sValueName=="SPEED" && !bSplit )
				{
					vector<RString> arrayArrowSpacingChangeExpressions;
					split( sParams[1], ",", arrayArrowSpacingChangeExpressions );

					for( unsigned b=0; b<arrayArrowSpacingChangeExpressions.size(); b++ )
					{
						vector<RString> arrayArrowSpacingChangeValues;
						split( arrayArrowSpacingChangeExpressions[b], "=", arrayArrowSpacingChangeValues );
						float fNewFactor = 0.5f;
						// XXX: Hard to tell which file caused this.
						if( arrayArrowSpacingChangeValues.size() == 2 )
						{
							;
						}
						else if( arrayArrowSpacingChangeValues.size() > 2 )
						{
							fNewFactor = StringToFloat( arrayArrowSpacingChangeValues[2] );
						}
						else 
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #SPEED value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayArrowSpacingChangeValues[0] );
						const float fNew = StringToFloat( arrayArrowSpacingChangeValues[1] );

						//float fNew = SCALE( fNewArrowSpacing, 0, 2, 0, 128 );						
						
						if( fNew >= 0.0f )
							pAuxTiming->AddSpeedSegment( SpeedSegment(BeatToNoteRow(fBeat), fNew, fNewFactor, 1) );
						else
							LOG->UserLog( "Song file", sPath, "has an invalid speed change at beat %f, speed %f.", fBeat, fNew );
					}
				}

				else if( sValueName=="MULTIPLIER" && !bSplit )
				{
					vector<RString> arrayComboExpressions;
					split( sParams[1], ",", arrayComboExpressions );
					
					for( unsigned f=0; f<arrayComboExpressions.size(); f++ )
					{
						vector<RString> arrayComboValues;
						split( arrayComboExpressions[f], "=", arrayComboValues );
						unsigned size = arrayComboValues.size();
						if( size < 2 )
						{
							LOG->UserLog("Song file",
									sPath,
									 "has an invalid #COMBOS value \"%s\" (must have at least one '='), ignored.",
									 arrayComboExpressions[f].c_str() );
							continue;
						}
						const float fComboBeat = StringToFloat( arrayComboValues[0] );
						const int iCombos = StringToInt( arrayComboValues[1] );
						const int iMisses = (size == 2 ? iCombos : StringToInt(arrayComboValues[2]));
						out.AddComboSegment( ComboSegment( fComboBeat, iCombos, iMisses ) );
					}
				}

				// This tag will get us to the next section.
				else if( sValueName=="OFFSET" && bSplit )
				{
					state = SMA_GETTING_STEP_INFO;
					pNewNotes = new Steps();
					//stepsTiming = TimingData( out.m_SongTiming.m_fBeat0OffsetInSeconds );
					//bHasOwnTiming = false;
					pAuxTiming = new TimingData();
					tt = TimingType_Invalid;					
				}
				break;				
			}//case
			case SMA_GETTING_STEP_INFO:
			{			
				if( sValueName=="NOTES" || sValueName=="NOTES2" )
				{
					state = SMA_GETTING_SONG_INFO;

					/*
					if( iNumParams < 7 )
					{
						LOG->UserLog( "Song file", sPath, "has %d fields in a #NOTES tag, but should have at least 7.", iNumParams );
						continue;
					}
					

					//Steps* pNewNotes = new Steps;

					pNewNotes->SetSMNoteData( sParams[1] );
					pNewNotes->TidyUpData();
					out.AddSteps( pNewNotes );
					*/

					if( iNumParams < 7 )
					{
						LOG->UserLog( "Song file", sPath, "has %d fields in a #NOTES tag, but should have at least 7.", iNumParams );
						continue;
					}

					//Steps* pNewNotes = new Steps;
					LoadFromSMTokens( 
						sParams[1], 
						sParams[2], 
						sParams[3], 
						sParams[4], 
						sParams[5], 
						sParams[6],
						*pNewNotes );

					pNewNotes->m_StepsTiming = *pAuxTiming;
					out.AddSteps( pNewNotes );

					/*
					switch( pNewNotes->m_StepsType ) 
					{
						case STEPS_TYPE_PUMP_SINGLE:
						{
							if( pNewNotes->GetDifficulty() == Difficulty_Easy )
								tt = NORMAL;
							else if(pNewNotes->GetDifficulty() == Difficulty_Medium)
								tt = HARD;
							else if(pNewNotes->GetDifficulty() == Difficulty_Hard)
								tt = CRAZY;
							else
								tt = TimingType_Invalid;

							break;
						}
						case STEPS_TYPE_PUMP_DOUBLE: 
						{
							if(pNewNotes->GetDifficulty() == Difficulty_Medium)
								tt = FREESTYLE;
							else if(pNewNotes->GetDifficulty() == Difficulty_Hard)
								tt = NIGHTMARE;
							else
								tt = TimingType_Invalid;

							break;
						}
					}

					if( tt != TimingType_Invalid )
						out.m_TimingExtra[tt] = *pAuxTiming;
					else
						out.m_Timing = *pAuxTiming;
					
					//LOG->Trace( "Cargando TimingType: %s", TimingTypeToString( tt ).c_str() );
					*/

				}
				
				else if( sValueName=="BPMS" && bSplit )
				{
					vector<RString> arrayBPMChangeExpressions;
					split( sParams[1], ",", arrayBPMChangeExpressions );

					for( unsigned b=0; b<arrayBPMChangeExpressions.size(); b++ )
					{
						vector<RString> arrayBPMChangeValues;
						split( arrayBPMChangeExpressions[b], "=", arrayBPMChangeValues );
						// XXX: Hard to tell which file caused this.
						if( arrayBPMChangeValues.size() != 2 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayBPMChangeExpressions[b].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayBPMChangeValues[0] );
						const float fNewBPM = StringToFloat( arrayBPMChangeValues[1] );
												
						//if( tt != TimingType_Invalid )
							pAuxTiming->AddBPMSegment( BPMSegment(BeatToNoteRow(fBeat), fNewBPM) );						
						//else
						//	out.m_Timing.AddBPMSegment( BPMSegment(BeatToNoteRow(fBeat), fNewBPM) );
					}
				}
				
				else if( sValueName=="STOPS" && bSplit )
				{
					vector<RString> arrayFreezeExpressions;
					split( sParams[1], ",", arrayFreezeExpressions );

					for( unsigned f=0; f<arrayFreezeExpressions.size(); f++ )
					{
						vector<RString> arrayFreezeValues;
						split( arrayFreezeExpressions[f], "=", arrayFreezeValues );
						if( arrayFreezeValues.size() != 2 )
						{
							// XXX: Hard to tell which file caused this.
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayFreezeExpressions[f].c_str() );
							continue;
						}

						const float fFreezeBeat = StringToFloat( arrayFreezeValues[0] );
						const float fFreezeSeconds = StringToFloat( arrayFreezeValues[1] );
						
						StopSegment new_seg( BeatToNoteRow(fFreezeBeat), fFreezeSeconds );

		//				LOG->Trace( "Adding a freeze segment: beat: %f, seconds = %f", new_seg.m_fStartBeat, new_seg.m_fStopSeconds );

						
						//if( tt != TimingType_Invalid )
							pAuxTiming->AddStopSegment( new_seg );					
						//else
						//	out.m_Timing.AddStopSegment( StopSegment(new_seg) );
					}
				}
				
				else if( sValueName=="DELAYS"  && bSplit )
				{
					vector<RString> arrayFreezeExpressions;
					split( sParams[1], ",", arrayFreezeExpressions );

					for( unsigned f=0; f<arrayFreezeExpressions.size(); f++ )
					{
						vector<RString> arrayFreezeValues;
						split( arrayFreezeExpressions[f], "=", arrayFreezeValues );
						if( arrayFreezeValues.size() != 2 )
						{
							// XXX: Hard to tell which file caused this.
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayFreezeExpressions[f].c_str() );
							continue;
						}

						const float fFreezeBeat = StringToFloat( arrayFreezeValues[0] );
						const float fFreezeSeconds = StringToFloat( arrayFreezeValues[1] );
						
						StopSegment new_seg( BeatToNoteRow(fFreezeBeat), fFreezeSeconds, true );

		//				LOG->Trace( "Adding a freeze segment: beat: %f, seconds = %f", new_seg.m_fStartBeat, new_seg.m_fStopSeconds );

						
						//if( tt != TimingType_Invalid )
							pAuxTiming->AddStopSegment( new_seg );					
						//else
						//	out.m_Timing.AddStopSegment( StopSegment(new_seg) );
					}
					
					/*if( tt != TimingType_Invalid )
						out.m_UsePiuStops[tt] = true;					
					else
						out.m_bUsePiuStops = true;*/
				}
				
				else if( sValueName=="TIMESIGNATURES"  && bSplit )
				{
					vector<RString> vs1;
					split( sParams[1], ",", vs1 );

					FOREACH_CONST( RString, vs1, s1 )
					{
						vector<RString> vs2;
						split( *s1, "=", vs2 );

						if( vs2.size() < 3 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with %i values.", (int)vs2.size() );
							continue;
						}

						const float fBeat = StringToFloat( vs2[0] );

						TimeSignatureSegment seg;
						seg.m_iStartRow = BeatToNoteRow(fBeat);
						seg.m_iNumerator = atoi( vs2[1] ); 
						seg.m_iDenominator = atoi( vs2[2] ); 
						
						if( fBeat < 0 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with beat %f.", fBeat );
							continue;
						}
						
						if( seg.m_iNumerator < 1 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with beat %f, iNumerator %i.", fBeat, seg.m_iNumerator );
							continue;
						}

						if( seg.m_iDenominator < 1 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid time signature change with beat %f, iDenominator %i.", fBeat, seg.m_iDenominator );
							continue;
						}

						
						//if( tt != TimingType_Invalid )
							pAuxTiming->AddTimeSignatureSegment( seg );			
						//else
						//	out.m_Timing.AddTimeSignatureSegment( TimeSignatureSegment(seg) );
					}
				}
				
				else if( sValueName=="TICKCOUNT" && bSplit )
				{
					LOG->Trace( "Cargando TimingType: %s, con valores: %s", TimingTypeToString( tt ).c_str(), sParams[1].c_str() );
					vector<RString> arrayTickChangeExpressions;
					split( sParams[1], ",", arrayTickChangeExpressions );

					for( unsigned b=0; b<arrayTickChangeExpressions.size(); b++ )
					{
						vector<RString> arrayTickChangeValues;
						split( arrayTickChangeExpressions[b], "=", arrayTickChangeValues );
						// XXX: Hard to tell which file caused this.
						if( arrayTickChangeValues.size() != 2 )
						{
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid #%s value \"%s\" (must have exactly one '='), ignored.",
									  sValueName.c_str(), arrayTickChangeExpressions[b].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayTickChangeValues[0] );
						const float fNewTickcount = StringToFloat( arrayTickChangeValues[1] );
						
						if( fNewTickcount > -1.0f )
						{							
							//if( tt != TimingType_Invalid )
								pAuxTiming->AddTickSegment( TickSegment(BeatToNoteRow(fBeat), fNewTickcount) );				
							//else
							//	out.m_Timing.AddTickSegment( TickSegment(BeatToNoteRow(fBeat), fNewTickcount) );
						}
						else
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid tickcount change at beat %f, tickcount %f.", fBeat, fNewTickcount );
					}
				}
				
				else if( sValueName=="MULTIPLIER" && bSplit )
				{
					vector<RString> arrayComboExpressions;
					split( sParams[1], ",", arrayComboExpressions );
					
					for( unsigned f=0; f<arrayComboExpressions.size(); f++ )
					{
						vector<RString> arrayComboValues;
						split( arrayComboExpressions[f], "=", arrayComboValues );
						unsigned size = arrayComboValues.size();
						if( size < 2 )
						{
							LOG->UserLog("Song file",
									sPath,
									 "has an invalid #COMBOS value \"%s\" (must have at least one '='), ignored.",
									 arrayComboExpressions[f].c_str() );
							continue;
						}
						const float fComboBeat = StringToFloat( arrayComboValues[0] );
						const int iCombos = StringToInt( arrayComboValues[1] );
						const int iMisses = (size == 2 ? iCombos : StringToInt(arrayComboValues[2]));
						pAuxTiming->AddComboSegment( ComboSegment( fComboBeat, iCombos, iMisses ) );
					}
				}
				
				else if( sValueName=="WARPS" && bSplit  )
				{
					//ProcessWarps(stepsTiming, sParams[1], out.m_fVersion);
					LOG->Trace( "Found Split Warps tag" );
				}
				
				else if( sValueName=="SPEED" && bSplit  )
				{
					//ProcessScrolls( stepsTiming, sParams[1] );
					//LOG->Trace( "Found SCROLLS Combos tag" );
					vector<RString> arrayArrowSpacingChangeExpressions;
					split( sParams[1], ",", arrayArrowSpacingChangeExpressions );

					for( unsigned b=0; b<arrayArrowSpacingChangeExpressions.size(); b++ )
					{
						vector<RString> arrayArrowSpacingChangeValues;
						split( arrayArrowSpacingChangeExpressions[b], "=", arrayArrowSpacingChangeValues );
						float fNewFactor = 0;
						// XXX: Hard to tell which file caused this.
						if( arrayArrowSpacingChangeValues.size() == 2 )
						{
							;
						}
						else if( arrayArrowSpacingChangeValues.size() > 2 )
						{
							fNewFactor = StringToFloat( arrayArrowSpacingChangeValues[2] );
						}
						else 
						{
							LOG->UserLog( "Song file", sPath, "has an unknown #SPEED value, \"%s\"; ignored.", sParams[1].c_str() );
							continue;
						}

						const float fBeat = StringToFloat( arrayArrowSpacingChangeValues[0] );
						const float fNew = StringToFloat( arrayArrowSpacingChangeValues[1] );

						//float fNew = SCALE( fNewArrowSpacing, 0, 2, 0, 128 );
						//LOG->Trace( "Split Scaling ArrowSpacing: %.2f to %.2f", fNewArrowSpacing, fNew );
						
						if( fNew >= 0.0f )							
							//if( tt != TimingType_Invalid )
								pAuxTiming->AddSpeedSegment( SpeedSegment(BeatToNoteRow(fBeat), fNew, fNewFactor, 1) );
							//else
							//	out.m_Timing.AddArrowSpacingSegment( ArrowSpacingSegment(BeatToNoteRow(fBeat), fNew) );
						else
							LOG->UserLog( "Song file", "(UNKNOWN)", "has an invalid speed change at beat %f, speed %f.", fBeat, fNew );
					}
				}
				
				else if( sValueName=="FAKES"  && bSplit )
				{
					vector<RString> arrayFakeExpressions;
					split( sParams[1], ",", arrayFakeExpressions );
					
					for( unsigned b=0; b<arrayFakeExpressions.size(); b++ )
					{
						vector<RString> arrayFakeValues;
						split( arrayFakeExpressions[b], "=", arrayFakeValues );
						if( arrayFakeValues.size() != 2 )
						{
							LOG->UserLog("Song file",
								out.GetSongFilePath(),
									 "has an invalid #FAKES value \"%s\" (must have exactly one '='), ignored.",
									 arrayFakeExpressions[b].c_str() );
							continue;
						}
						
						const float fBeat = SMALoader::RowToBeat( arrayFakeValues[0], 192 /* rowsPerBeat */ );
						const float fNewBeat = StringToFloat( arrayFakeValues[1] );
						
						if(fNewBeat > 0)	
						{
							//if( tt != TimingType_Invalid )
							//	out.m_TimingExtra[tt].AddFakeSegment( FakeSegment(BeatToNoteRow(fBeat), fNewBeat) );						
							//else
								pAuxTiming->AddFakeSegment( FakeSegment(fBeat, fNewBeat) );						
						}
						else
						{
							LOG->UserLog("Song file",
								out.GetSongFilePath(),
									 "has an invalid Fake at beat %f, BPM %f.",
									 fBeat, fNewBeat );
						}
					}
				}
				
				else if( sValueName=="LABELS"  && bSplit )
				{
					//ProcessLabels(stepsTiming, sParams[1]);
					LOG->Trace( "Found Split LABELS tag" );
				}
				
				else if( sValueName=="ATTACKS"  && bSplit )
				{
					//ProcessAttackString(pNewNotes->m_sAttackString, sParams);
					//ProcessAttacks(pNewNotes->m_Attacks, sParams);
					Attack attack;
					float end = -9999;
					int iNumAttacks = 0;
					for( unsigned j = 1; j < sParams.params.size(); ++j )
					{
						vector<RString> sBits;
						split( sParams[j], "=", sBits, false );
						if( sBits.size() < 2 )
							continue;

						Trim( sBits[0] );
						if( !sBits[0].CompareNoCase("TIME") )
							attack.fStartSecond = max( StringToFloat(sBits[1]), 0.0f );
						else if( !sBits[0].CompareNoCase("LEN") )
							attack.fSecsRemaining = StringToFloat( sBits[1] );
						else if( !sBits[0].CompareNoCase("END") )
							end = StringToFloat( sBits[1] );
						else if( !sBits[0].CompareNoCase("MODS") )
						{
							attack.sModifiers = sBits[1];
							
							if( end != -9999 )
							{
								attack.fSecsRemaining = end - attack.fStartSecond;
								end = -9999;
							}

							if( attack.fSecsRemaining <= 0.0f)
							{
								LOG->UserLog( "Song file", sPath, "has an attack with a nonpositive length: %s", sBits[1].c_str() );
								attack.fSecsRemaining = 0.0f;
							}
							
							// warn on invalid so we catch typos on load
							//CourseUtil::WarnOnInvalidMods( attack.sModifiers );

							attacks.push_back( attack );
							//out.m_Attacks.push_back( attack );
							iNumAttacks++;

							if( iNumAttacks > 0 )
								pNewNotes->m_bHasAttacks = true;
						}
						else
						{
							LOG->UserLog( "Song file", sPath, "has an unexpected value named '%s'", sBits[0].c_str() );
						}

						pNewNotes->m_Attacks = attacks;

						
						/*if( tt != TimingType_Invalid )
							out.m_AttacksExtra[tt] = attacks;
						else
							out.m_Attacks = attacks;*/
					}
				}
				
				else if( sValueName=="OFFSET" && bSplit )
				{
					//LOG->Trace( "Cargando OFFSET: %s", sParams[1].c_str() );
					
					//stepsTiming.m_fBeat0OffsetInSeconds = StringToFloat( sParams[1] );					
					//if( tt != TimingType_Invalid )
						pAuxTiming->m_fBeat0OffsetInSeconds = atof( sParams[1] );
					//else
					//	out.m_Timing.m_fBeat0OffsetInSeconds = atof( sParams[1] );
				}				
				break;
			}
		}
	}

	return true;
}


bool SMALoader::LoadFromDir( const RString &sPath, Song &out )
{
	vector<RString> aFileNames;
	GetApplicableFiles( sPath, aFileNames );

	if( aFileNames.size() > 1 )
	{
		LOG->UserLog( "Song", sPath, "has more than one SM file. There should be only one!" );
		return false;
	}

	/* We should have exactly one; if we had none, we shouldn't have been
	 * called to begin with. */
	ASSERT( aFileNames.size() == 1 );

	return LoadFromSMAFile( sPath + aFileNames[0], out );
}

bool SMALoader::LoadEditFromFile( RString sEditFilePath, ProfileSlot slot, bool bAddStepsToSong )
{
	LOG->Trace( "SMALoader::LoadEditFromFile(%s)", sEditFilePath.c_str() );

	int iBytes = FILEMAN->GetFileSizeInBytes( sEditFilePath );
	if( iBytes > MAX_EDIT_STEPS_SIZE_BYTES )
	{
		LOG->UserLog( "Edit file", sEditFilePath, "is unreasonably large. It won't be loaded." );
		return false;
	}

	MsdFile msd;
	if( !msd.ReadFile( sEditFilePath, true ) )  // unescape
	{
		LOG->UserLog( "Edit file", sEditFilePath, "couldn't be opened: %s", msd.GetError().c_str() );
		return false;
	}

	return LoadEditFromMsd( msd, sEditFilePath, slot, bAddStepsToSong );
}

bool SMALoader::LoadEditFromBuffer( const RString &sBuffer, const RString &sEditFilePath, ProfileSlot slot )
{
	MsdFile msd;
	msd.ReadFromString( sBuffer, true );  // unescape
	return LoadEditFromMsd( msd, sEditFilePath, slot, true );
}

bool SMALoader::LoadEditFromMsd( const MsdFile &msd, const RString &sEditFilePath, ProfileSlot slot, bool bAddStepsToSong )
{
	Song* pSong = NULL;

	for( unsigned i=0; i<msd.GetNumValues(); i++ )
	{
		int iNumParams = msd.GetNumParams(i);
		const MsdFile::value_t &sParams = msd.GetValue(i);
		RString sValueName = sParams[0];
		sValueName.MakeUpper();

		// handle the data
		if( sValueName=="SONG" )
		{
			if( pSong )
			{
				LOG->UserLog( "Edit file", sEditFilePath, "has more than one #SONG tag." );
				return false;
			}

			RString sSongFullTitle = sParams[1];
			sSongFullTitle.Replace( '\\', '/' );

			pSong = SONGMAN->FindSong( sSongFullTitle );
			if( pSong == NULL )
			{
				LOG->UserLog( "Edit file", sEditFilePath, "requires a song \"%s\" that isn't present.", sSongFullTitle.c_str() );
				return false;
			}

			if( pSong->GetNumStepsLoadedFromProfile(slot) >= MAX_EDITS_PER_SONG_PER_PROFILE )
			{
				LOG->UserLog( "Song file", sSongFullTitle, "already has the maximum number of edits allowed for ProfileSlotP%d.", slot+1 );
				return false;
			}
		}

		else if( sValueName=="NOTES" )
		{
			if( pSong == NULL )
			{
				LOG->UserLog( "Edit file", sEditFilePath, "doesn't have a #SONG tag preceeding the first #NOTES tag." );
				return false;
			}

			if( iNumParams < 7 )
			{
				LOG->UserLog( "Edit file", sEditFilePath, "has %d fields in a #NOTES tag, but should have at least 7.", iNumParams );
				continue;
			}

			if( !bAddStepsToSong )
				return true;

			Steps* pNewNotes = new Steps;
			LoadFromSMTokens( 
				sParams[1], sParams[2], sParams[3], sParams[4], sParams[5], sParams[6],
				*pNewNotes);

			pNewNotes->SetLoadedFromProfile( slot );
			pNewNotes->SetDifficulty( Difficulty_Edit );
			pNewNotes->SetFilename( sEditFilePath );

			if( pSong->IsEditAlreadyLoaded(pNewNotes) )
			{
				LOG->UserLog( "Edit file", sEditFilePath, "is a duplicate of another edit that was already loaded." );
				SAFE_DELETE( pNewNotes );
				return false;
			}

			pSong->AddSteps( pNewNotes );
			return true;	// Only allow one Steps per edit file!
		}
		else
		{
			LOG->UserLog( "Edit file", sEditFilePath, "has an unexpected value \"%s\".", sValueName.c_str() );
		}
	}

	return true;
	
}

void SMALoader::TidyUpData( Song &song, bool bFromCache )
{
	/*
	 * Hack: if the song has any changes at all (so it won't use a random BGA)
	 * and doesn't end with "-nosongbg-", add a song background BGC.  Remove
	 * "-nosongbg-" if it exists.
	 *
	 * This way, songs that were created earlier, when we added the song BG
	 * at the end by default, will still behave as expected; all new songs will
	 * have to add an explicit song BG tag if they want it.  This is really a
	 * formatting hack only; nothing outside of SMLoader ever sees "-nosongbg-".
	 */
	vector<BackgroundChange> &bg = song.GetBackgroundChanges(BACKGROUND_LAYER_1);
	if( !bg.empty() )
	{
		/* BGChanges have been sorted.  On the odd chance that a BGChange exists
		 * with a very high beat, search the whole list. */
		bool bHasNoSongBgTag = false;

		for( unsigned i = 0; !bHasNoSongBgTag && i < bg.size(); ++i )
		{
			if( !bg[i].m_def.m_sFile1.CompareNoCase(NO_SONG_BG_FILE) )
			{
				bg.erase( bg.begin()+i );
				bHasNoSongBgTag = true;
			}
		}

		/* If there's no -nosongbg- tag, add the song BG. */
		if( !bHasNoSongBgTag ) do
		{
			/* If we're loading cache, -nosongbg- should always be in there.  We must
			 * not call IsAFile(song.GetBackgroundPath()) when loading cache. */
			if( bFromCache )
				break;

			/* If BGChanges already exist after the last beat, don't add the background
			 * in the middle. */
			if( !bg.empty() && bg.back().m_fStartBeat-0.0001f >= song.m_fLastBeat )
				break;

			/* If the last BGA is already the song BGA, don't add a duplicate. */
			if( !bg.empty() && !bg.back().m_def.m_sFile1.CompareNoCase(song.m_sBackgroundFile) )
				break;

			if( !IsAFile( song.GetBackgroundPath() ) )
				break;

			bg.push_back( BackgroundChange(song.m_fLastBeat,song.m_sBackgroundFile) );
		} while(0);
	}
}

/*
 * (c) 2001-2004 Chris Danford, Glenn Maynard
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
