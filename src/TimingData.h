/* TimingData - Holds data for translating beats<->seconds. */

#ifndef TIMING_DATA_H
#define TIMING_DATA_H

#include "NoteTypes.h"

#define COMPARE(x) if(x!=other.x) return false;

struct BPMSegment 
{
	BPMSegment() : m_iStartRow(-1), m_fBPS(-1.0f) { }
	BPMSegment( int s, float b ) { m_iStartRow = max( 0, s ); SetBPM( b ); }
	int m_iStartRow;
	float m_fBPS;

	void SetBPM( float f ) { m_fBPS = f / 60.0f; }
	float GetBPM() const { return m_fBPS * 60.0f; }

	bool operator==( const BPMSegment &other ) const
	{
		COMPARE( m_iStartRow );
		COMPARE( m_fBPS );
		return true;
	}
	bool operator!=( const BPMSegment &other ) const { return !operator==(other); }
	bool operator<( const BPMSegment &other ) const { return m_iStartRow < other.m_iStartRow; }
};

struct StopSegment 
{
	StopSegment() : m_iStartRow(-1), m_fStopSeconds(-1.0f), m_bDelay(false) { }
	StopSegment( int s, float f, bool d = false ) { m_iStartRow = max( 0, s ); m_fStopSeconds = max( 0.0f, f ); m_bDelay = d; }
	int m_iStartRow;
	float m_fStopSeconds;
	bool m_bDelay;

	bool operator==( const StopSegment &other ) const
	{
		COMPARE( m_iStartRow );
		COMPARE( m_fStopSeconds );
		COMPARE( m_bDelay );
		return true;
	}
	bool operator!=( const StopSegment &other ) const { return !operator==(other); }
	bool operator<( const StopSegment &other ) const { return m_iStartRow < other.m_iStartRow; }
};


struct TickSegment
{
	TickSegment() : m_fStartBeat (-1.0f), m_iTickcount (-1) { }
	TickSegment( float s, int t ) { m_fStartBeat = max(0.0f,s); m_iTickcount = max(0,t); };
	float m_fStartBeat;
	int m_iTickcount;

	bool operator==( const TickSegment &other ) const
	{
		COMPARE( m_fStartBeat );
		COMPARE( m_iTickcount );
		return true;
	}
	bool operator!=( const TickSegment &other ) const { return !operator==(other); }
	bool operator<( const TickSegment &other ) const { return m_fStartBeat < other.m_fStartBeat; }
};


struct ArrowSpacingSegment
{
	ArrowSpacingSegment() : m_iStartRow (0), m_fArrowSpacing (1.0f), m_fShiftFactor(0.5f), m_iUnit(0) { }
	ArrowSpacingSegment( int s, int t, float r = 0.5f ) 
	{ 
		m_iStartRow = max(0,s); 
		m_fArrowSpacing = max(0,t); 
		m_fShiftFactor = max(0,r);
	};
	int m_iStartRow;
	float m_fArrowSpacing;
	float m_fShiftFactor;
	unsigned short m_iUnit;

	bool operator==( const ArrowSpacingSegment &other ) const
	{
		COMPARE( m_iStartRow );
		COMPARE( m_fArrowSpacing );
		COMPARE( m_fShiftFactor );
		COMPARE( m_iUnit );
		return true;
	}
	bool operator!=( const ArrowSpacingSegment &other ) const { return !operator==(other); }
	bool operator<( const ArrowSpacingSegment &other ) const { return m_iStartRow < other.m_iStartRow; }
};


struct ComboSegment
{
	ComboSegment() : m_fStartBeat (0), m_iComboFactor (1), m_iMissComboFactor (1) { }
	ComboSegment( float s, int t, int m ) { m_fStartBeat = max(0.0f,s); m_iComboFactor = max(1,t); m_iMissComboFactor = max(1,m); };
	float m_fStartBeat;
	int m_iComboFactor;
	int m_iMissComboFactor;

	bool operator==( const ComboSegment &other ) const
	{
		COMPARE( m_fStartBeat );
		COMPARE( m_iComboFactor );
		COMPARE( m_iMissComboFactor );
		return true;
	}
	bool operator!=( const ComboSegment &other ) const { return !operator==(other); }
	bool operator<( const ComboSegment &other ) const { return m_fStartBeat < other.m_fStartBeat; }
};

/* cambio dinamico de noteskins */
struct NoteSkinSegment
{
	NoteSkinSegment() : m_iStartRow (0), m_sNoteSkin ("default") { }
	NoteSkinSegment( int s, RString t = "default" ) { m_iStartRow = max(0,s); m_sNoteSkin = t; };
	int m_iStartRow;
	RString m_sNoteSkin;

	bool operator==( const NoteSkinSegment &other ) const
	{
		COMPARE( m_iStartRow );
		COMPARE( m_sNoteSkin );
		return true;
	}
	bool operator!=( const NoteSkinSegment &other ) const { return !operator==(other); }
	bool operator<( const NoteSkinSegment &other ) const { return m_iStartRow < other.m_iStartRow; }
};

struct FakeSegment /* for support fakes on ssc - sma shits */
{
	FakeSegment() : m_fStartBeat (-1.0f), m_fLength (0.0f) { }
	FakeSegment( float s, float l ) { m_fStartBeat = max(0.0f,s); m_fLength = max(0.0f,l); };
	float m_fStartBeat;
	float m_fLength;

	bool operator==( const FakeSegment &other ) const
	{
		COMPARE( m_fStartBeat );
		COMPARE( m_fLength );
		return true;
	}
	bool operator!=( const FakeSegment &other ) const { return !operator==(other); }
	bool operator<( const FakeSegment &other ) const { return m_fStartBeat < other.m_fStartBeat; }
};

struct ScrollSegment /* for support scroll on ssc - sma shits */
{
	ScrollSegment() : m_iStartRow (1), m_fRatio (1.0f) { }
	ScrollSegment( int s, float l ) { m_iStartRow = max(0,s); m_fRatio = max(0.0f,l); };
	int m_iStartRow;
	float m_fRatio;

	bool operator==( const ScrollSegment &other ) const
	{
		COMPARE( m_iStartRow );
		COMPARE( m_fRatio );
		return true;
	}
	bool operator!=( const ScrollSegment &other ) const { return !operator==(other); }
	bool operator<( const ScrollSegment &other ) const { return m_iStartRow < other.m_iStartRow; }
};

struct CountSeparatelySegment 
{
	CountSeparatelySegment() : m_iStartRow (1), m_iCountSep (0) { }
	CountSeparatelySegment( int s, int l ) { m_iStartRow = max(0,s); m_iCountSep = max(0,l); };
	int m_iStartRow;
	int m_iCountSep;

	bool operator==( const CountSeparatelySegment &other ) const
	{
		COMPARE( m_iStartRow );
		COMPARE( m_iCountSep );
		return true;
	}
	bool operator!=( const CountSeparatelySegment &other ) const { return !operator==(other); }
	bool operator<( const CountSeparatelySegment &other ) const { return m_iStartRow < other.m_iStartRow; }
};

struct RequireStepOnHoldHeadSegment 
{
	RequireStepOnHoldHeadSegment() : m_iStartRow (1), m_iRequireStep (0) { }
	RequireStepOnHoldHeadSegment( int s, int l ) { m_iStartRow = max(0,s); m_iRequireStep = max(0,l); };
	int m_iStartRow;
	int m_iRequireStep;

	bool operator==( const RequireStepOnHoldHeadSegment &other ) const
	{
		COMPARE( m_iStartRow );
		COMPARE( m_iRequireStep );
		return true;
	}
	bool operator!=( const RequireStepOnHoldHeadSegment &other ) const { return !operator==(other); }
	bool operator<( const RequireStepOnHoldHeadSegment &other ) const { return m_iStartRow < other.m_iStartRow; }
};

struct SpeedSegment
{
	SpeedSegment() : m_iStartRow (0), m_fRatio (1.0f), m_fWait(0.5f), m_iUnit(0) { }
	SpeedSegment( int s, float t, float r = 0.5f, int u = 0 ) 
	{ 
		m_iStartRow = max(0,s); 
		m_fRatio = t; 
		m_fWait = max(0,r);
		m_iUnit = u;
	};
	int m_iStartRow;
	float m_fRatio;
	float m_fWait;
	unsigned short m_iUnit;

	bool operator==( const SpeedSegment &other ) const
	{
		COMPARE( m_iStartRow );
		COMPARE( m_fRatio );
		COMPARE( m_fWait );
		COMPARE( m_iUnit );
		return true;
	}
	bool operator!=( const SpeedSegment &other ) const { return !operator==(other); }
	bool operator<( const SpeedSegment &other ) const { return m_iStartRow < other.m_iStartRow; }
};

struct WarpSegment /* for support warps on ssc - sma shits */
{
	WarpSegment() : m_fStartBeat (0), m_fLength (0), m_bDynamic(false) { }
	WarpSegment( float s, float l ) { m_fStartBeat = max(0.0f,s); m_fLength = max(0.0f,l); m_bDynamic = false; };
	float m_fStartBeat;
	float m_fLength;
	bool m_bDynamic;

	bool operator==( const WarpSegment &other ) const
	{
		COMPARE( m_fStartBeat );
		COMPARE( m_fLength );
		COMPARE( m_bDynamic );
		return true;
	}
	bool operator!=( const WarpSegment &other ) const { return !operator==(other); }
	bool operator<( const WarpSegment &other ) const { return m_fStartBeat < other.m_fStartBeat; }
};


/* This only supports simple time signatures. The upper number (called the numerator here, though this isn't
 * properly a fraction) is the number of beats per measure. The lower number (denominator here)
 * the the note value representing one beat. */
struct TimeSignatureSegment 
{
	TimeSignatureSegment() : m_iStartRow(-1), m_iNumerator(4), m_iDenominator(4)  { }
	TimeSignatureSegment( int iStartRow, int iNumerator, int iDenominator ){
		m_iStartRow = max( iStartRow, 0 );
		m_iNumerator = max( 1, iNumerator );
		m_iDenominator = max( 1, iDenominator );
	}
	int m_iStartRow;
	int m_iNumerator;
	int m_iDenominator;
	
	/* With BeatToNoteRow(1) rows per beat, then we should have BeatToNoteRow(1)*m_iNumerator
	 * beats per measure. But if we assume that every BeatToNoteRow(1) rows is a quarter note,
	 * and we want the beats to be 1/m_iDenominator notes, then we should have
	 * BeatToNoteRow(1)*4 is rows per whole note and thus BeatToNoteRow(1)*4/m_iDenominator is
	 * rows per beat. Multiplying by m_iNumerator gives rows per measure. */
	int GetNoteRowsPerMeasure() const { return BeatToNoteRow(1) * 4 * m_iNumerator / m_iDenominator; }

	bool operator==( const TimeSignatureSegment &other ) const
	{
		COMPARE( m_iStartRow );
		COMPARE( m_iNumerator );
		COMPARE( m_iDenominator );
		return true;
	}
	bool operator!=( const TimeSignatureSegment &other ) const { return !operator==(other); }
	bool operator<( const TimeSignatureSegment &other ) const { return m_iStartRow < other.m_iStartRow; }
};

/* cambio dinamico de noteskins */
struct LabelSegment
{
	LabelSegment() : m_iStartRow (0), m_sLabel ("") { }
	LabelSegment( int s, RString t = "" ) { m_iStartRow = max(0,s); m_sLabel = t; };
	int m_iStartRow;
	RString m_sLabel;

	bool operator==( const LabelSegment &other ) const
	{
		COMPARE( m_iStartRow );
		COMPARE( m_sLabel );
		return true;
	}
	bool operator!=( const LabelSegment &other ) const { return !operator==(other); }
	bool operator<( const LabelSegment &other ) const { return m_iStartRow < other.m_iStartRow; }
};

class TimingData
{
public:
	TimingData();

	void GetActualBPM( float &fMinBPMOut, float &fMaxBPMOut ) const;
	float GetBPMAtBeat( float fBeat ) const;
	bool DoesLabelExist( RString sLabel ) const;

	void SetBPMAtRow( int iNoteRow, float fBPM );
	void SetBPMAtBeat( float fBeat, float fBPM ) { SetBPMAtRow( BeatToNoteRow(fBeat), fBPM ); }
	void SetStopAtRow( int iNoteRow, float fSeconds, bool bDelay = false );
	void SetStopAtBeat( float fBeat, float fSeconds, bool bDelay = false ) { SetStopAtRow( BeatToNoteRow(fBeat), fSeconds, bDelay ); }
	float GetStopAtRow( int iNoteRow, bool bDelay /* select delays */ );
	float GetStopAtBeat( float fBeat, bool bDelay ) { return GetStopAtRow(BeatToNoteRow(fBeat), bDelay ); }

	void MultiplyBPMInBeatRange( int iStartIndex, int iEndIndex, float fFactor );
	void AddBPMSegment( const BPMSegment &seg );
	void AddStopSegment( const StopSegment &seg );
	void AddTimeSignatureSegment( const TimeSignatureSegment &seg );
	int GetBPMSegmentIndexAtBeat( float fBeat );
	int GetTickSegmentIndexAtBeat( float fBeat );
	BPMSegment& GetBPMSegmentAtBeat( float fBeat );
	TimeSignatureSegment& GetTimeSignatureSegmentAtBeat( float fBeat );
	void SetTimeSignatureAtBeat( float fBeat, int iNumerator, int iDenominator );
	void NoteRowToMeasureAndBeat( int iNoteRow, int &iMeasureIndexOut, int &iBeatIndexOut, int &iRowsRemainder ) const;

	int GetTickcountAtBeat( float fBeat ) const;
	int GetTickSegmentNumber ( float fBeat ) const;
	float GetTickSegmentStart ( int iSegment ) const;
	void SetTickcountAtBeat( float fBeat, int iTickcount );
	void AddTickSegment( const TickSegment &seg );
	TickSegment& GetTickSegmentAtBeat( float fBeat );
	bool HasTickChanges() const;

	//combo
	int GetComboFactorAtBeat( float fBeat ) const;
	int GetComboSegmentNumber ( float fBeat ) const;
	float GetComboSegmentStart ( int iSegment ) const;
	void SetComboFactorAtBeat( float fBeat, int iComboFactor, int iMissComboFactor );
	void AddComboSegment( const ComboSegment &seg );
	ComboSegment& GetComboFactorSegmentAtBeat( float fBeat );
	int GetComboSegmentIndexAtBeat( float fBeat );
	//misscombo
	int GetMissComboFactorAtBeat( float fBeat ) const;			
	bool HasComboFactorChanges() const;

	int GetArrowSpacingAtBeat( float fBeat, float &fShiftFactorOut ) const;
	int GetArrowSpacingSegmentNumber ( float fBeat ) const;
	float GetArrowSpacingSegmentStart ( int iSegment ) const;
	void SetArrowSpacingAtBeat( float fBeat, float fArrowSpacing, float fShiftFactor = 0.5f );
	void AddArrowSpacingSegment( const ArrowSpacingSegment &seg );
	ArrowSpacingSegment& GetArrowSpacingSegmentAtBeat( float fBeat );
	int GetArrowSpacingSegmentIndexAtBeat( float fBeat );
	bool HasArrowSpacingChanges() const;

	RString GetNoteSkinAtBeat( float fBeat ) const;
	int GetNoteSkinSegmentNumber ( float fBeat ) const;
	float GetNoteSkinSegmentStart ( int iSegment ) const;
	void SetNoteSkinAtBeat( float fBeat, RString sNoteSkin );
	void AddNoteSkinSegment( const NoteSkinSegment &seg );
	NoteSkinSegment& GetNoteSkinSegmentAtBeat( float fBeat );
	int GetNoteSkinSegmentIndexAtBeat( float fBeat );

	RString GetLabelAtBeat( float fBeat ) const;
	int GetLabelSegmentNumber ( float fBeat ) const;
	int GetLabelSegmentStart ( int iSegment ) const;
	void SetLabelAtBeat( float fBeat, RString sLabel );
	void AddLabelSegment( const LabelSegment &seg );
	LabelSegment& GetLabelSegmentAtBeat( float fBeat );
	int GetLabelSegmentIndexAtBeat( float fBeat );

	/* return the length of a fake */
	float GetFakeAtBeat( float fBeat ) const;
	int GetFakeSegmentNumber ( float fBeat ) const;
	float GetFakeSegmentStart ( int iSegment ) const;
	void SetFakeAtBeat( float fBeat, float fLength );
	void AddFakeSegment( const FakeSegment &seg );
	FakeSegment& GetFakeSegmentAtBeat( float fBeat );
	int GetFakeSegmentIndexAtBeat( float fBeat );
	bool IsFakeAtRow( int iRow );

	/* return the length of a fake */
	float GetWarpAtBeat( float fBeat ) const;
	int GetWarpSegmentNumber ( float fBeat ) const;
	float GetWarpSegmentStart ( int iSegment ) const;
	void SetWarpAtBeat( float fBeat, float fLength, bool bDynamic = false );
	void AddWarpSegment( const WarpSegment &seg );
	WarpSegment& GetWarpSegmentAtBeat( float fBeat ) const;
	int GetWarpSegmentIndexAtBeat( float fBeat ) const;
	bool IsWarpAtRow( int iRow );

	float GetScrollAtBeat( float fBeat ) const;
	int GetScrollSegmentNumber ( float fBeat ) const;
	float GetScrollSegmentStart ( int iSegment ) const;
	void SetScrollAtBeat( float fBeat, float fLength );
	void AddScrollSegment( const ScrollSegment &seg );
	ScrollSegment& GetScrollSegmentAtBeat( float fBeat );
	int GetScrollSegmentIndexAtBeat( float fBeat );

	/* ssc style speed implementation */
	float GetSpeedAtBeat( float fBeat ) const;
	int GetSpeedSegmentNumber ( float fBeat ) const;
	float GetSpeedSegmentStart ( int iSegment ) const;
	void SetSpeedAtBeat( float fBeat, float fRatio, float fWait, unsigned short iUnit );
	void AddSpeedSegment( const SpeedSegment &seg );
	SpeedSegment& GetSpeedSegmentAtBeat( float fBeat );
	int GetSpeedSegmentIndexAtBeat( float fBeat );

	bool GetCountSepAtBeat( float fBeat ) const; /* get if count separately at beat */
	int GetCountSepSegmentNumber ( float fBeat ) const;
	float GetCountSepSegmentStart ( int iSegment ) const;
	void SetCountSepAtBeat( float fBeat, int iCountSep );
	void AddCountSepSegment( const CountSeparatelySegment &seg );
	CountSeparatelySegment& GetCountSepSegmentAtBeat( float fBeat );
	int GetCountSepSegmentIndexAtBeat( float fBeat );

	bool GetReqHoldHeadAtBeat( float fBeat ) const; /* get if count separately at beat */
	int GetReqHoldHeadSegmentNumber ( float fBeat ) const;
	float GetReqHoldHeadSegmentStart ( int iSegment ) const;
	void SetReqHoldHeadAtBeat( float fBeat, int iRequireHoldHead );
	void AddReqHoldHeadSegment( const RequireStepOnHoldHeadSegment &seg );
	RequireStepOnHoldHeadSegment& GetReqHoldHeadSegmentAtBeat( float fBeat );
	int GetReqHoldHeadSegmentIndexAtBeat( float fBeat );

	/* Scroll utility */
	float GetDisplayedBeat( float fBeat ) const;
	/* Speed Utility */
	float GetDisplayedSpeedPercent( float fSongBeat, float fMusicSeconds );

	bool IsJudgableAtRow( int iRow ) { return !(IsFakeAtRow( iRow ) || IsWarpAtRow( iRow )); }

	void GetBeatAndBPSFromElapsedTime( float fElapsedTime, float &fBeatOut, float &fBPSOut, bool &bFreezeOut, bool &bDelayOut, int &iWarpBeginOut, float &fWarpDestOut ) const;
	float GetBeatFromElapsedTime( float fElapsedTime ) const	// shortcut for places that care only about the beat
	{
		float fBeat, fThrowAway, fThrowAway2;
		bool bThrowAway, bThrowAway2;
		int iThrowAway;
		GetBeatAndBPSFromElapsedTime( fElapsedTime, fBeat, fThrowAway, bThrowAway, bThrowAway2, iThrowAway, fThrowAway2 );
		return fBeat;
	}
	float GetElapsedTimeFromBeat( float fBeat ) const;

	void GetBeatAndBPSFromElapsedTimeNoOffset( float fElapsedTime, float &fBeatOut, float &fBPSOut, bool &bFreezeOut ) const;
	float GetBeatFromElapsedTimeNoOffset( float fElapsedTime ) const	// shortcut for places that care only about the beat
	{
		float fBeat, fThrowAway, fWta;
		bool bThrowAway, bdTA;
		int iWta;
		GetBeatAndBPSFromElapsedTimeNoOffsetSSC( fElapsedTime, fBeat, fThrowAway, bThrowAway, bdTA, iWta, fWta );
		return fBeat;
	}
	float GetElapsedTimeFromBeatNoOffset( float fBeat ) const;

	/* SSC style testing */
	float GetElapsedTimeFromBeatNoOffsetSSC( float fBeat ) const;
	void GetBeatAndBPSFromElapsedTimeNoOffsetSSC( float fElapsedTime, float &fBeatOut, float &fBPSOut, bool &bFreezeOut, bool &bDelayOut, int &iWarpBeginOut, float &fWarpDestinationOut ) const;

	bool HasBpmChanges() const;
	bool HasStops() const;
	bool HasNoteSkinChanges() const;
	void GetAllNoteSkinUsed( vector<RString> &sAddTo ) const;

	bool operator==( const TimingData &other )
	{
		COMPARE( m_BPMSegments.size() );

		for( unsigned i=0; i<m_BPMSegments.size(); i++ )
			COMPARE( m_BPMSegments[i] );

		COMPARE( m_StopSegments.size() );

		for( unsigned i=0; i<m_StopSegments.size(); i++ )
			COMPARE( m_StopSegments[i] );
		
		for( unsigned i=0; i<m_TickSegments.size(); i++ )
			COMPARE( m_TickSegments[i] );

		for( unsigned i=0; i<m_ArrowSpacingSegments.size(); i++ )
			COMPARE( m_ArrowSpacingSegments[i] );
		
		for( unsigned i=0; i<m_ComboSegments.size(); i++ )
			COMPARE( m_ComboSegments[i] );

		for( unsigned i=0; i<m_NoteSkinSegments.size(); i++ )
			COMPARE( m_NoteSkinSegments[i] );

		for( unsigned i=0; i<m_FakeSegments.size(); i++ )
			COMPARE( m_FakeSegments[i] );

		for( unsigned i=0; i<m_ScrollSegments.size(); i++ )
			COMPARE( m_ScrollSegments[i] );

		for( unsigned i=0; i<m_SpeedSegments.size(); i++ )
			COMPARE( m_SpeedSegments[i] );

		for( unsigned i=0; i<m_WarpSegments.size(); i++ )
			COMPARE( m_WarpSegments[i] );

		for( unsigned i=0; i<m_LabelSegments.size(); i++ )
			COMPARE( m_LabelSegments[i] );

		for( unsigned i=0; i<m_CSepSegments.size(); i++ )
			COMPARE( m_CSepSegments[i] );

		for( unsigned i=0; i<m_ReqHoldHeadSegments.size(); i++ )
			COMPARE( m_ReqHoldHeadSegments[i] );		

		COMPARE( m_fBeat0OffsetInSeconds );
		return true;
	}
	bool operator!=( const TimingData &other ) { return !operator==(other); }

	void ScaleRegion( float fScale = 1, int iStartRow = 0, int iEndRow = MAX_NOTE_ROW, bool bAdjustBPM = false  );
	void InsertRows( int iStartRow, int iRowsToAdd );
	void DeleteRows( int iStartRow, int iRowsToDelete );

	template<typename T>
	inline T ScalePosition( T start, T length, T newLength, T position )
	{
		if( position < start )
			return position;
		if( position >= start + length )
			return position - length + newLength;
		return start + (position - start) * newLength / length;
	}
	void Scale( int start, int length, int newLength, 
		vector<SpeedSegment>& speed, 
		vector<ScrollSegment>& scroll,
		vector<NoteSkinSegment>& skin,
		vector<WarpSegment>& warp, 
		vector<FakeSegment>& fake,
		vector<BPMSegment>& bpm,
		vector<StopSegment>& stop,
		vector<ComboSegment>& combo,
		vector<ArrowSpacingSegment>& arrow,
		vector<TickSegment>& tick,
		vector<LabelSegment>& label,
		vector<CountSeparatelySegment>& csep);

	RString				m_sFile;		// informational only

	vector<BPMSegment>				m_BPMSegments;	// this must be sorted before gameplay
	vector<StopSegment>				m_StopSegments;	// this must be sorted before gameplay
	vector<TimeSignatureSegment>	m_vTimeSignatureSegments;	// this must be sorted before gameplay
	vector<TickSegment>				m_TickSegments;
	vector<NoteSkinSegment>			m_NoteSkinSegments;
	vector<ArrowSpacingSegment>		m_ArrowSpacingSegments;
	vector<ComboSegment>			m_ComboSegments;
	vector<FakeSegment>				m_FakeSegments;
	vector<ScrollSegment>			m_ScrollSegments;
	vector<SpeedSegment>			m_SpeedSegments;
	vector<WarpSegment>				m_WarpSegments;
	vector<LabelSegment>			m_LabelSegments;
	vector<CountSeparatelySegment>	m_CSepSegments; /* count separately segments */
	vector<RequireStepOnHoldHeadSegment> m_ReqHoldHeadSegments;

	float	m_fBeat0OffsetInSeconds;
};

#undef COMPARE

#endif

/*
 * (c) 2001-2004 Chris Danford, Glenn Maynard
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
