#include "global.h"
#include "GameConstantsAndTypes.h"
#include "RageLog.h"
#include "GameState.h"
#include "XmlFile.h"
#include "ActorUtil.h"
#include "RageUtil.h"
#include "Steps.h"
#include "NetworkSyncManager.h"
#include "NetPlayerInfo.h"

REGISTER_ACTOR_CLASS( NetPlayerInfo )

NetPlayerInfo::~NetPlayerInfo()
{
	FOREACH( Actor *, m_vpInfo, d )
		delete *d;
	m_vpInfo.clear();
}

void NetPlayerInfo::LoadFromNode( const XNode* pNode )
{
	int iMaxPlayers = 8;
	pNode->GetAttrValue( "MaxPlayers", iMaxPlayers );

	const XNode *pDisplayNode = pNode->GetChild( "Display" );
	if( pDisplayNode == NULL )
	{
		RageException::Throw( "%s: NetPlayerInfo: missing the Display child", ActorUtil::GetWhere(pNode).c_str() );
		ASSERT(0);
	}

	for( int i=0; i<iMaxPlayers; i++ )
	{
		Actor *pDisplay = ActorUtil::LoadFromNode( pDisplayNode, this );
		pDisplay->SetUseZBuffer( true );
		m_vpInfo.push_back( pDisplay );
	}

	ActorScroller::LoadFromNode( pNode );
}

void NetPlayerInfo::SetFromGameState()
{
	RemoveAllChildren();

	unsigned uNumPlayers = NSMAN->m_PlayerNames.size();

	for( int i=0; i<(int)uNumPlayers; i++ )
	{
		Actor *pDisplay = m_vpInfo[i];
		SetItemFromGameState( pDisplay, i );
		this->AddChild( pDisplay );
	}

	this->SetLoop( false );
	this->Load2();
	this->SetTransformFromHeight( m_vpInfo[0]->GetUnzoomedHeight() );
	this->EnableMask( m_vpInfo[0]->GetUnzoomedWidth(), m_vpInfo[0]->GetUnzoomedHeight() );

	if( 0 )
	{
		SetPauseCountdownSeconds( 1.5f );
		this->SetDestinationItem( m_vpInfo.size()+1 );	// loop forever
	}
}

void NetPlayerInfo::SetItemFromGameState( Actor *pActor, int iPlayerIndex )
{
	Message msg("SetNetPlayerInfo");
	msg.SetParam( "Difficulty", NSMAN->m_Difficulties[iPlayerIndex] );
	msg.SetParam( "Meter", NSMAN->m_iMeters[iPlayerIndex] );
	msg.SetParam( "StepsType", NSMAN->m_StepsTypes[iPlayerIndex] );
	msg.SetParam( "PlayerName", NSMAN->m_PlayerNames[iPlayerIndex] );
	pActor->HandleMessage( msg );
	/*const Course *pCourse = GAMESTATE->m_pCurCourse;

	FOREACH_HumanPlayer(pn)
	{
		const Trail *pTrail = GAMESTATE->m_pCurTrail[pn];
		if( pTrail == NULL ||  iCourseEntryIndex >= (int) pTrail->m_vEntries.size() )
			continue;

		const TrailEntry *te = &pTrail->m_vEntries[iCourseEntryIndex];
		const CourseEntry *ce = &pCourse->m_vEntries[iCourseEntryIndex];
		if( te == NULL )
			continue;

		RString s;
		Difficulty dc;
		if( te->bSecret )
		{
			if( ce == NULL )
				continue;

			int iLow = ce->stepsCriteria.m_iLowMeter;
			int iHigh = ce->stepsCriteria.m_iHighMeter;

			bool bLowIsSet = iLow != -1;
			bool bHighIsSet = iHigh != -1;

			if( !bLowIsSet  &&  !bHighIsSet )
			{
				s = "?";
			}
			if( !bLowIsSet  &&  bHighIsSet )
			{
				s = ssprintf( ">=%d", iHigh );
			}
			else if( bLowIsSet  &&  !bHighIsSet )
			{
				s = ssprintf( "<=%d", iLow );
			}
			else if( bLowIsSet  &&  bHighIsSet )
			{
				if( iLow == iHigh )
					s = ssprintf( "%d", iLow );
				else
					s = ssprintf( "%d-%d", iLow, iHigh );
			}

			dc = te->dc;
			if( dc == Difficulty_Invalid )
				dc = Difficulty_Edit;
		}
		else
		{
			s = ssprintf("%d", te->pSteps->GetMeter());
			dc = te->pSteps->GetDifficulty();
		}
			
		Message msg("SetSong");
		msg.SetParam( "PlayerNumber", pn );
		msg.SetParam( "Song", te->pSong );
		msg.SetParam( "Steps", te->pSteps );
		msg.SetParam( "Difficulty", dc );
		msg.SetParam( "Meter", s );
		msg.SetParam( "Number", iCourseEntryIndex+1 );
		msg.SetParam( "Modifiers", te->Modifiers );
		msg.SetParam( "Secret", te->bSecret );
		pActor->HandleMessage( msg );
	}
	*/
}

// lua start
#include "LuaBinding.h"

class LunaNetPlayerInfo: public Luna<NetPlayerInfo>
{
public:
	static int SetFromGameState( T* p, lua_State *L )			{ p->SetFromGameState(); return 0; }

	LunaNetPlayerInfo()
	{
		ADD_METHOD( SetFromGameState );
	}
};

LUA_REGISTER_DERIVED_CLASS( NetPlayerInfo, ActorScroller )
// lua end

/*
 * (c) 2012 v1toko
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
