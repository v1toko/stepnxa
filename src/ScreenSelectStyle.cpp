#include "global.h"
#include "ScreenSelectStyle.h"
#include "GameManager.h"
#include "GameSoundManager.h"
#include "NetworkSyncManager.h"
#include "ThemeManager.h"
#include "ScreenManager.h"
#include "GameState.h"
#include "AnnouncerManager.h"
#include "ActorUtil.h"
#include "CommonMetrics.h"
#include "LocalizedString.h"
#include "InputEventPlus.h"
#include "MessageManager.h"
#include "PrefsManager.h"

#define ICON_GAIN_FOCUS_COMMAND		THEME->GetMetricA(m_sName,"IconGainFocusCommand")
#define ICON_LOSE_FOCUS_COMMAND		THEME->GetMetricA(m_sName,"IconLoseFocusCommand")
#define DISABLED_COMMAND			THEME->GetMetricA(m_sName,"DisabledCommand")
//MODIFICADO POR MI
//METRIC AGREGADA
#define USENXSTYLE					THEME->GetMetricB(m_sName,"UseNXStyle")

REGISTER_SCREEN_CLASS( ScreenSelectStyle );

void ScreenSelectStyle::Init()
{
	ScreenSelect::Init();

	m_iSelection = 0;

	m_bSelected = false;

	for( unsigned i=0; i<m_aGameCommands.size(); i++ )
	{
		const GameCommand& mc = m_aGameCommands[i];

		//
		// Load icon
		//
		RString sIconPath = THEME->GetPathG(m_sName,ssprintf("icon%d",i+1));

		m_textIcon[i].SetName( ssprintf("Icon%d",i+1) );
		m_sprIcon[i].SetName( ssprintf("Icon%d",i+1) );

		if( sIconPath.empty() )	// element doesn't exist
		{
			m_textIcon[i].LoadFromFont( THEME->GetPathF("Common","normal") );
			m_textIcon[i].SetText( mc.m_sName );
			m_textIcon[i].SetZoom(0.5f);
			this->AddChild( &m_textIcon[i] );
		}
		else
		{
			m_sprIcon[i].Load( sIconPath );
			this->AddChild( &m_sprIcon[i] );
		}
	

		//
		// Load Picture
		//
		RString sPicturePath = THEME->GetPathG(m_sName, ssprintf("picture%d",i+1));
		if( sPicturePath != "" )
		{
			m_sprPicture[i].SetName( "Picture" );
			m_sprPicture[i].Load( sPicturePath );
			m_sprPicture[i].SetDiffuse( RageColor(1,1,1,0) );
			this->AddChild( &m_sprPicture[i] );
			LOAD_ALL_COMMANDS( m_sprPicture[i] );
		}


		//
		// Load info
		//
		RString sInfoPath = THEME->GetPathG(m_sName,ssprintf("info%d",i+1));
		if( sInfoPath != "" )
		{
			m_sprInfo[i].SetName( "Info" );
			m_sprInfo[i].Load( sInfoPath );
			m_sprInfo[i].SetDiffuse( RageColor(1,1,1,0) );
			this->AddChild( &m_sprInfo[i] );
			LOAD_ALL_COMMANDS( m_sprInfo[i] );
		}
	}

	LOCK_INPUT_AFTER_MOVE.Load( "ScreenSelectStyle", "LockInputAfterChange" );

	if( USENXSTYLE )
	{
		m_StationWheel.SetName( "StationWheel" );
		{
			vector<GameCommand> gcp;
			for( unsigned i=0; i<m_aGameCommands.size(); i++ )
			{
				const GameCommand& mc = m_aGameCommands[i];
				//if( mc.IsPlayable() )
					gcp.push_back( mc );
			}
			ASSERT_M( gcp.size() > 0, "No hay styles posibles para jugar" );
			m_StationWheel.Load( "StationWheel", gcp );//pasamos solo los jugables.
		}
		LOAD_ALL_COMMANDS_AND_SET_XY( m_StationWheel );
		m_StationWheel.SetRotationZ( -90 );
		this->AddChild( &m_StationWheel );
	}

	m_sprWarning.SetName( "Warning" );
	m_sprWarning.Load( THEME->GetPathG(m_sName,"warning") );
	this->AddChild( &m_sprWarning );
		
	m_sprExplanation.SetName( "Explanation" );
	m_sprExplanation.Load( THEME->GetPathG(m_sName,"explanation") );
	this->AddChild( &m_sprExplanation );

	if( USENXSTYLE )
	{
		m_soundStart.Load( THEME->GetPathS("ScreenSelectMusic","start") );
	}
	//-------------------

	// fix Z ordering of Picture and Info so that they draw on top
	for( unsigned i=0; i<this->m_aGameCommands.size(); i++ )
		this->MoveToTail( &m_sprPicture[i] );
	for( unsigned i=0; i<this->m_aGameCommands.size(); i++ )
		this->MoveToTail( &m_sprInfo[i] );


	m_sprPremium.SetName( "Premium" );

	switch( GAMESTATE->GetPremium() )
	{
	case Premium_DoubleFor1Credit:
		m_sprPremium.Load( THEME->GetPathG(m_sName,"doubles premium") );
		this->AddChild( &m_sprPremium );
		break;
	case Premium_2PlayersFor1Credit:
		m_sprPremium.Load( THEME->GetPathG(m_sName,"joint premium") );
		this->AddChild( &m_sprPremium );
		break;
	}


	m_soundChange.Load( THEME->GetPathS(m_sName,"change"), true );


	//
	// TweenOnScreen
	//
	for( unsigned i=0; i<m_aGameCommands.size(); i++ )
	{
		LOAD_ALL_COMMANDS_AND_SET_XY( m_textIcon[i] );
		LOAD_ALL_COMMANDS_AND_SET_XY( m_sprIcon[i] );
	}
	LOAD_ALL_COMMANDS_AND_SET_XY( m_sprExplanation );
	LOAD_ALL_COMMANDS_AND_SET_XY( m_sprWarning );
	LOAD_ALL_COMMANDS_AND_SET_XY( m_sprPremium );
	

	// let AfterChange tween Picture and Info
}

void ScreenSelectStyle::BeginScreen()
{
	if( USENXSTYLE )
		m_StationWheel.BeginScreen();

	this->UpdateSelectableChoices();

	//
	// TweenOnScreen
	//
	for( unsigned i=0; i<m_aGameCommands.size(); i++ )
	{
		ON_COMMAND( m_textIcon[i] );
		ON_COMMAND( m_sprIcon[i] );
	}
	ON_COMMAND( m_sprExplanation );
	ON_COMMAND( m_sprWarning );
	ON_COMMAND( m_sprPremium );

	if( USENXSTYLE )
	{
		ON_COMMAND( m_StationWheel );
		
		int iItems = m_StationWheel.GetNumItems();
		m_StationWheel.ChangeMusic( -iItems );
	}

	ScreenSelect::BeginScreen();
}

void ScreenSelectStyle::MenuLeft( const InputEventPlus &input )
{
	if( input.type == IET_REPEAT || this->IsTransitioning() )
		return;

	if( USENXSTYLE )
	{
		if( m_StationWheel.IsMoving() )
			return;
	}

	//MODIFICADO POR MI
	if( m_bSelected && PREFSMAN->m_bUseNXStyle )
	{
		/*m_sprConfirm.SetVisible (false);
		m_sprConfirmMessage.SetVisible (false);*/
		MESSAGEMAN->Broadcast( "CenterHidden" );
		m_bSelected = false;
		//m_ScrollingList.StopBouncing();
	}

	if( USENXSTYLE )
		m_StationWheel.ChangeMusic( -1 );

	Screen::SetLockInputSecs( LOCK_INPUT_AFTER_MOVE );

	int iSwitchToIndex = -1;	// -1 means none found
	for( int i=m_iSelection-1; i>=0; i-- )
	{
		if( m_aGameCommands[i].IsPlayable() )
		{
			iSwitchToIndex = i;
			break;
		}
	}

	//if( iSwitchToIndex == -1 )
	//	return;
	if( iSwitchToIndex == -1 )
	{
		for( int i=m_aGameCommands.size()-1; i>=0; i-- )
		{
			if( m_aGameCommands[i].IsPlayable() )
			{
				iSwitchToIndex = i;
				break;
			}
		}
	}

	BeforeChange();
	m_iSelection = iSwitchToIndex;
	m_soundChange.Play();

	MESSAGEMAN->Broadcast( "DownLeftPressed" );
	AfterChange();
}

void ScreenSelectStyle::MenuRight( const InputEventPlus &input )
{
	if( input.type == IET_REPEAT || this->IsTransitioning() )
		return;

	if( USENXSTYLE )
	{
		if( m_StationWheel.IsMoving() )
			return;
	}

	//MODIFICADO POR MI
	if( m_bSelected && PREFSMAN->m_bUseNXStyle )
	{
		/*m_sprConfirm.SetVisible (false);
		m_sprConfirmMessage.SetVisible (false);*/
		MESSAGEMAN->Broadcast( "CenterHidden" );
		m_bSelected = false;
		//m_ScrollingList.StopBouncing();
	}

	int iSwitchToIndex = -1;	// -1 means none found
	for( unsigned i=m_iSelection+1; i<m_aGameCommands.size(); i++ )	
	{
		if( m_aGameCommands[i].IsPlayable() )
		{
			iSwitchToIndex = i;
			break;
		}
	}

	if( USENXSTYLE )
		m_StationWheel.ChangeMusic( 1 );

	Screen::SetLockInputSecs( LOCK_INPUT_AFTER_MOVE );

	//if( iSwitchToIndex == -1 )
	//	return;
	if( iSwitchToIndex == -1 )
	{
		for( unsigned i=0; i <= m_aGameCommands.size()-1; i++ )
		{
			if( m_aGameCommands[i].IsPlayable() )
			{
				iSwitchToIndex = i;
				break;
			}
		}
	}

	BeforeChange();
	m_iSelection = iSwitchToIndex;
	m_soundChange.Play();

	MESSAGEMAN->Broadcast( "DownRightPressed" );
	AfterChange();
}

void ScreenSelectStyle::MenuStart( const InputEventPlus &input )
{
	if( input.type == IET_REPEAT || this->IsTransitioning() )
		return;

	if( USENXSTYLE )
	{
		if( m_StationWheel.IsMoving() )							
			return;

		if( !((StationWheelItemData*)m_StationWheel.GetItem(m_StationWheel.GetSelection()))->m_GameCommand.IsPlayable() )
			return;
	}

		//MODIFICADO POR MI
	//EXPERIMENTAL
		//CONFIRMACION DE SELECCION
	if(!m_bSelected && PREFSMAN->m_bUseNXStyle)
	{
		/*m_sprConfirm.SetVisible (true);
		m_sprConfirmMessage.SetVisible (true);*/
		MESSAGEMAN->Broadcast( "CenterWasPressedOnce" );
		m_soundStart.Play();
		m_bSelected = true;
		//las dejamos aqui por que o sino se playean 2 veces!
		const GameCommand& mc = m_aGameCommands[GetSelectionIndex(input.pn)];
		SOUND->PlayOnceFromDir( ANNOUNCER->GetPathTo(ssprintf("%s comment %s",m_sName.c_str(),mc.m_sName.c_str())) );
		MESSAGEMAN->Broadcast( ssprintf( "Selected%s", mc.m_sName.c_str() ) );
		return;
	}
	MESSAGEMAN->Broadcast( "CenterWasPressedTwice" );

	/* Stop all tweens where they are, since we might have selected before
	 * we finished tweening in. */
	for( unsigned i=0; i<m_SubActors.size(); i++ )
		m_SubActors[i]->StopTweening();

	SCREENMAN->PlayStartSound();
	SCREENMAN->SendMessageToTopScreen( SM_BeginFadingOut );

	/*const GameCommand& mc = m_aGameCommands[GetSelectionIndex(input.pn)];
	SOUND->PlayOnceFromDir( ANNOUNCER->GetPathTo(ssprintf("%s comment %s",m_sName.c_str(),mc.m_sName.c_str())) );*/

	//
	// TweenOffScreen
	//

	for( unsigned i=0; i<m_aGameCommands.size(); i++ )
	{
		OFF_COMMAND( m_sprIcon[i] );
		OFF_COMMAND( m_textIcon[i] );
	}
	OFF_COMMAND( m_sprExplanation );
	OFF_COMMAND( m_sprWarning );
	OFF_COMMAND( m_sprPicture[m_iSelection] );
	OFF_COMMAND( m_sprInfo[m_iSelection] );
	OFF_COMMAND( m_sprPremium );

	if( USENXSTYLE )
		OFF_COMMAND( m_StationWheel );
}

int ScreenSelectStyle::GetSelectionIndex( PlayerNumber pn )
{
	return m_iSelection;
}

static LocalizedString NO_STYLES_ARE_SELECTABLE( "ScreenSMOnlineLogin", "No Styles are selectable." );
void ScreenSelectStyle::UpdateSelectableChoices()
{
	for( unsigned i=0; i<m_aGameCommands.size(); i++ )
	{
		/* If the icon is text, use a dimmer diffuse, or we won't be
		 * able to see the glow. */
		if( m_aGameCommands[i].IsPlayable() )
		{
			m_sprIcon[i].SetDiffuse( RageColor(1,1,1,1) );
			m_textIcon[i].SetDiffuse( RageColor(0.5f,0.5f,0.5f,1) );	// gray so glow is visible
		}
		else
		{
			m_sprIcon[i].RunCommands( DISABLED_COMMAND );
			m_textIcon[i].RunCommands( DISABLED_COMMAND );
		}
	}

	// Select the first enabled choice.
	BeforeChange();

	int iSwitchToStyleIndex = -1;	// -1 means none found
	for( unsigned i=0; i<m_aGameCommands.size(); i++ )
	{
		const GameCommand& mc = m_aGameCommands[i];
		if( mc.IsPlayable() )
		{
			iSwitchToStyleIndex = i;
			break;
		}
	}

	if( iSwitchToStyleIndex == -1 )// no styles are enabled.  We're stuck!
	{
		DEBUG_ASSERT(0);
		SCREENMAN->SystemMessage( NO_STYLES_ARE_SELECTABLE );
		this->PostScreenMessage( SM_GoToPrevScreen, 0 );
		return;
	}
	

	m_iSelection = iSwitchToStyleIndex;

	if( USENXSTYLE )
	{
		m_StationWheel.FillWheelBaseData();

		if( m_StationWheel.GetSelection() != 0 )
			m_StationWheel.ChangeMusic( -m_StationWheel.GetSelection() );
		else
			m_StationWheel.RebuildWheelItems();
	}

	Screen::SetLockInputSecs( LOCK_INPUT_AFTER_MOVE );

	AfterChange();
}

void ScreenSelectStyle::BeforeChange()
{
	// dim/hide old selection
	m_sprIcon[m_iSelection].RunCommands( ICON_LOSE_FOCUS_COMMAND );
	m_textIcon[m_iSelection].RunCommands( ICON_LOSE_FOCUS_COMMAND );
	m_sprPicture[m_iSelection].StopTweening();
	m_sprInfo[m_iSelection].StopTweening();
	m_sprPicture[m_iSelection].SetDiffuse( RageColor(1,1,1,0) );
	m_sprInfo[m_iSelection].SetDiffuse( RageColor(1,1,1,0) );
	m_sprPicture[m_iSelection].SetGlow( RageColor(1,1,1,0) );
	m_sprInfo[m_iSelection].SetGlow( RageColor(1,1,1,0) );
}

void ScreenSelectStyle::AfterChange()
{
	m_sprIcon[m_iSelection].RunCommands( ICON_GAIN_FOCUS_COMMAND );
	m_textIcon[m_iSelection].RunCommands( ICON_GAIN_FOCUS_COMMAND );
	m_sprPicture[m_iSelection].SetDiffuse( RageColor(1,1,1,1) );
	m_sprInfo[m_iSelection].SetDiffuse( RageColor(1,1,1,1) );
	m_sprPicture[m_iSelection].SetZoom( 1 );
	m_sprInfo[m_iSelection].SetZoom( 1 );
	SET_XY_AND_ON_COMMAND( m_sprPicture[m_iSelection] );
	SET_XY_AND_ON_COMMAND( m_sprInfo[m_iSelection] );
	m_sprExplanation.PlayCommand( "Enter" );
}

/*
 * (c) 2001-2004 Chris Danford
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
