return Def.ActorFrame {
	Def.Sprite {
		Texture='_receptor';
		Frame0000=1;
		Delay0000=1;
		InitCommand=cmd();
	};

	Def.Sprite {
		Texture='_receptor';
		Frame0000=1;
		Delay0000=1;
		InitCommand=NOTESKIN:GetMetricA('Receptor','InitCommand');
	};

	Def.Sprite {
		Texture='_receptor';
		Frame0000=4;
		Delay0000=1;
		InitCommand=cmd(zoom,1;diffusealpha,0;glow,1,1,1,0);
		PressCommand=cmd(finishtweening;glow,1,1,1,1;zoom,1;linear,0.2;glow,1,1,1,0;zoom,1.2);
	};

}