local ret = ... or {}

ret.Redir = function(sButton, sElement)

	if sElement == "Hold Head Inactive" or
	   sElement == "Hold Head Active" or
	   sElement == "Roll Head Inactive" or
	   sElement == "Roll Head Active"
	then
		sElement = "Tap Note";
	end;

	return sButton, sElement;

end

ret.Blank =
{
};


return ret;