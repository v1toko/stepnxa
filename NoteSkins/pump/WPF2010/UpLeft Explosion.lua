local t = Def.ActorFrame {

	NOTESKIN:LoadActor(Var "Button", "Tap Note") .. {
		InitCommand=cmd(blend,"BlendMode_Add";zoom,1;diffusealpha,0);
		W1Command=cmd(visible,true;playcommand,"Glow");
		W2Command=cmd(visible,true;playcommand,"Glow");
		W3Command=cmd(visible,true;playcommand,"Glow");
		W4Command=cmd(visible,false);
		W5Command=cmd(visible,false);
		HeldCommand=cmd(visible,true;playcommand,"Glow");
		GlowCommand=cmd(finishtweening;
				diffusealpha,0.95;zoom,1;linear,0.15;
				zoom,1.15;linear,0.15;
				diffusealpha,0;zoom,1.30);
	};

	LoadActor('_flash') .. {
		InitCommand=cmd(blend,"BlendMode_Add";zoom,0.5;diffusealpha,0);
		W1Command=cmd(visible,true;playcommand,"Glow");
		W2Command=cmd(visible,true;playcommand,"Glow");
		W3Command=cmd(visible,true;playcommand,"Glow");
		W4Command=cmd(visible,false);
		W5Command=cmd(visible,false);
		HeldCommand=cmd(visible,true;playcommand,"Glow");
		GlowCommand=cmd(finishtweening;
				diffusealpha,0.95;zoom,0.55;linear,0.15;
				zoom,0.6;linear,0.15;
				diffusealpha,0;zoom,0.65;);
	};

	Def.Quad {
		InitCommand=cmd(diffuse,1,1,1,0;zoomto,SCREEN_WIDTH*100,SCREEN_HEIGHT*100;zoomz,SCREEN_WIDTH*SCREEN_HEIGHT);
		HitMineCommand=cmd(finishtweening;diffusealpha,1;linear,0.3;diffusealpha,0);
		ItemCommand=cmd(playcommand,"HitMine");
	};

}
return t;