local ret = ... or {}

ret.Redir = function(sButton, sElement)

	if sElement == "Hold Head Inactive" or
	   sElement == "Hold Head Active"
	then
		sElement = "Tap Note";
	end;
	
--------------------------------
	
	if sElement == "Roll Body Inactive" or
	   sElement == "Roll Body Active"
	then
		sElement = "Hold Body";
	end;

	return sButton, sElement;

end

ret.Blank =
{
};


return ret;
