local t = Def.ActorFrame {
	NOTESKIN:LoadActor(Var "Button", "Tap Note") .. {
		InitCommand=cmd(blend,"BlendMode_Add";zoom,1;diffusealpha,0);
		W1Command=cmd(visible,true;playcommand,"Glow");
		W2Command=cmd(visible,true;playcommand,"Glow");
		W3Command=cmd(visible,true;playcommand,"Glow");
		W4Command=cmd(visible,false);
		W5Command=cmd(visible,false);
		HeldCommand=cmd(visible,true;playcommand,"Glow");
		GlowCommand=cmd(finishtweening;
				diffusealpha,1;zoom,1.00;linear,0.1;
				diffusealpha,0.5;zoom,1.15;linear,0.1;
				diffusealpha,0;zoom,1.30);
	};

	LoadActor('_b1') .. {
		InitCommand=cmd(blend,"BlendMode_Add";zoom,0.5;diffusealpha,0);
		W1Command=cmd(visible,true;playcommand,"Glow");
		W2Command=cmd(visible,true;playcommand,"Glow");
		W3Command=cmd(visible,true;playcommand,"Glow");
		W4Command=cmd(visible,false);
		W5Command=cmd(visible,false);
		HeldCommand=cmd(visible,true;playcommand,"Glow");
		GlowCommand=cmd(finishtweening;
				diffusealpha,0.5;zoom,0.25;accelerate,0.1;
				diffusealpha,2;zoom,0.5;linear,0.2;
				diffusealpha,0;zoom,1;);
	};
	
			LoadActor('_b2') .. {
		InitCommand=cmd(blend,"BlendMode_Add";zoom,0.5;diffusealpha,0);
		W1Command=cmd(visible,true;playcommand,"Glow");
		W2Command=cmd(visible,true;playcommand,"Glow");
		W3Command=cmd(visible,true;playcommand,"Glow");
		W4Command=cmd(visible,false);
		W5Command=cmd(visible,false);
		HeldCommand=cmd(visible,true;playcommand,"Glow");
		GlowCommand=cmd(finishtweening;
				diffusealpha,0.0;zoom,0.0;linear,0.1;
				diffusealpha,0.5;zoom,0.8;linear,0.2;
				diffusealpha,0;zoom,0;);
	};
	
				LoadActor('_b3') .. {
		InitCommand=cmd(blend,"BlendMode_Add";zoom,0.5;diffusealpha,0);
		W1Command=cmd(visible,true;playcommand,"Glow");
		W2Command=cmd(visible,true;playcommand,"Glow");
		W3Command=cmd(visible,true;playcommand,"Glow");
		W4Command=cmd(visible,false);
		W5Command=cmd(visible,false);
		HeldCommand=cmd(visible,true;playcommand,"Glow");
		GlowCommand=cmd(finishtweening;
				diffusealpha,0.0;zoom,0.2;linear,0.1;
				diffusealpha,0.6;zoom,0.3;linear,0.2;
				diffusealpha,0;zoom,0.6;);
	};

	Def.Quad {
		InitCommand=cmd(diffuse,1,1,1,0;zoomto,SCREEN_WIDTH*100,SCREEN_HEIGHT*100;zoomz,SCREEN_WIDTH*SCREEN_HEIGHT);
		HitMineCommand=cmd(finishtweening;diffusealpha,1;linear,0.3;diffusealpha,0);
		ItemCommand=cmd(playcommand,"HitMine");
	};

}
return t;