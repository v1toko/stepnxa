local t = Def.ActorFrame {
	NOTESKIN:LoadActor(Var "Button", "Tap Note") .. {
		InitCommand=cmd(blend,"BlendMode_Add";zoom,1;diffusealpha,0);
		W1Command=cmd(visible,true;playcommand,"Glow");
		W2Command=cmd(visible,true;playcommand,"Glow");
		W3Command=cmd(visible,true;playcommand,"Glow");
		W4Command=cmd(visible,false);
		W5Command=cmd(visible,false);
		HeldCommand=cmd(visible,true;playcommand,"Glow");
		GlowCommand=cmd(finishtweening;
				diffusealpha,1;zoom,1.00;linear,0.1;
				diffusealpha,0.5;zoom,1.15;linear,0.1;
				diffusealpha,0;zoom,1.30);
	};

	LoadActor('_1') .. {
		InitCommand=cmd(blend,"BlendMode_Add";zoom,0.5;diffusealpha,0);
		W1Command=cmd(visible,true;playcommand,"Glow");
		W2Command=cmd(visible,true;playcommand,"Glow");
		W3Command=cmd(visible,true;playcommand,"Glow");
		W4Command=cmd(visible,false);
		W5Command=cmd(visible,false);
		HeldCommand=cmd(visible,true;playcommand,"Glow");
		GlowCommand=cmd(finishtweening;
				diffusealpha,0.5;zoom,0;accelerate,0.1;
				diffusealpha,2;zoom,0.4;linear,0.3;
				diffusealpha,0;zoom,0.8;);
	};
	
			LoadActor('_2') .. {
		InitCommand=cmd(blend,"BlendMode_Add";zoom,0.5;diffusealpha,0);
		W1Command=cmd(visible,true;playcommand,"Glow");
		W2Command=cmd(visible,true;playcommand,"Glow");
		W3Command=cmd(visible,true;playcommand,"Glow");
		W4Command=cmd(visible,false);
		W5Command=cmd(visible,false);
		HeldCommand=cmd(visible,true;playcommand,"Glow");
		GlowCommand=cmd(finishtweening;
				diffusealpha,0.0;zoom,1;linear,0.1;
				diffusealpha,0.75;zoom,0.5;linear,0.2;
				diffusealpha,0;zoom,0;);
	};
	
				LoadActor('_3') .. {
		InitCommand=cmd(blend,"BlendMode_Add";zoom,0.5;diffusealpha,0);
		W1Command=cmd(visible,true;playcommand,"Glow");
		W2Command=cmd(visible,true;playcommand,"Glow");
		W3Command=cmd(visible,true;playcommand,"Glow");
		W4Command=cmd(visible,false);
		W5Command=cmd(visible,false);
		HeldCommand=cmd(visible,true;playcommand,"Glow");
		GlowCommand=cmd(finishtweening;
				diffusealpha,0.3;zoom,0.2;linear,0.1;
				diffusealpha,0.15;zoom,0.6;linear,0.2;
				diffusealpha,0;zoom,0;);
	};

	Def.Quad {
		InitCommand=cmd(diffuse,1,1,1,0;zoomto,SCREEN_WIDTH*100,SCREEN_HEIGHT*100;zoomz,SCREEN_WIDTH*SCREEN_HEIGHT);
		HitMineCommand=cmd(finishtweening;diffusealpha,1;linear,0.3;diffusealpha,0);
		ItemCommand=cmd(playcommand,"HitMine");
	};

}
return t;