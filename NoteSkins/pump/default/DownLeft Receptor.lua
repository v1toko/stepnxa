local function Beat(self)
	local this = self:GetChildren()
	this.Glowpart:diffusealpha(0);
	local beat = GAMESTATE:GetSongBeat()
	local part = beat%1
	local eff = scale(part,0,0.5,1,0)
	if beat >=0 then
		this.Glowpart:diffusealpha(eff);
	end
end

return Def.ActorFrame {
	--InitCommand=cmd(SetUpdateFunction,Beat);

	--[[LoadActor("_receptor")..{
		InitCommand=cmd(pause;setstate,0);
	};
	LoadActor("_receptor")..{
		Name="Glowpart";
		InitCommand=cmd(pause;setstate,3;blend,"BlendMode_Add";diffuseshift;effectcolor1,1,1,1,0;effectcolor2,1,1,1,0.4;effectclock,"Beat";effecttiming,0,0,0.5,0.5);
	};]]

	LoadActor("_press")..{
		InitCommand=cmd(pause;setstate,5;zoom,0.9;diffusealpha,0;glow,1,1,1,0);
		PressCommand=cmd(finishtweening;diffusealpha,1;zoom,0.9;linear,0.2;diffusealpha,0;zoom,1.2);
	};
}
