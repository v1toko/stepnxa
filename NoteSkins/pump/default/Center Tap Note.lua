return Def.ActorFrame { 
	LoadActor('basespr')..{
		Frames = {
			{ Frame = 32 , Delay = 0.075 };
			{ Frame = 37 , Delay = 0.075 };
			{ Frame = 42 , Delay = 0.075 };
			{ Frame = 47 , Delay = 0.075 };
			{ Frame = 52 , Delay = 0.075 };
			{ Frame = 57 , Delay = 0.075 };
		};
	};
}
