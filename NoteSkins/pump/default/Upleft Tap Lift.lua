return Def.ActorFrame {
	NOTESKIN:LoadActor(Var "Button", "Graphic") .. {
		Frames = {
			{ Frame = 0 , Delay = 0.075 };
			{ Frame = 1 , Delay = 0.075 };
			{ Frame = 2 , Delay = 0.075 };
			{ Frame = 3 , Delay = 0.075 };
			{ Frame = 4 , Delay = 0.075 };
			{ Frame = 5 , Delay = 0.075 };
		};
		InitCommand=cmd(zoom,1;sleep,0.15;zoom,2;linear,0.3;zoom,1;queuecommand,"Init");
	};
}