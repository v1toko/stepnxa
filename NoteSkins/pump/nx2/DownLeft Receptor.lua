return Def.ActorFrame {
	Def.Sprite {
		Texture='_receptor';
		Frame0000=0;
		Delay0000=1;
		InitCommand=cmd(addx,100);
	};

	Def.Sprite {
		Texture='_receptor';
		Frame0000=1;
		Delay0000=1;
		InitCommand=cmd(addx,100;blend,"BlendMode_Add";diffuseshift;effectcolor1,1,1,1,0;effectcolor2,1,1,1,0.4;effectclock,"Beat";effecttiming,0,0,0.5,0.5);
	};

	Def.Sprite {
		Texture='_press';
		Frame0000=0;
		Delay0000=1;
		InitCommand=cmd(zoom,1;diffusealpha,0;glow,1,1,1,0);
		PressCommand=cmd(finishtweening;diffusealpha,1;zoom,0.9;linear,0.2;diffusealpha,0;zoom,1.2);
	};

}