return Def.ActorFrame {
	Def.Sprite {
		Texture='_press';
		Frame0000=3;
		Delay0000=1;
		InitCommand=cmd(zoom,1;diffusealpha,0;glow,1,1,1,0);
		PressCommand=cmd(finishtweening;diffusealpha,1;zoom,0.9;linear,0.2;diffusealpha,0;zoom,1.2);
	};

}