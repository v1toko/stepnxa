function ScreenTitleBranch()
	-- Don't show the title menu (says "Press Start") if there are 0 credits
	-- inserted and CoinMode is pay.
	if GAMESTATE:GetCoinsNeededToJoin() > GAMESTATE:GetCoins() then
		return THEME:GetMetric( "Common", "InitialScreen" )
	end

	if GAMESTATE:GetCoinMode() == "CoinMode_Home" then return "ScreenTitleMenu" end
	return "ScreenPressCenter"
end

function ScreenLoadUsbBranch()
	--return "ScreenLoadUsb"
	return SongSelectionScreen()
end

function ScreenGraphNXNext() --ScreengNX ? profilesave : saveusb
	if PROFILEMAN:IsPersistentProfile(PLAYER_1) or PROFILEMAN:IsPersistentProfile(PLAYER_2) then
		return "ScreenSaveUsb"
	end
	
	return "ScreenProfileSave"
end

function ScreenSelectProfileNext()
	local pm = GAMESTATE:GetPlayMode()
	
	if pm == "PlayMode_Mission" then
		return ScreenWorldMaxBranch()
	else
		return SongSelectionScreen()
	end
end

function ScreenCautionBranch()
	if PREFSMAN:GetPreference("ShowCaution") then return "ScreenCaution" end
	return "ScreenSelectStyle"
end

function ScreenSelectProfileBranch()
	return "ScreenProfileLoad"
end

function ScreenSelectGroupBranch()
	--if PREFSMAN:GetPreference("ShowSelectGroup") then return "ScreenSelectGroup" end
	--return GetScreenInstructions()
	return SongSelectionScreen()
end

function ScreenWorldMaxBranch()
	return "ScreenWorldMax"
end

function ScreenSelectMusicEasyBranch()
	return "ScreenSelectMusicEasy"
end

function ScreenSelectMusicBrainBranch()
	return "ScreenSelectMusicBrainShower"
end

function SongSelectionScreen()
	local pm = GAMESTATE:GetPlayMode()
	local cm = GAMESTATE:GetCoinMode()

	if pm ~= "PlayMode_Regular" and IsNetConnected() then
		SCREENMAN:SystemMessage( THEME:GetString("NetworkSyncManager","Not Supported") )
		return "ScreenSelectDifficulty"
	end

	if pm == "PlayMode_Nonstop"	then return "ScreenSelectCourseNonstop" end
	if pm == "PlayMode_Oni"		then return "ScreenSelectCourseOni" end
	if pm == "PlayMode_Endless"	then return "ScreenSelectCourseEndless" end
	if IsNetConnected() then ReportStyle() end
	if IsNetSMOnline() then return SMOnlineScreen() end
	if IsNetConnected() then return "ScreenNetSelectMusic" end
	if pm == "PlayMode_Mission" then return "ScreenWorldMax" end
	if pm == "PlayMode_Easy" then return "ScreenSelectMusicEasy" end
	if pm == "PlayMode_Special" then return "ScreenSelectMusicSpecial" end
	if pm == "PlayMode_Brain" then return "ScreenSelectMusicBrainShower" end
	
	return "ScreenSelectMusic"
end

--cambiamos la funcion para volver desde el gameplay para filtrar mejor cuando est� o no logeado en SMO
function SongSelectionScreenFilter()
	local pm = GAMESTATE:GetPlayMode()
	
	if IsNetConnected() then return "ScreenNetSelectMusic" end
	if pm == "PlayMode_Mission" then return "ScreenWorldMax" end
	if pm == "PlayMode_Easy" then return "ScreenSelectMusicEasy" end
	if pm == "PlayMode_Special" then return "ScreenSelectMusicSpecial" end
	if pm == "PlayMode_Brain" then return "ScreenSelectMusicBrainShower" end
	
	return "ScreenSelectMusic"
end

function NextNetworkScreen()
	--if IsNetConnected() then return "ScreenCaution" end
	return "ScreenTitleMenu"
end

function SMOnlineScreen()
	if not IsSMOnlineLoggedIn(PLAYER_1) and GAMESTATE:IsPlayerEnabled(PLAYER_1) then return "ScreenSMOnlineLogin" end
	if not IsSMOnlineLoggedIn(PLAYER_2) and GAMESTATE:IsPlayerEnabled(PLAYER_2) then return "ScreenSMOnlineLogin" end
	return "ScreenNetRoom"
end	

function GetSelectMusicNext()
	if SCREENMAN:GetTopScreen():GetGoToOptions() then return SelectFirstOptionsScreen() end	
	return "ScreenStage"
end

function ScreenPlayerStart()
	local pm = GAMESTATE:GetCoinMode()
 	local coins = GAMESTATE:GetCoins();
	local cointojoin = GAMESTATE:GetCoinsNeededToJoin();

	if pm == "CoinMode_Pay" and cointojoin <= coins or pm == "CoinMode_Free" then return "ScreenPressCenter" end
	return "ScreenTitleMenu"
end

function SelectFirstOptionsScreen()
	if GAMESTATE:GetPlayMode() == "PlayMode_Rave" then return "ScreenRaveOptions" end
	return "ScreenPlayerOptions"
end

function ScreenPlayerOptionsNext()
	if SCREENMAN:GetTopScreen():GetGoToOptions() then return "ScreenSongOptions" end
	
	return "ScreenStage"
end

function ScreenSongOptionsNext()
	return "ScreenStage"
end

function GetGameplayScreen()
	local st = GAMESTATE:GetCurrentStyle():GetStyleType()
	if st == "StyleType_TwoPlayersSharedSides" then return "ScreenGameplayShared" end
	return "ScreenGameplay"
end

function SelectEvaluationScreen()
	if IsNetConnected() then return "ScreenNetEvaluation" end
	local pm = GAMESTATE:GetPlayMode()
	if( GAMESTATE:GetMultiplayer() ) then return "ScreenEvaluationMultiplayer" end
	if( pm == "PlayMode_Regular" )	then return "ScreenEvaluationStage" end
	if( pm == "PlayMode_Nonstop" )	then return "ScreenEvaluationNonstop" end
	if( pm == "PlayMode_Oni" )	then return "ScreenEvaluationOni" end
	if( pm == "PlayMode_Endless" )	then return "ScreenEvaluationEndless" end
	if( pm == "PlayMode_Rave" )	then return "ScreenEvaluationRave" end
	if( pm == "PlayMode_Battle" )	then return "ScreenEvaluationBattle" end
	--modemission
	if( pm == "PlayMode_Mission" ) then return "ScreenEvaluationMission" end
	if( pm == "PlayMode_Special" ) then return "ScreenEvaluationStage" end
	if( pm == "PlayMode_Easy" ) then return "ScreenEvaluationStage" end
	if( pm == "PlayMode_Brain" ) then return "ScreenEvaluationBrain" end
end

function GetTopScreenMetric( sElement )
	local sClass = SCREENMAN:GetTopScreen():GetName();
	return THEME:GetMetric( sClass, sElement )
end

local function GetEvaluationNextScreenInternal( sNextScreen, sFailedScreen, sEndScreen )
	--SCREENMAN:SystemMessage( "GetEvaluationNextScreen: " .. sNextScreen .. ", " .. sFailedScreen .. ", " .. sEndScreen )

	if not GAMESTATE:IsMissionMode() then
		sScreenCom = "ScreenGraphNX"
	else
		sScreenCom = "ScreenSaveUsb"
	end

	--if GAMESTATE:IsMissionMode() then
	--	if GAMESTATE:HasUnlock() then
	--		sScreenCom = "ScreenUnlockNew"
	--	else
	--		sScreenCom = "ScreenGameOver"
	--	end
	--end

	if GAMESTATE:IsEventMode() then
		Trace( "IsEventMode" )
		return sNextScreen
	end
	
	local bIsExtraStage = GAMESTATE:IsAnExtraStage()
	-- Not in event mode.  If failed, go to the game over screen.
	if STATSMAN:GetCurStageStats():AllFailed() and not bIsExtraStage then
		Trace( "Failed" )
		return sFailedScreen
	end

	local sIsStage = not GetTopScreenMetric( "Summary" ) and not GAMESTATE:IsCourseMode()
	if sIsStage then
		local bLastStage = GAMESTATE:GetSmallestNumStagesLeftForAnyHumanPlayer() <= 0
		if not bLastStage then
			Trace( "Another" )
			return sNextScreen
		end
	end


	Trace( "End" )
	return sScreenCom
end

function GetEvaluationSummaryNextScreen()
	return GetEvaluationNextScreenInternal( "ScreenNameEntry", "ScreenProfileSave", "ScreenNameEntry" );
end

function GetEvaluationNextScreen()
	local sFailedScreen;
	local sEndScreen;
	local pm = GAMESTATE:GetPlayMode();
	if pm == "PlayMode_Regular" or
		pm == "PlayMode_Rave" or 
		pm == "PlayMode_Mission"  or 
		pm == "PlayMode_Special" or 
		pm == "PlayMode_Easy" or 
		pm == "PlayMode_Brain" then
		sFailedScreen = "ScreenSaveUsb";
		sEndScreen = "ScreenEvaluation";
		--sScreenCom = "ScreenGameOver"
	elseif pm == "PlayMode_Nonstop" or
		pm == "PlayMode_Oni" or
		pm == "PlayMode_Endless" then
		sFailedScreen = "ScreenNameEntry";
		sEndScreen = "ScreenNameEntry";
	else
		assert(false);
	end

	Trace( "branches" )

	local sNextScreen;
	if IsNetSMOnline() then sNextScreen = "ScreenNetRoom"
	elseif( IsNetConnected() ) then sNextScreen = "ScreenNetSelectMusic"
	else sNextScreen = SongSelectionScreen(); end
	
	if GAMESTATE:IsEventMode() and not ForceSMEventMode() then return "ScreenGameOver" end
	
	return GetEvaluationNextScreenInternal( sNextScreen, sFailedScreen, sEndScreen );
	--return sScreenCom;
end

function GetNextScreenTransition() --evaluation ? transition : graphNX
	--local bMissionPassed = not STATSMAN:GetCurStageStats():MissionFailed()
	--local bIsMission = GAMESTATE:IsMissionMode()
	--local iStageIndex = GAMESTATE:GetStageIndex() --etapas jugadas
	local iStageLeft = GAMESTATE:GetSmallestNumStagesLeftForAnyHumanPlayer() --tokens que quedan 2 = 1 etapa

	if ForceSMEventMode() then return "ScreenTransitionAfterEval" end
	
	if iStageLeft < 2 or GAMESTATE:IsEventMode() then
		return "ScreenGraphNX"
	end

	--if GAMESTATE:GetCoinMode() == "CoinMode_Free" then return "ScreenGraphNX" end
	
	return "ScreenTransitionAfterEval"
end

function ScreenBranchNetAfterEval()
	if IsNetSMOnline() then return "ScreenSMOnlineSelectMusic" end
	if IsNetConnected() then return "ScreenNetSelectMusic" end
	return "ScreenSelectMusic"
end	

function SelectEndingScreen()
	local bIsExtraStage = GAMESTATE:IsAnExtraStage()
	if STATSMAN:GetCurStageStats():AllFailed() and not bIsExtraStage then
		return "ScreenGameOver"
	end

	local grade = STATSMAN:GetBestFinalGrade()
	if Grade:Compare( grade, "Grade_Tier03" ) >= 0 then return "ScreenMusicScroll" end
	return "ScreenCredits"
end	

function IsEventMode()
	return PREFSMAN:GetPreference( "EventMode" )
end


function ForceSMEventMode()
	return PREFSMAN:GetPreference( "ForceSMEventMode" )
end
-- For "EvalOnFail", do:
-- function GetGameplayNextScreen() return SelectEvaluationScreen() end

function GetGameplayNextScreen()
	Trace( "GetGameplayNextScreen: " )
	local Passed = not STATSMAN:GetCurStageStats():AllFailed()
	local bMissionPassed = not STATSMAN:GetCurStageStats():MissionFailed()
	local bIsMission = GAMESTATE:IsMissionMode()
	local iStageIndex = GAMESTATE:GetStageIndex() --etapas jugadas
	local iStageLeft = GAMESTATE:GetSmallestNumStagesLeftForAnyHumanPlayer() --tokens que quedan 2 = 1 etapa
	Trace( " Passed = "..tostring(Passed) )
	Trace( " IsCourseMode = "..tostring(GAMESTATE:IsCourseMode()) )
	Trace( " IsExtraStage = "..tostring(GAMESTATE:IsExtraStage()) )
	Trace( " IsExtraStage2 = "..tostring(GAMESTATE:IsExtraStage2()) )
	Trace( " Event mode = "..tostring(IsEventMode()) )
	--Trace( " Has unlock = "..tostring(GAMESTATE:HasUnlock()) )
	
	Trace( " Num Stages Left = "..tostring(iStageLeft) )
	Trace( " Num Stages Index = "..tostring(iStageIndex) )
	Trace( " IsMission = "..tostring(bIsMission) )
	Trace( " MissionPassed = "..tostring(bMissionPassed) )
	
	if Passed then
		return SelectEvaluationScreen()
	end
	
	if bIsMission then --mission branch stuff
		if not bMissionPassed then --si no pasamos
			if iStageLeft < 2 then --y no nos quedan stages
				return "ScreenGraphNX"
			else --si es que si
				return "ScreenWorldMax"
			end
		end
	end

	if (not Passed and not IsEventMode()) and not ForceSMEventMode() then
		return "ScreenGraphNX"
	end

	if not Passed and (IsEventMode() and not ForceSMEventMode()) then
		return GetEvaluationNextScreen()
	end

	--if not Passed then return "ScreenGraphNX" end
	
	return SelectEvaluationScreen()
end

local function ShowScreenInstructions()
	if not PREFSMAN:GetPreference("ShowInstructions") then
		return false
	end

	if not GAMESTATE:GetPlayMode() then
		Trace( "ShowScreenInstructions: called without PlayMode set" )
		return true
	end

	if GAMESTATE:GetPlayMode() ~= "PlayMode_Regular" then
		return true
	end

	for pn in ivalues(PlayerNumber) do
		if Difficulty:Compare( GAMESTATE:GetPreferredDifficulty(pn), "Difficulty_Easy" ) <= 0 then
			return true
		end
	end

	return false
end

function GetScreenInstructions()
	if not ShowScreenInstructions() then
		return THEME:GetMetric("ScreenInstructions","NextScreen")
	else
		return "ScreenInstructions"
	end
end

function OptionsMenuAvailable()
	if GAMESTATE:IsMissionMode() then return false end
	if GAMESTATE:GetPlayMode() == "PlayMode_Oni" then return false end
	return true
end

function ModeMenuAvailable()
	if GAMESTATE:IsMissionMode() then
		return false
	end
	return true
end

-- (c) 2005 Glenn Maynard, Chris Danford
-- All rights reserved.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, and/or sell copies of the Software, and to permit persons to
-- whom the Software is furnished to do so, provided that the above
-- copyright notice(s) and this permission notice appear in all copies of
-- the Software and that both the above copyright notice(s) and this
-- permission notice appear in supporting documentation.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
-- OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
-- THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
-- INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
-- OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
-- OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
-- OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
-- PERFORMANCE OF THIS SOFTWARE.

