--/ nombre para cada dificultad *0*

function diffstring(pn)

	local curstep = GAMESTATE:GetCurrentSteps(pn)
	local diff = curstep:GetDifficulty()
	local stype = curstep:GetStepsType()
	--[[ PIU: ]]
	--//5flechas
	    if stype == 'StepsType_Pump_Single' and diff == 'Difficulty_Beginner'	then return "EASY"
	elseif stype == 'StepsType_Pump_Single' and diff == 'Difficulty_Easy'		then return "NORMAL"
	elseif stype == 'StepsType_Pump_Single' and diff == 'Difficulty_Medium'		then return "HARD"
	elseif stype == 'StepsType_Pump_Single' and diff == 'Difficulty_Hard'		then return "CRAZY"
	elseif stype == 'StepsType_Pump_Single' and diff == 'Difficulty_Challenge'	then return "WILD"
	--//10flechas
	elseif stype == 'StepsType_Pump_Double' and diff == 'Difficulty_Begginner'	then return "BEGINFREESTYLE"
	elseif stype == 'StepsType_Pump_Double' and diff == 'Difficulty_Easy'		then return "EASYFREESTYLE"
	elseif stype == 'StepsType_Pump_Double' and diff == 'Difficulty_Medium'		then return "FREESTYLE"
	elseif stype == 'StepsType_Pump_Double' and diff == 'Difficulty_Hard'		then return "NIGHTMARE"
	elseif stype == 'StepsType_Pump_Double' and diff == 'Difficulty_Challenge'	then return "ANOTHERNIGHTMARE"

	elseif diff == 'Difficulty_Edit' then

		if curstep:GetDescription() == "" then return DifficultyToLocalizedString(diff)
		else return curstep:GetDescription() end

	--[[ DDR/ITG/4P ]]
	elseif (stype == 'StepsType_Dance_Single' or
		stype == 'StepsType_Dance_Double' or
		stype == 'StepsType_Dance_Couple' or
		stype == 'StepsType_Dance_Solo')
			then return DifficultyToLocalizedString(diff)

	else return DifficultyToLocalizedString(diff).."//"..curstep:GetDescription()
	end

end

function metercolor(pn)
	local meter = GAMESTATE:GetCurrentSteps(pn):GetMeter()

	    if meter >= 1 and meter <= 8  then	return color("#00FFCC")
	elseif meter >= 9 and meter <= 16 then	return color("#FFF199")
	elseif meter >= 17 		  then	return color("#ES0066")
	else					return color("#FFFFFF")
	end
end

function missionmetercolor()
	local meter = GAMESTATE:GetCurrentMission():GetLevel()

	    if meter == 1 then	return color("#00FFFF")
	elseif meter == 2 then	return color("#33FF00")
	elseif meter == 3 then	return color("#FFFF00")
	elseif meter == 4 then	return color("#FF9900")
	elseif meter == 5 then	return color("#FF0000")
	else					return color("#FFFFFF")
	end
end

function missionmetercropright()
	local meter = GAMESTATE:GetCurrentMission():GetLevel()

	if meter == 1  then return 0.79
	elseif meter == 2 then return 0.61
	elseif meter == 3 then return 0.4
	elseif meter == 4 then return 0.2
	end

	return 0
end

function starseasycropright()
	--esto es por que en easy, no podemos jugar cosas mas dificiles!
	local meter = GAMESTATE:GetCurrentSteps(GAMESTATE:GetFirstHumanPlayer()):GetMeter()

	if meter == 1 then return 0.88
	elseif meter == 2 then return 0.75
	elseif meter == 3 then return 0.63
	elseif meter == 4 then return 0.5
	elseif meter == 5 then return 0.375
	elseif meter == 6 then return 0.245
	elseif meter == 7 then return 0.125
	end

	return 0
end

--arriba, gracias daisuke

function GetPlayerStageStatsForMission( pn )
	--devuelve las stats para el player en mode mission, como siempre es uno, pedimos a
	--gamestate el unico player, y no como argumento a la funcion
	--despues para usarla llamamos a la funcion y pedimos lo que queremos as�:
	--GetPlayerStageStatsForMission():GetAlgoQueNosSirva(Argumentos si es que nos pide)
	return STATSMAN:GetCurStageStats():GetPlayerStageStats(pn)
end

function side(pn)
	local stype = GAMESTATE:GetCurrentSteps(pn):GetStepsType()
	if stype == 'StepsType_Pump_Single' then return "SOLO"
	elseif stype == 'StepsType_Pump_Double' then return "DOUBLE"
	end
end

function FailMission( pn )
	GetPlayerStageStatsForMission( pn ):SetMissionFailed( true )
end

function GetPlayerStageStatsForMissionExtraStats1()
	return STATSMAN:GetCurStageStats():GetFirstExtraPlayerStageStats()
end

function GetPlayerStageStatsForMissionExtraStats2()
	return STATSMAN:GetCurStageStats():GetSecondExtraPlayerStageStats()
end

function getgroupcolor()
	return SONGMAN:GetSongColor( GAMESTATE:GetCurrentSong() )
end

function gettns(pn, tns)--scores
	return STATSMAN:GetCurStageStats():GetPlayerStageStats(pn):GetTapNoteScores(tns)
end

function gettnsfromextrastats1( pn, tns )
	return STATSMAN:GetCurStageStats():GetFirstExtraPlayerStageStats():GetTapNoteScores(tns)
end

function gettnsfromextrastats2( pn, tns )
	return STATSMAN:GetCurStageStats():GetSecondExtraPlayerStageStats():GetTapNoteScores(tns)
end

function GetProfileDisplayName( pn )
	if PROFILEMAN:IsPersistentProfile( pn ) then
		return PROFILEMAN:GetProfile( pn ):GetDisplayName()
	end
	return nil
end

function GetPlayerProfile( pn )
	if PROFILEMAN:IsPersistentProfile( pn ) then
		return PROFILEMAN:GetProfile( pn )
	end
	return PROFILEMAN:GetMachineProfile()
end

--devuelve el lugar index [1 - 20]
--solo para attracts screens, por que esa funcion (getnameandrecord) es miembro de solo las attract screens
--el segundo argumento es el numero de espacios entre el nombre y el puntaje
function GetRecordName( index )
	return SCREENMAN:GetTopScreen():GetNameAndRecord(index,22)
end

function GetStageTokens( pn )
	return GAMESTATE:GetNumStagesLeft( pn ) - GAMESTATE:GetNumStagesForCurrentSongAndStepsOrCourse()
end

--usar con hide_if, no con Condition, no s� por que.
function IsArrowModsVisible()
	return GAMESTATE:GetCurrentMission():GetIfAllowShowArrowMods()
end

--solo para screen stage!, usar con settext
function GetCurrentMileageCost(pn)
	local mileage = SCREENMAN:GetTopScreen():GetMileageCost() 
	local mileagetotal = GetPlayerProfile( pn ):GetTotalMileage()
	mileage = math.abs( mileage )

	return string.format( "%.6i - %.4i", mileagetotal , mileage )
end

--nos da el tipo de la ultima barrera que pasamos =B
function GetLastBarrierType()
	local sType = GAMESTATE:GetLastBarrierType()
	
	Trace("GetLastBarrierType " .. tostring(sType) )
	
	return sType
end

--obtener si mostramos la barrera que indica que quitamos el bloqueo despu�s de un Boss
--lo podemos usar en Condition
function GetIfShowBlockOff()
	--local bIsBoss = GAMESTATE:GetCurrentMission():IsBoss()
	--local bIsPassed = GAMESTATE:GetCurrentMission():IsPassed()
	--if bIsBoss and not bIsPassed then return true end
	
	--return false
	--local profile = GetPlayerProfile( GAMESTATE:GetFirstHumanPlayer() )
	
	--local bCond = GAMESTATE:GetCurrentMission():CheckBarrierData( profile )
	
	--return bCond
	
	local bCond = GAMESTATE:GetIfBarrierWasPassed()
	
	Trace("GetIfBarrierWasPassed " .. tostring(bCond) )
	
	return bCond
end

--cambiar los segundos del menu timer en una (Top) screen
--para llamar usamos: 	
Def.ActorFrame {
	BeginCommand=cmd(queuecommand,"Load");
	LoadCommand=function()
		SetSecondsInMenuTimerScreen( 30 )
	end;
};
function SetSecondsInMenuTimerScreen( newseconds )
	SCREENMAN:GetTopScreen():GetChild('Timer'):setseconds( newseconds )
	return
end

--stage tiene que ser [ 0, 2 ] 1st (0), final (1), bonus (2)
function GetPlayerKCalForPlayerAndStage( stage, pn )
	local kcals = STATSMAN:GetPlayedStageStats( stage ):GetPlayerStageStats( pn ):GetKcal()
	
	if kcals then
		return kcals
	end
	
	return 0
end

function GetPlayerKCalForPlayerAndStageIncludingHolds( stage, pn )
	if not GAMESTATE:IsPlayerEnabled( pn ) then return end;
	
	local kcals = STATSMAN:GetPlayedStageStats( stage ):GetPlayerStageStats( pn ):GetKcalIncludingHolds()
	
	if kcals then
		return kcals
	end
	
	return 0
end

function GetPlayerVO2ForPlayerAndStage( stage, pn )

	local vo2 = STATSMAN:GetPlayedStageStats( stage ):GetPlayerStageStats( pn ):GetOxigenTaken()
	
	if vo2 then
		return vo2
	end
	
	return 0
end

function GetPlayerVO2ForPlayerAndStageIncludingHolds( stage, pn )
	if not GAMESTATE:IsPlayerEnabled( pn ) then return end;
	
	local vo2 = STATSMAN:GetPlayedStageStats( stage ):GetPlayerStageStats( pn ):GetOxigenIncludingHolds()
	
	if vo2 then
		return vo2
	end
	
	return 0
end

function GetAccumPlayedStageStats( pn )
	return STATSMAN:GetAccumPlayedStageStats():GetPlayerStageStats(pn)
end

--devuelve si podemos cargar o no la foto de la usb
function CharLoader( pn )
	if not GAMESTATE:IsPlayerEnabled( pn ) or not PROFILEMAN:IsPersistentProfile( pn ) then return "dummy" end
	
	return GetPlayerProfile( pn ):GetCharacterID_NX()
end

function HandleWheelNew( self,offsetFromCenter,itemIndex,numItems )
	local x = offsetFromCenter * 266

	self:x( x )

	if offsetFromCenter < 0 then
		if offsetFromCenter >= -1 then
			self:rotationy( offsetFromCenter*-16 )
		else
			self:rotationy( 16 )
			self:z( (offsetFromCenter+1)*80 )
		end
	else
		if offsetFromCenter <= 1 then
			self:rotationy( -offsetFromCenter*16 )
		else
			self:rotationy( -16 )
			self:z( -(offsetFromCenter-1)*80 )
		end
	end
end


function HandleWheel( self,offsetFromCenter,itemIndex,numItems )
	local x = offsetFromCenter * 180

	if offsetFromCenter >= 1 then 
		x = scale(offsetFromCenter+1,-1,1,-.5,.5) * 180
	elseif offsetFromCenter <= -1 then
		x = scale(offsetFromCenter-1,-1,1,-.5,.5) * 180
	end

	--[[if offsetFromCenter >= 3 then
		x = scale(offsetFromCenter+1,-1,1,-.5,.5) * 160
	elseif offsetFromCenter <= -3 then
		x = scale(offsetFromCenter-1,-1,1,-.5,.5) * 160
	end]]

	self:zoom(math.cos(offsetFromCenter/math.pi))
	self:x( x )
	self:z((-clamp(math.abs(offsetFromCenter),0,1)*170)-math.abs(offsetFromCenter))
	self:rotationy( clamp( offsetFromCenter, -1, 1) * 75)
end

function HandleWheelFiveItems( self,offsetFromCenter,itemIndex,numItems )
	local x = offsetFromCenter * 220

	if offsetFromCenter >= 1 then 
		x = scale(offsetFromCenter+1,-1,1,-.5,.5) * 220
	elseif offsetFromCenter <= -1 then
		x = scale(offsetFromCenter-1,-1,1,-.5,.5) * 220
	end

	self:zoom(math.cos(offsetFromCenter/math.pi))
	self:x( x )
	self:z((-clamp(math.abs(offsetFromCenter),0,1)*180)-math.abs(offsetFromCenter))
	self:rotationy( clamp( offsetFromCenter, -1, 1) * 70)

end

function HandleWheelFiveItems2( self,offsetFromCenter,itemIndex,numItems )
	--la ruleta, por metrics tiene una rotacion de 90�... eso cambia los ejes coordenados
	--x = y... y viceversa.
	local curve = 3.14*0.4*2;
	local WHEEL_3D_RADIUS = 300; 
	--aqui tenemos 5 items en la wheel, no numItem por que ese valor siempre es 1
	local NumItems = 5;
	local x = math.cos(offsetFromCenter/math.pi);
	local rotationx_radians = scale(offsetFromCenter,-NumItems/2,NumItems/2,-curve/2,curve/2); 
	local y = 300 * math.sin(rotationx_radians);
	
	self:x( 1 - x );
	
	self:y( y ); 
	
	self:z( 200 * math.cos(rotationx_radians) ); 
	
	self:zoom( x );
	
	rotationx_radians = (rotationx_radians * 180/math.pi)*3; --pasamos a grados �
	
	--controlamos la rotaci�n.
	if rotationx_radians > 60 then
		rotationx_radians = 60;
	elseif rotationx_radians < -60 then
		rotationx_radians = -60;
	end	
	
	self:rotationx( -rotationx_radians );
end

function GetBrainShowerTotals( pn )
	local iTotal = GetPlayerProfile( pn ):GetNumSongPassedInBrainShower()
	
	if iTotal == 0 then
		return 0
	end
	
	return (iTotal/SONGMAN:GetBrainShowerNumSongs())*100
end

-- (c) 2009 v1toko
-- All rights reserved.
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, and/or sell copies of the Software, and to permit persons to
-- whom the Software is furnished to do so, provided that the above
-- copyright notice(s) and this permission notice appear in all copies of
-- the Software and that both the above copyright notice(s) and this
-- permission notice appear in supporting documentation.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
-- OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
-- THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
-- INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
-- OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
-- OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
-- OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
-- PERFORMANCE OF THIS SOFTWARE.
