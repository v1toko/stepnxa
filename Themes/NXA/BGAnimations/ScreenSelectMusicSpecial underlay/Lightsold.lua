local bNoEnoughStages = false

return Def.ActorFrame {	

	Def.ActorFrame {
		Condition = GAMESTATE:GetStageIndex() == 0 or GAMESTATE:GetStageIndex() == 2;
		OnCommand=cmd(sleep,1.5;diffusealpha,1);
		OffCommand=cmd(sleep,0.5;diffusealpha,0);

	LoadActor( "../ScreenSelectMusicSpecial overlay/brillo" ) .. {
		InitCommand=cmd(blend,"BlendMode_Add";x,SCREEN_CENTER_X-61;y,SCREEN_CENTER_Y+136;zoom,.17;diffusealpha,0);
		CurrentSongChangedMessageCommand=function(self)
			local stage = GAMESTATE:GetStageIndex()

		if stage == 2 then (cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,0;effectcolor2,.6,1,0,1))(self)
			else (cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,1;effectcolor2,.6,1,0,1))(self)
		end end;
	};

	LoadActor( "../ScreenSelectMusicSpecial overlay/brillo" ) .. {
		InitCommand=cmd(blend,"BlendMode_Add";x,SCREEN_CENTER_X-36.5;y,SCREEN_CENTER_Y+136;zoom,.17;diffusealpha,0);
		CurrentSongChangedMessageCommand=function(self)
			local stage = GAMESTATE:GetStageIndex()

		if stage == 2 then (cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,0;effectcolor2,.6,1,0,1))(self)
			else (cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,1;effectcolor2,.6,1,0,1))(self)
		end end;
	};

	LoadActor( "../ScreenSelectMusicSpecial overlay/brillo" ) .. {
		InitCommand=cmd(blend,"BlendMode_Add";x,SCREEN_CENTER_X-11.5;y,SCREEN_CENTER_Y+136;zoom,.17;diffusealpha,0);
		CurrentSongChangedMessageCommand=function(self)
			local stage = GAMESTATE:GetStageIndex()
			local song = GAMESTATE:GetCurrentSong()
			local marathon = GAMESTATE:GetCurrentSong():IsMarathon()
		if stage == 2 or marathon then (cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,0;effectcolor2,.6,1,0,1))(self)
			else (cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,1;effectcolor2,.6,1,0,1))(self)
		end end;
	};

	LoadActor( "../ScreenSelectMusicSpecial overlay/brillo" ) .. {
		InitCommand=cmd(blend,"BlendMode_Add";x,SCREEN_CENTER_X+13.5;y,SCREEN_CENTER_Y+136;zoom,.17;diffusealpha,0);
		CurrentSongChangedMessageCommand=function(self)
			local stage = GAMESTATE:GetStageIndex()
			local song = GAMESTATE:GetCurrentSong()
			local long = GAMESTATE:GetCurrentSong():IsLong()
			local marathon = GAMESTATE:GetCurrentSong():IsMarathon()

		if stage == 2 or long or marathon then (cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,0;effectcolor2,.6,1,0,1))(self)
			else (cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,1;effectcolor2,.6,1,0,1))(self)
		end end;

	};

};


	Def.ActorFrame {
		Condition = GAMESTATE:GetStageIndex() == 1;
		OnCommand=cmd(sleep,1.5;diffusealpha,1);
		OffCommand=cmd(sleep,0.5;diffusealpha,0);

	LoadActor( "../ScreenSelectMusicSpecial overlay/brillo" ) .. {
		InitCommand=cmd(blend,"BlendMode_Add";x,SCREEN_CENTER_X-61;y,SCREEN_CENTER_Y+136;zoom,.17;diffusealpha,0);
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong()
			local long = GAMESTATE:GetCurrentSong():IsLong()
			local marathon = GAMESTATE:GetCurrentSong():IsMarathon()
			local normal = not long and not marathon
			local sStage1 = GAMESTATE:GetFirstStageString()
			local sStage2 = GAMESTATE:GetSecondStageString()
			local sStage3 = GAMESTATE:GetThirdStageString()
			bNoEnoughStages = false

			if sStage2 == "" and sStage1 == "Normal" then
				if normal then
					(cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,1;effectcolor2,.6,1,0,1))(self)
				elseif long then
					(cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,1;effectcolor2,.6,1,0,1))(self)
				elseif marathon then
					self:diffuseshift()
					self:effectperiod(.2)
					self:effectcolor1(.7,1,0,0)
					self:effectcolor2(.7,1,0,1)
					bNoEnoughStages = true
				end
			elseif sStage2 == "" and sStage1 == "Remix" then
				if normal then
					(cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,1;effectcolor2,.6,1,0,1))(self)
				elseif long then
					self:diffuseshift()
					self:effectperiod(.2)
					self:effectcolor1(.6,1,0,0)
					self:effectcolor2(.6,1,0,1)
					bNoEnoughStages = true
				elseif marathon then
					self:diffuseshift()
					self:effectperiod(.2)
					self:effectcolor1(.6,1,0,0)
					self:effectcolor2(.6,1,0,1)
					bNoEnoughStages = true
				end
			elseif sStage2 == "" and sStage1 == "Full" then
					self:diffuseshift()
					self:effectperiod(.2)
					self:effectcolor1(.6,1,0,0)
					self:effectcolor2(.6,1,0,1)
					bNoEnoughStages = true
			end

			MESSAGEMAN:Broadcast( "SetLifeIcons" )
	end;
	};

	LoadActor( "../ScreenSelectMusicSpecial overlay/brillo" ) .. {
		InitCommand=cmd(blend,"BlendMode_Add";x,SCREEN_CENTER_X-36.5;y,SCREEN_CENTER_Y+136;zoom,.17;diffusealpha,0);
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong()
			local long = GAMESTATE:GetCurrentSong():IsLong()
			local marathon = GAMESTATE:GetCurrentSong():IsMarathon()
			local normal = not long and not marathon
			local sStage1 = GAMESTATE:GetFirstStageString()
			local sStage2 = GAMESTATE:GetSecondStageString()
			local sStage3 = GAMESTATE:GetThirdStageString()
			bNoEnoughStages = false

			if sStage2 == "" and sStage1 == "Normal" then
				if normal then
					(cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,1;effectcolor2,.6,1,0,1))(self)
				elseif long then
					self:diffuseshift()
					self:effectperiod(.2)
					self:effectcolor1(.6,1,0,0)
					self:effectcolor2(.6,1,0,1)
					bNoEnoughStages = true
				elseif marathon then
					self:diffuseshift()
					self:effectperiod(.2)
					self:effectcolor1(.6,1,0,0)
					self:effectcolor2(.6,1,0,1)
					bNoEnoughStages = true
				end
			elseif sStage2 == "" and sStage1 == "Remix" then
				if normal then
					self:diffuseshift()
					self:effectperiod(.2)
					self:effectcolor1(.6,1,0,0)
					self:effectcolor2(.6,1,0,1)
				elseif long then
					self:diffuseshift()
					self:effectperiod(.2)
					self:effectcolor1(.6,1,0,0)
					self:effectcolor2(.6,1,0,1)
					bNoEnoughStages = true
				elseif marathon then
					self:diffuseshift()
					self:effectperiod(.2)
					self:effectcolor1(.6,1,0,0)
					self:effectcolor2(.6,1,0,1)
					bNoEnoughStages = true
				end
			elseif sStage2 == "" and sStage1 == "Full" then
					self:diffuseshift()
					self:effectperiod(.2)
					self:effectcolor1(.6,1,0,0)
					self:effectcolor2(.6,1,0,1)
					bNoEnoughStages = true
			end

			MESSAGEMAN:Broadcast( "SetLifeIcons" )

	end;
	};

	LoadActor( "../ScreenSelectMusicSpecial overlay/brillo" ) .. {
		InitCommand=cmd(blend,"BlendMode_Add";x,SCREEN_CENTER_X-11.5;y,SCREEN_CENTER_Y+136;zoom,.17;diffusealpha,0);
		CurrentSongChangedMessageCommand=cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,0;effectcolor2,.6,1,0,1);
	};

	LoadActor( "../ScreenSelectMusicSpecial overlay/brillo" ) .. {
		InitCommand=cmd(blend,"BlendMode_Add";x,SCREEN_CENTER_X+13.5;y,SCREEN_CENTER_Y+136;zoom,.17;diffusealpha,0);
		CurrentSongChangedMessageCommand=cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,0;effectcolor2,.6,1,0,1);
	};

};


	LoadActor( "../ScreenSelectMusicSpecial overlay/brillo" ) .. {
		InitCommand=cmd(blend,"BlendMode_Add";x,SCREEN_CENTER_X+38.5;y,SCREEN_CENTER_Y+136;zoom,.17;diffusealpha,0);
		OnCommand=cmd(sleep,1.5;diffusealpha,1);
		OffCommand=cmd(stoptweening;diffusealpha,0);
		CurrentSongChangedMessageCommand=cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,0;effectcolor2,.6,1,0,1);		
	};

	LoadActor( "../ScreenSelectMusicSpecial overlay/brillo" ) .. {
		InitCommand=cmd(blend,"BlendMode_Add";x,SCREEN_CENTER_X+63.5;y,SCREEN_CENTER_Y+136;zoom,.17;diffusealpha,0);
		OnCommand=cmd(sleep,1.5;diffusealpha,1);
		OffCommand=cmd(stoptweening;diffusealpha,0);
		CurrentSongChangedMessageCommand=cmd(diffuseshift;effectperiod,.2;effectcolor1,.6,1,0,0;effectcolor2,.6,1,0,1);
	};

};