local function mileage(player)
	return LoadFont("(player)milleage") .. { 
	--return LoadFont("TextBrainShower") .. { 
		InitCommand=cmd(maxwidth,270;shadowlength,0);
		MilleageCommand=function(self)
			local mil = GetPlayerProfile(player)
			local mileage = mil:GetTotalMileage()

			if not mil then
				self:hidden(1)
				return
			end

			self:hidden(0)
			self:y(88.5)
			mileage = string.format( "%.7i", mileage );

			if mileage then
				self:settext( mileage )
			else
				self:settext( "" )
			end
		end;
		
		CurrentStepsP1ChangedMessageCommand=function(self)
			if player == PLAYER_1 then
				self:playcommand("Milleage");
			end;
		end;
		CurrentStepsP2ChangedMessageCommand=function(self)
			if player == PLAYER_2 then
				self:playcommand("Milleage");
			end;
		end;

		SongBoughtMessageCommand=function(self)
			local mil = GetPlayerProfile(player)
			local mileage = mil:GetTotalMileage()

			if not mil then
				self:hidden(1)
				return
			end

			self:hidden(0)
			mileage = string.format( "%.7i", mileage );

			if mileage then
				self:settext( mileage );
			else
				self:settext( "" );
			end end;

		OffCommand=cmd(stoptweening;hidden,1);
	}
end

return Def.ActorFrame {
	BeginCommand=cmd(diffuse,.9,.9,1,0);
--*******El efectito para las millas, quedo un poco lento comparado con la original, pero igual se ve bien
-- PLAYER 1

	mileage(PLAYER_1) .. {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_1);
		InitCommand=cmd(x,SCREEN_CENTER_X-139;zoom,.65;cropleft,.9;diffusealpha,0);
		OnCommand=cmd(sleep,.6;linear,.3;diffusealpha,1;zoom,.4);
	};
	mileage(PLAYER_1) .. {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_1);
		InitCommand=cmd(x,SCREEN_CENTER_X-139;zoom,.65;cropright,.2;cropleft,.7;diffusealpha,0);
		OnCommand=cmd(sleep,.7;linear,.3;diffusealpha,1;zoom,.4);
	};
	mileage(PLAYER_1) .. {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_1);
		InitCommand=cmd(x,SCREEN_CENTER_X-139;zoom,.65;cropright,.3;cropleft,.6;diffusealpha,0);
		OnCommand=cmd(sleep,.8;linear,.3;diffusealpha,1;zoom,.4);
	};
	mileage(PLAYER_1) .. {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_1);
		InitCommand=cmd(x,SCREEN_CENTER_X-139;zoom,.65;cropright,.5;cropleft,.4;diffusealpha,0);
		OnCommand=cmd(sleep,.9;linear,.3;diffusealpha,1;zoom,.4);
	};
	mileage(PLAYER_1) .. {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_1);
		InitCommand=cmd(x,SCREEN_CENTER_X-139;zoom,.65;cropright,.6;cropleft,.3;diffusealpha,0);
		OnCommand=cmd(sleep,1;linear,.3;diffusealpha,1;zoom,.4);
	};
	mileage(PLAYER_1) .. {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_1);
		InitCommand=cmd(x,SCREEN_CENTER_X-139;zoom,.65;cropright,.7;cropleft,.2;diffusealpha,0);
		OnCommand=cmd(sleep,1.1;linear,.3;diffusealpha,1;zoom,.4);
	};
	mileage(PLAYER_1) .. {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_1);
		InitCommand=cmd(x,SCREEN_CENTER_X-139;zoom,.65;cropright,.9;diffusealpha,0);
		OnCommand=cmd(sleep,1.2;linear,.3;diffusealpha,1;zoom,.4);
	};
-- PLAYER 2

	mileage(PLAYER_2) .. {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_2);
		InitCommand=cmd(x,SCREEN_CENTER_X+139;zoom,.65;cropleft,.9;diffusealpha,0);
		OnCommand=cmd(sleep,1.2;linear,.3;diffusealpha,1;zoom,.4);
	};
	mileage(PLAYER_2) .. {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_2);
		InitCommand=cmd(x,SCREEN_CENTER_X+139;zoom,.65;cropright,.2;cropleft,.7;diffusealpha,0);
		OnCommand=cmd(sleep,1.1;linear,.3;diffusealpha,1;zoom,.4);
	};
	mileage(PLAYER_2) .. {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_2);
		InitCommand=cmd(x,SCREEN_CENTER_X+139;zoom,.65;cropright,.3;cropleft,.6;diffusealpha,0);
		OnCommand=cmd(sleep,1;linear,.3;diffusealpha,1;zoom,.4);
	};
	mileage(PLAYER_2) .. {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_2);
		InitCommand=cmd(x,SCREEN_CENTER_X+139;zoom,.65;cropright,.5;cropleft,.4;diffusealpha,0);
		OnCommand=cmd(sleep,.9;linear,.3;diffusealpha,1;zoom,.4);
	};
	mileage(PLAYER_2) .. {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_2);
		InitCommand=cmd(x,SCREEN_CENTER_X+139;zoom,.65;cropright,.6;cropleft,.3;diffusealpha,0);
		OnCommand=cmd(sleep,.8;linear,.3;diffusealpha,1;zoom,.4);
	};
	mileage(PLAYER_2) .. {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_2);
		InitCommand=cmd(x,SCREEN_CENTER_X+139;zoom,.65;cropright,.7;cropleft,.2;diffusealpha,0);
		OnCommand=cmd(sleep,.7;linear,.3;diffusealpha,1;zoom,.4);
	};
	mileage(PLAYER_2) .. {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_2);
		InitCommand=cmd(x,SCREEN_CENTER_X+139;zoom,.65;cropright,.9;diffusealpha,0);
		OnCommand=cmd(sleep,.6;linear,.3;diffusealpha,1;zoom,.4);
	};


}