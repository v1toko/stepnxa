local t = Def.ActorFrame {
	LoadActor("../Common Utils/special background") .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT);
	};

	LoadActor( "../ScreenSelectMusicUtils/bg aux backmask" ) .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;sleep,0.6;diffusealpha,1);
		OffCommand = cmd( diffusealpha,0);
	};


	LoadActor( "BackgroundDRKSpecial" ) .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+4;zoom,2;linear,0.5;zoom,1);
		OffCommand = cmd( sleep,0.5;linear,.71; zoom,2;diffusealpha,0 );
	};

	LoadActor( "BackgroundDRKSpecial" ) .. {
		OnCommand = cmd(croptop,.2;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+9;zoom,2;linear,0.5;zoom,1;blend,"BlendMode_Add";diffuse,1,0,0,1;diffusealpha,.75);
		OffCommand = cmd(diffusealpha,0 );
	};

	Def.ActorFrame {
		InitCommand=cmd(x,SCREEN_CENTER_X);
		OffCommand=cmd(sleep,.5;linear,0.5;addy,-200;zoom,2);

	LoadActor("../ScreenSelectMusic underlay/Top") .. {
		OnCommand=cmd(y,-150;zoom,2;linear,0.5;y,66;zoom,1;);
	};
	LoadActor("../ScreenSelectMusic underlay/Top") .. {
		OnCommand=cmd(blend,"BlendMode_Add";diffusealpha,0;y,-150;zoom,2;linear,0.5;y,66;zoom,1;sleep,.7;diffusealpha,1;linear,.5;diffusealpha,0);
		GroupChangedMessageCommand=cmd(finishtweening;diffusealpha,1;linear,.5;diffusealpha,0);
		CenterWasPressedTwiceMessageCommand=cmd(finishtweening;;diffusealpha,1;linear,.5;diffusealpha,0);
	};
	LoadActor("../ScreenSelectMusic underlay/Top") .. {
		OnCommand=cmd(blend,"BlendMode_Add"y,-150;zoom,2;linear,0.5;y,66;zoom,1;diffuseshift;effectperiod,3;effectcolor1,1,1,1,.2;effectcolor2,1,1,1,0);
	};
};

	Def.ActorFrame {
		InitCommand=cmd(x,SCREEN_CENTER_X);

	LoadActor("../ScreenSelectMusic underlay/Bottom") .. {
		OffCommand=cmd(hidden,1);
		OnCommand=cmd(y,630;zoom,2;linear,0.5;y,396;zoom,1;blend,"BlendMode_Add";diffusealpha,.4);
	};
	LoadActor("../ScreenSelectMusic underlay/Bottom") .. {
		OnCommand=cmd(y,630;zoom,2;linear,0.5;y,401;zoom,1);
		OffCommand=cmd(sleep,.5;linear,0.5;addy,200;zoom,2);
	};
	LoadActor("../ScreenSelectMusic underlay/Bottom") .. {
		OnCommand=cmd(blend,"BlendMode_Add";diffusealpha,0;y,630;zoom,2;linear,0.5;y,401;zoom,1;sleep,.7;diffusealpha,1;linear,.5;diffusealpha,0);
		GroupChangedMessageCommand=cmd(finishtweening;diffusealpha,1;linear,.5;diffusealpha,0);
		CenterWasPressedTwiceMessageCommand=cmd(finishtweening;;diffusealpha,1;linear,.5;diffusealpha,0);
	};
	LoadActor("../ScreenSelectMusic underlay/Bottom") .. {
		OnCommand=cmd(blend,"BlendMode_Add"y,630;zoom,2;linear,0.5;y,401;zoom,1;diffuseshift;effectperiod,3;effectcolor1,1,1,1,.2;effectcolor2,1,1,1,0);
		OffCommand=cmd(hidden,1);
	};
};

	LoadActor("../ScreenSelectMusic underlay/Channel/_Channel") .. {};

	LoadActor("../ScreenSelectMusicUtils/song background scroller") .. {
		InitCommand = cmd(y,SCREEN_CENTER_Y+156;zoom,0.9);
		OffCommand = cmd(stoptweening;linear,0.5;diffusealpha,0);
	};

	Def.ActorFrame {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+160);
		OffCommand=cmd(hidden,1);

	LoadActor("../ScreenSelectMusic underlay/header") .. {
		OnCommand=cmd(addy,229;zoom,2;linear,0.5;addy,-229;zoom,1);
	};
	LoadActor("../ScreenSelectMusic underlay/header") .. {
		OnCommand=cmd(blend,"BlendMode_Add";diffusealpha,0;addy,229;zoom,2;linear,0.5;addy,-229;zoom,1;sleep,.7;diffusealpha,1;linear,.5;diffusealpha,0);
		GroupChangedMessageCommand=cmd(finishtweening;diffusealpha,1;linear,.5;diffusealpha,0);
		CenterWasPressedTwiceMessageCommand=cmd(finishtweening;;diffusealpha,1;linear,.5;diffusealpha,0);
	};
	LoadActor("../ScreenSelectMusic underlay/header") .. {
		OnCommand=cmd(blend,"BlendMode_Add";addy,229;zoom,2;linear,0.5;addy,-229;zoom,1;diffuseshift;effectperiod,3;effectcolor1,1,1,1,.2;effectcolor2,1,1,1,0);
	};
};
	
	LoadActor( "../ScreenSelectMusic underlay/versus" ) .. {
		Condition=GAMESTATE:GetNumPlayersEnabled()==2;
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;zoom,2;linear,0.5;zoom,1;diffusealpha,1;diffuse,getgroupcolor() );
		GroupChangedMessageCommand = cmd( diffuse,getgroupcolor() );
		OffCommand = cmd( sleep,0.5;linear,0.5; zoom,2;diffusealpha,0 );
	};

	LoadActor( "../Common Utils/ArrowsScript" ) .. {InitCommand=cmd(addy,-20);};

	LoadActor( "../ScreenSelectMusicUtils/StyleText" ) .. {};
	LoadActor("../ScreenSelectMusicUtils/Laser") .. {};

	Def.ActorProxy {
		BeginCommand=function(self) local timer = SCREENMAN:GetTopScreen():GetChild('Timer'); self:SetTarget(timer); end;
		OnCommand=cmd(diffusealpha,1);
		OffCommand=cmd(sleep,0.5;linear,0.2;diffusealpha,0);
	};

	LoadActor("../ScreenSelectMusicUtils/DefaultGroupNames") .. {
		OnCommand=cmd(x,320;y,SCREEN_TOP+20);
	};

	LoadActor( "mileage" ) .. {};
	LoadActor("SpecialStagesFrame") .. {
		InitCommand=cmd(x,325;y,SCREEN_BOTTOM-105;zoom,1.5;addy,229);
		OnCommand=cmd(linear,.5;addy,-229;zoom,.75);
		OffCommand=cmd(sleep,.5;linear,.5;addy,200;zoom,1.5);
	};
	LoadActor( "lights" ) .. {OnCommand=cmd(addy,500;addx,3;sleep,.95;addy,-500)};

};
return t;
