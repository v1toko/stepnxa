return Def.ActorFrame {
	LoadActor("Join/Versus(right)") .. {
		OnCommand=cmd(x,640;y,SCREEN_CENTER_Y;diffusealpha,0;addx,320);
		PlayerJoinedMessageCommand=cmd(diffusealpha,1;linear,0.3;addx,-320);
	};
	
	LoadActor("Join/Versus(left)") .. {
		OnCommand=cmd(x,0;y,SCREEN_CENTER_Y;diffusealpha,0;addx,-320);
		PlayerJoinedMessageCommand=cmd(diffusealpha,1;linear,0.3;addx,320);
	};
	
	LoadActor("Join/Versus(sello)") .. {
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;blend,"BlendMode_Add");
		PlayerJoinedMessageCommand=cmd(sleep,0.6;linear,0.3;diffusealpha,1);
	};
	
	LoadActor("Join/Versus(sello)") .. {
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;blend,"BlendMode_Add";croptop,0.5;cropbottom,0.5);
		PlayerJoinedMessageCommand=cmd(sleep,0.6;linear,0.3;diffusealpha,1;croptop,0;cropbottom,0);
	};
	
	LoadActor("Join/Versus(orbe)") .. {
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;blend,"BlendMode_Add");
		PlayerJoinedMessageCommand=cmd(linear,0.3;diffusealpha,1);
	};
	
	LoadActor("Join/Versus(orbe)") .. {
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;blend,"BlendMode_Add");
		PlayerJoinedMessageCommand=cmd(zoom,1.5;linear,0.3;zoom,1;diffusealpha,1;linear,0.1;diffusealpha,0);
	};
	
	LoadActor("Join/PUSHPANEL") .. {
		OnCommand=cmd();
		PlayerJoinedMessageCommand=cmd(play);
	};
};
