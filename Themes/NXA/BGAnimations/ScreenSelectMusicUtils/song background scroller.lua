return Def.ActorFrame {

	LoadFont("Micrograma2") .. {
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetDisplayMainTitle();
			local artist = GAMESTATE:GetCurrentSong():GetDisplayArtist();
			local bps1 = GAMESTATE:GetCurrentSong():BPMMin();
			local bps1 = string.format("%3.f", bps1);
			local bps2 = GAMESTATE:GetCurrentSong():BPMMax();
			local bps2 = string.format("%3.f", bps2);
			local spLocked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
			local text = song .. "   -   " .. artist .. "   -   BPM: " .. bps1
			local text2 = song .. "   -   " .. artist .. "   -   BPM: " .. bps1 .. "~" .. bps2

			if not song and not course or spLocked then
				self:hidden(1)
				return
			end
			if song == "" then song = "Unknow song" end
			if artist == "" then artist = "Unknow Artist" end

				self:hidden(0)

			if bps1 == bps2 then self:settext(text) else self:settext(text2) end

				self:stoptweening();
				self:x(205);
				self:horizalign("HorizAlign_Left");
				self:diffuse(1,1,1,0);
				self:sleep(0.5);
				self:diffuse(1,1,1,0.7);
				self:maxwidth(450);
				self:sleep(0.6);
				self:linear(8);
				self:x(-235);
				self:queuecommand("MoveFaster");
			end;

			MoveFasterCommand=cmd(x,750;linear,5.5;x,-235;queuecommand,"MoveFaster");
			InitCommand=cmd(shadowlength,0;zoom,.9;addy,3;blend,"BlendMode_Add");
			OffCommand=cmd(finishtweening;stoptweening;hidden,1);

			};

		};
