return Def.ActorFrame {
	OffCommand=cmd(sleep,0.5;linear,0.1;diffusealpha,0);
	OnCommand=cmd(zoom,2;addy,-70;linear,0.5;zoom,1;addy,72);
	
	--[[Def.Sprite {
		Texture="blank.png";
		GroupChangedMessageCommand=function(self)
			local gfxpath = SONGMAN:GetSongGroupBannerPath( GAMESTATE:GetCurrentSong():GetGroupName() )
			if gfxpath then
				self:Load( gfxpath )
				self:SetTexture( self:GetTexture() )
				self:visible( true )
			else
				--SCREENMAN:SystemMessage( "Error no group gfx" )
				self:visible( false )
			end
		end;
		OnCommand=cmd(visible,false;addy,2;playcommand,"GroupChanged");
	};]]

	LoadFont("GroupNames") .. {
		OnCommand=cmd(shadowlength,0);
		CurrentSongChangedMessageCommand=function(self)
			local name = GAMESTATE:GetCurrentSong():GetGroupName();
			--local artist = GAMESTATE:GetCurrentSong():GetDisplayArtist();

			--local gfxpath = SONGMAN:GetSongGroupBannerPath( GAMESTATE:GetCurrentSong():GetGroupName() )
			--if gfxpath then
			--	self:visible( false )
			--else
				self:visible( true );
				self:zoom( 0.45 );
			--end

			self:settext(string.sub(name,4));
		end; 
	};
	
	LoadFont("GroupNames") .. {
		OnCommand=cmd(shadowlength,0);
		CurrentSongChangedMessageCommand=function(self)
			local name = GAMESTATE:GetCurrentSong():GetGroupName();
			--local artist = GAMESTATE:GetCurrentSong():GetDisplayArtist();

			--local gfxpath = SONGMAN:GetSongGroupBannerPath( GAMESTATE:GetCurrentSong():GetGroupName() )
			--if gfxpath then
			--	self:visible( false )
			--else
				self:visible( true );
				self:zoom( 0.45 );
				self:diffuse( getgroupcolor() );
				self:cropright( 0.5 );
				self:fadeleft( 0.3 );
				self:diffusealpha( 0.6 );
			--end

			self:settext(string.sub(name,4));
		end; 
	};

	LoadFont("GroupNames") .. {
		OnCommand=cmd(shadowlength,0);
		CurrentSongChangedMessageCommand=function(self)
			local name = GAMESTATE:GetCurrentSong():GetGroupName();
			--local artist = GAMESTATE:GetCurrentSong():GetDisplayArtist();

			--local gfxpath = SONGMAN:GetSongGroupBannerPath( GAMESTATE:GetCurrentSong():GetGroupName() )
			--if gfxpath then
			--	self:visible( false )
			--else
				self:visible( true );
				self:zoom( 0.45 );
				self:diffuse( getgroupcolor() );
				self:cropleft( 0.5 );
				self:faderight( 0.3 );
				self:diffusealpha( 0.6 );
			--end

			self:settext(string.sub(name,4));
		end; 
	};
};
--[[
	Def.ActorFrame {
		OffCommand=cmd(sleep,0.5;linear,0.1;diffusealpha,0);
		OnCommand=cmd(zoom,2;addy,-70;linear,0.5;zoom,1;addy,72);

	LoadActor( "Channels/_New" ) .. {
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();

			if song == "newtunes" or song == "1new tunes" or song == "new tunes" or song == "1 NEW TUNES" or song == "1NEW TUNES" or song == "NEW TUNES" then
				self:diffusealpha(1); else self:diffusealpha(0); end self:zoom(1.25) end;
	};
	
	LoadActor( "Channels/_KPop" ) .. {
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();

			if song == "kpop" or song == "k pop" or song == "k-pop" or song == "3k-pop" or song == "k_pop" or song == "2 K-POP" or song == "2K-POP" or song == "3K-POP" or song == "3 K-POP" or song == "K-POP" then
				self:diffusealpha(1); else self:diffusealpha(0); end self:zoom(1.25) end;
	};
	
	LoadActor( "Channels/_PIU" ) .. {
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();

			if song == "piuoriginal" or song == "2piu original" or song == "piu original" or song == "piu originals" or song == "piuoriginals" or song == "pump it up originals" or song == "pump it up original" or song == "4 PUMP IT UP ORIGINAL" or song == "4PUMP IT UP ORIGINAL" or song == "PUMP IT UP ORIGINAL" or song == "Banya" or song == "4BANYA" or song == "4PIU ORIGINAL" or song == "4 PIU ORIGINAL" or song == "PIU ORIGINAL"
			 or song == "2PUMP IT UP ORIGINAL" or song == "2Banya" or song == "2BANYA" or song == "2PIU ORIGINAL" or song == "2 PIU ORIGINAL" or song == "2 PUMP IT UP ORIGINAL" then
				self:diffusealpha(1); else self:diffusealpha(0); end self:zoom(1.25) end;
	};
	
	LoadActor( "Channels/_Pop" ) .. {
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();

			if song == "4pop" or song == "pop channel" or song == "pop" or song == "3 POP" or song == "3POP" or song == "POP" or song == "4POP" or song == "4 POP" or song == "POP CHANNEL" or song == "4pop" or song == "4 pop" then
				self:diffusealpha(1); else self:diffusealpha(0); end self:zoom(1.25) end;
	};
	
	LoadActor( "Channels/Metal" ) .. {
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();

			if song == "Metal" then
				self:diffusealpha(1); else self:diffusealpha(0); end end;
	};

	LoadActor( "Channels/AnotherStep" ) .. {
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();

			if song == "another" or song == "another channel"  or song == "7 ANOTHER STEP" or song == "7ANOTHER STEP" or song == "ANOTHER STEP" then
				self:diffusealpha(1);  else self:diffusealpha(0); end end;
	};
	
	LoadActor( "Channels/Full Songs" ) .. {
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();

			if song == "5full songs" or song == "full channel" or song == "full songs" or song == "full songs channel" or song == "fullsongs" or song == "fullsong" or song == "5 FULL SONG" or song == "5FULL SONG" or song == "FULL SONG" or song == "FULLSONG" then
				self:diffusealpha(1); else self:diffusealpha(0); end end;
	};
	
	LoadActor( "Channels/Remix" ) .. {
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();

			if song == "6remix" or song == "remix channel" or song == "remix" or song == "6 REMIX" or song == "6REMIX" or song == "REMIX" then
				self:diffusealpha(1); else self:diffusealpha(0); end end;
	};
	
	LoadFont("BoostSSi") .. {
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();

			if song == "6remix" or song == "remix channel" or song == "remix" or song == "6 REMIX" or song == "6REMIX" or song == "REMIX"
						or song == "5full songs" or song == "full channel" or song == "full songs" or song == "full songs channel" or song == "fullsongs" or song == "fullsong" or song == "5 FULL SONG" or song == "5FULL SONG" or song == "FULL SONG" or song == "FULLSONG"
						or song == "another" or song == "another channel"  or song == "7 ANOTHER STEP" or song == "7ANOTHER STEP" or song == "ANOTHER STEP"
						or song == "4pop" or song == "pop channel" or song == "pop" or song == "3 POP" or song == "3POP" or song == "POP" or song == "4POP" or song == "4 POP" or song == "POP CHANNEL" or song == "4pop" or song == "4 pop"
						or song == "kpop" or song == "k pop" or song == "k-pop" or song == "3k-pop" or song == "k_pop" or song == "2 K-POP" or song == "2K-POP" or song == "3K-POP" or song == "3 K-POP" or song == "K-POP"
						or song == "piuoriginal" or song == "2piu original" or song == "piu original" or song == "piu originals" or song == "piuoriginals" or song == "pump it up originals" or song == "pump it up original" or song == "4 PUMP IT UP ORIGINAL" or song == "4PUMP IT UP ORIGINAL" or song == "PUMP IT UP ORIGINAL" or song == "Banya" or song == "4BANYA" or song == "4PIU ORIGINAL" or song == "4 PIU ORIGINAL" or song == "PIU ORIGINAL"
						or song == "2PUMP IT UP ORIGINAL" or song == "2Banya" or song == "2BANYA" or song == "2PIU ORIGINAL" or song == "2 PIU ORIGINAL" or song == "2 PUMP IT UP ORIGINAL"
						or song == "newtunes" or song == "1new tunes" or song == "new tunes" or song == "1 NEW TUNES" or song == "1NEW TUNES" or song == "NEW TUNES" 
						or song == "SMF 1, 2 y 3" or song == "00000" or song == "Metal" then
				self:diffusealpha(0); else self:diffusealpha(1); end 

			if song then
				self:settext(song);
			else
				self:settext("No Song");
			end
		end;
	};


	LoadFont("BoostSSi") .. {
		CurrentSongChangedMessageCommand=function(self)
			local name = GAMESTATE:GetCurrentSong():GetGroupName();
			local artist = GAMESTATE:GetCurrentSong():GetDisplayArtist();

		if name == "SMF 1, 2 y 3" then self:hidden(0) else self:hidden(1) end


		self:settext(artist); end; };


	LoadActor( THEME:GetPathS("", "NewTunes") ) .. {
		SetCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();

			if song == "newtunes" or song == "1new tunes" or song == "new tunes" or song == "1 NEW TUNES" or song == "1NEW TUNES" or song == "NEW TUNES" then
				self:finishtweening()
				self:play()
			else
				self:stop(play)
			end
		end;

		GroupChangedMessageCommand=cmd( playcommand,"Set" );
	};

	LoadActor( THEME:GetPathS("", "K-POP") ) .. {
		SetCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();

			if song == "kpop" or song == "k pop" or song == "k-pop" or song == "3k-pop" or song == "k_pop" or song == "2 K-POP" or song == "2K-POP" or song == "K-POP" or song == "3K-POP" or song == "3 K-POP" then
				self:play();
			else
				self:stop();
			end
		end;

		GroupChangedMessageCommand=cmd( playcommand,"Set" );
	};

	LoadActor( THEME:GetPathS("", "POP") ) .. {
		SetCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();

			if song == "4pop" or song == "pop channel" or song == "pop" or song == "3 POP" or song == "3POP" or song == "POP" then
				self:play();
			else
				self:stop();
			end
		end;

		GroupChangedMessageCommand=cmd( playcommand,"Set" );
	};

	LoadActor( THEME:GetPathS("", "ORIGINAL") ) .. {
		SetCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();

			if song == "piuoriginal" or song == "2piu original" or song == "piu original" or song == "piu originals" or song == "piuoriginals" or song == "pump it up originals" or song == "pump it up original" or song == "4 PUMP IT UP ORIGINAL" or song == "4PUMP IT UP ORIGINAL" or song == "PUMP IT UP ORIGINAL" or song == "Banya" or song == "4BANYA" or song == "4PIU ORIGINAL" or song == "4 PIU ORIGINAL" or song == "PIU ORIGINAL"
						or song == "2PUMP IT UP ORIGINAL" or song == "2Banya" or song == "2BANYA" or song == "2PIU ORIGINAL" or song == "2 PIU ORIGINAL" or song == "2 PUMP IT UP ORIGINAL" then
				self:play();
			else
				self:stop();
			end
		end;

		GroupChangedMessageCommand=cmd( playcommand,"Set" );
	};

	LoadActor( THEME:GetPathS("", "Full Songs") ) .. {
		GroupChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();
			if song == "5full songs" or song == "full channel" or song == "full songs" or song == "full songs channel" or song == "fullsongs" or song == "fullsong" or song == "5 FULL SONG" or song == "5FULL SONG" or song == "FULL SONG" or song == "FULLSONG" then
				self:play();
			else
				self:stop();
			end
		end;
	};

	LoadActor( THEME:GetPathS("", "Remix") ) .. {
		GroupChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();

			if song == "6remix" or song == "remix channel" or song == "remix" or song == "6 REMIX" or song == "6REMIX" or song == "REMIX" then
				self:play();
			else
				self:stop();
			end
		end;
	};

	LoadActor( THEME:GetPathS("", "Another") ) .. {
		GroupChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();

			if song == "another" or song == "another channel"  or song == "7 ANOTHER STEP" or song == "7ANOTHER STEP" or song == "ANOTHER STEP" then
				self:play();
			else
				self:stop();
			end
		end;
	};
};
]]
