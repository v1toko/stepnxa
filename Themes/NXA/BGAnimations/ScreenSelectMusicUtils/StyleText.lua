local function Styletext(player)
	return LoadActor( "Icons/Text" ) .. {
		UpdateCommand=function(self)
			local text = diffstring(player)
			local stype = side(player)
			
			self:hidden( 0 );

		if stype == "SOLO" then
			self:hidden(0) else self:hidden(1) end
			
			if text == "EASY" then
				self:setstate( 0 );
			elseif text == "NORMAL" then
				self:setstate( 1 );
			elseif text == "HARD" then
				self:setstate( 2 );
			elseif text == "CRAZY" then
				self:setstate( 3 );
			elseif text == "WILD" then
				self:setstate( 4 );
			else
				self:setstate( 5 );
			end
			
			self:playcommand( "Show" );
		end;
		ShowCommand=cmd(finishtweening;zoomy,0;diffusealpha,0;sleep,0.37;linear,0.5;zoomy,.7;diffusealpha,1);
		OffCommand=cmd(hidden,1);
		CurrentStepsP1ChangedMessageCommand=function(self)
			if player == PLAYER_1 then
				self:playcommand("Update");
			end;
		end;
		CurrentStepsP2ChangedMessageCommand=function(self)
			if player == PLAYER_2 then
				self:playcommand("Update");
			end;
		end;
	}
end
	
local function StyletextDouble(player)
	return LoadActor( "Icons/TextDouble" ) .. {
		UpdateCommand=function(self)
			local text = diffstring(player)
			local stype = side(player)
			
			self:hidden( 0 );
			
		if stype == "DOUBLE" then
			self:hidden(0) else self:hidden(1) end

			if text == "BEGINFREESTYLE" then
				self:setstate( 1 );
			elseif text == "EASYFREESTYLE" then
				self:setstate( 1 );
			elseif text == "FREESTYLE" then
				self:setstate( 2 );
			elseif text == "NIGHTMARE" then
				self:setstate( 3 );
			elseif text == "ANOTHERNIGHTMARE" then
				self:setstate( 5 );
			else
				self:setstate( 5 );
			end
			
			self:playcommand( "Show" );
		end;
		ShowCommand=cmd(finishtweening;zoomy,0;diffusealpha,0;sleep,0.37;linear,0.5;zoomy,.7;diffusealpha,1);
		OffCommand=cmd(hidden,1);
		CurrentStepsP1ChangedMessageCommand=function(self)
			if player == PLAYER_1 then
				self:playcommand("Update");
			end;
		end;
		CurrentStepsP2ChangedMessageCommand=function(self)
			if player == PLAYER_2 then
				self:playcommand("Update");
			end;
		end;
	}
end

return Def.ActorFrame {

	Styletext(PLAYER_1) .. {
		InitCommand=function(self)
			local sides = GAMESTATE:GetNumSidesJoined()
			self:x(sides == 1 and 320 or SCREEN_CENTER_X-100);
			self:y(SCREEN_TOP+54);
			self:pause();
			self:zoom(.7);
			self:diffusealpha(0);
		end;
	};

	Styletext(PLAYER_2) .. {
		InitCommand=function(self)
			local sides = GAMESTATE:GetNumSidesJoined()
			self:x(sides == 1 and 320 or SCREEN_CENTER_X+100);
			self:y(SCREEN_TOP+54);
			self:pause();
			self:zoom(.7);
			self:diffusealpha(0);
		end;
	};


	StyletextDouble(PLAYER_1) .. {
		InitCommand=cmd(x,320;y,SCREEN_TOP+54;diffusealpha,0;pause;zoom,.7);
	};
	StyletextDouble(PLAYER_2) .. {
		InitCommand=cmd(x,320;y,SCREEN_TOP+54;diffusealpha,0;pause;zoom,.7);
	};

}