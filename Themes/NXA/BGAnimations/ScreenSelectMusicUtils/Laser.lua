return Def.ActorFrame {
	----Efecto laser en el cambio de dificultad(inicio)

	LoadActor( "Glows/bg aux style2players laser" ) .. {
		OnCommand = cmd(stoptweening;x,320;blend,"BlendMode_Add";sleep,0.35;queuecommand,"Update");
		UpdateCommand = cmd(finishtweening;y,261;diffusealpha,0;sleep,.3;diffusealpha,1;linear,0.6;y,SCREEN_CENTER_Y-6;zoom,1;diffusealpha,.4;linear,.4;diffusealpha,0);
		CurrentStepsP1ChangedMessageCommand=cmd(playcommand,"Update");
		CurrentStepsP2ChangedMessageCommand=cmd(playcommand,"Update");
		OffCommand = cmd( diffusealpha,0 );
	};

	LoadActor( "Glows/bg aux style2players laser" ) .. {
		OnCommand = cmd(stoptweening;x,320;blend,"BlendMode_Add";sleep,0.35;queuecommand,"Update");
		UpdateCommand = cmd(finishtweening;y,238;diffusealpha,0;sleep,.3;diffusealpha,1;linear,0.6;y,SCREEN_CENTER_Y+28;zoom,1;diffusealpha,4;linear,.4;diffusealpha,0;);
		CurrentStepsP1ChangedMessageCommand=cmd(playcommand,"Update");
		CurrentStepsP2ChangedMessageCommand=cmd(playcommand,"Update");
		OffCommand = cmd( diffusealpha,0 );
	};
};
