local t = Def.ActorFrame {
	
	LoadActor( THEME:GetPathG("", "_cristal") ) .. {
		InitCommand=cmd(x,320;y,228.5;zoomx,1.03;zoomy,.9865);
		OnCommand = cmd(stoptweening;diffusealpha,0;finishtweening;sleep,.55;diffusealpha,1);
		CurrentSongChangedMessageCommand=function(self)
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
					self:hidden(1)
				return
				end 
					self:hidden(0);
					self:playcommand("On"); end;
		OffCommand=cmd(stoptweening;diffusealpha,1;sleep,0.2;linear,0.5;zoom,0.5;decelerate,0.9;zoom,3);
	};

	LoadActor("Banner/Panelglow") .. {
		Condition=not IsNetSMOnline();
		InitCommand=function(self) self:pause()
		if GAMESTATE:GetNumPlayersEnabled() == 1 then self:setstate(0) end
		if GAMESTATE:GetNumPlayersEnabled() == 2 then self:setstate(1) end end;			
		OnCommand=cmd(x,320;y,311;diffusealpha,0;zoom,0.66;finishtweening;sleep,.5;linear,.15;diffusealpha,1);
		OffCommand=cmd(finishtweening;linear,0.3;diffusealpha,0);
		CurrentSongChangedMessageCommand=function(self)
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
					self:hidden(1)
				return
				end 
					self:hidden(0);
					self:playcommand("On"); end;
	};

	LoadActor("Banner/PanelGlow") .. {
		Condition=not IsNetSMOnline();
		InitCommand=function(self) self:pause()
		if GAMESTATE:GetNumPlayersEnabled() == 1 then self:setstate(0) end
		if GAMESTATE:GetNumPlayersEnabled() == 2 then self:setstate(1) end self:blend("BlendMode_Add") end;			
		OnCommand=cmd(x,320;y,311;diffusealpha,0;zoom,0.66;finishtweening;sleep,.725;diffusealpha,.5;linear,.35;zoom,.71;diffusealpha,0);
		OffCommand=cmd(finishtweening;linear,0.3;diffusealpha,0);
		CurrentSongChangedMessageCommand=function(self)
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
					self:hidden(1)
				return
				end 
					self:hidden(0);
					self:playcommand("On"); end;
	};



--*********************************************MEDIR ACTIVIDAD F�SICA****************************************************
--***********************PLAYER 1
		
		LoadFont( "Calories" ) .. {
			SetKcalPlayer1MessageCommand=function(self, param)
				local Kcal = param.iKcal
				Kcal = string.format( "%03i", Kcal );
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then self:hidden(1) return end
					
				if Kcal then self:hidden(0) self:settext( Kcal ) end
			end;		
			Condition=GAMESTATE:IsPlayerEnabled(PLAYER_1);
			InitCommand=function(self)
				local players = GAMESTATE:GetNumSidesJoined()
					self:x(players == 1 and 224 or 212);
					self:zoom(players == 1 and .55 or .525);
					self:y(321);
					self:shadowlength(0);
					self:diffuse(1,.8,.8,1);
			end;
			CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
			OffCommand=cmd(stoptweening;hidden,1);
		};

		LoadFont( "VO2" ) .. {
			SetVO2Player1MessageCommand=function(self, param)
				local Vo2 = param.iVO2
				Vo2 = string.format( "%03i", Vo2 );
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then self:hidden(1) return end
					
				if Vo2 then self:settext( Vo2 ) self:hidden(0) end
			end;
			Condition=GAMESTATE:IsPlayerEnabled(PLAYER_1);
			InitCommand=function(self)
				local players = GAMESTATE:GetNumSidesJoined()
					self:x(players == 1 and 417 or 241);
					self:zoom(players == 1 and .55 or .525);
					self:y(321);
					self:shadowlength(0);
					self:diffuse(.8,.8,1,1);
			end;
			CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
			OffCommand=cmd(stoptweening;hidden,1);
		};

--***********************PLAYER 2


		LoadFont( "Calories" ) .. {
			SetKcalPlayer2MessageCommand=function(self, param)
				local Kcal = param.iKcal
				Kcal = string.format( "%03i", Kcal );
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then self:hidden(1) end
					
				if Kcal then self:hidden(0) self:settext( Kcal ) end
			end;
			Condition=GAMESTATE:IsPlayerEnabled(PLAYER_2);
			InitCommand=function(self)
				local players = GAMESTATE:GetNumSidesJoined()
					self:x(players == 1 and 224 or 401);
					self:zoom(players == 1 and .55 or .525);
					self:y(321);
					self:shadowlength(0);
					self:diffuse(1,.8,.8,1);
			end;
			CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
			OffCommand=cmd(stoptweening;hidden,1);
		};

		LoadFont( "VO2" ) .. {
			SetVO2Player2MessageCommand=function(self, param)
				local Vo2 = param.iVO2
				Vo2 = string.format( "%03i", Vo2 );
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then self:hidden(1) end
					
				if Vo2 then self:settext( Vo2 ) self:hidden(0) end
			end;
			Condition=GAMESTATE:IsPlayerEnabled(PLAYER_2);
			InitCommand=function(self)
				local players = GAMESTATE:GetNumSidesJoined()
					self:x(players == 1 and 417 or 430);
					self:zoom(players == 1 and .55 or .525);
					self:y(321);
					self:shadowlength(0);
					self:diffuse(.8,.8,1,1);
			end;
			CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
			OffCommand=cmd(stoptweening;hidden,1);
		};
		

--*************************************************Fin Medir Actividad F�sica

	LoadActor("Banner/ScreenSelectMusic banner frame") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-8;blend,"BlendMode_Add");
		--OnCommand=cmd(stoptweening;diffuse,getgroupcolor();diffusealpha,0;zoomx,1.8;zoomy,.975;sleep,.5;linear,.18;diffusealpha,1;zoomx,.975;diffusealpha,.75);
		OnCommand=cmd(stoptweening;finishtweening;diffuse,getgroupcolor();zoom,.975;diffusealpha,0);
		CurrentSongChangedMessageCommand=function(self)
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
					self:hidden(1)
				return
				end 
					self:hidden(0);
					self:playcommand("Restore"); end;

		CenterWasPressedOnceMessageCommand=cmd(blend,"BlendMode_Add";playcommand,"Pulse");
		PulseCommand=cmd(finishtweening;diffusealpha,1;zoom,.975;linear,0.4;zoom,1.075;diffusealpha,0;sleep,0.2;queuecommand,"Pulse");
		StyleChangedMessageCommand=cmd(playcommand,"Restore");
		RestoreCommand=cmd(stoptweening;finishtweening;diffuse,getgroupcolor();zoom,.975;diffusealpha,0);
		OffCommand=cmd(finishtweening;diffusealpha,1;blend,"BlendMode_Normal";zoom,.965;sleep,.2;linear,.5;zoom,.475;decelerate,.9;zoom,2.8);
	};

	LoadActor("Banner/ScreenSelectMusic banner frame") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-8;blend,"BlendMode_Add");
		OnCommand=cmd(diffuse,getgroupcolor();diffusealpha,0;zoomx,1.8;zoomy,.975;sleep,0.5;linear,0.18;diffusealpha,1;zoomx,.975;sleep,.1;diffusealpha,0;sleep,.3;diffusealpha,1;linear,.3;zoom,1.3;diffusealpha,.5;linear,.1;diffusealpha,0);
		CurrentSongChangedMessageCommand=function(self)
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
					self:hidden(1)
				return
				end 
					self:hidden(0); end;
	};

	LoadActor("Banner/ScreenSelectMusic banner frame") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-8;blend,"BlendMode_Add";zoom,.99);
		OnCommand=cmd(stoptweening;diffusealpha,0;sleep,.2;playcommand,"Change");
		CurrentSongChangedMessageCommand=function(self)
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
					self:hidden(1)
				return
				end 
					self:hidden(0);

				self:playcommand("Change");
		end;

		ChangeCommand=cmd(finishtweening;diffuse,getgroupcolor();diffusealpha,0;sleep,.4;zoom,1.2;diffusealpha,.4;linear,.3;zoom,.975;diffuse,1,1,1,.4);
		CenterWasPressedOnceMessageCommand=cmd(diffusealpha,0);	
		OffCommand=cmd(Stoptweening;hidden,1);
		StyleChangedMessageCommand=cmd(diffusealpha,.4);
	};

	LoadActor("Banner/glow1") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-8;blend,"BlendMode_Add";glowshift;effectcolor1,1,1,1,.1;effectcolor2,1,1,1,.5;effectperiod,1.5);
		OnCommand=function(self)
			local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then self:hidden(1) return end

				(cmd(hidden,0;stoptweening;diffuse,getgroupcolor();diffusealpha,0;zoomx,1.8;zoomy,.99;sleep,.5;linear,.18;diffusealpha,.5;zoomx,.99))(self)

		end;

		CenterWasPressedOnceMessageCommand=function(self)
			local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then self:hidden(1) return end
				if not Locked then self:hidden(0); self:playcommand("Pulse"); end end;


		PulseCommand=cmd(finishtweening;diffusealpha,.6;zoom,0.99;linear,0.4;zoom,1.225;diffusealpha,0;sleep,0.2;queuecommand,"Pulse");
		StyleChangedMessageCommand=cmd(finishtweening;playcommand,"Restore");
		CurrentSongChangedMessageCommand=cmd(playcommand,"Restore");
		RestoreCommand=cmd(stoptweening;finishtweening;diffuse,getgroupcolor();zoom,0.99;diffusealpha,0);
		OffCommand=cmd(stoptweening;zoom,1;diffusealpha,.8;sleep,.2;linear,.2;zoomx,1.5;zoomy,1.25;diffusealpha,0);
	};
	

	LoadActor("Banner/glow1") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-8;blend,"BlendMode_Add";glowshift;effectcolor1,1,1,1,.1;effectcolor2,1,1,1,.5;effectperiod,1.5;zoom,.99);
		OnCommand=cmd(stoptweening;diffusealpha,0);
		CurrentSongChangedMessageCommand=function(self)
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
					self:hidden(1)
				return
				end 
					self:hidden(0);

				self:playcommand("Change");
		end;

		ChangeCommand=cmd(finishtweening;diffuse,getgroupcolor();diffusealpha,0;sleep,.6;linear,.1;diffusealpha,.6);
		CenterWasPressedOnceMessageCommand=cmd(diffusealpha,0);	
		OffCommand=cmd(stoptweening;hidden,1);
		StyleChangedMessageCommand=cmd(diffusealpha,.5);
	};

--************************************************Score/Nombre

	LoadFont( "ScoreName" ) .. {
		SendHighScoreNameMessageCommand=function(self, param)
			local sName = param.sScoreName
			local song = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
			
			if song then self:settext("") return end 
			
			if not sName then self:settext("") return end
						
			self:settext( sName )
		end;
		
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+63;shadowlength,0;zoomy,.9);
		CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
		OffCommand=cmd(finishtweening;linear,0.3;diffusealpha,0);
	};
	
	LoadFont( "Record Font" ) .. {
		SendHighScoreMessageCommand=function(self, param)
			local iScore = param.iScore
			local song = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
			
			if song then self:settext("") return end 
			
			if iScore == 0 then self:settext("") return end
			
			self:settext( iScore )
		end;
		
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+77;zoom,0.42;shadowlength,0);
		CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
		OffCommand=cmd(finishtweening;linear,0.3;diffusealpha,0);
	};


};

return t;