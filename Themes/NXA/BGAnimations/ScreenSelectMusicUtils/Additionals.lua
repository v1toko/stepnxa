local function meter(player)
	return LoadFont("MeterColor") .. {
		MeterCommand=function(self)
			local meter = GAMESTATE:GetCurrentSteps(player);
			local song = meter:GetMeter();
			local pn = GAMESTATE:GetNumPlayersEnabled()

			if not song then self:hidden(1) return end
			if song then self:settext( song ) else self:settext( "No Song" ); end
			if song >= 25 then self:settext( "" ); end
			if song < 9 then self:diffuse(0,.6,.6,0)
			elseif song > 8 and song < 17 then self:diffuse(1,.6,.3,0)
			elseif song >= 17 then self:diffuse(.9,.4,.5,0) end
			if pn == 1 then (cmd(finishtweening;diffusealpha,0;zoomy,.8;zoomx,1.2;sleep,.3;linear,.2;diffusealpha,.2;sleep,.1;linear,.2;diffusealpha,1;zoomx,.6;sleep,.1;linear,.1;zoomx,.9;linear,.1;zoomx,.6))(self) end
			if pn == 2 then (cmd(finishtweening;diffusealpha,0;zoomy,.7;zoomx,.75;sleep,.3;linear,.2;diffusealpha,.2;sleep,.1;linear,.2;diffusealpha,1;zoomx,.45;sleep,.1;linear,.1;zoomx,.75;linear,.1;zoomx,.45))(self) end
		end;
		CurrentStepsP1ChangedMessageCommand=function(self)
			if player == PLAYER_1 then
				self:playcommand("Meter");
			end;
		end;
		CurrentStepsP2ChangedMessageCommand=function(self)
			if player == PLAYER_2 then
				self:playcommand("Meter");
			end;
		end;
		PlayerJoinedMessageCommand=cmd(playcommand,"Meter");
		OffCommand=cmd(hidden,1);
	}
end

local function Lv(player)
	return LoadFont("BoostSSi") .. {
		TextCommand=function(self)
			local pn = GAMESTATE:GetNumSidesJoined()

				if pn == 1 then self:settext("Lv.") else self:settext("Level") end

				--[[self:finishtweening();
				self:settext(pn == 1 and "Lv." or "Level");
				self:diffusealpha(0);
				self:sleep(.5);
				self:linear(.3);
				self:diffusealpha(1);]]
				self:playcommand("Show")
		end;
		ShowCommand=cmd(finishtweening;diffusealpha,0;sleep,.5;linear,.3;diffusealpha,1);
		CurrentSongChangedMessageCommand=cmd(playcommand,"Show");
		CurrentStepsP1ChangedMessageCommand=function(self)
			if player == PLAYER_1 then
				self:playcommand("Text");
			end;
		end;
		CurrentStepsP2ChangedMessageCommand=function(self)
			if player == PLAYER_2 then
				self:playcommand("Text");
			end;
		end;
		PlayerJoinedMessageCommand=cmd(playcommand,"Text");
		OffCommand=cmd(hidden,1);
	}
end


local function Unknow(player)
	return LoadActor("Glows/Interrogation") .. {
		SetCommand=function(self)
			local meter = GAMESTATE:GetCurrentSteps(player);
			local song = meter:GetMeter();

			if not song then self:hidden(1) return end
			self:hidden(0)

			if song >= 25 then self:playcommand( "Show" ); else self:hidden(1); end
		end;
		OffCommand=cmd(hidden,1);
		PlayerJoinedMessageCommand=cmd(playcommand,"set");
		ShowCommand=function(self)
			local pn = GAMESTATE:GetNumPlayersEnabled()

			if pn == 1 then
			(cmd(finishtweening;diffusealpha,0;zoomx,.75;sleep,0.3;linear,0.3;diffusealpha,1;spring,.4;zoomx,1))(self) end
			if pn == 2 then
			(cmd(finishtweening;zoom,.75;diffusealpha,0;zoomx,.5;sleep,0.3;linear,0.3;diffusealpha,1;spring,.4;zoomx,.75))(self) end
		end;
			CurrentStepsP1ChangedMessageCommand=function(self)
				if player == PLAYER_1 then
					self:playcommand("Set");
				end;
			end;
			CurrentStepsP2ChangedMessageCommand=function(self)
				if player == PLAYER_2 then
					self:playcommand("Set");
				end;
			end;
	}
end

local function diffdisplaygray(player)
	local gray = Def.ActorFrame {}
	
	for i=1,8 do
		gray[#gray+1] = LoadActor("Icons/DiffIcons")..{
			InitCommand=cmd(pause;player,player);
			DiffDisplayCommand=function(self)
				local meter = GAMESTATE:GetCurrentSteps(player);
				local players = GAMESTATE:GetNumPlayersEnabled();
				meter = meter:GetMeter();
				local number = (((meter-1) % 8))+1
				local hide = i > number and 1 or 0

			if meter >= 25 then self:croptop(1) else self:croptop(-1) end
			if meter < 9 then self:setstate(3) end
			if meter > 8 and meter < 17 then self:setstate(4) end
			if meter > 16 then self:setstate(5) end
			if players == 1 then (cmd(finishtweening;diffusealpha,0;sleep,.5;x,i*30;zoom,.95;linear,.15;visible,hide;diffusealpha,.85;zoom,.8))(self) end
			if players == 2 then (cmd(finishtweening;diffusealpha,0;sleep,.5;x,i*17;zoom,.83;linear,.1;visible,hide;diffusealpha,.85;zoom,.67))(self) end

			if players == 1 and meter > 16 and meter < 25 then
				 (cmd(finishtweening;diffusealpha,0;sleep,.5;x,i*30;zoom,1;linear,.15;visible,hide;diffusealpha,.85;zoom,.925))(self) end
			if players == 2 and meter > 16 and meter < 25 then 
				(cmd(finishtweening;diffusealpha,0;sleep,.5;x,i*17;zoom,1.2;linear,.1;visible,hide;diffusealpha,.85;zoom,.85))(self) end

			if meter >= 9 and meter <=16 then self:faderight(.8) else self:faderight(0) end end;
			CurrentStepsP1ChangedMessageCommand=function(self)
				if player == PLAYER_1 then
					self:playcommand("DiffDisplay");
				end
			end;
			CurrentStepsP2ChangedMessageCommand=function(self)
				if player == PLAYER_2 then
					self:playcommand("DiffDisplay");
				end
			end;
	}
	end
return gray
end

local function diffdisplay(player)
	local t = Def.ActorFrame {}
	
	for cc=1,8 do
		t[#t+1] = LoadActor("Icons/DiffIcons")..{
			InitCommand=cmd(pause;player,player);
			DiffDisplayCommand=function(self)
				local meter = GAMESTATE:GetCurrentSteps(player)
				local players = GAMESTATE:GetNumPlayersEnabled();
				meter = meter:GetMeter();

			if players == 1 and meter >= 25 then meter = 25 end
			if players == 2 and meter >= 25 then meter = 24 end
			if meter >= 25 and players == 1 then self:croptop(1) else self:croptop(-1) end

				local number = (((meter-1) % 8))+1
				local hide = cc > number and 0 or 1

			if meter < 9 then self:setstate(0) end
			if meter > 8 and meter < 17 then self:setstate(1) end
			if meter > 16 then self:setstate(2) end
			if players == 1 then (cmd(finishtweening;diffusealpha,0;sleep,.5;x,cc*30;visible,hide;diffusealpha,1;zoomy,.75;zoomx,1.05;sleep,.15;linear,.18;zoomx,.75))(self) end
			if players == 2 then (cmd(finishtweening;diffusealpha,0;sleep,.5;x,cc*17;visible,hide;diffusealpha,1;zoomy,.64;zoomx,.94;sleep,.15;linear,.18;zoomx,.64))(self) end

			if players == 1 and meter > 16 and meter < 25 then 
				(cmd(finishtweening;diffusealpha,0;sleep,.5;x,cc*30;visible,hide;diffusealpha,1;zoomy,.925;zoomx,1.2;sleep,.15;linear,.18;zoomx,.925))(self) end
			if players == 2 and meter > 16 and meter < 25 then
				(cmd(finishtweening;diffusealpha,0;sleep,.5;x,cc*17;visible,hide;diffusealpha,1;zoomy,.85;zoomx,1;sleep,.15;linear,.18;zoomx,.85))(self) end

			if meter > 16 and meter < 24 then
				self:diffuse(1,.7,.7,1) else self:diffuse(1,1,1,1) end
			end;

			CurrentStepsP1ChangedMessageCommand=function(self)
				if player == PLAYER_1 then
					self:playcommand("DiffDisplay");
				end
			end;
			CurrentStepsP2ChangedMessageCommand=function(self)
				if player == PLAYER_2 then
					self:playcommand("DiffDisplay");
				end
			end;
	}
	end
return t
end

local function skull(player)
	local red = Def.ActorFrame {}
	
	for k=1,8 do
		red[#red+1] = LoadActor("Icons/RedSkull")..{
			InitCommand=cmd(player,player;y,SCREEN_BOTTOM-42);
			DiffDisplayCommand=function(self)
				local meter = GAMESTATE:GetCurrentSteps(player)
				local players = GAMESTATE:GetNumPlayersEnabled();
				meter = meter:GetMeter();

			if players == 1 and meter >= 25 then meter = 25 end
			if players == 2 and meter >= 25 then meter = 24 end
			if meter >= 25 and players == 1 then self:cropleft(1) else self:cropleft(-1)  end

				local number = (((meter-1) % 8))+1
				local hide = k > number and 0 or 1				

			if meter > 16 then self:setstate(2) end

			if players == 1 and meter > 16 and meter < 25 then 
				(cmd(finishtweening;diffusealpha,0;sleep,.5;x,k*30;visible,hide;diffusealpha,1;zoomy,.925;zoomx,1.2;sleep,.15;linear,.18;zoomx,.925))(self) end
			if players == 2 and meter > 16 and meter < 25 then
				(cmd(finishtweening;diffusealpha,0;sleep,.5;x,k*17;visible,hide;diffusealpha,1;zoomy,.85;zoomx,1;sleep,.15;linear,.18;zoomx,.85))(self) end
			if meter > 16 and meter < 25 then
				self:croptop(-1)
			else
				self:stoptweening();
				self:croptop(1);
			end

		end;

			CurrentStepsP1ChangedMessageCommand=function(self)
				if player == PLAYER_1 then
					self:playcommand("DiffDisplay");
					self:playcommand("Move");
				end
			end;
			CurrentStepsP2ChangedMessageCommand=function(self)
				if player == PLAYER_2 then
					self:playcommand("DiffDisplay");
					self:playcommand("Move");
				end
			end;
			MoveCommand=cmd(y,SCREEN_BOTTOM-42;sleep,.4;y,SCREEN_BOTTOM-39;sleep,.15;y,SCREEN_BOTTOM-45;queuecommand,"Move");
			OffCommand=cmd(stoptweening;hidden,1);
	}
	end
return red
end

local function danger(player)
	return LoadActor("Glows/red diff efect") .. {
		SetCommand=function(self)
			local steps = GAMESTATE:GetCurrentSteps(player)
			local iDiff = steps:GetMeter()

			if iDiff < 17 then self:hidden(1) else
				self:finishtweening();
				self:hidden(1);
				self:sleep(.3);
				self:hidden(0);
				self:playcommand( "Show" );
			end
		end;

		CurrentStepsP1ChangedMessageCommand=function(self)
			if player == PLAYER_1 then
				self:playcommand("Set");
			end;
		end;
		CurrentStepsP2ChangedMessageCommand=function(self)
			if player == PLAYER_2 then
				self:playcommand("Set");
			end;
		end;
		ShowCommand=cmd(diffusealpha,1;accelerate,0.25;diffusealpha,0;sleep,0.3;queuecommand,"Show");
		OffCommand=cmd(stoptweening,stopeffect,finishtweening;diffusealpha,0);
	}
end

local function glow(player)
	return LoadActor( "Glows/diff glow" ) .. {
		CurrentStepsP1ChangedMessageCommand=function(self)
			if player == PLAYER_1 then
				self:playcommand("Update");
			end;
		end;
		CurrentStepsP2ChangedMessageCommand=function(self)
			if player == PLAYER_2 then
				self:playcommand("Update");
			end;
		end;
		OnCommand=cmd(stoptweening;diffusealpha,0;fadeleft,.1;faderight,.1;sleep,.6;playcommand,"Update");
		OffCommand=cmd(stoptweening;diffusealpha,0);
		UpdateCommand=cmd(finishtweening;diffusealpha,0;zoomx,1.6;sleep,.3;linear,.15;diffusealpha,1;zoomx,1;sleep,.25;linear,.05;diffusealpha,0);
	}
end

return Def.ActorFrame {

	Def.ActorFrame {
		InitCommand=cmd(y,SCREEN_CENTER_Y+197;queuecommand,"Start");
		StartCommand=cmd(diffusealpha,0;sleep,2;diffusealpha,1);
		OffCommand=cmd(stoptweening;hidden,1);
		diffdisplaygray(PLAYER_1)..{ OnCommand=function(self)
			local sides = GAMESTATE:GetNumSidesJoined()
				self:x(sides == 1 and 235 or 164); end; };
		diffdisplaygray(PLAYER_2)..{ OnCommand=function(self)
			local sides = GAMESTATE:GetNumSidesJoined()
				self:x(sides == 1 and 235 or 317); end; };
		diffdisplay(PLAYER_1)..{ OnCommand=function(self)
			local sides = GAMESTATE:GetNumSidesJoined()
				self:x(sides == 1 and 235 or 164); end; };
		diffdisplay(PLAYER_2)..{ OnCommand=function(self)
			local sides = GAMESTATE:GetNumSidesJoined()
				self:x(sides == 1 and 235 or 317); end; };
	};

	Def.ActorFrame {
		InitCommand=cmd(queuecommand,"Start");
		StartCommand=cmd(diffusealpha,0;sleep,2;diffusealpha,1);
		OffCommand=cmd(stoptweening;hidden,1);
		skull(PLAYER_1)..{ BeginCommand=function(self)
			local sides = GAMESTATE:GetNumSidesJoined()
				self:x(sides == 1 and 235 or 164) end; };

		skull(PLAYER_2)..{ BeginCommand=function(self)
			local sides = GAMESTATE:GetNumSidesJoined()
				self:x(sides == 1 and 235 or 317); end; };

	};

	Lv(PLAYER_1) .. {
		InitCommand=function(self)
			local pn = GAMESTATE:GetNumSidesJoined()
			self:y(SCREEN_BOTTOM-26);
			self:x(pn == 1 and 136 or SCREEN_LEFT+200);
			self:shadowlength(0);
			self:diffusealpha(0);
			self:playcommand("Appear");
		end;			
		AppearCommand=cmd(croptop,1;zoom,0.6;sleep,1.3;croptop,-1);
		};

	Lv(PLAYER_2) .. {
		InitCommand=function(self)
			local pn = GAMESTATE:GetNumSidesJoined()
			self:y(SCREEN_BOTTOM-26);
			self:x(pn == 1 and 136 or SCREEN_RIGHT-200);
			self:shadowlength(0);
			self:diffusealpha(0);
			self:playcommand("Appear");
		end;			
		AppearCommand=cmd(croptop,1;zoom,0.6;sleep,1.3;croptop,-1);
		};


	danger(PLAYER_1)..{ InitCommand=function(self)
		local pn=GAMESTATE:GetNumPlayersEnabled()
			if pn == 2 then self:cropright(.45); self:faderight(.1); else self:cropright(-1); self:faderight(0); end
			(cmd(y,240;x,320;blend,"BlendMode_Add";diffusealpha,0))(self) end; };

	danger(PLAYER_2)..{ InitCommand=function(self)
		local pn=GAMESTATE:GetNumPlayersEnabled()
			if pn == 2 then self:cropleft(.45); self:fadeleft(.1); else self:cropleft(-1); self:fadeleft(0); end
			(cmd(y,240;x,320;blend,"BlendMode_Add";diffusealpha,0))(self) end; };


	meter(PLAYER_1) .. { InitCommand=function(self)
		local sides = GAMESTATE:GetNumSidesJoined()
			self:x(sides == 1 and SCREEN_LEFT+180 or SCREEN_CENTER_X-167); 
			self:y(sides == 1 and SCREEN_BOTTOM-37 or SCREEN_BOTTOM-38);
			self:diffusealpha(0);
			self:shadowlength(0); end; };

	meter(PLAYER_2) .. { InitCommand=function(self)
		local sides = GAMESTATE:GetNumSidesJoined()
			self:x(sides == 1 and SCREEN_LEFT+180 or SCREEN_CENTER_X+167);
			self:y(sides == 1 and SCREEN_BOTTOM-37 or SCREEN_BOTTOM-38);
			self:shadowlength(0);
			self:diffusealpha(0); end; };

	meter(PLAYER_1) .. { InitCommand=function(self)
		local sides = GAMESTATE:GetNumSidesJoined()
			self:x(sides == 1 and SCREEN_LEFT+180 or SCREEN_CENTER_X-167);
			self:y(sides == 1 and SCREEN_BOTTOM-37 or SCREEN_BOTTOM-38);
			self:blend("BlendMode_Add");
			self:shadowlength(0);
			self:diffusealpha(0); end; };

	meter(PLAYER_2) .. { InitCommand=function(self)
		local sides = GAMESTATE:GetNumSidesJoined()
			self:x(sides == 1 and SCREEN_LEFT+180 or SCREEN_CENTER_X+167);
			self:y(sides == 1 and SCREEN_BOTTOM-37 or SCREEN_BOTTOM-38);
			self:blend("BlendMode_Add");
			self:shadowlength(0);
			self:diffusealpha(0); end; };


	Unknow(PLAYER_1) .. { InitCommand=function(self)
		local sides = GAMESTATE:GetNumSidesJoined()
			self:x(sides == 1 and SCREEN_CENTER_X-145 or SCREEN_CENTER_X-170);
			self:y(sides == 1 and SCREEN_CENTER_Y+203 or SCREEN_CENTER_Y+197);
			self:diffusealpha(0); end; };

	Unknow(PLAYER_2) .. { InitCommand=function(self)
		local sides = GAMESTATE:GetNumSidesJoined()
			self:x(sides == 1 and SCREEN_CENTER_X-145 or SCREEN_CENTER_X+166);
			self:y(sides == 1 and SCREEN_CENTER_Y+203 or SCREEN_CENTER_Y+197);
			self:diffusealpha(0); end; };


	glow(PLAYER_1)..{ InitCommand=function(self)
		local pn=GAMESTATE:GetNumPlayersEnabled()
			if pn == 2 then self:cropright(.45); else self:cropright(-1);end
			(cmd(y,245;x,320;blend,"BlendMode_Add";diffusealpha,0))(self) end; };

	glow(PLAYER_2)..{ InitCommand=function(self)
		local pn=GAMESTATE:GetNumPlayersEnabled()
			if pn == 2 then self:cropleft(.45); else self:cropleft(-1); end
			(cmd(y,245;x,320;blend,"BlendMode_Add";diffusealpha,0))(self) end; };

	Def.ActorFrame {
		Condition=GAMESTATE:GetNumPlayersEnabled()==1;
		InitCommand=cmd(x,SCREEN_CENTER_X+40;y,SCREEN_CENTER_Y+199);
		OnCommand=cmd(diffusealpha,0;sleep,0.5;diffusealpha,1);
		CurrentStepsP1ChangedMessageCommand=cmd(playcommand,"Set");
		CurrentStepsP2ChangedMessageCommand=cmd(playcommand,"Set");
		OffCommand=cmd(hidden,1);

	LoadActor("Glows/Danger") .. {
		SetCommand=function(self)
			local steps = GAMESTATE:GetCurrentSteps(GAMESTATE:GetFirstHumanPlayer());
			local meter = steps:GetMeter();

			if not steps then
				self:hidden(1)
				return
			end

			self:hidden(0)

			if meter >= 25 then
				self:finishtweening();
				self:zoom(.6);
				self:diffusealpha(0);
				self:sleep(.3);
				self:linear(.3);
				self:diffusealpha(1);
			else
				self:hidden(1);
			end
		end;

	};

	LoadActor("Glows/Danger2") .. {
		SetCommand=function(self)
			local steps = GAMESTATE:GetCurrentSteps(GAMESTATE:GetFirstHumanPlayer());
			local meter = steps:GetMeter();

			if not steps then
				self:hidden(1)
				return
			end

			self:hidden(0)

			if meter >= 25 then
				self:playcommand( "Show" );
			else
				self:hidden(1);
			end
		end;
		ShowCommand=cmd(finishtweening;diffusealpha,0;sleep,0.3;linear,0.3;diffusealpha,1;playcommand,"Pulse");
		PulseCommand=cmd(zoom,.6;diffusealpha,1;linear,.5;diffusealpha,0;zoom,.8;sleep,.1;queuecommand,"Pulse");
	};

};

}	