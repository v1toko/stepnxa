local t = Def.ActorFrame {
	LoadSongBackground() .. {
		Condition=not GAMESTATE:IsMissionMode();
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;sleep,2);
	};
};

return t;
