return Def.ActorFrame {
	--[[LoadActor("../ScreenEvaluationStage background/fondo") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT);
		OnCommand=cmd(stoptweening;diffuse,0,0,0,1;sleep,.5;linear,.6;diffuse,1,1,1,1);
		EditCommand=cmd(finishtweening;linear,.5;diffuse,1,1,1,1);
		HideCommand=cmd(finishtweening;linear,1;diffuse,.5,.5,.5,1);
		PlayingCommand=cmd(playcommand,"Hide");
		RecordCommand=cmd(playcommand,"Hide");
		RecordPausedCommand=cmd(playcommand,"Hide");	};
		]]--

	LoadActor("help.png") .. {
		InitCommand=cmd(y,SCREEN_CENTER_Y);
		OnCommand=cmd(horizalign,left;finishtweening);
		EditCommand=cmd(playcommand,"Show");
		PlayingCommand=cmd(playcommand,"Hide");
		RecordCommand=cmd(playcommand,"Hide");
		RecordPausedCommand=cmd(playcommand,"Hide");
		ShowCommand=cmd(stoptweening;accelerate,0.25;x,SCREEN_LEFT);
		HideCommand=cmd(stoptweening;accelerate,0.25;x,SCREEN_LEFT-192);
	};
	LoadActor("info.png") .. {
		InitCommand=cmd(y,SCREEN_CENTER_Y);
		OnCommand=cmd(horizalign,right;finishtweening);
		EditCommand=cmd(playcommand,"Show");
		PlayingCommand=cmd(playcommand,"Hide");
		RecordCommand=cmd(playcommand,"Hide");
		RecordPausedCommand=cmd(playcommand,"Hide");
		ShowCommand=cmd(stoptweening;accelerate,0.25;x,SCREEN_RIGHT);
		HideCommand=cmd(stoptweening;accelerate,0.25;x,SCREEN_RIGHT+192);
	};


};
