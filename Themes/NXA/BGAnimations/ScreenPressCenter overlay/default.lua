local t = Def.ActorFrame {
	
	LoadActor ("../ScreenLogo background/NXAbsolute" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;FullScreen);
	};

	LoadActor("../Common Utils/start") .. {
		OnCommand=cmd();
		OffCommand=cmd(play);
	};

	LoadActor( "../Common Utils/_press 5x2" ) .. {
		Condition = GAMESTATE:GetCoinMode() == "CoinMode_Pay" and GAMESTATE:GetCoins() > 0 or GAMESTATE:GetCoinMode() == "CoinMode_Free";
		Frames = Sprite.LinearFrames(10,.47);
		InitCommand = cmd(x,SCREEN_LEFT+75;y,SCREEN_BOTTOM-87;zoom,.92);
		OffCommand=cmd(stoptweening;diffusealpha,0);
	};

	LoadActor( "../Common Utils/_press 5x2" ) .. {
		Condition = GAMESTATE:GetCoinMode() == "CoinMode_Pay" and GAMESTATE:GetCoins() > 0 or GAMESTATE:GetCoinMode() == "CoinMode_Free";
		Frames = Sprite.LinearFrames(10,.47);
		InitCommand = cmd(x,SCREEN_RIGHT-75;y,SCREEN_BOTTOM-87;zoom,.92);
		OffCommand=cmd(stoptweening;diffusealpha,0);
	};

	LoadActor("../ScreenTitleMenu overlay") .. {};

}
return t
