local t = Def.ActorFrame {

	LoadActor("JoinedA") .. {
		InitCommand=cmd(draworder,99999999);
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;sleep,0.05;linear,0.75;x,SCREEN_CENTER_X-445);
	};


	LoadActor("JoinedB") .. {
		InitCommand=cmd(draworder,99999999);
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;sleep,0.05;linear,0.75;x,SCREEN_CENTER_X+445);
	};
        
	LoadActor("Orbe") .. {
		InitCommand=cmd(draworder,99999999);
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;linear,0.15;diffuse,1,1,1,0);
	};

}
return t 
