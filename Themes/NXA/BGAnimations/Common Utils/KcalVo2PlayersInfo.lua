return Def.ActorFrame {
	--kcal roja, vo2 azul
	--primero player 1
--*********************************************MEDIR ACTIVIDAD F�SICA****************************************************
--***********************PLAYER 1, UN JUGADOR

		LoadFont( "VO2" ) .. {
			SetVO2Player1MessageCommand=function(self, param)
				local Vo2 = param.iVO2
				Vo2 = string.format( "%03i", Vo2 );
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
					self:hidden(1)
				return
			end
					
				if Vo2 then
					self:settext( Vo2 );
					self:hidden(0);
				end
			end;

			Condition=GAMESTATE:GetNumPlayersEnabled()==1 and GAMESTATE:IsPlayerEnabled(PLAYER_1);
			InitCommand=cmd(x,418;y,313;zoom,0.5;shadowlength,0);
			CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
			OffCommand=cmd(stoptweening;hidden,1);

		};
		
		LoadFont( "Calories" ) .. {
			SetKcalPlayer1MessageCommand=function(self, param)
				local Kcal = param.iKcal
				Kcal = string.format( "%03i", Kcal );
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
					self:hidden(1)
				return

				end
					
				if Kcal then
					self:hidden(0);
					self:settext( Kcal );					
				end
			end;
		
			Condition=GAMESTATE:GetNumPlayersEnabled()==1 and GAMESTATE:IsPlayerEnabled(PLAYER_1);
			InitCommand=cmd(x,223;y,313;zoom,0.5;shadowlength,0);
			CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
			OffCommand=cmd(stoptweening;hidden,1);


		};


--***********************PLAYER 2, UN JUGADOR


		LoadFont( "VO2" ) .. {
			SetVO2Player2MessageCommand=function(self, param)
				local Vo2 = param.iVO2
				Vo2 = string.format( "%03i", Vo2 );
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
					self:hidden(1)
				return

				end
					
				if Vo2 then
					self:settext( Vo2 );
					self:hidden(0);
				end
			end;

			Condition=GAMESTATE:GetNumPlayersEnabled()==1 and GAMESTATE:IsPlayerEnabled(PLAYER_2);
			InitCommand=cmd(x,418;y,313;zoom,0.5;shadowlength,0);
			CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
			OffCommand=cmd(stoptweening;hidden,1);


		};
		
		LoadFont( "Calories" ) .. {
			SetKcalPlayer2MessageCommand=function(self, param)
				local Kcal = param.iKcal
				Kcal = string.format( "%03i", Kcal );
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
					self:hidden(1)
				return

				end
					
				if Kcal then
					self:hidden(0);
					self:settext( Kcal );
				end
			end;
		
			Condition=GAMESTATE:GetNumPlayersEnabled()==1 and GAMESTATE:IsPlayerEnabled(PLAYER_2);
			InitCommand=cmd(x,223;y,313;zoom,0.5;shadowlength,0);
			CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
			OffCommand=cmd(stoptweening;hidden,1);


		};


--***********************PLAYER 1, DOS JUGADORES


		LoadFont( "VO2" ) .. {
			SetVO2Player1MessageCommand=function(self, param)
				local Vo2 = param.iVO2
				Vo2 = string.format( "%03i", Vo2 );
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
					self:hidden(1)
				return

				end
					
				if Vo2 then
					self:hidden(0);
					self:settext( Vo2 );
				end
			end;

			Condition=GAMESTATE:GetNumPlayersEnabled()==2 and GAMESTATE:IsPlayerEnabled(PLAYER_1);
			InitCommand=cmd(x,241;y,313;zoom,0.45;zoomy,0.5;shadowlength,0);
			CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
			OffCommand=cmd(stoptweening;hidden,1);


		};
		
		LoadFont( "Calories" ) .. {
			SetKcalPlayer1MessageCommand=function(self, param)
				local Kcal = param.iKcal
				Kcal = string.format( "%03i", Kcal );
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
					self:hidden(1)
				return

				end
					
				if Kcal then
					self:hidden(0);
					self:settext( Kcal );
				end
			end;
		
			Condition=GAMESTATE:GetNumPlayersEnabled()==2 and GAMESTATE:IsPlayerEnabled(PLAYER_1);
			InitCommand=cmd(x,212;y,313;zoom,0.45;zoomy,0.5;shadowlength,0);
			CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
			OffCommand=cmd(stoptweening;hidden,1);


		};


--***********************PLAYER 2, DOS JUGADORES


		LoadFont( "VO2" ) .. {
			SetVO2Player2MessageCommand=function(self, param)
				local Vo2 = param.iVO2
				Vo2 = string.format( "%03i", Vo2 );
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
					self:hidden(1)
				return

				end
					
				if Vo2 then
					self:hidden(0);
					self:settext( Vo2 );
				end
			end;

			Condition=GAMESTATE:GetNumPlayersEnabled()==2 and GAMESTATE:IsPlayerEnabled(PLAYER_2);
			InitCommand=cmd(x,432;y,313;zoom,0.45;zoomy,0.5;shadowlength,0);
			CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
			OffCommand=cmd(stoptweening;hidden,1);


		};
		
		LoadFont( "Calories" ) .. {
			SetKcalPlayer2MessageCommand=function(self, param)
				local Kcal = param.iKcal
				Kcal = string.format( "%03i", Kcal );
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
					self:hidden(1)
				return

				end
					
				if Kcal then
					self:hidden(0);
					self:settext( Kcal );
				end
			end;
		
			Condition=GAMESTATE:GetNumPlayersEnabled()==2 and GAMESTATE:IsPlayerEnabled(PLAYER_2);
			InitCommand=cmd(x,402;y,313;zoom,0.45;zoomy,0.5;shadowlength,0);
			CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
			OffCommand=cmd(stoptweening;hidden,1);


		};
};
