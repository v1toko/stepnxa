return Def.ActorFrame {
	LoadFont( "ScoreName" ) .. {
		SendHighScoreNameMessageCommand=function(self, param)
			local sName = param.sScoreName
			local song = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
			
			if song then self:settext("") return end 
			
			if not sName then self:settext("") return end
						
			self:settext( sName )
		end;
		
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+55;shadowlength,0;zoomy,.9);
		CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
		OffCommand=cmd(finishtweening;linear,0.3;diffusealpha,0);
	};
	
	LoadFont( "Record Font" ) .. {
		SendHighScoreMessageCommand=function(self, param)
			local iScore = param.iScore
			local song = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
			
			if song then self:settext("") return end 
			
			if iScore == 0 then self:settext("") return end
			
			self:settext( iScore )
		end;
		
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+70;zoom,0.3;shadowlength,0);
		CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.8;diffusealpha,1);
		OffCommand=cmd(finishtweening;linear,0.3;diffusealpha,0);
	};
};