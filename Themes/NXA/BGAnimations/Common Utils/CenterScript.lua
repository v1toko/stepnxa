return Def.ActorFrame {

		LoadActor( "_press 5x2" ) .. {
			Frames = Sprite.LinearFrames(10,.47);
			CenterWasPressedOnceMessageCommand=cmd(finishtweening;diffusealpha,1);
			CenterWasPressedTwiceMessageCommand=cmd(diffusealpha,0);
			OffCommand=cmd(diffusealpha,0);
			CurrentSongChangedMessageCommand=cmd(diffusealpha,0);
			StyleChangedMessageCommand = cmd(diffusealpha,0);
			CenterHiddenMessageCommand = cmd(diffusealpha,0);
			InitCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+75;diffusealpha,0;zoom,.85); };
};