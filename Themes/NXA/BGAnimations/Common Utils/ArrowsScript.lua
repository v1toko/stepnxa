return Def.ActorFrame {
	LoadActor("UpLeft") .. {
		InitCommand=cmd(x,62;y,82;blend,"BlendMode_Add";diffusealpha,0);
		OnCommand=cmd(diffusealpha,0;sleep,1.2;diffusealpha,1;diffusecolor,color("#F17437");zoom,1;linear,0.3;zoom,1.15;diffusealpha,0);
		UpLeftPressedMessageCommand=cmd(finishtweening;diffusealpha,1;zoom,1.15;linear,.25;zoom,1;linear,0.3;zoom,1.15;diffusealpha,.5;linear,.1;diffusealpha,0);
		OffCommand=cmd(diffusealpha,0);
	};
	LoadActor("UpRight") .. {
		InitCommand=cmd(x,578;y,82;blend,"BlendMode_Add";diffusealpha,0);
		OnCommand=cmd(diffusealpha,0;sleep,1.2;diffusealpha,1;diffusecolor,color("#F17437");zoom,1;linear,0.3;zoom,1.15;diffusealpha,0);
		UpRightPressedMessageCommand=cmd(finishtweening;diffusealpha,1;zoom,1;zoom,1.15;linear,.25;zoom,1;linear,0.3;zoom,1.15;diffusealpha,.5;linear,.1;diffusealpha,0);
		OffCommand=cmd(diffusealpha,0);
	};
	LoadActor("DownLeft") .. {
		InitCommand=cmd(x,62;y,407;blend,"BlendMode_Add";diffusealpha,0);
		OnCommand=cmd(diffusealpha,0;sleep,1.2;diffusealpha,1;diffusecolor,color("#1587C5");zoom,1;linear,0.3;zoom,1.15;diffusealpha,0);
		DownLeftPressedMessageCommand=cmd(finishtweening;diffusealpha,1;zoom,1.15;linear,.25;zoom,1;zoom,1;linear,0.3;zoom,1.15;diffusealpha,.5;linear,.1;diffusealpha,0);
		OffCommand=cmd(diffusealpha,0);
	};
	LoadActor("DownRight") .. {
		InitCommand=cmd(x,578;y,407;blend,"BlendMode_Add";diffusealpha,0);
		OnCommand=cmd(diffusealpha,0;sleep,1.2;diffusealpha,1;diffusecolor,color("#1587C5");zoom,1;linear,0.3;zoom,1.15;diffusealpha,0);
		DownRightPressedMessageCommand=cmd(finishtweening;diffusealpha,1;zoom,1.15;linear,.25;zoom,1;zoom,1;linear,0.3;zoom,1.15;diffusealpha,.5;linear,.1;diffusealpha,0);
		OffCommand=cmd(diffusealpha,0);
	};
};