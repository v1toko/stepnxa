local t = 	Def.ActorFrame {
	LoadActor("Display.png") .. {
		SetCommand=function(self)
			--local player = GetProfileDisplayName( PLAYER_2 )
			local benabled = GAMESTATE:IsPlayerEnabled( PLAYER_1 ) and PROFILEMAN:IsPersistentProfile( PLAYER_1 )

			if not benabled then
				self:hidden(1)
				return
			end

			self:hidden(0)
			--player = string.upper( player )

			--if player then
				--self:settext( player );
				--self:visible( true );
			--else
				--self:visible( true );
			--end
		end;
		--Condition = PROFILEMAN:IsPersistentProfile( PLAYER_1 );
		InitCommand=cmd( x,70;y,461);
		ScreenChangedMessageCommand=cmd( playcommand, "Set" );
	};
	
	LoadActor("Display2.png") .. {
		SetCommand=function(self)
			--local player = GetProfileDisplayName( PLAYER_2 )
			local benabled = GAMESTATE:IsPlayerEnabled( PLAYER_1 ) and PROFILEMAN:IsPersistentProfile( PLAYER_1 )

			if not benabled then
				self:hidden(1)
				return
			end

			self:hidden(0)
		end;
		InitCommand=cmd( x,70;y,461;blend,"BlendMode_Add");
		ScreenChangedMessageCommand=cmd( playcommand, "Set" );
	};

	LoadFont("BoostSSi") .. {
		SetCommand=function(self)
			local player = GetProfileDisplayName( PLAYER_1 )
			local benabled = GAMESTATE:IsPlayerEnabled( PLAYER_1 ) and PROFILEMAN:IsPersistentProfile( PLAYER_1 )

			if not benabled then
				self:hidden(1)
				return
			end

			self:hidden(0)
			player = string.upper( player )

			if player then
				self:settext( player );
			else
				self:settext( "No Player" );
			end

			if player == "" then
				self:settext( "PLAYER 1" );
			end
		end;

		--Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		InitCommand=cmd(horizalign,"HorizAlign_Left";x,10;y,461;playcommand,"Set";zoom,0.75;shadowlength,0;maxwidth,300);
		ScreenChangedMessageCommand=cmd( playcommand, "Set" );
	};

	--------------------------------------------------------------------------------------------------
	
	LoadActor("Display.png") .. {
		SetCommand=function(self)
			--local player = GetProfileDisplayName( PLAYER_2 )
			local benabled = GAMESTATE:IsPlayerEnabled( PLAYER_2 ) and PROFILEMAN:IsPersistentProfile( PLAYER_2 )

			if not benabled then
				self:hidden(1)
				return
			end

			self:hidden(0)
			--player = string.upper( player )

			--if player then
				--self:settext( player );
				--self:visible( true );
			--else
				--self:visible( true );
			--end
		end;
		--Condition = PROFILEMAN:IsPersistentProfile( PLAYER_2 );
		InitCommand=cmd( x,570;y,461;rotationy,180);
		ScreenChangedMessageCommand=cmd( playcommand, "Set" );
	};

	LoadActor("Display2.png") .. {
		SetCommand=function(self)
			--local player = GetProfileDisplayName( PLAYER_2 )
			local benabled = GAMESTATE:IsPlayerEnabled( PLAYER_2 ) and PROFILEMAN:IsPersistentProfile( PLAYER_2 )

			if not benabled then
				self:hidden(1)
				return
			end

			self:hidden(0)
			--player = string.upper( player )

			--if player then
				--self:settext( player );
				--self:visible( true );
			--else
				--self:visible( true );
			--end
		end;
		InitCommand=cmd( x,570;y,461;blend,"BlendMode_Add";rotationy,180);
		ScreenChangedMessageCommand=cmd( playcommand, "Set" );
	};
	
	LoadFont("BoostSSi") .. {
		SetCommand=function(self)
			local player = GetProfileDisplayName( PLAYER_2 )
			local benabled = GAMESTATE:IsPlayerEnabled( PLAYER_2 ) and PROFILEMAN:IsPersistentProfile( PLAYER_2 )

			if not benabled then
				self:hidden(1)
				return
			end

			self:hidden(0)
			player = string.upper( player )

			if player then
				self:settext( player );
			else
				self:settext( "No Player" );
			end

			if player == "" then
				self:settext( "PLAYER 2" );
			end

		end;

		InitCommand=cmd(horizalign,"HorizAlign_Right";x,SCREEN_RIGHT-10;y,461;zoom,0.75;shadowlength,0;maxwidth,300);
		ScreenChangedMessageCommand=cmd( playcommand, "Set" );
	};
};

return t