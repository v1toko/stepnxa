local t = Def.ActorFrame {
--****************************************Accesorios Para Desbloquear las canciones

	LoadActor("price") .. {
		PriceCommand=function(self)
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if not Locked then
				self:hidden(1)
				return

				end
					
				if Locked then
				self:hidden(0);
				self:playcommand( "Show" );
				end
			end;
	
		InitCommand=cmd(x,SCREEN_CENTER_X+1.5;y,SCREEN_CENTER_Y+17;zoomx,.94);
		OffCommand=cmd(stoptweening;hidden,1);
		CurrentSongChangedMessageCommand=cmd(playcommand,"Price");
		ShowCommand=cmd(finishtweening;diffusealpha,0;sleep,0.85;linear,0.1;diffusealpha,1);
	};



	LoadFont("milleage") .. { 
		SetCommand=function(self)
			local song = GAMESTATE:GetCurrentSong()
			local milleage = song:GetSongMileageCost()
			local locked = song:SpecialSongIsLocked()

			if not locked then
				self:hidden(1)
				return
			end

			self:hidden(0)
			milleage = string.format( "%.3i", milleage );

			if milleage then
				self:settext( milleage );
				self:playcommand( "Show" );
			else
				self:settext( "" );
			end
		end;

		OnCommand=cmd( x,SCREEN_CENTER_X+1.5;y,SCREEN_CENTER_Y+17;playcommand,"Set";zoom,0.45;shadowlength,0;diffusealpha,0;linear,1;diffusealpha,1);
		OffCommand=cmd(stoptweening;hidden,1);
		CurrentSongChangedMessageCommand=cmd(playcommand,"Set");
		ShowCommand=cmd(finishtweening;diffusealpha,0;sleep,1;linear,0.5;diffusealpha,1);
	};


	LoadActor("../brillo") .. {
		PriceCommand=function(self)
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if not Locked then
				self:hidden(1)
				return

				end
					
				if Locked then
				self:hidden(0);
				self:playcommand( "Show" );
				end
			end;
	
		InitCommand=cmd(x,SCREEN_CENTER_X+1.5;y,SCREEN_CENTER_Y+17;blend,"BlendMode_Add");
		OffCommand=cmd(stoptweening;hidden,1);
		CurrentSongChangedMessageCommand=cmd(playcommand,"Price");
		ShowCommand=cmd(finishtweening;diffusealpha,0;zoomy,.45;zoomx,.9;sleep,.9;diffusealpha,1;linear,.25;zoomx,1.75;zoomy,.55;sleep,.1;linear,.1;diffusealpha,0);
	};


	LoadActor("LockUp") .. {
		LockCommand=function(self)
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if not Locked then
				self:hidden(1)
				return

				end
					
				self:sleep(1)
				self:hidden(0)

				if Locked then
				self:playcommand( "Show" );
				end
			end;
	
		InitCommand=cmd(diffusealpha,0;zoomy,.99);
		OffCommand=cmd(stoptweening;hidden,1);
		CurrentSongChangedMessageCommand=cmd(playcommand,"Lock");
		ShowCommand=cmd(finishtweening;x,SCREEN_CENTER_X+1.7;y,SCREEN_CENTER_Y+17;cropbottom,0;diffusealpha,0;sleep,0.8;diffusealpha,1;linear,0.35;cropbottom,1);
	};
	LoadActor("LockDown") .. {
		LockCommand=function(self)
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if not Locked then
				self:hidden(1)
				return

				end
					
				self:sleep(1)
				self:hidden(0)

				if Locked then
				self:playcommand( "Show" );
				end
			end;
	
		InitCommand=cmd(diffusealpha,0;zoomy,.99);
		OffCommand=cmd(stoptweening;hidden,1);
		CurrentSongChangedMessageCommand=cmd(playcommand,"Lock");
		ShowCommand=cmd(finishtweening;x,SCREEN_CENTER_X+1.7;y,SCREEN_CENTER_Y+18;croptop,0;diffusealpha,0;sleep,0.8;diffusealpha,1;linear,0.35;croptop,1);
	};


	LoadActor( "../../Common Utils/CenterScript" ) .. {
		CenterWasPressedOnceMessageCommand=function(self)
				local Locked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
		
				if Locked then
				self:hidden(1)
				return

				end
					
				self:hidden(0);
				end;

		OffCommand=cmd(stoptweening;hidden,1);
		OnCommand=cmd(addy,-51);
	};

	Def.Quad {
		InitCommand=cmd(stretchto,SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM;diffuse,color("#000000");diffusealpha,0);
		OffCommand=cmd(stoptweening;linear,0.3;diffusealpha,0);
		PulseCommand=cmd(finishtweening;linear,0.3;diffusealpha,0.4);
		BuyingSongMessageCommand=cmd(playcommand,"Pulse");
		SongBoughtMessageCommand=cmd(playcommand,"Off");
		CurrentSongChangedMessageCommand=cmd(playcommand,"Off");
		StyleChangedMessageCommand=cmd(playcommand,"Off");

	};


	LoadActor("MachineMilleage") .. {
		Condition=not GAMESTATE:IsAnyHumanPlayerUsingMemoryCard();
		InitCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+90;zoomx,0.8);
		OffCommand=cmd(stoptweening;linear,0.3;diffusealpha,0;zoomy,0);
		BuyingSongMessageCommand=cmd(finishtweening;zoomy,0.7;diffusealpha,1;cropleft,0.5;cropright,0.5;linear,0.3;cropleft,0;cropright,0);
		SongBoughtMessageCommand=cmd(playcommand,"Off");
		CurrentSongChangedMessageCommand=cmd(playcommand,"Off");
		StyleChangedMessageCommand=cmd(playcommand,"Off");
	};

	LoadActor("PlayerMilleage") .. {
		Condition=GAMESTATE:IsAnyHumanPlayerUsingMemoryCard();
		InitCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X-10;y,SCREEN_CENTER_Y+85;zoomx,0.8);
		OffCommand=cmd(stoptweening;linear,0.3;diffusealpha,0;zoomy,0);
		BuyingSongMessageCommand=cmd(finishtweening;zoomy,0.8;diffusealpha,1;cropleft,0.5;cropright,0.5;linear,0.3;cropleft,0;cropright,0);
		SongBoughtMessageCommand=cmd(playcommand,"Off");
		CurrentSongChangedMessageCommand=cmd(playcommand,"Off");
		StyleChangedMessageCommand=cmd(playcommand,"Off");
		CenterWasPressedP1MessageCommand = function(self)
			--local player = GAMESTATE:IsPlayerEnabled( PLAYER_1 );
			local profile = PROFILEMAN:IsPersistentProfile( PLAYER_1 );
				if not profile then
					self:hidden(1);
				end
					self:hidden(0)
			end;

		CenterWasPressedP2MessageCommand = function(self)
			--local player = GAMESTATE:IsPlayerEnabled( PLAYER_2 );
			local profile = PROFILEMAN:IsPersistentProfile( PLAYER_2 );
				if not profile or not PROFILEMAN:IsPersistentProfile( PLAYER_2 ) then
					self:hidden(1);
				end
					self:hidden(0)
			end;

	};

	LoadActor("UnlockedMessage") .. {
		InitCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+90;zoomx,0.8);
		OffCommand=cmd(linear,0.3;diffusealpha,0;zoomy,0);
		SongBoughtMessageCommand=cmd(finishtweening;diffusealpha,1;zoomy,0;linear,0.3;zoomy,0.7;sleep,0.5;playcommand,"Off");
	};

	LoadFont("(player)milleage") .. { 
		BuyingSongMessageCommand=function(self, param)
			local song = GAMESTATE:GetCurrentSong()
			local mileage = param.iMileage
			local block = song:SpecialSongIsLocked()

			if not block then
				self:hidden(1)
				return
			end

			self:hidden(0)
			mileage = string.format( "%.7i", mileage );

			if mileage then
				self:settext( mileage );
				self:playcommand( "Show" );
			else
				self:settext( "" );
			end
		end;

		OnCommand=cmd( x,SCREEN_CENTER_X+50;y,SCREEN_CENTER_Y+86;zoomy,0.4;zoomx,0.33;shadowlength,0);
		OffCommand=cmd(stoptweening;linear,0.4;diffusealpha,0 );
		CurrentSongChangedMessageCommand=cmd(playcommand,"Hide");
		ShowCommand=cmd(finishtweening;diffusealpha,0;sleep,0.5;linear,0.1;diffusealpha,1);
		HideCommand=cmd( linear,0.1; diffusealpha,0);
		StyleChangedMessageCommand=cmd(playcommand,"Hide");
	};

	LoadActor("Center") .. {
		Condition=GAMESTATE:GetPlayMode() == "PlayMode_Special";
		InitCommand=cmd(addy,24;addx,210);
	};

	LoadActor("BrokenLock") .. {
		Condition=GAMESTATE:GetPlayMode() == "PlayMode_Special";};

	LoadActor("NotEnoughtMilleage") .. {
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-14;diffusealpha,0);
		OffCommand=cmd(stoptweening;linear,0.3;diffusealpha,0);
		PulseCommand=cmd(finishtweening;diffusealpha,1;cropleft,0.5;cropright,0.5;linear,0.3;cropleft,0;cropright,0;sleep,1;linear,0.3;diffusealpha,0);
		NotEnoughMileageMessageCommand=cmd(playcommand,"Pulse");
		CurrentSongChangedMessageCommand=cmd(stoptweening;diffusealpha,0);
		BuyingSongMessageCommand=cmd(stoptweening;diffusealpha,0);
	};

};
return t;
