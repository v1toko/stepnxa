local t = Def.ActorFrame {

	Def.ActorFrame {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-9);
		OnCommand=cmd(hidden,1);

	LoadActor("Broken/Broken1A") .. {	
		SongBoughtMessageCommand=cmd(finishtweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-8;hidden,0;sleep,.2;playcommand,"Unlock");
		UnlockCommand=cmd(linear,.55;addx,-400;addy,-650);
	};
	LoadActor("Broken/Broken1B") .. {	
		SongBoughtMessageCommand=cmd(finishtweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-8;hidden,0;sleep,.2;playcommand,"Unlock");
		UnlockCommand=cmd(linear,.55;addx,-650;addy,-400);
	};
	LoadActor("Broken/Broken2A") .. {	
		SongBoughtMessageCommand=cmd(finishtweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-8;hidden,0;sleep,.2;playcommand,"Unlock");
		UnlockCommand=cmd(linear,.55;addx,400;addy,-650);
	};

	LoadActor("Broken/Broken2B") .. {	
		SongBoughtMessageCommand=cmd(finishtweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-8;hidden,0;sleep,.2;playcommand,"Unlock");
		UnlockCommand=cmd(linear,.55;addx,650;addy,-400);
	};
};


	Def.ActorFrame {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-9);
		OnCommand=cmd(hidden,1);

	LoadActor("Broken/Broken3A") .. {	
		SongBoughtMessageCommand=cmd(finishtweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-8;hidden,0;sleep,.2;playcommand,"Unlock");
		UnlockCommand=cmd(linear,.55;addx,-400;addy,650);
	};
	LoadActor("Broken/Broken3B") .. {	
		SongBoughtMessageCommand=cmd(finishtweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-8;hidden,0;sleep,.2;playcommand,"Unlock");
		UnlockCommand=cmd(linear,.55;addx,-650;addy,400);
	};
	LoadActor("Broken/Broken4A") .. {	
		SongBoughtMessageCommand=cmd(finishtweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-8;hidden,0;sleep,.2;playcommand,"Unlock");
		UnlockCommand=cmd(linear,.55;addx,400;addy,650);
	};

	LoadActor("Broken/Broken4B") .. {	
		SongBoughtMessageCommand=cmd(finishtweening;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-8;hidden,0;sleep,.2;playcommand,"Unlock");
		UnlockCommand=cmd(linear,.55;addx,650;addy,400);
	};

};

	LoadActor("../brillo") .. {	
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-9;blend,"BlendMode_Add";zoom,.6);
		OnCommand=cmd(hidden,1);
		SongBoughtMessageCommand=cmd(finishtweening;zoom,.6;hidden,0;diffusealpha,.7;playcommand,"Unlock");
		UnlockCommand=cmd(sleep,.2;linear,.65;zoom,.9;diffusealpha,.5;linear,.1;diffusealpha,0);
	};

	LoadActor("Broken/Glow1") .. {	
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-9;blend,"BlendMode_Add";zoom,.4);
		OnCommand=cmd(hidden,1);
		SongBoughtMessageCommand=cmd(finishtweening;zoom,.4;hidden,0;diffusealpha,1;playcommand,"Unlock");
		UnlockCommand=cmd(sleep,.4;linear,.75;zoom,.7;diffusealpha,.5;linear,.1;diffusealpha,0);
	};

	LoadActor("Broken/Glow2") .. {	
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-9;blend,"BlendMode_Add";zoom,.1);
		OnCommand=cmd(hidden,1);
		SongBoughtMessageCommand=cmd(finishtweening;zoom,.1;hidden,0;diffusealpha,1;playcommand,"Unlock");
		UnlockCommand=cmd(sleep,.2;linear,.45;zoom,.8;diffusealpha,0);
	};

	LoadActor("Broken/Glow3") .. {	
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-9;blend,"BlendMode_Add";zoom,.1);
		OnCommand=cmd(hidden,1);
		SongBoughtMessageCommand=cmd(finishtweening;zoom,.1;hidden,0;diffusealpha,.9;playcommand,"Unlock");
		UnlockCommand=cmd(sleep,.2;linear,.45;zoom,.8;diffusealpha,0);
	};

	LoadActor("Broken/Glow2") .. {	
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-9;blend,"BlendMode_Add";zoom,.1);
		OnCommand=cmd(hidden,1);
		SongBoughtMessageCommand=cmd(finishtweening;zoom,.1;hidden,0;diffusealpha,.8;playcommand,"Unlock");
		UnlockCommand=cmd(sleep,.2;linear,.45;zoom,.4;diffusealpha,0);
	};

};
return t;
