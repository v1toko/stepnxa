local t = Def.ActorFrame {
	Condition=not GAMESTATE:IsEasyMode();

	Def.Actor { OnCommand=cmd(sleep,0.9); };
	
		--MODS
	--[[Def.OptionIconRow {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_1);
		InitCommand=cmd(x,SCREEN_LEFT+28;y,SCREEN_TOP+93;set,PLAYER_1);
		OnCommand=cmd(diffusealpha,0;linear,0.2;diffusealpha,1);
		PlayerOptionsChangedP1MessageCommand=cmd(set,PLAYER_1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	Def.OptionIconRow {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_2);
		InitCommand=cmd(x,SCREEN_RIGHT-38;y,SCREEN_TOP+93;set,PLAYER_2);
		OnCommand=cmd(diffusealpha,0;linear,0.2;diffusealpha,1);
		PlayerOptionsChangedP2MessageCommand=cmd(set,PLAYER_2);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};]]

	Def.Actor {
		--OnCommand=cmd(stoptweening;y,200;sleep,.2;decelerate,.1;y,-10;decelerate,.1;y,0);
		--StartSelectingStepsMessageCommand=cmd(stoptweening;y,0;accelerate,.1;y,-10;decelerate,.1;y,200);
		GoBackSelectingGroupMessageCommand=function(self) 
			--cmd(stoptweening;y,0;accelerate,.1;y,-10;decelerate,.1;y,200);
			local Wheel = SCREENMAN:GetTopScreen():GetChild('MusicWheel'); 
			Wheel:stoptweening();
			Wheel:addy(0);
			Wheel:accelerate(0.1);
			Wheel:addy(-10);
			Wheel:decelerate(0.1);
			Wheel:addy(600);

			local Banner = SCREENMAN:GetTopScreen():GetChild('Banner'); 
			Banner:stoptweening();
			Banner:addy(0);
			Banner:accelerate(0.1);
			Banner:addy(-10);
			Banner:decelerate(0.1);
			Banner:addy(600);

			local CDTitle = SCREENMAN:GetTopScreen():GetChild('CDTitle'); 
			CDTitle:stoptweening();
			CDTitle:addy(0);
			CDTitle:accelerate(0.1);
			CDTitle:addy(-10);
			CDTitle:decelerate(0.1);
			CDTitle:addy(600);
			--SCREENMAN:SystemMessage("GoBackSelectingGroupMessageCommand");
		end;
		StartSelectingSongMessageCommand=function(self)		
			local Wheel = SCREENMAN:GetTopScreen():GetChild('MusicWheel');
			Wheel:stoptweening();
			Wheel:addy(-600)
			Wheel:decelerate(0.1)
			Wheel:addy(10)
			Wheel:decelerate(0.1)
			Wheel:addy(0);

			local Banner = SCREENMAN:GetTopScreen():GetChild('Banner'); 
			Banner:stoptweening();
			Banner:addy(-600)
			Banner:decelerate(0.1)
			Banner:addy(10)
			Banner:decelerate(0.1)
			Banner:addy(0);

			local CDTitle = SCREENMAN:GetTopScreen():GetChild('CDTitle'); 
			CDTitle:stoptweening();
			CDTitle:addy(-590)
			CDTitle:decelerate(0.1)
			--CDTitle:addy(-400)
			--CDTitle:decelerate(0.1)
			--CDTitle:addy(0);
			--SCREENMAN:SystemMessage("StartSelectingSongMessageCommand");
		end;
		--[[GoBackSelectingSongMessageCommand=function(self)
			local Wheel = SCREENMAN:GetTopScreen():GetChild('MusicWheel'); 
			--cmd(stoptweening;y,200;decelerate,.1;y,-10;decelerate,.1;y,0);
			Wheel:stoptweening();
			Wheel:addy(200)
			Wheel:decelerate(0.1)
			Wheel:addy(-10)
			Wheel:decelerate(0.1)
			Wheel:addy(0);
			SCREENMAN:SystemMessage("GoBackSelectingSongMessageCommand");
		end;]]
		--OffCommand=cmd(stoptweening;decelerate,.1;y,-15;decelerate,.1;y,200);
	};

	LoadActor("_groupwheel")..{
		OnCommand=cmd(stoptweening;y,(SCREEN_HEIGHT/2);x,(SCREEN_WIDTH/2));
		GoBackSelectingGroupMessageCommand=cmd(x,-20;sleep,.1;linear,.2;x,(SCREEN_WIDTH/2));
		StartSelectingSongMessageCommand=cmd(stoptweening;x,(SCREEN_WIDTH/2);linear,.1;x,-150);
	};

	LoadActor("_CM/default.lua");	

	Def.ActorFrame {

		GoBackSelectingGroupMessageCommand=function(self) 
			--cmd(stoptweening;y,0;accelerate,.1;y,-10;decelerate,.1;y,200);
			--local self = SCREENMAN:GetTopScreen():GetChild('Musicself'); 
			self:stoptweening();
			self:addy(0);
			self:accelerate(0.1);
			self:addy(-10);
			self:decelerate(0.1);
			self:addy(600);
			--self:linear(0);
			--self:visible(0);
			--SCREENMAN:SystemMessage("GoBackSelectingGroupMessageCommand");
		end;
		StartSelectingSongMessageCommand=function(self)		
			--local self = SCREENMAN:GetTopScreen():GetChild('Musicself');
			self:stoptweening();
			self:decelerate(0.1);
			self:addy(-600);			
			self:addy(10);
			self:decelerate(0.1);
			self:addy(0);
			--self:linear(0);
			--self:visible(1);
			--SCREENMAN:SystemMessage("StartSelectingSongMessageCommand");
		end;
	

		LoadActor( "../ScreenSelectMusic underlay/Channel/_channel3" ) .. {};
		LoadActor( "../ScreenSelectMusic underlay/Channel/_channel2" ) .. {};
		LoadActor("../ScreenSelectMusicUtils/Additionals") .. {};

	};
		LoadActor( "../ScreenSelectMusic overlay/efect info top" ) .. {
			OnCommand = cmd(x,SCREEN_CENTER_X-2;y,-143;zoom,2;linear,0.5;y,78;zoom,1);
			OffCommand=cmd(sleep,.5;linear,0.5;addy,-130;zoom,2);
		};

		LoadActor( "../ScreenSelectMusic overlay/efect info bottom" ) .. {
			OnCommand = cmd(x,SCREEN_CENTER_X-2;y,647;zoom,2;linear,0.5;y,418;zoom,1;diffusealpha,.6);
			OffCommand=cmd(sleep,.5;linear,0.5;addy,200;zoom,2);
		};

	Def.ActorFrame {

		GoBackSelectingGroupMessageCommand=function(self) 
			--cmd(stoptweening;y,0;accelerate,.1;y,-10;decelerate,.1;y,200);
			--local self = SCREENMAN:GetTopScreen():GetChild('Musicself'); 
			self:stoptweening();
			self:addy(0);
			self:accelerate(0.1);
			self:addy(-10);
			self:decelerate(0.1);
			self:addy(600);
			--self:linear(0);
			--self:visible(0);
			--SCREENMAN:SystemMessage("GoBackSelectingGroupMessageCommand");
		end;
		StartSelectingSongMessageCommand=function(self)		
			--local self = SCREENMAN:GetTopScreen():GetChild('Musicself');
			self:stoptweening();
			self:decelerate(0.1);
			self:addy(-600);			
			self:addy(10);
			self:decelerate(0.1);
			self:addy(0);
			--self:linear(0);
			--self:visible(1);
			--SCREENMAN:SystemMessage("StartSelectingSongMessageCommand");
		end;

		LoadActor("brillo")..{
			InitCommand=cmd(y,375;x,SCREEN_CENTER_X-83;blend,"BlendMode_Add";diffusealpha,0;zoom,1.1);
			OnCommand=cmd(blend,"BlendMode_Add";sleep,0.5;diffusealpha,.8;sleep,.15;linear,.77;x,SCREEN_CENTER_X+88.5;sleep,.2;linear,.3;diffusealpha,0);
		};


		LoadActor("../ScreenSelectMusicUtils/BannerFrame") .. {};
		LoadActor("../ScreenSelectMusicUtils/Press" ) .. {};
			
		Def.ActorFrame {
			Condition=GAMESTATE:IsHumanPlayer(PLAYER_1);
			InitCommand=function(self)
				local players = GAMESTATE:GetNumSidesJoined()
					self:x(players == 1 and SCREEN_LEFT+180 or SCREEN_LEFT+153);
					self:y(SCREEN_BOTTOM-37);
					self:diffusealpha(0);
				end;
			CurrentStepsP1ChangedMessageCommand=cmd(playcommand,"Update");
			OffCommand=cmd(stoptweening;hidden,1);

		LoadActor( "../ScreenSelectMusicSpecial overlay/brillo" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.55;diffusealpha,1;sleep,.4;linear,.3;diffusealpha,0;zoom,.8;zoomx,1);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.6;zoom,.55;diffusealpha,1;linear,.4;linear,.3;diffusealpha,0;zoom,.8;zoomx,1);
			};

		LoadActor( "../ScreenEvaluationStage background/O" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.05;diffusealpha,.065;linear,.35;zoom,.15;linear,.1;diffusealpha,0;zoomx,.2);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.64;zoom,.05;diffusealpha,.065;linear,.35;zoom,.15;linear,.1;diffusealpha,0;zoomx,.2);
			};

		LoadActor( "../ScreenEvaluationStage background/OrbeGlow" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.15;diffusealpha,.025;linear,.35;zoom,.5;linear,.1;diffusealpha,0);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.6;zoom,.15;diffusealpha,.025;linear,.35;zoom,.5;linear,.1;diffusealpha,0);
			};
		LoadActor( "../ScreenEvaluationStage background/O" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.25;diffusealpha,.025;linear,.35;zoom,.45;linear,.1;diffusealpha,0);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.6;zoom,.25;diffusealpha,.025;linear,.35;zoom,.45;linear,.1;diffusealpha,0);
			};
		};


		Def.ActorFrame {
			Condition=GAMESTATE:IsHumanPlayer(PLAYER_2);
			InitCommand=function(self)
				local players = GAMESTATE:GetNumSidesJoined()
					self:x(players == 1 and SCREEN_LEFT+180 or SCREEN_RIGHT-153);
					self:y(SCREEN_BOTTOM-37);
					self:diffusealpha(0);
				end;
			CurrentStepsP2ChangedMessageCommand=cmd(playcommand,"Update");
			OffCommand=cmd(stoptweening;hidden,1);

		LoadActor( "../ScreenSelectMusicSpecial overlay/brillo" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.55;diffusealpha,1;sleep,.4;linear,.3;diffusealpha,0;zoom,.8;zoomx,1);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.6;zoom,.55;diffusealpha,1;linear,.4;linear,.3;diffusealpha,0;zoom,.8;zoomx,1);
			};

		LoadActor( "../ScreenEvaluationStage background/O" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.05;diffusealpha,.065;linear,.35;zoom,.15;linear,.1;diffusealpha,0;zoomx,.2);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.64;zoom,.05;diffusealpha,.065;linear,.35;zoom,.15;linear,.1;diffusealpha,0;zoomx,.2);
			};

		LoadActor( "../ScreenEvaluationStage background/OrbeGlow" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.15;diffusealpha,.025;linear,.35;zoom,.5;linear,.1;diffusealpha,0);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.6;zoom,.15;diffusealpha,.025;linear,.35;zoom,.5;linear,.1;diffusealpha,0);
			};
		LoadActor( "../ScreenEvaluationStage background/O" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.25;diffusealpha,.025;linear,.35;zoom,.45;linear,.1;diffusealpha,0);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.6;zoom,.25;diffusealpha,.025;linear,.35;zoom,.45;linear,.1;diffusealpha,0);
			};
		};

		LoadActor("Lock/Unlock") .. {};

		LoadActor("../ScreenSelectMusicUtils/PlayerJoin_Effect" ) .. {--los commands estan en su archivo, no aqui
			Condition=GAMESTATE:GetNumPlayersEnabled() == 1;
			OffCommand=cmd(finishtweening;diffusealpha,0);
		};

		
		Def.Quad {
			InitCommand=cmd(stretchto,SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM;diffuse,color("#000000");draworder,99999);
			OnCommand=cmd(diffusealpha,0);
			OffCommand=cmd(sleep,0.7;linear,0.3;diffusealpha,1);
		};

	};
};

cx = (SCREEN_WIDTH/2);
cy = (SCREEN_HEIGHT/2);

-- Columna de Códigos P1
if GAMESTATE:IsSideJoined(PLAYER_1) then
t[#t+1] = GetCodesColumn( PLAYER_1 )..{
	InitCommand=cmd(x,24);
	OffCommand=cmd(visible,false);
};
end;

-- Columna de Códigos P2
if GAMESTATE:IsSideJoined(PLAYER_2) then
t[#t+1] = GetCodesColumn( PLAYER_2 )..{
	InitCommand=cmd(x,SCREEN_RIGHT-24);
	OffCommand=cmd(visible,false);
};
end;


if GAMESTATE:IsSideJoined(PLAYER_1) then
t[#t+1] = GetCommandWindow( PLAYER_1 )..{
	InitCommand=cmd(x,-100;y,cy-20;zoom,.5);
	OptionsListOpenedMessageCommand=function(self,params)
		if params.Player == PLAYER_1 then
			(cmd(finishtweening;x,-100;decelerate,.1;x,110;decelerate,.2;x,100))(self);
		end;
	end;
	OptionsListClosedMessageCommand=function(self,params)
		if params.Player == PLAYER_1 then
			(cmd(stoptweening;x,100;decelerate,.1;x,110;decelerate,.1;x,-100))(self);
		end;
	end;
	CodeMessageCommand=function(self,params)
	if GAMESTATE:IsBasicMode() then return; end;
	if  params.Name == 'OptionList' and params.PlayerNumber == PLAYER_1 then
		SCREENMAN:GetTopScreen():GetMusicWheel():Move(0);
		SCREENMAN:GetTopScreen():OpenOptionsList(PLAYER_1);
	end;
	end;
}
end;

if GAMESTATE:IsSideJoined(PLAYER_2) then
t[#t+1] = GetCommandWindow( PLAYER_2 )..{
	InitCommand=cmd(x,SCREEN_RIGHT+100;y,cy-20;zoom,.5);
	OptionsListOpenedMessageCommand=function(self,params)
		if params.Player == PLAYER_2 then
			(cmd(finishtweening;x,SCREEN_RIGHT+100;decelerate,.1;x,SCREEN_RIGHT-110;decelerate,.2;x,SCREEN_RIGHT-100))(self);
		end;
	end;
	OptionsListClosedMessageCommand=function(self,params)
		if params.Player == PLAYER_2 then
			(cmd(stoptweening;x,SCREEN_RIGHT-100;decelerate,.1;x,SCREEN_RIGHT-110;decelerate,.1;x,SCREEN_RIGHT+100))(self);
		end;
	end;
	CodeMessageCommand=function(self,params)
	if GAMESTATE:IsBasicMode() then return; end;
	if  params.Name == 'OptionList' and params.PlayerNumber == PLAYER_2 then
		SCREENMAN:GetTopScreen():GetMusicWheel():Move(0);
		SCREENMAN:GetTopScreen():OpenOptionsList(PLAYER_2);
	end;
	end;
}
end;

return t;

