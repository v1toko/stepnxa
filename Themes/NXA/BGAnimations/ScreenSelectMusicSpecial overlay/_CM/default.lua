--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Options = Speed, Display, etc.
-- Codes = 3x, V, M, Reset, etc.
local current_option = { Player1 = 1 , Player2 = 1 };
local option_names = { "Speed", "Display", "Noteskin", "Path", "Alternate", "Rush", "System" };
local current_option_name = "";
local isSelectingOption = { Player1 = true , Player2 = true };
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function IsSelectingOption( pn )
	if pn == PLAYER_1 then return isSelectingOption.Player1; end;
	if pn == PLAYER_2 then return isSelectingOption.Player2; end;
end;

function SetSelectingOption( pn, bol )
	if pn == PLAYER_1 then isSelectingOption.Player1 = bol; end;
	if pn == PLAYER_2 then isSelectingOption.Player2 = bol; end;
end;

function SetCurrentOption( pn , p)
	if pn == PLAYER_1 then current_option.Player1 = p; end;
	if pn == PLAYER_2 then current_option.Player2 = p; end;
end;

function GetOptionsNames()
	return option_names;
end;	

function GetCurrentOption( pn )
	if pn == PLAYER_1 then return current_option.Player1; end;
	if pn == PLAYER_2 then return current_option.Player2; end;
end;

function GetCurrentOptionName( pn )
	if pn == PLAYER_1 then return option_names[current_option.Player1]; end;
	if pn == PLAYER_2 then return option_names[current_option.Player2]; end;
end;

function GetCommandWindow( CUR_PLAYER )
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--[[  * Nuevos mensajes * params.Player
		OptionsListNext
		OptionsListPrev
		OptionsListBack
		OptionsListCenter
	]]--
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	local t = Def.ActorFrame {};

	t[#t+1] = Def.ActorFrame {
		OptionsListNextMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			SOUND:PlayOnce(THEME:GetPathS('','Command Windows/next'));
		end;
		OptionsListPrevMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			SOUND:PlayOnce(THEME:GetPathS('','Command Windows/next'));
		end;
		OptionsListCenterMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			if( IsSelectingOption(CUR_PLAYER) ) then 
				SetSelectingOption(CUR_PLAYER,false);
				MESSAGEMAN:Broadcast("OptionSelected",{ Player = CUR_PLAYER });
				SOUND:PlayOnce(THEME:GetPathS('','Command Windows/preselect'));
			else
				MESSAGEMAN:Broadcast("CodeSeleted",{ Player = CUR_PLAYER });
				SOUND:PlayOnce(THEME:GetPathS('','Command Windows/select'));
			end;
		end;
		OptionsListBackMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			if not( IsSelectingOption(CUR_PLAYER) ) then
				SetSelectingOption(CUR_PLAYER,true);
				MESSAGEMAN:Broadcast("GoBackSelectingOption",{ Player = CUR_PLAYER });
				SOUND:PlayOnce(THEME:GetPathS('','Command Windows/preselect'));
			else
				MESSAGEMAN:Broadcast("ExitOptionsList",{ Player = CUR_PLAYER });
				SOUND:PlayOnce(THEME:GetPathS('','Command Windows/exit'));
			end;
		end;
	};
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	-- Base
	t[#t+1] = LoadActor("_basic/base");

	t[#t+1] = LoadActor("_basic/base")..{
		InitCommand=cmd(stoptweening;blend,'BlendMode_Add';diffusealpha,0);
		OptionSelectedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;diffusealpha,.7;zoom,1;decelerate,.5;zoom,1.08;diffusealpha,0))(self);
		end;
		CodeSeletedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;diffusealpha,.7;zoom,1;decelerate,.5;zoom,1.08;diffusealpha,0))(self);
		end;
	}
	
	-- Lista de Comandos
	t[#t+1] = LoadActor("_commands");
	
	t[#t+1] = GetCommands( CUR_PLAYER )..{
		OptionSelectedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;y,0;decelerate,.05;y,60))(self);
		end;
		GoBackSelectingOptionMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;y,60;sleep,.2;linear,.05;y,0))(self);
		end;
	}
	
	-- Glow
	t[#t+1] = LoadActor("_basic/glow")..{
		InitCommand=cmd(stoptweening;blend,'BlendMode_Add';zoomx,0;y,-40;diffusealpha,.6);
		OptionSelectedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;sleep,.05;linear,.1;zoomx,1))(self);
		end;
		GoBackSelectingOptionMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;sleep,.15;zoomx,1;linear,.05;zoomx,0))(self);
		end;
	}

	-- Lista de Codigos
	t[#t+1] = LoadActor("objeto_base");
	
	
	t[#t+1] = GetCodes( CUR_PLAYER )..{
		InitCommand=cmd(y,-40);
	}
	
	-- Blue Arrows
	t[#t+1] = LoadActor("_basic/blue_arrows")..{
		InitCommand=cmd(stoptweening;blend,'BlendMode_Add');
		OptionsListOpenedMessageCommand=function(self,params)
			if (params.Player == CUR_PLAYER) then 
				(cmd(stoptweening;queuecommand,'Loop'))(self);
			end;
		end;
		LoopCommand=cmd(stoptweening;zoom,1;decelerate,.14;zoom,1.04;decelerate,.14;zoom,1;sleep,.06;queuecommand,'Loop');
		OptionsListNextMessageCommand=function(self,params)
			if params.Player == CUR_PLAYER and IsSelectingOption(CUR_PLAYER) then
				(cmd(finishtweening;diffusealpha,1;zoom,1;decelerate,.18;zoom,1.35;diffusealpha,.7;linear,.1;zoom,.9;diffusealpha,1;decelerate,.14;zoom,1,.04;decelerate,.14;zoom,1;queuecommand,'Loop'))(self);
			end;
		end;
		OptionsListPrevMessageCommand=function(self,params)
			if params.Player == CUR_PLAYER and IsSelectingOption(CUR_PLAYER) then
				(cmd(finishtweening;diffusealpha,1;zoom,1;decelerate,.18;zoom,1.35;diffusealpha,.7;linear,.1;zoom,.9;diffusealpha,1;decelerate,.14;zoom,1,.04;decelerate,.14;zoom,1;queuecommand,'Loop'))(self);
			end;
		end;
		OptionSelectedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;diffusealpha,1;decelerate,.05;zoom,.9;decelerate,.05;zoom,1.4;diffusealpha,0))(self);
		end;
		GoBackSelectingOptionMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;diffusealpha,0;zoom,1.4;sleep,.2;decelerate,.05;zoom,1;diffusealpha,1;queuecommand,'Loop'))(self);
		end;
	}
	
	-- Gold Frame
	t[#t+1] = LoadActor("_basic/gold_frame")..{
		InitCommand=cmd(stoptweening;blend,'BlendMode_Add';y,-40;zoom,.7;diffusealpha,0);
		CodeSeletedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;zoom,.7;decelerate,.1;zoom,.6;decelerate,.4;zoom,.7))(self);
		end;
		OptionSelectedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;zoom,1;diffusealpha,0;sleep,.2;linear,.05;diffusealpha,1;decelerate,.05;zoom,.7;queuecommand,'Loop'))(self);
		end;
		OptionsListNextMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			if IsSelectingOption(CUR_PLAYER) then self:diffusealpha(0); return; end;
			(cmd(finishtweening;sleep,.18;zoom,.7;diffusealpha,1;linear,.1;zoom,.65;decelerate,.12;zoom,.7;queuecommand,'Loop'))(self);
		end;
		OptionsListPrevMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			if IsSelectingOption(CUR_PLAYER) then self:diffusealpha(0); return; end;
			(cmd(finishtweening;sleep,.18;zoom,.7;diffusealpha,1;linear,.1;zoom,.65;decelerate,.12;zoom,.7;queuecommand,'Loop'))(self);
		end;
		GoBackSelectingOptionMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;diffusealpha,0))(self);
		end;
	}
	
	-- White Frame
	t[#t+1] = LoadActor("_basic/white_frame")..{
		InitCommand=cmd(stoptweening;blend,'BlendMode_Add';y,-40;zoom,.7;diffusealpha,0);
		CodeSeletedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;diffusealpha,0;zoom,.7;decelerate,.1;diffusealpha,.8;zoom,.6;decelerate,.4;zoom,.7;diffusealpha,0))(self);
		end;
	}
	
	-- Gold arrows
	t[#t+1] = LoadActor("_basic/gold_arrows")..{
		InitCommand=cmd(stoptweening;blend,'BlendMode_Add';y,-40;zoom,.7;diffusealpha,0);
		OptionSelectedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;zoom,1;diffusealpha,0;sleep,.2;linear,.05;diffusealpha,1;decelerate,.05;zoom,.7;queuecommand,'Loop'))(self);
		end;
		LoopCommand=cmd(stoptweening;zoom,.7;linear,.3;zoom,.9;linear,.3;zoom,.7;queuecommand,'Loop');
		CodeSeletedMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;sleep,.05;linear,.05;zoom,1.2;decelerate,.4;zoom,.7;queuecommand,'Loop'))(self);
		end;
		GoBackSelectingOptionMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER ) then return; end;
			(cmd(stoptweening;decelerate,.05;zoom,.7;sleep,.05;linear,.05;zoom,1.2;diffusealpha,0))(self);
		end;
		OptionsListNextMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			if IsSelectingOption(CUR_PLAYER) then self:diffusealpha(0); return; end;
			(cmd(finishtweening;zoom,.7;diffusealpha,1;linear,.1;zoom,.9;decelerate,.1;zoom,.7;queuecommand,'Loop'))(self);
		end;
		OptionsListPrevMessageCommand=function(self,params)
			if (params.Player ~= CUR_PLAYER) then return; end;
			if IsSelectingOption(CUR_PLAYER) then self:diffusealpha(0); return; end;
			(cmd(finishtweening;zoom,.7;diffusealpha,1;linear,.1;zoom,.9;decelerate,.1;zoom,.7;queuecommand,'Loop'))(self);
		end;
	}
	
	-- Arrows
	t[#t+1] = LoadActor("_arrows");			
	t[#t+1] = GetArrowsSteps( CUR_PLAYER );	
	
	return t;
end;

-- TODO FUNK OK :)