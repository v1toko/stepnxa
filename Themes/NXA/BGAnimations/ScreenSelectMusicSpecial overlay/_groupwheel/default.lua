local t = Def.ActorFrame {
	OnCommand=cmd(fov,60;zbuffer,true;vanishpoint,cx,cy);--SortByDrawOrder
}

--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Regresa la cantidad de grupos, para q sea mayor o igual a 7
function GetNumGroups()
	local temp = SONGMAN:GetNumSpecialSongGroups();
	if temp < 7 then
		while (temp < 7) do
			temp = temp + temp;
		end;
	end;
		
	return temp;
end;

-- Regresa un arreglo con todos los grupos, de tama�o GetNumGrops()
function GetAllGroups()
	local temp = SONGMAN:GetSpecialSongGroupNames();
	local lengh = #temp;
	--
	local vector = {};
	for i=1,lengh do
		vector[i]= temp[i];	--nombre
	end;
	
	--Falta arreglar!!!!!!!!!
	while #vector < 7 do
		local templengh = #vector;
		for i=1,(#temp) do
			vector[i+templengh] = temp[i];
		end;
	end;
	
	return vector;
end;

local NumGroups = GetNumGroups();
local AllGroups = GetAllGroups();

--SCREENMAN:SystemMessage(tostring(NumGroups));

function GetGroupInNumber(curGroup)
	--local curGroup = GAMESTATE:GetCurrentSong():GetGroupName();
	
	for j=1,NumGroups do
		if AllGroups[j]==curGroup then --regresa el primero que encuentre
		return j;
		end;
	end;
	
	return j;
	
end;

--Como minimo, el vector de AllGroups() debe tener 7 lugares
function GetBanners(curGroup)
	local toReturn = {};
	
	if curSong == 3 then
		toReturn = { NumGroups , 1 , 2 , 3 , 4 , 5 , 6 };
	elseif curGroup == 2 then
		toReturn = { NumGroups-1 , NumGroups , 1 , 2 , 3 , 4 , 5 };
	elseif curGroup == 1 then
		toReturn = { NumGroups-2 , NumGroups-1 , NumGroups , 1 , 2 , 3 , 4};
	elseif curGroup == NumGroups then
		toReturn = { NumGroups-3 , NumGroups-2 , NumGroups-1 , NumGroups , 1 , 2 , 3 };
	elseif curGroup == (NumGroups - 1) then
		toReturn = { NumGroups-4 , NumGroups-3 , NumGroups-2 , NumGroups-1 , NumGroups , 1 , 2 };
	elseif curGroup == (NumGroups - 2) then
		toReturn = { NumGroups-5 , NumGroups-4 , NumGroups-3 , NumGroups-2 , NumGroups-1 , NumGroups , 1 };
	else
		toReturn = { curGroup-3 , curGroup-2 , curGroup-1 , curGroup , curGroup+1, curGroup+2 , curGroup+3 };
	end;
	
	return toReturn;
end;
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--Posiciones iniciales
local InitPos = {
	(cmd(x,-360;rotationy,50;z,-150;zoom,.3;diffusealpha,0)),
	(cmd(x,-300;rotationy,50;z,-100;zoom,.5;diffusealpha,.4)),
	(cmd(x,-210;rotationy,50;z,-50;zoom,.8;diffusealpha,.6)),
	(cmd(x,0;z,0;zoom,1;rotationy,0;diffusealpha,1)),--center
	(cmd(x,210;rotationy,-50;z,-50;zoom,.8;diffusealpha,.6)),
	(cmd(x,300;rotationy,-50;z,-100;zoom,.5;diffusealpha,.4)),
	(cmd(x,360;rotationy,-50;z,-150;zoom,.3;diffusealpha,0)),
};

--Para el cambio del centro
local ChangeCenter = (cmd(x,0;z,0;zoom,1;rotationy,0;diffusealpha,1;sleep,.2;queuecommand,'Loop'));

--Posiciones de comienzo
local StartPos = {
	(cmd(diffusealpha,0;sleep,.2;linear,.1;diffusealpha,0)),
	(cmd(diffusealpha,0;sleep,.2;linear,.1;diffusealpha,.4)),
	(cmd(diffusealpha,0;sleep,.2;linear,.1;diffusealpha,.8)),
	(cmd(diffusealpha,0;sleep,.1;linear,.05;diffusealpha,1;sleep,.2;queuecommand,'Loop')),--center
	(cmd(diffusealpha,0;sleep,.2;linear,.1;diffusealpha,.8)),
	(cmd(diffusealpha,0;sleep,.2;linear,.1;diffusealpha,.4)),
	(cmd(diffusealpha,0;sleep,.2;linear,.1;diffusealpha,0)),
};

--Posiciones de salida
local OutPos = {
	(cmd(diffusealpha,.7;sleep,.05;linear,.24;diffusealpha,0)),
	(cmd(diffusealpha,1;sleep,.29;linear,.01;diffusealpha,0)),--center
};
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Pink glow
t[#t+1] = LoadActor("br")..{
	InitCommand=cmd(blend,'BlendMode_Add';diffusealpha,0);
	StartSelectingSongMessageCommand=cmd(stoptweening;diffusealpha,1;sleep,.25;linear,.05;diffusealpha,0);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;diffusealpha,0);
};

t[#t+1] = Def.ActorFrame {
	InitCommand=cmd(z,-30;zoom,0);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;zoom,0;sleep,.3;linear,.1;zoom,1.2;linear,.05;zoom,1.09);
	StartSelectingSongMessageCommand=cmd(stoptweening;zoom,0);
	ChangeGroupMessageCommand=cmd(stoptweening;accelerate,.1;zoom,0;sleep,.1;linear,.1;zoom,1.2;linear,.05;zoom,1.09);
	children = {
	LoadActor("sr")..{
		InitCommand=cmd(blend,'BlendMode_Add';x,150;y,-55;queuecommand,'Loop');
		LoopCommand=cmd(zoom,.26;linear,.5;zoom,.32;linear,.5;zoom,.26;queuecommand,'Loop');
	};

	LoadActor("sr")..{
		InitCommand=cmd(blend,'BlendMode_Add';x,115;y,-80;queuecommand,'Loop');
		LoopCommand=cmd(zoom,.9;linear,.6;zoom,1.12;linear,.6;zoom,.9;queuecommand,'Loop');
	};

	LoadActor("sr")..{
		InitCommand=cmd(blend,'BlendMode_Add';x,-135;y,65;queuecommand,'Loop');
		LoopCommand=cmd(zoom,.4;linear,.5;zoom,.5;linear,.5;zoom,.4;queuecommand,'Loop');
	};

	LoadActor("sr")..{
		InitCommand=cmd(blend,'BlendMode_Add';x,100;y,100;sleep,.2;queuecommand,'Loop');
		LoopCommand=cmd(zoom,1.35;linear,.6;zoom,1.5;linear,.6;zoom,1.35;queuecommand,'Loop');
	};

	LoadActor("sr")..{
		InitCommand=cmd(blend,'BlendMode_Add';x,-100;y,100;queuecommand,'Loop');
		LoopCommand=cmd(zoom,1.35;linear,.6;zoom,1.5;linear,.6;zoom,1.35;queuecommand,'Loop');
	};

	LoadActor("br")..{
		InitCommand=cmd(blend,'BlendMode_Add';queuecommand,'Loop');
		LoopCommand=cmd(zoom,1;linear,1.7;zoom,1.17;diffusealpha,0;sleep,.25;zoom,1;linear,.05;diffusealpha,.9;queuecommand,'Loop');
	};

	LoadActor("br")..{
		InitCommand=cmd(blend,'BlendMode_Add';queuecommand,'Loop');
		LoopCommand=cmd(zoom,1;linear,1;zoom,1.02;linear,1;zoom,1;queuecommand,'Loop');
	};
	};
}
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Funciones e Indice de grupo
local CurrentGroupNumber;
local Banners = {};

function ActualGroupNumber()
	return GetGroupInNumber( GAMESTATE:GetCurrentSong():GetGroupName() );
end;


-- Para cambiar el indice
t[#t+1] = Def.ActorFrame {
	InitCommand=function(self)
		local curGroup = GAMESTATE:GetCurrentSong():GetGroupName();
		CurrentGroupNumber = GetGroupInNumber(curGroup);
	end;
	GoBackSelectingGroupMessageCommand=function(self)
		local curGroup = GAMESTATE:GetCurrentSong():GetGroupName();
		CurrentGroupNumber = GetGroupInNumber(curGroup);
		Banners = GetBanners( CurrentGroupNumber );
		MESSAGEMAN:Broadcast("RefreshPositions");
		MESSAGEMAN:Broadcast("OpenPositions");
	end;
	ChangeGroupMessageCommand=function(self,params)
		SOUND:PlayOnce(THEME:GetPathS('','ButtonsSounds/change3.wav'));
		--SOUND:PlayOnce('Songs/NX2/sound.ogg');
		CurrentGroupNumber = CurrentGroupNumber + (params.Dir);	-- params.Dir = +- 1, dependiendo de la direccion
		if CurrentGroupNumber > NumGroups then CurrentGroupNumber = 1; end;
		if CurrentGroupNumber < 1 then CurrentGroupNumber = NumGroups; end;
		Banners = GetBanners( CurrentGroupNumber );
		MESSAGEMAN:Broadcast("Move");
		--SCREENMAN:SystemMessage(tostring(CurrentGroupNumber) );
	end;
	StartSelectingSongMessageCommand=function(self)
		MESSAGEMAN:Broadcast("MoveGroup", {Group = AllGroups[CurrentGroupNumber]});
	end
}

function PlayGroupSound()
	local fir = SONGMAN:GetSongGroupBannerPath( AllGroups[CurrentGroupNumber] );
	local ogg = string.gsub(fir,'banner.png','info/sound.ogg');
	local mp3 = string.gsub(fir,'banner.png','info/sound.mp3');
	local wav = string.gsub(fir,'banner.png','info/sound.wav');
	local file = "";
	if (FILEMAN:DoesFileExist(ogg)) then file = ogg;
	elseif (FILEMAN:DoesFileExist(mp3)) then file = mp3;
	elseif (FILEMAN:DoesFileExist(wav)) then file = wav;
	else return; end;
	
	SOUND:PlayOnce(file);
end;

t[#t+1] = Def.ActorFrame {
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;sleep,.5;queuecommand,'Play');
	MoveMessageCommand=cmd(stoptweening;sleep,.5;queuecommand,'Play');
	PlayCommand=function(self)
		PlayGroupSound()
	end;
}
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
-- Banners XD
for i=1,NumGroups do 
t[#t+1] = Def.Sprite {
	InitCommand = function(self)
		(cmd(stoptweening;diffusealpha,0;zbuffer,true))(self);
		local gname = AllGroups[i];
		self:scaletoclipped(250,270);
		if i then
			self:Load(SONGMAN:GetSongGroupBannerPath(gname));	-- se generan los actores
		end;
	end;
	RefreshPositionsMessageCommand = function(self)
		self:stoptweening(); 
		if 	    i==Banners[1] then (InitPos[1])(self);
		elseif 	i==Banners[2] then (InitPos[2])(self);
		elseif 	i==Banners[3] then (InitPos[3])(self);
		elseif 	i==Banners[4] then (InitPos[4])(self); --center
		elseif 	i==Banners[5] then (InitPos[5])(self);
		elseif 	i==Banners[6] then (InitPos[6])(self);
		elseif 	i==Banners[7] then (InitPos[7])(self);
		else (cmd(stoptweening;diffusealpha,0))(self);
		end;
		self:diffusealpha(1);
	end;
	MoveMessageCommand=function(self)
		self:stoptweening(); --detiene todas las animaciones
		if 	    i==Banners[1] then self:linear(.18);(InitPos[1])(self);
		elseif 	i==Banners[2] then self:linear(.18);(InitPos[2])(self);
		elseif 	i==Banners[3] then self:linear(.18);(InitPos[3])(self);
		elseif 	i==Banners[4] then self:linear(.18);(ChangeCenter)(self); --center
		elseif 	i==Banners[5] then self:linear(.18);(InitPos[5])(self);
		elseif 	i==Banners[6] then self:linear(.18);(InitPos[6])(self);
		elseif 	i==Banners[7] then self:linear(.18);(InitPos[7])(self);
		else (cmd(stoptweening;diffusealpha,0))(self);
		end;
	end;
	LoopCommand=cmd(stoptweening;zoom,1;decelerate,.6;zoom,1.03;decelerate,.6;zoom,1;queuecommand,'Loop');
	StartSelectingSongMessageCommand=function(self,params)
		self:stoptweening();	
		if 	    i==Banners[1] then (OutPos[1])(self);
		elseif 	i==Banners[2] then (OutPos[1])(self);
		elseif 	i==Banners[3] then (OutPos[1])(self);
		elseif 	i==Banners[4] then (OutPos[2])(self); --center
		elseif 	i==Banners[5] then (OutPos[1])(self);
		elseif 	i==Banners[6] then (OutPos[1])(self);
		elseif 	i==Banners[7] then (OutPos[1])(self);
		else (cmd(stoptweening;diffusealpha,0))(self);
		end;
	end;
	OpenPositionsMessageCommand=function(self)	--comienza la seleccion de grupos
		self:stoptweening();
		if 	    i==Banners[1] then (StartPos[1])(self);
		elseif 	i==Banners[2] then (StartPos[2])(self);
		elseif 	i==Banners[3] then (StartPos[3])(self);
		elseif 	i==Banners[4] then (StartPos[4])(self); --center
		elseif 	i==Banners[5] then (StartPos[5])(self);
		elseif 	i==Banners[6] then (StartPos[6])(self);
		elseif 	i==Banners[7] then (StartPos[7])(self);
		else (cmd(stoptweening;diffusealpha,0))(self);
		end;
	end;
}
end;


--[[t[#t+1] = LoadFont("_Category")..{
	InitCommand=function(self)
		self:zoom(.5);
		self:y(170);
		--self:settext( SONGMAN:GetSongGroupBannerPath(GAMESTATE:GetCurrentSong():GetGroupName()));
		self:settext('');
	end;
	MoveMessageCommand=function(self,params)--todo en screenoptionssimple
		--local dir = string.format("%8s",'Songs/'..tostring(params.Group)..'/info/sound.ogg' );
		--self:settext('Songs/'..params.Group..'/sound.ogg');
		--self:settext(params.Group..'  --   '..SONGMAN:GetSongGroupBannerPath(params.Group)..'dir:'..'Songs/'..tostring(params.Group)..'/info/sound.ogg');
		self:settext('');
		local fir = SONGMAN:GetSongGroupBannerPath( AllGroups[CurrentGroupNumber] );
		local dir = string.gsub(fir,'banner.png','info/text.ini');
		local tt = lua.ReadFile(dir);
		--self:settext(tt);
		(cmd(stoptweening;zoom,.5;shadowlength,0;wrapwidthpixels,600/0.75;cropright,1;sleep,.5;linear,0.7;cropright,0))(self);
		--SOUND:PlayOnce('Songs/'..(params.Group)..'/sound.ogg');
	end;
	--cmd(settext,NumGroups);
	GoBackSelectingGroupMessageCommand=cmd(playcommand,'Move');
	StartSelectingSongMessageCommand=cmd(stoptweening;settext,'');
	aInitCommand=function(self)
		local curGroup = GAMESTATE:GetCurrentSong():GetGroupName();
		self:settext(tostring(GetGroupInNumber(curGroup)));	--Siempre empieza en alguna cancion
	end;
	aChangeGroupMessageCommand=function(self,params)
		--local curGroup = GAMESTATE:GetCurrentSong():GetGroupName();
		self:settext(tostring(GetGroupInNumber(params.Group)));	--Siempre empieza en alguna cancion
	end;
	sChangeGroupMessageCommand=function(self,params)	--Solo si se habilita mostrar el preview, manda el comando
		self:stoptweening(); --detiene todas las animaciones
		--
		self:settext(CurrentGroupNumber);
	--	if params.Dir == +1 then CurrentGroupNumber = CurrentGroupNumber + 1; end; -- +-1, dependiendo de la direccion
	--	if params.Dir == -1 then CurrentGroupNumber = CurrentGroupNumber - 1; end; -- +-1, dependiendo de la direccion
	end;
}]]

--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
--der
t[#t+1] = LoadActor("arrow.png")..{
	InitCommand=cmd(x,190;zoom,0);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;zoom,0;sleep,.3;linear,.1;zoom,1;sleep,.05;queuecommand,'Loop');
	StartSelectingSongMessageCommand=cmd(stoptweening;zoom,0);
	NextGroupMessageCommand=cmd(stoptweening;linear,.05;zoom,1;x,190;decelerate,.2;x,210;zoomy,.68;decelerate,.1;zoom,1;x,190;queuecommand,'Loop');
	PrevGroupMessageCommand=cmd(stoptweening;linear,.05;zoom,1;x,190;sleep,.3;queuecommand,'Loop');
	LoopCommand=cmd(stoptweening;x,190;zoom,1;decelerate,.5;x,195;zoomy,.72;decelerate,.5;zoom,1;x,190;queuecommand,'Loop');
};

--izq
t[#t+1] = LoadActor("arrow.png")..{
	InitCommand=cmd(x,-190;rotationz,180;zoom,0);
	GoBackSelectingGroupMessageCommand=cmd(stoptweening;zoom,0;sleep,.3;linear,.1;zoom,1;sleep,.05;queuecommand,'Loop');
	StartSelectingSongMessageCommand=cmd(stoptweening;zoom,0);
	PrevGroupMessageCommand=cmd(stoptweening;linear,.05;zoom,1;x,-190;decelerate,.2;x,-210;zoomy,.68;decelerate,.1;zoom,1;x,-190;queuecommand,'Loop');
	NextGroupMessageCommand=cmd(stoptweening;linear,.05;zoom,1;x,-190;sleep,.3;queuecommand,'Loop');
	LoopCommand=cmd(stoptweening;x,-190;zoom,1;decelerate,.5;x,-195;zoomy,.72;decelerate,.5;zoom,1;x,-190;queuecommand,'Loop');
};


return t;