
local s = Def.ActorFrame {};

s[#s+1] = 	LoadActor( "GeneralMap" ) .. {
				OnCommand=cmd(visible,false;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
				SelectStateChangedMessageCommand=function(self,param)
					--modificar este nombre dependiendo del switch que requerimos
					local bSwitchAct = SCREENMAN:GetTopScreen():GetSwitchActive( "Switch 1" )
					
					if param.To == "GeneralMap" and not bSwitchAct then
						self:visible( true )
					else
						self:visible( false )
					end
				end;
			};
			
s[#s+1] = 	LoadActor( "GeneralMapSwitch" ) .. {
				OnCommand=cmd(visible,false;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
				SelectStateChangedMessageCommand=function(self,param)
					--modificar este nombre dependiendo del switch que requerimos
					local bSwitchAct = SCREENMAN:GetTopScreen():GetSwitchActive( "Switchearth" )
					
					if param.To == "GeneralMap" and bSwitchAct then
						self:visible( true )
					else
						self:visible( false )
					end
				end;
			};

return s