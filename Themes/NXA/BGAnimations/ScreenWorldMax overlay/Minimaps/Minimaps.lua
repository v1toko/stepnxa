
local s = Def.ActorFrame {};

local missions = SONGMAN:GetSectorNames()

for i = 1, #missions do
	local name = missions[i]
	name = string.lower( name )
	s[#s+1] = Def.ActorFrame {
		LoadActor( name ) .. {
			Name=name;
			SetCommand=function(self)
				local nameCurMap = SONGMAN:GetSectorNames()
				local index = SCREENMAN:GetTopScreen():GetCurrentMapIndex()
				local mapName = self:GetName()
				
				--Trace( "nameCurMap: " .. nameCurMap[index+1] .. " MapName: " .. mapName .. " index: " .. tostring(index+1))
				
				if string.lower(nameCurMap[index+1]) == mapName and not bSwitchAct then
					self:visible( true )
				else
					self:visible( false )
				end
			end;
			
			OnCommand = cmd(playcommand,"Set";x,528;y,369;diffusealpha,1;zoom,0.5;addx,500;sleep,0.3;accelerate,0.3;addx,-500);
			OffCommand=cmd(linear,0.3;addx,500);
			CurrentMissionChangedMessageCommand=cmd(playcommand,"Set");
			MapMovedMessageCommand=cmd(playcommand,"Set");
			SwitchStateChangedMessageCommand=cmd(playcommand,"Set");
		};
	};
end

--minimap con switch
s[#s+1] = Def.ActorFrame {
	LoadActor( "earthSwitch" ) .. {
		Name="earth";
		SetCommand=function(self)
			local nameCurMap = SONGMAN:GetSectorNames()
			local index = SCREENMAN:GetTopScreen():GetCurrentMapIndex()
			local mapName = self:GetName()
			
			--preguntamos si el switch "Switch 1" est� activo,
			--modificar dependiendo de lo que requeramos
			local bSwitchAct = SCREENMAN:GetTopScreen():GetSwitchActive( "Switchearth" )
			
			--Trace( "nameCurMap: " .. nameCurMap[index+1] .. " MapName: " .. mapName .. " index: " .. tostring(index+1))
			
			if string.lower(nameCurMap[index+1]) == mapName and bSwitchAct then
				self:visible( true )
			else
				self:visible( false )
			end
		end;
		
		OnCommand = cmd(playcommand,"Set";x,528;y,369;diffusealpha,1;zoom,0.5;addx,500;sleep,0.3;accelerate,0.3;addx,-500);
		OffCommand=cmd(linear,0.3;addx,500);
		CurrentMissionChangedMessageCommand=cmd(playcommand,"Set");
		MapMovedMessageCommand=cmd(playcommand,"Set");
		SwitchStateChangedMessageCommand=cmd(playcommand,"Set");
	};
};
			
return s