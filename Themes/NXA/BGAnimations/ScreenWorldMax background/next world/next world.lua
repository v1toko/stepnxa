

return Def.ActorFrame { 	
	
	LoadActor( "01" ) .. {
		OnCommand=cmd(x,0;y,0);
	};

	LoadActor( "02" ) .. {
		OnCommand=cmd(x,1114;y,0);
	};

	LoadActor( "03" ) .. {
		OnCommand=cmd(x,2050;y,0);
	};
	
	LoadActor( "04" ) .. {
		OnCommand=cmd(x,3164;y,0);
	};

	LoadActor( "05" ) .. {
		OnCommand=cmd(x,0;y,900);
	};

	LoadActor( "06" ) .. {
		OnCommand=cmd(x,1114;y,900);
	};

	LoadActor( "07" ) .. {
		OnCommand=cmd(x,2051;y,900);
	};
	
	LoadActor( "08" ) .. {
		OnCommand=cmd(x,3164;y,900);
	};
	
	LoadActor( "09" ) .. {
		OnCommand=cmd(x,0;y,1630);
	};

	LoadActor( "10" ) .. {
		OnCommand=cmd(x,1114;y,1630);
	};

	LoadActor( "11" ) .. {
		OnCommand=cmd(x,2051;y,1630);
	};
	
	LoadActor( "12" ) .. {
		OnCommand=cmd(x,3164;y,1630);
	};

	LoadActor( "13" ) .. {
		OnCommand=cmd(x,0;y,2522);
	};

	LoadActor( "14" ) .. {
		OnCommand=cmd(x,1114;y,2522);
	};

	LoadActor( "15" ) .. {
		OnCommand=cmd(x,2051;y,2522);
	};
	
	LoadActor( "16" ) .. {
		OnCommand=cmd(x,3164;y,2522);
	};


	LoadActor( "11_Switch" ) .. {
		OnCommand=cmd(x,2051;y,1630);
		SwitchStateChangedMessageCommand=cmd(playcommand,"Set");
		SetCommand=function(self)
			--preguntamos si el switch "Switch" est� activo,
			--modificar dependiendo de lo que requeramos
			local bSwitchAct = SCREENMAN:GetTopScreen():GetSwitchActive( "Switch" )
			
			if bSwitchAct then
				self:visible( true )
			else
				self:visible( false )
			end
		end;
	};

	LoadActor( "12_Switch" ) .. {
		OnCommand=cmd(x,3164;y,1630);
		SwitchStateChangedMessageCommand=cmd(playcommand,"Set");
		SetCommand=function(self)
			--preguntamos si el switch "Switch" est� activo,
			--modificar dependiendo de lo que requeramos
			local bSwitchAct = SCREENMAN:GetTopScreen():GetSwitchActive( "Switch" )
			
			if bSwitchAct then
				self:visible( true )
			else
				self:visible( false )
			end
		end;
	};

	LoadActor( "15_Switch" ) .. {
		OnCommand=cmd(x,2051;y,2522);
		SwitchStateChangedMessageCommand=cmd(playcommand,"Set");
		SetCommand=function(self)
			--preguntamos si el switch "Switch" est� activo,
			--modificar dependiendo de lo que requeramos
			local bSwitchAct = SCREENMAN:GetTopScreen():GetSwitchActive( "Switch" )
			
			if bSwitchAct then
				self:visible( true )
			else
				self:visible( false )
			end
		end;
	};

	LoadActor( "16_Switch" ) .. {
		OnCommand=cmd(x,3164;y,2522);
		SwitchStateChangedMessageCommand=cmd(playcommand,"Set");
		SetCommand=function(self)
			--preguntamos si el switch "Switch" est� activo,
			--modificar dependiendo de lo que requeramos
			local bSwitchAct = SCREENMAN:GetTopScreen():GetSwitchActive( "Switch" )
			
			if bSwitchAct then
				self:visible( true )
			else
				self:visible( false )
			end
		end;
	};
	
	MapMovedMessageCommand=function(self,param)
	
		if param.PosX == 0 or param.PosY == 0 then return end
		
		--HACK: sabemos como actuar
		--dependiendo de donde salga el mensaje
		--ajustamos las posicion inicial de mapa
		if param.From ~= "ScreenWorldMax" then		
			self:x( param.PosX-1410 )
			self:y( param.PosY-920 )
		else
			self:stoptweening( 0 )
			self:linear( 0.3 )
			self:x( param.PosX-1410 )
			self:y( param.PosY-920 )
		end
		
		--SCREENMAN:SystemMessage( param.From .. " POSX:" .. tostring( param.PosX ) .. " POSY:" .. tostring( param.PosY ) )
	end;
	
	CurrentMissionChangedMessageCommand=cmd(playcommand,"Set");
	OnCommand=cmd(playcommand,"Set");
	
	SetCommand=function(self)		
		local sectorname = SCREENMAN:GetTopScreen():GetCurrentSector()
		
		--SCREENMAN:SystemMessage( "SectorName: " .. sectorname )
		
		if string.lower(sectorname) ~= "next world" then 
			self:visible( false ) 
		else 
			self:visible( true ) 
		end	
	end;
	
};
