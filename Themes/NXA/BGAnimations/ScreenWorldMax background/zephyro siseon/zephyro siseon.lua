return Def.ActorFrame { 	
	
	LoadActor( "zephyro siseon" ) .. {
		OnCommand=cmd();
	};
	
	MapMovedMessageCommand=function(self,param)
	
		if param.PosX == 0 or param.PosY == 0 then return end
		
		--HACK: sabemos como actuar
		--dependiendo de donde salga el mensaje
		if param.From ~= "ScreenWorldMax" then		
			self:x( param.PosX+350 )
			self:y( param.PosY+400 )
		else
			self:stoptweening( 0 )
			self:linear( 0.3 )
			self:x( param.PosX+350 )
			self:y( param.PosY+400 )
		end
		
		--SCREENMAN:SystemMessage( param.From .. " POSX:" .. tostring( param.PosX ) .. " POSY:" .. tostring( param.PosY ) )
	end;
	
	CurrentMissionChangedMessageCommand=cmd(playcommand,"Set");
	OnCommand=cmd(playcommand,"Set");
	
	SetCommand=function(self)		
		local sectorname = SCREENMAN:GetTopScreen():GetCurrentSector()
		
		if string.lower(sectorname) ~= "zephyro siseon" then 
			self:visible( false ) 
		else 
			self:visible( true ) 
		end	
	end;
	
};