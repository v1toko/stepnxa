
local s = Def.ActorFrame {
	MapMovedMessageCommand=function(self,param)
	
		if param.PosX == 0 or param.PosY == 0 then return end
		
		--HACK: sabemos como actuar
		--dependiendo de donde salga el mensaje
		if param.From ~= "ScreenWorldMax" then		
			self:x( param.PosX-1410 )
			self:y( param.PosY-920 )
		else
			self:stoptweening( 0 )
			self:linear( 0.3 )
			self:x( param.PosX-1410 )
			self:y( param.PosY-920 )
		end
		
		--SCREENMAN:SystemMessage( param.From .. " POSX:" .. tostring( param.PosX ) .. " POSY:" .. tostring( param.PosY ) )
	end;
	
	CurrentMissionChangedMessageCommand=cmd(playcommand,"Set");
	OnCommand=cmd(playcommand,"Set");
	
	SetCommand=function(self)		
		local sectorname = SCREENMAN:GetTopScreen():GetCurrentSector()
		
		--SCREENMAN:SystemMessage( "SectorName: " .. sectorname )
		
		if string.lower(sectorname) ~= "vortexion" then 
			self:visible( false ) 
		else 
			self:visible( true ) 
		end	
	end;
};

s[#s+1] = 	LoadActor( "1" ) .. {
				OnCommand=cmd();
			};
			
s[#s+1] = 	LoadActor( "2" ) .. {
				OnCommand=cmd(addx,1094);
			};
			
s[#s+1] = 	LoadActor( "3" ) .. {
				OnCommand=cmd(addx,1094*2);
			};		

s[#s+1] = 	LoadActor( "4" ) .. {
				OnCommand=cmd(addy,869);
			};					

s[#s+1] = 	LoadActor( "5" ) .. {
				OnCommand=cmd(addx,1094;addy,869);
			};								
			
s[#s+1] = 	LoadActor( "6" ) .. {
				OnCommand=cmd(addx,1094*2;addy,869);
			};								
			
s[#s+1] = 	LoadActor( "7" ) .. {
				OnCommand=cmd(addy,869*2);
			};		

s[#s+1] = 	LoadActor( "8" ) .. {
				OnCommand=cmd(addx,1094;addy,869*2);
			};		

s[#s+1] = 	LoadActor( "9" ) .. {
				OnCommand=cmd(addx,1094*2;addy,869*2);
			};				

return s