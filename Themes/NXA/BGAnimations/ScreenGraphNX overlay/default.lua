local t = Def.ActorFrame {

	Def.Quad {
		InitCommand=cmd(stretchto,SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM;diffuse,color("#000000"));
		OnCommand=cmd(diffusealpha,1;sleep,.5;linear,0.5;diffusealpha,0);
	};


	LoadActor("light") .. {
		InitCommand=cmd(x,15;blend,"BlendMode_Add";vertalign,'VertAlign_Top';zoomy,.05;zoomx,1.1);
		OnCommand=cmd(sleep,.3;linear,.5;zoomx,.85;zoomy,2.5;sleep,.1;linear,.3;diffusealpha,0);
	};

	LoadActor("light") .. {
		InitCommand=cmd(x,SCREEN_RIGHT-15;blend,"BlendMode_Add";vertalign,'VertAlign_Top';zoomy,.05);
		OnCommand=cmd(sleep,.3;linear,.5;zoomx,.85;zoomy,2.5;sleep,.1;linear,.3;diffusealpha,0);
	};

	LoadActor("LEFT") .. {};

	LoadActor("LEFT") .. {InitCommand=cmd(rotationy,180;x,SCREEN_RIGHT-20);};


};

return t;

