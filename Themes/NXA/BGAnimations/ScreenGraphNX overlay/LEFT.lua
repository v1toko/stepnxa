local t = Def.ActorFrame {


	LoadActor("light") .. {
		InitCommand=cmd(y,20;addx,15;blend,"BlendMode_Add";horizalign,'HorizAlign_Left';zoomy,.05;rotationz,-90);
		OnCommand=cmd(sleep,.3;linear,.5;zoomx,.85;zoomy,2.45;sleep,.1;linear,.3;diffusealpha,0);
	};
	LoadActor("light") .. {
		InitCommand=cmd(y,55;addx,15;blend,"BlendMode_Add";horizalign,'HorizAlign_Left';zoomy,.05;rotationz,-90);
		OnCommand=cmd(sleep,.3;linear,.5;zoomx,.85;zoomy,2.45;sleep,.1;linear,.3;diffusealpha,0);
	};
	LoadActor("light") .. {
		InitCommand=cmd(y,65;addx,15;blend,"BlendMode_Add";horizalign,'HorizAlign_Left';zoomy,.05;rotationz,-90);
		OnCommand=cmd(sleep,.3;linear,.5;zoomx,.85;zoomy,2.45;sleep,.1;linear,.3;diffusealpha,0);
	};
	LoadActor("light") .. {
		InitCommand=cmd(y,390;addx,15;blend,"BlendMode_Add";horizalign,'HorizAlign_Left';zoomy,.05;rotationz,-90);
		OnCommand=cmd(sleep,.3;linear,.5;zoomx,.85;zoomy,2.45;sleep,.1;linear,.3;diffusealpha,0);
	};
	LoadActor("light") .. {
		InitCommand=cmd(y,400;addx,15;blend,"BlendMode_Add";horizalign,'HorizAlign_Left';zoomy,.05;rotationz,-90);
		OnCommand=cmd(sleep,.3;linear,.5;zoomx,.85;zoomy,2.45;sleep,.1;linear,.3;diffusealpha,0);
	};
	LoadActor("light") .. {
		InitCommand=cmd(y,487;addx,15;blend,"BlendMode_Add";horizalign,'HorizAlign_Left';zoomy,.05;rotationz,-90);
		OnCommand=cmd(sleep,.3;linear,.5;zoomx,.85;zoomy,2.45;sleep,.1;linear,.3;diffusealpha,0);
	};

};

return t;

