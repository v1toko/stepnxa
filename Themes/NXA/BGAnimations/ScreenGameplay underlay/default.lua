local function customlifemeter(pn)
local t = Def.ActorFrame {
	Def.Quad {
		BeginCommand=cmd(addx,25;scaletoclipped,255,25;player,pn;playcommand,"bGlow");
		bGlowCommand=function(self)
			local life = SCREENMAN:GetTopScreen():GetLifeMeter(pn):GetLife();
			local dgthreshold = 0.2
			self:glow(0,0,0.3,1);
			if life <= dgthreshold then
				self:glowshift();
				self:effectperiod(0.1);
				self:effectcolor1(1,0.25,0.25,1);
				self:effectcolor2(0,0,0,1);
			else
				self:stopeffect()
			end;
		end;
		LifeMeterChangedP1MessageCommand=cmd(playcommand,"bGlow");
		LifeMeterChangedP2MessageCommand=cmd(playcommand,"bGlow");
	};
	
	Def.Quad {
		BeginCommand=cmd(scaletoclipped,1,3;player,pn;blend,'BlendMode_NoEffect';zwrite,true;clearzbuffer,true;playcommand,"Start");
		StartCommand=function(self)
			local life = SCREENMAN:GetTopScreen():GetLifeMeter(pn):GetLife();
			local xpos = life*-256;
			self:x(xpos+154);
				if life == 1 then
					self:stopeffect();
				else
					self:pulse();
				end;
			self:horizalign(left);
			self:effectmagnitude(0,25,0);
			self:effectclock("beat");
			self:effecttiming(.5,.5,0,0);
		end;
		LifeMeterChangedP1MessageCommand=cmd(playcommand,"Start");
		LifeMeterChangedP2MessageCommand=cmd(playcommand,"Start");
	};

	--relleno
	LoadActor("life normal") .. {
		BeginCommand=cmd(addx,25;scaletoclipped,255,25;player,pn;playcommand,"Change";ztest,true);
		ChangeCommand=function(self)
			local lfzoom = SCREENMAN:GetTopScreen():GetLifeMeter(pn):GetLife();
			local hothreshold = 1.2
			self:cropleft(1-lfzoom);
				if lfzoom >= hothreshold then
					self:glowshift();
					self:effectperiod(0.1);
					self:effectcolor1(1,1,1,0);
					self:effectcolor2(1,1,1,1);
				else
					self:stopeffect();
				end;
		end;
		LifeMeterChangedP1MessageCommand=cmd(playcommand,"Change");
		LifeMeterChangedP2MessageCommand=cmd(playcommand,"Change");
	};

	--tip
	LoadActor("lifetip") .. {
		BeginCommand=cmd(player,pn;playcommand,"Xposition");
		XpositionCommand=function(self)
			local life = SCREENMAN:GetTopScreen():GetLifeMeter(pn):GetLife();
			life = clamp( life, 0, 1 )
			local xpos = life*-252;
			self:x(xpos+153);
					self:glowshift();
					self:effectperiod(0.1);
					self:effectcolor1(1,1,1,0);
					self:effectcolor2(1,1,1,1);
		end;
		LifeMeterChangedP1MessageCommand=cmd(playcommand,"Xposition");
		LifeMeterChangedP2MessageCommand=cmd(playcommand,"Xposition");
	};

	--frame
	LoadActor("lifeframe") .. {
		BeginCommand=cmd(player,pn);
		OnCommand=cmd(zoomx,-1);
	};

};
return t
end;

local t = Def.ActorFrame {
	LoadActor("Backp1.png") .. {
		Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoom,1);
	};
	
	LoadActor("Backp2.png") .. {
		Condition=GAMESTATE:IsPlayerEnabled( PLAYER_2 );
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoom,1);
	};
	
	LoadActor("Backp1-2.png") .. {
		Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 ) and GAMESTATE:IsPlayerEnabled( PLAYER_2 );
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoom,1);
	};

	customlifemeter(PLAYER_1) .. {
		Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		InitCommand=cmd(x,166;y,SCREEN_TOP+20;rotationy,180)
	};

	customlifemeter(PLAYER_2) .. {
		Condition=GAMESTATE:IsPlayerEnabled( PLAYER_2 );
		InitCommand=cmd(x,473;y,SCREEN_TOP+20)
	};
	
	LoadActor( "Stage.png" ) .. { 
		OnCommand = cmd(x,SCREEN_CENTER_X;y,23;zoom,1)
	};
	
	LoadActor( "Stage.png" ) .. {
		OnCommand=cmd(visible,false);
		PauseGamePressedMessageCommand=function(self)
			local b = SCREENMAN:GetTopScreen():IsPaused()			
			
			if b == true then
				SCREENMAN:GetTopScreen():PauseGame( false )
			elseif not b then
				SCREENMAN:GetTopScreen():PauseGame( true )
				SCREENMAN:SystemMessage( "For unpause press pause key" )
			end
		end;
	};


};
return t;


