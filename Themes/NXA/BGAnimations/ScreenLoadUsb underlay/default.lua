local t = Def.ActorFrame {

	LoadActor("USB.Lua") .. {
		OffCommand = cmd(linear,0.5; diffusealpha,0);};

	LoadActor( "USB enternousb1p.png" ) .. {
		Condition = GAMESTATE:IsPlayerEnabled( PLAYER_1 ) and not PROFILEMAN:IsPersistentProfile( PLAYER_1 );
		OnCommand = cmd( x,170; y, 240; addx,-460; zoom, 0.8;sleep,0.5;queuecommand,"Show" );
		ShowCommand = cmd( sleep, 1.5; linear,0.4; addx, 460 );
		OffCommand = cmd( linear, 0.4; addx,-460 );
		CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );};
		
	LoadActor("Info p1.png") .. {
		Condition = GAMESTATE:IsPlayerEnabled( PLAYER_1 ) and not PROFILEMAN:IsPersistentProfile( PLAYER_1 );
		OnCommand = cmd(x,160;y,365;diffuse,1,1,1,0;sleep,1;diffuse,1,1,1,1;sleep,4;playcommand,"Off");
		OffCommand = cmd(linear,.5;diffusealpha,0);
		CenterWasPressedP1MessageCommand = cmd(stoptweening;playcommand, "Off");};


	LoadActor("Info p1.png") .. {
		Condition = GAMESTATE:IsPlayerEnabled( PLAYER_1 ) and not PROFILEMAN:IsPersistentProfile( PLAYER_1 );
		InitCommand=cmd(blend,"BlendMode_Add";zoom,1.4;x,160;y,365);
		OnCommand = cmd(diffusealpha,0;sleep,.9;linear,.33;zoom,1;diffusealpha,.3;sleep,.01;diffusealpha,0);
		CenterWasPressedP1MessageCommand = cmd(stoptweening;diffuse,1,1,1,0);};


	LoadActor( "USB enternousb2p.png" ) .. {
		Condition = GAMESTATE:IsPlayerEnabled( PLAYER_2 ) and not PROFILEMAN:IsPersistentProfile( PLAYER_2 );
		OnCommand = cmd( x,SCREEN_RIGHT-170; y, 240; addx,460;sleep,0.5;queuecommand,"Show" );
		OffCommand = cmd( linear, 0.4; addx,460 );
		ShowCommand = cmd( zoom, 0.8;sleep,1.5; linear,0.4; addx, -460 );
		CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );};
		
	LoadActor("Info p2.png") .. {
		Condition = GAMESTATE:IsPlayerEnabled( PLAYER_2 ) and not PROFILEMAN:IsPersistentProfile( PLAYER_2 );
		OnCommand = cmd(x,SCREEN_RIGHT-160;y,365;diffuse,1,1,1,0;sleep,1;diffuse,1,1,1,1;sleep,4;playcommand,"Off");
		OffCommand = cmd(linear,.5;diffusealpha,0);
		CenterWasPressedP2MessageCommand = cmd( stoptweening;playcommand, "Off" );};


	LoadActor("Info p2.png") .. {
		Condition = GAMESTATE:IsPlayerEnabled( PLAYER_2 ) and not PROFILEMAN:IsPersistentProfile( PLAYER_2 );
		InitCommand = cmd(blend,"BlendMode_Add";zoom,1.4;x,SCREEN_RIGHT-160;y,365);
		OnCommand = cmd(diffusealpha,0;sleep,.9;linear,.33;zoom,1;diffusealpha,.3;sleep,.01;diffusealpha,0);
		CenterWasPressedP1MessageCommand = cmd(stoptweening;diffuse,1,1,1,0);};



};

return t
