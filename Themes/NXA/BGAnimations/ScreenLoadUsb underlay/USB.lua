local t = Def.ActorFrame {
		InitCommand=cmd(addx,8;addy,10);

	Def.ActorFrame {
		Condition=not GAMESTATE:IsAnyHumanPlayerUsingMemoryCard();

	LoadActor("USB_A.png") .. {
		OnCommand=cmd(x,320;y,258;diffusealpha,1;zoom,0.9;sleep,0.3;linear,0.3;addy,50;linear,0.2;addy,-20;linear,0;diffusealpha,0);};

        LoadActor("Protector.png") .. {
                OnCommand=cmd(x,320;y,162;diffusealpha,1;zoom,0.9;sleep,0.3;linear,0.3;addy,-50;diffusealpha,0);};

	LoadActor("USB_B.png") .. {
		OnCommand=cmd(x,325;y,285;diffusealpha,0;zoom,1;sleep,0.8;linear,0;diffusealpha,1;sleep,0.05;diffusealpha,0);};

	LoadActor("USB_C.png") .. {
		OnCommand=cmd(x,330;y,270;diffusealpha,0;zoom,1.1;sleep,0.85;linear,0;diffusealpha,1;sleep,0.05;diffusealpha,0);};

	LoadActor("USB_D.png") .. {
		OnCommand=cmd(x,335;y,270;diffusealpha,0;zoom,0.9;sleep,0.9;linear,0;diffusealpha,1;sleep,0.05;linear,0.05;diffusealpha,0);};

	LoadActor("USB_E.png") .. {
		OnCommand=cmd(x,340;y,280;diffusealpha,0;zoom,0.9;sleep,0.95;linear,0;diffusealpha,1);};

	LoadActor("USB_E.png") .. {
		InitCommand = cmd(x,340;y,280;diffusealpha,0;zoom,0.9);
		OnCommand = cmd(sleep,1.2;playcommand,"Pulse");
		PulseCommand = cmd(x,340;y,280;sleep,0.8;diffusealpha,0.5;linear,0.3;addx,-40;addy,-30;diffusealpha,0;queuecommand,"Pulse");
		OffCommand= cmd (stoptweening;linear,0.4;zoom,1,1;diffusealpha,0);};

	--LoadActor("Flechas 4x2.png") .. {
		--OnCommand=cmd(x,350;y,250;diffusealpha,0;blend,"BlendMode_Add";zoom,0.9;sleep,0.95;linear,0;diffusealpha,0.3);};

	LoadActor("Flechas 4x2.png") .. {
		OnCommand=cmd(x,350;y,250;blend,"BlendMode_Add";diffusealpha,0;zoom,0.9;sleep,0.95;linear,0;diffusealpha,1;diffuse,1,.7,.7,1);};



	LoadActor("USB_E.png") .. {
		--OnCommand=cmd(x,340;y,280;diffusealpha,0;zoom,1;sleep,1.15;diffusealpha,0.5;linear,0.45;addx,30;addy,20;diffusealpha,0);
		OnCommand=cmd(x,340;y,280;diffusealpha,0;zoom,1;sleep,1.1;diffusealpha,0.5;linear,1;zoom,1.1;diffusealpha,0);};

	LoadActor("Flechas 4x2.png") .. {
		InitCommand = cmd(x,350;y,250;blend,"BlendMode_Add";diffusealpha,0;zoom,0.9);
		OnCommand=cmd(sleep,1.2;playcommand,"Pulse");
		PulseCommand = cmd(x,350;y,250;sleep,0.8;diffusealpha,0.5;linear,0.3;zoom,1.2;diffusealpha,0;linear,0;queuecommand,"Pulse");
		OffCommand= cmd (stoptweening;linear,0.4;zoom,1,1;diffusealpha,0);};
	};


	Def.ActorFrame {
		Condition=GAMESTATE:IsAnyHumanPlayerUsingMemoryCard();

	LoadActor("USB_E.png") .. {
		OnCommand=cmd(x,340;y,280;diffusealpha,0;zoom,1;linear,0;diffusealpha,1);};

	LoadActor("USB_E.png") .. {
		InitCommand = cmd(x,340;y,280;diffusealpha,0;zoom,1);
		OnCommand = cmd(sleep,0.5;playcommand,"Pulse");
		PulseCommand = cmd(x,340;y,280;sleep,0.8;diffusealpha,0.5;linear,0.3;addx,-40;addy,-30;diffusealpha,0;queuecommand,"Pulse");};

	LoadActor("Flechas 4x2.png") .. {
		OnCommand=cmd(x,350;y,250;blend,"BlendMode_Add";diffusealpha,0;zoom,1;linear,0;diffusealpha,0.7);};


	LoadActor("Flechas 4x2.png") .. {
		InitCommand = cmd(x,350;y,250;blend,"BlendMode_Add";diffusealpha,0;zoom,1);
		OnCommand=cmd(sleep,0.5;playcommand,"Pulse");
		PulseCommand = cmd(x,350;y,250;sleep,0.8;diffusealpha,0.5;linear,0.3;zoom,1.3;diffusealpha,0;linear,0;queuecommand,"Pulse");};
	};
};

return t; 
