
local template = Def.ActorFrame {
	LoadFont("Micrograma2") .. {
		SetCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetDisplayMainTitle();
			local artist = GAMESTATE:GetCurrentSong():GetDisplayArtist();
			local bps1 = GAMESTATE:GetCurrentSong():BPMMin();
			local bps1 = string.format("%i", bps1);
			local bps2 = GAMESTATE:GetCurrentSong():BPMMax();
			local bps2 = string.format("%i", bps2);
			local spLocked = GAMESTATE:GetCurrentSong():SpecialSongIsLocked();
			--local song2 = GAMESTATE:GetCurrentSong();

			--local color = SONGMAN:GetSongColor(song2);

			if not song and not course then
				self:hidden(1)
				return
			end

			self:hidden(0)
			self:horizalign('HorizAlign_Left')
			--self:diffuse( color )

			if song then
				if bps1 == bps2 then
					self:settext( song .. " - " .. artist .. " -  BPM: " .. bps1 );
				else
					self:settext( song .. " - " .. artist .. " -  BPM: " .. bps1 .. "~" .. bps2 );
				end
			else
				self:settext( "No Song" );
			end

			--si la cancion esta bloqueada, no mostrar el nombre!
			if spLocked then
				self:settext( "" );
			end
		end;

		CurrentSongChangedMessageCommand=cmd(playcommand,"Set");
	};

	OnCommand=cmd(zoomx,0;sleep,0.3;linear,0.2;zoomx,1);

}

local t = Def.ActorScroller {
	SecondsPerItem = 5.5;
	NumItemsToDraw = 1;
	TransformFunction = function( self, offset, itemIndex, numItems )
		self:x(1240*offset)
	end;
	OnCommand = cmd(scrollwithpadding,2,1;SetLoop,true;addx,-350;maxwidth,430);
}

--cuantas veces inserta en la tabla
--for i=1,10 do
table.insert( t, template );
--end

return t;
