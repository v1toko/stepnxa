return Def.ActorFrame {	

	LoadActor("ToNormal.png") .. {
		Condition=GAMESTATE:GetPlayMode() == "PlayMode_Regular";
		OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT;sleep,0.01;diffusealpha,1;linear,2;queuecommand,"Test");		
		TestCommand=cmd(playcommand,"Load");
		LoadCommand=function()
			SCREENMAN:GetTopScreen():Continue();
		end;
	};

	LoadActor("ToSpecial.png") .. {
		Condition=GAMESTATE:GetPlayMode() == "PlayMode_Special";
		OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT;sleep,0.01;diffusealpha,1;linear,2;queuecommand,"Test");
		TestCommand=cmd(playcommand,"Load");
		LoadCommand=function()
			SCREENMAN:GetTopScreen():Continue();
		end;
	};

	LoadActor("ToWorldMax.png") .. {
		Condition=GAMESTATE:GetPlayMode() == "PlayMode_Mission";
		OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT;sleep,0.01;diffusealpha,1;linear,2;queuecommand,"Test");
		TestCommand=cmd(playcommand,"Load");
		LoadCommand=function()
			SCREENMAN:GetTopScreen():Continue();
		end;
	};

	LoadActor("ToEasy.png") .. {
		Condition=GAMESTATE:GetPlayMode() == "PlayMode_Easy";
		OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT;sleep,0.01;diffusealpha,1;linear,2;queuecommand,"Test");
		TestCommand=cmd(playcommand,"Load");
		LoadCommand=function()
			SCREENMAN:GetTopScreen():Continue();
		end;
	};
	
	LoadActor("ToBrain.png") .. {
		Condition=GAMESTATE:GetPlayMode() == "PlayMode_Brain";
		OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT;sleep,0.01;diffusealpha,1;linear,2;queuecommand,"Test");
		TestCommand=cmd(playcommand,"Load");
		LoadCommand=function()
			SCREENMAN:GetTopScreen():Continue();
		end;
	};

	Def.ActorFrame {		
	};
};
