local t = Def.ActorFrame {	
	LoadActor( "rank_aa.mp3" ) .. {--S, A
		Condition = STATSMAN:GetBestGrade() == 0 or STATSMAN:GetBestGrade() == 1; 
		OnCommand=cmd(sleep,4;queuecommand,"Play");
		PlayCommand=cmd(play);
	};
	
	LoadActor( "9-s.mp3" ) .. {--celebracion S
		Condition = STATSMAN:GetBestGrade() == 0 or STATSMAN:GetBestGrade() == 1; 
		OnCommand=cmd(sleep,4;queuecommand,"Play");
		PlayCommand=cmd(play);
	};
	
	LoadActor( "rank_a.mp3" ) .. {--A
		Condition = STATSMAN:GetBestGrade() == 2; 
		OnCommand=cmd(sleep,4;queuecommand,"Play");
		PlayCommand=cmd(play);
	};

	LoadActor( "9-a.mp3" ) .. {--celebracion a
		Condition = STATSMAN:GetBestGrade() == 2; 
		OnCommand=cmd(sleep,4;queuecommand,"Play");
		PlayCommand=cmd(play);
	};
	
	LoadActor( "rank_b.mp3" ) .. {--B
		Condition = STATSMAN:GetBestGrade() == 3; 
		OnCommand=cmd(sleep,4;queuecommand,"Play");
		PlayCommand=cmd(play);
	};
	
	LoadActor( "9-b.mp3" ) .. {--celebracion b
		Condition = STATSMAN:GetBestGrade() == 3; 
		OnCommand=cmd(sleep,4;queuecommand,"Play");
		PlayCommand=cmd(play);
	};
	
	LoadActor( "rank_c.mp3" ) .. {--c
		Condition = STATSMAN:GetBestGrade() == 4; 
		OnCommand=cmd(sleep,4;queuecommand,"Play");
		PlayCommand=cmd(play);
	};
	
	LoadActor( "9-c.mp3" ) .. {--celebracion c
		Condition = STATSMAN:GetBestGrade() == 4; 
		OnCommand=cmd(sleep,4;queuecommand,"Play");
		PlayCommand=cmd(play);
	};
	
	LoadActor( "rank_d.mp3" ) .. {--d
		Condition = STATSMAN:GetBestGrade() == 5; 
		OnCommand=cmd(sleep,4;queuecommand,"Play");
		PlayCommand=cmd(play);
	};
	
	LoadActor( "9-d.mp3" ) .. {--celebracion d
		Condition = STATSMAN:GetBestGrade() == 5; 
		OnCommand=cmd(sleep,4;queuecommand,"Play");
		PlayCommand=cmd(play);
	};
	
	LoadActor( "rank_f.mp3" ) .. {--f
		Condition = STATSMAN:GetBestGrade() == 6; 
		OnCommand=cmd(sleep,4;queuecommand,"Play");
		PlayCommand=cmd(play);
	};
	
	LoadActor( "rank_f.mp3" ) .. {--failed
		Condition = STATSMAN:GetBestGrade() == 20; 
		OnCommand=cmd(sleep,4;queuecommand,"Play");
		PlayCommand=cmd(play);
	};
	
	LoadActor( "9-f.mp3" ) .. {--celebracion d
		Condition = STATSMAN:GetBestGrade() == 6 or STATSMAN:GetBestGrade() == 20; 
		OnCommand=cmd(sleep,4;queuecommand,"Play");
		PlayCommand=cmd(play);
	};
};

return t; 
