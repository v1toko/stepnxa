local t = Def.ActorFrame {
	Def.ActorFrame {
		Condition=GetIfShowBlockOff();
		--OnCommand=cmd(hide_if,not GetIfShowBlockOff());
		LoadActor("../ScreenEvaluationStage background/fondo.avi") .. {
			OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;FullScreen);
		};

		LoadActor( "barrera broken back" ) .. { 
			OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;linear,0.5;diffusealpha,1);
			OffCommand = cmd(linear,0.5);
		};

		LoadActor( THEME:GetPathG("Mission", "barrier" .. GetLastBarrierType() ) ) .. { 
			OnCommand = cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoom,2.5;sleep,0.5;linear,0.5;diffusealpha,1; vibrate; sleep,1.4; diffusealpha,0);
		};
		
		LoadActor("explosion_grade") .. {
			OnCommand=cmd(blend,"BlendMode_Add";x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;draworder,3;diffusealpha,0;sleep,2.2;zoom,2;linear,0.2;diffusealpha,1;linear,0.2;diffusealpha,0;);
		};
		
		LoadActor("lockoff") .. {
			OnCommand=cmd(sleep,2;queuecommand,"Play");
			PlayCommand=cmd(play);
		};
		
		Def.Actor {
			OnCommand = cmd( sleep,4 );
		};
		
		Def.Quad {
			InitCommand=cmd(stretchto,SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM;diffuse,color("#000000"));
			OnCommand=cmd(diffusealpha,0);
			OffCommand=cmd(sleep,45;linear,0.4;diffusealpha,1);
		};
		
		LoadFont( "BoostSSi" ) .. {
			Condition=GAMESTATE:GetIfItemWasGained();
			OnCommand=cmd(zoom,0.9;diffusealpha,0;settext,"Item Obtenido";x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
			OffCommand=cmd( linear,0.1; diffusealpha,1;sleep,1 );
		};
	};
	
	Def.ActorFrame {
		Condition=not GetIfShowBlockOff();
		--OnCommand=cmd(hide_if,GetIfShowBlockOff());
		Def.Quad {
			InitCommand=cmd(stretchto,SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM;diffuse,color("#000000"));
			OnCommand=cmd(diffusealpha,0);
			OffCommand=cmd(linear,0.3;diffusealpha,1);
		};
		
		LoadFont( "BoostSSi" ) .. {
			Condition=GAMESTATE:GetIfItemWasGained();
			OnCommand=cmd(zoom,1.1;diffusealpha,0;settext,"Item Obtenido";x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
			OffCommand=cmd( linear,0.1; diffusealpha,1;sleep,1 );
		};
	};
};

return t; 
