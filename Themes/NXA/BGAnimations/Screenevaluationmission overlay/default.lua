local t = Def.ActorFrame {
	LoadActor("banner frame") .. {
		OnCommand=cmd(x,320;y,240;diffusealpha,0;zoomy,0.5;sleep,5;fadeleft,4;faderight,4;linear,0.3;fadeleft,0.5;faderight,0.5;diffusealpha,1;sleep,9.5;linear,0.3;diffusealpha,0);
	};

	Def.Quad {
		InitCommand=cmd(stretchto,SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM;diffuse,color("#000000"));
		OnCommand=cmd(diffusealpha,0;sleep,4.25;linear,.25;diffusealpha,.15;sleep,.2;linear,2;diffusealpha,0);
	};

	LoadActor("../ScreenGraphNX overlay/light") .. {
		InitCommand=cmd(x,320;y,175;blend,"BlendMode_Add";rotationz,90;diffuse,0,.2,1,1;zoomy,0);
		OnCommand=cmd(sleep,4.5;linear,0.5;zoomy,5;sleep,.1;accelerate,1.75;diffusealpha,0);
		OffCommand=cmd(diffusealpha,0);
	};

	LoadActor("../ScreenGraphNX overlay/light") .. {
		InitCommand=cmd(x,320;y,175;blend,"BlendMode_Add";rotationz,90;diffuse,0,.2,1,1;zoomy,0);
		OnCommand=cmd(sleep,4.5;linear,0.5;zoomy,5;sleep,.1;accelerate,1.75;diffusealpha,0);
		OffCommand=cmd(diffusealpha,0);
	};

	LoadActor("../ScreenGraphNX overlay/light") .. {
		InitCommand=cmd(x,320;y,305;blend,"BlendMode_Add";rotationz,90;diffuse,0,.2,1,1;zoomy,0);
		OnCommand=cmd(sleep,4.5;linear,0.5;zoomy,5;sleep,.1;accelerate,1.75;diffusealpha,0);
		OffCommand=cmd(diffusealpha,0);
	};

	LoadActor("../ScreenGraphNX overlay/light") .. {
		InitCommand=cmd(x,320;y,305;blend,"BlendMode_Add";rotationz,90;diffuse,0,.2,1,1;zoomy,0);
		OnCommand=cmd(sleep,4.5;linear,0.5;zoomy,5;sleep,.1;accelerate,1.75;diffusealpha,0);
		OffCommand=cmd(diffusealpha,0);
	};

	LoadActor("SUCESS").. {
		Condition=not STATSMAN:GetCurStageStats():MissionFailed();
		OnCommand=cmd(x,320;y,240;vibrate;diffusealpha,0;zoom,0.6;sleep,5;linear,0.4;diffusealpha,0.8;zoom,0.9;sleep,0;linear,0.3;zoom,1;diffusealpha,0;sleep,9.5;linear,0.3;diffusealpha,0);
		OffCommand=cmd(diffusealpha,0);
	};

	LoadActor("SUCESS").. {
		Condition=not STATSMAN:GetCurStageStats():MissionFailed();
		OnCommand=cmd(x,320;y,240;diffusealpha,0;zoom,0.6;sleep,5;linear,0.4;diffusealpha,1;zoom,0.7;sleep,9.5;linear,0.3;diffusealpha,0);
		OffCommand=cmd(diffusealpha,0);
	};

	LoadActor("FAILURE").. {
		Condition=STATSMAN:GetCurStageStats():MissionFailed();
		OnCommand=cmd(x,320;y,240;vibrate;diffusealpha,0;zoom,0.6;sleep,5;linear,0.4;diffusealpha,0.8;zoom,0.9;sleep,0;linear,0.3;zoom,1;diffusealpha,0;sleep,9.5;linear,0.3;diffusealpha,0);
		OffCommand=cmd(diffusealpha,0);
	};

	LoadActor("FAILURE").. {
		Condition=STATSMAN:GetCurStageStats():MissionFailed();
		OnCommand=cmd(x,320;y,240;diffusealpha,0;zoom,0.6;sleep,5;linear,0.4;diffusealpha,1;zoom,0.7;sleep,9.5;linear,0.3;diffusealpha,0);
		OffCommand=cmd(diffusealpha,0);
	};
};

return t; 
