local t = Def.ActorFrame {

	LoadActor("Background") .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;FullScreen;);
		OffCommand=cmd(linear,.3;diffuse,.3,.3,.3,1);
	};
		
	LoadActor( "SS2" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0);
		OnCommand=cmd(sleep,1;linear,0.7;diffusealpha,1);
		OffCommand=cmd(diffusealpha,0);
	};

	LoadActor("Select Station.png") .. {
		OnCommand = cmd(zoom,2;diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;linear,0.5;zoom,1;diffusealpha,1);
		OffCommand = cmd(sleep,0.5;linear,0.6;zoom,2;diffuse,.4,.4,.4,1);
	};

	LoadActor("Select Station.png") .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;sleep,0.8;diffusealpha,0.5;blend,'BlendMode_Add';linear,0.9;zoom,1.05;diffusealpha,0);
	};
	
	LoadActor("text glow") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-17;diffusealpha,0;blend,"BlendMode_Add";rotationz,180);
		OnCommand = cmd(zoom,2;diffusealpha,0;linear,0.5;zoom,1;diffusealpha,.8;sleep,.3;linear,1;diffusealpha,0);
	};

	LoadActor("text glow") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+5;diffusealpha,0;blend,"BlendMode_Add";rotationz,180;zoomx,.25);
		OnCommand = cmd(zoomy,1.8;diffusealpha,0;linear,0.5;zoomy,.9;diffusealpha,.8;sleep,.3;linear,1;diffusealpha,0);
	};

	LoadActor("text glow") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;blend,"BlendMode_Add");
		OnCommand = cmd(zoom,2;diffusealpha,0;linear,0.5;zoom,1;diffusealpha,.8;sleep,.3;linear,1;diffusealpha,0);
	};

	LoadActor("text glow") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;blend,"BlendMode_Add");
		DownLeftPressedMessageCommand=cmd(playcommand,"Show");
		DownRightPressedMessageCommand=cmd(playcommand,"Show");
		ShowCommand=cmd(finishtweening;zoomx,1;sleep,.4;diffusealpha,1;linear,.25;zoomx,1.15;linear,.5;diffusealpha,0);
	};
	

	Def.Actor { OffCommand=cmd(sleep,2.8); };
	};

return t 
