local t = Def.ActorFrame {
	LoadActor("GAMEOVER") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
		OnCommand=cmd(FullScreen);
	};

	Def.Quad {
		InitCommand=cmd(stretchto,SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM;diffuse,color("#000000");draworder,99999);
		OnCommand=cmd(diffusealpha,0;sleep,3.1;linear,.5;diffusealpha,1);
	};

	Def.Actor { OnCommand=cmd(sleep,6); };
};

return t;
