local index = ...
assert( index );

local t = Def.ActorFrame {

	LoadActor("bullet.png") .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoomx,1.38);
	};

	LoadFont("Micrograma2") .. { 
		OnCommand=cmd( x,SCREEN_CENTER_X-270;y,SCREEN_CENTER_Y-3;horizalign,'HorizAlign_Left';shadowlength,0;settext, string.format("%.2i", index) .. "          " .. GetRecordName(index) );
	};
}
return t 
