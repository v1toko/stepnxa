local t = Def.ActorFrame {

	LoadActor("Background.avi") .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT;zoomy,1.2);
	};

	LoadActor("bg.png") .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;linear,0;shadowlength,0;diffuse,1,1,1,1;zoomx,1;zoomy,1.3;linear,0.1;zoomx,0.8;zoomy,0.7;linear,0.45;diffuse,1,1,1,0.8;linear,0.4;zoomx,1.1;zoomy,0.7;diffuse,1,1,1,0);
	};

	Def.ActorFrame {
		OnCommand=cmd(diffusealpha,0;linear,2;diffusealpha,1;sleep,1;linear,10;addy,850;sleep,2);

		LoadActor("hs.lua", 20) .. {
			OnCommand = cmd(addy,100);
		};

		LoadActor("hs.lua", 19) .. {
			OnCommand = cmd(addy,50);
		};

		LoadActor("hs.lua", 18) .. {
			OnCommand = cmd(addy,0);
		};

		LoadActor("hs.lua", 17) .. {
			OnCommand = cmd(addy,-50);
		};

		LoadActor("hs.lua", 16) .. {
			OnCommand = cmd(addy,-100);
		};

		LoadActor("hs.lua", 15) .. {
			OnCommand = cmd(addy,-150);
		};
		
		LoadActor("hs.lua", 14) .. {
			OnCommand = cmd(addy,-200);
		};

		LoadActor("hs.lua", 13) .. {
			OnCommand = cmd(addy,-250);
		};

		LoadActor("hs.lua", 12) .. {
			OnCommand = cmd(addy,-300);
		};

		LoadActor("hs.lua", 11) .. {
			OnCommand = cmd(addy,-350);
		};

		LoadActor("hs.lua", 10) .. {
			OnCommand = cmd(addy,-400);
		};

		LoadActor("hs.lua", 9) .. {
			OnCommand = cmd(addy,-450);
		};

		LoadActor("hs.lua", 8) .. {
			OnCommand = cmd(addy,-500);
		};

		LoadActor("hs.lua", 7) .. {
			OnCommand = cmd(addy,-550);
		};

		LoadActor("hs.lua", 6) .. {
			OnCommand = cmd(addy,-600);
		};

		LoadActor("hs.lua", 5) .. {
			OnCommand = cmd(addy,-650);
		};

		LoadActor("hs.lua", 4) .. {
			OnCommand = cmd(addy,-700);
		};

		LoadActor("hs.lua", 3) .. {
			OnCommand = cmd(addy,-750);
		};

		LoadActor("hs.lua", 2) .. {
			OnCommand = cmd(addy,-800);
		};

		LoadActor("hs.lua", 1) .. {
			OnCommand = cmd(addy,-850);
		};
	};

	LoadActor("HIGHSCORE1.png") .. {
		OnCommand = cmd(zoom,2;diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_TOP+30;linear,0.5;zoom,1;diffusealpha,1;);
		OffCommand = cmd(linear,0.5;zoom,2);
	};

	LoadActor("HIGHSCORE1.png") .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_TOP+30;diffusealpha,0;sleep,0.8;diffusealpha,0.5;blend,'BlendMode_Add';linear,0.8;zoom,1.05;diffusealpha,0);
	};
}
return t 
