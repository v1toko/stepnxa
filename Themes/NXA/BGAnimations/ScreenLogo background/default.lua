local t = Def.ActorFrame {

	LoadActor ("NXAbsolute" ) .. {
		InitCommand=cmd(FullScreen);
	};
	

	Def.ActorFrame {
		Condition = GAMESTATE:GetCoinMode() == "CoinMode_Pay" and GAMESTATE:GetCoins() <= 0 or not GAMESTATE:GetCoinMode() == "CoinMode_Free";

	LoadActor("InsertCoin") .. {
		OnCommand=cmd(x,115;y,445;pulse;effectperiod,1;effectmagnitude,1,1.2,0);
	};
	
	LoadActor("InsertCoin") .. {
		OnCommand=cmd(x,115;y,445;diffusealpha,1;playcommand,"Pulse");
		PulseCommand=cmd(sleep,0.5;diffusealpha,1;zoom,1.2;linear,0.3;diffusealpha,0;zoom,1.4;sleep,0.2;queuecommand,"Pulse");
	};
	
	LoadActor("InsertCoin") .. {
		OnCommand=cmd(x,535;y,445;pulse;effectperiod,1;effectmagnitude,1,1.2,0);
	};
	
	LoadActor("InsertCoin") .. {
		OnCommand=cmd(x,535;y,445;diffusealpha,1;playcommand,"Pulse");
		PulseCommand=cmd(sleep,0.5;diffusealpha,1;zoom,1.2;linear,0.3;diffusealpha,0;zoom,1.4;sleep,0.2;queuecommand,"Pulse");
	};


	};

};

return t;