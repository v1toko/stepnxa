local t = Def.ActorFrame {};

t[#t+1] = Def.ActorFrame {
	Condition=not GAMESTATE:IsEasyMode();

	Def.Actor { OnCommand=cmd(sleep,0.9); };	
	
	--net stuff
	Def.Actor {
		NetNamesChangedMessageCommand=function(self,param)
			--SCREENMAN:SystemMessage( param.Names )
		end;

		-- test stuff!!
		-- CurrentSongChangedMessageCommand=function(self)
		-- 	local song = GAMESTATE:GetCurrentSong()
		-- 	SCREENMAN:SystemMessage( song:GetSongDir() )
		-- end;
	};
	
	--[[Def.Quad {
		InitCommand=cmd(stretchto,SCREEN_RIGHT-140,SCREEN_TOP+20,SCREEN_RIGHT-10,SCREEN_BOTTOM-100;diffuse,color("#000000"));
		OnCommand=cmd(diffusealpha,0.5);
		OffCommand=cmd(sleep,0.7;linear,0.3;diffusealpha,0);
	};
		
	LoadFont( "blaster" ) .. {
		OnCommand=cmd(x,SCREEN_RIGHT-10;y,SCREEN_TOP+30;zoom,0.5;horizalign,'HorizAlign_Right');
		PlayerUpdateInfoMessageCommand=function(self,param)
			local p = param.Players
			local p2 = param.NumPlayers
			local namesnet = param.NamesNet
			local names = param.Names
			local steps = param.StepsTypes
			local diffs = param.Difficulties
			local meters = param.Meters
			
			--sp = splitted
			local spnames = split( ",", names )
			local spsteps = split(",", steps )
			local spdiffs = split(",",diffs)
			local spmeters = split(",", meters)	
							
			local text = ""
			for i = 1, p, 1 do
				local stepss 
				if spsteps[i] == "pump-single" then
					stepss = "Single"
				else
					stepss = "Double"
				end
				text = "\n" .. text .. "\n" .. spnames[i] .. "\n" .. stepss .. " Lv: " .. spmeters[i]
				--self:diffusecolor( ShortDifficultyColor( spdiffs[i] ) )
			end	
			
			self:settext( text )
			self:shadowlength( 0 )
		end;	
	};]]
	
	Def.NetPlayerInfo {
		Condition=IsNetSMOnline();
		MaxPlayers = 8;

		InitCommand=cmd(x,SCREEN_RIGHT-90;y,SCREEN_TOP+90);
		OnCommand=cmd(zoomy,0;bounceend,0.3;zoom,1);
		OffCommand=cmd(zoomy,1;bouncebegin,0.3;zoomy,0);
		ShowCommand=cmd(bouncebegin,0.3;zoomy,1);
		HideCommand=cmd(linear,0.3;zoomy,0);
		SetCommand=function(self)
			self:zoom( 0.5 )
			self:horizalign('HorizAlign_Right')
			self:SetFromGameState();
			self:setsecondsperitem(999999);
			self:SetSecondsPauseBetweenItems(0);
			--self:scrollwithpadding(0, 0);
		end;
		PlayerUpdateInfoMessageCommand=cmd(playcommand,"Set");		

		Display = Def.ActorFrame { 
			InitCommand=cmd(setsize,270,44);

			LoadActor("Display") .. {
				SetNetPlayerInfoMessageCommand=function(self, params)					
					--self:settext( params.PlayerName );
					--self:zoomx( 1.5 )
					--self:zoomy( 1.3 )
					--if params.StepsType == 'StepsType_Pump_Double' then
					--	self:diffuse( color("#2cff00") );
					--elseif params.StepsType == 'StepsType_Pump_Single' then
					--	self:diffuse( color("#ff9a00") );
					--end
				end;
			};
			LoadActor("Display2") .. {
				SetNetPlayerInfoMessageCommand=function(self, params)					
					--self:zoomx( 1.5 )
					--self:zoomy( 1.3 )
					self:diffuse( DifficultyColor(params.Difficulty) );
				end;
			};
			
			LoadFont("Common","normal") .. {
				OnCommand=cmd(x,SCREEN_CENTER_X-200;y,-8);
				SetNetPlayerInfoMessageCommand=function(self, params)					
					self:maxwidth(250)
					local style
					if params.StepsType == 'StepsType_Pump_Double' then
						style = "Double Lv."
					elseif params.StepsType == 'StepsType_Pump_Single' then
						style = "Single Lv."
					end
					self:settext( params.PlayerName .. " " .. style .. " " .. params.Meter );
					self:shadowlength( 0 )
					--self:zoom( 0.5 )
					--self:diffuse( DifficultyColor(params.Difficulty) );
				end;
			};

			--[[Def.TextBanner {
				ArtistPrependString="/";
				SetCommand=TextBannerSet;
				InitCommand=cmd(LoadFromString,"", "", "", "", "", "");
				Title = LoadFont("TextBanner","text") .. {
					Name="Title";
					OnCommand=cmd(shadowlength,0);
				};
				Subtitle = LoadFont("TextBanner","text") .. {
					Name="Subtitle";
					OnCommand=cmd(shadowlength,0);
				};
				Artist = LoadFont("TextBanner","text") .. {
					Name="Artist";
					OnCommand=cmd(shadowlength,0);
				};
				SetSongCommand=function(self, params)
					if params.Song then
						self:LoadFromSong( params.Song );
						self:diffuse( SONGMAN:GetSongColor(params.Song) );
					else
						self:LoadFromString( "??????????", "??????????", "", "", "", "" );
						self:diffuse( color("#FFFFFF") );
					end
				end;
			};

			LoadFont("CourseEntryDisplay","number") .. {
				OnCommand=cmd(x,-118;shadowlength,0);
				SetSongCommand=function(self, params) self:settext(string.format("%i", params.Number)); end;
			};

			LoadFont("Common","normal") .. {
				OnCommand=cmd(x,SCREEN_CENTER_X-200;y,-8;zoom,0.7;shadowlength,0);
				DifficultyChangedCommand=function(self, params)
					if params.PlayerNumber ~= GAMESTATE:GetMasterPlayerNumber() then return end
					self:settext( params.Meter );
					self:diffuse( DifficultyColor(params.Difficulty) );
				end;
			};

			LoadFont("Common","normal") .. {
				OnCommand=cmd(x,SCREEN_CENTER_X-192;y,SCREEN_CENTER_Y-230;horizalign,right;zoom,0.5;shadowlength,0);
				SetSongCommand=function(self, params) self:settext(params.Modifiers); end;
			};

			LoadFont("CourseEntryDisplay","difficulty") .. {
				OnCommand=cmd(x,SCREEN_CENTER_X-222;y,-8;shadowlength,0;settext,"1");
				DifficultyChangedCommand=function(self, params)
					if params.PlayerNumber ~= GAMESTATE:GetMasterPlayerNumber() then return end
					self:diffuse( DifficultyColor(params.Difficulty) );
				end;
			};]]
		};
	};
	
		--MODS
	--[[Def.OptionIconRow {
		Name="optioniconrow";
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_1);
		InitCommand=cmd(x,SCREEN_LEFT+28;y,SCREEN_TOP+93;set,PLAYER_1);
		OnCommand=cmd(diffusealpha,0;linear,0.2;diffusealpha,1);
		PlayerOptionsChangedP1MessageCommand=cmd(set,PLAYER_1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
		ModsIconsChangedMessageCommand=function(self,param)
			--SCREENMAN:SystemMessage( "Mod: " .. param.FindedMod )
		end;
	};

	Def.OptionIconRow {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_2);
		InitCommand=cmd(x,SCREEN_RIGHT-38;y,SCREEN_TOP+93;set,PLAYER_2);
		OnCommand=cmd(diffusealpha,0;linear,0.2;diffusealpha,1);
		PlayerOptionsChangedP2MessageCommand=cmd(set,PLAYER_2);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};	
	
	LoadActor("i can flash") .. {		
		OnCommand=cmd(blend,"BlendMode_Add";diffusealpha,0;x,SCREEN_LEFT+28;y,SCREEN_TOP+93);		
		UpdateCommand=cmd(finishtweening;zoom,1;diffusealpha,0;linear,0.1;zoom,1.8;diffusealpha,1;linear,0.8;diffusealpha,0;zoom,0);
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_1);
		ModsIconsChangedP1MessageCommand=function(self,param)
			--Trace( tostring(param.Player) )
			--Trace( "MOD: effects" )
			--SCREENMAN:SystemMessage( "PLAYER 1: " .. param.Player )
			--if not GAMESTATE:IsHumanPlayer(param.Player) then return end
			--if not param.Player == "P1" then return end
			if param.FindedMod == "NONE" then return end
			
			--Trace( tostring(param.Player) )
			
			local colnum = param.ColNum					
			if colnum == -1 then return end

			colnum = colnum + 1
			
			--Trace( "num: " .. tostring(colnum) )
			
			local spacingY = THEME:GetMetric( "OptionIconRow", "SpacingY" )
			
			--Trace( "spc: " .. tostring(spacingY) )
			
			self:y( (SCREEN_TOP+93) + (colnum * spacingY) );
			self:playcommand( "Update" );
			
			--Trace( "MOD: effets FiN" )
		end;
	};
	
	LoadActor("i can flash") .. {		
		OnCommand=cmd(blend,"BlendMode_Add";diffusealpha,0;x,SCREEN_RIGHT-38;y,SCREEN_TOP+93);		
		UpdateCommand=cmd(finishtweening;zoom,1;diffusealpha,0;linear,0.1;zoom,1.8;diffusealpha,1;linear,0.8;diffusealpha,0;zoom,0);
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_2);
		ModsIconsChangedP2MessageCommand=function(self,param)
			--Trace( tostring(param.Player) )
			--Trace( "MOD: effects" )
			--SCREENMAN:SystemMessage( "PLAYER 2: " .. param.Player )
			--if not GAMESTATE:IsHumanPlayer(param.Player) then return end
			--if not param.Player == "P2" then return end
			if param.FindedMod == "NONE" then return end
			
			--Trace( tostring(param.Player) )
			
			local colnum = param.ColNum					
			if colnum == -1 then return end

			colnum = colnum + 1
			
			--Trace( "num: " .. tostring(colnum) )
			
			local spacingY = THEME:GetMetric( "OptionIconRow", "SpacingY" )
			
			--Trace( "spc: " .. tostring(spacingY) )
			
			self:y( (SCREEN_TOP+93) + (colnum * spacingY) );
			self:playcommand( "Update" );
			
			--Trace( "MOD: effets FiN" )
		end;
	};]]

	Def.Actor {
		--OnCommand=cmd(stoptweening;y,200;sleep,.2;decelerate,.1;y,-10;decelerate,.1;y,0);
		--StartSelectingStepsMessageCommand=cmd(stoptweening;y,0;accelerate,.1;y,-10;decelerate,.1;y,200);
		GoBackSelectingGroupMessageCommand=function(self) 
			--cmd(stoptweening;y,0;accelerate,.1;y,-10;decelerate,.1;y,200);
			local Wheel = SCREENMAN:GetTopScreen():GetChild('MusicWheel'); 
			Wheel:stoptweening();
			Wheel:addy(0);
			Wheel:accelerate(0.1);
			Wheel:addy(-10);
			Wheel:decelerate(0.1);
			Wheel:addy(600);

			local Banner = SCREENMAN:GetTopScreen():GetChild('Banner'); 
			Banner:stoptweening();
			Banner:addy(0);
			Banner:accelerate(0.1);
			Banner:addy(-10);
			Banner:decelerate(0.1);
			Banner:addy(600);

			local CDTitle = SCREENMAN:GetTopScreen():GetChild('CDTitle'); 
			CDTitle:stoptweening();
			CDTitle:addy(0);
			CDTitle:accelerate(0.1);
			CDTitle:addy(-10);
			CDTitle:decelerate(0.1);
			CDTitle:addy(600);
			--SCREENMAN:SystemMessage("GoBackSelectingGroupMessageCommand");
		end;
		StartSelectingSongMessageCommand=function(self)		
			local Wheel = SCREENMAN:GetTopScreen():GetChild('MusicWheel');
			Wheel:stoptweening();
			Wheel:addy(-600)
			Wheel:decelerate(0.1)
			Wheel:addy(10)
			Wheel:decelerate(0.1)
			Wheel:addy(0);

			local Banner = SCREENMAN:GetTopScreen():GetChild('Banner'); 
			Banner:stoptweening();
			Banner:addy(-600)
			Banner:decelerate(0.1)
			Banner:addy(10)
			Banner:decelerate(0.1)
			Banner:addy(0);

			local CDTitle = SCREENMAN:GetTopScreen():GetChild('CDTitle'); 
			CDTitle:stoptweening();
			CDTitle:addy(-590)
			CDTitle:decelerate(0.1)
			--CDTitle:addy(-400)
			--CDTitle:decelerate(0.1)
			--CDTitle:addy(0);
			--SCREENMAN:SystemMessage("StartSelectingSongMessageCommand");
		end;
		--[[GoBackSelectingSongMessageCommand=function(self)
			local Wheel = SCREENMAN:GetTopScreen():GetChild('MusicWheel'); 
			--cmd(stoptweening;y,200;decelerate,.1;y,-10;decelerate,.1;y,0);
			Wheel:stoptweening();
			Wheel:addy(200)
			Wheel:decelerate(0.1)
			Wheel:addy(-10)
			Wheel:decelerate(0.1)
			Wheel:addy(0);
			SCREENMAN:SystemMessage("GoBackSelectingSongMessageCommand");
		end;]]
		--OffCommand=cmd(stoptweening;decelerate,.1;y,-15;decelerate,.1;y,200);
	};

	LoadActor("_groupwheel")..{
		OnCommand=cmd(stoptweening;y,(SCREEN_HEIGHT/2);x,(SCREEN_WIDTH/2));
		GoBackSelectingGroupMessageCommand=cmd(x,-20;sleep,.1;linear,.2;x,(SCREEN_WIDTH/2));
		StartSelectingSongMessageCommand=cmd(stoptweening;x,(SCREEN_WIDTH/2);linear,.1;x,-150);
	};

	LoadActor("_CM/default.lua");	

	Def.ActorFrame {

		GoBackSelectingGroupMessageCommand=function(self) 
			--cmd(stoptweening;y,0;accelerate,.1;y,-10;decelerate,.1;y,200);
			--local self = SCREENMAN:GetTopScreen():GetChild('Musicself'); 
			self:stoptweening();
			self:addy(0);
			self:accelerate(0.1);
			self:addy(-10);
			self:decelerate(0.1);
			self:addy(600);
			--self:linear(0);
			--self:visible(0);
			--SCREENMAN:SystemMessage("GoBackSelectingGroupMessageCommand");
		end;
		StartSelectingSongMessageCommand=function(self)		
			--local self = SCREENMAN:GetTopScreen():GetChild('Musicself');
			self:stoptweening();
			self:decelerate(0.1);
			self:addy(-600);			
			self:addy(10);
			self:decelerate(0.1);
			self:addy(0);
			--self:linear(0);
			--self:visible(1);
			--SCREENMAN:SystemMessage("StartSelectingSongMessageCommand");
		end;

		LoadActor("../ScreenSelectMusicUtils/BannerFrame") .. {};
		--LoadActor("mods") .. {};
		
		LoadActor( "../ScreenSelectMusic underlay/Channel/_channel3" ) .. {};
		LoadActor( "../ScreenSelectMusic underlay/Channel/_channel2" ) .. {};

		LoadActor( "../ScreenSelectMusicUtils/Additionals") .. {};

		LoadActor( "efect info bottom" ) .. {
			OnCommand = cmd(x,SCREEN_CENTER_X-2;y,647;zoom,2;linear,0.5;y,418;zoom,1;diffusealpha,.6);
			OffCommand=cmd(sleep,.5;linear,0.5;addy,200;zoom,2);
		};

		Def.ActorFrame {
			Condition=GAMESTATE:IsHumanPlayer(PLAYER_1);
			InitCommand=function(self)
				local players = GAMESTATE:GetNumSidesJoined()
					self:x(players == 1 and SCREEN_LEFT+180 or SCREEN_LEFT+153);
					self:y(SCREEN_BOTTOM-37);
					self:diffusealpha(0);
				end;
			CurrentStepsP1ChangedMessageCommand=cmd(playcommand,"Update");
			OffCommand=cmd(stoptweening;hidden,1);

		LoadActor( "../ScreenSelectMusicSpecial overlay/brillo" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.55;diffusealpha,1;sleep,.4;linear,.3;diffusealpha,0;zoom,.8;zoomx,1);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.6;zoom,.55;diffusealpha,1;linear,.4;linear,.3;diffusealpha,0;zoom,.8;zoomx,1);
			};

		LoadActor( "../ScreenEvaluationStage background/O" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.05;diffusealpha,.065;linear,.35;zoom,.15;linear,.1;diffusealpha,0;zoomx,.2);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.64;zoom,.05;diffusealpha,.065;linear,.35;zoom,.15;linear,.1;diffusealpha,0;zoomx,.2);
			};

		LoadActor( "../ScreenEvaluationStage background/OrbeGlow" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.15;diffusealpha,.025;linear,.35;zoom,.5;linear,.1;diffusealpha,0);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.6;zoom,.15;diffusealpha,.025;linear,.35;zoom,.5;linear,.1;diffusealpha,0);
			};
		LoadActor( "../ScreenEvaluationStage background/O" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.25;diffusealpha,.025;linear,.35;zoom,.45;linear,.1;diffusealpha,0);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.6;zoom,.25;diffusealpha,.025;linear,.35;zoom,.45;linear,.1;diffusealpha,0);
			};
		};


		Def.ActorFrame {
			Condition=GAMESTATE:IsHumanPlayer(PLAYER_2);
			InitCommand=function(self)
				local players = GAMESTATE:GetNumSidesJoined()
					self:x(players == 1 and SCREEN_LEFT+180 or SCREEN_RIGHT-153);
					self:y(SCREEN_BOTTOM-37);
					self:diffusealpha(0);
				end;
			CurrentStepsP2ChangedMessageCommand=cmd(playcommand,"Update");
			OffCommand=cmd(stoptweening;hidden,1);

		LoadActor( "../ScreenSelectMusicSpecial overlay/brillo" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.55;diffusealpha,1;sleep,.4;linear,.3;diffusealpha,0;zoom,.8;zoomx,1);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.6;zoom,.55;diffusealpha,1;linear,.4;linear,.3;diffusealpha,0;zoom,.8;zoomx,1);
			};

		LoadActor( "../ScreenEvaluationStage background/O" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.05;diffusealpha,.065;linear,.35;zoom,.15;linear,.1;diffusealpha,0;zoomx,.2);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.64;zoom,.05;diffusealpha,.065;linear,.35;zoom,.15;linear,.1;diffusealpha,0;zoomx,.2);
			};

		LoadActor( "../ScreenEvaluationStage background/OrbeGlow" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.15;diffusealpha,.025;linear,.35;zoom,.5;linear,.1;diffusealpha,0);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.6;zoom,.15;diffusealpha,.025;linear,.35;zoom,.5;linear,.1;diffusealpha,0);
			};
		LoadActor( "../ScreenEvaluationStage background/O" ) .. {
			UpdateCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;zoom,.25;diffusealpha,.025;linear,.35;zoom,.45;linear,.1;diffusealpha,0);
			OnCommand=cmd(stoptweening;blend,"BlendMode_Add";diffusealpha,0;sleep,.6;zoom,.25;diffusealpha,.025;linear,.35;zoom,.45;linear,.1;diffusealpha,0);
			};
		};

	};
	
	LoadActor( "efect info top" ) .. {
		OnCommand = cmd(x,SCREEN_CENTER_X-2;y,-143;zoom,2;linear,0.5;y,78;zoom,1);
		OffCommand=cmd(sleep,.5;linear,0.5;addy,-130;zoom,2);
	};	

	


	LoadActor("../Common Utils/CenterScript.lua") .. {
		InitCommand=cmd(addy,-51);
	};
	
	--[[LoadFont("blaster") .. {
		Condition=false;
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-60;diffuse,.93,.93,.93,1;zoom,.75;shadowlength,2);
		OnCommand=function(self)
			local long = GAMESTATE:GetCurrentSong():IsLong();
			local marathon = GAMESTATE:GetCurrentSong():IsMarathon();
			local index = GAMESTATE:GetStageIndex();

		if index == 1 and long or marathon then self:hidden(1) else self:hidden(0) end
		if index == 0 then self:hidden(0) end end;

		CurrentSongChangedMessageCommand=function(self)
			local long = GAMESTATE:GetCurrentSong():IsLong();
			local marathon = GAMESTATE:GetCurrentSong():IsMarathon();
			local text = "ESTA CANCION CONSUMIRA 2 STAGES";

		if long or marathon then
			self:finishtweening();
			self:hidden(0);
			self:settext(text);
			self:playcommand("Show");
	
		else
			self:hidden(1)end end;

		ShowCommand=cmd(finishtweening;diffusealpha,0;sleep,1;linear,.7;diffusealpha,1;sleep,2.3;linear,.7;diffusealpha,0;queuecommand,"Show");
		OffCommand=cmd(stoptweening;hidden,1);
	};]]
	
	--sortorder text
	--[[LoadFont("blaster") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y-60;diffuse,.93,.93,.93,1;zoom,.75;shadowlength,2);
		SetCommand=function(self)
			self:settext( tostring(GAMESTATE:GetSortOrder()) )
		end;
		SortOrderChangedMessageCommand=cmd(playcommand,"Set");
	};
	]]


	LoadActor("../ScreenSelectMusicUtils/Press" ) .. {};

	LoadActor("../ScreenSelectMusicUtils/PlayerJoin_Effect" ) .. {--los commands estan en su archivo, no aqui
		Condition=GAMESTATE:GetNumPlayersEnabled() == 1;
		OnCommand=cmd();
		OffCommand=cmd(finishtweening;diffusealpha,0);
	};
	
	Def.Quad {
		InitCommand=cmd(stretchto,SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM;diffuse,color("#000000");draworder,99999);
		OnCommand=cmd(diffusealpha,0);
		OffCommand=cmd(sleep,0.7;linear,0.3;diffusealpha,1);
	};
};

cx = (SCREEN_WIDTH/2);
cy = (SCREEN_HEIGHT/2);

-- Columna de Códigos P1
if GAMESTATE:IsSideJoined(PLAYER_1) then
t[#t+1] = GetCodesColumn( PLAYER_1 )..{
	InitCommand=cmd(x,24);
	OffCommand=cmd(visible,false);
};
end;

-- Columna de Códigos P2
if GAMESTATE:IsSideJoined(PLAYER_2) then
t[#t+1] = GetCodesColumn( PLAYER_2 )..{
	InitCommand=cmd(x,SCREEN_RIGHT-24);
	OffCommand=cmd(visible,false);
};
end;


if GAMESTATE:IsSideJoined(PLAYER_1) then
t[#t+1] = GetCommandWindow( PLAYER_1 )..{
	InitCommand=cmd(x,-100;y,cy-20;zoom,.5);
	OptionsListOpenedMessageCommand=function(self,params)
		if params.Player == PLAYER_1 then
			(cmd(finishtweening;x,-100;decelerate,.1;x,110;decelerate,.2;x,100))(self);
		end;
	end;
	OptionsListClosedMessageCommand=function(self,params)
		if params.Player == PLAYER_1 then
			(cmd(stoptweening;x,100;decelerate,.1;x,110;decelerate,.1;x,-100))(self);
		end;
	end;
	CodeMessageCommand=function(self,params)
	if GAMESTATE:IsBasicMode() then return; end;
	if  params.Name == 'OptionList' and params.PlayerNumber == PLAYER_1 then
		SCREENMAN:GetTopScreen():GetMusicWheel():Move(0);
		SCREENMAN:GetTopScreen():OpenOptionsList(PLAYER_1);
	end;
	end;
}
end;

if GAMESTATE:IsSideJoined(PLAYER_2) then
t[#t+1] = GetCommandWindow( PLAYER_2 )..{
	InitCommand=cmd(x,SCREEN_RIGHT+100;y,cy-20;zoom,.5);
	OptionsListOpenedMessageCommand=function(self,params)
		if params.Player == PLAYER_2 then
			(cmd(finishtweening;x,SCREEN_RIGHT+100;decelerate,.1;x,SCREEN_RIGHT-110;decelerate,.2;x,SCREEN_RIGHT-100))(self);
		end;
	end;
	OptionsListClosedMessageCommand=function(self,params)
		if params.Player == PLAYER_2 then
			(cmd(stoptweening;x,SCREEN_RIGHT-100;decelerate,.1;x,SCREEN_RIGHT-110;decelerate,.1;x,SCREEN_RIGHT+100))(self);
		end;
	end;
	CodeMessageCommand=function(self,params)
	if GAMESTATE:IsBasicMode() then return; end;
	if  params.Name == 'OptionList' and params.PlayerNumber == PLAYER_2 then
		SCREENMAN:GetTopScreen():GetMusicWheel():Move(0);
		SCREENMAN:GetTopScreen():OpenOptionsList(PLAYER_2);
	end;
	end;
}
end;

return t;

