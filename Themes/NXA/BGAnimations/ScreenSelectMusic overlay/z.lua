--opciones como en la piupc :P ahora s� sirven, bendito lua xD
local function PlayerOptions(pn)
	--no player, no job
	if not GAMESTATE:IsPlayerEnabled(pn) then return Def.Actor {} end
	
	local Playerstate = GAMESTATE:GetPlayerState(pn)
	local options = Playerstate:GetPlayerOptions('ModsLevel_Preferred')
	options = string.lower(options)
	local SepOption = split(", ", options)
	local speed = 1
	
	--puedes declarar variables en una sola l�nea
	--los valores ser�n respectivamente:
	local rand, mirr, vnsh, nons = "r", "m", "v", "n"
	
	for i=1,#SepOption do
		if string.find(SepOption[i],"%dx") ~= nil then
			--tomar el primer caracter del speedmod y volverlo n�mero
			speed = tonumber(string.sub(SepOption[i],0,1))
			--no hay gr�fico speed de 5 para arriba...
			if speed > 4 then
				speed = 4
			end
		end
		if string.find(SepOption[i],"shuffle") or string.find(SepOption[i],"supershuffle") then
			rand = "R"
		end
		if string.find(SepOption[i],"left") or string.find(SepOption[i],"right") or string.find(SepOption[i],"mirror") then
			mirr = "M"
		end
		if string.find(SepOption[i],"hidden") or string.find(SepOption[i],"sudden") or string.find(SepOption[i],"randomvanish") then
			vnsh = "V"
		end
		if string.find(SepOption[i],"stealth") then
			nons = "N"
		end
	end
	
	--custom fonts
	return LoadFont("_SpecialTyphos")..{
		Text=string.format("%d\n%s\n%s\n%s\n%s",speed,rand,mirr,vnsh,nons);
		InitCommand=cmd(y,184;shadowlength,0;zoom,1.12;vertalign,top);
		PlayerOptionsChangedP2MessageCommand=function(self)

			local text = string.format("%d\n%s\n%s\n%s\n%s",speed,rand,mirr,vnsh,nons)

		self:settext(text) end;

	}
end

local t = Def.ActorFrame {}


t[#t+1] = PlayerOptions(PLAYER_1)..{
	InitCommand=cmd(x,28);
}
t[#t+1] = PlayerOptions(PLAYER_2)..{
	InitCommand=cmd(x,SCREEN_RIGHT-28);
}

return t