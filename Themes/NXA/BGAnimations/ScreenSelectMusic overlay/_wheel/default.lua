local t = Def.ActorFrame {}

--Posicion del wheel, menos 100 pixeles desde la base--
--ypos = SCREEN_HEIGHT-100;
--per = .8;

--Wheel--
--t[#t+1] = LoadActor("Wheel_FIESTA_EX.lua")


--[[t[#t+1] = Def.ActorProxy {
	BeginCommand=function(self) 
		local Wheel = SCREENMAN:GetTopScreen():GetChild('MusicWheel'); 
		self:SetTarget(Wheel); 
		end;
}

]]

--Sombra negra--
--[[t[#t+1] = LoadActor("blc.png")..{
	OnCommand=cmd(stoptweening;x,cx;y,ypos);
}]]

--Resplandor blanco--
--[[t[#t+1] = Def.Quad {
	InitCommand=cmd(x,cx;y,ypos;zoomto,173,120;diffuse,color("1,1,1,1"));
	OnCommand=cmd(stoptweening;diffusealpha,0);
	StartSelectingStepsMessageCommand=cmd(stoptweening;diffusealpha,0;accelerate,.01;diffusealpha,1;accelerate,.12;diffusealpha,0);
	CurrentSongChangedMessageCommand=cmd(stoptweening;diffusealpha,0;sleep,.02;linear,.04;diffusealpha,1;decelerate,.12;diffusealpha,0);
};

----Lucesss----
--Izquierdas--
for i=1,2 do
	t[#t+1] = LoadActor("light.png")..{
	InitCommand=cmd(y,ypos;x,cx-81;blend,'BlendMode_Add';zoom,.675;rotationy,180);
	OnCommand=function(self)
		if i==1 then
		(cmd(diffusealpha,0;sleep,.2;decelerate,.2;diffusealpha,1;sleep,.1;diffuseshift;effectcolor2,color("1,1,1,1");effectcolor1,color("1,1,1,.8");effectperiod,per))(self);
		elseif i==2 then
		self:diffusealpha(0);
		end;
	end;
	CurrentSongChangeMessageCommand=function(self)
		if i==1 then
		(cmd(stoptweening;stopeffect;diffusealpha,1;sleep,.25;diffuseshift;effectcolor2,color("1,1,1,1");effectcolor1,color("1,1,1,.8");effectperiod,per))(self);
		end;
	end;
	PreviousSongMessageCommand=function(self)
		if i==2 then
		(cmd(stoptweening;zoom,.675;diffusealpha,1;accelerate,.08;zoomx,2.3;decelerate,.08;zoomx,.675;decelerate,.02;diffusealpha,0))(self);
		end;
	end;
	GoBackSelectingSongMessageCommand=cmd(playcommand,'On');
	StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.2;diffusealpha,0);
	OffCommand=cmd(stoptweening;decelerate,.2;diffusealpha,0);
};
end;

--Derechas--
for j=1,2 do
	t[#t+1] = LoadActor("light.png")..{
	InitCommand=cmd(y,ypos;x,cx+81;blend,'BlendMode_Add';zoom,.675);
	OnCommand=function(self)
		if j==1 then
		(cmd(diffusealpha,0;sleep,.2;decelerate,.2;diffusealpha,1;sleep,.1;diffuseshift;effectcolor2,color("1,1,1,1");effectcolor1,color("1,1,1,.8");effectperiod,per))(self);
		elseif j==2 then
		self:diffusealpha(0);
		end;
	end;
	CurrentSongChangeMessageCommand=function(self)
		if i==1 then
		(cmd(stoptweening;stopeffect;diffusealpha,1;sleep,.25;diffuseshift;effectcolor2,color("1,1,1,1");effectcolor1,color("1,1,1,.8");effectperiod,per))(self);
		end;
	end;
	NextSongMessageCommand=function(self)
		if j==2 then
		(cmd(stoptweening;zoom,.675;diffusealpha,1;accelerate,.08;zoomx,2.3;decelerate,.08;zoomx,.675;decelerate,.02;diffusealpha,0))(self);
		end;
	end;
	GoBackSelectingSongMessageCommand=cmd(playcommand,'On');
	StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.2;diffusealpha,0);
	OffCommand=cmd(stoptweening;decelerate,.2;diffusealpha,0);
};
end;

--Frame Fiesta Ex--
t[#t+1] = LoadActor("newbox")..{
	OnCommand=cmd(stoptweening;x,cx;y,ypos;zoom,.675);	
}

--Flechas--
t[#t+1] = LoadActor("ar")..{
	OnCommand=cmd(stoptweening;x,cx;y,ypos;zoom,.68;diffusealpha,0;blend,'BlendMode_Add';sleep,.2;decelerate,.2;diffusealpha,1;sleep,.1;queuecommand,'Loop');
	LoopCommand=cmd(stoptweening;linear,.4;zoom,.69;linear,.4;zoom,.63;queuecommand,'Loop');
	StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.2;zoom,.63;diffusealpha,0);
	GoBackSelectingSongMessageCommand=cmd(playcommand,'On');
	CurrentSongChangedMessageCommand=cmd(stoptweening;diffusealpha,1;linear,.1;zoom,.7;diffusealpha,0;sleep,.1;zoom,.63;linear,.05;diffusealpha,1;queuecommand,'Loop');
}

--Back song num--
t[#t+1] = LoadActor("back_num")..{
	OnCommand=cmd(stoptweening;x,cx;y,ypos-55;zoom,.68;diffusealpha,0;sleep,0;decelerate,.1;diffusealpha,1;sleep,.1);
};

function GetCurrentSongNumber()
	local cursong = GAMESTATE:GetCurrentSong();
	
	for i=1,#AllSongs do
		if AllSongs[i] == cursong then
		return i;
		end;
	end;
end;

function GetMeMyIndex(i)
	local temp = SCREENMAN:GetTopScreen():GetMusicWheel():GetCurrentIndex()+1;
	
	local s1 = string.format("%i", temp /100);
	local s2 = string.format("%i", (temp - s1*100)/10 );
	local s3 = string.format("%i", (temp - s1*100 - s2*10));
	
	local num = {};
	num = { s1, s2, s3 };
	return num[i];

end;


t[#t+1] = Def.ActorFrame {
	OnCommand=cmd(playcommand,'Update');
	CurrentSongChangedMessageCommand=cmd(playcommand,'Update');
	children = {
	--1st digit
	LoadFont("SongNumber")..{
		UpdateCommand=cmd(stoptweening;settext,GetMeMyIndex(1);zoomx,.6;zoomy,.35;diffusealpha,1);
		OnCommand=cmd(stoptweening;x,cx-12;y,ypos-58;zoom,0;diffusealpha,0;sleep,.14;decelerate,.02;zoomx,.6;zoomy,.35;diffusealpha,1);
		StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
		GoBackSelectingGroupMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
		GoBackSelectingSongMessageCommand=cmd(playcommand,'On');
		StartSelectingSongMessageCommand=cmd(playcommand,'On');
		OffCommand=cmd(stoptweening);
	};
	--2nd digit
	LoadFont("SongNumber")..{
		UpdateCommand=cmd(stoptweening;settext,GetMeMyIndex(2);zoomx,.6;zoomy,.35;diffusealpha,1);
		OnCommand=cmd(stoptweening;x,cx;y,ypos-58;zoom,0;diffusealpha,0;sleep,.16;decelerate,.02;zoomx,.6;zoomy,.35;diffusealpha,1);
		StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
		GoBackSelectingGroupMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
		GoBackSelectingSongMessageCommand=cmd(playcommand,'On');
		StartSelectingSongMessageCommand=cmd(playcommand,'On');
		OffCommand=cmd(stoptweening);
	};
	--3er digit
	LoadFont("SongNumber")..{
		UpdateCommand=cmd(stoptweening;settext,GetMeMyIndex(3);zoomx,.6;zoomy,.35;diffusealpha,1);
		OnCommand=cmd(stoptweening;x,cx+12;y,ypos-58;zoom,0;diffusealpha,0;sleep,.18;decelerate,.02;zoomx,.6;zoomy,.35;diffusealpha,1);
		StartSelectingStepsMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
		GoBackSelectingGroupMessageCommand=cmd(stoptweening;decelerate,.08;zoom,0;diffusealpha,0);
		GoBackSelectingSongMessageCommand=cmd(playcommand,'On');
		StartSelectingSongMessageCommand=cmd(playcommand,'On');
		OffCommand=cmd(stoptweening);
	};
};
};]]
	
return t;