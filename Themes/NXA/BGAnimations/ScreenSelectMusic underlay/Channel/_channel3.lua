local t = Def.ActorFrame {
	
	LoadActor( "bottom" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,430;blend,"BlendMode_Add";playcommand,"Color";diffusealpha,0;fadebottom,.05;hidden,1);
		OffCommand = cmd(hidden,1);
		OnCommand=cmd(stoptweening;diffusealpha,0;zoom,2;linear,.5;zoom,1;sleep,.3;linear,.3;diffusealpha,.8;playcommand,"Color");
		CurrentSongChangedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,.4;linear,.2;diffusealpha,.8;playcommand,"Color");
		ColorCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();
		
			if song == "newtunes" or song == "1new tunes" or song == "new tunes" or song == "1 NEW TUNES" or song == "1NEW TUNES" or song == "NEW TUNES" then
				self:diffuse(.2,.5,1,1) self:diffusealpha(.4)
			elseif song == "piuoriginal" or song == "2piu original" or song == "piu original" or song == "piu originals" or song == "piuoriginals" or song == "pump it up originals" or song == "pump it up original" or song == "4 PUMP IT UP ORIGINAL" or song == "4PUMP IT UP ORIGINAL" or song == "PUMP IT UP ORIGINAL" or song == "Banya" or song == "4BANYA" or song == "4PIU ORIGINAL" or song == "4 PIU ORIGINAL" or song == "PIU ORIGINAL"
			 or song == "2PUMP IT UP ORIGINAL" or song == "2Banya" or song == "2BANYA" or song == "2PIU ORIGINAL" or song == "2 PIU ORIGINAL" or song == "2 PUMP IT UP ORIGINAL" then
				self:diffuse(1,.8,0,0) self:diffusealpha(.4)
			elseif song == "kpop" or song == "k pop" or song == "k-pop" or song == "3k-pop" or song == "k_pop" or song == "2 K-POP" or song == "2K-POP" or song == "3K-POP" or song == "3 K-POP" or song == "K-POP" then
				self:diffuse(1,0,.65,.9); self:diffusealpha(.4)
			elseif song == "4pop" or song == "pop channel" or song == "pop" or song == "3 POP" or song == "3POP" or song == "POP" or song == "4POP" or song == "4 POP" or song == "POP CHANNEL" or song == "4pop" or song == "4 pop" then
				self:diffuse(.2,.8,.4,1); self:diffusealpha(.4)
			elseif song == "another" or song == "another channel"  or song == "7 ANOTHER STEP" or song == "7ANOTHER STEP" or song == "ANOTHER STEP" then
				self:diffuse(.9,0,0,1) self:diffusealpha(.4)
			elseif song == "5full songs" or song == "full channel" or song == "full songs" or song == "full songs channel" or song == "fullsongs" or song == "fullsong" or song == "5 FULL SONG" or song == "5FULL SONG" or song == "FULL SONG" or song == "FULLSONG" then
				self:diffuse(.9,.2,.9,1) self:diffusealpha(.4)
			elseif song == "6remix" or song == "remix channel" or song == "remix" or song == "6 REMIX" or song == "6REMIX" or song == "REMIX" then
				self:diffuse(.25,1,1,.75) self:diffusealpha(.4)
			else
				self:diffuse(getgroupcolor()); self:diffusealpha(.4) end
		end;

	};
	
};

return t;

