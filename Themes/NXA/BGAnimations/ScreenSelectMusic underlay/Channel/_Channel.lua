local t = Def.ActorFrame {

	LoadActor( "bg aux top23" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;zoomx,2.4);
		OffCommand=cmd(sleep,.5;linear,0.875;addy,-200;zoom,2;zoomx,2.4);
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();
		
			if song == "newtunes" or song == "1new tunes" or song == "new tunes" or song == "1 NEW TUNES" or song == "1NEW TUNES" or song == "NEW TUNES" then
				self:diffuse(.2,.5,1,1)
			elseif song == "piuoriginal" or song == "2piu original" or song == "piu original" or song == "piu originals" or song == "piuoriginals" or song == "pump it up originals" or song == "pump it up original" or song == "4 PUMP IT UP ORIGINAL" or song == "4PUMP IT UP ORIGINAL" or song == "PUMP IT UP ORIGINAL" or song == "Banya" or song == "4BANYA" or song == "4PIU ORIGINAL" or song == "4 PIU ORIGINAL" or song == "PIU ORIGINAL"
			 or song == "2PUMP IT UP ORIGINAL" or song == "2Banya" or song == "2BANYA" or song == "2PIU ORIGINAL" or song == "2 PIU ORIGINAL" or song == "2 PUMP IT UP ORIGINAL" then
				self:diffuse(1,.7,0,1)
			elseif song == "kpop" or song == "k pop" or song == "k-pop" or song == "3k-pop" or song == "k_pop" or song == "2 K-POP" or song == "2K-POP" or song == "3K-POP" or song == "3 K-POP" or song == "K-POP" then
				self:diffuse(1,0,.65,1);
			elseif song == "4pop" or song == "pop channel" or song == "pop" or song == "3 POP" or song == "3POP" or song == "POP" or song == "4POP" or song == "4 POP" or song == "POP CHANNEL" or song == "4pop" or song == "4 pop" then
				self:diffuse(.2,.8,.4,1);
			elseif song == "another" or song == "another channel"  or song == "7 ANOTHER STEP" or song == "7ANOTHER STEP" or song == "ANOTHER STEP" then
				self:diffuse(.9,0,0,1)
			elseif song == "5full songs" or song == "full channel" or song == "full songs" or song == "full songs channel" or song == "fullsongs" or song == "fullsong" or song == "5 FULL SONG" or song == "5FULL SONG" or song == "FULL SONG" or song == "FULLSONG" then
				self:diffuse(.7,.2,1,1)
			elseif song == "6remix" or song == "remix channel" or song == "remix" or song == "6 REMIX" or song == "6REMIX" or song == "REMIX" then
				self:diffuse(.25,1,1,1);
			else
				self:diffuse(getgroupcolor()); end
		end;

		OnCommand = cmd(y,-143;linear,0.5;y,78;zoom,1);
	};

	LoadActor( "bg aux top23" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;zoomx,2.4);
		OffCommand=cmd(sleep,.5;linear,0.875;addy,-200;zoom,2;zoomx,2.4);
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();
		
			if song == "newtunes" or song == "1new tunes" or song == "new tunes" or song == "1 NEW TUNES" or song == "1NEW TUNES" or song == "NEW TUNES" then
				self:diffuse(.2,.5,1,.5)
			elseif song == "piuoriginal" or song == "2piu original" or song == "piu original" or song == "piu originals" or song == "piuoriginals" or song == "pump it up originals" or song == "pump it up original" or song == "4 PUMP IT UP ORIGINAL" or song == "4PUMP IT UP ORIGINAL" or song == "PUMP IT UP ORIGINAL" or song == "Banya" or song == "4BANYA" or song == "4PIU ORIGINAL" or song == "4 PIU ORIGINAL" or song == "PIU ORIGINAL"
			 or song == "2PUMP IT UP ORIGINAL" or song == "2Banya" or song == "2BANYA" or song == "2PIU ORIGINAL" or song == "2 PIU ORIGINAL" or song == "2 PUMP IT UP ORIGINAL" then
				self:diffuse(1,.7,0,.5)
			elseif song == "kpop" or song == "k pop" or song == "k-pop" or song == "3k-pop" or song == "k_pop" or song == "2 K-POP" or song == "2K-POP" or song == "3K-POP" or song == "3 K-POP" or song == "K-POP" then
				self:diffuse(1,0,.65,.5);
			elseif song == "4pop" or song == "pop channel" or song == "pop" or song == "3 POP" or song == "3POP" or song == "POP" or song == "4POP" or song == "4 POP" or song == "POP CHANNEL" or song == "4pop" or song == "4 pop" then
				self:diffuse(.2,.8,.4,.5);
			elseif song == "another" or song == "another channel"  or song == "7 ANOTHER STEP" or song == "7ANOTHER STEP" or song == "ANOTHER STEP" then
				self:diffuse(.9,0,0,.5)
			elseif song == "5full songs" or song == "full channel" or song == "full songs" or song == "full songs channel" or song == "fullsongs" or song == "fullsong" or song == "5 FULL SONG" or song == "5FULL SONG" or song == "FULL SONG" or song == "FULLSONG" then
				self:diffuse(.7,.2,1,.5)
			elseif song == "6remix" or song == "remix channel" or song == "remix" or song == "6 REMIX" or song == "6REMIX" or song == "REMIX" then
				self:diffuse(.25,1,1,.5);
			else
				self:diffuse(getgroupcolor()); end
		end;
		OnCommand = cmd(blend,"BlendMode_Add";y,-143;linear,0.5;y,78;zoom,1;croptop,.23;diffusealpha,.5);
	};


	Def.ActorFrame {
		InitCommand=cmd(x,SCREEN_CENTER_X);
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();
		
			if song == "newtunes" or song == "1new tunes" or song == "new tunes" or song == "1 NEW TUNES" or song == "1NEW TUNES" or song == "NEW TUNES" then
				self:diffuse(.2,.5,1,1)
			elseif song == "piuoriginal" or song == "2piu original" or song == "piu original" or song == "piu originals" or song == "piuoriginals" or song == "pump it up originals" or song == "pump it up original" or song == "4 PUMP IT UP ORIGINAL" or song == "4PUMP IT UP ORIGINAL" or song == "PUMP IT UP ORIGINAL" or song == "Banya" or song == "4BANYA" or song == "4PIU ORIGINAL" or song == "4 PIU ORIGINAL" or song == "PIU ORIGINAL"
			 or song == "2PUMP IT UP ORIGINAL" or song == "2Banya" or song == "2BANYA" or song == "2PIU ORIGINAL" or song == "2 PIU ORIGINAL" or song == "2 PUMP IT UP ORIGINAL" then
				self:diffuse(1,.8,0,1)
			elseif song == "kpop" or song == "k pop" or song == "k-pop" or song == "3k-pop" or song == "k_pop" or song == "2 K-POP" or song == "2K-POP" or song == "3K-POP" or song == "3 K-POP" or song == "K-POP" then
				self:diffuse(1,0,.65,1);
			elseif song == "4pop" or song == "pop channel" or song == "pop" or song == "3 POP" or song == "3POP" or song == "POP" or song == "4POP" or song == "4 POP" or song == "POP CHANNEL" or song == "4pop" or song == "4 pop" then
				self:diffuse(.2,.8,.4,1);
			elseif song == "another" or song == "another channel"  or song == "7 ANOTHER STEP" or song == "7ANOTHER STEP" or song == "ANOTHER STEP" then
				self:diffuse(.9,0,0,1)
			elseif song == "5full songs" or song == "full channel" or song == "full songs" or song == "full songs channel" or song == "fullsongs" or song == "fullsong" or song == "5 FULL SONG" or song == "5FULL SONG" or song == "FULL SONG" or song == "FULLSONG" then
				self:diffuse(.7,.2,1,1)
			elseif song == "6remix" or song == "remix channel" or song == "remix" or song == "6 REMIX" or song == "6REMIX" or song == "REMIX" then
				self:diffuse(.25,1,1,1);
			else
				self:diffuse(getgroupcolor()); end
		end;
	LoadActor( "bg aux bottom" ) .. {
		OnCommand = cmd(y,647;zoom,2;linear,0.5;y,418;zoom,1);
		OffCommand=cmd(sleep,.5;linear,0.5;addy,200;zoom,2);
	};

	LoadActor( "bg aux bottomsz" ) .. {
		OnCommand = cmd(y,647;zoom,2;linear,0.5;y,418;zoom,1);
		OffCommand=cmd(sleep,.5;linear,0.5;addy,200;zoom,2);
	};

	LoadActor( "bg aux bottom" ) .. {
		OnCommand = cmd(blend,"BlendMode_Add";y,647;zoom,2;linear,0.5;y,418;zoom,1;cropbottom,.375;diffusealpha,.9);
		OffCommand=cmd(sleep,.5;linear,0.5;addy,200;zoom,2);
	};
};

	LoadActor( "base" ) .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+3;zoom,2;linear,0.5;zoomx,.9;zoomy,1.01);
		OffCommand = cmd( sleep,0.5;linear,0.5; zoom,1.4;diffusealpha,0 );
	};

	LoadActor( "bg aux circle" ) .. {
		OnCommand = cmd(x,320;y,647;zoom,2;linear,0.5;y,418;zoom,1;diffuse,.5,.5,.5,.3);
		OffCommand=cmd(sleep,.5;linear,0.5;addy,200;zoom,2);
	};


	--[[LoadActor( "Channel/Arcadetop" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X);
		OffCommand=cmd(sleep,.55;linear,0.5;addy,-200;zoom,2);
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();
		
			if song == "newtunes" or song == "1new tunes" or song == "new tunes" or song == "1 NEW TUNES" or song == "1NEW TUNES" or song == "NEW TUNES" then
				self:hidden(0) else self:hidden(1) end
		end;
		OnCommand = cmd(y,-143;linear,0.5;y,68;zoom,1);
	};

	LoadActor( "Channel/BottomArc" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X);
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();
		
			if song == "newtunes" or song == "1new tunes" or song == "new tunes" or song == "1 NEW TUNES" or song == "1NEW TUNES" or song == "NEW TUNES" then
				self:hidden(0) else self:hidden(1) end
		end;
		OnCommand = cmd(y,673;zoom,2.55;linear,0.5;y,428;zoom,1.275);
		OffCommand=cmd(sleep,.5;linear,0.5;addy,200;zoom,2.55);
	};

	LoadActor( "Channel/ArcadeGlow" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X);
		CurrentSongChangedMessageCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();
		
			if song == "newtunes" or song == "1new tunes" or song == "new tunes" or song == "1 NEW TUNES" or song == "1NEW TUNES" or song == "NEW TUNES" then
				self:hidden(0) else self:hidden(1) end

			(cmd(finishtweening;diffusealpha,0;cropbottom,1;sleep,.3;linear,.2;cropbottom,-1;diffusealpha,1))(self)
		end;
		OnCommand = cmd(diffusealpha,0;y,647;zoom,2;linear,0.5;y,402;zoom,1;sleep,.1;linear,.2;diffusealpha,1);
		OffCommand=cmd(hidden,1);
	};]]

	Def.ActorFrame {
		InitCommand=cmd(x,SCREEN_CENTER_X;diffuse,getgroupcolor();diffusealpha,0);
		OffCommand=cmd(hidden,1);

	LoadActor( "bg aux top" ) .. {
		OnCommand = cmd(blend,"BlendMode_Add";diffusealpha,0;y,-143;linear,0.5;y,78;zoom,1;sleep,.7;diffusealpha,1;linear,.5;diffusealpha,0);
		GroupChangedMessageCommand = cmd(finishtweening;diffusealpha,1;diffuse,getgroupcolor();linear,0.5;diffusealpha,0 );
	};

	LoadActor( "bg aux bottom" ) .. {
		OnCommand = cmd(blend,"BlendMode_Add";diffusealpha,0;y,647;zoom,2;linear,0.5;y,418;zoom,1;sleep,.7;diffusealpha,1;linear,.5;diffusealpha,0);
		GroupChangedMessageCommand = cmd(finishtweening;diffusealpha,1;diffuse,getgroupcolor();linear,0.5;diffusealpha,0 );
	};
};

};
return t;
