local t = Def.ActorFrame {
	LoadActor( "glow" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X+2;y,SCREEN_CENTER_Y+1;blend,"BlendMode_Add";diffusealpha,0);
		OffCommand = cmd(hidden,1);
		OnCommand=cmd(stoptweening;playcommand,"Color";diffusealpha,0;cropbottom,1;zoom,2;linear,.5;zoom,1;zoomx,1.01;sleep,.3;linear,.3;cropbottom,-1;diffusealpha,.85);
		CurrentSongChangedMessageCommand=cmd(finishtweening;playcommand,"Color";diffusealpha,0;cropbottom,1;sleep,.4;linear,.2;cropbottom,-1;diffusealpha,.85);
		ColorCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();
		
			if song == "newtunes" or song == "1new tunes" or song == "new tunes" or song == "1 NEW TUNES" or song == "1NEW TUNES" or song == "NEW TUNES" then
				self:diffuse(.0,.4,1,1)
			elseif song == "piuoriginal" or song == "2piu original" or song == "piu original" or song == "piu originals" or song == "piuoriginals" or song == "pump it up originals" or song == "pump it up original" or song == "4 PUMP IT UP ORIGINAL" or song == "4PUMP IT UP ORIGINAL" or song == "PUMP IT UP ORIGINAL" or song == "Banya" or song == "4BANYA" or song == "4PIU ORIGINAL" or song == "4 PIU ORIGINAL" or song == "PIU ORIGINAL"
			 or song == "2PUMP IT UP ORIGINAL" or song == "2Banya" or song == "2BANYA" or song == "2PIU ORIGINAL" or song == "2 PIU ORIGINAL" or song == "2 PUMP IT UP ORIGINAL" then
				self:diffuse(1,.8,0,.2)
			elseif song == "kpop" or song == "k pop" or song == "k-pop" or song == "3k-pop" or song == "k_pop" or song == "2 K-POP" or song == "2K-POP" or song == "3K-POP" or song == "3 K-POP" or song == "K-POP" then
				self:diffuse(1,0,.65,.9);
			elseif song == "4pop" or song == "pop channel" or song == "pop" or song == "3 POP" or song == "3POP" or song == "POP" or song == "4POP" or song == "4 POP" or song == "POP CHANNEL" or song == "4pop" or song == "4 pop" then
				self:diffuse(.1,.8,.4,1);
			elseif song == "another" or song == "another channel"  or song == "7 ANOTHER STEP" or song == "7ANOTHER STEP" or song == "ANOTHER STEP" then
				self:diffuse(.9,0,0,1)
			elseif song == "5full songs" or song == "full channel" or song == "full songs" or song == "full songs channel" or song == "fullsongs" or song == "fullsong" or song == "5 FULL SONG" or song == "5FULL SONG" or song == "FULL SONG" or song == "FULLSONG" then
				self:diffuse(.7,.2,1,1)
			elseif song == "6remix" or song == "remix channel" or song == "remix" or song == "6 REMIX" or song == "6REMIX" or song == "REMIX" then
				self:diffuse(.25,1,1,.7);
			else
				self:diffuse(getgroupcolor()); end
		end;
	};
	
	LoadActor( "glow2" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X+2;y,SCREEN_CENTER_Y+1;blend,"BlendMode_Add";diffusealpha,0);
		OffCommand = cmd(hidden,1);
		OnCommand=cmd(stoptweening;playcommand,"Color";diffusealpha,0;zoom,2;linear,.5;zoom,1;zoomx,1.01;sleep,.5;linear,.3;diffusealpha,.5);
		CurrentSongChangedMessageCommand=cmd(finishtweening;playcommand,"Color";diffusealpha,0;sleep,.6;linear,.2;diffusealpha,.5);
		ColorCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();
		
			if song == "newtunes" or song == "1new tunes" or song == "new tunes" or song == "1 NEW TUNES" or song == "1NEW TUNES" or song == "NEW TUNES" then
				self:diffuse(.2,.7,1,1)
			elseif song == "piuoriginal" or song == "2piu original" or song == "piu original" or song == "piu originals" or song == "piuoriginals" or song == "pump it up originals" or song == "pump it up original" or song == "4 PUMP IT UP ORIGINAL" or song == "4PUMP IT UP ORIGINAL" or song == "PUMP IT UP ORIGINAL" or song == "Banya" or song == "4BANYA" or song == "4PIU ORIGINAL" or song == "4 PIU ORIGINAL" or song == "PIU ORIGINAL"
			 or song == "2PUMP IT UP ORIGINAL" or song == "2Banya" or song == "2BANYA" or song == "2PIU ORIGINAL" or song == "2 PIU ORIGINAL" or song == "2 PUMP IT UP ORIGINAL" then
				self:diffuse(1,.8,0,.2)
			elseif song == "kpop" or song == "k pop" or song == "k-pop" or song == "3k-pop" or song == "k_pop" or song == "2 K-POP" or song == "2K-POP" or song == "3K-POP" or song == "3 K-POP" or song == "K-POP" then
				self:diffuse(1,0,.65,.9);
			elseif song == "4pop" or song == "pop channel" or song == "pop" or song == "3 POP" or song == "3POP" or song == "POP" or song == "4POP" or song == "4 POP" or song == "POP CHANNEL" or song == "4pop" or song == "4 pop" then
				self:diffuse(.1,.8,.4,1);
			elseif song == "another" or song == "another channel"  or song == "7 ANOTHER STEP" or song == "7ANOTHER STEP" or song == "ANOTHER STEP" then
				self:diffuse(.9,0,0,1)
			elseif song == "5full songs" or song == "full channel" or song == "full songs" or song == "full songs channel" or song == "fullsongs" or song == "fullsong" or song == "5 FULL SONG" or song == "5FULL SONG" or song == "FULL SONG" or song == "FULLSONG" then
				self:diffuse(.9,.2,.9,1)
			elseif song == "6remix" or song == "remix channel" or song == "remix" or song == "6 REMIX" or song == "6REMIX" or song == "REMIX" then
				self:diffuse(.25,1,1,.7);
			else
				self:diffuse(getgroupcolor()); end
		end;
	};

};

return t;

