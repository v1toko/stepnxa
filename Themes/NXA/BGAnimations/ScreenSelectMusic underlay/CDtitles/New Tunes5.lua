local t = Def.ActorFrame {

	LoadActor("NT/CD01") .. {
		InitCommand = cmd(animate,false;zoomy,.82;zoomx,.8);
		CurrentSongChangedMessageCommand=function(self)
			local name = GAMESTATE:GetCurrentSong():GetDisplayMainTitle()
			self:finishtweening()

			if name == "Blaze Emotion" then 
				self:diffusealpha(1)
				self:setstate(0)
			elseif name == "Cannon X.1" then
				self:diffusealpha(1)
				self:setstate(1)
			elseif name == "Chopsticks Challenge" then
				self:diffusealpha(1)
				self:setstate(2)
			elseif name == "LaLaLa" then
				self:diffusealpha(1)
				self:setstate(3)
			else self:diffusealpha(0) end

		end;
	};

	LoadActor("NT/CD02") .. {
		InitCommand = cmd(animate,false;zoomy,.82;zoomx,.8);
		CurrentSongChangedMessageCommand=function(self)
			local name = GAMESTATE:GetCurrentSong():GetDisplayMainTitle()
			self:finishtweening()

			if name == "Adios" then 
				self:diffusealpha(1)
				self:setstate(0)
			elseif name == "Slightly" then
				self:diffusealpha(1)
				self:setstate(1)
			elseif name == "I Am Your Girl" then
				self:diffusealpha(1)
				self:setstate(2)
			elseif name == "Only You" then
				self:diffusealpha(1)
				self:setstate(3)
			else self:diffusealpha(0) end

		end;
	};
	
	LoadActor("NT/CD03") .. {
		InitCommand = cmd(animate,false;zoomy,.82;zoomx,.8);
		CurrentSongChangedMessageCommand=function(self)
			local name = GAMESTATE:GetCurrentSong():GetDisplayMainTitle()
			self:finishtweening()

			if name == "Chocolate" then 
				self:diffusealpha(1)
				self:setstate(0)
			elseif name == "Forward" then
				self:diffusealpha(1)
				self:setstate(1)
			elseif name == "Uprock" then
				self:diffusealpha(1)
				self:setstate(2)
			elseif name == "Crazy" then
				self:diffusealpha(1)
				self:setstate(3)
			else self:diffusealpha(0) end

		end;
	};

	LoadActor("NT/CD04") .. {
		InitCommand = cmd(animate,false;zoomy,.82;zoomx,.8);
		CurrentSongChangedMessageCommand=function(self)
			local name = GAMESTATE:GetCurrentSong():GetDisplayMainTitle()
			self:finishtweening()

			if name == "Panuelito Rojo" or name == "Pa�uelito Rojo" then 
				self:diffusealpha(1)
				self:setstate(0)
			elseif name == "Procedimientos para Llegar a un Comun Acuerdo" then
				self:diffusealpha(1)
				self:setstate(1)
			elseif name == "Digan Lo Que Digan" then
				self:diffusealpha(1)
				self:setstate(2)
			elseif name == "Pump Breakers" then
				self:diffusealpha(1)
				self:setstate(3)
			else self:diffusealpha(0) end

		end;
	};

	LoadActor("NT/CD05") .. {
		InitCommand = cmd(animate,false;zoomy,.82;zoomx,.8);
		CurrentSongChangedMessageCommand=function(self)
			local name = GAMESTATE:GetCurrentSong():GetDisplayMainTitle()
			self:finishtweening()

			if name == "Change Myself" then 
				self:diffusealpha(1)
				self:setstate(0)
			elseif name == "Come On!" then
				self:diffusealpha(1)
				self:setstate(1)
			elseif name == "Bad Character" then
				self:diffusealpha(1)
				self:setstate(2)
			elseif name == "U" then
				self:diffusealpha(1)
				self:setstate(3)
			else self:diffusealpha(0) end

		end;
	};

	LoadActor("NT/CD06") .. {
		InitCommand = cmd(animate,false;zoomy,.82;zoomx,.8);
		CurrentSongChangedMessageCommand=function(self)
			local name = GAMESTATE:GetCurrentSong():GetDisplayMainTitle()
			self:finishtweening()

			if name == "Breakin' Love" then 
				self:diffusealpha(1)
				self:setstate(0)
			elseif name == "The People Didn't Know" then
				self:diffusealpha(1)
				self:setstate(1)
			elseif name == "Dj Otada" then
				self:diffusealpha(1)
				self:setstate(2)
			elseif name == "K.O.A ~Alice in Wonderland~" then
				self:diffusealpha(1)
				self:setstate(3)
			else self:diffusealpha(0) end

		end;
	};

	LoadActor("NT/CD07") .. {
		InitCommand = cmd(animate,false;zoomy,.82;zoomx,.8);
		CurrentSongChangedMessageCommand=function(self)
			local name = GAMESTATE:GetCurrentSong():GetDisplayMainTitle()
			self:finishtweening()

			if name == "My Dreams" then 
				self:diffusealpha(1)
				self:setstate(0)
			elseif name == "Toccata" then
				self:diffusealpha(1)
				self:setstate(1)
			elseif name == "Do It!" then
				self:diffusealpha(1)
				self:setstate(2)
			elseif name == "Dawn of the apocalypse" then
				self:diffusealpha(1)
				self:setstate(3)
			else self:diffusealpha(0) end

		end;
	};

	LoadActor("NT/CD08") .. {
		InitCommand = cmd(animate,false;zoomy,.82;zoomx,.8);
		CurrentSongChangedMessageCommand=function(self)
			local name = GAMESTATE:GetCurrentSong():GetDisplayMainTitle()
			self:finishtweening()

			if name == "Final Audition EP. 2-X" then 
				self:diffusealpha(1)
				self:setstate(0)
			else self:diffusealpha(0) end

		end;
	};
};
return t;
