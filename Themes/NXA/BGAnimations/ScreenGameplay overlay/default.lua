local t = Def.ActorFrame {
BeginCommand=function(self)
local pm = GAMESTATE:GetPlayMode()
if pm == "PlayMode_Rave" then self:stoptweening(); self:hidden(1); else self:hidden(0) end 
end;
	Def.ActorFrame {
		Condition=GAMESTATE:IsNormalMode();
		LoadActor("1") .. {
			Condition = GAMESTATE:GetCurrentStage() == 'Stage_Normal' and GAMESTATE:GetStageIndex() == 0;
			OnCommand=cmd(x,SCREEN_CENTER_X;y,23;draworder,100;);
		};

		LoadActor("2") .. {
			Condition = GAMESTATE:GetCurrentStage() == 'Stage_Normal' and GAMESTATE:GetStageIndex() == 1;
			OnCommand=cmd(x,SCREEN_CENTER_X;y,23;draworder,100;);
		};

		LoadActor("Final") .. {
			Condition = GAMESTATE:GetCurrentStage() == 'Stage_Normal' and GAMESTATE:GetStageIndex() == 2;
			OnCommand=cmd(x,SCREEN_CENTER_X;y,23;draworder,100;);
		};
		LoadActor("Bonus") .. {
			Condition = GAMESTATE:GetCurrentStage() == 'Stage_Normal' and GAMESTATE:GetStageIndex() == 3;
			OnCommand=cmd(x,SCREEN_CENTER_X;y,23;draworder,100;);
		};
			
	};

	
	Def.ActorFrame {
		Condition=not GAMESTATE:IsNormalMode();
		LoadActor("1") .. {
			Condition = GAMESTATE:GetCurrentStage() == 'Stage_Normal' and GAMESTATE:GetStageIndex() == 0;
			OnCommand=cmd(x,SCREEN_CENTER_X;y,23;draworder,100;);
		};

		LoadActor("2") .. {
			Condition = GAMESTATE:GetCurrentStage() == 'Stage_Normal' and GAMESTATE:GetStageIndex() == 1;
			OnCommand=cmd(x,SCREEN_CENTER_X;y,23;draworder,100;);
		};

		LoadActor("Final") .. {
			Condition = GAMESTATE:GetCurrentStage() == 'Stage_Normal' and GAMESTATE:GetStageIndex() == 2;
			OnCommand=cmd(x,SCREEN_CENTER_X;y,23;draworder,100;);
		};
		LoadActor("Bonus") .. {
			Condition = GAMESTATE:GetCurrentStage() == 'Stage_Normal' and GAMESTATE:GetStageIndex() == 3;
			OnCommand=cmd(x,SCREEN_CENTER_X;y,23;draworder,100;);
		};
			
	};

	--[[Def.ActorFrame {
		Condition=not GAMESTATE:IsNormalMode();
		LoadActor("1.png") .. {
			Condition = GAMESTATE:GetCurrentStage() == 'Stage_Normal' and GAMESTATE:GetStageIndex() == 0;
			OnCommand=cmd(x,SCREEN_CENTER_X;y,23;draworder,100;);
		};
		LoadActor("2.png") .. {
			Condition = GAMESTATE:GetCurrentStage() == 'Stage_Normal' and GAMESTATE:GetStageIndex() == 1;
			OnCommand=cmd(x,SCREEN_CENTER_X;y,23;draworder,100;);
		};
		LoadActor("Final.png") .. {
			Condition = GAMESTATE:GetCurrentStage() == 'Stage_Normal' and GAMESTATE:GetStageIndex() == 2;
			OnCommand=cmd(x,SCREEN_CENTER_X;y,23;draworder,100;);
		};
		
	};]]

	
	Def.ActorFrame {
		Condition=GAMESTATE:IsBrainShowerMode() and GAMESTATE:IsPlayerEnabled(PLAYER_2);
		LoadActor( "GoodAnswer" ) .. {
			OnCommand=cmd(x,math.floor(scale(0.75,0,1,SCREEN_LEFT,SCREEN_RIGHT));y,SCREEN_CENTER_Y-120;diffusealpha,0;zoom,0.8);
			GoodAnswerMessageCommand=cmd(finishtweening;linear,0.2;diffusealpha,1;zoom,0.9;linear,0.2;diffusealpha,0;linear,0;zoom,0.8);
		};
		LoadActor( "BadAnswer" ) .. {
			OnCommand=cmd(x,math.floor(scale(0.75,0,1,SCREEN_LEFT,SCREEN_RIGHT));y,SCREEN_CENTER_Y-120;diffusealpha,0;zoom,1.5);
			BadAnswerMessageCommand=cmd(finishtweening;linear,0.2;diffusealpha,1;zoom,1.6;linear,0.2;diffusealpha,0;linear,0;zoom,1.5);
		};
	};
	
	Def.ActorFrame {
		Condition=GAMESTATE:IsBrainShowerMode() and GAMESTATE:IsPlayerEnabled(PLAYER_1);
		LoadActor( "GoodAnswer" ) .. {
			OnCommand=cmd(x,math.floor(scale(0.25,0,1,SCREEN_LEFT,SCREEN_RIGHT));y,SCREEN_CENTER_Y-120;diffusealpha,0;zoom,0.8);
			GoodAnswerMessageCommand=cmd(finishtweening;linear,0.2;diffusealpha,1;zoom,0.9;linear,0.2;diffusealpha,0;linear,0;zoom,0.8);
		};
		LoadActor( "BadAnswer" ) .. {
			OnCommand=cmd(x,math.floor(scale(0.25,0,1,SCREEN_LEFT,SCREEN_RIGHT));y,SCREEN_CENTER_Y-120;diffusealpha,0;zoom,1.5);
			BadAnswerMessageCommand=cmd(finishtweening;linear,0.2;diffusealpha,1;zoom,1.6;linear,0.2;diffusealpha,0;linear,0;zoom,1.5);
		};
	};

	LoadActor( "EVENT.png" ) .. { 
		Condition = GAMESTATE:IsEventMode() or PREFSMAN:GetPreference( "ForceSMEventMode" );
		OnCommand = cmd(x,SCREEN_CENTER_X-1;y,23);
	};
	
	Def.Actor {
		BeatCrossedMessageCommand=function(self)
			--local b = GAMESTATE:GetSongBeat()
			
			--SCREENMAN:SystemMessage( "BEAT: " .. tostring(b) )
			--Trace( "BEAT: " )
			--SCREENMAN:GetTopScreen():ChangePlayerPosXY( PLAYER_1, SCREEN_CENTER_X/2, 0 )
			--SCREENMAN:GetTopScreen():ChangePlayerPosXY( PLAYER_2, -SCREEN_CENTER_X/2, 0 )
			
		end;
	};

}

return t

