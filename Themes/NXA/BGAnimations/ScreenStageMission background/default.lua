local t = Def.ActorFrame {
	LoadActor("Mission Stage.png") .. {
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;sleep,2);
	};
	
	LoadFont("Microgramma") .. {
		SetCommand=function(self)
			local sText = GAMESTATE:GetCurrentMission():GetFirstGoal()
			
			if sText == 'x' or sText == 'X' then		
				self:settext( "X" )
				self:playcommand( "ShowCenter" )
			else
				self:settext( sText )
				self:playcommand( "Show" )
			end
		end;
		OnCommand=cmd(playcommand,"Set");
		ShowCommand=cmd(x,75;y,190;horizalign,left;shadowlength,2;zoom,0.4;wrapwidthpixels,1200;diffusealpha,0;sleep,0.4;diffusealpha,1);
		ShowCenterCommand=cmd(x,SCREEN_CENTER_X;y,210;shadowlength,2;zoom,0.4;wrapwidthpixels,1200;diffusealpha,0;sleep,0.4;diffusealpha,1);
		OffCommand=cmd();
	};

	Def.OptionIconRow {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_1);
		InitCommand=cmd(x,SCREEN_CENTER_X-250;y,SCREEN_CENTER_Y+80;set,PLAYER_1;zoom,1);
		OptionsChangedMessageCommand=cmd(set,PLAYER_1);
	};

	Def.OptionIconRow {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_2);
		InitCommand=cmd(x,SCREEN_CENTER_X-250;y,SCREEN_CENTER_Y+80;set,PLAYER_2;zoom,1);
		OptionsChangedMessageCommand=cmd(set,PLAYER_2);
	};
	
	LoadActor ( "Mileage" ) .. {
		OnCommand=cmd(x,SCREEN_CENTER_X;y,420; hide_if,not IsArrowModsVisible() );	
	};

	LoadFont("MileageCost") .. {
		OnCommand=cmd(horizalign,left;x,225;y,416;settext,GetCurrentMileageCost(GAMESTATE:GetMasterPlayerNumber());zoom,1.15;shadowlength,0; hide_if,not IsArrowModsVisible() );
		ModsChangedMessageCommand=cmd( settext,GetCurrentMileageCost(GAMESTATE:GetMasterPlayerNumber()) );
	};
	
	LoadActor("overlay.lua") .. {
		Condition=IsArrowModsVisible();
		InitCommand=cmd();
	};
	
};

return t;

