local t = Def.ActorFrame {

	LoadActor("../Common Utils/BRAIN_BG") .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;FullScreen);
	};

	LoadActor("bg aux.png") .. {
		OnCommand = cmd(zoom,2;diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;linear,0.5;zoom,1;diffusealpha,1);
		OffCommand = cmd(linear,0.5;zoom,2);
	};

	LoadActor("../ScreenSelectMusicUtils/Press" ) .. {};

	LoadActor("Sound" ) .. {OnCommand=cmd(); PlayerJoinedMessageCommand=cmd(play);};

	LoadActor("../ScreenSelectMusicUtils/song background scroller") .. {
		InitCommand = cmd(y,SCREEN_CENTER_Y+195;addx,-40);
		OffCommand = cmd(stoptweening;diffusealpha,0);
	};

}
return t 
