local t = Def.ActorFrame {
	Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
	LoadActor( "kcalp1" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,-3);
		OnCommand=cmd( sleep,1; linear,2; zoomx, clamp(SCALE(GetPlayerKCalForPlayerAndStage( 0, PLAYER_1 ),0,150,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=2,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,-3;sleep,1;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerKCalForPlayerAndStage( 0, PLAYER_1 ),0,99999) );
		OnCommand=function(self)
			local kcal = STATSMAN:GetPlayedStageStats(0):GetPlayerStageStats(PLAYER_1)
			kcal = kcal:GetKcal()
			local pointtokcal = Movetokcal(kcal)
				if not kcal or kcal <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(2); self:addx(pointtokcal); end;
	};

	LoadActor( "vo2p1" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,14);
		OnCommand=cmd( sleep,2; linear,1; zoomx, clamp(SCALE(GetPlayerVO2ForPlayerAndStage( 0, PLAYER_1 ),0,400,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=1.1,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,14;sleep,2;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerVO2ForPlayerAndStage( 0, PLAYER_1 ),0,99999) );
		OnCommand=function(self)
			local vo2 = STATSMAN:GetPlayedStageStats(0):GetPlayerStageStats(PLAYER_1)
			vo2 = vo2:GetOxigenTaken()
			local pointtovo2 = Movetovo2(vo2)
				if not vo2 or vo2 <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(1); self:addx(pointtovo2); end;
	};

	LoadActor( "kcalp1" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,70);
		OnCommand=cmd( sleep,1; linear,2; zoomx, clamp(SCALE(GetPlayerKCalForPlayerAndStage( 1, PLAYER_1 ),0,150,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=2,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,70;sleep,1;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerKCalForPlayerAndStage( 1, PLAYER_1 ),0,99999) );
		OnCommand=function(self)
			local kcal = STATSMAN:GetPlayedStageStats(1):GetPlayerStageStats(PLAYER_1)
			kcal = kcal:GetKcal()
			local pointtokcal = Movetokcal(kcal)
				if not kcal or kcal <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(2); self:addx(pointtokcal); end;
	};

	LoadActor( "vo2p1" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,87);
		OnCommand=cmd( sleep,2; linear,1; zoomx, clamp(SCALE(GetPlayerVO2ForPlayerAndStage( 1, PLAYER_1 ),0,400,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=1.1,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,87;sleep,2;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerVO2ForPlayerAndStage( 1, PLAYER_1 ),0,99999) );
		OnCommand=function(self)
			local vo2 = STATSMAN:GetPlayedStageStats(1):GetPlayerStageStats(PLAYER_1)
			vo2 = vo2:GetOxigenTaken()
			local pointtovo2 = Movetovo2(vo2)
				if not vo2 or vo2 <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(1); self:addx(pointtovo2); end;
	};

	LoadActor( "kcalp1" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,143);
		OnCommand=cmd( sleep,1; linear,2; zoomx, clamp(SCALE(GetPlayerKCalForPlayerAndStage( 2, PLAYER_1 ),0,150,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=2,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,143;sleep,1;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerKCalForPlayerAndStage( 2, PLAYER_1 ),0,99999) );
		OnCommand=function(self)
			local kcal = STATSMAN:GetPlayedStageStats(2):GetPlayerStageStats(PLAYER_1)
			kcal = kcal:GetKcal()
			local pointtokcal = Movetokcal(kcal)
				if not kcal or kcal <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(2); self:addx(pointtokcal); end;
	};

	LoadActor( "vo2p1" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,160);
		OnCommand=cmd( sleep,2; linear,1; zoomx, clamp(SCALE(GetPlayerVO2ForPlayerAndStage( 2, PLAYER_1 ),0,400,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=1.1,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,160;sleep,2;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerVO2ForPlayerAndStage( 2, PLAYER_1 ),0,99999) );
		OnCommand=function(self)
			local vo2 = STATSMAN:GetPlayedStageStats(2):GetPlayerStageStats(PLAYER_1)
			vo2 = vo2:GetOxigenTaken()
			local pointtovo2 = Movetovo2(vo2)
				if not vo2 or vo2 <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(1); self:addx(pointtovo2); end;
	};


	LoadActor( "kcalp1" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,214);
		OnCommand=cmd( sleep,1; linear,2; zoomx, clamp(SCALE(GetPlayerKCalForPlayerAndStage( 3, PLAYER_1 ),0,150,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=2,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,214;sleep,1;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerKCalForPlayerAndStage( 3, PLAYER_1 ),0,99999) );
		OnCommand=function(self)
			local kcal = STATSMAN:GetPlayedStageStats(3):GetPlayerStageStats(PLAYER_1)
			kcal = kcal:GetKcal()
			local pointtokcal = Movetokcal(kcal)
				if not kcal or kcal <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(2); self:addx(pointtokcal); end;
	};

	LoadActor( "vo2p1" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,231);
		OnCommand=cmd( sleep,2; linear,1; zoomx, clamp(SCALE(GetPlayerVO2ForPlayerAndStage( 3, PLAYER_1 ),0,400,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=1.1,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,231;sleep,2;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerVO2ForPlayerAndStage( 3, PLAYER_1 ),0,99999) );
		OnCommand=function(self)
			local vo2 = STATSMAN:GetPlayedStageStats(3):GetPlayerStageStats(PLAYER_1)
			vo2 = vo2:GetOxigenTaken()
			local pointtovo2 = Movetovo2(vo2)
				if not vo2 or vo2 <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(1); self:addx(pointtovo2); end;
	};


};

return t