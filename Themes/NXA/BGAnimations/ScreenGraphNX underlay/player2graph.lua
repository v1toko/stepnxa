local t = Def.ActorFrame {
	Condition=GAMESTATE:IsPlayerEnabled( PLAYER_2 );

	LoadActor( "kcalp2" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,-14);
		OnCommand=cmd( sleep,1; linear,2; zoomx, clamp(SCALE(GetPlayerKCalForPlayerAndStage( 0, PLAYER_2 ),0,150,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=2,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,-14;sleep,1;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerKCalForPlayerAndStage( 0, PLAYER_2 ),0,99999) );
		OnCommand=function(self)
			local kcal = STATSMAN:GetPlayedStageStats(0):GetPlayerStageStats(PLAYER_2)
			kcal = kcal:GetKcal()
			local pointtokcal = Movetokcal(kcal)
				if not kcal or kcal <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(2); self:addx(pointtokcal); end;
	};


	LoadActor( "vop2" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,3);
		OnCommand=cmd( sleep,2; linear,1; zoomx, clamp(SCALE(GetPlayerVO2ForPlayerAndStage( 0, PLAYER_2 ),0,400,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=1.1,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,3;sleep,2;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerVO2ForPlayerAndStage( 0, PLAYER_2 ),0,99999) );
		OnCommand=function(self)
			local vo2 = STATSMAN:GetPlayedStageStats(0):GetPlayerStageStats(PLAYER_2)
			vo2 = vo2:GetOxigenTaken()
			local pointtovo2 = Movetovo2(vo2)
				if not vo2 or vo2 <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(1); self:addx(pointtovo2); end;
	};

	LoadActor( "kcalp2" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,59);
		OnCommand=cmd( sleep,1; linear,2; zoomx, clamp(SCALE(GetPlayerKCalForPlayerAndStage( 1, PLAYER_2 ),0,150,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=2,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,59;sleep,1;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerKCalForPlayerAndStage( 1, PLAYER_2 ),0,99999) );
		OnCommand=function(self)
			local kcal = STATSMAN:GetPlayedStageStats(1):GetPlayerStageStats(PLAYER_2)
			kcal = kcal:GetKcal()
			local pointtokcal = Movetokcal(kcal)
				if not kcal or kcal <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(2); self:addx(pointtokcal); end;
	};


	LoadActor( "vop2" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,76);
		OnCommand=cmd( sleep,2; linear,1; zoomx, clamp(SCALE(GetPlayerVO2ForPlayerAndStage( 1, PLAYER_2 ),0,400,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=1.1,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,76;sleep,2;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerVO2ForPlayerAndStage( 1, PLAYER_2 ),0,99999) );
		OnCommand=function(self)
			local vo2 = STATSMAN:GetPlayedStageStats(1):GetPlayerStageStats(PLAYER_2)
			vo2 = vo2:GetOxigenTaken()
			local pointtovo2 = Movetovo2(vo2)
				if not vo2 or vo2 <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(1); self:addx(pointtovo2); end;
	};


	LoadActor( "kcalp2" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,132);
		OnCommand=cmd( sleep,1; linear,2; zoomx, clamp(SCALE(GetPlayerKCalForPlayerAndStage( 2, PLAYER_2 ),0,150,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=2,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,132;sleep,1;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerKCalForPlayerAndStage( 2, PLAYER_2 ),0,99999) );
		OnCommand=function(self)
			local kcal = STATSMAN:GetPlayedStageStats(2):GetPlayerStageStats(PLAYER_2)
			kcal = kcal:GetKcal()
			local pointtokcal = Movetokcal(kcal)
				if not kcal or kcal <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(2); self:addx(pointtokcal); end;
	};

	LoadActor( "vop2" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,149);
		OnCommand=cmd( sleep,2; linear,1; zoomx, clamp(SCALE(GetPlayerVO2ForPlayerAndStage( 2, PLAYER_2 ),0,400,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=1.1,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,149;sleep,2;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerVO2ForPlayerAndStage( 2, PLAYER_2 ),0,99999) );
		OnCommand=function(self)
			local vo2 = STATSMAN:GetPlayedStageStats(2):GetPlayerStageStats(PLAYER_2)
			vo2 = vo2:GetOxigenTaken()
			local pointtovo2 = Movetovo2(vo2)
				if not vo2 or vo2 <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(1); self:addx(pointtovo2); end;
	};


	LoadActor( "kcalp2" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,203);
		OnCommand=cmd( sleep,1; linear,2; zoomx, clamp(SCALE(GetPlayerKCalForPlayerAndStage( 3, PLAYER_2 ),0,150,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=2,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,203;sleep,1;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerKCalForPlayerAndStage( 3, PLAYER_2 ),0,99999) );
		OnCommand=function(self)
			local kcal = STATSMAN:GetPlayedStageStats(3):GetPlayerStageStats(PLAYER_2)
			kcal = kcal:GetKcal()
			local pointtokcal = Movetokcal(kcal)
				if not kcal or kcal <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(2); self:addx(pointtokcal); end;
	};

	LoadActor( "vop2" ) .. {
		InitCommand=cmd(horizalign,'HorizAlign_Left';zoomx,0;zoomy,0.6;addy,220);
		OnCommand=cmd( sleep,2; linear,1; zoomx, clamp(SCALE(GetPlayerVO2ForPlayerAndStage( 3, PLAYER_2 ),0,400,0,1),0,1) );
	};
	Def.RollingNumbers {
		File=THEME:GetPathF("","GraphNX"),
		Format="%1.f",
		ApproachSeconds=1.1,
		TargetNumber=0,
		InitCommand=cmd(shadowlength,0;addx,12;zoom,.5;addy,220;sleep,2;queuecommand,"Cicle");
		CicleCommand=cmd(targetnumber,clamp(GetPlayerVO2ForPlayerAndStage( 3, PLAYER_2 ),0,99999) );
		OnCommand=function(self)
			local vo2 = STATSMAN:GetPlayedStageStats(3):GetPlayerStageStats(PLAYER_2)
			vo2 = vo2:GetOxigenTaken()
			local pointtovo2 = Movetovo2(vo2)
				if not vo2 or vo2 <= 1 then self:croptop(1) else self:croptop(0) end
			self:linear(1); self:addx(pointtovo2); end;
	};

};

return t