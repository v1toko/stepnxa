local t = Def.ActorFrame {

	--Def.Actor {	OnCommand=cmd(sleep,4);	};
	
	LoadActor ("../ScreenEvaluationStage background/fondo" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;FullScreen);
	};

	LoadActor( "underlay" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;FullScreen);
		OnCommand=cmd();
	};

	LoadActor( "player2graph" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X-178;y,SCREEN_CENTER_Y-115);
	};
	
	LoadActor( "player1graph" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X-178;y,SCREEN_CENTER_Y-162);
	};
	
	Def.ActorFrame {
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_2);
		LoadFont( "GraphNX" ) .. {
			InitCommand=cmd(x,SCREEN_CENTER_X+210;y,SCREEN_CENTER_Y+182;shadowlength,0;zoom,0.9);
			OnCommand=cmd(settext, string.format( "%3.3f", STATSMAN:GetAccumPlayedStageStats():GetPlayerStageStats(PLAYER_2):GetOxigenTaken() ) );
		};
		
		LoadFont( "GraphNX" ) .. {
			InitCommand=cmd(x,SCREEN_CENTER_X+210;y,SCREEN_CENTER_Y+215;shadowlength,0;zoom,0.9);
			OnCommand=cmd(settext, string.format( "%3.3f", STATSMAN:GetAccumPlayedStageStats():GetPlayerStageStats(PLAYER_2):GetKcal() ) );
		};
	};
	
	Def.ActorFrame {
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_1);
		LoadFont( "GraphNX" ) .. {
			InitCommand=cmd(x,SCREEN_CENTER_X-210;y,SCREEN_CENTER_Y+182;shadowlength,0;zoom,0.9);
			OnCommand=cmd(settext, string.format( "%3.3f", STATSMAN:GetAccumPlayedStageStats():GetPlayerStageStats(PLAYER_1):GetOxigenTaken() ) );
		};
		
		LoadFont( "GraphNX" ) .. {
			InitCommand=cmd(x,SCREEN_CENTER_X-210;y,SCREEN_CENTER_Y+215;shadowlength,0;zoom,0.9);
			OnCommand=cmd(settext, string.format( "%3.3f", STATSMAN:GetAccumPlayedStageStats():GetPlayerStageStats(PLAYER_1):GetKcal() ) );
		};
	};
};

return t;

