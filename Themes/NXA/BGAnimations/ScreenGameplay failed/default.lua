local t = Def.ActorFrame {

	Def.Actor { OnCommand=cmd(sleep,5.3); };

	LoadActor("../_black.png") .. {
		InitCommand=cmd(stretchto,SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM);
		OnCommand=cmd(diffusealpha,0;sleep,0.5;linear,0.5;diffusealpha,1);
	};

	LoadActor("SB_") .. {
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
	};	

	LoadActor("STAGEBREAK") .. {
		StartTransitioningCommand=cmd(play);
	};
};

return t;
