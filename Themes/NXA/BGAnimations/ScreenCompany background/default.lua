local t = Def.ActorFrame {

	LoadActor("nexcade.png") .. {
		File="nexcade.png",
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0),
		OnCommand=cmd(diffusealpha,0;sleep,1;linear,1;diffusealpha,1),
        };
	Def.Quad {
		InitCommand=cmd(stretchto,SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM;diffuse,color("#000000");draworder,99999);
		OnCommand=cmd(diffusealpha,0);
		OffCommand=cmd(linear,0.6;diffusealpha,1);
	};

}

return t
