local t = Def.ActorFrame {
	--MODS
	
	LoadActor("EasyStationbg.png") .. {
		OnCommand = cmd(zoom,2;diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;linear,0.5;zoom,1;diffusealpha,1);
		OffCommand = cmd(linear,0.5;zoom,2);
	};

	LoadActor("EasyStationbg.png") .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;sleep,0.9;diffusealpha,0.8;blend,'BlendMode_Add';linear,0.8;zoom,1.1;diffusealpha,0);
	};
	
	LoadActor( "../ScreenSelectMusicEasy overlay/EasyStationbg (glow)" ) .. {
		InitCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;blend,"BlendMode_Add";diffuse,color("#ACFB94");diffusealpha,0);
		OnCommand=cmd(sleep,.8;linear,0.5;diffusealpha,1);
		OffCommand=cmd(diffusealpha,0);
	};

	LoadActor( "../ScreenSelectMusicEasy overlay/EasyStationbg (glow)" ) .. {
		InitCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;blend,"BlendMode_Add";diffuse,color("1.0,0.1,0.1,0"));
		OnCommand=cmd(sleep,1;diffusealpha,1;linear,.8;zoom,1.1;diffusealpha,0);
		OffCommand=cmd(diffusealpha,0);
	};

	LoadActor( "../Common Utils/ArrowsScript" ) .. {
		InitCommand=cmd(addy,-10);
		OnCommand = cmd();
		OffCommand = cmd();
	};


	Def.ActorProxy {
		BeginCommand=function(self) local timer = SCREENMAN:GetTopScreen():GetChild('Timer'); self:SetTarget(timer); end;
		OnCommand=cmd(diffusealpha,1);
		OffCommand=cmd(linear,0.2;diffusealpha,0);
	};
	
	LoadActor("gradeframe") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X+4;y,SCREEN_CENTER_Y-4;);
		OnCommand=cmd(scaletoclipped,298,245;finishtweening;diffusealpha,0;sleep,.4;linear,.16;diffusealpha,1);
		OffCommand=cmd(finishtweening;linear,0.1;diffusealpha,0);
		CurrentSongChangedMessageCommand=cmd(playcommand,"Set");
		CenterWasPressedTwiceMessageCommand=cmd(playcommand,"Off");
		SetCommand=function(self)
			local song = GAMESTATE:GetCurrentSong();
			local bpassed = GetPlayerProfile( GAMESTATE:GetFirstHumanPlayer() ):SongPassedInBrainShower( song )
			
			if bpassed then
				self:visible(1);
				self:playcommand("On");
			else
				self:visible(0);
			end
		end;

		SendHighScoreMessageCommand=function(self, param)
			local grade = param.Grade
			
			if grade == 'Grade_Tier01' or grade == 'Grade_Tier02' then
				self:visible( 1 )
				self:playcommand( "On" )
			elseif grade == 'Grade_Tier03' then
				self:visible( 1 )
				self:playcommand( "On" )
			else
				self:visible( 0 )
			end
		end;

	};
	
	LoadFont( "BoostSSi" ) .. {
		Condition=GAMESTATE:GetNumPlayersEnabled() == 1;
		InitCommand=cmd(x,SCREEN_CENTER_X+45;y,SCREEN_CENTER_Y-85;zoom,0.35;shadowlength,0);
		OnCommand=cmd(finishtweening;diffusealpha,0;sleep,0.4;linear,0.16;diffusealpha,1);
		OffCommand=cmd(finishtweening;linear,0.1;diffusealpha,0);
		--CurrentSongChangedMessageCommand=cmd(playcommand,"Set");
		CenterWasPressedTwiceMessageCommand=cmd(playcommand,"Off");
		SendHighScoreMessageCommand=function(self, param)
			local grade = param.Grade
			
			if grade == 'Grade_Tier01' or grade == 'Grade_Tier02' then
				self:visible( 1 )
				self:settext( "S" )
				self:playcommand( "On" )
			elseif grade == 'Grade_Tier03' then
				self:visible( 1 )
				self:settext( "A" )
				self:playcommand( "On" )
			else
				self:visible( 0 )
			end
		end;
	};
	
	LoadActor("levelframe") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X+4;y,SCREEN_CENTER_Y-4);
		OnCommand=cmd(scaletoclipped,298,245;finishtweening;diffusealpha,0;sleep,0.4;linear,0.16;diffusealpha,1);
		OffCommand=cmd(finishtweening;linear,0.1;diffusealpha,0);
		CurrentSongChangedMessageCommand=cmd(playcommand,"On");
		CenterWasPressedTwiceMessageCommand=cmd(playcommand,"Off");
	};
	
	LoadActor("../ScreenSelectMusicEasy overlay/marco") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X+4;y,SCREEN_CENTER_Y-7);
		OnCommand=cmd(diffuse,getgroupcolor();scaletoclipped,298,245;finishtweening;diffusealpha,0;sleep,0.4;linear,0.16;diffusealpha,1);
		OffCommand=cmd(finishtweening;scaletoclipped,298,245;sleep,0.2;linear,0.3;zoom,0.7;accelerate,0.5;zoom,3);
		CurrentSongChangedMessageCommand=cmd(playcommand,"On");
		CenterWasPressedTwiceMessageCommand=cmd(playcommand,"Off");
		PulseCommand=cmd(finishtweening;diffusealpha,1;zoom,0.61;zoomx,0.47;linear,0.4;zoom,0.68;diffusealpha,0;sleep,0.2;queuecommand,"Pulse");
	};
	
	LoadActor("../ScreenSelectMusicEasy overlay/marco") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X+4;y,SCREEN_CENTER_Y-7;blend,"BlendMode_Add");
		OnCommand=cmd(diffuse,getgroupcolor();scaletoclipped,298,245;finishtweening;diffusealpha,0;sleep,0.5;linear,0.16;diffusealpha,.6);
		OffCommand=cmd(finishtweening;scaletoclipped,298,245;sleep,0.2;linear,0.3;zoom,0.7;accelerate,0.5;zoom,3);
		CurrentSongChangedMessageCommand=cmd(playcommand,"On");
		CenterWasPressedTwiceMessageCommand=cmd(playcommand,"Off");
	};
	
	LoadActor("../ScreenSelectMusicEasy overlay/marco") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X+4;y,SCREEN_CENTER_Y-7;blend,"BlendMode_Add");
		OnCommand=cmd(diffuse,getgroupcolor();scaletoclipped,298,245;finishtweening;diffusealpha,0);
		OffCommand=cmd(finishtweening;stopeffect;diffusealpha,0);
		StyleChangedMessageCommand=cmd(playcommand,"On");
		CurrentSongChangedMessageCommand=cmd(playcommand,"On");
		CenterWasPressedOnceMessageCommand=cmd(playcommand,"Pulse");
		CenterWasPressedTwiceMessageCommand=cmd(playcommand,"Off");
		PulseCommand=cmd(finishtweening;diffusealpha,1;zoom,1;linear,0.4;zoom,1.2;diffusealpha,0;sleep,0.2;queuecommand,"Pulse");
	};
	
	LoadActor("../ScreenSelectMusicEasy overlay/marcoglow") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X+4;y,SCREEN_CENTER_Y-7;blend,"BlendMode_Add");
		OnCommand=cmd(scaletoclipped,355,293;finishtweening;diffusealpha,0;sleep,0.4;diffusealpha,1;zoom,1.2;linear,0.2;zoom,1);
		OffCommand=cmd(finishtweening;scaletoclipped,355,293;sleep,0.2;linear,0.3;zoom,0.7;accelerate,0.5;zoom,3);
		CurrentSongChangedMessageCommand=cmd(playcommand,"On");
		CenterWasPressedTwiceMessageCommand=cmd(playcommand,"Off");
	};
	
	LoadActor("../ScreenSelectMusicEasy overlay/diffdisplay") .. {};
	
	LoadActor("../Common Utils/CenterScript") .. {
		InitCommand=cmd(addy,-60);
	};
	
	LoadActor( "Memory mode" ) .. {
		SetCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();
			if not song then
				self:stop(1)
				return
			end

			song = string.lower( song );

			if song == "memory" then
				self:playcommand("Show");
			else
				self:playcommand("Hide");
			end
		end;

		OnCommand=cmd(playcommand,"Set";x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
		OffCommand=cmd(linear,0.1;diffusealpha,0);
		GroupChangedMessageCommand=cmd( playcommand,"Set" );
		StyleChangedMessageCommand=cmd( playcommand,"Set" );
		ShowCommand=cmd(diffusealpha,0;linear,0.1;diffusealpha,1);
		HideCommand=cmd(diffusealpha,0);
	};
	
	LoadActor( "Mathematics mode" ) .. {
		SetCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();
			if not song then
				self:stop(1)
				return
			end

			song = string.lower( song );

			if song == "mathematics" then
				self:playcommand("Show");
			else
				self:playcommand("Hide");
			end
		end;

		OnCommand=cmd(playcommand,"Set";x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
		OffCommand=cmd(linear,0.1;diffusealpha,0);
		GroupChangedMessageCommand=cmd( playcommand,"Set" );
		StyleChangedMessageCommand=cmd( playcommand,"Set" );
		ShowCommand=cmd(diffusealpha,0;linear,0.1;diffusealpha,1);
		HideCommand=cmd(diffusealpha,0);
	};
	
	LoadActor( "Observation mode" ) .. {
		SetCommand=function(self)
			local song = GAMESTATE:GetCurrentSong():GetGroupName();
			if not song then
				self:stop(1)
				return
			end

			song = string.lower( song );

			if song == "observation" then
				self:playcommand("Show");
			else
				self:playcommand("Hide");
			end
		end;

		OnCommand=cmd(playcommand,"Set";x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
		OffCommand=cmd(linear,0.1;diffusealpha,0);
		GroupChangedMessageCommand=cmd( playcommand,"Set" );
		StyleChangedMessageCommand=cmd( playcommand,"Set" );
		ShowCommand=cmd(diffusealpha,0;linear,0.1;diffusealpha,1);
		HideCommand=cmd(diffusealpha,0);
	};

	Def.Quad {
		InitCommand=cmd(stretchto,SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM;diffuse,color("#000000");draworder,99999);
		OnCommand=cmd(diffusealpha,0);
		OffCommand=cmd(sleep,0.65;linear,0.3;diffusealpha,1);
	};

}
return t 
