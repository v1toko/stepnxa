local t = Def.ActorFrame {

	LoadActor( "voice" ) .. {
		OnCommand=cmd(play);
	};
	LoadActor( "start" ) .. {
		OnCommand=cmd();
		OffCommand=cmd(play);
	};

	LoadActor ("Background.avi" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT);};
		
	LoadActor("Ranura") .. {
		Condition=not GAMESTATE:IsAnyHumanPlayerUsingMemoryCard();
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+10;diffusealpha,0;zoom,1.2;sleep,0.5;linear,0.5;diffusealpha,1;zoom,1;faderight,0.58);
		OffCommand  = cmd(linear,1;zoom,2);};
	
	LoadActor("Ranura") .. {
		Condition=GAMESTATE:IsAnyHumanPlayerUsingMemoryCard();
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+10;faderight,0.58);
		OffCommand  = cmd(linear,1;zoom,2);};

	LoadActor( "background usb.png" ) .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoom,1.4;linear,0.5;zoom,1);
		OffCommand = cmd(linear,0.5;zoom,1.1;diffusealpha,0);};

	LoadActor( "background usb.png" ) .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;blend,'BlendMode_Add';diffusealpha,0;sleep,0.5;diffusealpha,1;linear,0.5;zoom,1.1;diffusealpha,0);};

	LoadActor( "background usb (glow).png" ) .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;blend,'BlendMode_Add';diffusealpha,0;sleep,0.5;diffusealpha,1;linear,0.5;zoom,1;diffusecolor,color("#83b767"));
		OffCommand = cmd(linear,0.5;zoom,1.1;diffusealpha,0);};



};

return t
