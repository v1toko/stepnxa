local t = Def.ActorFrame {
	LoadActor ("../ScreenLoadUsb background/Background.avi" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT);
	};
	
	LoadActor( "voice" ) .. {
		OnCommand=cmd(sleep,3.25;queuecommand,"Play");
		PlayCommand=cmd(play);
	};

	LoadActor( "background usb.png" ) .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoom,1.4;linear,0.5;zoom,1);
		OffCommand = cmd(linear,0.5;zoom,1.1;diffusealpha,0);
	};
	LoadActor( "background usb.png" ) .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;blend,'BlendMode_Add';diffusealpha,0;sleep,0.5;diffusealpha,1;linear,0.5;zoom,1.1;diffusealpha,0);
	};

	LoadActor( "background usb (glow).png" ) .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;blend,'BlendMode_Add';diffusealpha,0;sleep,0.5;diffusealpha,1;linear,0.5;zoom,1;diffusecolor,color("#83b767"));
		OffCommand = cmd(linear,0.5;zoom,1.1;diffusealpha,0);

	};

	Def.ActorFrame {
	
		--cuidado con esto
		LoadActor( "../Common Utils/" .. CharLoader( PLAYER_1 ) ) .. {
			Condition = GAMESTATE:IsPlayerEnabled( PLAYER_1 ) and PROFILEMAN:IsPersistentProfile( PLAYER_1 );
			OnCommand = cmd( zoom,1.1;x,89; y, 142; addx,-460; sleep, 2; linear,0.4; addx, 460 );
			OffCommand = cmd( linear, 0.4; addx,-460 );
			CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
		};
		
		LoadActor( "USB savep1.png" ) .. {
			Condition = GAMESTATE:IsPlayerEnabled( PLAYER_1 ) and PROFILEMAN:IsPersistentProfile( PLAYER_1 );
			OnCommand = cmd( x,170; y, 240; addx,-460; sleep, 2; linear,0.4; addx, 460 );
			OffCommand = cmd( linear, 0.4; addx,-460 );
			CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
		};

		--stats del player 1
		Def.ActorFrame {
			Condition = GAMESTATE:IsPlayerEnabled( PLAYER_1 ) and PROFILEMAN:IsPersistentProfile( PLAYER_1 );
			--aqui muevo las stats que llevan MIN y %
			Def.ActorFrame {
				InitCommand=cmd(addx,-13);
				LoadFont("BoostSSi") .. {--total plays
					OnCommand = cmd( x,200; y, 205;shadowlength,0 ; addx,-460; zoom, 0.6; sleep, 2; linear,0.4; addx, 460; settext, string.format( "%.6i (%.6i)", GetPlayerProfile( PLAYER_1 ):GetTotalsPlays(),GetPlayerProfile(PLAYER_1):GetTotalGameplaySecons() ) );
					OffCommand = cmd( linear, 0.4; addx,-460 );
					CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
				};

				LoadFont("BoostSSi") .. {--wm complete
					OnCommand = cmd( x,140; y, 245;shadowlength,0 ; addx,-460; zoom, 0.6; sleep, 2; linear,0.4; addx, 460; settext, string.format( "%.1f", GetPlayerProfile( PLAYER_1 ):GetWorldMaxCompletePercent() ) );
					OffCommand = cmd( linear, 0.4; addx,-460 );
					CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
				};
				
				LoadFont("BoostSSi") .. {--arcade complete
					OnCommand = cmd( x,270; y, 245;shadowlength,0 ; addx,-460; zoom, 0.6; sleep, 2; linear,0.4; addx, 460; settext, string.format( "%.1f", GetPlayerProfile( PLAYER_1 ):GetSongsPercentCompleteAllDifficulties("StepsType_Pump_Single","PlayMode_Regular") ) );
					OffCommand = cmd( linear, 0.4; addx,-460 );
					CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
				};
				
				LoadFont("BoostSSi") .. {--brainshower complete
					OnCommand = cmd( x,140; y, 270;shadowlength,0 ; addx,-460; zoom, 0.6; sleep, 2; linear,0.4; addx, 460; settext, string.format( "%2.1f", GetBrainShowerTotals( PLAYER_1 ) ) );
					OffCommand = cmd( linear, 0.4; addx,-460 );
					CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
				};
				
				LoadFont("BoostSSi") .. {--special complete
					OnCommand = cmd( x,270; y, 270;shadowlength,0 ; addx,-460; zoom, 0.6; sleep, 2; linear,0.4; addx, 460; settext, string.format( "%.1f", GetPlayerProfile( PLAYER_1 ):GetSongsPercentCompleteAllDifficulties("StepsType_Pump_Single","PlayMode_Special") ) );
					OffCommand = cmd( linear, 0.4; addx,-460 );
					CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
				};
				
				LoadFont("BoostSSi") .. {--reward complete
					OnCommand = cmd( x,270; y, 292;shadowlength,0 ; addx,-460; zoom, 0.6; sleep, 2; linear,0.4; addx, 460; settext, string.format( "%.2i/%.2i", GetPlayerProfile( PLAYER_1 ):GetRewardPercentFromProfile(),GetPlayerProfile( PLAYER_1 ):GetRewardTotal() ) );
					OffCommand = cmd( linear, 0.4; addx,-460 );
					CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
				};
				
				LoadFont("BoostSSi") .. {--total
					OnCommand = cmd( x,140; y, 292;shadowlength,0 ; addx,-460; zoom, 0.6; sleep, 2; linear,0.4; addx, 460; settext, string.format( "%.1f", GetPlayerProfile( PLAYER_1 ):GetAllPercentComplete() ) );
					OffCommand = cmd( linear, 0.4; addx,-460 );
					CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
				};
			};
			
			LoadFont("BoostSSi") .. {--name
				OnCommand = cmd( x,220; y, 125;shadowlength,0 ; addx,-460; zoom, 0.8; sleep, 2; linear,0.4; addx, 460; settext, GetProfileDisplayName( PLAYER_1 ) );
				OffCommand = cmd( linear, 0.4; addx,-460 );
				CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
			};

			--LoadFont("BoostSSi") .. {--total steps
				--OnCommand = cmd( x,220; y, 175;shadowlength,0 ; addx,-460; zoom, 0.4; zoomx, 0.25; sleep, 2; linear,0.4; addx, 460; settext, string.format( "%.7i +%.7i", clamp(GetPlayerProfile( PLAYER_1 ):GetTotalTapsAndHolds()-GetAccumPlayedStageStats( PLAYER_1 ):GetTotalSteps(),0,9999999),GetAccumPlayedStageStats( PLAYER_1 ):GetTotalSteps() ) );
				--OffCommand = cmd( linear, 0.4; addx,-460 );
				--CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
			--};
			
			Def.RollingNumbers {
				File=THEME:GetPathF("","BoostSSi"),
				Format="%.0f",
				ApproachSeconds=0.1,--rapido!!
				TargetNumber=clamp(GetPlayerProfile( PLAYER_1 ):GetTotalTapsAndHolds()-GetAccumPlayedStageStats( PLAYER_1 ):GetTotalSteps(),0,9999999),
				OnCommand = cmd( x,145; y, 175;horizalign,'HorizAlign_Left';shadowlength,0 ; addx,-460; zoom, 0.8;zoomx, 0.5; sleep, 2; linear,0.4; addx, 460;sleep,3;queuecommand,"Change");
				ChangeCommand=cmd(setapproachseconds,2.6;targetnumber,clamp(GetPlayerProfile( PLAYER_1 ):GetTotalTapsAndHolds(),0,9999999));
				OffCommand = cmd(stoptweening;linear, 0.4; addx,-460 );
				CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
			};
			
			Def.RollingNumbers {
				File=THEME:GetPathF("","BoostSSi"),
				Format="+%.0f",
				ApproachSeconds=0.1,--rapido!!
				TargetNumber=GetAccumPlayedStageStats( PLAYER_1 ):GetTotalSteps(),
				OnCommand = cmd(x,225; y, 175;horizalign,'HorizAlign_Left';shadowlength,0 ; addx,-460; zoom, 0.8;zoomx, 0.5; sleep, 2; linear,0.4; addx, 460;sleep,3;queuecommand,"Change");
				ChangeCommand=cmd(setapproachseconds,2.6;targetnumber,0);
				OffCommand = cmd(stoptweening;linear, 0.4; addx,-460 );
				CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
			};
			
			--LoadFont("BoostSSi") .. {--mileage
				--OnCommand = cmd( x,220; y, 387;shadowlength,0 ; addx,-460; zoom, 0.3; zoomx,0.25;sleep, 2; linear,0.4; addx, 460; settext, string.format( "%.7i +%.7i", clamp(GetPlayerProfile( PLAYER_1 ):GetTotalMileage()-GetAccumPlayedStageStats( PLAYER_1 ):GetMileage(),0,9999999),GetAccumPlayedStageStats( PLAYER_1 ):GetMileage() ) );
				--OffCommand = cmd( linear, 0.4; addx,-460 );
				--CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
			--};
			
			Def.RollingNumbers {
				File=THEME:GetPathF("","BoostSSi"),
				Format="%.0f",
				ApproachSeconds=0.1,--rapido!!
				TargetNumber=clamp(GetPlayerProfile( PLAYER_1 ):GetTotalMileage()-GetAccumPlayedStageStats( PLAYER_1 ):GetMileage(),0,9999999),
				OnCommand = cmd( x,145; y, 387;horizalign,'HorizAlign_Left';shadowlength,0 ; addx,-460; zoom, 0.65;zoomx,0.5; sleep, 2; linear,0.4; addx, 460;sleep,3;queuecommand,"Change" );
				ChangeCommand=cmd(setapproachseconds,2.6;targetnumber,clamp(GetPlayerProfile( PLAYER_1 ):GetTotalMileage(),0,9999999));
				OffCommand = cmd(stoptweening;linear, 0.4; addx,-460 );
				CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
			};
			
			Def.RollingNumbers {
				File=THEME:GetPathF("","BoostSSi"),
				Format="+%.0f",
				ApproachSeconds=0.1,--rapido!!
				TargetNumber=GetAccumPlayedStageStats( PLAYER_1 ):GetMileage(),
				OnCommand = cmd( horizalign,'HorizAlign_Left';x,225; y, 387;shadowlength,0 ; addx,-460; zoom, 0.65;zoomx,0.5; sleep, 2; linear,0.4; addx, 460;sleep,3;queuecommand,"Change" );
				ChangeCommand=cmd(setapproachseconds,2.6;targetnumber,0);
				OffCommand = cmd(stoptweening;linear, 0.4; addx,-460 );
				CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
			};
			
			LoadFont("BoostSSi") .. {--lastupdate
				OnCommand = cmd( x,220; y, 402;shadowlength,0 ; addx,-460; zoom, 0.53; sleep, 2; linear,0.4; addx, 460; settext, string.format( "%.2i/%.2i/%.2i  %.2i:%.2i", DayOfMonth(),MonthOfYear()+1,Year(),Hour(),Minute() ) );
				OffCommand = cmd( linear, 0.4; addx,-460 );
				CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
			};
			
			LoadActor("Explanation.png") .. {
				--Condition = GAMESTATE:IsPlayerEnabled( PLAYER_1 ) and not PROFILEMAN:IsPersistentProfile( PLAYER_1 );
				OnCommand = cmd( x,170; y, 340; diffusealpha,0; sleep,9; queuecommand,"Show"  );
				OffCommand = cmd( stoptweening;sleep,1;linear, 0.4;diffusealpha,0 );
				ShowCommand = cmd(accelerate,0.4; diffusealpha,1 );
				CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );
			};
		};
		
		--cuidado con esto
		LoadActor( "../Common Utils/" .. CharLoader( PLAYER_2 ) ) .. {
			Condition = GAMESTATE:IsPlayerEnabled( PLAYER_2 ) and PROFILEMAN:IsPersistentProfile( PLAYER_2 );
			OnCommand = cmd( zoom,1.1;x,SCREEN_RIGHT-250; y, 142; addx,460; sleep, 2; linear,0.4; addx, -460 );
			OffCommand = cmd( linear, 0.4; addx,460 );
			CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
		};

		LoadActor( "USB savep2.png" ) .. {
			Condition = GAMESTATE:IsPlayerEnabled( PLAYER_2 ) and PROFILEMAN:IsPersistentProfile( PLAYER_2 );
			OnCommand = cmd( x,SCREEN_RIGHT-170; y, 240; addx,460; sleep, 2; linear,0.4; addx, -460 );
			OffCommand = cmd( linear, 0.4; addx,460 );
			CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
		};

		--stats del player 2
		Def.ActorFrame {
			Condition = GAMESTATE:IsPlayerEnabled( PLAYER_2 ) and PROFILEMAN:IsPersistentProfile( PLAYER_2 );
			Def.ActorFrame {--dejo aqui los que mover�
				InitCommand=cmd(addx,-13);
				
				LoadFont("BoostSSi") .. {--total plays
					OnCommand = cmd( x,SCREEN_RIGHT-140; y, 205;shadowlength,0 ; addx,460; zoom, 0.6; sleep, 2; linear,0.4; addx, -460; settext, string.format( "%.6i (%.6i)", GetPlayerProfile( PLAYER_2 ):GetTotalsPlays(),GetPlayerProfile(PLAYER_2):GetTotalGameplaySecons() ));
					OffCommand = cmd( linear, 0.4; addx,460 );
					CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
				};

				LoadFont("BoostSSi") .. {--wm complete
					OnCommand = cmd( x,SCREEN_RIGHT-200; y, 245;shadowlength,0 ; addx,460; zoom, 0.6; sleep, 2; linear,0.4; addx, -460; settext, string.format( "%.1f", GetPlayerProfile( PLAYER_2 ):GetWorldMaxCompletePercent() ) );
					OffCommand = cmd( linear, 0.4; addx,460 );
					CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
				};
				
				LoadFont("BoostSSi") .. {--arcade complete
					OnCommand = cmd( x,SCREEN_RIGHT-70; y, 245;shadowlength,0 ; addx,460; zoom, 0.6; sleep, 2; linear,0.4; addx, -460; settext, string.format( "%.1f", GetPlayerProfile( PLAYER_2 ):GetSongsPercentCompleteAllDifficulties("StepsType_Pump_Single","PlayMode_Regular") ) );
					OffCommand = cmd( linear, 0.4; addx,460 );
					CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
				};
				
				LoadFont("BoostSSi") .. {--special complete
					OnCommand = cmd( x,SCREEN_RIGHT-70; y, 270;shadowlength,0 ; addx,460; zoom, 0.6; sleep, 2; linear,0.4; addx, -460; settext, string.format( "%.1f", GetPlayerProfile( PLAYER_2 ):GetSongsPercentCompleteAllDifficulties("StepsType_Pump_Single","PlayMode_Special") ) );
					OffCommand = cmd( linear, 0.4; addx,460 );
					CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
				};
				
				LoadFont("BoostSSi") .. {--brainshower complete
					OnCommand = cmd( x,SCREEN_RIGHT-200; y, 270;shadowlength,0 ; addx,460; zoom, 0.6; sleep, 2; linear,0.4; addx, -460; settext, string.format( "%2.1f", GetBrainShowerTotals( PLAYER_2 ) ) );
					OffCommand = cmd( linear, 0.4; addx,460 );
					CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
				};
				
				LoadFont("BoostSSi") .. {--reward complete
					OnCommand = cmd( x,SCREEN_RIGHT-70; y, 292;shadowlength,0 ; addx,460; zoom, 0.6; sleep, 2; linear,0.4; addx, -460; settext, string.format( "%.2i/%.2i", GetPlayerProfile( PLAYER_2 ):GetRewardPercentFromProfile(),GetPlayerProfile( PLAYER_2 ):GetRewardTotal() ) );
					OffCommand = cmd( linear, 0.4; addx,460 );
					CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
				};
				
				LoadFont("BoostSSi") .. {--total
					OnCommand = cmd( x,SCREEN_RIGHT-200; y, 292;shadowlength,0 ; addx,460; zoom, 0.6; sleep, 2; linear,0.4; addx, -460; settext, string.format( "%.1f", GetPlayerProfile( PLAYER_2 ):GetAllPercentComplete() ) );
					OffCommand = cmd( linear, 0.4; addx,460 );
					CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
				};
			};
			
			LoadFont("BoostSSi") .. {--name
				OnCommand = cmd( x,SCREEN_RIGHT-120; y, 125;shadowlength,0 ; addx,460; zoom, 0.8; sleep, 2; linear,0.4; addx, -460; settext, GetProfileDisplayName( PLAYER_2 ) );
				OffCommand = cmd( linear, 0.4; addx,460 );
				CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
			};

			--LoadFont("BoostSSi") .. {--total steps
				--OnCommand = cmd( x,SCREEN_RIGHT-155; y, 175;shadowlength,0 ; addx,460; zoom, 0.4;zoomx, 0.25; sleep, 2; linear,0.4; addx, -460; settext, string.format( "%.7i +", clamp(GetPlayerProfile( PLAYER_2 ):GetTotalTapsAndHolds()-GetAccumPlayedStageStats( PLAYER_2 ):GetTotalSteps(),0,9999999) ) );
				--OffCommand = cmd( linear, 0.4; addx,460 );
				--CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
			--};
			
			Def.RollingNumbers {
				File=THEME:GetPathF("","BoostSSi"),
				Format="%.0f",
				ApproachSeconds=0.1,--rapido!!
				TargetNumber=clamp(GetPlayerProfile( PLAYER_2 ):GetTotalTapsAndHolds()-GetAccumPlayedStageStats( PLAYER_2 ):GetTotalSteps(),0,9999999),
				OnCommand = cmd( x,SCREEN_RIGHT-195; y, 175;horizalign,'HorizAlign_Left';shadowlength,0 ; addx,460; zoom, 0.8;zoomx, 0.5; sleep, 2; linear,0.4; addx, -460;sleep,3;queuecommand,"Change");
				ChangeCommand=cmd(setapproachseconds,2.6;targetnumber,clamp(GetPlayerProfile( PLAYER_2 ):GetTotalTapsAndHolds(),0,9999999));
				OffCommand = cmd(stoptweening;linear, 0.4; addx,460 );
				CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
			};
			
			Def.RollingNumbers {
				File=THEME:GetPathF("","BoostSSi"),
				Format="+%.0f",
				ApproachSeconds=0.1,--rapido!!
				TargetNumber=GetAccumPlayedStageStats( PLAYER_2 ):GetTotalSteps(),
				OnCommand = cmd(x,SCREEN_RIGHT-115; y, 175;horizalign,'HorizAlign_Left';shadowlength,0 ; addx,460; zoom, 0.8;zoomx, 0.5; sleep, 2; linear,0.4; addx, -460;sleep,3;queuecommand,"Change");
				ChangeCommand=cmd(setapproachseconds,2.6;targetnumber,0);
				OffCommand = cmd(stoptweening;linear, 0.4; addx,460 );
				CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
			};
			
			--LoadFont("BoostSSi") .. {--mileage
				--OnCommand = cmd( x,SCREEN_RIGHT-120; y, 387;shadowlength,0 ; addx,460; zoom, 0.3;zoomx,0.25; sleep, 2; linear,0.4; addx, -460; settext, string.format( "%.7i +%.7i", clamp(GetPlayerProfile( PLAYER_2 ):GetTotalMileage()-GetAccumPlayedStageStats( PLAYER_2 ):GetMileage(),0,9999999),GetAccumPlayedStageStats( PLAYER_2 ):GetMileage() ) );
				--OffCommand = cmd( linear, 0.4; addx,460 );
				--CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
			--};
			
			Def.RollingNumbers {
				File=THEME:GetPathF("","BoostSSi"),
				Format="%.0f",
				ApproachSeconds=0.1,--rapido!!
				TargetNumber=clamp(GetPlayerProfile( PLAYER_2 ):GetTotalMileage()-GetAccumPlayedStageStats( PLAYER_2 ):GetMileage(),0,9999999),
				OnCommand = cmd( x,SCREEN_RIGHT-190; y, 387;horizalign,'HorizAlign_Left';shadowlength,0 ; addx,460; zoom, 0.65;zoomx,0.5; sleep, 2; linear,0.4; addx, -460;sleep,3;queuecommand,"Change" );
				ChangeCommand=cmd(setapproachseconds,2.6;targetnumber,clamp(GetPlayerProfile( PLAYER_2 ):GetTotalMileage(),0,9999999));
				OffCommand = cmd(stoptweening;linear, 0.4; addx,460 );
				CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
			};
			
			Def.RollingNumbers {
				File=THEME:GetPathF("","BoostSSi"),
				Format="+%.0f",
				ApproachSeconds=0.1,--rapido!!
				TargetNumber=GetAccumPlayedStageStats( PLAYER_2 ):GetMileage(),
				OnCommand = cmd( horizalign,'HorizAlign_Left';x,SCREEN_RIGHT-120; y, 387;shadowlength,0 ; addx,460; zoom, 0.65;zoomx,0.5; sleep, 2; linear,0.4; addx, -460;sleep,3;queuecommand,"Change" );
				ChangeCommand=cmd(setapproachseconds,2.6;targetnumber,0);
				OffCommand = cmd(stoptweening;linear, 0.4; addx,460 );
				CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
			};
			
			LoadFont("BoostSSi") .. {--lastupdate
				OnCommand = cmd( x,SCREEN_RIGHT-120; y, 402;shadowlength,0 ; addx,460; zoom, 0.53; sleep, 2; linear,0.4; addx, -460; settext, string.format( "%.2i/%.2i/%.2i  %.2i:%.2i", DayOfMonth(),MonthOfYear()+1,Year(),Hour(),Minute() ) );
				OffCommand = cmd( linear, 0.4; addx,460 );
				CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
			};
			
			LoadActor("Explanation2p.png") .. {
				--Condition = GAMESTATE:IsPlayerEnabled( PLAYER_2 ) and not PROFILEMAN:IsPersistentProfile( PLAYER_2 );
				OnCommand = cmd( x,SCREEN_RIGHT-170; y, 340; diffusealpha,0; sleep,9; queuecommand,"Show" );
				OffCommand = cmd( stoptweening;sleep,1;linear, 0.4;diffusealpha,0 );
				ShowCommand = cmd( accelerate,0.4; diffusealpha,1 );
				CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );
			};
		};
	};

	Def.ActorFrame {
		LoadActor( "USB enternousb1p.png" ) .. {
			Condition = GAMESTATE:IsPlayerEnabled( PLAYER_1 ) and not PROFILEMAN:IsPersistentProfile( PLAYER_1 );
			OnCommand = cmd( x,170; y, 240; addx,-460; zoom, 0.8;sleep,0.5;queuecommand,"Show" );
			ShowCommand = cmd( sleep, 1.5; linear,0.4; addx, 460 );
			OffCommand = cmd( linear, 0.4; addx,-460 );
			CenterWasPressedP1MessageCommand = cmd( playcommand,"Off" );};
		

		LoadActor( "USB enternousb2p.png" ) .. {
			Condition = GAMESTATE:IsPlayerEnabled( PLAYER_2 ) and not PROFILEMAN:IsPersistentProfile( PLAYER_2 );
			OnCommand = cmd( x,SCREEN_RIGHT-170; y, 240; addx,460;sleep,0.5;queuecommand,"Show" );
			OffCommand = cmd( linear, 0.4; addx,460 );
			ShowCommand = cmd( zoom, 0.8;sleep,1.5; linear,0.4; addx, -460 );
			CenterWasPressedP2MessageCommand = cmd( playcommand,"Off" );};
		
	};


	LoadActor("Luz") .. {
		Condition = GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		InitCommand=cmd(blend,"BlendMode_Add";y,235;x,-40;diffusealpha,0);
		OnCommand=cmd(sleep,2.3;diffusealpha,1;sleep,.15;linear,.4;zoomx,13;sleep,.05;linear,1;diffusealpha,0);
		};


	LoadActor("Luz") .. {
		Condition = GAMESTATE:IsPlayerEnabled( PLAYER_2 );
		InitCommand=cmd(blend,"BlendMode_Add";y,235;x,680;diffusealpha,0;rotationy,180);
		OnCommand=cmd(sleep,2.3;diffusealpha,1;sleep,.15;linear,.4;zoomx,13;sleep,.05;linear,1;diffusealpha,0);
		};

	Def.Quad {
		InitCommand=cmd(stretchto,SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM;diffuse,color("#000000");draworder,99999);
		OnCommand=cmd(diffusealpha,0);
		OffCommand=cmd(sleep,0.2;linear,0.3;diffusealpha,1);
	};
};

return t
