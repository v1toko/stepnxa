local player = ...

return Def.ActorFrame {
	Condition=GAMESTATE:IsPlayerEnabled( player );
	OffCommand=cmd(stoptweening;linear,.3;diffusealpha,0);
		
	Def.RollingNumbers {
		File=THEME:GetPathF("","WMAX1"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,200;y,83;cropright,1;shadowlength,0;zoom,.9;sleep,1.4;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(player, 'TapNoteScore_W1')+gettns(player, 'TapNoteScore_W2'),0,99999999));
	};
					
	Def.RollingNumbers {
		File=THEME:GetPathF("","WMAX1"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,200;y,135;shadowlength,0;cropright,1;zoom,.9;sleep,1.5;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(player, 'TapNoteScore_W3'),0,99999999));
	};

	Def.RollingNumbers {
		File=THEME:GetPathF("","WMAX1"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,200;y,192;shadowlength,0;cropright,1;zoom,.9;sleep,1.6;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(player, 'TapNoteScore_W4'),0,99999999));
	};
		
	Def.RollingNumbers {
		File=THEME:GetPathF("","WMAX1"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,200;y,244;shadowlength,0;cropright,1;zoom,.9;sleep,1.7;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(player, 'TapNoteScore_W5'),0,99999999));
	};

	Def.RollingNumbers {
		File=THEME:GetPathF("","WMAX1"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,200;y,296;shadowlength,0;cropright,1;zoom,.9;sleep,1.8;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(player, 'TapNoteScore_Miss'),0,99999999));
	};
		
		
	Def.RollingNumbers {
		File=THEME:GetPathF("","WMAX1"),
		Format="%4.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,200;y,348;shadowlength,0;cropright,1;zoom,.9;sleep,1.9;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(STATSMAN:GetCurStageStats():GetPlayerStageStats(player):MaxCombo(),0,99999999));
	};
				
	Def.RollingNumbers {
		File=THEME:GetPathF("","WMAX1"),
		Format="%4.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,200;y,400;shadowlength,0;cropright,1;zoom,.9;sleep,2;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(STATSMAN:GetCurStageStats():GetPlayerStageStats(player):GetCurrentMissCombo(),0,99999999));
	};
				



	Def.RollingNumbers {
		File=THEME:GetPathF("","WMAX2"),
		Format="%4.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,459;y,83;shadowlength,0;cropright,1;zoom,.9;sleep,1.6;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(STATSMAN:GetCurStageStats():GetPlayerStageStats(player):GetCurrentNumHeartStepped(),0,99999999));
	};


	Def.RollingNumbers {
		File=THEME:GetPathF("","WMAX2"),
		Format="%4.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,459;y,135;shadowlength,0;cropright,1;zoom,.9;sleep,1.7;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(STATSMAN:GetCurStageStats():GetPlayerStageStats(player):GetCurrentNumPotionsStepped(),0,99999999));
	};

	Def.RollingNumbers {
		File=THEME:GetPathF("","WMAX2"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,459;y,192;shadowlength,0;cropright,1;zoom,.9;sleep,1.8;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(player, 'TapNoteScore_HitMine'),0,99999999));
	};

	Def.RollingNumbers {
		File=THEME:GetPathF("","WMAX2"),
		Format="%4.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,459;y,244;shadowlength,0;cropright,1;zoom,.9;sleep,1.9;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(STATSMAN:GetCurStageStats():GetPlayerStageStats(player):GetCurrentVelocityItems(),0,99999999));
	};

	Def.RollingNumbers {
		File=THEME:GetPathF("","WMAX2"),
		Format="%4.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,459;y,296;shadowlength,0;cropright,1;zoom,.9;sleep,2;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(STATSMAN:GetCurStageStats():GetPlayerStageStats(player):GetCurrentNumHiddenStepped(),0,99999999));
	};

	Def.RollingNumbers {
		File=THEME:GetPathF("","WMAX2"),
		Format="%1.f %%",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,459;y,348;shadowlength,0;cropright,1;zoom,.9;sleep,2.1;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(STATSMAN:GetCurStageStats():GetPlayerStageStats(player):GetCurrentLifePercent(),0,99999999));
	};

	Def.RollingNumbers {
		File=THEME:GetPathF("","WMAX2"),
		Format="%7.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,459;y,400;shadowlength,0;cropright,1;zoom,.9;sleep,2.2;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(STATSMAN:GetCurStageStats():GetPlayerStageStats(player):GetScore(),0,99999999));
	};

};