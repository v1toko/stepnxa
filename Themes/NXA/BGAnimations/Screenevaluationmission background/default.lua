local bUsableExtras = GAMESTATE:GetCurrentMission():UseExtraStats();

local t = Def.ActorFrame {

	LoadActor("../ScreenEvaluationStage background/fondo") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT);
		OnCommand=cmd(diffuse,0,0,0,1;sleep,.5;linear,.6;diffuse,1,1,1,1);
	};
	LoadActor( "Evaluation background" ) .. { 
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoom,1.4;linear,0.5;zoom,1);
		OffCommand= cmd(linear,0.5;zoom,1.4);
	};

	LoadActor( "Evaluation background" ) .. { 
		OnCommand = cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;blend,'BlendMode_Add';sleep,0.5;diffusealpha,1;linear,0.5;zoom,1.05;diffusealpha,0);
	};
	
	LoadActor( "background usb (glow)" ) .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;sleep,.6;linear,0.5;diffusealpha,1);
		OffCommand = cmd(linear,0.1;diffusealpha,0);
	};
	
	LoadActor("barras") .. {
		OnCommand=cmd(x,320;y,240;zoom,0.48;cropbottom,1;fadebottom,0.5;sleep,0.5;linear,0.5;cropbottom,-0.5);
		OffCommand=cmd(linear,0.3;fadetop,4;fadebottom,4;diffusealpha,0);
	};

	LoadActor("orbe") .. {
		OnCommand=cmd(x,320;y,240;zoom,0.45;diffusealpha,0;sleep,0.1;linear,0.1;diffusealpha,0.5;linear,0.2;zoom,0.575;diffusealpha,.9);
		OffCommand=cmd(linear,1;sleep,0.2;diffusealpha,0);
	};

	LoadActor("darkglow") .. {
		OnCommand=cmd(x,320;y,240;zoom,1;diffusealpha,0;zoom,1;linear,0.2;diffusealpha,1;zoom,3.25;sleep,0.1;linear,0.2;diffusealpha,0;zoom,0.25);
		OffCommand=cmd(stopeffect;diffusealpha,1;zoom,0.625);
	};

	LoadActor("blankglow") .. {
		OnCommand=cmd(x,320;y,240;zoom,1;blend,'BlendMode_Add';diffusealpha,0;linear,0.2;diffusealpha,2;linear,0.3;diffusealpha,0;);
		OffCommand=cmd(stopeffect;diffusealpha,1;zoom,0.625);
	};

	LoadActor("OrbeGlow") .. {
		OnCommand=cmd(x,320;y,240;zoom,.8;blend,"BlendMode_Add";diffusealpha,0;sleep,.4;diffusealpha,.75;linear,.6;zoom,.575;diffusealpha,0);
		OffCommand=cmd();
	};

	LoadActor("O") .. {
		OnCommand=cmd(x,320;y,240;zoom,.7;blend,"BlendMode_Add";diffusealpha,0;sleep,.4;diffusealpha,.45;linear,.6;zoom,1.2;diffusealpha,0);
		OffCommand=cmd();
	};

	LoadActor("Judge") .. {
		OnCommand=cmd(x,320;y,240;zoom,.48;cropbottom,1;fadebottom,0.5;sleep,0.5;linear,0.5;cropbottom,-0.5);
		OffCommand=cmd(linear,0.3;diffusealpha,0;fadetop,4;fadebottom,4);
	};
	
	--LoadActor("Calificacion", GAMESTATE:GetFirstHumanPlayer()) .. {};
	
	--LoadActor("CalificacionExtra1", GAMESTATE:GetFirstHumanPlayer()) .. {
	--	Condition=bUsableExtras == true;
	--	OnCommand=cmd(diffuse,color("#FF9999"));
	--	OffCommand=cmd();
	--};
	
	--LoadActor("CalificacionExtra2", GAMESTATE:GetFirstHumanPlayer()) .. {
	--	Condition=bUsableExtras == true;
	--	OnCommand=cmd(diffuse,color("#FFE56E"));
	--	OffCommand=cmd();
	--};
	

	LoadActor("Stats", GAMESTATE:GetFirstHumanPlayer()) .. {};

	LoadActor("Grades/default") .. {};

	LoadActor("explosion") .. {		
		InitCommand=cmd(x,320;y,240;draworder,3;diffusealpha,0;blend,"BlendMode_Add");
		OnCommand=cmd(sleep,3.8;diffusealpha,0.5;linear,0.3;zoom,1.1;diffusealpha,0);
	};


	LoadActor("blankglow") .. {
		InitCommand=cmd(x,320;y,237;blend,"BlendMode_Add";diffuse,0.3,0,0,1;diffusealpha,0;rotationz,-60;draworder,98);
		OnCommand=cmd(zoom,1;sleep,3.8;linear,0.1;diffusealpha,0.7;zoom,1.9;linear,0.4;diffusealpha,0;zoom,1);
	};

	LoadActor("blankglow") .. {
		InitCommand=cmd(x,329;y,237;blend,"BlendMode_Add";diffuse,.5,.5,0,1;diffusealpha,0;rotationz,-60;draworder,99);
		OnCommand=cmd(zoom,1;sleep,3.8;linear,0.1;diffusealpha,1;zoom,1.5;linear,0.4;diffusealpha,0;zoom,1);
	};
};

return t; 
