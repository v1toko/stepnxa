local player = ...

return Def.ActorFrame {
	Condition=GAMESTATE:IsPlayerEnabled( player );
	OnCommand=cmd(addx,-110);
	--PLAYER 1
	LoadFont( "BoostSSi" ) .. { --perfect
		--Condition=GAMESTATE:IsPlayerEnabled( player );
		InitCommand=cmd(x,200;y,83;shadowlength,0;zoom,0.85;settext,string.format( "%.4i", gettnsfromextrastats1( player, 'TapNoteScore_W1' )+gettnsfromextrastats1( player, 'TapNoteScore_W2' ) ) );
		OnCommand=cmd(cropright,1;sleep,1;linear,0.5;cropright,-1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	--de momento no nos sirven, estan pero no escribimos nada :D
	LoadFont( "BoostSSi" ) .. { --hearts
		--Condition=GAMESTATE:IsPlayerEnabled( player );
		InitCommand=cmd(x,450;y,83;shadowlength,0;zoom,0.85;settext,string.format( "" ) );
		OnCommand=cmd(cropright,1;sleep,1;linear,0.5;cropright,-1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	LoadFont( "BoostSSi" ) .. { --great
		--Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		InitCommand=cmd(x,200;y,135;shadowlength,0;zoom,0.85;settext,string.format( "%.4i", gettnsfromextrastats1( player, 'TapNoteScore_W3' ) ) );
		OnCommand=cmd(cropright,1;sleep,1;linear,0.5;cropright,-1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	LoadFont( "BoostSSi" ) .. { --potions
		--Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		InitCommand=cmd(x,450;y,135;shadowlength,0;zoom,0.85;settext,string.format( "" ) );
		OnCommand=cmd(cropright,1;sleep,1;linear,0.5;cropright,-1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	LoadFont( "BoostSSi" ) .. { --good
		--Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		InitCommand=cmd(x,200;y,192;shadowlength,0;zoom,0.85;settext,string.format( "%.4i", gettnsfromextrastats1( player, 'TapNoteScore_W4' ) ) );
		OnCommand=cmd(cropright,1;sleep,1;linear,0.5;cropright,-1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	LoadFont( "BoostSSi" ) .. { --mines
		--Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		InitCommand=cmd(x,450;y,192;shadowlength,0;zoom,0.85;settext,string.format( "" ) );
		OnCommand=cmd(cropright,1;sleep,1;linear,0.5;cropright,-1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	LoadFont( "BoostSSi" ) .. { --bad
		--Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		InitCommand=cmd(x,200;y,244;shadowlength,0;zoom,0.85;settext,string.format( "%.4i", gettnsfromextrastats1( player, 'TapNoteScore_W5' ) ) );
		OnCommand=cmd(cropright,1;sleep,1;linear,0.5;cropright,-1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	LoadFont( "BoostSSi" ) .. { --velocity items
		--Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		InitCommand=cmd(x,450;y,244;shadowlength,0;zoom,0.85;settext,string.format( "" ) );
		OnCommand=cmd(cropright,1;sleep,1;linear,0.5;cropright,-1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	LoadFont( "BoostSSi" ) .. { --miss
		--Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		InitCommand=cmd(x,200;y,296;shadowlength,0;zoom,0.85;settext,string.format( "%.4i", gettnsfromextrastats1( player, 'TapNoteScore_Miss' ) ) );
		OnCommand=cmd(cropright,1;sleep,1;linear,0.5;cropright,-1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	LoadFont( "BoostSSi" ) .. { --hiddens
		--Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		InitCommand=cmd(x,450;y,296;shadowlength,0;zoom,0.85;settext,string.format( "" ) );
		OnCommand=cmd(cropright,1;sleep,1;linear,0.5;cropright,-1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	LoadFont( "BoostSSi" ) .. { --maxcombo
		--Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		InitCommand=cmd(x,200;y,348;shadowlength,0;zoom,0.85;settext,string.format( "%.4i", STATSMAN:GetCurStageStats():GetFirstExtraPlayerStageStats():MaxCombo() ) );
		OnCommand=cmd(cropright,1;sleep,1;linear,0.5;cropright,-1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	LoadFont( "BoostSSi" ) .. { --lifegauge
		--Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		InitCommand=cmd(x,450;y,348;shadowlength,0;zoom,0.85;settext,string.format( "" ) );
		OnCommand=cmd(cropright,1;sleep,1;linear,0.5;cropright,-1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	LoadFont( "BoostSSi" ) .. { --misscombo
		--Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		InitCommand=cmd(x,200;y,400;shadowlength,0;zoom,0.85;settext,string.format( "%.4i", STATSMAN:GetCurStageStats():GetFirstExtraPlayerStageStats():GetCurrentMissCombo() ) );
		OnCommand=cmd(cropright,1;sleep,1;linear,0.5;cropright,-1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	LoadFont( "BoostSSi" ) .. { --totalscore
		--Condition=GAMESTATE:IsPlayerEnabled( PLAYER_1 );
		InitCommand=cmd(x,450;y,400;shadowlength,0;zoom,0.85;settext,string.format( "" ) );
		OnCommand=cmd(cropright,1;sleep,1;linear,0.5;cropright,-1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};
};
