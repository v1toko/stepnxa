local t = Def.ActorFrame {	
	LoadActor("Background") .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;FullScreen);
	};
	
	LoadActor( "SS2" ) .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0);
		OnCommand=cmd(sleep,1;linear,0.6;diffusealpha,1);
		OffCommand=cmd(diffusealpha,0);
	};

	LoadActor("Select Station.png") .. {
		OnCommand = cmd(zoom,2;diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;linear,0.5;zoom,1;diffusealpha,1);
		OffCommand = cmd(sleep,0.5;linear,0.5;zoom,2);
	};

	LoadActor("Select Station.png") .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;sleep,0.8;diffusealpha,0.5;blend,'BlendMode_Add';linear,0.8;zoom,1.05;diffusealpha,0);
	};
	
	LoadFont ( "BoostSSi" ) .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+195;diffusealpha,0;linear,0.8;diffusealpha,1; settext, "Welcome to Lobby";shadowlength,0 );
	};
};
return t;
