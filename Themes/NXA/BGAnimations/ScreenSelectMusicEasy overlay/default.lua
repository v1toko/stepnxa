local t = Def.ActorFrame {
	--MODS
	
	LoadActor("EasyStationbg.png") .. {
		OnCommand = cmd(zoom,2;diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;linear,0.5;zoom,1;diffusealpha,1);
		OffCommand = cmd(linear,0.5;zoom,2);
	};

	LoadActor("EasyStationbg.png") .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;sleep,0.9;diffusealpha,0.8;blend,'BlendMode_Add';linear,0.8;zoom,1.1;diffusealpha,0);
	};
	
	LoadActor( "EasyStationbg (glow)" ) .. {
		InitCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;blend,"BlendMode_Add";diffuse,color("1.0,0.1,0.1,0"));
		OnCommand=cmd(sleep,.8;linear,0.5;diffusealpha,1);
		OffCommand=cmd(diffusealpha,0);
	};

	LoadActor( "EasyStationbg (glow)" ) .. {
		InitCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;blend,"BlendMode_Add";diffuse,color("1.0,0.1,0.1,0"));
		OnCommand=cmd(sleep,1;diffusealpha,1;linear,.8;zoom,1.1;diffusealpha,0);
		OffCommand=cmd(diffusealpha,0);
	};

	LoadActor( "../Common Utils/ArrowsScript" ) .. {
		InitCommand=cmd(addy,-10);
		OnCommand = cmd();
		OffCommand = cmd();
	};


	Def.ActorProxy {
		BeginCommand=function(self) local banner = SCREENMAN:GetTopScreen():GetChild('CDTitle');self:SetTarget(banner); end;
		OffCommand=cmd();
	};

	Def.OptionIconRow {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_1);
		InitCommand=cmd(hidden,1;x,SCREEN_LEFT+28;y,SCREEN_TOP+93;set,PLAYER_1);
		OnCommand=cmd(diffusealpha,0;linear,0.2;diffusealpha,1);
		PlayerOptionsChangedP1MessageCommand=cmd(set,PLAYER_1);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	Def.OptionIconRow {
		Condition=GAMESTATE:IsHumanPlayer(PLAYER_2);
		InitCommand=cmd(hidden,1;x,SCREEN_RIGHT-38;y,SCREEN_TOP+93;set,PLAYER_2);
		OnCommand=cmd(diffusealpha,0;linear,0.2;diffusealpha,1);
		PlayerOptionsChangedP2MessageCommand=cmd(set,PLAYER_2);
		OffCommand=cmd(linear,0.3;diffusealpha,0);
	};

	

	Def.ActorProxy {
		BeginCommand=function(self) local timer = SCREENMAN:GetTopScreen():GetChild('Timer'); self:SetTarget(timer); end;
		OnCommand=cmd(diffusealpha,1);
		OffCommand=cmd(linear,0.2;diffusealpha,0);
	};
	
	LoadActor("levelframe") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X+4;y,SCREEN_CENTER_Y-4);
		OnCommand=cmd(scaletoclipped,298,245;finishtweening;diffusealpha,0;sleep,0.4;linear,0.16;diffusealpha,1);
		OffCommand=cmd(finishtweening;linear,0.1;diffusealpha,0);
		CurrentSongChangedMessageCommand=cmd(playcommand,"On");
		CenterWasPressedTwiceMessageCommand=cmd(playcommand,"Off");
	};
	
	LoadActor("marco") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X+4;y,SCREEN_CENTER_Y-7);
		OnCommand=cmd(diffuse,getgroupcolor();scaletoclipped,298,245;finishtweening;diffusealpha,0;sleep,0.4;linear,0.16;diffusealpha,1);
		OffCommand=cmd(finishtweening;scaletoclipped,298,245;sleep,0.2;linear,0.3;zoom,0.7;accelerate,0.5;zoom,3);
		CurrentSongChangedMessageCommand=cmd(playcommand,"On");
		CenterWasPressedTwiceMessageCommand=cmd(playcommand,"Off");
		PulseCommand=cmd(finishtweening;diffusealpha,1;zoom,0.61;zoomx,0.47;linear,0.4;zoom,0.68;diffusealpha,0;sleep,0.2;queuecommand,"Pulse");
	};
	
	LoadActor("marco") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X+4;y,SCREEN_CENTER_Y-7;blend,"BlendMode_Add");
		OnCommand=cmd(diffuse,getgroupcolor();scaletoclipped,298,245;finishtweening;diffusealpha,0;sleep,0.5;linear,0.16;diffusealpha,.6);
		OffCommand=cmd(finishtweening;scaletoclipped,298,245;sleep,0.2;linear,0.3;zoom,0.7;accelerate,0.5;zoom,3);
		CurrentSongChangedMessageCommand=cmd(playcommand,"On");
		CenterWasPressedTwiceMessageCommand=cmd(playcommand,"Off");
	};
	
	LoadActor("marco") .. { --solo palpita.
		InitCommand=cmd(x,SCREEN_CENTER_X+4;y,SCREEN_CENTER_Y-7;blend,"BlendMode_Add");
		OnCommand=cmd(diffuse,getgroupcolor();scaletoclipped,298,245;finishtweening;diffusealpha,0);
		OffCommand=cmd(finishtweening;stopeffect;diffusealpha,0);
		StyleChangedMessageCommand=cmd(playcommand,"On");
		CurrentSongChangedMessageCommand=cmd(playcommand,"On");
		CenterWasPressedOnceMessageCommand=cmd(playcommand,"Pulse");
		CenterWasPressedTwiceMessageCommand=cmd(playcommand,"Off");
		PulseCommand=cmd(finishtweening;diffusealpha,1;zoom,1;linear,0.4;zoom,1.2;diffusealpha,0;sleep,0.2;queuecommand,"Pulse");
	};
	
	LoadActor("marcoglow") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X+4;y,SCREEN_CENTER_Y-7;blend,"BlendMode_Add");
		OnCommand=cmd(scaletoclipped,355,293;finishtweening;diffusealpha,0;sleep,0.4;diffusealpha,1;zoom,1.2;linear,0.2;zoom,1);
		OffCommand=cmd(finishtweening;scaletoclipped,355,293;sleep,0.2;linear,0.3;zoom,0.7;accelerate,0.5;zoom,3);
		CurrentSongChangedMessageCommand=cmd(playcommand,"On");
		CenterWasPressedTwiceMessageCommand=cmd(playcommand,"Off");
	};
	

	LoadActor("diffdisplay") .. {};

	LoadActor("../Common Utils/CenterScript") .. {
		InitCommand=cmd(addy,-60);
	};

	Def.Quad {
		InitCommand=cmd(stretchto,SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM;diffuse,color("#000000");draworder,99999);
		OnCommand=cmd(diffusealpha,0);
		OffCommand=cmd(sleep,0.65;linear,0.3;diffusealpha,1);
	};

}
return t 
