local function diffdisplaygray(pn)
	local gray = Def.ActorFrame {} for i=1,10 do
		gray[#gray+1] = LoadActor("../ScreenSelectMusicUtils/Icons/DiffIcons")..{
			InitCommand=cmd(pause;player,player);
			DiffDisplayCommand=cmd(finishtweening;diffusealpha,0;x,1*0;sleep,.6;linear,.15;x,i*14;diffusealpha,.7);
			CurrentStepsP1ChangedMessageCommand=cmd(playcommand,"DiffDisplay");
			CurrentStepsP2ChangedMessageCommand=cmd(playcommand,"DiffDisplay");
			OnCommand=cmd(faderight,.5;zoom,.5;setstate,4;playcommand,"DiffDisplay");
} end
return gray end

local function diffdisplay(pn)
	local t = Def.ActorFrame {}
	
	for cc=1,10 do
		t[#t+1] = LoadActor("../ScreenSelectMusicUtils/Icons/DiffIcons")..{
			InitCommand=cmd(pause;player,player);
			DiffDisplayCommand=function(self)
				local meter = GAMESTATE:GetCurrentSteps(GAMESTATE:GetFirstHumanPlayer())
				meter = meter:GetMeter();
				local number = (((meter-1) % 10))+1
				local hide = cc > number and 0 or 1

			if meter >= 10 then  meter = 10 end
			if meter >= 10 then number = 10 end
			(cmd(finishtweening;diffusealpha,0;zoom,.5;faderight,.5;x,cc*0;visible,hide;sleep,.6;linear,.15;x,cc*14;diffusealpha,1))(self) end;
			CurrentStepsP1ChangedMessageCommand=cmd(playcommand,"DiffDisplay");
			CurrentStepsP2ChangedMessageCommand=cmd(playcommand,"DiffDisplay");
			OnCommand=cmd(setstate,1);
	}
	end
return t
end

return Def.ActorFrame {

	LoadActor( "../ScreenSelectMusicSpecial overlay/brillo" ) .. {
		InitCommand=cmd(y,SCREEN_CENTER_Y+75;diffusealpha,0;draworder,99;zoom,.5);
		MeterCommand=cmd(finishtweening;x,287;blend,"BlendMode_Add";diffusealpha,0;sleep,.3;linear,.2;diffusealpha,1;sleep,.2;linear,.2;diffusealpha,0);
		CurrentStepsP1ChangedMessageCommand=cmd(playcommand,"Meter");
		CurrentStepsP2ChangedMessageCommand=cmd(playcommand,"Meter");
		OnCommand=cmd(stoptweening;sleep,.3;playcommand,"Meter");
		};


	Def.ActorFrame {
		InitCommand=cmd(x,275;y,SCREEN_CENTER_Y+75;playcommand,"Start");
		StartCommand=cmd(diffusealpha,0;sleep,2;diffusealpha,1);
		OffCommand=cmd(stoptweening;hidden,1);		
		diffdisplaygray()..{};
		diffdisplay()..{};

};
}