local t = Def.ActorFrame {

LoadActor("_NDA_IRO.PNG") .. {
		File="_NDA_IRO.PNG",
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;draworder,9999999),
		OnCommand=cmd(diffusealpha,0;sleep,3.65;linear,0.5;diffusealpha,1),
	};
LoadActor("LASH.PNG") .. {
		File="LASH.PNG",
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;draworder,9999999),
		OnCommand=cmd(x,175;y,264;diffusealpha,0;sleep,4.15;linear,0.5;diffusealpha,1),
	};

LoadActor("A.PNG") .. {
		File="A.PNG",
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;draworder,9999999),
		OnCommand=cmd(x,220;y,238;diffusealpha,0;zoom,1;linear,2;diffusealpha,1;sleep,1;linear,0.65;zoom,0.28;x,175;diffusealpha,1),
	};
LoadActor("M.PNG") .. {
		File="M.PNG",
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;draworder,9999999),
		OnCommand=cmd(x,420;y,238;diffusealpha,0;zoom,1;linear,2;diffusealpha,1;sleep,1;linear,0.65;zoom,0.28;x,378;diffusealpha,1;),
	};


}

return t
