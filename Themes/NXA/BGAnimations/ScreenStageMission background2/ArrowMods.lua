return Def.ActorFrame {

	LoadActor( "ChangeMods" ) .. {
		PlayCommand=cmd(play);
		VelocityChangedMessageCommand=cmd(playcommand,"Play");
		LifeChangedMessageCommand=cmd(playcommand,"Play");
		RemoveModsChangedMessageCommand=cmd(playcommand,"Play");
		LotteryChangedMessageCommand=cmd(playcommand,"Play");
	};

	LoadActor( "VelocityItems.png" ) .. {
		OnCommand=cmd(x,23;y,23;zoom,1;addx,-100;addy,-100;linear,0.3;addx,100;addy,100);
		OffCommand=cmd(linear,0.3;addx,-100;addy,-100;);
		VelocityChangedMessageCommand=function(self,param)
			local index = param.Index
			
			self:linear( 0.2 )
			self:addrotationz( 36 )	
		end;
		--ChangedCommand=cmd(finishtweening;linear,0.15;zoom,0.7;linear,0.15;zoom,1);
	};
	
	LoadActor( "VelocityMods" ) .. {
		OnCommand=cmd(x,70;y,70;zoom,1;addx,-100;addy,-100;linear,0.3;addx,100;addy,100);
		OffCommand=cmd(linear,0.3;addx,-100;addy,-100;);
	};
	
	LoadActor( "Velocity" ) .. {
		OnCommand=cmd(x,87;y,87;setstate,0;pause;addx,-100;addy,-100;linear,0.3;addx,100;addy,100);
		VelocityChangedMessageCommand=function(self,param)
			local index = param.Index
			
			self:playcommand( "Changed" )
			self:setstate( index )			
		end;
		ChangedCommand=cmd(finishtweening;linear,0.15;zoom,0.7;linear,0.15;zoom,1);
	};
	
	LoadActor( "LifeItems" ) .. {
		OnCommand=cmd(x,SCREEN_RIGHT-23;y,23;zoom,1;addy,-100;addx,100;linear,0.3;addx,-100;addy,100);
		OffCommand=cmd(linear,0.3;addy,-100;addx,100;);
		LifeChangedMessageCommand=function(self,param)
			local index = param.Index
			
			self:linear( 0.2 )
			self:addrotationz( 36 )	
		end;
		--ChangedCommand=cmd(finishtweening;linear,0.15;zoom,0.7;linear,0.15;zoom,1);
	};
	
	LoadActor( "LifeMods" ) .. {
		OnCommand=cmd(x,570;y,70;zoom,1;addy,-100;addx,100;linear,0.3;addx,-100;addy,100);
		OffCommand=cmd(linear,0.3;addy,-100;addx,100;);
	};
	
	LoadActor( "Life" ) .. {
		OnCommand=cmd(pause;x,553;y,87;zoom,1;addy,-100;addx,100;linear,0.3;addx,-100;addy,100);
		LifeChangedMessageCommand=function(self,param)
			local index = param.Index
			
			self:playcommand( "Changed" )
			self:setstate( index )			
		end;
		ChangedCommand=cmd(finishtweening;linear,0.15;zoom,0.7;linear,0.15;zoom,1);
	};
	
	LoadActor( "RemoveMods" ) .. {
		OnCommand=cmd(x,63;y,418;zoom,1;addx,-100;addy,100;linear,0.3;addx,100;addy,-100);
		OffCommand=cmd(linear,0.3;addx,-100;addy,100);
	};
	
	LoadActor( "RemoveModsBullet" ) .. {
		OnCommand=cmd(x,87;y,395;zoom,1;addx,-100;addy,100;linear,0.3;addx,100;addy,-100;diffusealpha,0);
		RemoveModsChangedMessageCommand=function(self,param)
			local index = param.Index
			
			if index == 1 then
				self:playcommand( "Changed" )
			else
				self:diffusealpha( 0 )
			end
			--self:setstate( index )			
		end;
		ChangedCommand=cmd(finishtweening;linear,0.15;zoom,0.7;sleep,0;diffusealpha,1;linear,0.15;zoom,1);
	};
	
	LoadActor( "ModeOffBulletOff.png" ) .. {
		InitCommand=cmd(diffusealpha,1);
		OnCommand=cmd(x,87;y,395;zoom,1;addx,-100;addy,100;linear,0.3;addx,100;addy,-100);
		RemoveModsChangedMessageCommand=function(self,param)
			local index = param.Index
			
			if index == 1 then
				self:playcommand( "Changed" )
			else
				self:diffusealpha( 1 )
			end
			--self:setstate( index )			
		end;
		ChangedCommand=cmd(finishtweening;diffusealpha,0;linear,0.15;zoom,0.7;diffusealpha,1;linear,0.15;zoom,1;diffusealpha,0);
	};
	
	LoadActor( "RemoveModsBullet_effect.png" ) .. {
		InitCommand=cmd(diffusealpha,0);
		OnCommand=cmd(x,87;y,395;zoom,1;addx,-100;addy,100;linear,0.3;addx,100;addy,-100);
		RemoveModsChangedMessageCommand=function(self,param)
			local index = param.Index
			
			if index == 1 then
				self:playcommand( "Changed" )
			else
				self:diffusealpha( 0 )
			end
			--self:setstate( index )			
		end;
		ChangedCommand=cmd(finishtweening;diffusealpha,0;linear,0.15;zoom,0.7;diffusealpha,1;linear,0.15;zoom,1;diffusealpha,0);
	};
	
	LoadActor( "LotteryMods" ) .. {
		OnCommand=cmd(x,577;y,418;zoom,1;addx,100;addy,100;linear,0.3;addx,-100;addy,-100);
		OffCommand=cmd(linear,0.3;addx,100;addy,100);
	};
	
	LoadActor( "LotteryBullet" ) .. {
		InitCommand=cmd(diffusealpha,0);
		OnCommand=cmd(x,553;y,395;zoom,1;addx,100;addy,100;linear,0.3;addx,-100;addy,-100);
		LotteryChangedMessageCommand=function(self,param)
			local index = param.Index
			
			if index == 1 then
				self:playcommand( "Changed" )
			else
				self:diffusealpha( 0 )
			end
			--self:setstate( index )			
		end;
		ChangedCommand=cmd(finishtweening;linear,0.15;zoom,0.7;sleep,0;diffusealpha,1;linear,0.15;zoom,1);
	};
	
	LoadActor( "LotteryBulletOff" ) .. {
		InitCommand=cmd(diffusealpha,1);
		OnCommand=cmd(x,553;y,395;zoom,1;addx,100;addy,100;linear,0.3;addx,-100;addy,-100);
		LotteryChangedMessageCommand=function(self,param)
			local index = param.Index
			
			if index == 1 then
				self:playcommand( "Changed" )
			else
				self:diffusealpha( 1 )
			end
			--self:setstate( index )			
		end;
		ChangedCommand=cmd(finishtweening;diffusealpha,0;linear,0.15;zoom,0.7;diffusealpha,1;linear,0.15;zoom,1;diffusealpha,0);
	};
	
	LoadActor( "LotteryBullet_effect.png" ) .. {
		InitCommand=cmd(diffusealpha,0);
		OnCommand=cmd(x,553;y,395;zoom,1;addx,100;addy,100;linear,0.3;addx,-100;addy,-100);
		LotteryChangedMessageCommand=function(self,param)
			local index = param.Index
			
			if index == 1 then
				self:playcommand( "Changed" )
			else
				self:diffusealpha( 0 )
			end
			--self:setstate( index )			
		end;
		ChangedCommand=cmd(finishtweening;diffusealpha,0;linear,0.15;zoom,0.7;diffusealpha,1;linear,0.15;zoom,1;diffusealpha,0);
	};
};