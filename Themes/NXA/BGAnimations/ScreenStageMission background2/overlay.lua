return Def.ActorFrame {
	LoadActor("Mods") .. {
		OnCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;addy,-10;zoom,1.5;linear,0.3;addy,10;zoom,1; hide_if,not IsArrowModsVisible() );
	};
	
	LoadActor( "ArrowMods.lua" ) .. {
		OnCommand=cmd();
	};
};