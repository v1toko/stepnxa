local t = Def.ActorFrame {

	LoadActor("fondo") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT);
		OnCommand=cmd(diffuse,0,0,0,1;sleep,.8;linear,.6;diffuse,1,1,1,1);
	};

	LoadActor( "Evaluation background" ) .. { 
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoom,1.4;linear,0.5;zoom,1);
		OffCommand= cmd(linear,0.5;zoom,1.4);
	};

	LoadActor( "Evaluation background" ) .. { 
		OnCommand = cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;blend,'BlendMode_Add';sleep,0.5;diffusealpha,1;linear,0.5;zoom,1.05;diffusealpha,0);
	};
	
	LoadActor( "background usb (glow)" ) .. {
		OnCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;diffusealpha,0;sleep,.6;linear,0.5;diffusealpha,1);
		OffCommand = cmd(linear,0.1;diffusealpha,0);
	};

	LoadActor("bars") .. {
		OnCommand=cmd(x,320;y,240;diffusealpha,.6;cropbottom,1;fadebottom,0.5;sleep,0.75;linear,0.5;cropbottom,-0.5);
		OffCommand=cmd(linear,0.3;diffusealpha,0;fadetop,4;fadebottom,4);
	};

	LoadActor("BarsBottom") .. {
		OnCommand=cmd(x,320;y,250;cropbottom,1;fadebottom,0.5;sleep,0.75;linear,0.5;cropbottom,-0.5);
		OffCommand=cmd(linear,0.3;diffusealpha,0;fadetop,4;fadebottom,4);
	};

	LoadActor("O") .. {
		OnCommand=cmd(x,320;y,240;zoom,.6;blend,"BlendMode_Add";glowshift;diffuse,0,0,1,1;diffusealpha,0;sleep,.2;linear,.2;diffusealpha,.5;sleep,.1;linear,.2;diffusealpha,0);
	};

	LoadActor("orbe") .. {
		OnCommand=cmd(x,320;y,240;zoom,0.575;diffuse,.8,.8,1,1;diffusealpha,0;sleep,0.3;linear,0.1;diffusealpha,0.5;linear,0.2;diffusealpha,.9;linear,5;diffuse,1,1,1,1);
		OffCommand=cmd(linear,1;sleep,0.2;diffusealpha,0);
	};

	LoadActor("orbe") .. {
		OnCommand=cmd(x,320;y,240;blend,"BlendMode_Add";zoom,.7;diffusealpha,0;sleep,0.2;linear,0.3;diffusealpha,0.5;zoom,0.575;diffusealpha,.9;linear,.2;diffusealpha,0;sleep,.005;diffusealpha,.5;linear,.6;zoom,.7;diffusealpha,0);
		OffCommand=cmd(linear,1;sleep,0.2;diffusealpha,0);
	};

	LoadActor("darkglow") .. {
		OnCommand=cmd(x,320;y,240;zoom,1;diffusealpha,0;zoom,1;linear,0.2;diffusealpha,1;zoom,3.25;sleep,0.1;linear,0.2;diffusealpha,0;zoom,0.25);
		OffCommand=cmd(stopeffect;diffusealpha,1;zoom,0.625);
	};

	LoadActor("blankglow") .. {
		OnCommand=cmd(x,320;y,240;zoom,1;blend,'BlendMode_Add';diffusealpha,0;linear,0.2;diffusealpha,2;linear,0.3;diffusealpha,0;);
		OffCommand=cmd(stopeffect;diffusealpha,1;zoom,0.625);
	};

	LoadActor("OrbeGlow") .. {
		OnCommand=cmd(x,320;y,240;zoom,.8;blend,"BlendMode_Add";diffusealpha,0;sleep,.6;diffusealpha,.75;linear,.6;zoom,.575;diffusealpha,0);
		OffCommand=cmd();
	};

	LoadActor("O") .. {
		OnCommand=cmd(x,320;y,240;zoom,.7;blend,"BlendMode_Add";diffusealpha,0;sleep,.6;diffusealpha,.45;linear,.6;zoom,1.2;diffusealpha,0);
		OffCommand=cmd();
	};

	LoadActor("Judge") .. {
		OnCommand=cmd(x,320;y,240;cropbottom,1;fadebottom,0.5;sleep,0.8;linear,0.5;cropbottom,-0.5);
		OffCommand=cmd(linear,0.3;fadetop,4;fadebottom,4);
	};

	LoadActor("Stats") .. {};
	
	Def.Quad {
		InitCommand=cmd(stretchto,SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM;diffuse,color("#000000");draworder,99999);
		OnCommand=cmd(diffusealpha,0);
		OffCommand=cmd(linear,0.3;diffusealpha,1);
	};

};

return t; 
