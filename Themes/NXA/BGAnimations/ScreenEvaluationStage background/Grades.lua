local function Grade(pn)
	local PlayerGrade = STATSMAN:GetCurStageStats():GetPlayerStageStats(pn):GetGrade()
	local Grades = {Grade_Tier01 = "S";Grade_Tier02 = "S";Grade_Tier03 = "A";Grade_Tier04 = "B";Grade_Tier05 = "C";Grade_Tier06 = "D";Grade_Tier07 = "F";Grade_Failed = "F";}
	local Picture = Grades[PlayerGrade] return LoadActor( Picture..".png" )..{InitCommand=cmd(player,pn);} end

local grade = Def.ActorFrame {

		Grade(PLAYER_1)..{
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_1);
		InitCommand=cmd(y,237;x,110);
		OnCommand=cmd(diffusealpha,0;zoom,1.6;sleep,3.5;linear,0.2;diffusealpha,1;zoom,.9);
		OffCommand=cmd(stoptweening;linear,0.3;diffusealpha,0);
		};

		Grade(PLAYER_1)..{
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_1);
		InitCommand=cmd(y,237;x,110;blend,"BlendMode_Add");
		OnCommand=cmd(diffusealpha,0;zoom,.9;sleep,3.7;diffusealpha,1;linear,1;diffusealpha,0;zoom,1.8);
		OffCommand=cmd(stoptweening;linear,0.3;diffusealpha,0);
		};

		Grade(PLAYER_2)..{
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_2);
		InitCommand=cmd(y,237;x,537);
		OnCommand=cmd(diffusealpha,0;zoom,1.6;sleep,3.5;linear,0.2;diffusealpha,1;zoom,.9);
		OffCommand=cmd(stoptweening;linear,0.3;diffusealpha,0);
		};

		Grade(PLAYER_2)..{
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_2);
		InitCommand=cmd(y,237;x,537;blend,"BlendMode_Add");
		OnCommand=cmd(diffusealpha,0;zoom,.9;sleep,3.7;diffusealpha,1;linear,1;diffusealpha,0;zoom,1.8);
		OffCommand=cmd(stoptweening;linear,0.3;diffusealpha,0);
		};

	}
return grade