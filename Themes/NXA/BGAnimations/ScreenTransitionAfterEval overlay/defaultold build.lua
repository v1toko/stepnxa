local t = Def.ActorFrame {
	LoadActor( "next1" ) .. {
		Condition=GAMESTATE:GetStageIndex() == 1;
		OnCommand=cmd(FullScreen);
	};
	
	LoadActor( "NST1.MP3" ) .. {
		Condition=GAMESTATE:GetStageIndex() == 1;
		OnCommand=cmd(play);
	};
		
	LoadActor( "bonus" ) .. {
		Condition=GAMESTATE:GetStageIndex() == 2 and GAMESTATE:IsNormalMode();
		OnCommand=cmd(FullScreen);
	};
	
	LoadActor( "NST3.MP3" ) .. {
		Condition=GAMESTATE:GetStageIndex() == 2 and GAMESTATE:IsNormalMode();
		OnCommand=cmd(play);
	};
	
	LoadActor( "next2" ) .. {
		Condition=GAMESTATE:GetStageIndex() == 2 and not GAMESTATE:IsNormalMode();
		OnCommand=cmd(FullScreen);
	};
	
	LoadActor( "NST2.MP3" ) .. {
		Condition=GAMESTATE:GetStageIndex() == 2 and not GAMESTATE:IsNormalMode();
		OnCommand=cmd(play);
	};
	
	LoadActor("../ScreenLoadUsb out/ToNormal.png") .. {
		Condition=GAMESTATE:GetPlayMode() == "PlayMode_Regular";
		OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT;sleep,3;diffusealpha,1;linear,2);
	};

	LoadActor("../ScreenLoadUsb out/ToSpecial.png") .. {
		Condition=GAMESTATE:GetPlayMode() == "PlayMode_Special";
		OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT;sleep,3;diffusealpha,1;linear,2);
	};

	LoadActor("../ScreenLoadUsb out/ToWorldMax.png") .. {
		Condition=GAMESTATE:GetPlayMode() == "PlayMode_Mission";
		OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT;sleep,3;diffusealpha,1;linear,2);
	};

	LoadActor("../ScreenLoadUsb out/ToEasy.png") .. {
		Condition=GAMESTATE:GetPlayMode() == "PlayMode_Easy";
		OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT;sleep,3;diffusealpha,1;linear,2);
	};
		
	LoadActor("../ScreenLoadUsb out/ToBrain.png") .. {
		Condition=GAMESTATE:GetPlayMode() == "PlayMode_Brain";
		OnCommand=cmd(diffusealpha,0;x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;scaletoclipped,SCREEN_WIDTH,SCREEN_HEIGHT;sleep,3;diffusealpha,1;linear,2);
	};
	
	Def.Actor { OnCommand=cmd(sleep,2.8); };
};

return t;

