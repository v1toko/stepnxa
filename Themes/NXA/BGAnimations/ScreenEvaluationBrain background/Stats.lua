return Def.ActorFrame {

	Def.ActorFrame {
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_1);
		OffCommand=cmd(stoptweening;linear,0.1;diffusealpha,0);
		
	Def.RollingNumbers {
		File=THEME:GetPathF("","Evaluation"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,16;y,112;cropright,1;shadowlength,0;horizalign,'HorizAlign_Left';sleep,1.4;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(PLAYER_1, 'TapNoteScore_W1')+gettns(PLAYER_1, 'TapNoteScore_W2'),0,99999999));
	};
					
	Def.RollingNumbers {
		File=THEME:GetPathF("","Evaluation"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,16;y,154;shadowlength,0;cropright,1;horizalign,'HorizAlign_Left';sleep,1.5;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(PLAYER_1, 'TapNoteScore_W3'),0,99999999));
	};

	Def.RollingNumbers {
		File=THEME:GetPathF("","Evaluation"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,16;y,196;shadowlength,0;cropright,1;horizalign,'HorizAlign_Left';sleep,1.6;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(PLAYER_1, 'TapNoteScore_W4'),0,99999999));
	};
		
	Def.RollingNumbers {
		File=THEME:GetPathF("","Evaluation"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,16;y,238;shadowlength,0;cropright,1;horizalign,'HorizAlign_Left';sleep,1.7;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(PLAYER_1, 'TapNoteScore_W5'),0,99999999));
	};

	Def.RollingNumbers {
		File=THEME:GetPathF("","Evaluation"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,16;y,280;shadowlength,0;cropright,1;horizalign,'HorizAlign_Left';sleep,1.8;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(PLAYER_1, 'TapNoteScore_Miss'),0,99999999));
	};
		
						
	LoadFont( "Evaluation" ) .. {--CORRECTAS / INCORRECTAS
		InitCommand=cmd(settext,string.format( "%.2i / %.2i", STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetCurrentCorrectAnswers(),STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetCurrentIncorrectAnswers());shadowlength,0);
		OnCommand=cmd(x,16;horizalign,'HorizAlign_Left';y,322;cropright,1;sleep,1.9;linear,.7;cropright,-1);
		OffCommand=cmd(linear,0.1;diffusealpha,0);
	};

	Def.RollingNumbers {
		File=THEME:GetPathF("","Evaluation"),
		Format="%7.f",
		ApproachSeconds=1.1,
		TargetNumber=0,
		InitCommand=cmd(x,16;y,364;shadowlength,0;cropright,1;horizalign,'HorizAlign_Left';sleep,2;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetScore(),0,99999999));
	};
				
};
	
	Def.ActorFrame {
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_2);
		OffCommand=cmd(stoptweening;linear,0.1;diffusealpha,0);
		
	Def.RollingNumbers {
		File=THEME:GetPathF("","Evaluation"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,625;y,112;shadowlength,0;cropright,1;horizalign,'HorizAlign_Right';sleep,1.4;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(PLAYER_2, 'TapNoteScore_W1')+gettns(PLAYER_2, 'TapNoteScore_W2'),0,99999999));
	};
					
	Def.RollingNumbers {
		File=THEME:GetPathF("","Evaluation"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,625;y,154;shadowlength,0;cropright,1;horizalign,'HorizAlign_Right';sleep,1.5;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(PLAYER_2, 'TapNoteScore_W3'),0,99999999));
	};

	Def.RollingNumbers {
		File=THEME:GetPathF("","Evaluation"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,625;y,196;shadowlength,0;cropright,1;horizalign,'HorizAlign_Right';sleep,1.6;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(PLAYER_2, 'TapNoteScore_W4'),0,99999999));
	};
		
	Def.RollingNumbers {
		File=THEME:GetPathF("","Evaluation"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,625;y,238;shadowlength,0;cropright,1;horizalign,'HorizAlign_Right';sleep,1.7;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(PLAYER_2, 'TapNoteScore_W5'),0,99999999));
	};

	Def.RollingNumbers {
		File=THEME:GetPathF("","Evaluation"),
		Format="%3.f",
		ApproachSeconds=0.8,
		TargetNumber=0,
		InitCommand=cmd(x,625;y,280;shadowlength,0;cropright,1;horizalign,'HorizAlign_Right';sleep,1.8;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(gettns(PLAYER_2, 'TapNoteScore_Miss'),0,99999999));
	};
		
		
	LoadFont( "Evaluation" ) .. {--CORRECTAS / INCORRECTAS
		InitCommand=cmd(settext,string.format( "%.2i / %.2i", STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetCurrentCorrectAnswers(),STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetCurrentIncorrectAnswers());shadowlength,0);
		OnCommand=cmd(x,625;horizalign,'HorizAlign_Right';y,322;cropright,1;sleep,1.9;linear,.7;cropright,-1);
		OffCommand=cmd(linear,0.1;diffusealpha,0);
	};

				
	Def.RollingNumbers {
		File=THEME:GetPathF("","Evaluation"),
		Format="%7.f",
		ApproachSeconds=1.1,
		TargetNumber=0,
		InitCommand=cmd(x,625;y,364;shadowlength,0;cropright,1;horizalign,'HorizAlign_Right';sleep,2;queuecommand,"Cicle");
		CicleCommand=cmd( linear,.7;cropright,0;targetnumber,clamp(STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetScore(),0,99999999));
	};


};

};