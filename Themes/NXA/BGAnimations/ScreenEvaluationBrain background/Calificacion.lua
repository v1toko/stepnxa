return Def.ActorFrame {
	Def.ActorFrame {
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_1);
		
		LoadFont( "BoostSSi" ) .. {--perfects
			InitCommand=cmd(settext,string.format( "%.4i", gettns(PLAYER_1, 'TapNoteScore_W1')+gettns(PLAYER_1, 'TapNoteScore_W2'));zoom,0.5;shadowlength,0);
			OnCommand=cmd(x,60;y,98;cropright,1;sleep,1;linear,.5;cropright,-1);
			OffCommand=cmd(linear,0.1;diffusealpha,0);
		};
		
		LoadFont( "BoostSSi" ) .. {--great
			InitCommand=cmd(settext,string.format( "%.4i", gettns(PLAYER_1, 'TapNoteScore_W3'));zoom,0.5;shadowlength,0);
			OnCommand=cmd(x,60;y,145;cropright,1;sleep,1.2;linear,.7;cropright,-1);
			OffCommand=cmd(linear,0.1;diffusealpha,0);
		};
		
		LoadFont( "BoostSSi" ) .. {--good
			InitCommand=cmd(settext,string.format( "%.4i", gettns(PLAYER_1, 'TapNoteScore_W4'));zoom,0.5;shadowlength,0);
			OnCommand=cmd(x,60;y,192;cropright,1;sleep,1.4;linear,.5;cropright,-1);
			OffCommand=cmd(linear,0.1;diffusealpha,0);
		};
		
		LoadFont( "BoostSSi" ) .. {--bad
			InitCommand=cmd(settext,string.format( "%.4i", gettns(PLAYER_1, 'TapNoteScore_W5'));zoom,0.5;shadowlength,0);
			OnCommand=cmd(x,60;y,240;cropright,1;sleep,1.6;linear,.5;cropright,-1);
			OffCommand=cmd(linear,0.1;diffusealpha,0);
		};
		
		LoadFont( "BoostSSi" ) .. {--miss
			InitCommand=cmd(settext,string.format( "%.4i", gettns(PLAYER_1, 'TapNoteScore_Miss'));zoom,0.5;shadowlength,0);
			OnCommand=cmd(x,60;y,284;cropright,1;sleep,1.8;linear,.5;cropright,-1);
			OffCommand=cmd(linear,0.1;diffusealpha,0);
		};
		
		LoadFont( "BoostSSi" ) .. {--CORRECTAS / INCORRECTAS
			InitCommand=cmd(settext,string.format( "%.3i / %.3i", STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetCurrentCorrectAnswers(),STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetCurrentIncorrectAnswers());zoom,0.5;shadowlength,0);
			OnCommand=cmd(x,80;y,331;cropright,1;sleep,2.0;linear,.5;cropright,-1);
			OffCommand=cmd(linear,0.1;diffusealpha,0);
		};
		
		LoadFont( "BoostSSi" ) .. {--score
			InitCommand=cmd(settext,string.format( "%.7i", STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_1):GetScore());zoom,0.5;shadowlength,0);
			OnCommand=cmd(x,91;y,378;cropright,1;sleep,2.2;linear,.5;cropright,-1);
			OffCommand=cmd(linear,0.1;diffusealpha,0);
		};
	};
	
	Def.ActorFrame {
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_2);
		InitCommand=cmd(horizalign,right);
		
		LoadFont( "BoostSSi" ) .. {--perfects
			InitCommand=cmd(settext,string.format( "%.4i", gettns(PLAYER_2, 'TapNoteScore_W1')+gettns(PLAYER_2, 'TapNoteScore_W2'));zoom,0.5;shadowlength,0);
			OnCommand=cmd(x,SCREEN_RIGHT-60;y,98;cropright,1;sleep,1;linear,.5;cropright,-1);
			OffCommand=cmd(linear,0.1;diffusealpha,0);
		};
		
		LoadFont( "BoostSSi" ) .. {--great
			InitCommand=cmd(settext,string.format( "%.4i", gettns(PLAYER_2, 'TapNoteScore_W3'));zoom,0.5;shadowlength,0);
			OnCommand=cmd(x,SCREEN_RIGHT-60;y,145;cropright,1;sleep,1.2;linear,.7;cropright,-1);
			OffCommand=cmd(linear,0.1;diffusealpha,0);
		};
		
		LoadFont( "BoostSSi" ) .. {--good
			InitCommand=cmd(settext,string.format( "%.4i", gettns(PLAYER_2, 'TapNoteScore_W4'));zoom,0.5;shadowlength,0);
			OnCommand=cmd(x,SCREEN_RIGHT-60;y,192;cropright,1;sleep,1.4;linear,.5;cropright,-1);
			OffCommand=cmd(linear,0.1;diffusealpha,0);
		};
		
		LoadFont( "BoostSSi" ) .. {--bad
			InitCommand=cmd(settext,string.format( "%.4i", gettns(PLAYER_2, 'TapNoteScore_W5'));zoom,0.5;shadowlength,0);
			OnCommand=cmd(x,SCREEN_RIGHT-60;y,240;cropright,1;sleep,1.6;linear,.5;cropright,-1);
			OffCommand=cmd(linear,0.1;diffusealpha,0);
		};
		
		LoadFont( "BoostSSi" ) .. {--miss
			InitCommand=cmd(settext,string.format( "%.4i", gettns(PLAYER_2, 'TapNoteScore_Miss'));zoom,0.5;shadowlength,0);
			OnCommand=cmd(x,SCREEN_RIGHT-60;y,284;cropright,1;sleep,1.8;linear,.5;cropright,-1);
			OffCommand=cmd(linear,0.1;diffusealpha,0);
		};
		
		LoadFont( "BoostSSi" ) .. {----CORRECTAS / INCORRECTAS
			InitCommand=cmd(settext,string.format( "%.3i / %.3i", STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetCurrentCorrectAnswers(),STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetCurrentIncorrectAnswers());zoom,0.5;shadowlength,0);
			OnCommand=cmd(x,SCREEN_RIGHT-80;y,331;cropright,1;sleep,2.0;linear,.5;cropright,-1);
			OffCommand=cmd(linear,0.1;diffusealpha,0);
		};
		
		LoadFont( "BoostSSi" ) .. {--score
			InitCommand=cmd(settext,string.format( "%.7i", STATSMAN:GetCurStageStats():GetPlayerStageStats(PLAYER_2):GetScore());zoom,0.5;shadowlength,0);
			OnCommand=cmd(x,SCREEN_RIGHT-91;y,378;cropright,1;sleep,2.2;linear,.5;cropright,-1);
			OffCommand=cmd(linear,0.1;diffusealpha,0);
		};
	};
};