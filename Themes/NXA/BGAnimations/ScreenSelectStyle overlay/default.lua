local t = Def.ActorFrame {

	Def.ActorFrame {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y+10;diffusealpha,0;blend,"BlendMode_Add");
		DownLeftPressedMessageCommand=cmd(stopeffect;finishtweening;stoptweening;diffusealpha,0);
		DownRightPressedMessageCommand=cmd(stopeffect;finishtweening;stoptweening;diffusealpha,0);
		OffCommand=cmd(stopeffect;finishtweening;stoptweening;diffusealpha,0);


	LoadActor( THEME:GetPathG( "Station", "single" ) ) .. {
		SelectedsingleMessageCommand=cmd(playcommand,"Repeat");
		RepeatCommand=cmd(finishtweening;zoom,1;diffusealpha,0.5;linear,0.6;zoom,1.2;diffusealpha,0;sleep,0.5;queuecommand,"Repeat");
		PlayerJoinedMessageCommand=cmd(stoptweening;diffusealpha,0;zoom,1);
	};
	
	LoadActor( THEME:GetPathG( "Station", "singleversus" ) ) .. {
		SelectedsingleversusMessageCommand=cmd(playcommand,"Repeat");
		RepeatCommand=cmd(finishtweening;zoom,1;diffusealpha,0.5;linear,0.6;zoom,1.2;diffusealpha,0;sleep,0.5;queuecommand,"Repeat");
		PlayerJoinedMessageCommand=cmd(stoptweening;diffusealpha,0;zoom,1);
	};
	
	LoadActor("sounds/arcade") .. {
		SelectedsingleMessageCommand=cmd(play);
		SelectedsingleversusMessageCommand=cmd(play);
		OnCommand=cmd();
	};

	LoadActor( THEME:GetPathG( "Station", "special" ) ) .. {
		SelectedspecialMessageCommand=cmd(playcommand,"Repeat");
		RepeatCommand=cmd(finishtweening;zoom,1;diffusealpha,0.5;linear,0.6;zoom,1.2;diffusealpha,0;sleep,0.5;queuecommand,"Repeat");
		PlayerJoinedMessageCommand=cmd(stoptweening;diffusealpha,0;zoom,1);
	};
	
	LoadActor( THEME:GetPathG( "Station", "specialversus" ) ) .. {
		SelectedspecialversusMessageCommand=cmd(playcommand,"Repeat");
		RepeatCommand=cmd(finishtweening;zoom,1;diffusealpha,0.5;linear,0.6;zoom,1.2;diffusealpha,0;sleep,0.5;queuecommand,"Repeat");
		PlayerJoinedMessageCommand=cmd(stoptweening;diffusealpha,0;zoom,1);
	};
	
	LoadActor("sounds/special") .. {
		SelectedspecialMessageCommand=cmd(play);
		SelectedspecialversusMessageCommand=cmd(play);
		OnCommand=cmd();
	};

	LoadActor( THEME:GetPathG( "Station", "easy" ) ) .. {
		SelectedeasyMessageCommand=cmd(playcommand,"Repeat");
		RepeatCommand=cmd(finishtweening;zoom,1;diffusealpha,0.5;linear,0.6;zoom,1.2;diffusealpha,0;sleep,0.5;queuecommand,"Repeat");
		PlayerJoinedMessageCommand=cmd(stoptweening;diffusealpha,0;zoom,1);
	};
	
	LoadActor( THEME:GetPathG( "Station", "easyversus" ) ) .. {
		SelectedeasyversusMessageCommand=cmd(playcommand,"Repeat");
		RepeatCommand=cmd(finishtweening;zoom,1;diffusealpha,0.5;linear,0.6;zoom,1.2;diffusealpha,0;sleep,0.5;queuecommand,"Repeat");
		PlayerJoinedMessageCommand=cmd(stoptweening;diffusealpha,0;zoom,1);
	};
	
	LoadActor("sounds/easy") .. {
		SelectedeasyMessageCommand=cmd(play);
		SelectedeasyversusMessageCommand=cmd(play);
		OnCommand=cmd();
	};

	LoadActor( THEME:GetPathG( "Station", "mission" ) ) .. {
		SelectedmissionMessageCommand=cmd(playcommand,"Repeat");
		RepeatCommand=cmd(finishtweening;zoom,1;diffusealpha,0.5;linear,0.6;zoom,1.2;diffusealpha,0;sleep,0.5;queuecommand,"Repeat");
		PlayerJoinedMessageCommand=cmd(stoptweening;diffusealpha,0;zoom,1);
	};
	
	LoadActor("sounds/mission") .. {
		SelectedmissionMessageCommand=cmd(play);
		OnCommand=cmd();
	};

	LoadActor( THEME:GetPathG( "Station", "brain" ) ) .. {
		SelectedbrainMessageCommand=cmd(playcommand,"Repeat");
		RepeatCommand=cmd(finishtweening;zoom,1;diffusealpha,0.5;linear,0.6;zoom,1.2;diffusealpha,0;sleep,0.5;queuecommand,"Repeat");
		PlayerJoinedMessageCommand=cmd(stoptweening;diffusealpha,0;zoom,1);
	};
	
	LoadActor( THEME:GetPathG( "Station", "brainversus" ) ) .. {
		SelectedbrainversusMessageCommand=cmd(playcommand,"Repeat");
		RepeatCommand=cmd(finishtweening;zoom,1;diffusealpha,0.5;linear,0.6;zoom,1.2;diffusealpha,0;sleep,0.5;queuecommand,"Repeat");
		PlayerJoinedMessageCommand=cmd(stoptweening;diffusealpha,0;zoom,1);
	};

	LoadActor("sounds/brain") .. {
		SelectedbrainMessageCommand=cmd(play);
		SelectedbrainversusMessageCommand=cmd(play);
		OnCommand=cmd();
	};

};

	LoadActor("mask") .. {
		InitCommand = cmd(x,SCREEN_CENTER_X;y,252);
		OnCommand=cmd(clearzbuffer,true;zwrite,true;blend,"BlendMode_NoEffect");
	};


	LoadActor("glow") .. {
		InitCommand = cmd(x,SCREEN_CENTER_X;y,252;glowshift;zoom,.975;diffuse,.9,.9,.9,1;diffusealpha,0);
		OnCommand=cmd(diffusealpha,0;sleep,1.6;linear,.25;diffusealpha,1);
		PulseCommand=cmd(finishtweening;linear,.1;diffusealpha,0;sleep,.8;linear,.2;diffusealpha,1);
		DownLeftPressedMessageCommand=cmd(playcommand,"Pulse");
		DownRightPressedMessageCommand=cmd(playcommand,"Pulse");
		PlayerJoinedMessageCommand=cmd(stoptweening;diffusealpha,0;sleep,1.3;diffusealpha,1);
		OffCommand=cmd(stopeffect;stoptweening;diffuse,.85,.85,.85,1;sleep,.2;linear,0.45;zoom,0.75;diffuse,1,1,1,1;glowshift;linear,0.45;zoom,2.5;diffuse,.8,.8,.8,.8);
	};

	LoadActor("ring") .. {
		InitCommand = cmd(ztest,true;x,SCREEN_CENTER_X;y,252;blend,"BlendMode_Add";diffusealpha,0;sleep,1;glowshift;effectcolor1,.8,.8,1,.4;effectcolor2,1,1,1,0;effectperiod,1;zoom,1.25;linear,.3;zoom,.85;diffusealpha,1);
		OnCommand=cmd(diffusealpha,0;zoom,1.25;sleep,.4;linear,.25;diffusealpha,1;zoom,.85;spin;effectmagnitude,0,0,-140);
		PulseCommand=cmd(finishtweening;linear,.1;diffusealpha,0;zoom,.95;sleep,.3;zoom,.95;linear,.3;accelerate,.3;diffusealpha,1;zoom,.85);
		DownLeftPressedMessageCommand=cmd(playcommand,"Pulse");
		DownRightPressedMessageCommand=cmd(playcommand,"Pulse");
		PlayerJoinedMessageCommand=cmd(stoptweening;diffusealpha,0;sleep,1.3;zoom,1;linear,.3;diffusealpha,1;zoom,.85);
		OffCommand=cmd(stoptweening;linear,0.1;diffusealpha,0);
	};

	LoadActor("back") .. {
		InitCommand = cmd(x,SCREEN_CENTER_X;y,252;blend,"BlendMode_Add";diffusealpha,0;sleep,1;glowshift;effectcolor1,.8,.8,1,.4;effectcolor2,1,1,1,0;effectperiod,1;zoom,1.25;linear,0.3;zoom,1;diffusealpha,1);
		OnCommand=cmd(diffusealpha,0;zoom,1.45;sleep,.4;linear,0.25;diffusealpha,1;zoom,1.05);
		PulseCommand=cmd(finishtweening;linear,0.1;diffusealpha,0;zoom,1.15;sleep,0.3;zoom,1.15;linear,0.3;accelerate,0.3;diffusealpha,1;zoom,1.05);
		DownLeftPressedMessageCommand=cmd(playcommand,"Pulse");
		DownRightPressedMessageCommand=cmd(playcommand,"Pulse");
		PlayerJoinedMessageCommand=cmd(stoptweening;diffusealpha,0;sleep,1.3;zoom,1,15;linear,.3;diffusealpha,1;zoom,1.05);
		OffCommand=cmd(stoptweening;linear,0.1;diffusealpha,0);
	};


	LoadActor("../Common Utils/CenterScript") .. {InitCommand=cmd(draworder,999999999);};
	LoadActor("../Common Utils/ArrowsScript") .. {};
	
	LoadActor("arrows") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoomx,.8);
		OnCommand=cmd(diffuse,1,.9,.9,1;diffusealpha,0;sleep,1;linear,.3;diffusealpha,1;zoomx,1);
		DownLeftPressedMessageCommand=cmd(finishtweening;diffuse,.8,.8,.8,1;sleep,1;linear,.5;diffuse,1,1,1,1);
		DownRightPressedMessageCommand=cmd(finishtweening;diffuse,.8,.8,.8,1;sleep,1;linear,.5;diffuse,1,1,1,1);
		OffCommand=cmd(stoptweening;linear,.7;diffusealpha,0);
	};

	LoadActor("arrowglow") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y;zoomx,.8;blend,"BlendMode_Add";glowshift;effectcolor1,1,.5,.5,0.4;effectcolor2,1,1,1,0;effectperiod,1);
		OnCommand=cmd(diffuse,1,.5,.5,1;diffusealpha,0;sleep,1;linear,.3;diffusealpha,.6;zoomx,1);
		DownLeftPressedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,1;linear,.5;diffusealpha,.6);
		DownRightPressedMessageCommand=cmd(finishtweening;diffusealpha,0;sleep,1;linear,.5;diffusealpha,.6);
		OffCommand=cmd(stoptweening;linear,.7;diffusealpha,0);
	};

	LoadActor("arrow") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X-174;y,SCREEN_CENTER_Y;diffusealpha,0;fadetop,.1;fadebottom,.1;glowshift;effectcolor1,1,.5,.5,0.4;effectcolor2,1,1,1,0;effectperiod,1);
		OnCommand=cmd(diffuse,1,.5,.5,1;diffusealpha,0);
		DownLeftPressedMessageCommand=cmd(finishtweening;zoom,1;diffusealpha,.4;linear,.5;zoom,1.2;sleep,.1;linear,.3;diffusealpha,0);
		OffCommand=cmd(stoptweening;diffusealpha,0);
	};

	LoadActor("arrow") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X+181;y,SCREEN_CENTER_Y;rotationy,180;diffusealpha,0;fadetop,.1;fadebottom,.1;glowshift;effectcolor1,1,.5,.5,0.4;effectcolor2,1,1,1,0;effectperiod,1);
		OnCommand=cmd(diffuse,1,.5,.5,1;diffusealpha,0);
		DownRightPressedMessageCommand=cmd(finishtweening;zoom,1;diffusealpha,.4;linear,.5;zoom,1.2;sleep,.1;linear,.3;diffusealpha,0);
		OffCommand=cmd(stoptweening;diffusealpha,0);
	};

	LoadActor("arrow") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X-174;y,SCREEN_CENTER_Y;diffusealpha,0;fadetop,.1;fadebottom,.1;glowshift;effectcolor1,1,.5,.5,0.4;effectcolor2,1,1,1,0;effectperiod,1);
		OnCommand=cmd(diffuse,1,.3,.3,1;diffusealpha,0;blend,"BlendMode_Add");
		DownLeftPressedMessageCommand=cmd(finishtweening;zoom,1;diffusealpha,1;linear,.5;zoom,1.2;sleep,.1;linear,.3;diffusealpha,0);
		OffCommand=cmd(stoptweening;diffusealpha,0);
	};

	LoadActor("arrow") .. {
		InitCommand=cmd(x,SCREEN_CENTER_X+181;y,SCREEN_CENTER_Y;rotationy,180;diffusealpha,0;fadetop,.1;fadebottom,.1;glowshift;effectcolor1,1,.5,.5,0.4;effectcolor2,1,1,1,0;effectperiod,1);
		OnCommand=cmd(diffuse,1,.3,.3,1;diffusealpha,0;blend,"BlendMode_Add");
		DownRightPressedMessageCommand=cmd(finishtweening;zoom,1;diffusealpha,1;linear,.5;zoom,1.2;sleep,.1;linear,.3;diffusealpha,0);
		OffCommand=cmd(stoptweening;diffusealpha,0);
	};

	LoadActor( "Start" ) .. {
		OnCommand=cmd();
		CenterWasPressedOnceMessageCommand=cmd(play);};


	Def.Quad {
		InitCommand=cmd(stretchto,SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM;diffuse,color("#000000");draworder,99999);
		OnCommand=cmd(diffusealpha,0);
		OffCommand=cmd(sleep,0.6;linear,0.3;diffusealpha,1);
	};
};

return t
