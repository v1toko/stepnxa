local function CreditsText( pn )
	function update(self)
		local str = ScreenSystemLayerHelpers.GetCreditsMessage(pn);
		if GAMESTATE:IsEventMode() then
			self:settext( "EVENT" );
			return
		end
		self:settext(str);
	end

	function UpdateVisible(self)
		local screen = SCREENMAN:GetTopScreen();
		local bShow = true;
		if screen then
			local sClass = screen:GetName();
			bShow = THEME:GetMetric( sClass, "ShowCoinsAndCredits" );
		end

		self:visible( bShow );
	end

	local text = LoadFont(Var "LoadingScreen","credits") .. {
		InitCommand=THEME:GetMetric(Var "LoadingScreen","CreditsInitCommand");
		RefreshCreditTextMessageCommand=update;
		CoinInsertedMessageCommand=update;
		PlayerJoinedMessageCommand=update;
		ScreenChangedMessageCommand=UpdateVisible;
	};
	return text;
end

local t = Def.ActorFrame {
	LoadActor( "Common Utils/Credits.png" ) .. { 
		InitCommand = cmd(x,SCREEN_CENTER_X;y,SCREEN_CENTER_Y);
	};

	LoadFont(Var "LoadingScreen","message") .. {
		InitCommand=cmd(maxwidth,750;
		horizalign,left;vertalign,top;
		zoom,0.8;shadowlength,2;
		y,SCREEN_TOP+20;
		diffusealpha,0
		);

		SystemMessageMessageCommand = function(self, params)
				self:settext( params.Message );
			local f = cmd(finishtweening;x,SCREEN_LEFT+20;diffusealpha,1;addx,-SCREEN_WIDTH;linear,0.5;addx,SCREEN_WIDTH); f(self);
			self:playcommand( "On" );
			if params.NoAnimate then
				self:finishtweening();
			end
			f = cmd(sleep,5;linear,0.5;diffusealpha,0); f(self);
			self:playcommand( "Off" );
		end;
		HideSystemMessageMessageCommand = cmd(finishtweening);
	};
	CreditsText( PLAYER_1 ) .. {
		InitCommand=cmd(x,THEME:GetMetric(Var "LoadingScreen","CreditsP1X");y,THEME:GetMetric(Var "LoadingScreen","CreditsP1Y"););
	};
	CreditsText( PLAYER_2 ) .. {
		InitCommand=cmd(x,THEME:GetMetric(Var "LoadingScreen","CreditsP2X");y,THEME:GetMetric(Var "LoadingScreen","CreditsP2Y"););
	};
	
	LoadActor("Common Utils/DisplayProfileName.lua") .. {
		OnCommand=cmd();
		OffCommand=cmd();
	};

};

t[#t+1] = Def.Actor {
	CoinInsertedMessageCommand=function(self)
		local coins = GAMESTATE:GetCoins()
		local cointojoin = GAMESTATE:GetCoinsNeededToJoin()
		local remain = coins % cointojoin
		if remain == 0 then
			SOUND:PlayOnce(THEME:GetPathS("","COIN2"));
		else
			SOUND:PlayOnce(THEME:GetPathS("","COIN1"));
		end
	end;
}

return t;
