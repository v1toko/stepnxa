local function Grade(pn)
	local PlayerGrade = STATSMAN:GetCurStageStats():GetPlayerStageStats(pn):GetGrade()
	local Grades = {Grade_Tier01 = "S";Grade_Tier02 = "S";Grade_Tier03 = "A";Grade_Tier04 = "B";Grade_Tier05 = "C";Grade_Tier06 = "D";Grade_Tier07 = "F";Grade_Failed = "F";}
	local Picture = Grades[PlayerGrade] return LoadActor( Picture..".png" )..{
	
	InitCommand=cmd(player,pn);
	OffCommand=cmd(stoptweening; diffusealpha, 0);
} 	
end

local t = Def.ActorFrame {

	Grade(PLAYER_1)..{
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_1);
			InitCommand=cmd(y,237;x,110);
			OnCommand=cmd(diffusealpha,0;zoom,1.9;sleep,3.6;diffusealpha,.2;linear,0.2;diffusealpha,.7;zoom,.9);
			OffCommand=cmd(stoptweening;linear,0.3;diffusealpha,0);
		};

	Grade(PLAYER_1)..{
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_1);
			InitCommand=cmd(y,237;x,110);
			OnCommand=cmd(diffusealpha,0;zoom,1.4;sleep,3.6;diffusealpha,.2;linear,0.2;diffusealpha,.7;zoom,.9);
			OffCommand=cmd(stoptweening;linear,0.3;diffusealpha,0);
		};

	Grade(PLAYER_1)..{
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_1);
			InitCommand=cmd(y,237;x,110;blend,"BlendMode_Add");
			OnCommand=cmd(diffusealpha,0;zoom,.9;sleep,3.8;diffusealpha,1;linear,.6;zoom,2;diffusealpha,0);
			OffCommand=cmd(stoptweening;linear,0.3;diffusealpha,0);
		};


	LoadActor("../ScreenEvaluationStage background/O") .. {
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_1);
			OnCommand=cmd(x,120;y,237;blend,"BlendMode_Add";zoom,.4;diffusealpha,0;sleep,3.8;diffusealpha,.5;linear,.4;zoomx,.7;zoomy,.5;diffusealpha,0);
			OffCommand=cmd(stoptweening; diffusealpha, 0);
		};

	LoadActor("../ScreenSelectMusicSpecial overlay/brillo") .. {
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_1);
		OnCommand=cmd(x,120;y,237;blend,"BlendMode_Add";zoom,.8;diffusealpha,0;sleep,3.2;linear,.2;diffusealpha,1;linear,1;zoom,2.2;linear,.2;diffusealpha,0;zoom,2.4);
		OffCommand=cmd(stoptweening; diffusealpha, 0);
	};

	Grade(PLAYER_2)..{
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_2);
			InitCommand=cmd(y,237;x,537);
			OnCommand=cmd(diffusealpha,0;zoom,1.9;sleep,3.6;diffusealpha,.2;linear,0.2;diffusealpha,.7;zoom,.9);
			OffCommand=cmd(stoptweening;linear,0.3;diffusealpha,0);
		};

	Grade(PLAYER_2)..{
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_2);
			InitCommand=cmd(y,237;x,537);
			OnCommand=cmd(diffusealpha,0;zoom,1.4;sleep,3.6;diffusealpha,.2;linear,0.2;diffusealpha,.7;zoom,.9);
			OffCommand=cmd(stoptweening;linear,0.3;diffusealpha,0);
		};

	Grade(PLAYER_2)..{
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_2);
			InitCommand=cmd(y,237;x,537;blend,"BlendMode_Add");
			OnCommand=cmd(diffusealpha,0;zoom,.9;sleep,3.8;diffusealpha,1;linear,.6;zoom,2;diffusealpha,0);
			OffCommand=cmd(stoptweening;linear,0.3;diffusealpha,0);
		};

	LoadActor("../ScreenEvaluationStage background/O") .. {
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_2);
			OnCommand=cmd(x,540;y,237;blend,"BlendMode_Add";zoom,.4;diffusealpha,0;sleep,3.8;diffusealpha,.5;linear,.4;zoomx,.7;zoomy,.5;diffusealpha,0);
			OffCommand=cmd(stoptweening; diffusealpha, 0);
		};

	LoadActor("../ScreenSelectMusicSpecial overlay/brillo") .. {
		Condition=GAMESTATE:IsPlayerEnabled(PLAYER_2);
			OnCommand=cmd(x,540;y,237;blend,"BlendMode_Add";zoom,.8;diffusealpha,0;sleep,3.2;linear,.2;diffusealpha,1;linear,1;zoom,2.2;linear,.2;diffusealpha,0;zoom,2.4);
			OffCommand=cmd(stoptweening; diffusealpha, 0);
		};


	LoadActor("explosion") .. {
			OnCommand=cmd(x,320;y,237;zoomy,.85;blend,"BlendMode_Add";draworder,99;diffusealpha,0;sleep,3.8;diffusealpha,.35;linear,0.4;zoomx,1.5;linear,.2;zoomy,1;zoomx,1.75;diffusealpha,0);
			OffCommand=cmd(stoptweening; diffusealpha, 0);
		};



};

return t; 
