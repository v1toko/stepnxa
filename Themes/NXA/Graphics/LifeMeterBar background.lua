--return Def.Quad {
--	InitCommand=cmd(diffuse,color("#000000");diffusetopedge,0.5,0.5,0.5,1);
--};

local t = Def.ActorFrame {
	LoadActor("LifeMeterBar background.png") .. {
		OnCommand=cmd(zoom,0.035;zoomx,0.004);
	};
--	LoadActor("demonstration.png") .. {
--		OnCommand=cmd(hidden,1);
--	};
};

return t;
