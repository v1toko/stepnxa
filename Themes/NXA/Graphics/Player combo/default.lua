local c;
local player = Var "Player";
local ShowComboAt = THEME:GetMetric("Combo", "ShowComboAt");
local Pulse = THEME:GetMetric("Combo", "PulseCommand");

local NumberMinZoom = THEME:GetMetric("Combo", "NumberMinZoom");
local NumberMaxZoom = THEME:GetMetric("Combo", "NumberMaxZoom");
local NumberMaxZoomAt = THEME:GetMetric("Combo", "NumberMaxZoomAt");

local t = Def.ActorFrame {
	LoadFont( "Combo", "numbers" ) .. {
		Name="Number";
		OnCommand = THEME:GetMetric("Combo", "NumberOnCommand");
	};
	LoadActor("Combo label") .. {
		Name="ComboLabel";
		OnCommand = THEME:GetMetric("Combo", "LabelOnCommand");
	};
	LoadActor("Combo misses") .. {		
		Name="MissesLabel";
		OnCommand = THEME:GetMetric("Combo", "LabelOnCommand");
	};

	InitCommand = function(self)
		c = self:GetChildren();
		c.Number:visible(false);
		c.ComboLabel:visible(false);
		c.MissesLabel:visible(false);
	end;

	ComboCommand=function(self, param)
		local iCombo = param.Misses or param.Combo;
		if not iCombo or iCombo < ShowComboAt then
			c.Number:visible(false);
			c.ComboLabel:visible(false);
			c.MissesLabel:visible(false);
			return;
		end		

		local iUsingRevert = GAMESTATE:IsUsingReverseGrade(player);

		local Label;
		if iUsingRevert == 0 then
			if param.Combo then
				Label = c.ComboLabel;
			else
				Label = c.MissesLabel;
			end
		else
			if param.Combo then
				Label = c.MissesLabel;
			else
				Label = c.ComboLabel;
			end
		end


		local misscombo;
		if iUsingRevert == 0 then
			if param.Misses then
				misscombo = color("1,.3,.3,.85");
			else
				misscombo = color("1,1,1,1");
			end
		else
			if param.Misses then
				misscombo = color("1,1,1,1");
			else
				misscombo = color("1,.3,.3,.85");
			end
		end

		-- reset the label's visibility so we don't accidentally draw both labels
		Label:visible(false);

		param.Zoom = scale( iCombo, 0, NumberMaxZoomAt, NumberMinZoom, NumberMaxZoom );
		param.Zoom = clamp( param.Zoom, NumberMinZoom, NumberMaxZoom );

		c.Number:stoptweening();
		c.Number:visible(true);
		c.Number:diffuse(misscombo);
		
		if not GAMESTATE:IsEditing() then
			Label:visible(true);		
		end

		if iCombo<10 then

			c.Number:settext( string.format("00%i", iCombo) );
		
		end

		if iCombo>=10 and iCombo<100 then

			c.Number:settext( string.format("0%i", iCombo) );
		
		end

		if iCombo>=100 then

			c.Number:settext( string.format("%i", iCombo) );
		
		end

		Pulse( c.Number, param );
		Pulse( Label, param );
	end;
};

return t;
