local text = ...;

return Def.ActorFrame {
        OnCommand=cmd();
        OffFocusedCommand=cmd();
        OffUnfocusedCommand=cmd();

	LoadFont( "Common", "normal" ) .. {
		Text=THEME:GetString("PaneDisplay", text);
		OnCommand=cmd(hidden,1);
	};
};
