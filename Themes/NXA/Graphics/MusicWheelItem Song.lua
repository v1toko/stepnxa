return Def.ActorFrame {

	LoadActor("_cristal.png") .. {
	Condition=GAMESTATE:IsBrainShowerMode() or GAMESTATE:IsEasyMode();
	InitCommand=cmd(zoom,1.1;addy,-6);
	OnCommand=cmd(diffuse,1,1,1,1);
	CurrentSongChangedMessageCommand=cmd(stoptweening;diffuse,1,1,1,1);
	DownLeftPressedMessageCommand=cmd(stoptweening;diffuse,1,1,1,1);
	DownRightPressedMessageCommand=cmd(stoptweening;diffuse,1,1,1,1);
	};

	LoadActor("_bframeBs.png") .. {
	Condition=GAMESTATE:IsBrainShowerMode() or GAMESTATE:IsEasyMode();
	InitCommand=cmd(zoomx,.485;zoomy,.63;addy,-.2);
	};


	LoadActor("_cristal.png") .. {
	Condition=GAMESTATE:IsNormalMode() or GAMESTATE:IsSpecialMode();
	InitCommand=cmd(zoomx,1.075;addy,-3;croptop,.075);
	OnCommand=cmd(diffuse,1,1,1,1);
	CurrentSongChangedMessageCommand=cmd(stoptweening;diffuse,1,1,1,1);
	DownLeftPressedMessageCommand=cmd(stoptweening;diffuse,1,1,1,1);
	DownRightPressedMessageCommand=cmd(stoptweening;diffuse,1,1,1,1);
	};

	LoadActor("_bframe.png") .. {
	Condition=GAMESTATE:IsNormalMode() or GAMESTATE:IsSpecialMode();
	InitCommand=cmd(zoomx,.9675;zoomy,.985);
	};

};