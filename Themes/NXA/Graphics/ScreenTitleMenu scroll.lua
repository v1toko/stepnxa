local t = Def.ActorFrame {
	LoadFont("", "Mileage") ..{
		Text=THEME:GetString( 'ScreenTitleMenu', Var("GameCommand"):GetText() );
		InitCommand=cmd(horizalign,center;shadowlength,0);
		GainFocusCommand=cmd(stoptweening;linear,0.2;zoom,1.15;diffuseshift;effectperiod,0.5;effectcolor1,1,1,0,1;effectcolor2,.75,.75,0,1;);
		LoseFocusCommand=cmd(stoptweening;stopeffect;zoom,1);
		DisabledCommand=cmd(diffuse,0.5,0.5,0.5,1);
	};
};

return t;
