#!/bin/bash
#  --disable-option-checking  ignore unrecognized --enable/--with options
#  --disable-FEATURE       do not include FEATURE (same as --enable-FEATURE=no)
#  --enable-FEATURE[=ARG]  include FEATURE [ARG=yes]
#  --enable-maintainer-mode  enable make rules and dependencies not useful 		  (and sometimes confusing) to the casual installer
#  --disable-dependency-tracking  speeds up one-time build
#  --enable-dependency-tracking   do not reject slow dependency extractors
#  --disable-rpath         do not hardcode runtime library paths
#  --disable-gtk2          Disable GTK support
#  --disable-test       do not try to compile and run a test GTK+ program
#  --enable-force-oss      Force OSS
#  --enable-tests          Build test programs
#  --enable-lua-binaries   Build lua and luac
#
#    Optional Packages:
#  --with-PACKAGE[=ARG]    use PACKAGE [ARG=yes]
#  --without-PACKAGE       do not use PACKAGE (same as --with-PACKAGE=no)
#  --with-debug            Enable debug mode
#  --with-fast-compile     Enable fast compile
#  --with-prof             Enable profiling
#  --with-x                use the X Window System
#  --with-static-png       Statically link libpng
#  --without-jpeg          Disable JPEG support
#  --with-static-jpeg      Statically link libjpeg
#  --without-network       Disable networking
#  --with-static-zlib      Statically link zlib
#  --without-vorbis        Disable Vorbis support
#  --with-integer-vorbis   Integer vorbis decoding only
#  --with-static-vorbis    Statically link vorbis libraries
#  --without-mp3           Disable MP3 support
#  --without-ffmpeg        Disable ffmpeg support
#  --with-static-ffmpeg    Statically link ffmpeg libraries
#  --with-alsa-prefix=PFX  Prefix where Alsa library is installed(optional)
#  --with-alsa-inc-prefix=PFX  Prefix where include libraries are (optional)
#  --with-gnu-ld           assume the C compiler uses GNU ld default=no
#  --with-libiconv-prefix[=DIR]  search for libiconv in DIR/include and DIR/lib
#  --without-libiconv-prefix     don't search for libiconv in includedir and libdir


echo "Limpiando compilacion anterior"
call make distclean

echo "Borrando anterior bin"
call rm stepmania
echo "ok"

echo "compilando libav"
call cd libav-0.8.12/
call ./configure --enable-static --disable-vaapi
call make
echo "ok"

echo "copiando librerias para linkear con stepnxa"
call cp libavcodec/libavcodec.a ../../src/
call cp libavutil/libavutil.a ../../src/
call cp libavformat/libavformat.a ../../src/
call cp libswscale/libswscale.a ../../src/
echo "ok"

call cd ..

echo "corriendo autogen"
call ./autogen.sh

echo "corriendo configure"
call ./configure

echo "compilando"
call make -s

call cd src/
call sh linkear_con_ffmpeg.sh
call mv stepmania ../
echo "listo: binario ok ctm"